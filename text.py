import os
import nltk

class Corpus():
    """Corpus-level activity."""
    def __init__(self, path, ending=".txt"):
        """Each file with the specified ending within the stated path will be construed as a document."""
        self.path = path
        self.ending = ending

        self.stopwords = nltk.corpus.stopwords.words("english")
        self.corpus = []
        self.sentences = []
        self.corpus_files = self.get_corpus_files()

        print(f"Found {len(self.corpus_files)} files.")

    def get_corpus_files(self):
        """Look in path and find all files to build the corpus."""
        return [os.path.join(self.path, t) for t in os.listdir(self.path) if t.endswith(self.ending)]

    def build_corpus(self, lowercase=True, remove_stopwords=True):
        """Convert corpus file pointers to corpora of texts."""
        for path in self.corpus_files:
            doc = []
            with open(path) as f:
                raw = f.read().lower() if lowercase else f.read()
            tokens = nltk.word_tokenize(raw)
            for token in tokens:

                if (token in stopwords and remove_stopwords) or not Corpus.no_punct(token):
                    continue

                else:
                    doc.append(token.strip())

            self.corpus.append(doc)

    def build_sentence_corpus(self, lowercase=True, remove_stopwords=True, minimum_sentence_length=None):
        """Build corpus of sentences, no longer separated by document."""
        for path in self.corpus_files:
            with open(path) as f:
                raw = f.read().lower() if lowercase else f.read()

            sentences = [nltk.word_tokenize(s) for s in nltk.sent_tokenize(raw)]
            for s in sentences:

                if minimum_sentence_length:
                    if len(s) < minimum_sentence_length:
                        continue

                if remove_stopwords:
                    clean_sentence = [w for w in s if w not in self.stopwords and Corpus.no_punct(w)]

                else:
                    clean_sentence = [w for w in s if Corpus.no_punct(w)]

                self.sentences.append(clean_sentence)

        return self.sentences

    @staticmethod
    def no_punct(w):
        """Check for tokens which are all punctuation.

        Return 'true' if w is free of punctuation.
        Return 'false' if any non-alpha, non-digit characters exist.

        Suitable for tokenizers that separate out punctuation.
        """
        check = False
        if not all((t.isalpha() or t.isdigit()) for t in w):
            pass
        else:
            check = True
        return check
