Sunset Hills Elementary School, established in 1974, has a reputation for providing an excellent learning environment for students. 
We pride ourselves on implanting a positive school wide climate where "Seagulls Soar and Character Counts!". Sunset Hills currently 
serves approximately 457 students in Transitional Kindergarten through Fifth grade. This quaint elementary school is located in Rancho 
Penasquitos and is part of the renowned Poway Unified School District (PUSD). The families of Sunset Hills represent more than 28 
languages. Of the languages, Vietnamese at 5% and Chinese at 3% are the most popular languages spoken. Fifteen (13%) of our 
population are Limited English Language Learners. Twelve (12%) of our students qualify for the Free and Reduced Lunch Program. 
Enhancing the schools diversity is the self-contained special education classes. Twenty one (23%) of our total population are 
designated students with disabilities. Many of our Special Education Students are mainstreamed into the general education classes. 
This provides a valuable experience for all. 
 
Sunset Hills Elementary School is a proud member of the NO EXCUSES UNIVERSITY (NEU) national network of schools. College 
readiness is not the expectation that all students will attend college, but instead, it is the belief that we must prepare all students to 
have a full range of postsecondary education and training options available to them after high school. We believe that elementary 
education is the foundation for all future academic achievement. All of our classrooms have adopted a College or University and we 
proudly wear a college bound No Excuse shirt every Monday to kick off each week. 
 
All staff at Sunset Hills commit to a culture of Universal Achievement for all students. The following commitment is endorsed by all 
staff: "Universal Achievement is the commitment we make to the academic accomplishments of all children. We hold our students to 
high standards, with the belief that they are capable learners who have the right to be prepared for college, careers and a life of 
learning. We are further committed to developing the well-being of our students by encouraging a balance of academic, social and 
emotional growth. We commit to a language of hope to foster a positive school environment. At Sunset Hills, all students, parents 
and staff members are treated with courtesy and respect. We acknowledge that challenges exist, but we refuse to accept excuses." 
Visitors regularly comment about the friendly and welcoming environment of our school and the intense level of commitment every 
staff member has for each student's success. Character Counts lessons are embedded in many activities on campus including our 
TRRFFC Thursday assemblies where we come together to celebrate good character, friendship, our community and school pride. 
 
Our Sunset Hills community annually develops a 3 Way Pledge (Students + Parents + Teacher = Success) endorsing the commitment 
to student engagement and following the Character Counts traits in a safe setting. Students, teachers and parents create their own 
pledge at separate summits. The success of Sunset Hills owes much to the staff and parents, who give their time, share their skills and 
donate resources to our school. Our biggest strength at Sunset Hills is the sense of a caring community who work collaboratively on 
behalf of each and every student. 
 
 
 
 

2017-18 School Accountability Report Card for Sunset Hills Elementary School 

Page 2 of 11