American Canyon Middle School was the first Gold Ribbon Award winning middle school in Napa County. We serve the City of 
American Canyon, the southernmost city in Napa County. The school is located on the Western edge of American Canyon abutting 
the wetlands and floodplain of the Northern San Francisco Bay. The mission statement of American Canyon Middle School is "Inspiring 
and empowering students today to shape a better tomorrow." ACMS is a school that offers technology infused Project Based 
Learning. It is essentially a 1:1 computing environment where laptop computers are utilized as a research and learning tool. Teachers 
focus on providing engaging units of learning, centered on the Common Core Standards and connected to student's personal lives 
with real-world applications. Students work in a collaborative learning environment to master each subject's content and to improve 
their Communication, Critical Thinking, Creativity, Global Citizenship, Character and Collaboration skills. American Canyon Middle 
School is a member of the New Tech Network. 
 
The students at American Canyon Middle School are served by forty-five full-time teachers and a support staff that includes two 
counselors, a school resource officer, five instructional assistants, and part-time specialists including a school psychologist, speech 
therapist, registered nurse, health clerk, social worker, and library technician. The school operates on a block schedule. The class 
periods are 84 minutes in length. Students alternate schedules on an “A” and “B” schedule day. Mathematics, Language Arts and 
advisory do not alternate but meet daily. Students rotate in various electives at the end of each trimester. This is a wonderful school 
for students due to the polite behavior of our student body and the quality of our teachers.