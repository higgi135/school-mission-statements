While honoring Kaizen, "What can I do better today, than yesterday" and adhering to our District's vision of "Everyday a WIN!" the 
mission of Rafer Johnson Junior High School is to make sure that every student will be a hero and become "The Best They Can Be." 
 
SCHOOL BELIEFS 
RJJH will establish high academic, personal, and behavioral expectations for ALL students. Our goal is to establish an environment of 
respect, and a reputation for extra-curricular, athletic, and academic excellence as a result of reinforcing strong work ethics coupled 
with self-awareness, self-discipline, and self-respect. 
 
The Staff of Rafer Johnson Junior High is committed to lifelong learning and are committed to nurturing high self-esteem and respect 
for others. We believe that everyone can learn, become better thinkers and independent learners. An integral part of our learning 
process will have our school community learning how to ask questions, solve problems and make thoughtful decisions. We are 
committed to setting the precedence for teaching and learning. We encourage the qualities of honesty, respect, responsibility, and 
empowering students to acquire habits of effectiveness and distinction that will guide them in their future. 
 
The goal of Rafer Johnson Junior High is to prepare our students for high school and beyond by providing a safe environment that 
nurtures their intellectual, physical, social and moral capacities as individuals so they can contribute to their community. 
 
On behalf of the entire faculty and staff at Rafer Johnson, we want to welcome you to a new school year. You are an important part 
of the success of the Rafer team. We hope that your two years at RJJH will be challenging, rewarding and engaging.