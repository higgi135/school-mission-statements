SCHOOL DESCRIPTION 
George B. Miller Elementary School is one of eight elementary schools in the Centralia School District and is located in the City of La 
Palma, a suburban community located in the Northwest corner of Orange County. George B. Miller Elementary School's attendance 
area also includes portions of the City of Buena Park and the City of Cypress. George B. Miller Elementary School is a school in which 
educational excellence is a shared responsibility between students, staff, family and community. Children are the focal point of all 
decisions. Students and staff members hold themselves to high standards of personal performance and are accountable for their 
decisions and their actions. Our goal for all students is that they become self-motivated individuals who demonstrate respect for 
themselves and others by learning to excel in a culturally diverse population. 
 
A School-Wide Positive Behavior Intervention System (PBIS) program is in operation at the school. The program focuses on instructing 
students in the school's core values: Be Respectful and Achieve. Be Considerate and Excel (R.A.C.E. to Success). 
 
VISION STATEMENT 
In alignment with Centralia School District's vision, George B. Miller Elementary School is a leader in education. It is a positive, 
collaborative environment where new ideas are encouraged and innovative practices are fostered. Student success is our top priority 
and is valued by the entire community. 
 
MISSION STATEMENT 
In alignment with the Centralia School District's mission, George B. Miller Elementary School is committed to meeting the diverse 
educational needs of all students and creating lifelong learners, by providing exceptional staff and opportunities for family and 
community involvement in a safe and nurturing environment. 
 
AWARDS 
Platinum Award- PBIS Implementation, 2017, 2018 
National Red Ribbon Photo Contest Winner, 2017 
City of La Palma Red Ribbon Challenge Winner, 2017, 2018 
California Distinguished School Award, 2006, 2010 
California Business for Education Excellence Honor Roll School 2012-2017