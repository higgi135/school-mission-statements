Vision: We envision… 

 

• 
• 
• 
• 
• 

• 
• 
• 
• 
• 
• 

*A school where students will develop foundational skills, individual talents, and skills needed to be college and career ready. 
*A school where everyone is physically and emotionally safe. 
*A school where everyone takes responsibility for their own actions. 
*A school where parents, community, and staff encourage and support students to do their best. 
*A school where 100% of all students will meet or exceed their individual growth goals based on the California Common Core 
Standards. 
*A school where students have learning opportunities to develop 21st Century Skills. 
*A school where students are technologically literate and globally minded. 
*A school where students and staff communicate effectively and work cooperatively. 
*A school where students will develop critical thinking and problem solving skills. 
*A school where students and staff model the Eight Great Character Traits. 
*A school where students give to others and the greater community. 

 
Mission: 
Core Values (belief statements that guide us) 

• 
• 
• 
• 
• 
• 
• 
• 

*Children come first. 
*All children can learn. 
*We focus on results. (meeting/exceeding growth targets) 
*Our expectations and standards are high. 
*Evaluation drives improvement. 
*Collaboration and teamwork improves student achievement. 
*We honor diversity. 
*We act ethically and with integrity, and treat everyone with courtesy and respect. 

 
Principal's Message 
 
The River Oaks staff takes great pride in creating a culturally sensitive school environment that is safe, nurturing, caring, and 
intellectually challenging. High standards have been set for behavior and academic personal growth. Students are recognized and 
rewarded daily, weekly, and monthly for demonstrating the Eight Great Character Traits in their school work and personal interactions 
with adults and peers. We believe it is important for students and parents to have a voice and to feel a sense of ownership and pride 
in their school. Students have many opportunities to participate in extracurricular activities such as: Student Council, Cross Age 
Tutoring, Band, Choir, After School Clubs, Makers Lab, and Running Clubs. Parents are encouraged to volunteer in and out of the 
classrooms. They support teachers and contribute to our positive school community in many ways. We are thankful for a very active 
and supportive PTA, English Language Advisory Committee, and School Site Council. All students are challenged to meet individual 
growth goals and to perform to the best of their abilities. Individual strengths and talents are recognized in all learners. There are 
many opportunities for students to use their strengths and talents at school. The teachers and support staff at River Oaks are 
dedicated to student achievement towards meeting Common Core State Standards. Teachers, specialists, and administration 
collaborate on a regular basis to provide a personalized, standards-based education for all learners. Careful and precise data analysis 
of state and local assessments drive instruction and the needs for enrichment and remedial interventions. We are proud to report 
that River Oaks has been recognized as a California Distinguished School in 1995, 2002, 2008, and 2018. 
 
 
 

2017-18 School Accountability Report Card for River Oaks Elementary School 

Page 2 of 11