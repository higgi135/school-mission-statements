Glenwood Elementary School is in the Linden Unified School District, a medium-sized rural district serving approximately 2,252 
students in grades K-12. The District is located in the eastern part of San Joaquin County and is comprised of four elementary schools, 
one comprehensive high school, and one alternative high school. Glenwood Elementary School is a kindergarten through eighth grade 
elementary school with an enrollment of 405 students as of November, 2017. Glenwood School's Mission Statement was updated 
with input from all stakeholders: 
 
Glenwood School is where all children flourish. This will be accomplished by creating a challenging learning environment in which all 
students will learn and succeed in a bully free environment. 
 
Each student will: 
* gain confidence in oneself 
* Become creative thinkers 
* Be kind 
* Show empathy for others 
 
Each child will be encouraged to: 
* Think highly of oneself 
* Have good reading and study skills 
* Collaborate with others 
 
Each child will show: 
* Confidence in oneself 
* A feeling of self worth 
* Excitement for learning 
* Excitement for excelling 
* Excitement for others 
 
Each child will know: 
* They matter 
* Their viewpoints are important 
* Different ways to problem solve 
 
 
 
 

2017-18 School Accountability Report Card for Glenwood Elementary 

Page 2 of 11