Come Back Butte Charter (CBBC) opened in the fall of 2016, as a place where students could come back and finish what they started, 
by completing the necessary courses to earn a high school diploma. CBBC is a locally-funded, dependent, public charter school (Charter 
#1811) authorized by the Butte County Board of Education and administered by the Butte County Superintendent of Schools through 
Butte County Office of Education's Student Programs and Services Division. CBBC is a no cost program for students, ages 16 and older, 
who have not previously experienced success in traditional educational settings. CBBC serves students who did not complete high 
school for various reasons. Students from Butte County, as well as adjacent counties (Tehama, Plumas, Yuba, Sutter, Colusa and Glenn), 
may enroll at CBBC. 
 
At CBBC students are provided with the opportunity to earn their high school diploma and think about ways to focus on their future 
plans and goals. Through an independent study instructional model students meet with teachers once a week to discuss their progress 
as they work at their own pace on-line. The CBBC staff works with each student to set attainable goals and work toward earning a 
high school diploma, developing skills to prepare for vocational programs, and pursuing higher education. 
 
Using data-based problem solving methods, through Multi-Tiered Systems of Supports (MTSS), the staff at CBBC are able to build 
strong relationships with students while helping students to make positive academic progress and learn to be successful students. 
The staff also work closely with local community partners to provide appropriate support programs for students. 
 
Mission Statement: Transforming lives through exemplary education and vocational training.