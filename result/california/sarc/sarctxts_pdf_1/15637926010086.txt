Principal’s Message 
 
Standard Elementary, built in the 1940s, is a traditional grade TK-5 school in a residential neighborhood of Bakersfield. Although the 
district has recently reconfigured the boundaries to manage the growing student population, Standard’s numbers have settled at 
approximately 576 with additional growth expected over the next few years. The school has a diverse population that includes 
Caucasian, Hispanic, and African-American students. 
 
Standard Elementary provides a comprehensive California Content Standards curriculum with a strong emphasis on language arts, 
reading, and math skills. Teachers work together to plan and assess the progress of the students and to ensure that the curriculum is 
delivered in an effective and consistent manner. The goal is for student assessments to demonstrate continuous growth in academic 
achievement. Recent CAASPP scores demonstrate various success over the past 3 years. The District purchased common core language 
arts and math curriculum in the 16-17 academic year. Goals for this school year include the continuation of the successful 
implementation of Common Core, the successful integration of the new curriculum, Professional Learning Communities, the continued 
refinement of the Positive Behavioral Interventions and Supports (PBIS) system, and increasing student use and learning of technology. 
Teachers have attended workshops to develop a solid understanding of Common Core, and are implementing Common Core lessons 
in the classroom. PBIS focuses on schoolwide expectations and positive reinforcement for appropriate behaviors and is being refined 
throughout the school year. Standard recently achieved the Silver Certification from the California PBIS Coalition. Standard Elementary 
now has deployed Chromebooks to all first through fifth-grade students that are being utilized daily to enhance student learning. Each 
kindergarten class has access to Chromebooks to support Common Core practice during small groups. Standard Elementary provides 
Multi-Tiered Systems of Support for the students who are not making growth on the Smarter Balanced CASSPP assessments or not 
showing sufficient success in their language arts or math classes. Classes use core materials, Lexia and Renaissance Learning for Math 
and Language Arts. 
 
Standard has a schoolwide Title I designation and receives federal monies to fund supplementary educational opportunities in math, 
reading, and language arts. All students are served breakfast and lunch at no cost to families. LCAP and lottery funds support our 
library. 
 
Parents and community members are an important part of our academic program. We welcome participation and encourage joining 
Parent Teacher Student Association (PTSA). Our entire staff looks forward to working with families and our students for another year 
of academic success. 
 
Standard Elementary teachers value respect, honesty, integrity, and the joy of learning. 
 
School Mission Statement: 
Standard Elementary School is committed to a quality educational experience for all students. Students who are participating in the 
district’s curriculum and instruction will be literate, self-reliant, prepared for the future, and willing to contribute constructively to 
society. Our school will nurture this idea by being supportive, emphasizing high expectations, valuing the worth of each individual, and 
promoting a safe and secure environment in a facility that effectively serves students. 
 
School Vision Statement: 
Striving Together All Reach Success 
“Making a difference, one STAR at a time.” 
Standard STARS are safe, responsible, respectful! 
 
 

2017-18 School Accountability Report Card for Standard Elementary School 

Page 2 of 11