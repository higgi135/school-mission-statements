The mission of Bill E. Young, Jr. Middle School, a learning community with a tradition of high expectations, is to ensure an educational 
experience where students achieve their academic, personal and social potential through a system of learning distinguished by: 

• A comprehensive and rigorous curriculum. 
• A school environment of responsibility, service, respect, and trust. 
• A variety of academic and social support programs and interventions. 
• A strong partnership with family and community. 
• A celebration of individual and school-wide achievement. 
• A technology rich environment.