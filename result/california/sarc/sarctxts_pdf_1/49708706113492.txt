Piner-Olivet Charter School is a small, nurturing middle school with a strong academic focus for 7th and 8th graders with a total of 201 
students. Students are divided into four teams with each team having two core subject teachers; one for English/history and one for 
math/science. Curriculum is Common Core standards-based. Students also take enrichment classes, which may include; maker, 
mulitimedia, student leadership, minecraft, health, PE, gardening, robotics, and a wide variety of art courses. Staff includes 8 full time 
teachers, one half time teacher, 1 Physical Education Specialist, 6 Program Assistants, Office Manager and Principal. Our Resource 
Specialist is shared with Jack London School. All teachers are fully credentialed. 
 
Special services include Resource Specialist, speech, nurse, and counseling. Extended School Year classes are offered as needed and 
during the summer. Additional support is provided to students in the regular classroom by Program Assistants and in support classes 
during the enrichment schedule in English Language Development, Math and Language Arts/Math Support classes, and after school 
support sessions. 
 
Piner-Olivet Charter School is housed on the Jack London Elementary campus. It has eight full classrooms clustered in its own area and 
uses one Jack London Elementary classroom that is nearby. The Charter School shares the gym, computer lab, food services, office, 
RSP classroom, Project Room, other small classrooms, and playground and field area of the Jack London School. 
 
Mission Statement 
 
Piner-Olivet Charter School provides middle school students with an academically challenging education in a small, safe, and caring 
environment where students gain confidence, self-esteem, and an understanding of their place in the world. 
 
Our overall goal is to well prepare our students academically for high school so that students can be successful at their highest level 
possible for them personally. Additionally, we emphasize a strong social-emotional learning component for our students, with the 
goal of helping them grow into caring, empathetic individuals.