Washington Union serves grades 9-12 and is a comprehensive high school, located in the town of Easton. The school was established 
in 1982, making it one of the oldest high schools in Fresno County. The school educates over 1,000 students and prides itself on its 
excellence, evidenced by continuing academic achievement. School culture is enhanced through positive relationships between staff, 
students and parents. Washington Union High School is proud to offer countless programs and extracurricular activities that also 
promote student success. 
 
Mission Statement 
All Washington Union High School graduates will be: 
 
Academically Successful- demonstrate a connection between education and life by using comprehensive skills in reading, writing, oral 
communication, mathematics, and critical thinking to solve personal, community and global problems. By doing so, each student will 
leave WUHS with skills needed to succeed in college and/or career. 
 
Responsible citizens- demonstrate positive character traits that contribute to the quality of the school and community and 
understanding of world viewpoints, the interpersonal skills necessary to work collaboratively in a diverse setting. 
 
Part of an Engaged Campus- Students, staff and parents work together to promote membership in the classroom, athletics, campus 
clubs, extra curricular activities, and leadership teams to strengthen the campus culture. 
 
Vision Statement- Great futures begin at Washington Union High School, a place where all students are educated and empowered for 
success.