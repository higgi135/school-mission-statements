School Mission: 
Carl Hankey K-8 International Baccalaureate School develops active, compassionate, internationally-minded, life-long learners. 
 
There are many things that make Carl Hankey K-8 school so special. Hankey is known for its warm, neighborhood atmosphere, its 
deeply rooted sense of community and its configuration as a K-8 school, which provides parents with an alternative to the traditional 
elementary K-5 to middle school 6-8 matriculation pathway. There is another thing, however, which sets our school apart, 
distinguishing it as a unique educational setting. We are proud to be the ONLY public International Baccalaureate (IB) World School 
in Orange County offering both the Primary and Middle Year IB programs, fusing together the Common Core State Standards, CUSD’s 
District Initiatives, and IB World’s rigorous program mandates, woven into a rich instructional tapestry. 
 
What is different about an IB school? At Hankey we strive to develop inquiring, knowledgeable and caring youth who help create a 
better and more peaceful world through intercultural understanding and respect. Our curriculum embraces an inquiry-based 
instructional program promoting critical thinking and problem solving. Traditional subjects such as math, science, history, languages, 
literature, and the arts are integrated and intertwined, rather than compartmentalized neatly into prescribed time slots. For example, 
rather than learning about math, then closing the text book and completing an art project, IB students learn mathematic principles 
through creative art assignments. Or, they might solve a social problem using the scientific method. Wherever possible, Hankey’s 
teachers strive to make connections that deepen student understanding and push students to ask “Why?” and “How?” 
 
Students in our The Middle Years Program (MYP- 6-8th grade) embark on a yearlong academic journey framed by six global context 
themes: Identities and relationships, Orientation in space and time, Personal and cultural expression, Scientific and technical 
innovation, Globalization and sustainability, Fairness and development. Each theme is integrated into the MYP curriculum through 
unit developments at each grade level and in every subject taught. 
 
The MYP curriculum includes 8 subjects; Language and Literature (English/Lit), Language Acquisition (Spanish), Individuals and 
Societies (Social Science), Science, Math, PE, Arts (visual and performing), and Design. Since we have a six period day, our Design class 
is pushed into our Arts, Science and CTE Lab classes, and Fine Arts is woven into Individuals and Societies. PE and Language Acquisition 
are on an A/B schedule that flips each week. 
 
Hankey’s staff maintains a collective belief that all students must learn essential English Language Arts and Mathematics Standards to 
excel. We use a variety of instructional strategies to intertwine the CCSS in ELA and math, with higher-level thinking and problem-
solving skills to ensure that all learners can attain the rigorous thinking demanded by our interdisciplinary IB curriculum. Students 
who struggle are supported with intervention, while students who are ready to accelerate and move beyond are provided with 
opportunities to extend their thinking. 
 
Carl Hankey provides a disciplined, safe and nurturing student environment through its positive behavior system featuring the IB 
Learner Profiles, which celebrate the attributes of a successful scholar and form the foundation of our Essential Agreements, agreed 
upon norms for student behavior. Each month teachers in all grades select one student from their class who has demonstrated 
exemplary application of one of these attributes, and these students are honored at the Student of the Month celebration. Students 
who observe our Essential Agreements earn Hawk Pride tickets for weekly raffles, ASB, Vex Robotics, Water Bottle Wednesday, our 
school recycling program, Yearbook Club, Afterschool Drama Hawks, assemblies and field trips are other programs that further support 
the development of the whole child and offer an impressively extensive educational program. 
 
 

2017-18 School Accountability Report Card for Carl Hankey Middle School 

Page 2 of 13 

 

Hankey K-8 is a neighborhood school with a huge heart and tremendous spirit. We are proud of our accomplishments; implementing 
the IB program, being named a California Department of Education 2015 Gold Ribbon School and a California Business for Education 
Excellence (CBEE) Star School. Here at Hankey we subscribe to the belief that we truly are a small school, making a big difference. 
 
For additional information about our school, please visit www.chhawks.schoolloop.com. Follow Us On: 
Twitter|Instagram|Facebook