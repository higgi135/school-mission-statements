Indian Springs High School opened in 2012 and is the newest of nine high schools in the San Bernardino City Unified School District. 
The school serves students in the southern portion of the city of San Bernardino and the western portion of the city of Highland. It 
has 102 classrooms, a library, a gymnasium, a multipurpose room, and an administration office, providing sufficient space for 
instruction. The school has a focus on Science, Technology, Engineering, and Mathematics (STEM) and provides students with rigorous 
and relevant coursework that will prepare them to graduate and be ready to enter the career or college of their choice. 
 
Indian Springs is an urban school serving predominantly Hispanic and African-American students with relatively low means of 
income. The mobility rate of students in this area is relatively high due to many factors such as low employment rates and housing 
that consists mostly of rentals and apartments. 
 
Vision 
We believe all students are relevant and have the ability to learn, grow, and be productive. 
 
Slogan 
"I am. We are. Indian Springs Coyotes" 
 
Mission 
We will build partnerships and create real world experiences for all Indian Springs High School students, to be invaluably prepared to 
charge into today's world after graduation. We prepare our students to be resourceful competitors who can achieve their goals and 
have a positive impact in the local and global community.