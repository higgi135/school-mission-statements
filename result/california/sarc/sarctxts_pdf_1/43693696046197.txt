Lee Mathson Middle School is a school that has high expectations for every student.Since school year 2014 - 2015 Mathson Institute 
of Technology is committed to providing a comprehensive educational experience for our students to foster the development of critical 
thinkers, effective communicators, collaborative creators, and committed life-long learners. Students will develop the ability to use 
technology to analyze, learn, and explore; promoting a college career path in STEM. We emphasize academic rigor and promote 
positive social and emotional development. We celebrate our community and work to even better our service to our students and 
their families. We are on a course of continuous improvement, and every indicator points to success! We strive for excellence, and we 
know that together we succeed. We celebrate community and regularly partner with our beautiful Mayfair Community Center (located 
right across the street) and with MACSA Youth center. All 6th graders attend an extended learning program until 6:00 p.m. daily to 
assure student success in transitioning to Middle School. Our college- going culture is prevalent in many student events including 
College Week and our annual Career Fair. We celebrate over a dozen athletics programs and various student clubs. We welcome all 
members of the community to get involved in helping our school achieve its goals! Tours are available.