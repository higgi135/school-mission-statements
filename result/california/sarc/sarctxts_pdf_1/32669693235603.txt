Our mission at Portola Jr./Sr. High School is to provide our students with a safe learning environment. We recognized the need to help 
students prepare for their future by instilling the skills necessary for success in a changing world. We encourage our students to frame 
the present in a meaningful context and to anticipate the future with attainable goals. We show our sensitivity to our students’ wide 
variety of interests and educational objectives by including academic, vocational, and extracurricular offerings. We modify our 
curriculum to meet all of our students’ diverse needs. We develop in our students an appreciation for the democratic process, and 
we challenged them to accept the responsibilities of good citizenship and to contribute to the overall betterment of our school and 
society. 
Our students are expected to engage in the 5 C's - Communication, Collaboration, Critical Thinking, Creativity and Compassion.