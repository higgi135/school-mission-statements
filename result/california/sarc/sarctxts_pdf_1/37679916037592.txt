Chase Avenue Elementary School promotes a partnership involving students, parents, and teachers to achieve student academic 
success and develop social responsibility. Students will be challenged to work to their potential through a well-balanced, integrated 
curriculum of basic skills and enrichment activities. Emphasis will be placed on maintaining high academic achievement and fostering 
self-esteem through embracing the seven essential virtues of Respect, Conscience, Empathy, Fairness, Kindness, Self-Control and 
Tolerance.