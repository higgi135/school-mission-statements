The vision of Portola Springs Elementary is to inspire students, parents, and staff to communicate and collaborate, creating a vibrant 
learning community that is safe, inclusive, innovative, and encourages a growth mindset. Our focus is to instill a joy for learning, and 
empower our students to become creative problem solvers and responsible caring members of our local and global communities. Our 
mission statement reads: Portola Springs Elementary is a family of respectful and responsible learners. Together with our community 
we empower our students to be compassionate, empathetic citizens who make a positive difference in our world. Our Positive 
Behavior Intervention and Supports (PBIS) philosophy can be found throughout the school in calling our students to be GREAT-- 
Grizzlies are... Respectful, Empathetic, Always positive, and Truly responsible. 
 
Our ELA goal is to develop consistency with ELA differentiation and small group instruction strategies including RTI and Daily 5 models. 
 
This year, our mathematics focus is to use math target trackers to immediately drive instruction. 
 
Our staff also continues to focus on Multi-Tier System of Supports (MTSS) and supporting students across disciplines, with behavior, 
and with socio-emotional development. We do this through a comprehensive MTSS process along with our focus on Professional 
Learning Community (PLC) teams. These PLC teams meet weekly to review data and formulate and analyze assessments and practices. 
We ask ourselves as professionals, what do we want our students to learn? How do we know they’ve learned it? How do we respond 
when they’re having difficulty? How do we respond when they already know it? This year, our goal for MTSS and PLC teams to use 
data driven decision making to place students in interventions in the area of socio-emotional, behavior, and academic interventions. 
Additionally, we remain committed to maintaining the culture and community feel as our school grows. 
 
Additionally, we are focusing on an integration of technology and hands on learning to promote student engagement.