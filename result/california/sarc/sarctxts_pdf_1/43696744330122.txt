Santa Clara High School, originally established in the downtown area of Santa Clara in 1872, moved to its current location on the 
present 32-acre campus in 1981 as a senior high school serving grades 10-12. In 1987, the ninth grade was added, and now the school 
serves students from Santa Clara, Sunnyvale, and San Jose as a traditional 9-12 institution. 
 
Duplexes and apartments surround the school that is located south of El Camino Real on Benton Street. The service area includes retail 
commercial establishments, high technology, industry, and residential property ranging from the least expensive property to the most 
expensive property in the city. The school is located two blocks away from the City of Santa Clara's Central Park, which houses the 
Community Recreation Center and the International Swim Center. 
 
The community makes extensive use of the schools facilities, including the Performing Arts Center, which opened in February 2005. A 
new Fab Lab for STEM (science, technology, engineering, and math) education will be available for student and community use 
beginning in the 2016-2017 school year. 
 
Santa Clara High School's teachers and staff believe all students are capable of succeeding. The school is committed to meeting the 
needs of students in grades 9-12 by providing an effective instructional program that improves students academic, social, physical, 
and psychological achievements, as well as one that meets the expectations of the district and state guidelines. 
 
The mission of Santa Clara High School is to foster inquiry and self-reliance in our diverse student body while imparting an excellent, 
well-rounded education in a safe, inclusive, and collaborative environment. We at Santa Clara High School strive to graduate 
productive, intellectually curious students who use their education, talents, and skills for the betterment of themselves, their 
community, and the world. 
 
Our goal is to develop students who are: 

• 
• 
• 
• 
• 
• 

effective communicators 
contributing citizens 
informed, productive thinkers 
selfdirected learners 
collaborative workers 
information processors