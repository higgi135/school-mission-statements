Paloma Elementary School opened its doors in 1995. Our current school enrollment is 607 students. 
 
 In 1998, our school was named a California Distinguished School. In 2017, our school was awarded the Silver Ribbon as a Green Ribbon 
School. 
 
OUR VISION: 
 
 Paloma Elementary School will ensure that all children meet challenging academic standards through powerful learning experiences 
in a positive, motivating environment. 
 
OUR SCHOOL MISSION: 
 
• All students will learn a relevant and rigorous core curriculum which emphasizes interdependence among subject areas. 
 
 • The staff will provide a learning environment which supports the social, intellectual, physical, and emotional growth of every child. 
 
 • Staff excellence will be supported through meaningful staff development, increased opportunities for decision making, and 
collaboration. 
 
 • The school will use a system of authentic, performance-based assessments and use existing norm-referenced data to ensure 
accountability. 
 
 • The school community will foster students’ ability to make wise decisions, practice ethical values and appreciate cultural differences. 
 
 • The school will continue to develop and maintain a comprehensive program to increase partnerships with parents, businesses, and 
the community.