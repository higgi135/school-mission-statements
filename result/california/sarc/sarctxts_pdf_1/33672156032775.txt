Victoria was built in 1956 when the surrounding area was rural orange groves. Modernization of the entire campus was completed in 
1996. In 2013, front perimeter fencing was installed to ensure that all visitors check into the office and receive a pass. The gates remain 
locked until dismissal time. The surrounding neighborhood is relatively free of crime and the school has experienced little vandalism 
and graffiti over the past few years. 
 
Many special programs are available for students: 
 
AVID (Advancement Via Individual Determination) for grades 3-6 
 
Early Literacy Skills by grade 3 
 
Student Engagement 
 
HEARTS -Afterschool program providing academic hour and enrichment activities 
 
STEAM Enrichment Days - A day once a month when the whole school participates in activities with Science, Technology, Engineering, 
Art, or Math themes 
 
Read 180 - Individualized reading assistance 
 
Parent/Family Nights 
 
AOK (Acts of Kindness) - Students bring coins to help the disadvantaged students' families in our school during the holiday season. 
 
LOL (Lunch on the Lawn) - Once a month parents come to the school and have lunch with their student 
 
ESL (English as a Second Language) class for parents 
 
Lunchtime soccer supervision for grades 1-6 
 
Parent Workshops 
 
100 Mile Club - All students run/walk each morning with the goal of reaching 100 miles within the school year 
 
Our Mission Statement: 
 
"Victoria's educators, parents, and community members are committed to providing the foundation necessary to prepare students to 
achieve academic success." 
 
Victoria School's Staff BELIEVES! 
 

2017-18 School Accountability Report Card for Victoria Elementary 

Page 2 of 15

We BELIEVE every child has value. All students have special talents and abilities. We can tap into their strengths. Therefore, we will 
identify student strengths and provide opportunities for all students to explore their special talents and abilities. We will use these 
strengths to motivate students. We BELIEVE every student can rise to teachers' expectations. Setting high expectations provides 
students opportunities for growth and achievement. Therefore, we will plan rigorous instruction, scaffolding prior knowledge. We 
will set daily goals (objectives) and show them how to reach them. We BELIEVE children need to have self esteem. Therefore, we will 
let them know we value them by actively seeking the positives and acknowledging them. We will positively reinforce desired behavior 
and academic efforts. We BELIEVE we are a collaborative team. Our learning community is responsible for everyone. Therefore, we 
will collaboratively analyze data and plan for improved instruction based on current reality. We BELIEVE we create the culture of the 
school. Attitude is everything. Therefore, we model a "we can and will succeed" attitude. We will minimize the negative and accentuate 
the positive. We BELIEVE all children deserve a safe, caring, positive, enriching learning environment. Everyone should be treated with 
respect. Therefore, we will consistently implement the school discipline plan. We will review school and classroom rules on a regular 
basis throughout the year.