VISION STATEMENT: 
We believe that the most promising strategy for achieving the mission of our school is to develop our capacity to function as a 
Professional Learning Community. 
 
We envision a school in which all staff: 

 

• Unite to achieve a common purpose and goals 

• Work together in collaborative teams and become interdependent. 

• Seek and implement promising strategies for improving student learning on a continuous basis. 

• Monitor each student’s progress on a frequent basis utilizing Formative assessments and Common Formative assessments. 

• Demonstrate a personal commitment and genuine concern for the academic success and general wellbeing of each student. 

• Employ the talents of people who know there is always a solution to every challenge. 

MISSION STATEMENT: 
We at Emerson Middle School are committed to each student, and to the building of a community of scholars prepared for the 
challenges of high school and beyond.