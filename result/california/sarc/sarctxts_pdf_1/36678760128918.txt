Paakuma' K-8 School opened in fall of 2015. The school has 39 classrooms, a gymnasium, a multipurpose room, computer lab, and an 
administration office. The facility strongly supports teaching and learning through its ample classroom and playground space, and a 
staff resource room. The school also has professional office space for support staff (i.e., speech therapist, psychologist, etc.). 
 
The mission of Paakuma' K-8, home of the Huunam, is to ensure that all students are culturally proficient and understand the rich 
history that has influenced our community. Within this, we will provide opportunities to acquire college and career readiness skills 
through high academic, behavior and safety expectations. Through the journey, we will encourage student involvement and 
community voice as we celebrate excellence.