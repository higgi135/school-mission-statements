Petaluma Junior High School (PJHS) is one of seven secondary schools in the Petaluma City School District and one of two 
comprehensive junior high schools that serves students in grades 7 and 8. Petaluma Junior High School serves 14 small elementary 
schools representing 10 different school districts. Our school prepares students for entry to Petaluma High School, which is located 
one half mile away. Our school has a School Site Council (SSC), an active Parent Teacher Student Association (PTSA), and English Learner 
Advisory Committee (ELAC). 
 
All students take a core program of six academic classes including physical education and one period of elective. Based on assessments, 
students may enroll in accelerated/advanced classes in English, Math in seventh grade, and continue on to similarly advanced levels 
in eighth grade, including Integrated Math 1. Our students must achieve a minimum of 110 credits (of a potential 120) over the course 
of two years to be eligible for participation in the promotion exercise at the end of their eighth grade year.