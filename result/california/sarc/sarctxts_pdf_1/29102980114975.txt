Sierra Montessori Academy (SMA) is a K-8 public charter school serving approximately 125 students in the south Nevada County area. 
SMA is sponsored under the Nevada County Office of Education. 
 
The mission of SMA is to educate K-8 students of the Sierra Foothills through a self-motivating, individualized and child focused, 
comprehensive instructional program. Research has shown that a Montessori education leads to better social and academic skills. We 
strive to help each child develop the habits, skills, attitudes, and confidence which are essential to a lifetime of creative learning. We 
blend this Montessori philosophy with instructional programs based on the California Common Core State Standards. These standards 
are designed to be relevant in our constantly changing world. Student proficiency reflects the knowledge and the skills students need 
for success in both higher education and their ultimate careers. 
 
Our objective is to provide a sensory-rich, hands-on, student-centered environment where all students develop respect and tolerance 
for themselves and others; become involved, responsible citizens, and realize their full potential.