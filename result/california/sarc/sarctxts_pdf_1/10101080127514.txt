OUR MISSION STATEMENT 
We inspire joy and purpose in learning to develop well-educated, ethical, caring, effective, and innovative members of the community. 
 
VISION 
We envision integrated, thriving communities where youth and adults work together to create new solutions to shared challenges. 
 
GOALS 
1. Kepler students demonstrate personal growth and mastery of academic standards in all content areas. 
2. Kepler teachers and staff foster student strengths and differentiate to support academic, creative, social, and emotional needs of 
every student. 
3. Kepler Neighborhood School creates and supports a safe and fostering school environment for every student. 
4. Kepler parents, staff, and community are valued, contributing members to Kepler School programs and culture.