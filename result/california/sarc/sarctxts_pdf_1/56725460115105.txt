The Camarillo Academy of Progressive Education (CAPE) is a K-12 charter school that expands upon the “Open School” philosophy that 
thrived in Camarillo for over 30 years, first at Bedford Open School and then at Los Senderos Open School. During the 2017-2018 
school year CAPE expanded to include an online high school available to students in grades 9-12. The program is done virtual through 
Pearson's Connexus Online Learning. 
 
Focusing on the California Common Core Standards, this progressive education operates on the premise that children learn best when: 
1) They believe in themselves and their abilities 
2) They are interested in learning 
3) They have opportunities to participate in the decision-making process 
4) They are empowered to pursue an assignment to its completion 
 
CAPE opened its doors in September 2007 after the Oxnard Union High School District (OUHSD) Board of Trustees voted unanimously 
in June 2007 to approve CAPE’s charter application. The Academy serves approximately 580 students for the 2017-18 school year. 
Currently, CAPE leases facilities from the Pleasant Valley School District (PVSD), which is the elementary district of residence of most 
of the school’s students. 
 
CAPE serves a region in Ventura County known as the Oxnard Plain, which is one of the world's most important agricultural centers, 
with distinctions as one of the nation’s leading producers of strawberries and lima beans. The area is home to nearly 300,000 residents, 
two U.S. Navy bases, the Port of Hueneme (which is the busiest commercial port between Los Angeles and San Francisco) and California 
State University at Channel Islands. CAPE serves a diverse community: agricultural and industrial, rural and suburban. 
 
The CAPE Philosophy is based upon a set of beliefs about children and how they learn. Children have a natural curiosity, a desire to 
please, and are motivated to learn. Further, these assets may be capitalized upon by careful planning to make education both 
interesting and successful. Thus, CAPE produces students with a life-long interest in learning, with solid experience in self-direction 
and goal selection, and with the confidence to succeed. The students learn the value of community membership and are equipped 
with the requisite skills: intellectual, physical, social, and emotional. 
 
CAPE’s School Wide Learning Outcomes 
Every student will achieve academic success. 
Every student will be a conscientious and effective communicator. 
Every student will positively contribute to CAPE and beyond! 
 
THE CAPE SUPER 7 
We take pride in everything we do: our work, our appearance, our school, and our community. 
We are honest, kind and find ways to resolve problems peacefully. 
We think before we act and speak. 
We come to school on time, prepared, and ready to participate in our learning with a positive attitude. 
We try our hardest and never give up. 
We believe in and support each other. 
We respect the rights, property, opinions and diversity within our CAPE community. 
 
 

 

2017-18 School Accountability Report Card for Camarillo Academy of Progressive Education 

Page 2 of 13 

 

Major Achievements – Most Recent Year 
In the eleven years since the Charter School’s inception, Camarillo Academy of Progressive Education has evolved, expanded and 
celebrated great achievements. CAPE supports a comprehensive instructional plan by providing safe nurturing and intellectually 
engaging surroundings, where students are inspired to intrinsically value learning as they achieve social and academic success. Among 
the numerous successes experienced are the following achievements that have contributed to the growth and development of CAPE 
between 2007-2018. 
 
2013 WASC Accreditation 
2016 California Golden Ribbon School 
2017 California Honor Roll School 
2017 California Honor Roll Charter School 
2017 California Honor Roll School with Specialization in STEM 
2012 California Distinguished School Award 
California Business for Education Excellence Honor Roll 
Named one of the Top Ten Charter Schools of California (2013 Study by USC) 
98% of staff has been retained over the past 5 years 
Participation in Camarillo Academic Olympics 
Super Quiz Team placed 1st in citywide competition multiple years 
Annual participation in Ventura County Spelling Bee 
Robotics Team actively involved in First Lego League (“FLL”) and all local competitions 
Big and Little Buddy Program 
Beginning and Advanced Band Program 
4th Grade mandatory Build-a-Band program 
Guitar 
Strings 
Spanish 
Development of Musical Theater 
CAPE Cares Service Projects 
High School (grades 9-12) expansion implemented in fall of 2017 
Artist and Composer of the Month 
Junior Optimist Club 
CAPE Leadership Team 
Partnership with Oxnard Union High School District to provide high school readiness for our 8th graders 
After school athletics program (flag football, volleyball, basketball, soccer) 
Student teacher placement site for CSUCI and Cal Lutheran University 
Annual Participation in the Ventura County Geo Bee 
Annual Participation of 8th graders in the Ventura County Geo Bowl 
Annual Participation in the 3rd grade PVEF Speech Tournament 
 
These achievements supplement the progress made, which have been successful in providing a rigorous, project-based educational 
program for all students. CAPE strives to provide an education that encourages students to become positive, responsible, contributing 
citizens who value themselves and others in their choices as they strive to become life-long learners. 
 
Focus for Improvement – Most Recent Year 
Please see the school's yearly LCAP which is available on the school's website www.camarilllocharter.org for the specific action plan 
for improvement. 
 
Homework – Most Recent Year 
Homework has an important place in the educational program at CAPE Charter School. Teachers carefully consider the child's needs 
when planning homework to make home assignments an integral part of your child's educational experience. Homework that is 
meaningful in content will be assigned on a regular basis, Monday through Thursday in grades K-5 (special circumstances may warrant 
homework to be completed over the weekend.), and Monday through Friday for grades 6-8. The frequency and length of homework 
assignments will be established by grade level teachers. Parents will be informed of the classroom homework policy in writing at Back-
to-School night. 
 
School Schedule – Most Recent Year 
2018-2019 Bell Schedule 
 

2017-18 School Accountability Report Card for Camarillo Academy of Progressive Education 

Page 3 of 13 

 

Bell Schedule for Grades 1-8 
8:00 AM Front Gate Opens (there is no supervision of students before this bell rings) 
8:05 AM 1st Bell for Middle School *all 6-8 grade students to their seats 
8:10 AM 1st Bell (students go to classrooms) 
8:10 AM Middle School Tardy Bell - Classes Begin 
8:15 AM Tardy Bell (students must be in their seats) 
9:50 AM 10:10 AM 1st grade and 2nd grade AM recess 
10:25 AM 10:45 AM 3rd grade, 4th grade and 5th grade AM recess 
11:01 AM 11:41 AM 6th grade, 7th grade and 8th grade lunch 
11:45 AM 12:25 PM Full Day K, 1st and 2nd grade lunch 
12:30 PM 1:10 PM 3rd grade, 4th grade and 5th grade lunch 
1:15 PM 1:30 PM 1st grade and 2nd grade PM recess 
2:30 Dismissal 
 
Bell Schedule for All Day Kindergarten 
8:00 AM Front Gate Opens (there is no supervision of students before this bell rings) Students go directly to the classroom 
8:15 AM Tardy Bell (students must be in their seats) 
10:00 AM 10:20 AM AM recess 
11:45 AM 12:25 PM Lunch 
2:00 Dismissal 
 
Bell Schedule for AM Kindergarten 
8:00 AM Front Gate Opens (there is no supervision of students before this bell rings) Students go directly to the classroom 
8:15 AM Tardy Bell (students must be in their seats) 
9:50 AM 10:10 AM AM recess 
11:35 Dismissal 
 
Bell Schedule for PM Kindergarten 
11:05 AM Front Gate Opens (there is no supervision of students before this bell rings) Students go directly to the classroom 
11:10 AM Tardy Bell (students must be in their seats) 
12:45 PM 1:05 PM PM recess 
2:30 PM Dismissal