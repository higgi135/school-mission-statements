Vision Statement: 
 
Ball Junior High School is an inclusive community of students, educators, and families who are committed and involved. Together we 
forge a path toward college and career readiness through meaningful learning experiences. Our goal is to promote creativity, 
collaboration, critical thinking, character, and communication skills that prepare our students for lifelong success in a global society. 
Mission Statement: 
 
Ball Junior High School is committed to providing all students with: 
· Engaging, enriching, and rigorous learning experiences that emphasize 21st Century skills. 
· A safe, caring, and positive academic atmosphere founded on mutual respect for all stakeholders. 
· A staff that is committed to working with students and family members to help build the necessary social and academic skills to 
ensure students are college and career ready. 
 
Highlights: 
 
Ball Junior High School offers the following courses, which ultimately support students in completing University of California A-G 
requirements: Math 1, Math 2; Spanish 1 and Spanish 1 for Spanish Speakers. Ball Junior High School also has an award-winning Visual 
and Performing Arts program (Art 1, Art 2, Drama, Band [beginning, intermediate, advanced, and jazz], and choir [beginning, 
intermediate, and advanced]). We also have a thriving AVID, MESA and intramural sports program. Ball Junior High School offers 
inclusion classes (co-taught) in English, math, science, ELD 1 and ELD 2, and history for our English Language Learners. We also offer 
AVID Excel 7/8 as an additional academic support for English Learners. 
 
Ball Junior High School staff have made a conscious effort to focus on the development of the 5Cs (Creativity, Critical Thinking, 
Communication, Collaboration, and Citizenship) in the classroom, and increase student engagement through the use of the District’s 
First Best Instruction (FBI) placemat when planning lessons. Our teachers have increased collaboration time in their own departments 
and with other departments in all subject areas. One-hundred percent of our teachers have been trained in Capturing Kids Hearts, and 
as a result of that training, staff have committed to knowing the name, face, and story of each student, greeting students at the door 
daily, developing a social contract with all classes, using positive affirmations, and focusing on emphasizing kindness and compassion 
to create safe spaces for students to learn. This year, we have successfully implemented an advisory period for all students and 
teachers. This has provided additional time to support students’ academic and social-emotional needs. Additionally, it provides 
multiple opportunities for staff to build deeper connections and to have students connect more with one another. 
 
Ball Junior High School offers daily tutoring before and after school to help students who are struggling academically. This is in addition 
to the after-school hours that individual teachers are available to students who need assistance. There is also help that is provided 
weekly during students’ advisory period. We have also partnered with Imagine Science to offer robotics classes and STEM classes after 
school. Additionally, Ball Junior High School offers Saturday Academy enrichment courses on a monthly basis. 
 
 
Demographic Information: 
Ball Junior High School, located in Anaheim, California, serves 1,037 students, in which 89.5% participate in the free and reduced meal 
program, 27% are designated as English Learners, and 12% are Students With Disabilities. The demographic profile also indicates the 
following regarding student subgroups: 81% Hispanic; 7% White; 6% Asian; 2.59% Filipino, 2% African American, 1.5% Pacific Islander, 
and .5% Mixed Race/Multiracial. 
 
 
 

2017-18 School Accountability Report Card for Ball Junior High School 

Page 2 of 10