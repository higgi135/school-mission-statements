The mission of Culver City Middle School, a culturally diverse and environmentally active learning community, is to ensure that all 
students are provided a challenging educational program that will foster integrity, respect, and a passion for success in our ever-
changing global environment by bringing the resources available in our unique city together to achieve this goal. 
 
WE BELIEVE: 

Family and community partnerships are integral to students' learning. 
School should meet the needs of every student to promote active learning. 
Students and staff will learn from each other and respect one another’s contributions and diversity. 
The school’s facilities must be safe, aesthetically pleasing, and conducive to learning. 

• All students can learn. 
• 
• 
• 
• 
• Character and academic expectations should be clearly defined. 
• 

Environment encourages open communication and dialogue among staff, students, and families.