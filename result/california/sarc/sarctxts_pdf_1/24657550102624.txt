Lorena Falasco Elementary School is a traditional calendar school, which serves Kindergarten through Sixth Grade students. Located 
in Los Banos Unified School District which is situated on the west side of the San Joaquin Valley. LFE opened July 8, 2004 as a K-5 multi-
track year round school with 700 students. In 2007-2008 we added 6th grade and in 2010-2011 LFE converted to a traditional calendar. 
Currently, student enrollment is just under 900 students. 
 
We are privileged because our students come from a wide variety of ethnic and socio-economic backgrounds. The staff has high 
expectations for student achievement and believes that all students are learners and achievers. Lorena Falasco Elementary has a staff 
that is professionally skilled and personally committed to meeting the learning needs of all students. Currently, all students in grades 
1st through 6th have one to one iPADs in the classroom. Kindergarten has iPADs for math and literacy centers. 
 
The Vision Statement for Lorena Falasco Elementary School is: In partnership with our community, Lorena Falasco Elementary School 
is committed to creating a safe and positive environment with high expectations that encourage success and lifelong learning for 
everyone. 
 
Our school mission is to educate all students according to their diverse needs as learners. We believe that all students can and will 
learn because of what we do. We believe that student performance and outstanding accomplishments should be recognized and 
rewarded and our students are motivated to perform well. Our school community supports the use of technology as a tool for teaching 
and learning to ensure our students are prepared to be college and career ready for the 21 Century. 
 
You may request additional information regarding the School Accountability Report Card by calling the school office, 209-827-5834. 
 
Thank you, 
Jane Brittell, Principal