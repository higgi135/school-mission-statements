Our district mission statement reads, opportunities for learning are limitless. The Orcutt Union School District's mission is to nurture, 
educate, empower, and inspire our children to successfully navigate and thrive in an ever changing world. At Pine Grove, our mission 
is to ensure the educational success of all students by maintaining high expectations, a safe learning environment, a commitment to 
excellence, and comprehensive programs which empower students to reach their fullest potential as responsible and productive 
citizens in a continuously changing world. 
 
Pine Grove Elementary School is located in the northern region of Santa Maria and serves students in grades kindergarten through six 
following a traditional calendar. At the beginning of the 2018-19 school year, 523 students were enrolled, including 11.5% in special 
education, 7.6% qualifying for English Language Learner support, and 33% qualifying for free or reduced price lunch.