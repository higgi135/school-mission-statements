National City Middle School (NCM) is a community school that educates approximately 882 7th and 8th graders. Named a California 
Distinguished School in 1996 Golden Bell award-winner in 1998 and 2003 and a National Center for Urban School Transformation 
Award winner 2012, and is ranked 7 out of 10 in state ranking and 10 out of 10 in similar school. Currently employs a credentialed 
staff of 32 full-time teachers. 
 
Our staff is committed to implementing Common Core State Standards with high expectations on career and college readiness 
preparation. In addition, we have a strong focus on comprehensive intervention programs before & after school that put academics 
first. Allowing teachers to extend learning opportunities for targeted students. NCM’s Intervention Program services between 150-
200 students on a daily basis. 
 
Language Development classes and SDAIE (sheltered) instruction supports all stages of language acquisition. Our English Learners 
program was recognized as one of 15 exemplary programs in California. Currently, our student population consist of seventy-five 
percent of our students speak a language other than English at home. 
 
Students in the Sweetwater Union High School District are expected to master Common Core State Standards and completing all A-G 
requirements while preparing them to meet the challenges of the 21st century 
 
Vision and Mission Statement 
 
Vision: 
 
At NCM, we change lives and encourage our youth to develop a confident vision for a better tomorrow through education. 
 
Mission: 
 
As a nurturing environment with a historically rich culture, NCM’s mission is to inspire, encourage, engage and empower students to 
self-advocate their development to become constructive, productive citizens in a global community by cultivating a: 
 
Learning experience that is rich and relevant based on a foundation of school-wide literacy, instructional strategies and collaboration 
 
Nurturing, safe environment that acknowledges and praises overcoming adversity and embracing challenges 
 
Growth mindset culture focused on multiple opportunities to achieve mastery through self-reflection and self-monitoring of learning 
 
Transformative school culture based on each participant’s strengths and achievements 
 
Strong partnership among students, staff, parents, community and educational partners creating a foundation for life skills and 
career/college preparation 
 
Commitment to the Kingsmen Code: Perseverance, Respect, Integrity, Drive and Enthusiasm fostering responsible community 
members and personal resiliency 
 
 

2017-18 School Accountability Report Card for National City Middle 

Page 2 of 11