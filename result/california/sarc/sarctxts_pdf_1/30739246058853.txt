The Oak community believes in providing a rigorous, nurturing and active learning environment for all children that ensures personal 
success, a life-long desire to learn, and civic responsibility. The following shared beliefs will guide our actions within Oak's professional 
learning community and standards-based learning environment. 
Shared Values and Beliefs: 
Educational Program 
We believe our first responsibility is to provide a quality educational program that meets the needs of all students. 
Decision Making 
We believe in making decisions based on what is in the best interest of our students. 
High Expectations For All 
We believe in an emphasis on the value of hard work, high expectations, and persistence. 
A Commitment To Character 
We believe that we teach the whole child. We expect academic achievement, personal responsibility, honesty, cultural sensitivity, and 
respect for all people, property, and the environment. 
Teamwork 
We believe the education of the child is a shared responsibility between the school, parents, and the community. 
Teaching And Learning 
We believe in providing an exciting, challenging curriculum responsive to individual interests, needs, and learning styles. We will 
provide extraordinary experiences for everyone! 
Safe And Orderly Environment 
We believe all students are entitled to a safe and secure learning environment. 
Our Mascot: The lion 
 symbolizes our vision for children....strength and pride while directing a keen eye toward a noble purpose.. Our vision statement: 
Educating , empowering and encouraging every student every day! We provide Extraordinary Experiences for Everyone. 
Oak Middle School 
 encourages parents and community members to be actively involved in all aspects of our school program. 
This powerful partnership supports our students and staff through some of the activities listed below: 
 

Lion Pride kick off 

• 
• Back to School Night 
• New Parent Night 
• Monthly PTA meetings 
• Quarterly School Site Council Meetings 
• GATE Parent Advisory Committee (PAC) meetings 
• District English Learner Advisory Committee (DELAC) 
• Veteran's Day Celebration 
• Red Ribbon Week 
• PEACE Week 
• Numerous service projects and activities 
• Volunteering in classrooms, media center, PAW Prints copy center and the office 
• Dances 
• 
• Honorary Service Awards/Founders' Day 
• Pi Day 
• Career Day 
• 
• 

Eighth grade party 
Student recognition program:Lion Pride Lunches, PAW awards 

Lunch on the lawn 

2017-18 School Accountability Report Card for Oak Middle School 

Page 2 of 10 

 

• Musical performances including Band, Jazz Band and Orchestra 
• Musical theater performances from our many school choir groups 
• Drama performances