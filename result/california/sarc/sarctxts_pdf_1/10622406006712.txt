District Vision 
It is the vision for each student to become a life-long learner and productive citizen, leading a fulfilled life. Included in the vision of the 
Charter is the drive to enable students to become educated citizens, who will be literate, have problem solving skills, be self-motivated, 
able to utilize technology, and become socially responsible. 
 
Principal’s Message 
Linking community traditions to our continuously evolving educational goals of providing universal access is the core of Roosevelt 
School’s philosophy. The first Roosevelt School established in 1921 was the third school built in Kingsburg. The Works Project 
Administration under Franklin D. Roosevelt constructed the current building in 1938. We are currently an all First Grade School. 
Roosevelt was initially established as a sixth, seventh, and eighth grade school and was restructured into a fifth and sixth grade school 
in 1992. In 1996, the staff of the Kingsburg Elementary School District, in conjunction with the citizens of the community, adopted a 
charter school compact. This allowed Roosevelt School to become a charter school within the Kingsburg Elementary School District, 
the second in the State. Through this living document, the charter, Roosevelt, has been able to adopt creative methods of educating, 
using researched-based best practices, while maintaining the traditional high standards expected by the Kingsburg Community. 
 
Roosevelt is a family of 219 students and 30 staff members with a commitment to excellence. With the unique grade configuration 
of the schools in our District, there is a common thread that unites the students as they transition into and out of the schools 
throughout the years. Together, they develop strong friendships where tolerance is accepted and diversity is welcomed. The staff 
takes pride in knowing generations of families. Roosevelt has developed a strong partnership with the community it serves, where 
high expectations for academic standards, citizenship, community service, recreation, and traditional values are embraced and 
enveloped in the framework of a caring community. The parent community who works alongside our students on a regular basis are 
evidence of this fact. 
 
Roosevelt’s community continues to evolve and become more diverse each year. Of our 219 students, we have 107 female and 112 
male students. As of this year, indicators have displayed 59% of our students are Hispanic, 31% are White (Non-Hispanic), 3% Asian, 
4% are multiracial, 1% Black, .5% American Indian . 
 
The Roosevelt staff is a highly-qualified and enthusiastic group of professionals, who are dedicated to bringing out the best qualities 
in our students, ourselves, and in the work that we do together. We are truly an organization that respects learning, honors teaching, 
and teaches for understanding. The staff collaborates in many ways, with a focus on the academic success of each student embedded 
into every aspect of planning, organization, and use of resources—materials, fiscal and personnel. The maintenance staff is an 
important part of the learning community, taking pride in making this historical building a place where students and staff are honored 
to attend. In addition to maintaining beautiful grounds and facilities, it is not uncommon to find this staff assisting students, decorating 
Christmas trees, and enjoying each other’s company. Roosevelt creates a stimulating, and aesthetically pleasing environment that is 
enjoyed by the community. Roosevelt is proud to be a member of the Kingsburg Elementary Community Charter District.