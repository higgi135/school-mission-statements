The Stanislaus County Office of Education alternative program is comprised of both court and community school programs. This report 
will only address the court school portion of the program. The County Office of Education operates the educational program at 
Stanislaus County West Campus. Students complete grade appropriate, standards-based academic courses and work on basic skills to 
complete requirements or earn high school credits. There are opportunities to complete GED requirements as well as high school 
diploma requirements while detained. Students at the West Campus who have been identified as needing special education services 
are provided supplementary services through credentialed special education teachers and para professional support.