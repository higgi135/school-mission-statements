School Profiles 
 
Gravenstein Union School District is a Transitional Kindergarten through 8th-grade school district with an enrollment of approximately 
750 students, most of who come from English speaking homes with a variety of socio-economic backgrounds. The district is located in 
the western rural section of Sebastopol in Western Sonoma County. The District serves a primarily rural population in an area of limited 
growth. Approximately ninety-four percent of the property located within the school district boundaries is zoned “rural residential” in 
the Sonoma County General Plan. This zoning classification also affects the number of students living within the district. 
 
There are four schools in the district. Gravenstein School is a TK-5 charter school with the exception of some first-grade classes (called 
Gravenstein First School). Hillcrest Middle School is a 6th - 8th-grade middle school which is a charter school, and the district also has 
a Community Day School. The District has a K-8 Magnet/GATE Program at both campuses (ENRICH!). 
 
The sites reflect the rural nature of the community. The relatively low teacher: student ratio at each site, excellent staff, and very 
involved parents have worked together to establish a student body that has achieved some of the highest standardized test scores in 
Sonoma County for the last 15 years. 
 
All classes TK-6 are self-contained. Primary classes (TK-3) have an approximate ratio of 20-1 and middle grades average an approximate 
ratio of 25-1. Instructional and temporary support assistants, paid by categorical monies, are utilized in Grades TK-8 according to need 
in the classroom. Instructional assistants work in conjunction with the Special Education Teachers to instruct in Learning Labs at both 
campuses. The Learning Labs are operated through combining categorical funding to serve students who need intervention or tutoring. 
Students are provided service according to their need – regardless of whether they qualify for special education or not. This setting 
provides additional services in the areas of Reading/Language Arts and Math to identified and/or below-grade-level students. Other 
services available to Gravenstein Union School District students (on both campuses) include the Speech and Language Program, an 
academic counselor, and a school psychologist. The same services are available at the middle school campus. 
 
Students in grades TK through 5 receive music instruction one day per week and students in grades 4 and 5 may opt for additional 
band time by the district’s music specialist. Visual art is also part of the Gravenstein School program. Classroom teachers and an art 
specialist provide this program. Hillcrest Middle School has a variety of scheduled music, art and drama programs. Our Grade K-8 
Magnet/GATE Program does include additional field trips and visual and enrichment classes. 
 
The school district utilizes a social-emotional learning program (SEL) entitled Second Step. Empathy, Emotion Management, Impulse 
Control, Problem Solving, and Anger Management are some of the topics covered. 
 
Thank you for your interest in our schools. 
 
Jennifer Schwinn, Superintendent 
 
School Description: 
There are four schools within Gravenstein Union School District. They are Gravenstein Elementary, Gravenstein First, Hillcrest Middle 
School, and Gravenstein Community Day School. 
 
District Vision: 
We are convinced that all students can learn and that our mission is to ensure that each child masters the knowledge and develops 
the attitudes and skills necessary to become a contributing and productive citizen. We believe the education of our children is a 
cooperative effort of home, school and community. We provide a comprehensive program of education for grades K-8 and are grateful 
for our very supportive and caring parents. 

2017-18 School Accountability Report Card for Hillcrest Middle 

Page 2 of 11 

 

 
Mission Statement 
We, the Gravenstein Union School District, are dedicated to academic excellence and the cultivation of individual strengths and talents 
in a caring and cooperative environment. Our sense of community fosters a high level of ethical, responsible citizenship. State and 
District policies support our common goal.