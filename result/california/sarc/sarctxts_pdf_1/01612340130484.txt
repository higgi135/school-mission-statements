Crossroads Independent Studies Program is part of the MacGregor Alternative Learning Center which includes Bridgepoint High School, 
Newark Adult Education and Transition to Adult Lifestyle Learning for developmentally disabled 18-22 year old students. 
 
The mission of Crossroads is to prepare students for success in college or career technical education.