Principal's Message 
 
Prospect High School is dedicated to high standards of performance. Our curricula are continually evaluated and modified to reflect 
the needs of students and to ensure that all students are challenged and supported in their academic growth. As a site—and 
districtwide—we continue to collaborate on implementing the newly adopted Common Core State Standards (CCSS), Next Generation 
Science Standards (NGSS), and the new social studies framework. We offer numerous Honors and Advanced Placement classes for the 
collegebound student, surpassing California State University and University of California requirements. Vocational and technical 
coursework is offered through the Silicon Valley Career Technical Education Center (SVCTE), formerly CCOC. Our excellent special 
education and English language development programs foster the spirit of full inclusion through mainstreaming and support with 
workshop courses. Ninety-five percent of Prospect’s graduating seniors enroll in a two- or four-year college or university. 
 
Parent and community volunteers are a vital part of the programs at Prospect. Each year, parents give more than 10,000 hours in 
support of the school. New parent members are welcomed into the school booster organizations, including the Parent Teacher Student 
Association (PSTA), Panther Paws Athletic Boosters, English Language Advisory Committee (ELAC), Music Boosters, Spirit Boosters and 
the Grad Night Committee. School Site Council (SSC) has waned in past years. In addition, we are continuing to work on fostering 
greater ties with all members of our community, including Saratoga City Council and West Gate Church. 
 
Prospect High School challenges students with a rigorous curriculum that prepares them to be effective communicators and problem 
solvers. Our students will demonstrate respect for themselves, their community and their environment while preparing to become 
contributing members of society. 
 
School Mission Statement 
 
Prospect High School provides students with a challenging, dynamic education that offers them the opportunity to gain the skills 
needed to be prepared for success in career and college. 
 
We encourage empowerment by honoring student voice and diversity within a safe and supportive campus culture. 
 
We value and teach the importance of integrity, perseverance and empathy. 
 
We recognize that we are part of a greater community, and a strong, active partnership strengthens all. 
 
School Vision Statement 
 
Prospect believes every student as the right to: 

• Reach her/his potential through a dynamic and engaging education 
• Attain skills and knowledge to be prepared for career and college 
• Be empowered to take an active role in hers/his community 
• Be a part of a supportive campus culture that prepares her/him to be an active citizen in a global world 

 

2017-18 School Accountability Report Card for Prospect High School 

Page 2 of 14