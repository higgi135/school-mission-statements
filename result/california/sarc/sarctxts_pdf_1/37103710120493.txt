The mission of Monarch School is to educate students impacted by homelessness and to help them develop hope for a future with 
the necessary skills and experiences for personal success. 
 
Our vision: Monarch School provides an excellent academic and supportive environment in which any student in San Diego County 
who is impacted by homelessness will receive a rigorous education and grow personally to become a highly motivated, contributing 
member of society. 
 
Monarch School is part of Juvenile Court and Community Schools within the San Diego County Office of Education (SDCOE). Monarch 
was founded in 1989 as a single-classroom community school with one teacher only a few blocks away from its current location. Strong 
community support, national exposure, and the formation of the nonprofit Monarch School Project (MSP) transformed the capacity 
of the school over the years. A capital campaign spearheaded by MSP raised $17 million over 10 years, enabling the school to move 
into its current location in May 2013. The larger, state-of-the-art facility enabled the school to more than double its student population 
with a capacity of 24 students per grade level and 350 potential students total. There are currently 273 students enrolled in grades 
kindergarten through 12 with an average class size of 20. Monarch has the unique opportunity to serve students ages 5 to 19. 
 
Monarch School is a national model for providing education and support services to students impacted by chronic homelessness. A 
satellite view of the school reveals that it is located strategically near downtown San Diego in the Barrio Logan neighborhood in close 
proximity to partner organizations that provide services and shelter for individuals and families in the San Diego community impacted 
by homelessness. A majority of our families are recipients of those services and are referred to Monarch directly from these partner 
agencies pursuant to Welfare and Institutions Code 300. Students reside in family shelters, shelters for victims of domestic violence, 
teen shelters, motels, hotels, in vehicles with their families, or share residences with other families in order to afford rent. Due to 
family transiency, students attending Monarch reside throughout San Diego County. The majority of students live in the central and 
south part of the county, where 71.1 percent of the county's homeless population resides (according to the San Diego Regional Task 
Force on the Homeless). 
 
In San Diego County, approximately 11.4 percent of the student population is receiving special education services; statewide, California 
has approximately 11 percent receiving special education services. Monarch School’s special education population is 18 percent, with 
45 students with Individualized Education Plans (IEPs). Monarch School meets the needs of these students with a special education 
coordinator and two education specialists. An itinerant speech-language pathologist, occupational therapist, and school psychologist 
provide services weekly per students' IEPs. 
 
Another significant group of students are those eligible and receiving free and reduced-price lunches. It is no surprise that 100 percent 
of Monarch School students qualify for free lunch given that the mission of our school is to serve families enduring homelessness and 
the impacts of poverty. 
 
Monarch School's instructional technology plan includes providing 1:1 access to technology for our students. We have 300 netbooks 
equitably distributed throughout our classrooms. All of our instructional spaces and classrooms have Smart Boards. 
 

2017-18 School Accountability Report Card for Monarch School - Public View Document 

Page 2 of 15 

 

The Monarch School Project (MSP) and Monarch School work hand-in-hand in an innovative partnership addressing not only the 
academic needs of our students, but also the social, emotional, and life skills required to ensure future success. These four areas of 
focus encompass a framework called the Four Pillars, which we use at all levels of our organization to put our mission into action 
including our Schoolwide Learner Outcomes. The partnership is strategic in its implementation of programs and services and allocation 
of resources around the Four Pillars. With a total operating cost of $4 million per year, MSP contributes 55 percent of the total budget. 
The emotional pillar takes a strengths-based and trauma-informed approach to provide students with ongoing supports and 
interventions. Student advocates aligned to grade levels implement organized systems to proactively support students and intervene 
so that instruction and learning remain the focus. Additionally, we provide students with therapeutic opportunities, such as expressive 
arts therapy and wrap-around mental health services. The life skills pillar focuses on setting and accomplishing achievable goals as our 
students prepare for college and career. Additionally, we provide targeted supports for alumni. The academic pillar is connected to 
standards-aligned instruction with student-centered learning where students are expected to be creative, collaborative, and 
resourceful problem-solvers. The social pillar encompasses activities involving athletics, student leadership, and developing interests 
and passions. Programs are implemented during our Social Growth Wheel, Discovery Hour, athletics, and after-school program. 
 
Monarch School utilizes an approach called Getting Results Intervention Team (GRIT) as a team approach to identify needs, implement 
additional academic and social/emotional interventions, and monitor progress, as part of a multi-tiered system of supports (MTSS). 
 
Monarch School eliminates barriers to school that students impacted by homelessness face by providing access to basic necessities, 
including showers, clothing, laundry facilities, and hygiene kits. Partnerships and donors enable Monarch to provide students with 
dental, vision, and hearing screenings, as well as follow-up care. Monarch has a partnership with Family Health Centers of San Diego, 
enabling us to have a registered nurse and health care navigator on-site 25 hours per week, giving our students access to basic health 
care, medication management, and support in obtaining additional resources and services. Monarch, in collaboration with the San 
Diego Family Health Centers, offers medical exams for all students and medical care on an as-needed basis. A nurse provides care for 
students at the school’s Health Center three days per week. The nurse also coordinates healthcare services for our students provided 
at the school and in the community through volunteer medical groups, such as dental and vision care providers. 
 
Monarch School is a school of choice for the families and students it serves. A student's enrollment options include: 1) the school of 
origin (school the student was attending when they became homeless; 2) school of residence (students who are permanently-housed 
and live in the area); and 3) Monarch School. Final decision for enrollment is made as a team that includes the student’s 
parent/guardian and Monarch School administrative staff. In some instances, school site or district staff members of the school or 
district of origin are included in the decision-making process. Placement decisions are made in the best educational interest of the 
student. 
 
To maximize the operational capacity and the planning for our school, we divide up elementary, middle, and high school programs for 
scheduling, programs, and services. School programs run from 6 a.m. to 6 p.m. daily, starting with breakfast and ending with the after-
school program. Class is in session from 8 a.m. to 3 p.m. Thursday is our minimum day to allow for professional learning and 
collaboration. 
 
Monarch School is a recipient of an After School Education and Safety (ASES) program grant. It is currently funded for --- students 
totaling $119,000. The program is run by our partner agency the Jackie Robinson Family YMCA. There are currently --- students in 
grades kindergarten through 8 enrolled. We attained -- percent of our attendance goal in 2017-18. 
 
Our ASES program is aligned with the content of regular school day and other extended learning opportunities. It offers a safe physical 
and emotional environment and the staff takes the same strengths-based and trauma-informed approach to supporting and 
intervening with students. YMCA after-school staff works closely with the Monarch principal to integrate an educational literacy 
element and an educational enrichment element connected to Monarch’s curriculum, instruction, and learning support activities. 
The educational literacy element provides tutoring and/or homework assistance designed to help students meet standards 
in reading/language arts, mathematics, history and social studies, or science. 
The educational enrichment element offers additional services, programs, and activities that reinforce and complement 
Monarch’s academic program. The YMCA offers visual and performing arts, music, physical activity, health/nutrition 
promotion, and general recreation. 

• 

• 

In order to support our mission, we have taken a collaborative approach to develop a Positive Behavioral Intervention and Support 
plan. Our commitment with this plan is that every student, staff member, and parent will understand their role in supporting a safe, 
civil, and restorative environment. Monarch School will be an environment that encourages and reinforces behaviors that are 
respectful, responsible, safe, and where students are ready to learn. 
 

2017-18 School Accountability Report Card for Monarch School - Public View Document 

Page 3 of 15 

 

After two years of visioning, planning, and fundraising, a 5,000-square-foot, 21st-century learning and career technical education 
space called the Launch Pointe opened in November 2016. Solution-oriented project-based learning with themes that relate to 
persevering through trauma along with performance-based assessments are innovative features within our instructional program. 
Learning takes place at school and through intentional opportunities in our community and with partners. Individual growth planning 
will match student goals with opportunities and resources. Our students will be empowered with choices when they leave our 
program. They will obtain and retain steady employment with a living-wage job and career path. More than 40 high school students 
participate in the Monarch Internship Program (MIP) with 24 community businesses and partners. MIP interns are paid a monthly 
stipend provided by MSP. Students can also pursue additional technical training, attend community college, or go to a four-year 
university. 
 
Monarch's goals are aligned with the goals of the San Diego County Office of Education's Local Control and Accountability Plan (LCAP): 
 
Goal 1. Ensure excellence in teaching and learning so each student is prepared to succeed in college and career. 
 
Goal 2. Cultivate stakeholder engagement to support excellence in each student's success. 
 
Goal 3. Develop coherent and transparent systems for operational excellence to support each student's success. 
 
Goal 4. Support the integration and transition of students who are at risk, expelled, English learner, and foster youth to be prepared 
to succeed in college and career.