Cedarwood Elementary is nestled among the cedar and pine trees in the beautiful mountain town of Magalia, California. Our school 
staff provides a positive, engaging, and rigorous environment where students feel safe and are motivated to learn. We are committed 
to a quality education for all students through meaningful learning emphasizing creativity, communication, collaboration, and critical 
thinking. We hold high expectations for student achievement, student behavior, and ourselves as professionals. At Cedarwood 
Elementary School, we believe every child is capable of learning, and we provide various opportunities for all students to reach their 
potential. Our teachers use multiple assessment information, including achievement data, to continually evaluate student progress 
and adapt their classroom practices to meet student needs. 
 
We provide academic interventions for students requiring additional support through a Response to Intervention model provided by 
highly qualified teachers and paraprofessional staff members. This blended model allows special education, Title I, and regular 
education teachers to collaborate and provide the best program possible for our students. It also allows us to challenge students who 
are performing above standard. As a result of these efforts, we received the prestigious award of a California Distinguished School in 
2006 and the California Title I Academic Achievement Award for the 2003/2004, 2004/2005 and 2005/2006 school years. 
 
Our curriculum is focused on the California Common Core State Standards, including English Language Arts/Literacy, Writing, Speaking 
& Listening, Language and mathematics. Our mathematics curriculum includes the progression of mathematical practices exemplifying 
the three principles of focus, coherence, and rigor. Bridges Math curriculum was introduced and implemented during the 2016-2017 
school year. We encourage strong parent and community partnerships and invite our parents, grandparents, guardians and 
community members to participate in the classroom. We believe a skilled and dedicated staff, motivated students, and supportive 
parents are powerful components for student success! 
 
We believe: Every student matters. Every moment counts.