Principal's Message 
Roosevelt is a small neighborhood school serving 360 Transitional Kindergarten through fifth grade students located in Burlingame, a 
suburban neighborhood ten miles south of San Francisco. 
 
Teachers, staff, and parents work together to support and reinforce Roosevelt’s mission statement: Roosevelt's mission is to develop 
students who are curious, compassionate problem solvers who have active and creative minds, the ability to collaborate and the will 
to take risks with their learning. 
 
Our students strive to be leaders in the 21st Century who are comfortable with technology, are able to work with others and who have 
the ability to creatively seek out and solve problems in their community. 
 
Matthew Pavao, PRINCIPAL