PRINCIPAL'S MESSAGE 
 
I'd like to welcome you to Cedar Middle School's Annual School Accountability Report Card. In accordance with Proposition 98, every 
school in California is required to issue an annual School Accountability Report Card that fulfills state and federal disclosure 
requirements. Parents will find valuable information about our academic achievement, professional staff, curricular programs, 
instructional materials, safety procedures, classroom environment, and condition of facilities. 
 
Cedar Middle School provides a learner-centered environment with a focus on providing a well-rounded educational experience for 
all students. Students receive a standards-based, rigorous curriculum along with opportunities to explore areas of interest. A dedicated 
professional staff, utilizing continuous assessment, provides innovative instruction focusing on 21st Century Skills such as 
collaboration, communication, critical thinking and creativity for all students. The variety of exploratory and support offerings 
accompanying the rigorous academic classes provide students opportunities for academic excellence, development of positive self-
expression, and personal maturity. 
 
The staff at Cedar Middle School is committed to working together in order to provide the best educational program possible for our 
students and we welcome any suggestions or questions you may have about the information contained in this report or about the 
school. It is through our collective commitment that we ensure all of our students will succeed. 
 
SCHOOL MISSION STATEMENT 
 
We at Cedar Middle School are dedicated to the pursuit of knowledge for all through passion and innovation. 
 
SCHOOL PROFILE 
 
Cedar Middle School is located in the central/southern region of Hesperia and serves students in grades seven and eight. At the 
beginning of the 2017-18 school year, 1331 students were enrolled, including 12% in special education, 14% qualifying for English 
learner support, and 73% receiving free or reduced-price meals. This was the fifth year to administer the California Assessment of 
Student Performance and Progress (CAASPP) with 32% of 7th grade students and 35% of 8th grade students meet or exceed the 
standards in ELA. For the Math CAASPP scores we had we had 18% of 7th grade students and 15% of 8th grade students meet or 
exceed the expected standards.