Principal’s Message 
Welcome to Brookside Elementary - at Brookside, we take great pride in our district, school, students, staff, community, and 
instructional approach. Our school team works collaboratively with our students, families, and community to provide a caring and safe 
environment that fosters mutual respect, exemplary effort, responsibility, teamwork, open communication, and recognition of 
excellence. This SARC is designed to help you understand our school’s educational programs, services, student achievement, and 
curriculum. It will also communicate the progress our students and school team are making toward accomplishing the mission and 
goals of the district and school, along with progress toward achieving federal, state, district and school academic targets. Our dedicated 
team of professionals - classified and certificated - are committed to providing all students with equal access to quality instruction, 
programs, and services so that all learners can achieve their highest potential. 
 
I would like to thank our Brookside families and community for their amazing involvement and support of our school, and welcome 
your continued participation in our advisory councils, Parent Teacher Association, family events, trainings, and school volunteer 
program. Thank you for your generous donations of time and resources. As indicated in this SARC, our students are benefiting from 
the collaborative efforts of our dedicated school team, families, and community participation. Together, we are making a difference 
in the lives of our students. 
 
Sincerely, 
Michael Griffin 
Principal 
 
Description and Mission Statement 
Brookside Elementary School serves approximately 750 students from Transitional Kindergarten through fifth grade. Our students 
come from newer communities in the Oak Valley housing development, as well as more established neighborhoods in Beaumont and 
Cherry Valley. As one of 7 elementary schools located in the Beaumont Unified School District, it is nestled in the beautiful San 
Gorgonio Mountain Pass. Brookside is home to 32 teachers, 24 support personnel, a six-hour librarian, one speech and language 
pathologist, and two full-time specialized academic instructors Our school team, in collaboration with our parents and community, 
are committed to continual improvement of all of our school programs. AVID, Advancement Via Individual Determination, is a school-
wide program with the goal of preparing all students for college readiness and success in a global society. In addition, to our AVID 
focus, we strive to teach students to be good all around citizens through the implementation of PBIS, Positive Behavioral Interventions 
and Supports, which explicitly teaches students the steps and skills needed to maintain appropriate behavior where ever they may be. 
Brookside also provided other youth services including Tutoring Academies, Art Club, Robotics Club, a partnership with the Anti 
Bullying Institute, and Friday Night Live. Our goal is to provide students with a well-rounded experience and for students to achieve 
both academically and socially. 
 
School Vision 
The vision of Brookside Elementary School is to provide a quality educational program designed to ensure that all students become 
productive, responsible, independent thinkers who contribute to a global, diverse, changing, technological society as life-long learners. 
 
School Mission 
The mission of Brookside Elementary is to provide high-quality educational opportunities for all students in a safe and secure 
environment through a shared commitment among home, school, and community. 
 
 

 

2017-18 School Accountability Report Card for Brookside Elementary School 

Page 2 of 11 

 

School Goals 
The school's goals are aligned with the district's goals. They are: 
 
Goal #1 
Brookside Elementary School will ensure a positive climate and school culture for students by providing opportunities for them to 
build positive relationships and to access resources from Beaumont and the larger community. 
Goal #2 
Brookside Elementary School will ensure a viable 21st-century learning environment for all students that includes full access and 
success in California State Standards & college and career preparatory courses. 
Goal #3 
Brookside Elementary School will provide an optimum learning and working environment by employing highly qualified certificated, 
classified, and substitute employees and maintaining the school facilities.