The Morningside Mariners have been “Sailing for Success” since July, 2000. Morningside School has achieved success by focusing on 
high expectations in all academic skills and social/emotional environments. Our campus is surrounded by single family dwellings and 
a city park. Staff, students, and parents establish short-term and long-term goals. All K-5 students receive a student planner that 
promote good study habits, to write goals, and encourages reading, good attendance, good character, and high academic success. 
 
Morningside’s Professional Learning Community works in partnership with all stakeholders to achieve the sites mission: Morningside 
School is anchored to high expectations where all Mariners will embark on a journey toward becoming life-long learners. Parents, 
teachers, and the community will provide a positive learning environment in which all learners will receive strategies to solve problems 
and understanding content. Multiple measures such as common formative assessments, district unit assessments, and other local 
assessments will be used to measure student academic achievement. A Response to Intervention model will be applied to assist 
struggling students in achieving the grade level standards. 
 
Morningside Staff will work collaboratively to prepare high quality instruction supported by research based techniques and strategies. 
All students will have access to the core curriculum. A variety of methods to check for understanding, including assessments, will be 
used to determine if students are meeting grade level standards and goals. For those students not meeting the grade level standards, 
a Response to Intervention model is applied. Tier 1 activities will consist of the core curriculum supplemented by universal access 
activities in the regular education classroom. Tier 2 activities will consist of grouping students for leveled instruction and the English 
Language Development Block. Tier 3 interventions will consist of the Learning Center pull-out program consisting of ELD, ELA, Math 
and after school tutoring.