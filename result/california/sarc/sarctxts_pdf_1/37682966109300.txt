DEER CANYON ELEMENTARY 
 
 Mission and Vision Statement 
 
 Our mission is to ensure all of our students develop a love of learning and become highly successful in school, society, and the work 
place. Through close collaboration, and in partnership with our families and community, we will provide challenging, real life learning 
opportunities to prepare our students for success in a diverse society and ever-changing world. We will model what we expect so our 
children become: 
 
 Self Directed Learners 
 Collaborative Team Members 
 Clear Communicators 
 Constructive Thinkers and Problem Solvers 
 Effective Users of Tools and Technology 
 Responsible Members of Society 
 
 It is our vision that Deer Canyon becomes a school where all students have the individual support, encouragement and opportunity 
they need to achieve academically at very high levels and to develop the personal habits and values which will allow them to be 
successful both in school and as productive members of our society. 
 
Deer Canyon Culture 
 
The staff and parents of Deer Canyon encourage students to be intrinsically motivated and ensure that our students develop a love of 
learning. We embed habits for future success through the explicit development of skills such as critical thinking, self-monitoring, 
persistence, and responsibility. Staff models a positive, caring, inspirational attitude every day. Through a rigorous and relevant 
academic program, we believe in and celebrate the success of every child. 
Deer Canyon Guiding Principles 
 
We, the staff, pledge to support the implementation of the District vision in concert with our mission and vision to ensure rigor, 
relevance, and relationships in all that we do. These are our guiding principles: 
 
Rigor 

• We believe all children can learn at high levels and it is our responsibly to ensure that they do 
• We ensure students demonstrate their understanding of concepts through multiple pathways such as technology, visual 

learning, use of kinesthetic materials, and creative problem solving 

• We believe a strong foundation in reading, writing, and math is critical to student success and strive for high academic and 

behavioral expectations for students and staff 

• We maintain a safe school environment that values respect and the individual gifts of our students 
• We continually seek innovative technology to create challenging classroom learning experiences resulting in 

differentiated, self-paced, multi-modality instruction 

 

 

2017-18 School Accountability Report Card for Deer Canyon Elementary School 

Page 2 of 12 

 

 
Relevance 

• We embrace the responsibility of preparing our students to become creative problem solvers who can be flexible and adapt 

to a changing world 

• Along with the California State Standards, we provide interactive, collaborative STEAM activities for students which develop 

strong problem solving life skills 

• We ensure academic support is in place for students with special needs in an inclusive environment 
• We provide opportunities to engage in enrichment activities to support all learners and learning 
• With the integration of technology and personalized learning, we ensure the 21st century student develops the necessary 

skills to be college and career ready 

 
Relationships 

• To ensure strong lines of communication are built throughout our organization, we foster positive relationships among 

staff, students, parents, and the community 

• Staff work collaboratively as a community of active learners through teamwork and professional learning 
• We foster leadership within our learning community among both staff and students 
• Through trusting and collaborative relationships with PTA and Foundation, we ensure programs are implemented to 

support the development of the total child 

• We embrace the unique and diverse culture of our school community and foster opportunities to promote tolerance and 

understanding 

• We value the relationship our parents and staff feel between each other and our school community 

 
 School Description 
 
 Deer Canyon Elementary School, which opened in 1990, is located in the southwestern portion of the Poway Unified School District 
in the community of Rancho Penasquitos in the city of San Diego. The school is a three-time recipient of the California Distinguished 
School Award; 1993, 1997, and 2000. Our 2005 application for Distinguished Schools status received honorable mention. Deer CAnyon 
was named a National Blue Ribbon School in 2015. Pre-school through grade 5 students are ethnically diverse yet similar in 
socioeconomic status. The student population is 42% Caucasian, 36% Asian, 12% Filipino, 8% Hispanic, 2% African American, and 1% 
other racial ethnic groups. 20% of the students are learning English as a second language and 7% qualify for the free or reduced lunch 
program. 
 
 Deer Canyon is a neighborhood school, with virtually all of our students coming from the immediate community. No buses are needed 
to serve our regular education students, thanks in part to the school’s quiet residential setting. 
 
 The teachers and support staff at Deer Canyon believe that a strong academic focus supported by a positive and caring learning 
environment is essential to student success. A strong emphasis is placed on providing a safe, attractive environment for students and 
staff. Equally important is the emphasis here on developing the whole child through early mental health programs, character 
development and service learning opportunities. The school climate is truly peaceful with student rights and school rules maintained 
campus wide. 
 
 Deer Canyon’s parent volunteer program enhances both instruction in the classroom and the school’s special events. PTA efforts help 
the school provide assembly programs, parent education workshops, Partners in Print, study trips and library books. The DCES 
Foundation which works hand in hand with the PTA helps fund special programs in the areas of: music, PE, technology, art, and science. 
 
 Our school meets the needs of all of our students through many special programs including English Language Learners (ELL), Gifted 
and Talented Education (GATE), Speech and Language, Special Education, a Parent Participation Preschool, Extended Student Services 
(ESS) and an Extended Day Program. Deer Canyon is also the home of an Autism Spectrum Disorder program, serving children from 
pre-school through Kindergarten in four Special Day Classes. These forty-eight children receive all of their support services here and 
they have many opportunities to participate with typical peers in all aspects of school life. 
 
 
 
 

2017-18 School Accountability Report Card for Deer Canyon Elementary School 

Page 3 of 12