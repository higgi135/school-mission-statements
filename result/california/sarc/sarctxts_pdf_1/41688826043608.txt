Principalís Message 
Washington Elementary exemplifies a true neighborhood school. Our community embraces all aspects of our students' learning from 
academics to their social and emotional development. Our teachers work together in professional learning communities reviewing 
student data in order to develop the most appropriate instruction for each Washington student. On any given day you will meet 
parents volunteering in the classroom, monitoring the playground, or supporting our computer lab and library. We are always looking 
for new Wildcats! 
Julie Eastman, PRINCIPAL