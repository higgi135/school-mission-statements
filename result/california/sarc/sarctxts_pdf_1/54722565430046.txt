Description of District 
The Visalia Unified School District is the oldest school district in Tulare County. The district is comprised of 25 elementary schools, 5 
middle schools, 4 comprehensive high schools, 1 continuation high school, 5 charter schools, 1 adult school, and a school that serves 
orthopedic handicapped students. Over 32,000 students Pre-K to adult are served through the Visalia Unified School District. 
 
Description of School 
Golden West High School served approximately 1,750 students in grades 9-12 in 2018-19. Our teachers and staff are dedicated to 
ensuring the academic success of every student and providing a safe and productive learning experience. The school holds high 
expectations for the academic and social development of all students. Curriculum planning, staff development, and assessment 
activities are focused on assisting students in mastering the state academic content standards, as well as increasing the overall student 
achievement of all subgroups. 
 
School Vision Statement 
All students will achieve academic success 
All students will have a sense of belonging and connectedness to Golden West High School 
 
School Mission Statement 
Students will pursue success in academics, activities, and athletics.