Principal's Message 
Hello families of Yermo School students. The purpose of the School Accountability Report card is to provide parents and community 
members with information about Yermo School's instructional programs, academic achievement, materials and facilities, and the staff. 
 
We have made a commitment to provide the best educational program possible for our students. The outstanding quality of our school 
is a reflection of our highly committed staff. We are dedicated to ensuring that our school provides a welcoming and stimulating 
environment where students are actively involved in learning academics, as well as positive values. Through our hard work, together, 
our students will be challenged to reach their maximum potential. 
 
Parents and community play a very important role in our schools. Understanding our educational program, student achievement, and 
curriculum development can assist both our school and the community in ongoing improvement. 
 
Yermo School Mission 
Yermo School students will achieve at their maximum potential in a safe, engaging, inspiring, and challenging learning environment. 
Yermo School will work in partnership with students, families and the community to ensure that each student acquires the knowledge, 
21st Century Learning skills and core values necessary to achieve personal success and to enrich the community. 
 
Community & School Profile 
Located in the rural high desert of Southern California, 126 miles from Los Angeles and 144 miles from Las Vegas, Silver Valley Unified 
School District educates nearly 2,200 students in transitional kindergarten through grade twelve. Covering an area of 3,200 square 
miles, the district serves the communities of Calico, Daggett, Fort Irwin, Ludlow, Newberry Springs, and Yermo. 
 
Yermo School is located in the Mojave Desert off of Interstate 15. Yermo School serves approximately 380 Transitional Kindergarten 
through eighth grade students on a traditional calendar schedule.. 
 
The district is comprised of seven school sites including four elementary schools, one middle school, one comprehensive high school, 
and an alternative education center.