School Description 
 
Sierra Pacific High School has proudly been the home of the Golden Bears since its doors opened in August 2009. The school started 
as a vision between the Hanford Joint Union High School District, College of the Sequoias and the City of Hanford in the late 90's. Sierra 
Pacific is part of a joint educational complex built by these three entities. Our staff of professionals is dedicated to cultivating an 
educational environment where all students can reach their full potential. 
 
In our fourth year of existence Sierra Pacific housed all four grade levels for the first time filling the campus with a population of 823 
students grades nine through twelve. This population is appropriate for the first phase of the campus that was projected to house a 
maximum of 850 students. In June of 2013, Sierra Pacific held its first graduation ceremony for 185 senior Golden Bears. During the 
2012-13 school year, the Sierra Pacific staff completed the school's first full self-study and earned a six-year accreditation with a 
midterm revisit. As education moves into the 21st century and with the adoption of the Common Core State Standards, the core 
academic programs are working diligently to align curriculum and focus on strengthening instructional practices. Sierra Pacific High 
School not only maintains a strong core academic program, the campus also offers a variety of co-curricular and extra-curricular 
programs providing students a diverse range of activities to serve their unique interests and connect students to the school and 
community. In addition, safety is top priority. Our safety team constantly reviews and maintains effective safety practices and 
procedures to keep our students and staff focused on education. Our goal is to service and educate the whole child providing each 
student with the tools to succeed beyond the walls of the school. We believe that whether our students are planning to work after 
high school, attend community or four-year colleges, or join the military, each student here at Sierra Pacific High School deserves 
access to the best education and opportunities we can offer. 
 
 
Mission Statement 
 
Develop academically prepared, goal-oriented, responsible students through a positive learning environment 
that fosters creativity and diversity to become productive, successful citizens and reach their full potentials.