Valley Oaks Vision Statement: 
 
At Valley Oaks, we envision: 
Students meeting and exceeding their individual growth goals based on Common Core State Standards 
Students receiving personalized and challenging instruction 
Students being encouraged by parents, staff, and community to do their best 
Students learning in a positive and safe environment 
Students and staff embracing and respecting diversity 
Students developing life skills, critical thinking, and problem solving skills 
Students using 21st century technology to enhance learning 
Students and staff developing and celebrating their identified strengths 
Students and staff are modeling the Four School Rules and Eight Great Character Traits 
Students, parents and staff working as a team 
Students giving to others and the greater community 
Students and staff taking pride in their work EVERYDAY 
 
 
Valley Oaks Mission Statement: 
 
Education is the shared responsibility of everyone: student, teacher, parent and community. 
Valley Oaks Elementary is committed to: Growing And Learning Together 
 
 
Principal’s Message 
Welcome to 21st century learning at Valley Oaks - where every student is guided on a path to their own bright future! We are 
integrating the California Common Core State Standards with constantly updated technology through the individual use of 
Chromebooks for each student in grades TK-6, as well as software resources to bring the world into our classrooms. This allows 
classrooms to transform into blended personalized learning environments that integrate technology throughout academic content 
areas. Students participate daily in developing critical thinking skills, collaborating with peers while building their communication 
skills, and with creative and innovative service learning projects. In addition, staff, parents, and students work together to set and 
create personalized learning plans based on identified student strengths and individual academic needs. The staff continues to work 
collaboratively to offer rigorous and creative opportunities for our students to prepare them for college and career opportunities. 
Valley Oaks students know they are valued, can achieve, and are on the path to becoming independent and confident teenagers and 
young adults. Our students feel engaged and have a high sense of engagement while at school, as well as high hope for themselves 
and their future. 
 
 
 

2017-18 School Accountability Report Card for Valley Oaks Elementary School 

Page 2 of 23