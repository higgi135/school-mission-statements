Newhart’s Mission Statement: 
It is the mission of Newhart Middle School to prepare all students to achieve high levels of academic success and develop character 
while instilling the tools necessary for their future in a competitive world. 
 
Newhart Vision Statement: 
Newhart provides an inclusive educational experience that promotes exploration, collaboration, and engagement, resulting in global 
citizens who are able to adapt and compete in a rapidly changing world. 
 
Newhart Middle School is a sixth through eighth grade school located adjacent to the Norman P. Murray Center in Mission Viejo. The 
school enjoys a wonderful working relationship with the City of Mission Viejo and has access to the parks adjacent to the campus. The 
community parks and school facilities are used on a reciprocal basis to the benefit of all. The school currently runs a Character Counts! 
program that mirrors the program run by the city. 
 
There are approximately 1250 students at Newhart Middle School. The school employs 56 teachers, three administrators, and a large 
support staff. Students at Newhart are offered a strong academic program in the core curriculum areas of language arts, social science, 
mathematics, and science. 
 
The teaching staff is composed of talented individuals who are collaborative educators. They have established high standards for 
themselves as well as for students. 
 
Many Newhart students participate in the school’s Visual and Performing Arts program. Student groups and individuals consistently 
earn recognition and awards in the areas of instrumental music, choral music, and art. The school is also home to excellent programs 
in the areas of industrial arts, drama, engineering and language immersion in both Spanish and Mandarin. 
 
Based on the performance of students on statewide examinations, and the overall quality of the educational program, Newhart Middle 
School has been selected as a National Blue Ribbon School, as well as a California Distinguished School. 
 
Newhart students have a continued record of high achievement. Newhart demonstrates its degree of excellence through high test 
scores; achievement of students at higher levels of education; success in District, county, and state sponsored competitions; and 
parent and student satisfaction. The success of the school is due to an involved, supportive community which values quality education 
and a professional staff that loves the excitement of helping young adolescents to learn. 
 
For additional information about school and district programs, please visit www.capousd.org