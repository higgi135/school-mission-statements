School Vision Statement 
Lewis Elementary School is dedicated to work in a partnership with parents, community, and staff to meet the academic, social, and 
emotional needs of each student. Collectively, we will provide a safe, supportive learning environment so that all students reach their 
maximum potential and become lifelong learners. 
 
We are Lewis 
United, Educated, Moving Forward ... 
Coyote Strong!