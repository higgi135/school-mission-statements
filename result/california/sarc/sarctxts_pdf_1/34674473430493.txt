Mission Statement 
Bella Vista High School, a collaborative learning community based on a foundation of excellence, will inspire each student to excel and 
positively impact society by empowering them to be dynamic thinkers, leaders, and creators in a complex world. 
 
School Profile 
Bella Vista High School is one of nine comprehensive high schools in the San Juan Unified School District with approximately 2,000 
students. Bella Vista offers a wide variety of courses and extra-curricular opportunities for student involvement. Our programs are 
highly successful with athletic, academic, and performance championships won regularly. We are a California Gold Ribbon School, and 
in the spring of 2016, the school received a full six-year accreditation from the Western Association of Schools and Colleges (WASC). 
Students, teachers, staff, parents, and the community work together towards increased levels of student achievement. Our superior 
reputation is a direct result of high expectations and our vision of every student being equipped to excel after high school. 
 
Student demographics include 70% White, 19% Hispanic, 6% Asian, 3% African American, 1% Pacific Islander, 1% American Indian. 
Only 2% of our students are English learners, and 14% receive services for disabilities. Our low socioeconomic numbers have steadily 
increased, and currently 28% of our students receive free or reduced price lunches. 
 
Principal's message 
We offer a wide range of opportunities and unique programs: glass art, competitive athletic teams in 14 sports, marching band, choir, 
nationally-ranked wrestlers, a Rock The Arts event, the county's top Academic Decathlon team, Japanese, and a College and Career 
Center run by parents are great examples. Our academic supports include a successful AVID program, an Advocacy course for 
struggling freshmen, and after-school tutoring for all students. 
 
From academics to extra-curricular competitions, Bella Vista stands out as a school community that excels. We hold students to high 
standards, and they rise to the expectation set before them. 
 
Bella Vista offers an academic program, aligned with Common Core State Standards, which is designed to prepare students for success 
in the workplace and in post-secondary schooling. As we work towards the achievement of our district LCAP and Strategic Plan, we 
are focusing on these two goals: 
 
1. Improve and support student learning to close achievement gaps and ensure all students graduate college and career ready. 
2. Foster respectful, collaborative, and reflective school and district cultures that ensure academic and social/emotional well-being for 
each student. 
 
The goals in this school improvement plan will move our student population closer to achieving our district mission and ensure student 
success. 
 
 

2017-18 School Accountability Report Card for Bella Vista High School 

Page 2 of 15