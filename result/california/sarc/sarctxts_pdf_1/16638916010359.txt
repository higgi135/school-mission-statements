"Increasing Student Success!” 
 
The Vision of Corcoran Unified is to be "A Destination District where people are drawn to Corcoran due to the quality, reputation, and 
accomplishments of our students and schools." 
 
The MISSION of the Corcoran Unified School District is “We are improvement driven - Mind, Character, and Body.” 
 
The Vision and Mission of John Muir Middle School is “We are Respectful of Others, Property, and Self, and We ARE Future High School 
Graduates.”