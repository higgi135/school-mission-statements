Le Grand High School has a long tradition of serving the educational needs of the young people of our communities. Our philosophy 
is to provide a democratic campus in which pupils, parents, teachers, administrators, the school board, and communities work 
together in a cooperative effort to make it a wholesome, exploring, and achieving educational center of learning that will prepare our 
young adults to meet their future. 
 
We believe that the school environment is a place to encourage the development of high ethical and moral values, cultural integrity, 
and a love of learning. Our educational programs instill in the learner the principle that access to an education is a privilege in a free 
society. The mission of Le Grand High School is to provide 
the highest quality of education possible for all students through independent thinking, moral and social responsibility, a positive 
impact on school and community, and economic self-reliance. 
 
Le Grand High School has continued to work with Cunningham Education Consulting to provide staff and administration with 
professional Development. This school year was productive in fulfilling the requirements of our LCAP Plan which is funded through 
State and Federal Categorical monies.