Golden Ridge School is the court school in El Dorado County designed to serve the western slope region. Student population consists 
of court incarcerated youth that generally range between grades 7-12. The school operates 228 days per year. 
 
The school is located in Placerville, CA. Partnerships with Probation, Juvenile Hall, Mental Health and Health Care staff is essential. 
These partnerships focus on the emotional, physical and academic well-being of our clients. 
 
Mission Statement: The Court School Program staff, in partnership with Probation staff at both Golden Ridge School (Placerville) and 
Blue Ridge School (South Lake Tahoe) are dedicated to creating safe and productive learning environments, where positive experiences 
enrich student alternatives and choices. 
 
We are committed to… 
 
Providing individualized academic programs for each student that best advocates for their present and future educational needs and 
goals. 
 
Providing differentiated instruction that allows access to standards-based curriculum, innovated use of technology and development 
of 21st century skills needed to be college and/or career ready, upon graduation. 
 
Creating a positive and productive learning environment that promotes social and emotional skill development by engaging students 
in activities that improve self-esteem; help students learn to interact in a positive manner with others; and utilize appropriate conflict 
resolution, anger management, and decision-making strategies. 
 
Creating a safe, structured, well-defined instructional program that communicates high expectations, provides high levels of support 
and demonstrates staff unity in standing firmly together on standards of behavior, academic performance and, ultimately, the success 
of each and every student.