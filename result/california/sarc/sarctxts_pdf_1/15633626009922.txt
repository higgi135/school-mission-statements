Amy B. Seibert School is a neighborhood school located at 2800 Agate Street between Planz Road and Wilson Road, just east of 
Highway 99. The school named for Amy Boettler Seibert who dedicated over 50 years of her life to education, welcomed its first 
students in the fall of 1963. As a Kern County librarian, a teacher at Panama School, and a 22-year member of the Panama School 
Board, Mrs. Seibert always worked for children. She is remembered for her love of learning and teaching and her determination that 
children deserve the best they can be offered. Staff members, parents, and members of the community have kept Mrs. Seibert’s 
tradition of support and involvement alive. Seibert School is successful because of this continued help and guidance. Together we 
strive to achieve the District’s goal of “Excellence in Education.” The School Accountability Report Card was established by Proposition 
98, an initiative passed by California voters in November 1988. The Report Card, to be issued annually by local school boards for each 
elementary and secondary school in the state, provides for parents and other interested people a variety of information about the 
school, its resources, its successes, and the areas in which improvements are needed. 
 
Seibert Elementary School receives school-wide Title I and Local Control Funds. While Title I allocations fund closing the achievement 
gap through providing additional direct services to students, Local Control Funding is used to assist students identified as English 
Language Learners, Foster, or Homeless. 
 
Our mission is "BUILDING HOPE AND A FUTURE TOGETHER" by working collaboratively as a staff with parents to ensure that all of our 
students learn at the highest levels. Seibert Elementary is continuously working to improve student learning. A standards-based 
curriculum, common assessments, and a Multi-Tiered System of Supports (MTSS) model provide the foundation for systematic 
improvement of student performance and school programs. We believe in the use of Positive Behavior Intervention Support while 
working with students and staff. Students will leave Seibert Elementary School with a love of learning, a strong foundation in personal 
and social skills, and the ability to meet future academic challenges.