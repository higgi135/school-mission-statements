The mission of the faculty, staff, administration and Governing Board of Ella Barkley Continuation High School is to provide a 
comprehensive educational program for all students so that they may have the skills and the opportunity to realize their full potential 
and, after graduation, become productive and contributing members of society. Each student, regardless of abilities, socio-economic, 
or cultural background should develop a sense of self-worth, accountability, responsibility, a desire for lifelong learning, and a genuine 
concern for the welfare and cultural diversity of others.