"We at Weed Elementary School believe it is our responsibility to provide quality educational programs which foster significant 
academic achievement for ALL students in a safe environment by promoting mutual respect and acceptance of individual differences." 
 
Weed Elementary School is located at the base of majestic Mt. Shasta in far northern California in Siskiyou County, one hour north of 
Redding and an hour south of the Oregon border, just off Interstate 5. The school site is centrally located on a hill overlooking the 
town of Weed. The campus is arranged in three separate building groups to accommodate the primary, intermediate and middle 
school classrooms. There is a large gymnasium complete with locker rooms and a separate cafeteria with a full commercial kitchen. 
 
Weed Elementary School District is a kindergarten through eighth grade district with an enrollment of approximately 257. 
Approximately 83% of our student population participates in the free and reduced lunch program. There is a high rate of mobility in 
Siskiyou County. Our K-3 grades are piloting the reading program Success For All, 4th-5th grades are piloting Benchmarks and our 
middle school, 6th-8th grades, have adopted Amplify. We have adopted California Math for 6-8 grades and Math Expressions for 
grades for K-5. We offer intervention programs for individual and groups of students in grades kindergarten through eighth during 
the regular school day. Our middle school program continues to improve in preparing our students to be college and career ready. 
 
We believe that parents are our partners in education. We have an active Site Council and a parent group called Cub Power that 
sponsors numerous fundraisers throughout the year to help support our student activities. We provide a variety of extra curricular 
activities including co-ed volleyball, cross country, girls and boys basketball, cheerleading, track, student council, peer counseling and 
we use PBIS as our systems of approach to behavior. WES offers many different electives for 6-8 grade classes such as cooking, VAPA, 
music, coding, technology, 3-D printing and design, small engine exploration, tutoring and green team. We have a SAFE program 
(Siskiyou After School For Everyone) operated within the guidelines set by the California Department of Education After School 
Education and Safety (ASES) program. The SAFE Program is open from the end of each regular school day until 6 PM offering a 
nutritious snack, supper, homework assistance, academic enrichment and physical activities in a healthy, safe environment which 
encourages parents, teachers, students and the community to share resources that benefit both the students and the community. 
 
In addition, Weed Elementary School provides a nutritionally balanced hot breakfast and lunch program, a full day Kindergarten, small 
class sizes, as well as daily intervention that is built into the academic schedule for all grades.