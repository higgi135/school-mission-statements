PRINCIPAL'S MESSAGE 
As principal, I have the unique privilege of introducing you to the Annual School Accountability Report Card for Lime Street Elementary 
School. Whether student, parent, staff, or community member, the data contained within these pages will prove useful in informing 
you about our school and community, including but not limited to: demographics, achievements, progress evaluation, ongoing goal 
realization, discipline, budget, and facility enhancement. 
 
In accordance with Proposition 98, every school in California is required to issue an annual School Accountability Report Card. 
However, we view this as not only a means of complying with state legislature, but as an opportunity to keep our community, and the 
public in general, well informed. We desire to keep the lines of communication open and welcome any suggestions, comments, or 
questions you may have. It is the opinion of the district, myself in particular that a well-informed public is vital in continuing to advance 
in an ever-evolving world. 
 
SCHOOL MISSION STATEMENT 
Lime Street Elementary - School of Technology and Innovative Learning community is dedicated to the success of all students by 
creating an environment committed to "Higher learning for all, whatever it takes!" 
 
SCHOOL PROFILE 
Hesperia Unified School District is located in the high desert region of San Bernardino County, approximately 40 miles north of the 
Ontario/San Bernardino valley. More than 20,000 students in grades kindergarten through twelve receive a rigorous, standards-based 
curriculum from dedicated and highly qualified professionals. The district is comprised of 15 elementary schools which includes 3 
choice schools. At the secondary level Hesperia has 3 middle schools, 3 comprehensive high schools, 1 alternative school, 2 
continuation high schools, 1 community day school, and 6 charter schools. 
 
Lime Street Elementary is located in the south/central area of Hesperia and serves students in grades PreSchool 3 years old through 
sixth grade. Currently, we have 881 students enrolled, including 20.0% in special education, 18% qualifying for English learner support, 
76.7% Hispanic, 18.5% African American, 16.7% White and 100% receiving free or reduced-price meals. All students can receive a free 
breakfast every morning and lunch everyday.