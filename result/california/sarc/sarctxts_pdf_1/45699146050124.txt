PRINCIPAL’S MESSAGE 
Anderson Middle School is the final stop for Cascade Union Elementary School District Students and we work hard to prepare our 
students for success at their next level of education. We have a phenomenal instrumental music program. We have a growing choir 
program and also offer American Sign Language, technology, art, have started a robotics program this year. We are especially proud 
of our partnership with the City of Anderson. We have a community service elective and leadership class that support community 
events throughout the school year and summer. 
 
Anderson Middle School was built in 1949 and its most recent addition is the new state of the art gymnasium which was added in 
2014. The library, media center, science lab and STEM lab were all updated in 2018. 
 
Our full time staff consists of a principal and a team of certificated teachers, resource specialists, and special day class instructors. We 
are supported by paraprofessional, secretarial, library, custodial, and kitchen staff members, as well as specialists in behavior 
intervention, technology, health and special education. 
Anderson Middle School’s ethnically diverse enrollment includes American Indian, Asian, Pacific Islander, Filipino, Black, Multi-ethnic, 
Hispanic/Latino, and white students. 
 
MISSION 
Inspire – Believe – Achieve 
 
VISION 
Inspire – The entire staff support, include, and serve all students from across our community, creating a culture of excellence, 
challenging them to be successful, continuous learners who are academically, socially, and emotionally prepared for the future. 
Believe-Using student-centered curriculum and engaging instruction, enhanced with cutting-edge technology, we provide positive and 
safe classrooms that focus on the whole child. 
Achieve – With students, families, and the community as equal partners, we are dedicated to preparing confident, healthy, respectful, 
and responsible students who can succeed and be productive tomorrow, next year, in high school, and in their post-graduation college 
and/or work careers.