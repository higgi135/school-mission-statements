Gold Ridge is a Kindergarten through fifth grade school. Our student enrollment, reported on the California Basic Educational Data 
System (CBEDS) in October 2017 was 612. The Gold Ridge staff of dedicated professionals and paraprofessionals fosters a love of 
school and learning in each child.The school is located in the beautiful Broadstone subdivision of Folsom, surrounded by Kemp Park 
and the natural wetland marshes. The campus houses 25 classrooms, a library/media center, small group instructional spaces, a 
Student Care Center providing before and after-school care, an adult education preschool program, and a multipurpose room which 
includes a stage and serving kitchen. 
 
We have a culturally diverse student population. Our students speak 30 different home languages; language minority students 
represent almost one-third of our school population. Our traditional school year calendar begins mid-August, providing three 
trimesters of instruction. Our educational focus is based on a strong academic curriculum as well as enrichment instruction. Our 
dedicated staff believes in unlocking the hidden talents of the whole child by supporting the strengths of every student in both 
academic and non-academic areas. To complement our academic emphasis, we believe all students benefit from a rich variety of 
experiences in art, music, physical education, technology, and the performing arts. Students are provided these experiences through 
classroom activities, outdoor educational field trips, after-school clubs, assemblies, and guest speakers. To further serve the needs of 
our students, a number of programs are available including special education, speech and language services, Student Care Center, 
Adult Education Preschool, and extended day reading and math intervention. 
 
Gold Ridge values and honors its relationship with the community. Our Parent Teacher Association (PTA) is actively involved in planning 
many exciting family activities year round, enriching the lives of our community members and building lasting friendships among the 
students and their families. We are fortunate to have many different volunteers to support our school: PTA board and participant 
members, room parents, classroom/teacher/student support volunteers, community volunteers from Vista del Lago High School, and 
Intel employees working in our PC Pals program. School Site Council advises the principal on expenditures that supplement and enrich 
classroom learning experiences and on professional development for our faculty. The strong parental support we receive helps us 
attain our goal of providing an excellent educational program in a warm, caring and safe environment. School spirit abounds as the 
Gold Ridge student body participates in Spirit Days planned by the Student Council, musical performances, and after-school clubs. 
 
The Gold Ridge staff believes all children can learn! We are committed to providing the very best educational opportunities and hold 
high behavioral and academic expectations of our students to ensure their success. Being a part of the Gold Ridge experience means 
being valued, respected, and appreciated. Staff, students, parents, and community have forged strong bonds that support and 
celebrate the adventure of individual success and the challenge of change. We are very proud to be an excellent learning community! 
We are confident our students will look back with pride and fond memories on their time spent learning at Gold Ridge.