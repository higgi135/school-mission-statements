Woodrow Wallace Middle School, serving sixth through eighth graders, is dedicated to ensuring the academic success of every student 
and providing a safe and comprehensive educational experience.The school is located in a rural, recreational setting approximately 40 
miles northeast of Bakersfield. The school serves the communities of Lake Isabella, Bodfish, Mt. Mesa, Wofford Heights, Kernville, and 
Havilah. Woodrow Wallace Middle School operates on a traditional school calendar. The educational programs at the school are 
tailored to meet the needs of the changing society. 
 
Our district mission is to be: A COMMUNITY UNITED IN CREATING LIFELONG LEARNERS TO IMPROVE OUR FUTURE THROUGH 
EDUCATION 
 
To meet this challenge, our school community is actively engaged in professional learning in order to align and improve our practices 
in implementing the California State Standards, and the instructional shifts inherent in those documents. Our professional learning is 
focused on essential components in instruction and learning of the standards--particularly regarding the use and development of 
students' academic language, analytical reading, and writing. 
 
In the 2015-2016 school year, our Professional Learning Communities have continued our study of the shifts in common core 
instruction and learning with a focus on lesson design for close reading, academic vocabulary development, student academic 
discourse and writing across the curriculum. We have implemented trimester benchmark assessments in mathematics using iReady 
by Curriculum Associates for reading diagnostic and progress monitoring assessment and math diagnostic assessment. We continue 
building our reading and mathematics infrastructure for collecting timely and actionable data about students' learning needs and 
progress, and have implemented Tier 2 support time in our master schedule blocked periods during which teachers address additional 
learning needs of students who need more time and practice to master skills or concepts. We have implemented a cycle of pretest, 
teach, test, reteach and retest to provide Tier 2 level strategic support for students with this need. (Response to Instruction and 
Intervention) Woodrow Wallace Middle implemented student-led parent conferences in 2015-2016 which led to a significant increase 
to over 50% of parents engaging with their children around demonstration of learning. 
 
In addition to academic supports, we include the addition of a full time district behavioral specialist who works with our students 
needing behavior supports using such strategies as daily check-in/check-out, small groups for developing social skills and grief support, 
and individual supports for students in crisis. 
 
Our school began the implementation of Positive Behavioral Intervention Systems (PBIS) in 2013-2014, and we have expanded the 
program this year. As is evident in our suspension and expulsion data, PBIS activities have been a contributing factor in the reduction 
of student suspensions. Students earn points each day for attendance and appropriate behavior, as well as Colt Cash (our token 
economy) which can be used to purchase activities and special privileges during the week. These programs have been instrumental in 
creating a positive shift in our school culture and school climate. 
 
In addition to our core academic courses, we are proud to offer elective course access to all 7th and 8th grade students which include 
courses in visual arts, media production, leadership skills, Mock Trial, Advance Cardio, Classics, intervention, and school/community 
service activities. Our students are also provided opportunities to participate in after-school sports teams for volleyball and basketball. 
Students participate in academic competitions such as the yearly Elks patriotism essay and Daughters of the American Revolution 
essay contests, science fair, and history day. Recognition for academic achievement is given at the end of each trimester and at the 
end of the year. Students may qualify to join our chapter of the California Junior Scholarship Federation, which is very active on our 
campus in fund raising and community service. The CJSF students have attended the state conference for the past 3 years, and we 
have brought home the Marian Huhn Scholarship winner one of those years. 
 

2017-18 School Accountability Report Card for Woodrow Wallace Middle School 

Page 2 of 11 

 

Woodrow Wallace Middle School also offers KREM (Kern River Enviornmental Magnet School) which is an independent study program 
that provides one day per week of classroom instruction and preparation for a second day each week of field study experiences in 
environmental sciences. This program offers the alternative for those parents/families who prefer a "home schooling" type option 
blended with a classroom and "hands-on" educational experience. We are uniquely positioned at the intersection of 5 bioregions to 
provide this type of educational opportunity to our families. In its inaugural year, 2013-2014, KREM won awards for its program, and 
individual students won awards for their studies and projects. We look with anticipation toward the evolution of this unique program 
over time. 
 
Woodrow Wallace Middle School's Associated Study Body is active in leading and promoting a great school climate. Officers have 
attended the California Association of Student Councils leadership conference in Santa Barbara for the past two years. One of the 
most important activities they have implemented this year is our Friday morning school mixers. Our entire student body assembles 
each morning around our flag pole for announcements, encouragement, and the Pledge of Allegiance. But on Friday mornings, as a 
whole school community, we participate in fun songs and chants. Students, teachers, and classes are recognized for their school spirit. 
The ASB has organized and run spirit assemblies, brought motivational guest speakers to campus, and plans the behavioral reward 
days for each trimester. For the past 4 years in a row, we have ASB students involved in planning the Leaders in Life Conference held 
anually in Bakersfield.