Mission Statement: 
 
The mission at Plumas Avenue School is to is to insure high levels of learning for all students. We accomplish this by creating a safe 
and caring community, partnering with families and staff. 
 
School Description: 
 
Plumas Avenue School is a transitional kindergarten through fifth-grade school that lies in a residential neighborhood in northern 
Oroville, California. The average enrollment of Plumas is 330 students, and to serve those students we have 13 regular classroom 
teachers, an intervention/English language teacher, a part time resource specialist (special education) and a half-time school 
counselor. In addition, we have 6 Para-professionals assisting in grades TK-1, a half-time library clerk, a part-time computer lab 
technician (para-educator), a part-time health assistant, a full-time and part-time secretary, a full-time custodian and a part-time 
evening custodian. We also receive additional district services from the psychologist, speech teacher, and nurse. All 2nd-5th grade 
classrooms have a Chromebook cart, and each student in these classrooms has his/her own Chromebook for use (1:1). Each of the 
transitional kindergarten, kindergarten, and 1st grade classrooms have 8 Chromebooks, as well as the use of a computer lab with 34 
desktop computers. All students and teachers utilize Google Applications for Education, including Google Classroom, Google slides, 
Google documents and more. 
 
The staff at Plumas Avenue view the student as the ultimate focus of all activities on our campus and value each child’s uniqueness. 
We try hard to work in partnership with families to provide students an outstanding learning experience. Our school is academically 
oriented, providing all students with the educational programs necessary for them to develop a solid base in reading, math, and written 
language, through innovative instructional practices incorporating the arts, science and technology.