SCHOOL DESCRIPTION: 
Since Mission Crest's opening in 2007, it has been a commitment of this school to provide a quality education for the students in its 
community. Currently we have aligned our educational practice with the district's goal, to prepare today's students for tomorrow's 
world. We are committed to refining our teaching practices to stay current with 21st Century teaching practices including the infusion 
of the 4Cs (Critical Thinking, Collaboration, Communication, and Creativity) into our daily practice. The teachers at Mission Crest 
collaboratively work together through the PLC process to implement the federally regulated California State Standards (CSS) as well 
as fulfilling the school mission of inspiring every student to care, think, achieve, and learn. At its core, Mission Crest is the school in 
service of others. Service learning is integrated into our curriculum and is a component in nearly all events at Mission Crest. The staff 
at Mission Crest works diligently to create a climate that instills within it's students that they are capable of growing and being 
successful. 
 
Our mission statement is at the heart of what we do as an educational team. Due to a population growth that was occurring in 
Hesperia, Mission Crest Elementary opened in August of 2007 for students in grades K - 5. During the 2007/2008 school year, the 
school was split onto two campuses until our move to one site in March 2008. Mission Crest opened with 745 students and 33 
classrooms which included two special education classrooms. One unique thing about our campus is that it is the first all indoor campus 
in Hesperia. Our 6th graders joined our campus in March of 2009. We have continued to experience growth adding 5 portables in 
2012/2013. As the district has worked to lower class size in primary grades to a student to teacher ratio of 23:1 additional classrooms 
were needed. In 2016/2017, Mission Crest opened with 6 new permanent portable classrooms. This year, Mission Crest was allotted 
2 more portables totaling 11. The newly added portables created much needed room for additional TK - 6 classrooms, district and 
county support services, as well as programs such as ASAP, Parent Center, and Music room. Currently, Mission Crest has 41 highly 
qualified Certificated teaching staff members, 4 certificated support staff, and 29 non-certificated support staff. Mission Crest has the 
unique distinction of being the largest elementary school in the Hesperia Unified School District. 
 
 
PRINCIPAL'S MESSAGE: 
Welcome to the Mission Crest Elementary School Annual School Accountability Report Card (SARC). This document is designed to 
provide you with valuable information about Mission Crest's instructional programs, academic achievement, classroom materials, 
school safety, facilities and staff. The staff at Mission Crest is committed to continuously improving student programs, student 
achievement as well as meeting the challenge of preparing our students to be college and career ready through the California Content 
Standards and 21st Century Learning. Each year, the Mission Crest staff spends at least one hour a week along with 5 full days out of 
the classroom dedicated to collaboration in order to update or create units of study as well as inspire and challenge students. 
 
Mission Crest Elementary School is dedicated to the belief that learning is a continuous process and students must be provided with 
multiple opportunities in a variety of formats in order to ensure success. Through the Professional Learning Community collaborative 
process, each grade level works together to create an educational environment that accommodates individual learning styles and 
maintain high, yet obtainable expectations for our students. As a product of the PLC collaborative process, our highly trained staff 
implements a well-balanced curriculum that provides students with the skills necessary to travel further down the road in being college 
and career ready. As we continue our journey in implementing the California Content Standards our teachers are working toward 
creating units that at the core have a Big Idea, Essential Questions, and teach the standards through cross-curriculum lessons as well 
as project based learning. With 21st Century Learning as the Core and the 4Cs (Critical Thinking, Collaboration, Communication, and 
Creativity) as the driver, our staff has selected to focus on communication and collaboration. This last year, our staff committed to 
attending Kagan Professional Development. This professional development focuses structures that organize and promote 
communication and collaboration that can be integrated into any lesson. The staff at Mission Crest continues to grow professionally 
in order to provide the best education experience for all students. 
 

2017-18 School Accountability Report Card for Mission Crest Elementary 

Page 2 of 13 

 

The staff has placed an emphasis on integrating computer science into the regular curriculum. Currently, Mission Crest hosts the 
Western Regional Vex Robotics competition as well as participates in Junior FLL Lego Robotics. Moreover, the teachers grades K - 6 at 
Mission Crest are diligent in their efforts to integrate technology into their curriculum. The staff attended trainings on robotics and 
coding during staff meetings and through Code.org and currently is working toward coding and robotic into their curriculum. Further, 
Mission Crest has purchased one to one computer devices giving every student in grades Kindergarten through 6th grade a device to 
assist with learning. Over the last two years, Mission Crest has placed 2nd Overall in the It is our commitment to grow in our 
professional practice to provide our students with current 21st Century teaching practices and learning environment. 
 
Some of our proudest accomplishments include our Chargers Ignite Program (awarding students for 90% completion of homework 
and classwork, 95% attendance and good behavior). We completed the School Wide Positive Behavior and Intervention Support 
(PBIS) program in 2016 which included revising the school’s existing school-wide behavior program. For our efforts in implementing 
the PBIS principles, Mission Crest was recognized by the County of San Bernardino as a PBIS Silver School. As a staff, we collectively 
agreed upon a school theme, Mission Crest a School in Service of Others. It is our goal to integrate service learning as part of our daily 
curriculum and special activities. We continue to diversify our systems of communication through the use of Facebook, Twitter, 
Mission Crest App, school website, regular connect education automated phone calls home, providing a monthly class newsletter or 
updated class website, and communicating weekly with families through "Monday News.” Although we are a TK-6 school with over 
1,000 students, Mission Crest has a warm family atmosphere that caters to the individual needs of its students. Yearly the staff reviews 
and discusses the school’s Mission, Vision and Commitments and works diligently towards achieving them. 
 
SCHOOL THEME: 
Service of Others 
 
MISSION STATEMENT: 
To inspire every student to care, think, achieve, and learn 
 
VISION STATEMENT: 
The Mission Crest Community will work together to ensure success for all students. 
 
COMMITMENTS: 
WE WILL... 
Commit to Continuous Learning 
Maintain high expectations for the Mission Crest Community 
Work collaboratively to integrate core knowledge and promote critical thinking 
Frequently analyze date to achieve growth 
Provide support and enrichment for all students 
 
FOCUS: 
CPR - Communication as a Pathway to Reading 
 
SCHOOL PROFILE: 
Hesperia Unified School District is located in the high desert region of San Bernardino County, approximately 40 miles north of the 
Ontario/San Bernardino valley. More than 20,000 students in grades kindergarten through twelve receive a rigorous, standards-based 
curriculum from dedicated and highly qualified professionals. The district is comprised of 15 elementary schools which includes 3 
choice schools. At the secondary level Hesperia has 3 middle schools, 3 comprehensive high schools, 1 alternative school, 2 
continuation high schools, 1 community day school, and 6 charter schools. 
 
Mission Crest Elementary is located in the northwest region of Hesperia and serves students in grades Transitional kindergarten 
through sixth. The school follows a modified-traditional calendar. Student enrollment has steadily grown since it’s opening in 2008 as 
new neighborhoods and communities emerge in the high desert region. Students are introduced to a rigorous, standards-based 
curriculum by highly qualified staff. All teachers hold credentials to teach non-English speaking students. 
 
 
 

2017-18 School Accountability Report Card for Mission Crest Elementary 

Page 3 of 13