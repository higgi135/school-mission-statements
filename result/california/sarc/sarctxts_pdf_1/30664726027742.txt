San Marino is an elementary school that provides services to students from very diverse backgrounds in grades preschool through 
sixth, including four Special Day Classes (SDC). The San Marino School attendance area covers portions of Buena Park, Anaheim, and 
Cypress. The school staff is committed to providing an exemplary school experience to each of its students. Students at San Marino 
come from socio-economically diverse homes. The school's enrollment is approximately 558 students with 18 regular education 
classrooms in grades PS-6 and four Special Day Classes serving students from preschool to sixth grade. A total of 25 highly qualified 
certificated staff with CLAD certificates and 11 classified staff provide a comprehensive instructional program and related support 
services. The Centralia School District provides centralized funding for textbooks, Special Education, GATE, and English Learners 
programs. District funds employ a principal, an office manager, a part time health clerk, a full time Speech Teacher, a part time school 
psychologist, a full time special education resource teacher, a child development coordinator for child care, and a day and night 
custodian. 
 
San Marino is a school where academic achievement and student conduct is the yardstick by which success is measured. We use data 
to drive our academic decisions with a focus on reading, math, writing and character development. Teachers work collaboratively to 
ensure the success for all students. They engage students in learning by making the curriculum rigorous and relevant by providing 
high quality grade level instruction. San Marino School prides itself on data driven decision making by providing early intervention, 
small reading group instruction based on assessment data from a variety of sources both formal and informal. A Media 
Center/Computer Lab and Reading Learning Center provide additional educational opportunities for our students. Recently, the school 
has added an Innovation Lab. The Innovation Lab provides an additional space to create a 21st learning environment that allows our 
students to apply skills and concepts in Science, Technology, Engineering, Arts, and Mathematics (S.T.E.A.M). 
 
San Marino School, in partnership with the community, is dedicated to producing self-directed lifelong learners who master academic 
skills through a comprehensive curriculum which challenges the learner's creative ideas, talents and interests in an environment of 
cooperative spirit, positive attitude and mutual respect. San Marino is committed to meeting the needs of all students through explicit 
direct instruction including differentiated instruction. Students are provided a comprehensive curriculum in language arts, 
mathematics, history/social science, science, physical education, and health education. Many specialized programs including English 
Learner (EL), Gifted and Talented Education (GATE), Speech and Language, Special Education, Early Intervention Program, and the 
school's Response to Intervention program provides extra support to students. 
 
With input from the School Site Council, ELAC, PTA, and Teacher Leadership Team, the school continues to implement the district's 
model for a comprehensive school that focuses on measurable objectives that capture student achievement while building on an 
analysis of previous year's data. Our curriculum and instructional focus for the next few years will be directed toward the 
implementation of 21st Century learning to help our students become college and career ready through increased skills in critical 
thinking and problem solving, communication, collaboration, creativity and innovation while building character and citizenship. 
 
Mission and Vision Statement 
Our mission is to encourage, inspire, and support students as they develop the skills to be self-directed, life-long learners and 
contributing members of the community within an atmosphere of respect and integrity. 
 
Core Values 
Integrity - Always doing what’s right even when no one is looking 
Excellence - Always striving and persevering to do better 
Teamwork - Working together with the best interest of the whole group in mind 
Leadership - Building qualities that inspire others and leading by example 
Equality - Providing an equitable access of opportunity, resources, content, tools, and respect to ALL 
 

2017-18 School Accountability Report Card for San Marino Elementary School 

Page 2 of 16 

 

Goals/Vision 
 
Comprehensive Curriculum 
San Marino Elementary School prides themselves in providing rigorous, standards-based curriculum for ALL learners. 
 
Engaging Instruction 
San Marino Elementary School seeks and implements engaging and innovative instruction for the 21st Century Learner. 
 
Compassionate School Culture 
San Marino Elementary School promotes and creates productive, compassionate relationships within our school community. 
 
Safe and Secure Environment 
San Marino Elementary provides a safe and secure learning environment in which students can learn and prosper. 
 
Support All Learners 
San Marino Elementary is dedicated to supporting ALL students through equal access to materials and resources. 
 
Unified Cooperative School Community 
San Marino Elementary School seeks the active participation of parents, staff, and local community members to build cooperative 
relationships. 
 
San Marino's Mission and Vision along with it's Core Values are in alignment with the overarching Mission and Vision of Centralia 
School District. San Marino School is committed to meeting the diverse needs of all students and creating lifelong learners by providing 
exceptional staff and opportunities for family and community involvement in a safe and nurturing environment. A School-Wide 
Positive Behavior Intervention System (PBIS) program has long been established at San Marino. The program focuses on the RAMS 
attributes (Respectful, Act Kindly, Make Responsible Choices, and Safety Minded) and the 7 B's - Be on time, Be dressed for success, 
Be respectful, Be responsible, Be safe, Be prepared to learn, and Be scholarly. This program has tremendous staff and parent support, 
and focuses on reinforcing positive behavior and teaching appropriate behavior along with social skills and character education while 
building a safe clean environment for students and staff. Additional support from school personnel and programs include Second Step 
Social Skills program, and a small group Zones program for students providing daily weekly positive reinforcements. Additional positive 
reinforcement is provided weekly for all students through Rewards Recess and a once a month Monday Fun Day. 
 
San Marino is committed to providing quality classroom instruction by highly-trained personnel. We strive to provide equitable, 
student-centered learning opportunities which positively impact our students.We believe in data-driven decision making, professional 
development, and small learning communities. In 2017, San Marino students in 3rd through 6th grades were introduced to and used 
many of AVID’s strategies which were incorporated into daily classroom instruction. This year the school is expanding their adoption 
of AVID and is including all grades Kindergarten through sixth. All students will learn about organization, study skills, communication, 
and self-advocacy using the AVID systematic approach to Writing, Inquiry, Collaboration, Organization and Reading (WICOR). Dr. 
Omaira Lee, Principal at San Marino Elementary, shared, that, “AVID has proven that when given a system of rigorous curriculum and 
strategic support, all students can be academically successful.” 
 
San Marino has also taken on the appearance of AVID culture, both outside and inside the classrooms. These changes include posting 
flags from various universities throughout the campus, wearing university shirts on Fridays, banners announcing that San Marino is an 
AVID school, and a morning chant. Within the classrooms, there are college flags, teachers’ alma mater displays, student research of 
different colleges, Growth Mindset projects, multiple examples of students using academic language and collaboration, and every 
student has an organized binder. All students also attend a field trip to a local college or university where they are able to see what 
college is like this includes are Kinder students who attend KinderCaminata at Cypress College. San Marino is AVID STRONG, AVID 
MINDED, and COLLEGE BOUND! Goooooo RAMS!!! 
 
 

2017-18 School Accountability Report Card for San Marino Elementary School 

Page 3 of 16 

 

Recent School Accomplishments: 
There is no single practice that makes San Marino the educational facility that it is. We have excellent, thoughtful, caring teachers 
who are continually striving to improve their instructional practice. We hold our students, as well as ourselves, to a high level of 
expectation. We continue to refine our practices, knowing that we live in a changing world and quickly adapt to give our students 
every chance to be successful. It is the understanding that we are the best hope for our students future that keeps us motivated and 
makes San Marino an outstanding school and the recipient of the California Gold Ribbon School 2016, California Distinguished School 
Award in 2010 and 2014, California Business for Education Excellence Honor Roll School, 2011, 2012, 2013, 2014, 2015, 2016, 2017 
and Title 1 Academic Achievement Award in 2005, 2006, and 2010, and 2016. Schools receiving this distinction from the California 
Business community have demonstrated consistent high levels of student academic achievement, improvement in achievement levels 
over time and reduction in achievement gaps among student populations. 
 
Awards 
PBIS SIlver Award 2017, 2018 
California Gold Ribbon School 2016 
California Distinguished School, 2010, 2014 
California Business for Education Excellence Honor Roll School, 2011, 2012, 2013, 2014, 2015, 2016 
Title 1 Academic Achievement Award, 2005, 2006, 2010, 2016 
 
San Marino is proud of its excellence in academic achievement and is committed to providing quality instruction. We believe that the 
success of our school directly correlates to increased parent involvement, students who are motivated to learn intrinsically and 
extrinsically, and a dedicated and highly skilled team of certificated and classified staff.