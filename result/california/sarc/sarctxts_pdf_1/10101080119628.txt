Big Picture Educational Academy educates students from kindergarten through adult education via our elementary, middle, high and 
adult classes. We are one of the sixty five Big Picture Learning Schools nationally and serve all students, especially students who may 
not have found success in traditional settings. We implement an interest based curriculum as well as project based learning. Our high 
school allows students to pursue their passions via internships. Our teachers are called advisors because of the bond that is formed 
and teaching children one student at a time. 
 
School Mission 
Big Picture Educational Academy educates individuals of all ages, one student at a time, to understand and engage in the world around 
them, take ownership of their learning, and develop abilities and passions for success in career and life. 
 
School Vision 
BPEA students leave confident in their knowledge, skills, and relationships to pursue unique passions and make lasting positive impact 
on their lives and communities.