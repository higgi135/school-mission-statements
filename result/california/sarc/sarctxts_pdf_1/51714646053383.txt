At Gray Avenue Middle School, all staff members work to motivate and teach responsibility so that our students leave here prepared 
both academically and socially for success in high school and beyond. As a 6th - 8th grade middle school, we understand we have these 
young adolescents for some very important and formative years. Our students are raw with potential and curiosity. The experiences 
they have here will have a life-long impact on their enthusiasm for learning. Middle school students are eager to become independent 
and obsessed with proving themselves and defining their social roles. Without good role models, purposeful guidance and structure, 
this energy and curiosity can easily lead down the wrong path. It is fundamentally our purpose to TEACH Gray Avenue students the 
value of education and to help them be responsible for their learning. We value every minute we have to make an impression on our 
students. The staff at Gray Avenue will do whatever it takes to ensure students have the appropriate academic and social experience. 
 
The purpose of Gray Avenue Middle School is to develop a learning organization where our staff and students acquire the knowledge, 
the attitude, and the skills to equip our students for academic and social success in high school and beyond. To that end we will work 
to: 

• Become an organization of learners 
• Prepare OUR students academically with instructional excellence 
• Motivate and teach responsibility 
• 

Foster relationships and guide social development