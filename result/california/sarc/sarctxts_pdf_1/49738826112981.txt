Lawrence E. Jones Middle School is a diverse community of learners united by the strong conviction that opportunity comes through 
education. We promote learning as discovery, through independent thinking as well as cooperative learning. We strive to model and 
encourage integrity in a positive and respectful environment. We recognize the potential of every student and offer a rigorous 
curriculum that is relevant for today's fast changing world and equips our students for the challenges of tomorrow. We recognize and 
reward perseverance as students begin to take responsibility for their futures. We honor the service of both students and staff, and 
are committed to the physical, social, and emotional well-being of our school community. To our students and to one another, we 
offer support and guidance that is rooted in a profound sense of responsibility for the enormously important work we share. 
 
Mission: Lawrence E Jones Middle school is a diverse community of learners. We recognize the potential of every student and offer a 
rigorous curriculum that is relevant for today’s fast changing world and equips our students for the challenges of tomorrow. 
 
Vision: The LJ Way 
Discovery 
Integrity 
Perseverance 
Service