Rustic Lane Elementary School, the home of the Leopards, guides students to explore the attributes of Respect, Ownership, Attitude, 
and Responsibility as they embark on their learning adventures. Our school is committed to providing learning opportunities that are 
engaging and will assist our students not only with academic success, but also in making responsible decisions when solving problems 
that may confront them in the challenging world they live in. We hold high standards of excellence for ourselves and our students and 
work hard to give every student what he/she needs to succeed. At Rustic Lane, we strive for students to experience a well-rounded 
program that helps them harness their uniqueness and strengths. Our teachers work diligently to create a learning environment that 
is responsive to the differences in our students' academic levels, interests, and learning styles. We provide a safe, positive, and 
supportive school experience so that all students feel valued and encouraged to achieve their personal best. 
 
Teachers meet regularly to discuss effective teaching strategies, and analyze data from Common Core aligned assessments to inform 
their planning. Within that collaboration time, teachers organize curriculum and learning experiences to guide students in developing 
a deep understanding of the Common Core Standards. The goal is for students to be engaged in learning experiences that prepare 
them for their futures; developing skills along the way that guide them to be college and career ready. Problem solving, critical thinking, 
analysis, and technology are regular components of the high-level expectations that teachers build into the daily instructional 
practices. 
 
Besides the regular instructional program, various other programs exist to challenge and support our students. Rustic Lane has a 
variety of extended learning opportunities (Saturday School/Family Nights, etc.), Think Together, 100 Mile Club, GATE, Primary 
Intervention/Early Literacy programs, and Guided Reading, to name a few. 
 
It takes true partnership to make Rustic Lane a success, and with everyone working together, our students can and will achieve their 
dreams. Rustic Lane students will become collaborative, reflective, and thoughtful citizens contributing great things to society. Our 
world needs individuals that can think critically, communicate clearly, and participate responsibly. Within our community, we can 
guide and support students to achieve and dream big! 
 
Mission/Vision Statement 
We are committed to establishing a school culture which recognizes no limitations in the achievement and success of each student. 
 
Students will develop skills to ensure College & Career Readiness; engage academically and socially in a safe, orderly, and inviting 
environment; and our families will be encouraged to be active participants in their students' education. 
 
Collective Commitments (Values) 
In order to achieve the vision of our school, the Rustic Lane Elementary staff has made the following collective commitments: 

• Practice civility, collegiality, & professionalism in all levels of communication 
• Utilize District Developed Units of Study to provide strategic standards-based instruction 
• 

Implement and analyze the results of State, District, and Grade Level assessments to inform best practices and meet 
student needs 

• Utilize identified instructional strategies, including AVID, PBIS, healthy living initiatives, and technology supports to 

promote success for all students 

• Provide parents and students with resources, strategies, and information to help students succeed 

 

2017-18 School Accountability Report Card for Rustic Lane Elementary 

Page 2 of 11