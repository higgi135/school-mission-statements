William T. Newland Elementary is one of seven elementary schools in the Fountain Valley School District, located in Orange County. 
Currently, the school serves the needs of about 500+ students ranging from Transitional Kindergarten to fifth grade. 
 
The mission of Newland Elementary School is to provide an educational environment in which academic excellence is expected and 
all children are encouraged to develop their maximum potential through a positive attitude toward self and others, a love of learning, 
an appreciation for diversity, and the cultivation of the ability to be a productive, useful member of society. The focus is on the unique 
needs of elementary school students as they transition through elementary to middle school. 
 
Newland’s focus on academic achievement requires us to continually adapt to the ever-changing needs of our student population. 
Moving into an age of California State Standards and Smarter Balanced Assessment Consortium (SBAC), Newland students continue 
to demonstrate proficiency at a high level with students. Newland also provides students an enriched instructional program. We are 
very fortunate to be able to offer choir, orchestra, and band programs along with several music-focused assemblies throughout the 
year. Additionally, we implement a wide-ranging art program throughout the school. Newland is committed to educating the whole 
child and fostering a sense of belonging to the school community. It is our mission to propel our Newland Dolphins to their greatest 
learning and social growth potential.