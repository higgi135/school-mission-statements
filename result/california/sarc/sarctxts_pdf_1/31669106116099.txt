Catheryn Gates Elementary School opened in the Fall of 1999 on a temporary campus serving K-6 grade students. Our permanent 
campus officially opened its doors in the fall of 2000. We currently serve 560 children in grades Kindergarten through 5th grade. Our 
school is named in honor of Miss Catheryn Gates, a long-time teacher in Roseville City School District. Our school also hosts the 
district’s Accelerated Pacing Academy for 5th grade. 
 
Our school vision is to provide success in all directions for every child with: 

• High standards and accountability; 
• Academic and character development; and 
• 
School, family and community involvement. 

Our teaching practices at Catheryn Gates Elementary School reflect a balanced curriculum devoted to reading, writing, oral language, 
literature, math, science, and physical education per implementation of the current California State Standards. District instructional 
standards support these high expectations. Ongoing assessments help students; parents and teachers monitor success and adjust 
instruction as needed. 
 
Providing an outstanding education and learning environment for all our students is the mission of our school. It is our firm belief that 
in order for us to be successful in our endeavor, we must be in partnership with our students, parents and community. We look forward 
to building positive and productive relationships with our students. 
 
We will: 

• Differentiate instruction to meet each child’s needs 
• Communicate with students’ families (classroom and whole school) 
• Adhere to and progress monitor our district aligned school goals 
• Continue to work in PLCs and ask the questions, “What do we do when students aren’t learning?” and 

 “What do we do when they are learning?” 

• Have positive attitudes and be problem solvers 
• Have a student-centered mindset 
• Be committed to student achievement, collaboration and providing intervention as needed, including analyzing data and 

progress monitoring.