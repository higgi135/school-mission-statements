Rossmoor Elementary is proud to be a three-time National Blue Ribbon, seven-time California Distinguished, as well as a Gold Ribbon 
School. What makes Rossmoor so special is our community which is comprised of our dedicated staff, supportive parents, and 
community members that work harmoniously to nurture the growth of our students. Together we provide a strong foundation to 
support the academic, social and emotional well-being of our students so that they can grow and develop the necessary skills needed 
to be successful in our 21st century global society. Our students are continuously exposed to classroom practices and opportunities 
around critical thinking, communication, collaboration, creativity and innovation. 
 
Our school goals that supports our mission are determined by a variety of factors. This includes our District Priority Goals, reviewing 
student progress through a variety of assessments (state mandated assessments, district benchmarks, and our Fountas and Pinnell 
reading assessments. We ensure our teachers continuously receive outstanding professional development to support the following 
District Signature Practices: 
-We have a balanced literacy program- Reading Foundations of the Common Core (RFCC), language wall, integrating phonics and 
decoding, in order to meet our students needs and develop reading comprehension. Our teachers utilize Junior Great Books, Reading 
and Writing Workshop, and Thinking Maps. 
- All our teachers are utilizing Cognitively Guided Instruction (CGI) strategies for teaching number sense and conceptual math and to 
develop strong problem solving skills. 
- Another way we support our students growth and inquiry is through Science Technology Engineering and Mathematics through our 
STEM Lab where they receive instruction supported by NGSS. They are also exposed to technology instruction to prepare them to 
succeed in our 21st century learning. Our TK-1st grade have access to iPads, and our 2nd -5th grades have 1:1 Chromebooks. 
 
Here at Rossmoor Elementary there are so many opportunities for magic and dreams to come true daily! The dedicated staff believes 
that ALL children can learn and succeed. Our teachers are well-versed in our district signature practices and provide our Knights with 
rigorous academic experiences that help them grow, be challenged and discover their individual strengths. As you enter our campus 
one can see and feel the magic that takes place. Our Knights are eager to learn, excited to be challenged, and regularly set goals that 
they work to accomplish!