King-Chávez Athletics Academy (KCAA) is a charter school located in southeast San Diego. Our mission is to "Seek excellence in 
Academics, the Arts and Athletics from the Foundation of Love." We create a warm and caring environment in which students actively 
learn academic content as well as positive values. In keeping with our philosophy of educating the whole child in mind, body, and 
spirit, we are also proud to provide over 200 minutes of physical education to our students every week. 
 
Athletics Academy students have access to a high-quality, rigorous, and engaging educational program. Our curriculum materials, 
instructional strategies, professional development, and student assessments are aligned with the Common Core Standards. All 
students are taught by credentialed teachers who approach their work with integrity and passion. We offer increased learning time 
after school, during our two weeks of intercession, and in the summer.