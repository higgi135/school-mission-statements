School Description 
Buena Terra School is located in the city of Buena Park, California and is part of the Centralia School District. Buena Terra serves 
approximately 580 students in grades K-6 with approximately 16% of these students designated as limited English proficient and 35% 
socio-economically disadvantage. Students are provided a comprehensive curriculum including language arts, mathematics, 
history/social science, science, visual and performing arts, physical education and health. We believe in providing students with a 
comprehensive education that prepares them with 21st Century skills. Buena Terra’s Science, Technology, Engineering, Arts and Math 
(STEAM) focus has integrated skills such as computer science coding into the curriculum. Buena Terra opened its innovation lab, “The 
Launch Pad.” The Launch Pad is a place where students will explore and engineer solutions that will enhance society. Students will 
learn and apply the engineering design process to solve real world problems. The Launch Pad will also serve as a Makerspace. A 
space for students to explore and discover new ideas. A space for students to enhance their creativity. Our Makerspace will provide 
students with a place where they can create something that aligns with their passion, a place where students can unleash the genius 
within them. Our learning center provide additional educational opportunities for our students. Classrooms are equipped with 
integrated technology systems to engage students in learning. Buena Terra is committed to meeting the needs of all students through 
explicit direct instruction and differentiated instruction. With input from the School Site Council, the staff continues to implement the 
District's model for a Comprehensive School Based Coordinated Plan that focuses on measurable objectives for program improvement 
and student achievement while building on an analysis of the previous year's data. Our curriculum and instructional focus for the next 
few years will be directed toward the implementation of the new California Common Core Standards. Buena Terra is a proud recipient 
of the California Business for Education Excellence STEM Honor Roll School. 
 
At Buena Terra, we provide a warm, welcoming culture for students, teachers, parents, and community. Parents are encouraged to 
volunteer in the school, are invited to Principal Chats, and are kept up to date with weekly messages. Buena Terra has implemented 
the PBIS program for several years now and are committed to providing a multi-tiered framework that supports a rich learning 
environment for students. This structure allows for the input from all of our stakeholders, especially students, to create a safe, 
nurturing school environment for all. In 2017, Buena Terra received a Silver ranking from the California PBIS Coalition's System of 
Recognition. Parents and community members feel a part of the school and we have been able to expand events and services provided 
to the Buena Terra family. Our PTA has won numerous City awards for participation in City events. We are proud of our active 
participation by parents with large numbers of parents volunteering in the classrooms. 
 
Mission Statement 
The mission of Buena Terra School is to create a safe environment in which all students attain high academic achievement. All students 
will receive a strong foundation of knowledge and skills to ensure future success.