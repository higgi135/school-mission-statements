Description of District 
The Visalia Unified School District is the oldest school district in Tulare County. The district is comprised of 25 elementary schools, 5 
middle schools, 4 comprehensive high schools, 1 continuation high school, 5 charter schools, and 1 adult school. Over 32,000 students 
Pre-K to adult are served through the Visalia Unified School District. 
 
Description of School 
Visalia Charter Independent Study (VCIS) serves approximately 600 students in grades 9-12 in 2018-19. VCIS offers both a college style 
learning schedule or an online learning option. Our teachers and staff are dedicated to ensuring the academic success of every student 
and providing a safe and productive learning experience. VCIS has 50 part-time teachers and 14 full-time teachers, and another 20 
individuals to support our school. The school holds high expectations for the academic and social development of all students. VCIS 
is an accredited dependent charter of VUSD and our curriculum is UC approved. VCIS is a hybrid learning independent study charter 
high school which means students complete courses independently with master teacher support as well as attend courses that require 
more instruction such as math and science. We are leading the way with technology instruction by offering C+ programming, a culinary 
program, full performing arts, and much more. Curriculum planning, staff development, and assessment activities are focused on 
assisting and measuring student growth and intervention needs for all groups. We offer many career exploration classes to connect 
both college and career options for all students. These classes have built another level of confidence and success for students; such 
as performing arts, culinary, science and engineering, and more. We had the pleasure to have 230 graduates last year. We also 
received the best practices grant to share our successful, creative, and rigorous learning model with the entire state of California. 
 
Vision Statement: 
Visalia Charter Independent Study is committed to providing an education that will focus on individual student needs, fostering their 
academic potential and inspiring a passion for learning, while guiding students towards success in college, career, and life 
 
Mission Statement: 
Visalia Charter Independent Study has one mission: to offer students an innovative approach to academics that recognizes the unique 
and varied needs of each student. Our goal is to create a learning environment that takes into account individual needs by working at 
a pace that fosters academic success. As partners in this journey, parents, school staff, and the community come together to ensure 
that students are achieving their potential as they become learners in the 21st century. VCIS pledges to instill confidence, diligence, 
and self-worth for all students.