The mission of Plumas Charter School (PCS) is to provide a personalized learning environment with a culture of acceptance for a diverse 
community. We encourage the development of compassionate, resilient, life-long learners. We embrace the core values of Respect, 
Accountability, Responsiveness and Compassion. 
 
Plumas Charter School offers a variety of programs, curriculum and instruction including a selection of state-approved curriculum, 
resource center based classes, tutoring, online courses, literature and original sources; character education and customized 
combinations of the above. A careful selection is made for each student taking into consideration student's strengths, growth areas, 
optimum learning style and personality. Plumas Charter School operates resource centers in four Plumas County communities (Quincy, 
Greenville, Taylorsville and Chester). Each center is designed to meet the specific needs of the community. 
 
Plumas Charter School was founded in 1998, and first became WASC accredited in 2008. The original enrollment was 70 students and 
is now 340. The goal of PCS is to support students in obtaining the skills necessary to succeed in a rapidly changing world.