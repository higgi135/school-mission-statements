Douglas J. Miller Elementary School opened in 2009. As the newest addition in the Panama Buena Vista Union School District, we 
proudly work to further "Excellence in Education." Located in southwest Bakersfield, CA- Miller Elementary is home to over 800 
students Transitional Kindergarten through Sixth grade. Our namesake is more than a namesake. Douglas J.Miller, a former 
Superintendent of the Panama Buena Vista Union School District, built a legacy on investing in our most precious resources- children. 
Even today, Doug Miller, along with his wife Martha, continue to invest in children through their active involvement at school. 
 
Mission: Together, we selflessly strive to Breakthrough, Inspire, and Grow. Value: Miller is Family! Committed to the success of ALL 
Mustangs! Vision: Preparing every child, every year, for the next grade level and beyond! 
We strive to teach students Respect, Responsibility, and Safety. As "Miller Mustangs," we are committed to "Leading Through 
Reading" and developing life - time learners.