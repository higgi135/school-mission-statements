Ridgecrest Charter School (RCS), a Kindergarten through 8th Grade California Public Charter School, is located along US Route 395 in 
the Indian Wells Valley in northeastern in Kern County, California. The city of Ridgecrest is the population center of the Indian Wells 
Valley with an estimated population of 28,000 residents According to the California Employment Development Department, the U.S. 
Navy and its contracts and the hospital are Ridgecrest’s two largest employers. 
 
RCS began educating children in September 2001. Enrollment data submitted to CALPADS and reported on the Ed-Data website show 
RCS’s enrollment has steadily increased since 2008. RCS enrollment grew from 262 students during the 2008-09 school year to 475 
students during the 2016-17 school year. Enrollment reported to CALPADS in October 2018 was 477 students for the 2018-19 school 
year. RCS strives to maintain a 24:1 student-teacher ratio in kindergarten through third grade, 30:1 ratio in grades four and five and 
32:1 in grade six through eight. Math and English-Language Arts classes for 6th through 8th grade students are smaller with a student-
teacher ratio of approximately 20:1. 
 
RCS is a small school with approximately 64 students per grade level in grades TK-3, and 62 students per grade level in grades 4 and 5, 
and 40 students per grade level in grades 6-8. Our small classes are designed to provide additional opportunities for student academic 
and social-emotional development. Students are more likely to succeed in a small school, where teachers and administration connect 
with each family and students’ needs are met. In smaller classes, teachers can adapt their instructional practices and provide more 
individual attention to each students’ academic and social-emotional needs. 
 
As a school of choice, the RCS vision is to create a growth- minded school by providing programs otherwise unavailable in our 
community where all students are empowered to reach their full academic and social-emotional potential. The RCS mission is to 
pursue programs and innovative resources to create a unique and successful learning environment for all students. 
 
Ridgecrest Charter School is committed to providing a successful, safe, and rigorous educational experience aligned to the State 
Standards, while promoting the joy, dedication to, and importance of learning for all of our students. Ridgecrest Charter School 
welcomes California students legally attending a California public school in grades TK– 8. Our educational program is committed to 
academic, social, civic, and character development. 
 
RCS’s educational program is based on the instructional needs of our target student profile. RCS targets the following students: 

• 
• 

• 
• 
• 

Students who are not currently successful in their current core academic subjects; 
Students with special needs who require a small class setting and their academic, emotional and behavioral needs are not 
being met in a traditional school setting; 
Students whose academic or English learning needs necessitate a small school environment with personalized attention; 
Students whose academic or English language learning needs are not being met in a traditional school environment; and 
Students with diversity backgrounds. 

We believe one size does not fit all in education, and RCS is dedicated to providing students and families throughout the Ridgecrest 
area with a small school option that can meet their unique needs. 
 
Ridgecrest Charter School’s core beliefs, focus areas and tactics are: 
 
Core Beliefs: SPACE 
 
Safety: All children can learn in a safe and supportive environment 
 
Partnership: Education is a partnership amongst students, educators, parents, and the 
 

2017-18 School Accountability Report Card for Ridgecrest Charter School 

Page 2 of 14 

 

Development of 21st Century Skills 

 community 
 
Accountability: Life is about choices, personal responsibility and personal accountability 
 
Communication: is the foundation for relationships, progress and success 
 
Equality: Every person should be treated with dignity and respect 
 
Four Focus Areas and Tactics 
 
1. Foundational Education 
 
Every student will have access to quality teachers, curriculum, and enrichment so that each student can demonstrate academic growth 
through multiple measures. 
 
We will hire and develop qualified, credentialed teachers and support staff 
 
We will set high expectations for our students, teachers, and support staff to support the growth of each of our students. 
 
We will provide a well-rounded education focused on Science, Technology, Engineering, Arts, and Math (“STEAM”) 
 
We will provide personalized learning through Individual Learning Plans for each of our students 
 
We will provide extended learning opportunities and academic support through programs including GATE, technology, events, 
tutoring, reading intervention, English Learner (“EL”) support, and clubs. 
 
We will teach and develop Executive Functioning skills with our upper elementary and middle school students 
 
2. 
 
Every student will have opportunities to develop skills necessary to compete in a global society. 
 
We will develop soft skills- critical thinking, communication, collaboration, and creativity- with our students through the general 
curriculum, STEAM education including elementary music, financial literacy, and Design Thinking principles 
 
We will invest in technology for students’ and teachers’ use 
 
3. Safe and Supportive Learning Environments 
 
Every student will be supported in a nurturing and safe environment. 
 
We will invest in the physical security and functional safety in and around our campus 
 
We will maintain our existing facilities and will substantially invest in facility improvements and additions 
 
We will develop our students’ social skills through Positive Behavior Interventions and Supports (“PBIS”), Character Counts, Second 
Step, and counseling 
 
We will teach and practice personal responsibility habits with our students. 
 
We will provide small learning environments to support personalized learning. 
 
4. 
 
Partnerships are integral to the success of every student and the success of the school community. 
 
We will partner with our families and school community to support our extracurricular and enrichment activities 
 

Family and Community Partnerships 

2017-18 School Accountability Report Card for Ridgecrest Charter School 

Page 3 of 14 

 

We will use multiple avenues such as our website, messaging system, on-line gradebook, newsletters, and social media to regularly 
communicate with our parents and community 
 
We will strengthen our school, family, and community partnerships through events such as Back to School Night, Open House, math 
night, movie night, STEAM night, and other family events. 
 
We will provide opportunities for families and community members to participate in school decisions such as the LCAP, safety and 
security planning, facilities plan, and English Learner plan. 
 
We will partner with the community to create measurable school goals presented in the LCAP found on the school website. 
 
The educational program includes a multi-instructional strategies approach that meet the personal learning needs of our students. 
Teachers utilize explicit direct instruction and project based learning using the design thinking process to teach and reinforce content 
and skills. We provide additional instructional support through in-school reading intervention, in-school small group instruction, in-
school RTI, and after school tutoring. 
 
Our instructional program and curriculum is structured to provide learning opportunities that create literate, self-reliant, and confident 
learners. Age and developmentally appropriate homework is assigned to reinforce learning. RCS’s dress code was designed to promote 
equality and learning. The dress code is fully explained in the Parent-Student Handbook. Our instructional program and curriculum is 
designed to cultivate student academic and social-emotional growth and development. Special Education students’ growth 
expectations are reflected in each of their Individualized Education Programs.