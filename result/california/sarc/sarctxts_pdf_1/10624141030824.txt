Sanger Community Day School mission 
is to prepare student to earn a diploma and achieve their goals beyond 
graduation. Our instructional vision is "Sanger Community Day School Mavericks will provide rigorous expectations through a variety 
of engaging lessons and activities utilizing accountability and rewards." 
 
The Community Day School (CDS) is the only school of its kind in the Sanger Unified School District. The school lies in the central section 
of the downtown area of the City of Sanger. It operates on a traditional school calendar. On a daily basis, there are around 49 students 
enrolled at the school. During the 2018-2019 school year, the school enrolled and served approximately 120 students in grades 7 
through 10. 100%of these students participated in the Free or Reduced-Priced Lunch Program. Approximately 6% of the students are 
considered English Learners and approximately 8% are SPED students. 
 
The school has a unique support structure. CDS is a Sanger Unified School, yet operates with support from the City of Sanger. The City 
of Sanger owns the building where we operate our school. We rent space at the Sanger Youth Center. We also share the building with 
the Boys and Girls Club, Sanger Boxing, and Sanger Zumba.