PRINCIPAL'S MESSAGE 
I'd like to welcome you to Kingston Elementary's Annual School Accountability Report Card. In accordance with Proposition 98, every 
school in California is required to issue an annual School Accountability Report Card that fulfills state and federal disclosure 
requirements. Parents will find valuable information about our academic achievement, professional staff, curriculum programs, 
instructional materials, safety procedures, classroom environment, and condition of facilities. 
 
Kingston Elementary provides a warm, stimulating environment where students are actively involved in learning academics, as well 
as, positive values. Students receive a standards-based, challenging curriculum by a dedicated professional staff. In addition, the 
individual needs of the students are addressed throughout the day. Ongoing evaluation of student progress and achievement helps 
us refine the instructional program so students can achieve academic proficiency. 
 
We have made a commitment to provide the best educational program possible for Kingston Elementary's students, and welcome any 
suggestions or questions you may have about the information contained in this report or about the school. Together through our hard 
work, our students will be challenged to reach their maximum potential. 
 
SCHOOL MISSION STATEMENT 
At Kingston Elementary School, we will work as a team to make a positive difference in the lives of children by providing a rich academic 
and social foundation. Our quest is to inspire students to achieve their goals in order to meet today's challenges and future 
expectations. Kingston's Mission Statement is "No Less Than Our Best." 
 
SCHOOL PROFILE 
Hesperia Unified School District is located in the high desert region of San Bernardino County, approximately 40 miles north of the 
Ontario/San Bernardino valley. More than 20,000 students in grades transisitional kindergarten through twelve receive a rigorous, 
standards-based curriculum from dedicated and highly qualified professionals. The district is comprised of 15 elementary schools 
which includes 3 choice schools. At the secondary level Hesperia has 3 middle schools, 3 comprehensive high schools, 1 alternative 
school, 2 continuation high schools, 1 community day school, and 6 charter schools. 
 
Kingston Elementary is located in the southern area of Hesperia and serves students in grades transisitional kindergarten through six. 
Kingston's has a current enrollment of 685 students. 11% special education students, 17% English learner support and 80% qualifying 
to receive free or reduced meals.