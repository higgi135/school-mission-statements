School Description 
Joe Walker Middle School is located in the southwest portion of the Antelope Valley at the intersection of 57th Street West and Avenue 
L-8. Our enrollment is 1059 students in grades six through eight. Students from Valley View Elementary, Quartz Hill Elementary and 
Sundown Elementary feed into Joe Walker Middle School. 
 
The school includes 3 main buildings composed of 11 classrooms each, a shop/homemaking building, gymnasium, multipurpose 
cafeteria with band room and stage, and boys and girls locker rooms. There are also 13 permanent re-locatable classrooms. Included 
on campus are specialized rooms to accommodate library, computer lab, and Hi Tech Lab. The school also includes a concrete play 
area with equipment, grass athletic fields,quarter mile all weather track and outdoor picnic tables. 
 
Mission Statement 
Joe Walker Middle School provides a safe and positive environment which promotes academic success in Science, Technology, 
Engineering, And Learning Through Humanities. Through the collaborative efforts of school staff, students, parents, and community 
partners, STEALTH education creates critical thinkers, increases twenty-first century literacy, inspires creativity and develops the next 
generation of innovators to improve the world in which we live. At Joe Walker Middle School, student achievement is measured by a 
balanced assessment system using formal assessments as well as project based learning. 
 
Vision Statement 
At the 1972 dedication of Joe Walker Middle School, then-member of the Westside Union School District School Board, James Skalicky 
stated, “We all want the best education possible for our children; an education that creates enthusiasm and pride in one’s 
accomplishments; one that stimulates the inner self for a desire of understanding people, cultures, society, and education itself.” To 
fulfill this vision, Joe Walker Middle School creates a culture of success by partnering with community organizations to promote 
instructional relevance in our educational program. In order to best prepare our students for success with twenty-first century skills, 
we provide a balanced curriculum that educates the whole child combining science, technology, engineering, and mathematics with 
the arts and humanities. Every child will receive a STEALTH education at Joe Walker Middle School.