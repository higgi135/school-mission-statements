Mt. Diablo Elementary School is a learning community that strives to educate the mind and encourage the heart and spirit of every 
individual. 
 
VISION: 
 
The members of the school community provide a safe, nurturing, positive learning environment that includes a spirit of trust, 
development of character, mutual 
respect, and cooperation. High professional and educational standards, along w ith effective instructional practices employed and 
articulated throughout the grade levels, help all students to be academically successful, w ell-rounded, and responsible citizens. 
 
At Mt. Diablo Elementary: 
 
Teachers help in establishing a positive climate through their caring, dedication, and high academic and behavioral expectations for 
students. 
 
Classroom teachers and support staff believe that all students can succeed, and they provide assistance to help every student become 
a successful, lifelong learner. 
 
There is an exceptionally high level of parent cooperation, involvement, and support in classrooms, library, computer lab, and other 
areas of the school. 
 
An active Parent Faculty Club supports the instructional program in many ways, including providing materials and equipment to 
improve and enrich the program for students. 
 
There is a strong commitment to establishing and maintaining effective communication between home and school, and a willingness 
to work cooperatively for the benefit of all children. 
 
The Clayton Community School on campus provides child-care and preschool services. 
 
Close cooperation and support exists between the school and the City of Clayton, which has provided financial and personnel support 
to assist students and improve the school facility. 
 
The integration of technology to support instruction and learning is maintained through the use of computer lab with the staffing of a 
part-time instructional assistant. 
 
 
 

2017-18 School Accountability Report Card for Mt. Diablo Elementary 

Page 2 of 11