Walter White Elementary School is located in the heart of downtown Ceres, California. The school campus is home to approximately 
600 kindergarten to 6th grade students and 50 preschool students. The school community is diverse in culture, linguistics and 
economic status. 
 
School Vision Statement 
Walter White will collaborate to provide a positive environment that empowers students to achieve their lifelong goals. 
 
School Mission 
High levels of learning for all. 
 
Our mission, at Walter White Elementary School is to continue to promote high level learning for all through, a balanced curriculum, 
collaboration among staff and community, and academic growth within a safe learning environment. 
 
Walter White School enrollment changes on a regular basis. We welcome new students nearly every week and encourage them to 
become important members of the school family. We have a focus of grade level collaboration in the areas of language arts and math; 
with a special focus on the English Learner subgroup. As well as school culture including positive behavior support systems and 
reducing rates of suspension.