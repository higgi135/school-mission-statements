Description of District 
In 2017-18 Lindsay Unified School District (LUSD) was comprised of six elementary schools, one comprehensive high school, and three 
alternative education schools. During the 2017-18 school year, the district served approximately 4,111 students (source: CALPADS) in 
grades K-12. The LUSD Mission Statement is “Empowering and Motivating for Today and Tomorrow.” 
 
Description of School 
Washington Elementary Learning Community served approximately 699 learners in grades K-8 in 2017-18. Our learning facilitators 
(teachers) and staff are dedicated to providing a positive learning environment and student-centered learning experience for all 
students. In addition to the Washington Elementary core academic program, a wide range of support services are available to learners. 
All programs, practices, interventions, and supplemental activities are focused on ensuring student academic achievement and 
personal success.