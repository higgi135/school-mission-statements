Our Vision of Excellence states that student success can be achieved when a safe and nurturing learning environment is joined with 
the collaborative efforts of staff and community to assist all learners as they pursue their academic and personal goals. In this 
environment, students will develop an appreciation for life long learning and the unique character of our diverse culture. The McAuliffe 
Vision of Excellence, EAGLE PRIDE, addresses five essential areas: P - Professional Learning Community R - Respect for All I - Instruction 
D - Differentiation E - Expectations. Professional Learning Community: McAuliffe Middle School is a professional learning community 
dedicated to the success of ALL students. Through collaborative efforts staff, students, and parents are committed to providing 
rigorous and diverse educational programs where ALL students have the opportunity to experience academic success and develop the 
habits of strong character. Respect for All: The McAuliffe school environment is safe, positive, and caring to support the uniqueness 
of the middle level student. Students are respected and valued by the school community and in turn, the students demonstrate 
responsibility, honesty, cultural sensitivity, and respect for All. 
 
Instruction: All students are engaged in a rigorous and challenging standards-based curriculum that is also responsive to student 
interests and needs. Strong exploratory and enrichment programs encourage students to make informed choices that compliment the 
core academic curriculum. Differentiation: Instruction is differentiated to meet the learning needs of students with a wide range of 
abilities allowing them to build upon their personal strengths. The school community understands and appreciates the diversity of our 
students and their talents and their personal and cultural traditions. Expectations: High expectations are set for ALL students to 
maximize their potential. ALL students have the ability to learn and the entire school staff and community are committed to helping 
them achieve their goals. Mission Statement: McAuliffe has a long tradition of providing students with a rigorous academic curriculum 
delivered in a caring, nurturing environment. Our mission statement underscores that commitment: The mission for McAuliffe is to 
educate all students intellectually, socially, and physically in a safe environment, to produce citizens with an appreciation and respect 
for learning and democratic ideals, and to cultivate citizens who will celebrate the diversity of all cultures. McAuliffe staff, students, 
and parents view the attainment of this goal as a shared responsibility. 
To the attainment of that goal, we share the following goals for the current school year. FOCUS - McAuliffe staff will continue to utilize 
several signature practices including EDI (Explicit Direct Instruction), Thinking Maps, Depth/Complexity/Rigor, research based active 
engagement and CFU (checking for understanding) strategies,Close reading, Acer Writing strategies and a research based academic 
vocabulary program in every content area. Staff will continue to participate in instructional networking, weekly PLC meetings and 
increasing the use of technology to support classroom learning. 
 
The Language Arts and Mathematics departments will continue to create and pilot common core units in all grade levels. All school 
staff will have the opportunity to develop interdisciplinaryPBL (Project Based Learning) activities. Our SST (Student Study team) process 
has been revised with the goal of further inclusion for all special needs students. In addition staff will continue to participate in and 
support McAuliffe's multi-layered intervention program. This includes a 6th grade language arts literacy core, Math Academy classes 
held on Saturday mornings, Principal's Academy on Saturday mornings and Homework Club held Monday-Thursday after school in the 
media center. In addition teachers will support the PAL/PASS program, a targeted intervention program designed to assist all at risk 
students in reaching proficiency by offering additional instruction and time to complete assignments at lunch and on Saturday 
mornings. Lastly staff will "adopt" students, serving as Teacher Advocates for our most at risk students, connecting with both students 
and parents to work as a team to ensure student success. 
 
 GROW - Staff will further their instructional expertise and prepare for the common core by participating in school site, district and 
county staff development opportunities. These include research based district trainings in depth and complexity (Kaplan, Grubb), close 
reading trainings across content areas (Jago, District led trainings by TOSAs), academic vocabulary and active engagement (Marzano), 
use of ipads/chromebooks in the classroom (district), and instructional networking (school site, district). In addition staff will continue 
to promote literacy in all content areas by implementing the ACER writing strategies in Language Arts, Math, Science and Social 
Science; by focuing on annotating text and using interactive academic notebooks; and by participating in one or more school 
wide interdisciplinary activities. 

2017-18 School Accountability Report Card for McAuliffe Middle School 

Page 2 of 10 

 

EXPLORE/EXPERIMENT - All teachers will complete the SBAC practice tests in English/Language Arts and Mathematics along with 
district benchmarks, and will implement new common core units. Science teachers will begin writing and piloting NGSS units. Teachers 
will attend a variety of trainings regarding how to use technology in the classroom to enhance student learning in reading, language 
arts, math, science and social science. INSPIRE - McAuliffe staff recognizes the unique needs of the middle school students, and to 
best serve this population, recognize that connecting with students is key to both academic and social/emotional growth. Staff will 
inspire students by modelling daily mindfulness, joy, kindness and compassion. They will support students, parents and their 
colleagues by letting them know that they will do anything to assist them, and they will "let their eyes light up" when they interact 
with all stakeholders. All students will be offered a wide variety of community service activities through clubs, ASB, PTA sponsored 
events and outside organizations who ask to partner with the school. These activities will inspire students to help others and to become 
both active and caring members of the community at large.