Del Oro High School is located in Loomis, CA and is one of six high schools in the Placer Union High School District. Opened in 1959 on 
approximately 56 acres, Del Oro serves students in grades 9 - 12. Average enrollment is 1725 students. Del Oro serves a semi-rural 
community trying to maintain slow growth while being surrounded by communities of high growth. There has been a phasing out of 
the agricultural way of life and an increase of homes on acreage and small housing tracts within our attendance boundaries. The area 
is becoming a bedroom community for the greater Sacramento area. There continues to be slow growth of businesses within the 
community, but the existing businesses are generally very supportive of Del Oro. Del Oro High School’s teachers, counselors, and 
administrators are committed to providing an education program that instills in all students the value of education as a lifelong process 
and provides opportunities for growth and development. It operates on the fundamental assumption that all students can succeed 
and are capable of achieving excellence. 
 
Del Oro students will master the essential skills to be: 

Effective Communicators 
Life-long Learners 

• Critical Thinkers 
• 
• 
• Responsible Citizens 
• Healthy Individuals 

Mission Statement: 
Del Oro High School’s mission is to continue a tradition of excellence and integrity in a safe, supportive, and respectful environment 
where all students can engage in the learning process, apply knowledge, and contribute as responsible citizens of their local and global 
communities.