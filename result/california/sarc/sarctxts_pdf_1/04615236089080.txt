Palermo Union School District is comprised of six schools: Helen M. Wilcox Elementary School (K-3), Honcut Elementary (K-2), Golden 
Hills Elementary (4-5), Palermo Middle School (6-8), and Palermo Community Day School (K-8). The district is located in Butte County, 
in the northern part of California's Sacramento Valley. 
 
Located twenty-two miles south of Oroville, Honcut Elementary is a one-room schoolhouse. The enrollment is approximately 15 
students (K-3). Honcut provides a Title 1 school-wide program. We have a full-day Kindergarten program. We also have the following 
programs: Indian Education and English Learner. The students also have access to Chromebooks and a library. Palermo Union School 
District implements a Professional Learning Community (PLC) model. The teaching staff and administrators have developed 
differentiated instructional strategies to teach students at all abilities. We have implemented a Response to Intervention (RTI), focus 
groups, and student tutoring within school hours. Teachers and administrators are implementing Explicit Direct Instruction (EDI) based 
on student engagement. 
 
School goal-all students will reach high standards, attaining met or exceeded the standard in reading and mathematics by 2018-2019. 
Progress indicators will be from local district benchmarks, DIBELs, local assessments formative and summative, and are monitored 
weekly, at each trimester and at the end of the school year. 
 
The mission of Honcut and the Palermo Union School District is to provide a variety of educational programs, in a safe and mutually 
respectful environment that is effective, accessible, and equitable; prepare students for leadership, employment, and citizenship; and 
promote students' intellectual, ethical, cultural, emotional, moral, social, and physical growth. We will maintain a safe, caring, moral, 
drug-free, and supportive environment, with the ultimate goal of students becoming successful, productive and responsible citizens