Learning for all; Whatever it takes. We focus on student learning and achievement with the idea that all students can learn. We 
commit to improving the culture of our school through strong academics and behavioral expectations and supports. Our focus is to 
provide an equal learning opportunity to all students and focus on providing additional services where applicable. 
 
Our student enrollment, reported on the California Basic Educational Data System (CBEDS) in October 2017 was 1516.