The vision of Folsom Middle School is to create a collaborative culture of excellence through academic rigor, instructional best practices 
and technology so that all students can apply academic knowledge, critical thinking, and communication skills to their future lives in 
the global community. Our main goal is provide the opportunity for all students to learn and grow in an atmosphere conducive to 
learning and empower all students with the ability to make responsible life choices. 
 
Folsom Middle School is located thirty minutes East of Sacramento in the foothills of the Sierra Mountains in the town of Folsom 
(population 73,000, elevation 220 ft). Our student enrollment, reported on the California Basic Educational Data System (CBEDS) in 
October 2017 was 1420. FMS serves sixth, seventh and eighth graders. FMS's ethnicity makeup is 63.1% White, 16.34% Asian and 
11.13% Hispanic. FMS has 59 teachers, an educational program to include Honors English, Enriched Math, Specialized Academic 
Enrichment for students with an IEP, an elective wheel which rotates each trimester (3), and year long electives to include choir, 
orchestra, band, jazz band, student government and Project Lead the Way. FMS has many after school clubs which include National 
Junior Honor Society, Math Club, Legos Robotics Club, Book Club, Minecraft Club, Reach One Alliance, Club Live, Chess Club, and 
Engineering Club.