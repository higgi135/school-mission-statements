THE MISSION 
 
The mission of the Diamond Creek learning community is to ensure that all students learn at high levels, find success and contribute 
responsibly in our global community. 
 
VISION 
 
The Diamond Creek staff is a collaborative group of professionals who are committed to providing each individual student a 
differentiated, diverse education in a safe and supportive environment. 
 
COLLECTIVE COMMITMENTS 

 

• We will collaborate to build a meaningful curriculum that serves our students. 
• We will design interventions based on data. 
• We will use assessments to drive instruction. 
• We will celebrate our successes 
• We will support each member as we examine our own authentic results. 

School Description and Profile 
 
Diamond Creek is in the Northwest part of Roseville located within a mile of the junction of Woodcreek Oaks and Blue Oaks Boulevards. 
Diamond Creek Elementary School is in its eighteenth year of operation, having many unique features including classrooms built 
around an inner pod for planning, conferences, and small group teaching. It has two small playgrounds, and it is connected to a small 
community park maintained by the City of Roseville. The 1850’s era of paddle-wheels on rivers, miners and 
 
explorers seeking gold is a prevalent theme on the buildings. Our staff and students, in keeping with that theme, chose the school 
name of ‘Trailblazers’. 
 
Diamond Creek opened on August 17th, 2001 with seventeen permanent classrooms, a multipurpose room and administrative 
building. Today Diamond Creek serves over 605 students with twenty-three general education teachers and three special education 
teachers. Our teaching practices at Diamond Creek Elementary School reflects the new California Content Standards focusing on rigor 
in the subjects of reading, writing, oral language, literature, math, science, and physical education.