PRINCIPAL'S MESSAGE 
I'd like to welcome you to Mesquite Trails Elementary's Annual School Accountability Report Card. In accordance with Proposition 98, 
every school in California is required to issue an annual School Accountability Report Card that fulfills state and federal disclosure 
requirements. Parents will find valuable information about our academic achievement, professional staff, curriculum programs, 
instructional materials, safety procedures, classroom environment, and condition of facilities. 
 
Mesquite Trails Elementary provides a warm, stimulating environment where students are actively involved in learning academics, as 
well as, positive values. Students receive challenging curriculum by dedicated professional staff and based on the individual needs of 
the students. Ongoing evaluation of student progress and achievement helps us refine the instructional program so students can 
achieve academic proficiency. We have made a commitment to provide the best educational program possible for Mesquite Trails 
Elementary's students, and welcome any suggestions or questions you may have about the information contained in this report or 
about the school. Together through our hard work, our students will be challenged to reach their maximum potential. 
 
SCHOOL MISSION STATEMENT 
The mission of Mesquite Trails Elementary is to inspire and educate a school of leaders and learners to reach their full potential in a 
safe and positive environment. 
 
SCHOOL PROFILE 
Hesperia Unified School District is located in the high desert region of San Bernardino County, approximately 40 miles north of the 
Ontario/San Bernardino valley. More than 21,000 students in grades kindergarten through twelve receive a rigorous, standards-based 
curriculum from dedicated and highly qualified professionals. The district is comprised of 15 elementary schools which includes 3 
choice schools. At the secondary level Hesperia has 3 middle schools, 3 comprehensive high schools, 1 alternative school, 2 
continuation high schools, 1 community day school, and 6 charter schools. 
 
Mesquite Trails Elementary is located in the Oak Hills area of Hesperia and serves students in grades transitional kindergarten through 
sixth. At the beginning of the 2018-2019 school year, approximately 930 students were enrolled, including 12.2% in special education, 
11.8% qualifying for English learner support, and 66.3 socioeconomically disadvantaged per California Dashboard.