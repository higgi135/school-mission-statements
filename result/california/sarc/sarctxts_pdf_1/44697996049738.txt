Located behind Fire Station #1 in Aptos, our small school of 458 students in grades TK-6 welcomes visitors into a safe, nurturing 
community surrounded by evergreen trees and landscaped grounds. Mar Vista is a neighborhood school, but 25 percent of our 
students are from other parts of Santa Cruz County. In a county with declining enrollment, we are proud that so many families choose 
our school and welcome the diversity that our students bring. Our parent club, Mar Vista Parents (MVPs), helps build this sense of 
community by encouraging families to volunteer in classrooms & the library, care for our grounds & gardens, participate in barbecues, 
festivals, bingo nights, campus beautification days, and student recognition events. 
 
Our school seeks to educate the whole child, and our dedicated faculty and staff works very hard to improve achievement for every 
student. We are proud to see the "achievement/opportunity gap" closing as well. Teachers work closely together both in and across 
grade levels to review data, identify student needs, set goals, and develop common strategies. Teachers differentiate instruction 
through individualized reading and math programs, and they spend extra instructional time with identified students after school. 
 
We also improved our facilities; our School Site Council (SSC) added new theater lights in the multipurpose room and our parent club 
added document cameras/projectors to classrooms, added a new play structure, provided an iPad for each teacher and replaced our 
outdoor stage. A federal garden grant enabled us to plant native trees in a once barren area of our campus. A new roof has been 
installed during the summer of 2015-16, three new modular classes were installed during the summer of 2016-17. These 
improvements have been made with funds from the Measure L Bond.