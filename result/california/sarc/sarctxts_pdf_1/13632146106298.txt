Principal’s Message 
Welcome to San Pasqual Valley Middle School’s Annual School Accountability Report Card. As principal, I could not be more proud of 
the middle school students and staff. The purpose of this report card is to afford you further insight into the quality educational 
program we offer. 
 
It is the belief of San Pasqual Valley Middle School that students can and will excel in an environment that is tailored to their evolving 
needs. We strive to provide age-appropriate social, emotional, and academic strategies to support students in the middle school 
setting. Through daily encouragement from all staff, the students are provided the support they need to accomplish personal and 
academic goals. 
 
School Motto 
Together we believe, we achieve, we succeed. 
 
Mission Statement 
Providing students with a high-quality education that will equip them with the skills and knowledge and information to be successful 
in all post-secondary endeavors, their careers, and their lives. 
 
School Profile 
San Pasqual Valley Unified School District is located in Imperial County near the Arizona border. The district is comprised of one 
elementary school, one middle school, one comprehensive high school, and one alternative education high school. During the 2017-
2018 year, the district educated 701 students within its schools. San Pasqual Valley Unified School District is committed to providing 
a quality education for all its students.