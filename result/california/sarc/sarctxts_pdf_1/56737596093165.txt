MISSION STATEMENT 
 
Cypress Elementary School promotes academic excellence, nurtures learning as a lifelong process, and supports all students as they 
become informed, productive, contributing members of society. 
 
SCHOOL DESCRIPTION 
 
Students at Cypress Elementary School participate in a standards-based educational program which encompasses the integration of 
technology within their daily lessons. Measure I funds enabled the ability to use Wi-Fi throughout the campus, update student 
computers, and purchase iPads and Promethean Panels. The PTA also actively supports the integration of technology through 
assistance in purchasing Promethean Panels and contributing funds toward instructional programs such as PE, music, computer, and 
art. The school implements a Response to Intervention program that is embedded within the school day and targets instruction in 
small groups for all levels. The Cypress spirit is demonstrated through events planned by our student council and Parent Teacher 
Association. Parents are actively engaged in our school community by assisting in the classroom, teaching art to students through the 
use of Art Trek (a community based partnership), and participating in school wide events such as the Jog-A-Thon, Carnival, and Family 
Reading and Science Nights. The school collaborates with community volunteers to help teach kindness, compassion, and empathy 
through a week long Abilities Awareness Week. Our school counselor teaches conflict resolution through the use of Kelso's Choices 
and promotes kindness through a school-wide campaign of "Bucket filling". School-wide goals are developed collaboratively with staff 
members, parents, and members of our School Site Council through the use of the Local Control and Accountability Plan (LCAP), State 
and District Standards, and overall data analysis of students educational performance.