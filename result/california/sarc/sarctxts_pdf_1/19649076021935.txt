The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters in November 1988. 
The Report Card, issued annually by school boards for each public school in the state, provides for parents and other interested people 
a variety of information about the school, its resources, its successes, and areas in which improvement is needed. As you read 
Philadelphia’s report card, I hope you will be pleased with its successes and bright future. Our teaching staff provides outstanding 
instruction. Our support staff consistently strives to improve all aspects of our students’ educational day. All employees at our school 
have the best interests of your children at heart. While the data are not current for this academic year, I hope you will find this report 
informative and beneficial. If you would like additional information on any of these areas, please feel free to call my office at (909) 
397-4660. I welcome your comments on this report! 
 
Alicia Castaneda 
 
Principal 
 
Philadelphia Elementary School serves a diverse and growing population, it is our position that the school program reflects the goals 
and objectives of parents, staff, and community. 
 
Philadelphia provides a safe and clean environment for learning. While the facility is over 50 years old, the staff, teachers and support 
personnel, keeps the campus and classrooms clean and maintain suitable learning environments, in order to comply with the Williams 
Act. Also, as an integral portion of that same Williams Act, our entire teaching faculty is fully credentialed and all students are, by 
mandate, in possession of their core textbooks and supplemental supplies. 
 
Mission Statement and Goals 
The mission of the Philadelphia staff is to provide a high quality, academically rigorous, and standards-based instructional program to 
help students achieve proficiency and advanced status. As part of Cluster 2, Philadelphia Elementary School will implement a School 
Plan for Student Achievement whereby every student will be given the opportunity to learn and master skills needed to meet and 
exceed grade level standards. 
 
Our staff works in collaboration with parents and the community to: 

• Provide a strong comprehensive scholastic education program to achieve the 21st century skills of creativity, collaboration, 

communication and critical-thinking. 

• Maintain a campus atmosphere and organize school activities that promote responsible, honest behavior directed towards 

developing citizens who perceive themselves as important participants in a democratic society. 
Treat students in a manner that promotes self-esteem, confidence, responsibility, and respect. 

• 
• Monitor and communicate to parents, our students’ overall progress in all core areas via collaborative, frequently 

administered, formative assessments, District-wide assessments and statewide testing results. 

 

2017-18 School Accountability Report Card for Philadelphia Elementary School 

Page 2 of 12