Burney Elementary School (BES) is located in eastern Shasta County approximately 55 east of Redding on Highway 299 East. The town 
of Burney has a population of about 3,300 and is the most urban of the rural communities served by the Fall River Joint Unified School 
 
Burney Elementary opened in 1952. 
 
The mission of BES is to provide a safe and secure environment in which students are free to pursue a comprehensive curriculum in 
order to achieve their maximum potential. Our definition of a comprehensive curriculum includes all aspects of school life, academic, 
social, co and extracurricular. 
 
The Board of Trustees of the Fall River Joint Unified School District believes that it is the responsibility of the school and home, working 
together, to positive environment for students within which they may: 

• Realize potentials for learning 
• Develop and maintain basic skills and concepts 
• Develop an understanding of responsibilities to self, to family and to community 
• Develop creativity 
• 
• Respect and appreciate different cultures 
• Respect and appreciate the American heritage 
• 
• 

Learn in an environment that fosters a feeling of mutual respect and tolerance 
Learn in a safe and positive environment 

Enjoy learning and become lifelong learners