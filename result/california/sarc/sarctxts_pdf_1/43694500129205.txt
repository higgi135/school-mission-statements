Mission and Vision 
We see greatness and potential in every student, family, and community. 
 
Our students will be prepared to thrive in the global economy as they develop critical thinking skills and deep conceptual knowledge 
through academically challenging courses. Through a rigorous literacy program, students write and tell their stories, as well as listen 
to the stories of others with compassion. As our students master skills and content necessary to be successful in college and life, they 
will learn to advocate for themselves and their communities with humility and clarity. 
 
At KIPP Heritage Academy, we embrace our histories as we build a brighter future in East San Jose and beyond. Equipped with this 
strong foundation of knowledge, skills, and character, KIPP Heritage students will be agents of positive change, empowered as authors 
of their own lives. 
 
Description 
KIPP Heritage Academy is a public charter school open to all students in San Jose. KIPP Heritage began in 2014 and now educates 
students in fifth- through eighth-grades in partnership with the Franklin-McKinley School District. In the upcoming 2017-18 school 
year, the school will be located in a brand new facility next to the Seven Trees Community Center. 
 
Honors 
Top Bay Area Public Schools for Underserved Students, Innovate Public Schools Report 2017