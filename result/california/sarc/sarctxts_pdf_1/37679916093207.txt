The mission of Montgomery Middle School is to provide an appropriately challenging curriculum that cultivates learners who are 
collaborative communicators and critical thinkers. In an effort to prepare these life-long learners for 21st Century jobs, many yet to 
be created, students will grow and develop into knowledgeable inquirers and caring leaders. They will endeavor to create a more just 
and peaceful world by contributing intercultural and ethical understanding, wisdom, compassion, and respect to our global society.