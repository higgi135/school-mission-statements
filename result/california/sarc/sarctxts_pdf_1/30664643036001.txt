San Clemente High School (SCHS) is located in the seaside city of San Clemente, California, also known as “The Spanish Village by the 
Sea.” This historic beach community, famous for its climate and surf, is situated on the southern border of Orange County, midway 
between Los Angeles and San Diego. San Clemente High School serves students from diverse communities; San Clemente, Capistrano 
Beach, San Juan Capistrano, and Camp Pendleton Marine Base. Our student population is a microcosm of the diversity of our nation. 
We are a campus that has a long history of academic, athletic and extra-curricular success that is 50 years strong. Our school creed of 
Tradition, Commitment, & Excellence drive our school resolve and it is the foundation for our Triton Points of Focus – Culture & 
Community, Collaboration & Leadership and Academic Achievement – that guides our collaborative work. 
 
San Clemente High School has a strong community identity, strengthened by the number of teachers and parents who are SCHS 
graduates. Nurturing relationships with our staff, students, parents, and community is the cornerstone of our school slogan, “One 
Team-One Town.” 
 
Our Vision and Mission are closely aligned our school creed and Triton Points of Focus: 
 
Vision 
San Clemente High School will deliver a world-class education that encourages collaboration, communication, creativity, and critical 
thinking. Our graduates will successfully contribute to and compete in the global community. 
 
Mission Statement 
San Clemente High School provides opportunities for all students to engage in a rigorous and relevant standards-based instructional 
program. Through collaborative leadership, we will analyze stakeholder input and multiple sources of data to drive decision-making, 
establish goals, and implement action plans that will affect school-wide improvement. Our staff is dedicated to promoting high 
achievement for all students through: 

• Curriculum that is rigorous, relevant, and accessible. 
• 
• 
• Assessment that motivates students with a variety of formative and summative strategies, and produces data that is used 

Instruction that employs best practices and teaching strategies to maximize student proficiency. 
Infrastructure and technology that supports 21st Century Learning. 

to guide instruction. 
Interventions which are timely, systematic, and effective. 

• 
• Co-curricular and extra-curricular opportunities coupled with community involvement that enriches the high school 

experience for all students. 

• Continual improvement of curriculum and instruction through professional development, staff collaboration and 

Professional Learning Community practices. 

San Clemente High School is a WASC accredited four-year high school with an enrollment of over 3,028 students supported by a faculty 
of caring and (110) talented teachers, (75) support staff, (5) administrators, an activities director, and an athletics director. 
 

2017-18 School Accountability Report Card for San Clemente High School 

Page 2 of 17 

 

Based on exceptional performance in academics, athletics, and activities, San Clemente High School has been recognized as a California 
Distinguished School by the California Department of Education in 2007. Our staff and student commitment to excellence has 
consistently led to high test scores, league and CIF athletic championships, first place and sweepstakes awards for the Triton Marching 
Entertainment Unit, National Grand Champion Triton Dance Team, and academic distinctions. SCHS students score above State and 
National averages on SAT and ACT scores. Our students are also recognized for academic distinctions on Advanced Placement Exams 
(2014-15); 813 tested, 77% scored “3” or better, with 4 students recognized at National AP Scholars, 84 (10%) recognized as AP Scholars 
with Distinction, and 59 (7.25%) recognized as AP Scholars with Honors and 113 recognized with AP Scholar Awards. Our International 
Baccalaureate program continues to prepare our students to compete in a global community. SCHS continues to prepare our students 
for college and career opportunities: our of the 2015 graduating class 42% of our students enrolled in Four Year Colleges/Universities, 
52% enrolled in Two-Year Colleges, and 6% joined the workforce. 
 
San Clemente High School is home of the Tritons. The main campus (or lower campus) was built in 1963 while the upper campus, 
which houses freshman students, was acquired in 2010. The upper campus is unique to the high schools within the district, the primary 
focus of the campus is to serve as a campus reserved mainly for freshman, research has shown that having a “freshman house” can 
help students transition into the increased rigor and social structure of high school. SCHS recently celebrated its 50 year anniversary. 
Our campus has a total of 116 classrooms (94 on the lower campus and 22 on the upper campus), two multipurpose rooms (one on 
the upper and one on the lower campus), a library, a small theater, an administration building, and two computer labs. Major facilities 
upgrades have occurred over the years (1999, 2003 and 2010). Recently we have added portable classroom Chromebook carts. Each 
cart is equipped with forty Chromebooks available for teachers to use to enhance instruction and student engagement. 
 
SCHS operates on a modified block schedule with three traditional days (Monday, Tuesday, and Friday) and two block days (Wednesday 
and Thursday). Ninth and tenth grade students are required to enroll in a minimum of six classes, while eleventh and twelfth grade 
students must be enrolled in at least five classes. The 36-week academic year is divided into two semesters.