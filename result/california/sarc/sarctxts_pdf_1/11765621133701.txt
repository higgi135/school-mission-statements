Hamilton High School, with a 2018-2019 current enrollment of 288 students, is located in Hamilton City, along Highway 32 
approximately 10 miles from both Chico and Orland. Our school is a 23-acre campus with five main buildings, including a gymnasium, 
cafeteria, a 15,000 book capacity modern library, and an agricultural farm and facility with maturing olive and mandarin trees. This 
past school year, we celebrated 100 years of providing an exceptional educational experience for the greater Hamilton City community. 
 
As a comprehensive 9-12 high school, we offer a challenging curriculum for our students and enjoy a strong academic record and 
reputation. Students at Hamilton High School can choose Advanced Placement courses, Career Tech education courses, fine arts and 
Spanish language courses, Agricultural-based courses in addition to our rigorous core curriculum offerings in Math, English, Science, 
and the Social Sciences. In Spring 2015, Hamilton High School was awarded a six year accreditation by the Western Association of 
Schools and Colleges (WASC)- the highest level that can be achieved by a secondary school. 
 
We are fortunate to offer a varied amount of co-curricular and extra-curricular activities at Hamilton High. Many of our students 
participate in our Future Farmers of America (FFA) program, which is recognized as one of the finest in the North state area. 
Additionally, we annually have a theatrical play, a band program, and offer the following sports- volleyball, football, cross country, 
cheer, basketball, soccer, track & field, baseball, softball, and wrestling. We are pleased to state that our sports teams and student-
athletes are recognized as competitive while exhibiting appropriate sportsmanship and exceptional behavior by our teams and players. 
 
School and student safety are paramount for our students and staff at Hamilton High and a priority of the site administrator. The site 
administrator, with assistance from the two Deans of Students, consistently supervises campus throughout the day and, along with 
the athletic director, each can be found at most extracurricular events. Periodic drills and training for fire, lockdown, and other 
emergency situations are conducted to enhance the preparedeness and safety of our staff and students. 
 
The Hamilton High School campus is attractive and welcoming with an abundance of trees, shrubs, and grassy areas and a courtyard 
quad that often serves as a main focal point for student interaction. 
 
The mission of the faculty, staff, and administration of Hamilton High School is to provide a comprehensive educational program for 
all students so that all may have the skills and the opportunity to realize their full potential and, after graduation, become productive 
and contributing members of society. Each student, regardless of abilities, socio-economic, or cultural background should develop a 
sense of self-worth, accountability, responsibility, a desire for life-long learning, and a genuine concern for the welfare and cultural 
diversity of others.