John Adams Elementary School Mission Statement: 
 
 
"We exist to educate, inspire, and empower students for the future." 
 
 
 
John Adams Elementary is a Core Knowledge school where the entire staff is committed to educating, inspiring and empowering 
students for the future. We strive to ensure our students leave equipped with the skills, content knowledge and strategies to be 
prepared for the future, that they develop and acquire a love for reading and an understanding that reading is a life skill. We instill in 
our students the belief that they have the ability to learn because they have hope and courage to try new things and persevere through 
challenges. We also build in them the core virtues of responsibility, respect and readiness to be successful and productive members 
of their community, and as educators and positive role models, we provide a safe and structured environment to develop trust and a 
strong rapport with all students. We facilitate student driven learning while teaching rigorous and rich content knowledge in literature, 
math, science, history, and the arts. We remain flexible by using multiple strategies to differentiate instruction and engage every 
student. We collaborate with one another, keeping students’ needs in mind to drive them towards empowerment. Teachers instruct 
with researched based instructional practices in all subject areas so students learn the skills and content standards for speaking, 
listening, reading, writing, math, science, social studies, art, music and language development. 
 
 
For developing students' language arts skills and content knowledge, teacher are trained in and use the Amplify Core Knowledge 
English language arts and English language development curriculum. Core Knowledge science, social studies, art, and music content 
is integrated with other content areas throughout the instructional day. At John Adams developing students' early literacy skills and 
developing students' ability to engage in collaborative conversations has been a successful focus in the past. We continue to stay the 
course by progress monitoring students' early literacy skills three times in the year using the DIBELS measure, progress monitoring 
strategic students every month, and intensive students every two week. For further intensive support, K-2 institute, 3-6 Advanced 
phonics, and READ 180 are provided to students who are struggling with reading skills by classroom teachers and two extra support 
resource teachers. Students continue to engage in speaking and listening daily opportunities through collaborative conversations. 
For the 2018-2019 school year, the ELA/ELD focus is for students to read/ observe with a clear purpose to analyze text through multiple 
readings, note taking, annotations to respond to text dependent questions. Students' progress will be monitored at every trimester 
with the use of District approved ELA assessments. 
 
 
For developing students' math skills, teachers are trained on best math practices based on the California Mathematics Framework. 
This school year, 2018-2019, teachers are implementing RUSD's adopted math program Eureka that is aligned to the state mathematics 
standards. With this tool, teachers engage students in daily math fluency practices, application practices and concept development. 
In addition, kindergarten through second grade teachers are trained to address early numeracy with specific strategies. For students 
who are struggling with math skills, teachers provide small group differentiation during the day and an extra support resource teacher 
provides extra intensive support during the day. For the 2018-2019 school year, the math focus is for students to explain and apply 
mathematical concepts and interpret and carry out mathematical procedures with precision and fluency. Students' progress will be 
monitored at every trimester with the use of District approved math assessments. 
 
 

2017-18 School Accountability Report Card for John Adams Elementary School 

Page 2 of 13

Other programs to support the school's academic and attendance goal are the visual and performing arts opportunities, character 
education, and attendance celebrations. VAPA opportunities are made available to our students through out the year with the support 
of RUSD LCFF and community partnerships. Currently third grade students receive 9 weeks of ballet classes by a professional ballet 
dancer, twice a week. All 5th grade students are learning to play an instrument during band, twice a week. All students, TK - 6 receive 
9 art lessons through the Riverside Art Museum's program Art to Go. In addition, teacher engage students in Core Knowledge VAPA 
lessons. In addition, our character education program is tied to Core Virtues. The monthly core virtue is taught in every class and 
celebrated every month, along with a Dragon Award for academic performance. In addition, daily morning announcements provide 
reminders to students about how to demonstrate the core virtue and what is expected of everyone so that John Adams is a safe 
learning environment for everyone. Satisfactory whole class attendance is recognized every week during the morning 
announcements, a trophy is awarded to the class with the best attendance at Dragon assemblies, individual perfect attendance is also 
awarded once a month and at trimester assemblies. Our HEARTS after-school program also offers participating students the 
opportunity to engage in enriching activities such as visual and performing arts, computer coding, and physical exercise.