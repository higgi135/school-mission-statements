PRINCIPAL'S MESSAGE 
As principal, I have the unique privilege of introducing you to the Annual School Accountability Report Card for Mesa Grande 
Elementary School. Whether student, parent, staff, or community member, the data contained within these pages will prove useful in 
informing you about our school and community, including but not limited to; demographics, achievements, progress evaluation, 
ongoing goal realization, discipline, budget, and facility enhancement. 
 
In accordance with Proposition 98, every school in California is required to issue an annual School Accountability Report Card. 
However, we view this as not only a means of complying with the state legislature, but as an opportunity to keep our community, and 
the public in general, well-informed. We desire to keep the lines of communication open and welcome any suggestions, comments, 
or questions you may have. It is the opinion of the district, myself in particular, that a well-informed public is vital in continuing to 
advance in an ever-evolving world. 
 
It is the belief of Mesa Grande Elementary School of Health and Medicine that students can and will excel in an environment that is 
tailored to their evolving needs. 
 
SCHOOL MISSION STATEMENT 
At Mesa Grande Elementary School, our mission is "Learning for all through critical thinking, collaboration, communication, and 
creativity." Mesa Grande has a focus on Health and Medicine. 
 
SCHOOL PROFILE 
Hesperia Unified School District is located in the high desert region of San Bernardino County, approximately 40 miles north of the 
Ontario/San Bernardino Valley. More than 20,000 students in grades kindergarten through twelve receive a rigorous, standards-based 
curriculum from dedicated and highly qualified professionals. The district is comprised of 15 elementary schools which include 3 choice 
schools. At the secondary level, Hesperia has 3 middle schools, 3 comprehensive high schools, 1 alternative school, 2 continuation 
high schools, 1 community day school, and 5 charter schools. 
 
Mesa Grande Elementary is located in the central region of Hesperia and serves students in grades transitional kindergarten through 
sixth. According to recent data, 617 students were enrolled, including 20% in special education, 26% qualifying for English learner 
support, and 79% qualifying to receive free or reduced-price meals.