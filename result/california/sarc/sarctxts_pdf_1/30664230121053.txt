VISION STATEMENT- 
Ponderosa is an academic community where scholars, staff, and parents work together to foster lifelong learners with emphasis on 
PRIDE, Respect, Excellence, Success and Scholarly Behavior. POnderosa prepares scholars to be competitive in the global economy 
through intensive instruction of Mathematics and Science, using Project Lead the Way curriculum.