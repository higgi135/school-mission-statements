Old Town Academy (OTA) is located in the heart of historic Old Town. OTA is a K–8 public charter school that began operations during 
the 2011–12 school year. OTA has one class per grade level, and maximum class size is 30 students, for a total capacity of 270 students. 
The school employs nine classroom teachers, one STEM teacher, one physical education teacher, one Education Specialist, one Spanish 
teacher, one music teacher, a part-time robotics consultant, and three instructional assistants. The principal and office manager 
comprise the administration, in addition to two staff members serving as administrative designees. The curriculum is built around a 
combination of Core Knowledge, Project-Based Learning, and Digital Literacy and is aligned with Common Core State Standards. 
Students are prepared with both the world-class core learning skills and the active learning habits of mind they will need to survive 
and thrive in high school and in twenty-first century society. The Old Town Academy program reinforces that students, when 
challenged and given the appropriate resources, can excel to the very highest standards. OTA’s mission is to graduate students who 
can compete anywhere in the world—surviving, and thriving, in the global economy with confidence, creativity, and competence. The 
school motto is: “Excellence and equity for all students.”