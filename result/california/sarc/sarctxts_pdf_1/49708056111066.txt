School Vision and Mission 
 
The mission of John B. Riebli School is to prepare and support students in 21st century learning within a global community built on 
relationships, relevance and rigor. Riebli offers a comprehensive elementary school program including rigorous academic instruction 
in all core subject areas. Project Based learning, in which students are engaged in learning through integrated thematic units is a strong 
focus as a charter elementary school. Students gain knowledge and skills by working for an extended period of time to investigate and 
respond to a complex question, problem, or challenge. The core instructional program is supplemented with computer classes for all 
3rd-6th graders, an Art Docent program, a weekly fitness program, a music program, field trips, assemblies and special events. Support 
for the programs come from district funds, our resourceful Parent Teacher Association, and the Mark West Ed Foundation. We offer 
English Language Development and academic intervention for struggling learners as a response to intervention (RtI). Riebli School is 
dedicated to the development of the whole child. 
We believe that all students will succeed in the safe, nurturing school culture, based on strong character development and a positive 
discipline approach. Riebli has adopted the BEST (Building Effective Schools Together) framework for school wide behavior 
expectations and positive discipline. Our campus is orderly and students are rewarded with "BEST" behavior at monthly award 
assemblies. We use the Second Step and "Toolbox" (social and emotional learning) programs to promote tolerance and empathy 
school wide. The district supported Counseling Program allows for social skills learning opportunities in small groups and individual 
sessions. 
 
 
Academic Goals for academic years 2018-19 School Site Plan are: 
Goal #1: All students (including English Learners [ELs] and Socio-Economically Disadvantaged Students [SES]) will demonstrate a 3% 
overall growth on the Smarter Balanced Assessments in English Language Arts. (PUPIL OUTCOMES) 
 
Goal #2: All students (including English Learners [ELs] and Socio-Economically Disadvantaged Students [SES]) will demonstrate a 3% 
overall growth on the Smarter Balanced Assessments in Mathematics. (PUPIL OUTCOMES) 
 
Goal #3: All students (including EL and SES) will improve their overall composite scores in grades K-3 by 10% as measured by DIBELS 
benchmark score in a year’s time. (PUPIL OUTCOMES) 
 
Goal #4: Among all level 1 & 2 EL students in grades K - 6th, 75% will advance to the next level. Among all level 3 students in K - 6th, 
60% will advance to level 4.(PUPIL OUTCOMES) 
 
Goal #5: All students will be educated in a safe and healthy learning environment. (SCHOOL CLIMATE) 
 
 

2017-18 School Accountability Report Card for John B. Riebli Charter Elementary 

Page 2 of 12