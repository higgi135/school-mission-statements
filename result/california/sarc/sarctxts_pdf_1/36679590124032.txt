Mission: 
 
At Competitive Edge Charter Academy we are united by our commitment to learning. We seek to empower globally-minded, self-
motivated, respectful thinkers who make honorable contributions to their community. We are continually assessing and reflecting 
using multiple measures including academic data and input from our students, staff, and families. 
 
Vision: 
 
To provide our students with the greatest opportunity for success in both their local and global community, we have made the 
following commitments: 
 
100% of CECA students read proficiently according to grade-level nationally normed reading standards by grade 3. 
100% of CECA students are mathematically proficient according to grade-level mathematical fluency standards as established by the 
adopted Common Core State Standards by grade 3. 
100% of CECA students are proficient in the area of writing based on nationally normed writing standards by grade 3. 
 
 
The curricular plan for the school is based on the Learner Profile of the International Baccalaureate Primary and Middle Years Programs 
and the development of fluency and literacy in a second language. The established values and guidelines for personal growth and 
education will guide the whole school approach to a safe, clean, orderly, enriching environment in forming common expectations of 
respect for all members of the school community. We believe in the empowerment of learners through use of inquiry, and that all 
children can be successful through effort and a solid work ethic. We also believe that commitment, enthusiasm, perseverance and 
the building of strong interpersonal relationships among all members of the school community will lead to success. We will strive to 
provide a collaborative and supportive environment with shared responsibility among students, family, community, teachers and staff, 
with a singular focus on student learning. It is our goal, by teaching through inquiry and through the use of the International 
Baccalaureate and second language opportunity, that students will possess the following competencies when they leave CECA: 
 
Pose and pursue substantive questions 
Critically interpret, evaluate, and synthesize information 
Explore, define, and solve complex problems 
Communicate effectively for a given purpose 
Advocate for ideas, causes and actions 
Generate innovative, creative ideas and products 
Collaborate with others to produce a unified work and/or heightened understanding 
 
INTERNATIONAL BACCALAUREATE MISSION STATEMENT 
The International Baccalaureate Program aims to develop inquiring, knowledgeable and caring young people who help to create a 
better and more peaceful world through intercultural understanding and respect. To this end, the organization works with schools, 
governments and international organizations to develop challenging programs of international education and rigorous assessment. 
These programs encourage students across the world to become active, compassionate and lifelong learners who understand that 
other people, with their differences, can also be right. 
 
Competitive Edge Charter Academy is fully authorized to offer both the PYP and the MYP as an International Baccalaureate World 
School. All teachers in grades K-8 have been trained in the International Baccalaureate Program of Inquiry process. 
 
 

 

2017-18 School Accountability Report Card for Competitive Edge Charter Academy 

Page 2 of 11 

 

DISTRICT PROFILE 
Located in San Bernardino County, nestled at the base of the San Bernardino Mountains, the Yucaipa-Calimesa Joint Unified School 
District educates approximately 9,000 kindergarten through twelfth grade students from the diverse suburban communities of Yucaipa 
and Calimesa. The district is proud of its long tradition of academic excellence. The district is comprised of six elementary schools 
(Grades TK-5); one dependent charter school (Grades K-8); twp middle schools (Grades 6-8); one comprehensive high school campus 
(Grades 9-12); a community day school (Grades 7-12); an independent study program PEP and PEP+ (Grades K-12); a continuation high 
school (Grades 9-12); a special education success program (Grades K-12) including a preschool program; and an adult education 
program. The Yucaipa-Calimesa Joint Unified School District is dedicated to educational excellence and the continuous academic 
growth of all students. 
 
 
A Message from the Superintendent 
 
Dear Yucaipa-Calimesa Community, Parents and Students: 
 
YCJUSD is honored to serve students in two wonderful communities. We, as a team, continue to prepare our students to be successful 
in the 21st century. We work collaboratively with community partners, businesses and colleges to provide the best possible education 
for each and every student. The district employs high quality employees, who care for student well-being as well as academics. The 
school sites have a variety of clubs and programs to suit student interests and all our elementary schools have an after school care 
program. We strive to provide a wide variety of high quality services and programs in a safe environment. 
 
As you become a partner of the YCJUSD, please take the opportunity to be involved. It is our desire to work hand in hand with parents 
to support our children. The best way to get involved is to start at the school site. Our principals can help guide you to the many 
opportunities that exist. No amount of involvement is too small! We also offer classes for parents that will help you and your child in 
their educational journey. We have the Family Learning Center which offers a host of classes that support parents in learning strategies 
to work with children in grades K-12. Please take an opportunity to view the website to learn more about the classes. 
 
The role of educating children in our two communities is taken very seriously and we appreciate your trust. My goal, as your 
Superintendent, is to ensure that high quality instruction is delivered daily, our campuses are secure and well maintained, money is 
spent wisely, and students graduate from Yucaipa High School prepared to be successful! 
 
This school year is the opportunity to work with you in supporting education of our children. Please do not hesitate to contact your 
principal or the district office if you have questions about the district or how to become involved.