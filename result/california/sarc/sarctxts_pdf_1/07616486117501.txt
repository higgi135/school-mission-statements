Diablo Vista Elementary School’s Mission is to prepare students to become contributing, successful members of society. In order to 
accomplish this, our mission is to help students grow and achieve both academically and socially, with an understanding of essential 
life skills; to provide a learning environment and instructional programs that allows all students every opportunity to reach their fullest 
potential; to nurture a safe, uplifting school climate that creates a sense of belonging and empowers students to make positive 
decisions about their lives and education.