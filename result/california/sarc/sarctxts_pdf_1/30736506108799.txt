Our mission is to enable students to become contributing members of society empowered with the skills, knowledge and values 
necessary to meet the challenges of a changing world, by providing the highest quality educational experience we can envision. 
 
Westpark School opened its doors in July, 1991. Our first enrollment included 341 students in grades K-5. Currently, we maintain an 
enrollment of 862 students in grades TK-6. Westpark is one of four schools in Irvine that operate with a year-round calendar. In addition 
to serving neighborhood children, we offer the year-round calendar as an alternative to other families in Irvine who choose this 
schedule. 
 
Westpark was recognized as a California Distinguished School during the 2013-2014 school year. 
 
In every academic setting we provide a learning environment that emphasizes individual initiative and creativity as well as group 
cooperation and communication skills. Our offerings include the Alternative Program for Academically Accelerated Students (APAAS) 
classes, GATE clusters in classrooms, Specialized Academic Instruction support, Speech and Language support, New Comers Cluster 
Class in grade K and 1 with SDAIE and EL instructional support for our English Learners, Early Intervention Reading Model for emerging 
readers, and Response to Instruction (RTI) for all students in reading, and regular education classes. Intervention programs are 
designed and implemented to offer academic skills and support through supplemental funding. The programs are designed with input 
from the teachers based on their assessments and teacher recommendation. Progress monitoring of student progress allows for 
flexible support. In addition to general education classrooms, we serve Special Education students collaborative classroom settings 
and also three upper-grade classrooms for academically talented students (APAAS). Our student population includes students from 
homes where more than 30 languages other than English are spoken. Our staff carefully monitors each child and celebrates each 
child’s successes and uniqueness at our regular grade level assemblies. We are a PBIS, Positive Behavior Intervention and Supports 
school and provide all students the social and emotional supports along with the academic supports while emphasizing our school 
values of Accountability, Integrity, and Respect. 
 
During the 2018-2019 school year the facilities at Westpark will be under construction for modernization and to bring the school to 
the education specs determined by IUSD. The project adds collaboration spaces, an innovation lab, design lab, and new kitchen 
building.