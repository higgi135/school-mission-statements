The Santiago Hills Mission Statement reads: Santiago Hills Elementary School is committed to all students reaching their highest 
potential by establishing a curriculum that meets or exceeds government standards for education; providing programs that develop 
children's mental, physical and social skills; fully equipping students with essential technology for 21st century challenges; supporting 
the work of teachers; and partnering with parents and the community to create an environment geared to the success of all students. 
 
Santiago Hills serves a diverse student population, with students from transitional kindergarten through 6th grade in general 
education, special education, and Alternative Program for Academically Advanced Students (APAAS) programs. The focus of Santiago 
Hills Elementary School’s 2018-2019 site goals is to create a base for future growth and continued achievement. Santiago Hills 
Elementary students, including English learners, are excelling academically when compared to state, county, and district achievement. 
However, we recognize that we need to prioritize goals that focus on our low socioeconomic and disability students to ensure they 
are also making academic strides. Our School Plan for Student Achievement (SPSA) proposes two goals that can both be classified as 
“stepping stones” to a larger goal or broader implementation. The first goal is to meet the IUSD LCAP technology ratio of 1.3 students 
per device in schools. We strive to reach that goal in a responsible manner by creating a long-term plan of technology acquisition, 
maintenance, and replacement. Our second goal is to improve our offering of effective social-emotional tools that positively impact 
student behavioral and academic needs.