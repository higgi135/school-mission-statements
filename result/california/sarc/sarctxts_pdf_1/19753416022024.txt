Beryl Heights School, founded in 1909, is located in South Redondo Beach. Beryl provides a disciplined learning climate for all students. 
Students, parents, and staff are encouraged to learn and work together, to take risks, and to stretch themselves beyond their 
expectations. Staff members support and encourage students, parents, and each other to accept and find the best in themselves and 
others. Beryl is a place where everyone learns and everyone teaches. An atmosphere of trust, caring, support and encouragement 
prevails. Learning communities’ work together providing focus, constructive feedback, implementation support, and encouragement. 
Everyone takes responsibility for ensuring that each student and adult reaches his or her full potential. Students are challenged, 
supported, and encouraged to set goals and take risks. There is a "can-do" attitude present in classrooms. A strong partnership 
between home, school, and community exists that recognizes in order for the school to be successful, each child must be successful. 
Following the six pillars of character is the way we approach students to make sure they are caring, responsible, respectful students. 
The school sees itself as one entity rather than individual classrooms. Everyone is dedicated to doing whatever it takes to ensure that 
each student is prepared to be the teachers, leaders, and dreamers of tomorrow.