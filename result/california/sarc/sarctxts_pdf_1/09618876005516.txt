OUR DISTRICT VISION 
The vision for Gold Trail School District is to create and maintain an environment where every child receives a high quality education 
and comes to school feeling safe, cared for, and optimistic about his or her ability to learn. 
 
OUR SCHOOL MISSION 
We strive to make Gold Trail School a place where students, parents, and staff work together to provide a positive, caring, and safe 
place where each student can achieve his or her personal best. 
 
SCHOOL DESCRIPTION (FY 17-18) 
Gold Trail School is located in a beautiful rural setting located three miles from Coloma where gold was first discovered in California. 
The school property is completely surrounded by Gold Hill Ranch, a 272 acre property recently acquired by American River Conservancy 
for its historical and natural value. The school has a student enrollment of 391 and serves grades 4 through 8. Gold Trail’s 4th grade 
is team taught. Grades 5 and 6 are served by core teachers at each grade level providing instruction in Language Arts, History, Science, 
and Math. All students in grades 4 through 6 attend Music or Band class, as well as physical education every day. Both of these 
programs are taught by credentialed specialists. 7th and 8th grade students attend core academic classes as well as an offering of 
elective courses. As with grades 4 - 6, all students in grades 7 and 8 receive physical education instruction and have the opportunity 
to attend classes with our Grammy-nominated Fine Arts instructor or to learn Spanish.