The mission of the Kern County Juvenile Court School is to meet the unique educational, social, and emotional needs of our diverse 
student population in a safe and nurturing environment to encourage students to develop an enthusiasm for life-long learning and 
pursue post-high school opportunities. 
 
The Juvenile Court School programs are operated by the Kern County Superintendent of Schools Office. All juveniles served by Court 
Schools are placed by the juvenile justice system. Court School operates five separate year-round programs including four residential 
programs (Central School, Erwin Owen High School, Redwood High School, and the Jamison Children's Shelter School) and one non-
residential program (Bridges Academy). 
 
Central School provides educational services to elementary and high school students who are wards of the court and either reside in: 
the James G. Bowles Juvenile Hall; Pathways Academy, a female treatment camp housed in the facility; or the Furlough Treatment and 
Recovery Program, a short-term intervention program for wards arrested for violations of furlough. 
 
Erwin Owen High School is located within a non-secure, residential treatment camp in Kernville, provides educational and vocational 
services to male juvenile offenders for average periods of four to six months. Educational and vocational services include: ROP Auto 
Shop, forestry work and animal husbandry provide work experience, vocational training and career pathway development. Weekly 
pre-release conferences are conducted to make the student and parent aware of all supports that are in place to help the student as 
he transitions out of the institution. 
 
Redwood High School is located at the Larry J. Rhodes Crossroads Facility and provides educational services to Kern County's most 
delinquent male youth in a boot camp-like setting offering 12 to 36-week commitment programs. A construction technology class is 
available for students to obtain high-quality vocational training. Weekly pre-release conferences are conducted to make the student 
and parent aware of all supports that are in place to help the student as he transitions out of the institution. 
 
A. Miriam Jamison Children's Center School is housed on the grounds of a 24-hour emergency shelter for neglected and abandoned 
children, offers short-term educational services to students in transition to foster care or other home placements. 
 
Bridges Academy is a school-based collaborative involving Court School, Probation, Mental Health providers and other agencies, 
offering a structured and supervised school environment where offenders, age 16-18, can receive vocational education and 
enrichment while working to complete high school. 
 
 

2017-18 School Accountability Report Card for Kern County Juvenile Court School 

Page 2 of 15