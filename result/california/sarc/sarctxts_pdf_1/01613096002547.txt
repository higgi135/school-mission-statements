Principal’s Message 
Dayton Elementary School is located in the Washington Manor area of San Leandro. Although within the city limits of San Leandro, 
Dayton is part of the San Lorenzo Unified School District. In September 1998, Dayton re-opened as a K–5 elementary school. Students 
who attend Dayton primarily are from single-family homes in the area. A number of apartment complexes also claim Dayton as their 
home school. 
 
The students enrolled at Dayton reflect many cultures and represent a wide ethnic diversity. This diverse population is serviced by a 
well-qualified group of teachers and other educational specialists. The Dayton School staff continues to be committed to providing a 
well-balanced educational program that promotes a high level of academic achievement while simultaneously building on each 
student’s sense of self-worth. Using Responsive Classroom and Restorative Justice practices, the Dayton community continually 
supports students social-emotional needs. 
 
Students receive a strong foundation in English/Language Arts and Mathematics. They also receive instruction in Science, 
History/Social Studies, Physical Education, Music, Art, and Technology. For the past several years, Dayton Staff has been actively 
involved in professional development opportunities to strengthen our understanding of the Common Core State Standards (CCSS) and 
how to best improve our instructional practices to meet 21st century learning needs and prepare students for college and career 
readiness. In addition to analyzing the CCSS and planning lessons that align with these standards, the staff reviews multiple 
assessments systems including F&P Reading Assessment Benchmarks, NWEA, and CAASPP and adjusting current practices to best 
support students to ensure success on the new assessments. 
 
The school provides many enrichment activities, such as assemblies and field trips that build self-esteem and enable students to 
become good decision-makers and responsible citizens. Throughout the school year, Dayton teachers plan activities which focus on 
cultural awareness and appropriate behavior. These activities are organized in order to help students gain an understanding and 
appreciation of the rich community and personal heritage that students bring to the school campus. To better understand us, we 
invite you to visit classes and attend school-sponsored events including Wednesday Morning Gatherings, School Site Council (SSC), 
English Learner Advisory Committee (ELAC), Back-To-School Night, Open House, Walk-a-Thon, PTO meetings, and other special events. 
 
Dayton Elementary Mission Statement: 
All Dayton students are valued and appreciated for their unique talents and backgrounds. Dayton School challenges our students to 
become independent thinkers, responsible citizens, and lifelong learners, by providing an interactive and mutually respectful learning 
environment. The Dayton staff works toward bettering our understanding of how our behaviors and practices are linked to our 
expectations of student achievement. We acknowledge that having conversations about equity and the achievement gap are 
uncomfortable, yet through these discussions, we further build our skills and strategies in teaching all students within our diverse 
student body. We understand that moments of discomfort may also be moments for great learning. We use multiple data sets to 
create new tools and improve old ones, while continually remaining flexible. Dayton School remains focused on our students and the 
community we serve. 
 
Equity Mission 
Equity in the San Lorenzo Unified School district is acknowledging historical biases and changing the way they are addressed in terms 
of closing the achievement and opportunity gap. We define our approach to the work of equity as providing for each student the 
academic, emotional and social supports needed to increase the achievement of underserved subgroups at an accelerated rate while 
additionally increasing overall student performance. 
 
 
 
 

2017-18 School Accountability Report Card for Dayton Elementary School 

Page 2 of 12 

 

District Mission Statement 
The San Lorenzo Unified School District teachers and staff will collaborate with families and the community to cultivate safe learning 
environments and ensure equitable opportunities and outcomes for all students. All students will become engaged community 
members contributing to, and becoming good stewards of our changing world. All students will reach their highest potential as 
creative and critical thinkers prepared for college, career and lifelong learning. 
 
District Vision 
Students will become creative, collaborative, compassionate, resilient, well-informed and socially responsible advocates for equity 
and social justice as a result of their education, experience, and support from educators, families and the community.