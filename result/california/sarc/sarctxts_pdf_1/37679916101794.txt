The parents, administration, and staff are committed to the principle of "celebrating life through learning." Our mission is to instill in 
our students an enthusiasm for learning and a sense of pride in their accomplishments. We do this by encouraging a high degree of 
staff expertise, parental involvement, and a strong academic focus. It is our intent that by fostering common goals between home, 
school, and community, we will better prepare our students to lead productive lives in a constantly changing society. Furthermore, 
we wish to provide a safe, responsible, and kind environment that motivates children to do their very best in reaching their full 
academic, social, emotional, and physical potential.