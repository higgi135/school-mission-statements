The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters. As you read the 
Report Card, you will gain a better understanding of William Penn as a school with a record for improvement, a faculty that is 
professionally skilled and personally committed to meeting the learning needs of students and a student body which is enthusiastic 
and motivated to perform to the best of their ability. 
 
William Penn Mission Statement: 
 
William Penn staff is committed to providing each student with optimal learning opportunities so that they can reach their full 
academic and social potential. Toward this end we commit available resources to ensure that... 
1. We build positive relationships to foster a collaborative and supportive school environment. 
2. We encourage and celebrate success and achievement in our school community. 
3. We enhance our practices through shared leadership, continual professional growth, and education. 
4. We have high expectations for all children and we provide additional time for support and enrichment. 
5. We provide an environment which is orderly, safe, inviting and stimulating. 
6. The leadership is supportive, encouraging, and fosters positive changes. 
7. We cultivate a school where all families of diverse backgrounds and beliefs feel welcome and are proud to send their children.