Nuestro Elementary School is a small, somewhat old-fashioned, school nestled in a local farming community at the base of the Sutter 
Buttes. All members of the Nuestro family focus on the well-being and success of our children, and our goal is that every child receives 
the foundation they need to live successful, joyful lives! We are proud to offer a safe, family-focused atmosphere supporting our 
students' character development as they acquire a top-notch education. To leverage best practices, we include the use of modern 
technology for learning and to support a targeted intervention approach where each student receives tailored instruction to meet his 
or her individual needs. This approach allows advanced students to extend their learning as well. In addition to our strong academic 
program, we offer social/emotional support highlighted by ongoing support groups. When school dismisses, we provide an after-
school program with homework help for students needing a safe place to stay until their parents can be with them. 
 
Statement 
The school environment is one in which all students are able to grow and succeed in a safe community atmosphere. Parents are 
important partners in their children's education, and we encourage parental involvement in a variety of ways. We all work to support 
our students and expect the very best.