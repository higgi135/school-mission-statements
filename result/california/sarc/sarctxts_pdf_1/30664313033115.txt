School Mission Statement: 
John F. Kennedy High School’s Mission is… 
 
To ENGAGE students in school, community, and global activities which encourage passion, empathy, and open-mindedness. 
 
To EDUCATE students in a rigorous and diverse curriculum that fosters critical thinking, creativity, collaboration, and communication. 
 
To EMPOWER students to be inquirers who work in conventional and innovative ways to be successful in their life pursuits. 
 
Highlights: 
John F. Kennedy High School is one of nine comprehensive high schools in the Anaheim Union High School District (AUHSD). Kennedy 
opened its doors in 1964 and has a current enrollment of approximately 2,366 students in grades nine through twelve. It was the first 
school in the United States to be named after President John F. Kennedy. Kennedy is the only high school in the small city of La Palma. 
It became a California Distinguished School in 2013 and a Gold Ribbon School in 2017. Go Irish! 
 
John F. Kennedy High School offers the only International Baccalaureate (IB) program in AUHSD. This year, 125 juniors and seniors 
enrolled in IB. In 2018, 43 seniors earned their IB Diploma (81% pass rate). Based on current enrollment, over 656 students will be 
eligible to take Advanced Placement exams this school year. The 615 Kennedy students who took AP exams in May 2018 had a pass 
rate of 62.3 percent. Additionally, 94.8% of Kennedy seniors from the Class of 2018 completed all requirements for graduation. 
 
Demographic Information: 
John F. Kennedy High School has 2,426 students, of which approximately 45.5% participate in the free and reduced meal program, 
5.6% are English Learners, and 9% are Special Education. The demographic profile also indicates the following: 32% Hispanic/Latino, 
23% Asian, 15% White, 11% Filipino, 4% African-American, 1% Pacific Islander and 14% identify as two or more races.