Palmdale School District Mission Statement 
To support our vision, we will implement priorities, actions, and services targeted to staff, students, and parents designed to equip 
Palmdale School District's students to live their lives to their full potential. 
 
School Profile: 
Palm Tree Elementary School is one of twenty-six schools in the Palmdale School District and is located in a low socio-economic status 
area. Palm Tree serves a diverse community of approximately 600 students in grades Transitional Kindergarten through Fifth. This 
diverse population includes about 78% Hispanic, 16% African American, 10% White, and 1% Asian/Pacific Islander students. Many of 
our Kindergartners come from the Head Start Preschool located on site. 
 
Palm Tree has 22 classroom teachers on staff, all of whom have full credentials and hold CLAD/BCLAD certification. We also have a 
Learning Support Teacher, 5 Instructional Aides and 3 Bilingual-Instructional Aides who provide additional support to our staff and 
students. 
 
We have incorporated a variety of intervention programs and extended learning opportunities for our students to help address their 
academic and individual needs. Selection for intervention is based on results of California, District, and common formative 
assessments. Referred students in grades K through 5 are assessed and diagnosed, intervention is prescribed, progress is monitored, 
and changes in program are made when necessary. In addition, our 3rd through 5th grade teachers have undergone Advancement Via 
Individual Determination (AVID) training, which has been implemented in our 3rd, 4th, & 5th grade classes. 
 
Palm Tree is committed to the teaching practices and pedagogies that are an integral part of being a Common Core State Standards 
based curriculum school. We firmly believe in the value of building a professional learning community, and are continuously evaluating, 
collaborating, and communicating what is essential to achieving this goal. 
 
"Palm Tree: An Oasis of Education"