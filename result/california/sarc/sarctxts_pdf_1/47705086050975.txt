Principal’s Message 
 
Jackson Street Elementary School serves students in fourth through eighth grades. Because of our large enrollment, we are able to 
offer a strong music program that includes band and choir in grades 5-8, general music to fourth grade, weekly science labs for fourth 
and fifth grades, one computer lab, a mobile lab of thirty Chrome Books for each classroom, a media center, a gymnasium, a 
multipurpose room, intramural and inter-scholastic competitive sports, a growing offering of elective classes and a strong physical 
education program. 
 
The Yreka Union Elementary School District provides a strong academic program that is articulated across all grade levels. Our adoption 
committees meet regularly to review programs to ensure that we are aligned to the California State Standards. We also provide 
services to meet the needs of our Special Education population, English Language Learners (ELL), and Gifted and Talented Education 
(GATE) students. As a four-time recipient of the California Distinguished School Award and a Title I Academic Achievement Award, we 
are proud of the accomplishments of our Jackson Street School students, and will continue to provide a curriculum that produces 
articulate, confident, and skilled individuals. 
 
Jackson Street School staff are committed to our student population and their families. We have a tiered approach to intervention 
for academics and behavior. Staff collaborates frequently to identify students that need extra support, and we work together to 
ensure we put students in the best possible situation for their success. This is a team effort that oftentimes is instigated through our 
SST process. Our RSP teachers address the students with the greatest needs, while our Title I-VI program coordinates nine 
paraprofessionals to address the next tier of students that need intervention. 
 
In January of 2007, we began our Siskiyou After School for Everyone (SAFE) program, which provides after-school support for over 125 
students daily. There is a required homework completion time, snack, and recreation. Specialty activities include a math club 
w/instruction, cooking, a large Wii computer gaming center as a reward, sewing, jazz band, guitar lessons, and organized games. The 
program operates from the time school ends until 6:00 PM every school day. 
 
The Jackson Street School staff is committed to providing our students with the most current educational opportunities. The staff has 
been, and will continue to be, involved with professional development that focuses on the implementation of the Common Core State 
Standards. Additionally, with our partnership with ETS/College Options, we will continue to work with students to get each student 
on the path to a successful college and/or career.