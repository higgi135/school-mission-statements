School Description: 
E.V. Cain Middle School serves approximately 700 students in grades sixth through eighth. We offer a dynamic education integrating 
21st-century skills with a technology integrated, academically rigorous, core body of knowledge in all content areas with a focus on 
STEAM (Science, Technology, Engineering, Arts, Mathematics) education. Students engage in four periods of core instruction, a 
rigorous physical education class integrating health standards into the instructional program, as well as an elective. We also offer 
numerous enrichment opportunities including Lego Robotics, SeachPerch Underwater Rover Robotics, Cooking, Conservation Club, 
Music Club, Shakespeare Club, Solar Suitcase Club, Science Club, Entrepreneur Club, Drama Club, Service Club, and Destination 
Imagination. Advanced courses are offered for qualified students. Many of our 6th grade students participate in the STEM Expo and 
all 7th and 8th grade students participate in an annual Engineering Challenge. 
 
Mission Statement: 
EV Cain is a community where all are valued, creating successful learners one student at a time. 
 
E.V. Cain believes all students can learn and can be successful. The staff believes that a strong foundation in science and mathematics 
provides a critical component to a successful 21st-century career. We believe in integrating technology throughout the curriculum. 
The STEAM curriculum provides: 

• 

Embedded technology across all curricular areas which provides support that meets the varied learning needs with 
multiple learning levels and student populations. 

• Combined traditional and inquiry-based instructional practices to ensure that students master both concepts and skills in 

all of their classes. 

• Active engagement in learning, providing opportunities for critical thinking, for asking challenging questions, for problem-

solving and decision making, for creativity and innovation, and to develop both personal and group responsibility. 
Integrated, spiraling concepts in our teaching so that students revisit core skills and concepts many times throughout the 
year. 
Focused project-based learning instruction which encourages students to be “risk-takers” in a safe, rigorous learning 
environment. 
STEAM aligned electives and enrichment opportunities to engage and inspire STEAM career opportunities. 

• 

• 

• 

School Vision: 
EV Cain strives to cultivate lifelong learners with an emphasis on academic excellence, character development, and community 
involvement, representing the best in Auburn. Together CAIN Can! 
 
School Mantra: 
Together CAIN Can! Community, Achievement, Integrity, Now