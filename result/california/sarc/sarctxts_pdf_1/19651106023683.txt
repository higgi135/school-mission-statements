School profile 
Lydia Jackson Elementary School is located on the eastern edge of the Whittier City School District in the City of Whittier. Our school 
has 17 TK - 5th grade classrooms and additionally 5 Special Ed Classrooms that service children from Pre-K to 5th grade. We currently 
have 468 students enrolled. Ninety percent of our students are Latino/Hispanic. Of our 468 students, 31% are English Learners and 
84% are Free/Reduced Lunch. Our staff of 21 teachers meet the criteria of being highly qualified. Lydia Jackson Elementary School’s 
rigorous, well-rounded curriculum nurtures and enriches each individual learner. Students have high academic achievement and 
conduct that promotes a positive learning environment for all. Our well-trained staff offers differentiated instruction for our diverse 
learners with total commitment to the success of all students. By providing students with the best education possible, students 
become a positive asset to any and all communities. 
 
Our school has high expectations for students and staff. Our staff understands the importance of developing life long goals at an early 
age, therefore, we have committed to implement the philosophy of AVID (Advancement Via Individual Determination) to instill and 
develop college preparedness across all grade levels. Our staff has committed to develop the 4 essentials goals to achieve AVID 
certification: Culture, Instruction, Leadership and Systems. Students are taught note-taking, organizational skills and they are 
consistently provided opportunities to develop their critical thinking skills. Jackson is a certified AVID school and was named a 
"showcase" school.. Our school received this distinction due to our ongoing commitment to implement AVID strategies across the 
curriculum and across all grade levels. Jackson School is one of 9 schools across California to earn this distinction. As a result of our 
dedicated work to ensure college readiness, 
 
our district board has granted our school permission to theme our school as a college readiness academy. We will be adopting a new 
name with our theme by the end of the school year. We will rename our site as Lydia Jackson College Preparatory Academy. 
 
Parents consistently monitor their child’s progress and classroom activities by visiting our website at www.whittiercity.net, following 
our Jackson Facebook account @ Lydia Jackson Jets and through the Sangha App. 
 
Our staff, students, and parents take a positive, active role in supporting learning in all academic areas. Our parents assist their children 
with their daily homework to support daily learning. Parents support all school functions with their attendance and participation in all 
school activities. Parents and staff positively influence our students to become life-long learners and create a productive learning 
environment, so that every child feels supported in every aspect possible. Parents are encouraged to be active partners in the 
education of their children. Monthly events and meeting are scheduled during the day and evening to inform parents about our 
schoolwide goals, academic progress and best practices. 
 
Our students are motivated to attend school daily, and they are prepared to responsibly participate in their learning. Our Positive 
Behavior Intervention Support has provided a consistent and clear expectation of positive behavior throughout the school. Our 
students are respectful, actively follow the school rules, seek positive resolution to problems, and are held accountable for their 
actions with fair and consistent school and home discipline. Our teachers consistently acknowledge and reward our students by 
dispensing Jackson Jet pride tickets. Students are recognized daily, weekly and monthly. Our students strive to be Successful Scholars, 
Outstanding Citizens, AVID Achievers and to be Responsible and Respectful. Our school motto, "Jackson Jets SOAR" motivates our 
students and staff to do their best daily. 
 

2017-18 School Accountability Report Card for Lydia Jackson College Preparatory Academy 

Page 2 of 13 

 

Vision and Mission Statements 
 
Our Purpose: 
 
The mission of Lydia Jackson Elementary is to empower, prepare, enrich and motivate students as critical thinkers. Our students will 
be caring and successful leaders now and in their college and careers. We cultivate individual determination so all students can be 
contributing members of our 21st century global society. 
 
Our Vision, Our future: 
 
Lydia Jackson Elementary is committed to welcoming all community members to our high performing school. Our school is dedicated 
to the following: 

• Preparing our students for college and career readiness using AVID strategies 
• 
• Working collaboratively as a committed staff to continuously develop and increase our professional knowledge and skills 

Teaching a rigorous academic program enhanced with state of the art technology 

WELCOME TO THE PARTNERSHIP!