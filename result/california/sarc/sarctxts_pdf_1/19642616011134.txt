Baldwin Stocker is recognized as a California Distinguished School and 2012 National Blue Ribbon School recipient. Our staff aims to 
create a positive environment for learning to ensure high student achievement, to enhance self-esteem, and to develop positive social 
behavior. As a school community, we are committed to excellence in all that we do, and we concentrate on people as the centerpiece 
of the educational process. We strive to create an environment that is safe for all, not only physically, but also emotionally and 
educationally. At Baldwin Stocker School, the focus is on the process of creating challenging and engaging work for all students. We 
constantly work toward improvement in the quality of the curriculum, and high-quality student work as a key element of the mission 
and vision of our school. Programs such as English Language Development (ELD) and Intervention have found high success in meeting 
students’ needs. Instructional programs such as Thinking Maps and Write from the Beginning provide rich learning experiences for all 
students. An excellent teaching and classified staff, a very supportive community, a dedicated governing board, and district 
administrators all work together to help us achieve our goals.