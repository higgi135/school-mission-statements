San Jose Charter Academy (SJCA) has a Transition Kindergarten (TK) through 8th-grade enrollment of 1249 students. Our student 
population is predominantly Hispanic but includes a variety of other ethnicities. Established as a charter school in 1998, SJCA has 
worked intentionally and purposefully in providing students with an education that prepares them for high school and beyond. Our 
vision speaks to our efforts in recognizing that our students need an education that includes not only superior academic curriculum 
and instruction but access to technology and its integration. In addition, our school provides programs and resources addressing our 
students’ socio-emotional needs…crucial for educating the whole child. 
 
As you enter our school, you are greeted by students who smile readily, with enthusiastic and energetic teachers vigorously engaged 
in current best practices designed to reach all learners. As you walk through our campus, you are delighted to find brightly lit 
classrooms filled with student work. Student engagement in their learning is a non-negotiable. Our staff receives a variety of 
professional development opportunities geared toward strategies on how to keep students effectively engaged. 
 
Unique in school design and organizational structure, our school offers students and parents a unique educational opportunity. To 
better meet the needs of our students, our school is divided into smaller academies – Primary (K-2), Elementary (3-5), and Junior 
Academy (6-8). Within this structure “Houses” of teachers and students are grouped, which creates the feeling of smaller school 
communities. These Houses of teachers are supported by a number of teacher leaders throughout the school. (Lead teachers, grade 
level coordinators, and curriculum coordinators.) Students attending SJCA attend 7.5 hours of school daily, 7:45a.m. - 3:15p.m., 185 
school days per year. TK and kindergarten students attend 6.5 hours of school daily. Students have access to a rigorous standards-
based curriculum that is far more than just traditional academics. In the two major core areas of reading and math, instruction is 
delivered using standards-based, aligned curriculum selected after careful research and evaluation. Students also participate in a 
comprehensive and articulated hands-on science program, as well as literature and project-based social science program. Writing is 
not a stand-alone subject. It is woven throughout every subject area in the school. In addition to the above core curricular areas, 
students receive extensive instruction in music, art, and physical fitness. A “0 period” Spanish class incorporating the Rosetta Stone 
on-line program is available to 7th and 8th-grade students. Our Character and Ethics program is built on the foundation of our eight 
core values: Wisdom, Justice, Courage, Compassion, Hope, Respect, Responsibility, and Integrity. We are on our first year of "The 
Leader In Me" implementation. 
 
Interwoven throughout curriculum and instruction is our school’s technology component. Every teacher and paraprofessional are 
provided with his/her own laptop, as well as all 6th, 7th, and 8th-grade students. In addition, all 3rd, 4th, and 5th-grade classroom 
have 1:1 iPad access. Our school houses an 80 seat iPad computer lab and a 34 station PC lab. One unique aspect involving technology 
is our Customized Learning block, which is a 50-minute daily period built into the schedule for the purpose of providing students with 
the opportunity to improve and/or extend their learning. SJCA has been selected as a 2009 and 2016 National Blue Ribbon School and 
a California Distinguished School for 2004, 2008, and 2012. In addition, we have been awarded the Title 1 Achieving school award 
numerous times. While we are truly honored by these recognitions, SJCA continues to have a growth mindset of a school in continuous 
improvement. 
 
Our vision is to inspire students to create, innovate, and imagine limitless possibilities… 
 
 *TO DREAM 
 
 

*TO DO *TO CREATE 

 

2017-18 School Accountability Report Card for San Jose Charter Academy (Formerly San Jose-Edison Academy) 

Page 2 of 15 

 

FOCUS ON IMPROVEMENT 

• 

• 

• 
• 

To increase our effectiveness in moving all students academically to proficiency through the implementation of the 
California State Standards. 
To increase our effectiveness in moving all students academically to proficiency level through effective data-driven 
instruction. 
To provide professional development for all instructional staff that will maximize the effectiveness of instruction. 
To improve intervention strategies. 

From GreatSchools.org - "This school is rated above average in school quality compared to other schools in California. Students here 
perform above average on state tests, are making above average year-over-year academic improvement, and this school has above 
average results in how well it’s serving disadvantaged students." January 2019