The mission of Shelter Cove Community Day School is to provide quality education to at risk students so that each might master basic 
academic skills, develop adequate social skills and earn a diploma or equivalent. 
 
Shelter Cove is a K-8 school; however; most of the students are 6th-8th grade . Our students often have behavior and attendance 
problems that interfere with educational achievement. SCCDS provides placement for students who are expelled from other schools, 
placed by juvenile authorities, placed by SARB, or referred by regular education. Students are offered assistance with behavioral, 
social, emotional and educational skills needed to succeed in school while working to complete courses for high school diploma 
requirements. 
 
There is one classroom with one full time teacher and a 1.0 FTE counselor. The teacher must be prepared to provide everything from 
remedial to advanced instruction in all curricular areas. Career education and social adjustment and responsibility must be equally 
emphasized. The school is designed for a maximum enrollment of 15-20 students. One administrator, the counselor, and a full time 
office manager staff the office. 
 
All students attend six hours a day from 8:05 am to 2:50 pm. Shelter Cove provides a structured environment in which students are 
taught the skills and knowledge needed for success at the next grade level. The classrooms have computers and internet availability 
for student use. Shelter Cove has a closed campus. 
The students now have access to afternoon electives. They can choose from art, woodshop, culinary arts, music and world languages. 
This has helped them gain skills in areas that are so very important in creating a well rounded student.