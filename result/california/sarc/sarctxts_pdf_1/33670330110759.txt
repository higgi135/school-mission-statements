Mission: John F. Kennedy Middle College high school is a school of choice for self-directed learners desiring a supportive environment 
where they can pursue college and career pathways by taking Norco College and Kennedy courses concurrently. 
 
Vision: John F. Kennedy Middle College high school envisions a school where all students take Norco College courses, complete a 
minimum of 30 Norco College units, and graduate career and college ready with all A-G requirements met. 
 
 
John F. Kennedy Middle College High School was founded in 2006 as an "Alternative School of Choice" in the Corona-Norco Unified 
School District. Built on the campus of Norco College, JFK students attend high school and college classes throughout the school day. 
JFK serves a regional population drawing students from counties including Riverside, San Bernardino, Orange, and Los Angeles and is 
currently one of the largest middle college programs in the nation. 
While we are large compared to other middle college high schools, we are small and intimate when compared with comprehensive 
high schools. Our entire staff works to instill a sense of caring and individual support with each and every student at JFK. As a result, 
we have a school environment where students feel supported to achieve their highest academic goals. 
 
Students at JFK also have the ability to take as many as two years of college courses at Norco College. The opportunity to take college 
classes helps students grow both academically and socially – the expectation is that they will perform and behave like college students, 
and our students respond to these expectations with great success. Students apply to a four-year college or university and receive 
credit for up to two years of their coursework. 
 
The central purpose of JFK is to provide opportunities for students who fall into categories generally perceived as being “under 
represented” to access career and collegiate pathways. While at more traditional high schools, our students were often overshadowed 
by higher achieving students or by those with greater needs. Often they were not engaged in their former schools which resulted in 
their inability to reach their fullest potential. Our approach in working with students is to identify gaps in their learning, effort, and 
achievement. We then address identified gaps by prescribing curriculum, making classes relevant, providing appropriate rigor, and 
building relationships. 
 
Due to the joint focus on supporting the “middle-performing” student and access to college courses, JFK does not offer Advanced 
Placement or International Baccalaureate classes. Indeed, the focus of our school is not on ranking; therefore, we do not select 
valedictorians or salutatorians. Instead, we support all students towards college and career readiness and success while still in high 
school. Colleges and universities accept our students with confidence, knowing they have already met with success in a college 
environment and will transition well to a full-time program of higher education. 
 
JFK Middle College provides a nurturing, academically rigorous environment for students to ensure high school completion and success 
in their careers, college, and beyond. We are creating conscious contributors to society, one student at a time. 
 
 
 

2017-18 School Accountability Report Card for John F. Kennedy Middle College High School 

Page 2 of 15