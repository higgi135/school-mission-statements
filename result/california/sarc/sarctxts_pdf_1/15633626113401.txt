Located at 5601 Harris Road, Bill L. Williams Elementary is named in honor of Dr. Bill L. Williams, who served 37 years as an educator 
including 15 of those years as Superintendent of our District. The school opened on August 26, 1996, and is built on a 10 acre site 
adjacent to a 10 acre park. During the school year, Bill L. Williams Elementary School consists of 1 transitional kindergarten, 5 
kindergarten classrooms, 16 permanent classrooms, 15 portable classrooms, a Title I portable, a speech room, an ACES portable, a 
daycare room, a library, a band/music room, kitchen, multi-purpose room, and administration offices. In addition Williams houses 2 
full time II/C teacher portables. Support personnel included an academic coach, a part-time assistant principal, a speech pathologist, 
nurse, and school psychologist. Instrumental and vocal music teachers are assigned to Bill L. Williams. 
 
The Bill L. Williams Elementary School staff works hard to promote and maintain the District standard of “Excellence in Education.” As 
dedicated professionals our teachers strive to develop every child to his/her fullest potential. Each year, Bill L. Williams’ staff and 
student body continues to grow and take on the Pioneer Pride in a climate that is safe, rewarding and responsive to the needs of our 
students. 
 
Bill L. Williams Elementary takes pride in its reputation as a learning center which is safe, secure, and child-oriented. Throughout the 
years, we have maintained a tradition of high academic expectations. Yet the greatest reward will always be the knowledge that our 
staff is providing the best education possible for a community of future leaders. 
 
We are committed to providing a learning environment and an instructional program that meets the needs of all learners. The goal 
of curriculum and instruction is to enable each student to acquire the academic, social and emotional skills necessary to compete 
successfully at higher academic levels and to exercise the rights and responsibilities of citizenship. To this end, school staff, parents, 
the Board of Trustees, District administration, and the broader community work collaboratively and cooperatively for the benefit of 
each student. 
 
Taking pride in our outstanding academic climate and tradition of service to the school community, Williams Elementary School is 
dedicated to preparing students to be productive and informed citizens of the 21st century. Our united team of dedicated teachers, 
support staff, students, and parents welcomes you to a school where traditional activities and events augment students’ academic 
learning experiences. The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters 
in November 1988. The Report Card, to be issued annually by local school boards for each elementary and secondary school in the 
state, provides for parents and other interested people a variety of information about the school, its resources, its successes, and the 
areas in which improvements are needed. 
 
A standards-based curriculum and sound assessment strategies provide the foundation for continuous improvement of student 
performance and school programs. Students will leave Williams Elementary School with a positive attitude toward learning, a strong 
foundation in basic skills, and the ability to meet future academic challenges. 
 
"We focus today on students reaching their greatest potential to become tomorrow’s confident leaders." 
 
 

2017-18 School Accountability Report Card for Bill L. Williams Elementary School 

Page 2 of 11