From a one-room schoolhouse in Avery to its present location across from White Pines Lake and named for its founding teacher, Hazel 
Fischer Elementary School maintains a small, supportive community feel that would make Miss Fischer proud. Some of Miss Fischer’s 
former students now have grandchildren attending this school. Through parents, staff, and community resources, Hazel Fischer 
Elementary offers an extended variety of learning activities such as painting, ceramics, ski trips, a weekly sing-along, afterschool sports, 
a computer Lab, and Student Leadership opportunities. 
 
Stakeholders have worked together with the Vallecito Union School District to develop goals for the Local Control Accountability Plan 
and School Plan for the site. Schoolwide initiatives support student learning and proficiency in the Common Core State Standards 
(CCSS) and include multiple online and curriculum-embedded formative assessments, progress monitoring in English Language Arts 
using Guided Reading and running records, Multi-Tiered System of Support, the adoption and implementation of curriculum aligned 
with CCSS and professional development focusing on curriculum adoptions. Social Emotional Learning workshops and training for staff 
have included Mindfulness, Trauma-Informed Strategies, and bullying-prevention activities.