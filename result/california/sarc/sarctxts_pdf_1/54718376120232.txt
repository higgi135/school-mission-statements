Vision Jim Maples Academy will prepare our students to be community minded citizens who are academically and technologically 
prepared for their secondary education. Mission Jim Maples Academy engages learners in experiences that foster critical thinking, 
communication, collaboration, and creativity through: 1. A safe and welcoming learning environment. 2. Character development 
through Capturing Kids Heart and Character Counts. 3. Implementation of the Common Core State Standards through STEAM 
curriculum. 4. Cross curricular reading, writing, and discussion. 5. Timely formative assessment and meaningful feedback to teachers 
and students.