Our mission statement: 
At Thorman, we empower GRIT 
 
Gratitude 
Resilience 
Innovation 
Thinking Critically 
 
Again during the 2018-19 school year, Jeane Thorman Elementary School we focus on rigorous standards, engaging strategies 
supported by technology leading to increased student learning and a comprehensive learning experience. Students continue to learn 
the importance of being ALERT- attentive, likable, eager and reliable. 
 
To help accomplish our mission, the administration, students, staff and parents are encouraged to instill five core values that best 
represent Jeane Thorman Elementary. Those five core values are Attentive, Likeable, Eager, Reliable, and Thoughtful (ALERT). We 
believe in the importance of developing resilience within our school community of using grit during tough times to work through the 
difficulties our students face on a daily basis. We are accountable to ourselves, as staff, students, parents and community members 
to prioritize, embrace challenges, set goals, practice, take risks, compete and finish strong in all we endeavor. We will work in unity 
toward common goals with consistency in learning and behavioral expectations, instructional strategies, and supports to aid all 
students in meeting high levels of achievement. Our students will develop the courage and personal integrity to take academic risks, 
to achieve high levels of achievement by setting goals and stretching beyond their comfort levels. 
 
We believe a promising strategy for achieving the mission of Jeane Thorman Elementary School is to develop our capacity to function 
as a professional learning community. Teachers meet weekly to develop common assessments based on essential standards. Together 
they analyze data and share strategies and plans for intervention when needed. In addition to teacher-created assessments, data from 
District formative and summative assessments are used to monitor progress and plan instruction. Students At Thorman, all 
stakeholders have high expectations for all and we accept no excuses for below standard work. With the implementation and 
reinforcement of our core values, as well as the data that is collected to help drive our instruction, we are confident that all students 
will work at the highest of levels.