Merced Elementary School, being highly celebrated, has received commendations including the National Blue Ribbon School of 
Excellence, California Distinguished School, Title One Achieving School Award, California Academic Achievement Award, California 
Business Honor Roll for Educational Excellence, California Golden Bell for Exceptional Academic Achievement, and the California EISS 
Award. Merced’s mission is to build a strong commitment to academic achievement for all students through our guiding principle of 
continually “Building on Excellence.” It is our cumulative responsibility to provide a strong academic foundation and guidance in 
developing strong leadership and citizenship skills. 
 
The school is enriched by a well-stocked library, staffed by a six-hour library clerk each day. Student textbooks are up-to-date to 
support their learning, while new textbooks are adopted in accordance with the District’s textbook adoption cycle. The school 
maintains an inventory to identify, store and account for its valuable property. Sports, recreational and playground equipment is 
available to all students, along with after-school opportunities to participate in intramural sporting and enrichment activities. 
 
Students in need of specialized instruction are mainstreamed as much as possible into the general education setting per their IEP 
goals. The Special Education Teacher works collaboratively with the general education teachers to plan instructional opportunities 
aligned to the core curriculum. Modifications are made to make the core curriculum accessible to students. In addition, as appropriate, 
our special education students attend an Extended School Year (ESY) program in the summer to help maintain the knowledge learned 
during the regular school year. The Teacher on Special Assignment (TOSA) has welcome meetings with foster youth to ensure they 
have all the necessary resources to be successful at Merced. Moreover, she monitors their academic progress and recommends for 
intervention as needed to help address any achievement gaps. 
 
Merced dedicates its focus on citizenship and academics with engagement and critical thinking, problem-solving, understanding 
informational literacy and student postsecondary education or career options. It is evident that all Merced students are positive about 
their learning experiences and arrive happy and eager to learn. Merced's core mission is to prepare its scholars with 21st Century 
skills. 
 
Major Achievements: 

• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 

2017 Top WCUSD Public Elementary CAASPP Score in both ELA and Math 
2015 WCUSD Board Recognition for Meeting PTA Membership Goals 
2015 Honor Roll School Campaign for Business and Education Excellence 
2014 Business for Education Excellence Honor Roll School 
2013 Honor Roll STAR School California Business for Education Excellence 
2013 Title I Academic Achievement Award 
2013 Top WCUSD Public Elementary API Score in both ELA and Math 
2012 Title I Academic Achievement Award 
2012 Top WCUSD Public Elementary API Score in both ELA and Math 
2011 National Blue Ribbon School of Excellence 
2011 Title I Academic Achievement Award 
2010 Title I Academic Achievement Award 
2010 California Distinguished School Award 
2009 California Business for Education Excellence Honor Roll School 
2005 Bravo! School of Distinction for Visual and Performing Arts 
2004 California Academic Achievement Award 
2003 California Distinguished School Award 
2003 National Blue Ribbon School of Excellence 
2003 California Title I Achievement Award 
2003 Golden Bell Award 

2017-18 School Accountability Report Card for Merced Elementary School 

Page 2 of 12