Students at Death Valley Academy are guided by specific rules and classroom expectations that promote respect, cooperation, 
courtesy and acceptance of others. The goal of Death Valley Academy's discipline program is to maintain an orderly, stimulating and 
productive working environment. Parents and students are informed of school rules and discipline policies through the Student 
Handbook and statement of parents rights and responsibilities which are sent home at the beginning of the school year or upon a 
student's enrollment at Death Valley Academy. 
 
Students are encouraged to participate in the school's academic and extracurricular activities, which are an integral part of the 
educational program. These schoolwide and classroom activities promote positive attitudes, encourage achievement, and aid in the 
prevention of behavioral problems. Extracurricular activities focus on educational and recreational field trips and targeted athletic 
activities. 
 
Death Valley Academy recognizes and celebrates the achievements and successes of students and staff on a regular basis. Students 
are recognized for their achievements during awards ceremonies throughout the school year.