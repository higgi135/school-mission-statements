At Palms Elementary we believe all students can learn. Therefore, we continue to have high expectations for student learning. Our 
instructional program is based on the Common Core State Standards, which outline what students need to know and learn at each 
specific grade level. We emphasize high-quality instruction and employ a wide variety of instructional tools, strategies, and formative 
assessments to monitor instruction, improve teaching and ultimately achieve high levels of learning for all students. 
 
Teachers, the assistant principal, and principal continue to enhance their skills and knowledge through professional development in 
the core content areas of English Language Arts, Mathematics and English Language Development. Additionally, Social Emotional 
Learning is also a focus, as we believe it is important that our students develop the skills necessary to manage emotions, establish and 
maintain positive relationships, set goals, monitor their progress towards achieving their goals, and ultimately experience increased 
academic success. To ensure we are meeting the diverse needs of all students, Palms has embraced the Professional Learning 
Communities (PLC) model where we encourage and promote collaboration among teachers, coaches, resource personnel, and 
classified staff, as a means to establish and maintain a high performing team that is committed to ensuring high levels of learning for 
all students. 
 
Parents and community have an informed partnership with the schools. We welcome your participation in our school and encourage 
our stakeholders to join and participate in the many parent involvement opportunities offered throughout the year. If interested in a 
more long-term commitment, the Family Teachers Association (FTA), Family Involvement Action Team (FIAT), School Site Council (SSC), 
or our English Language Advisory Committee (ELAC) might be of interest. For our dads, we offer the WATCH D.O.G.S program; for our 
moms, we have started a new program called MOMS (Moms of Marvelous Students), and for those looking to simply relax and enjoy 
time with their family and friends, we offer a host of fun activities throughout the year such as family reading, math, and art nights, 
as well as a variety of dances, movie nights, evening performances, etc.. 
 
We continue to use our resources to carry out the School Plan for Student Achievement. The purpose of this plan is to improve student 
performance and to ensure that all students succeed in reaching academic standards set by the State Board of Education. 
 
Kathleen Rittikaidachar, 
PRINCIPAL