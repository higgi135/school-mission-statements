MISSION STATEMENT 
 
At Redwood Middle School, our mission is to develop lifelong learners who communicate, collaborate, and think critically, creatively, 
and independently. We encourage students to embrace the challenges that are necessary to become outstanding citizens and scholars. 
Redwood is a community that strives to maintain a safe, nurturing, and academic environment where there is a place for everyone. 
 
SCHOOL DESCRIPTION 
 
Redwood Middle School is located in the heart of Thousand Oaks, with an enrollment of approximately 800 students in Grades 6, 7, 
and 8. 
 
Redwood is committed to making sure that each student feels comfortable and safe before, during, and after school each day. Our 
campus supervisors, administrators, and counselors work together throughout the day to ensure the safety of each student. Student 
safety is and will continue to be a top priority at Redwood. Redwood believes that the values that are essential to a school’s success 
are mirrored in the school community and that a strong school/community connection supports the standard of excellence that has 
been achieved at Redwood. We value our relationships with the community and with each Viking family. We encourage parents to 
communicate with staff via phone or email at any time. Parents are also encouraged to stay active in PTSA, SSC, and the many volunteer 
opportunities that come up throughout the year. With the help and involvement of the entire Redwood community, we will continue 
with our longstanding tradition of excellence. 
 
Maintaining high expectations, we at Redwood are committed to providing all students with access to relevant and rigorous academic 
curricula. Recognized as a California Gold Ribbon School (2017) and a California Distinguished School (2013), Redwood offers 
outstanding academic as well as extracurricular opportunities.