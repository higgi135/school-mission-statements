The present facility of Modoc High School was constructed in 1939 and remains an educational and social hub for the community of 
Alturas. Modoc High School services a community of approximately 3,000 people and a county of about 9,000. While we are small and 
remote, our students have many of the same educational opportunities that large schools offer, as well as some they cannot. 
 
Currently, Modoc High School serves approximately 260 students in grades nine through twelve. Modoc High School has a strong 
academic core curriculum of English/language arts, social science, science, and mathematics. We generally offer Advanced Placement 
(AP) classes in art, English, math, and science. For a small school, we offer a wide variety of electives, including agriculture, visual art, 
music (instrumental & vocal), Spanish, drafting, welding and woodshop. Graduates from Modoc High School have been accepted into 
military academies, the University of California system, the California State University system, out-of-state universities, Ivy League 
schools, and a variety of junior colleges and technical schools. Every year several graduates proudly serve our country in the military. 
Athletic activities at Modoc High School are diverse and always competitive. Though the Griswold Gymnasium, named after legendary 
coach Wally Griswold, is over 35 years old, it continues to function as a state-of-the-art facility. Our maintenance department does an 
excellent job of maintaining our grounds and our practice and game fields are in game-day condition on a year-round basis. We 
welcome and invite students, parents, and visitors to visit our campus. 
 
Buildings and Grounds: 
 
Our main building was built in 1939 and is well maintained, housing two offices; 15 classrooms; including two science labs; a library 
with computer stations; a computer lab; a multipurpose room used for cafeteria and community/student events; and student 
restrooms. Additional buildings provide classrooms for resource students, agriculture, welding and PE. The gymnasium serves for 
instruction, athletics, and for community events. T here are two large fields, which serve also for instruction, athletics, and community 
events. We have 21st century science wing, and have nearly completed our remodeling of the south wing. New bleachers have been 
installed at the football field and boilers have been modernized. Two custodians keep the facilities clean. Each classroom is cleaned 
every day. District maintenance picks up litter, removes graffiti, andmaintains landscaping on a regular weekly schedule. 
 
Library: 
 
Modoc High School offers a library that holds approximately 4,000 volumes and 24 Internet-connected computer stations, which serve 
individual students and classes as a computer lab. The library is open for teacher and classroom use when scheduled. 
 
Computers: 
 
Sixty-five computers, 180 Chromebooks, and more than a dozen iPads are available for student use; averaging one computer for every 
five students. Fifteen classrooms are connected to the Internet. In addition to the lab in the library, a computer lab with 28 computers 
and a laptop cart with 30 computers serves for student use and computer related instruction. Teachers use the AERIES program to 
keep attendance, track grades, and communicate with parents. Computers are also used extensively to design materials for their 
students. All of our teachers use email as a communication tool with other staff, parents, and colleagues 
 
 

2017-18 School Accountability Report Card for Modoc High School 

Page 2 of 12