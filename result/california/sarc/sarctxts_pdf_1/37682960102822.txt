Students are at the center of our school, and our relationship with them drives all decisions made at Stone Ranch. Our initiatives 
include staff collaboration, curriculum alignment, implementation of California State Standards, student goal setting, and focused 
intervention programs. Each of these initiatives has at its core the success of our students. We are about respecting and recognizing 
children, helping them learn, explore, and discover. Our vision is to provide educational opportunities which will create self-directed 
students capable of succeeding in a rigorous, standards-based curriculum. Our staff uses core curriculum resources, and creates 
standards based goals centered around students' individual and group needs. To ensure that our students are prepared for college 
and other post-secondary success, we provide personalized learning by inspiring students to be self reliant problem-solvers who will 
achieve their fullest potential. 
 
Through goal setting and encouraging students to be actively involved in their education, we will build the foundation for them to be 
citizens who recognize that effort and persistence matter, and that their self-direction and initiative will make their learning relevant, 
valuable, and meaningful. All Trailblazers will be "college ready" and prepared for a range of opportunities and choices made available 
to them as they promote to middle school and beyond to pursue their individual interests and passions. To this extent, teachers build 
on students' prior knowledge, life experience, and interests to achieve learning goals. Instructional strategies will be adjusted to the 
needs of the students, with a focus on critical thinking skills. Stone Ranch continues its tradition of focusing on students' reading-
writing connection to provide self directed and reflective learning for all students. 
 
Stone Ranch has a limited number of reportable sub-groups. Ethnically, overall our Asian (35%) and White (38%) populations are our 
significant subgroups. Our students qualifying for Economic Aide (8%) is relatively small compared to the school. Our designated 
English Learner student population is 20%. We provide an environment that fosters differentiation and meets the needs of all learners 
including English Language Learners (ELL), Gifted and Talented Education (GATE), and Special Education (Speech and Language, RSP, 
OT, APE). We have both an ASD Pre-school class and RSP Integrated Pre-school class. We are proactive and offer early intervention 
support programs, preschool, and Transitional Kindergarten (a two-year Kindergarten program), that help all of our children develop 
the language and foundational skills necessary to ensure academic success. Our ELL providers and Special Education team work 
collaboratively with the classroom teachers and administration. We offer intervention support through our ELL program with two 
highly-qualified ELL aides, and an equally qualified classroom teacher acting as our coordinator. As each ELL student has unique needs 
our students' ELPAC scores are accessed and instruction is provided in specific targeted areas of need. Progress is monitored 
throughout the school year. Targeted individualized instruction is at the forefront of closing the achievement gap for all subgroups, as 
each student is first and foremost an individual. 
 
We realize that the standards for English Language Arts includes Listening, Speaking, Reading and Writing, and that developing this 
complete set of skills benefit all our students, and extending opportunities for our students to develop these language skills include a 
focus on oral presentations, extensive writing instruction, and student selected reading (combined with structured, standards based 
curriculum). Students write across the curriculum - in all subject areas - and recognize the reading-writing connection. They are 
provided opportunities, in their class as well as cross-grade levels, to share their work and develop their public speaking skills. Teachers 
collaboratively studied the key shifts in math focus impacting instruction so our students can: make sense of problems and persevere 
in solving them, reason abstractly and quantitatively, construct viable arguments and critique the reasoning of others, model with 
mathematics, use appropriate tools strategically, attend to precision, look for and make use of structure, and look for and express 
regularity in repeated reasoning. 
 
 

2017-18 School Accountability Report Card for Stone Ranch Elementary School 

Page 2 of 11