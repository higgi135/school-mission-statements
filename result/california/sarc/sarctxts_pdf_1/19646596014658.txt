Welcome to Palm Crest Elementary School, home of the Panthers! Established in 1956, Palm Crest is proud of its tradition of excellence 
and achievement. We are continually impressed by the hard work our 675 students in grades kindergarten through sixth grade and 
our 85 faculty and staff members accomplish every day. 
 
Palm Crest staff provide students with a safe, nurturing, and rigorous academic program that allows students to grow in their own 
individual ways. Teachers are committed to preparing students for the future by embedding 21st century learning such as critical 
thinking, writing skills, and peer collaboration through Common Core State Standards (CCSS) and the Next Generation Science 
Standards (NGSS). Regular integration of technology and cross curricular lessons also provide students with opportunities to access 
curriculum at a deeper level. Teachers collaborate regularly to analyze and reflect on student performance data, and guide instruction 
to meet the needs of all students. Strengths among colleagues are honored, ideas are shared, and fresh ways are sought to build 
student character and self-esteem ensuring well-prepared and confident 21st century learners. 
 
In addition to a challenging and meaningful academic instruction, students benefit from regular classes in art, music, drama, computer 
technology, and physical education. Enrichment opportunities abound, including optional Spanish for students in grades three through 
six and after school activities.