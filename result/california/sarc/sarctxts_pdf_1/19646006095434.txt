School Description: 
The Hermosa Beach City School District (HBCSD) was established in 1904. The district is located in the southwest portion of Los Angeles 
County and serves the community of Hermosa Beach. The district serves approximately 1,350 students and is located on two sites. 
Hermosa Valley is located in the heart of the city along Valley Drive near Pier Avenue. Hermosa Valley School provides a comprehensive 
4th-8th grade program that is focused on a vision of empowering our students to achieve and thrive through the lens of making 
connections. Students are exposed to a variety of enrichment opportunities through Music, STEAM, Mindfulness, Garden/Nutrition, 
Physical Education, and a technology rich blended learning approach. Teachers and students build relationships around purposeful 
curriculum and lessons using the reading and writing workshop model, cognitive guided approach to math instruction, understanding 
phenomena with hands on science, and interactive social studies instruction. 
 
Hermosa Valley drives decisions through the lens of making connections. Music instruction stimulates brain development, creates 
human connection, integrates a variety of curriculum, teaches discipline, and relieves stress. The school celebrates music through 
shared performances. STEAM introduces students to design thinking, real world problem solving, collaborative team work, and 
production for authentic audiences. Our STEAM focus is weaved throughout a student's experience at Valley. Mindfulness increases 
focus, attention, self-control, classroom participation, compassion, and empathy. Students who are mindful are able to increase 
academic performance, resolve conflict, and improve overall well-being. The school garden provides a platform for students to become 
environmental stewards and responsible caretakers, builds community within and outside of the school on a large and small scale, 
introduces students to tenets of a healthy lifestyle, and provides a real world, soil rich on-site laboratory for exploration. Students 
benefit from physical activity mentally and physically. Exercise and activity reduces health risks, improves cardiovascular health, 
maintains strong and healthy bodies and minds, and reduces anxiety among students. Valley students are encouraged to walk, bike, 
or scooter/skateboard to school. Recess and lunch provide time for students to interact, play, engage, and be active. Students also 
receive instruction from physical education teachers to help connect lessons with team building. Students have access to one-to-one 
chromebooks in their classrooms where they can research, record, document and share through Google Classrooms. The enrichment 
coupled with instruction, provides a balanced, unique, and connected approach for our students so they can achieve and thrive. 
 
As part of a small school district, we enjoy a high level of communication and collaboration throughout the district, school 
administration, students, teachers, parents, and local stakeholders. Our community dedicates time and resources to help support the 
vision of the school and to demonstrate to their children the importance of education and service. Hermosa Valley provides a well 
designed, challenging environment for all students where they thrive. 
 
Hermosa Valley has been awarded the following accolades: 
2018: 1st Place, Sandyhook Promise: Start With Hello Community Reach and Sustainability Award 
2018: Middle School Science Teacher (8th grade), Northrop Fellow Award Winner 
2018: California Distinguished School 
2018: Exemplary Physical Activity and Nutrition Education School 
2017: Elementary STEAM Teacher, Northrop Fellow Award Winner 
2017: ASCA:SBAA Administrator of the Year, Principal Middle Grades 
2015: ITEEA Program Excellence Award 
2014: ITEA Outstanding Middle School Technology Program of the Year 
2014: California Distinguished School 
2005: National Blue Ribbon Status 
2004: California Distinguished School Award 
 

2017-18 School Accountability Report Card for Hermosa Valley Elementary School 

Page 2 of 13 

 

Vision & Mission Statement- On December 14, 2016, the HBCSD Board of Directors updated the vision and mission to guide 
educational decisions and assist with moving the organization forward through a common lens. Our vision: All students are 
empowered to learn and thrive. Our mission: In partnership with our community, prepare students to thrive by providing a relevant 
education, a safe and healthy environment, and an inclusive culture. 
 
We Are Committed To An Environment That Supports Learning And Includes These Core Values: 
Community Engagement 
Culture of Excellence 
Global Citizenship 
Respect and Inclusion 
Responsible Stewardship 
Rigor and Relevance 
Safety and Well Being