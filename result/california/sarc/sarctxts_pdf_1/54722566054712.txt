Description of District 
The Visalia Unified School District is the oldest school district in Tulare County. The district is comprised of 25 elementary schools, 5 
middle schools, 4 comprehensive high schools, 1 continuation high school, 5 charter schools, 1 adult school, and a school that serves 
orthopedic handicapped students. Over 32,000 students Pre-K to adult are served through the Visalia Unified School District. 
 
Description of School 
Veva Blunt Elementary School served approximately 700 students in grades K-7 in 2017-2018. Our teachers and staff are dedicated to 
ensuring the academic success of every student and providing a safe and productive learning experience. The school holds high 
expectations for the academic and social development of all students. Curriculum planning, staff development, and assessment 
activities are focused on assisting all students in mastering the state academic content standards, as well as ensuring appropriate 
social development. Positive Behavior Intervention Systems are an integral part of the Veva Blunt School structure encouraging 
P.R.I.D.E. - Perseverance, Respect, Integrity, Discipline and Etiquette to each of our students. Parent Teacher Club is active and plays 
a vital role in connecting our families, community and staff towards the singular purpose of providing experiences and support to our 
students. 
 
School Mission Statement 
Our mission is to promote high academic achievement, maintain a safe and respectful learning environment, value diversity, and to 
foster a strong collaborative partnership between students, parents, staff and community. 
 
The staff, parents, and community of Veva Blunt Elementary School work together to provide a high quality education for all students. 
Our mission is to help students to: 

• develop skills, strategies,and confidence to master rigorous grade level standards, 
• 

foster the love of learning and ensure that our students grow up to be life-long learners, critical thinkers, persevere when 
faced with challenges and become productive citizens. 
attain strategies, study skills, and communication skills 

• 
• become resourceful, flexible, and curious learners who see the value in acquiring new skills as they progress through their 

educational career and into the work place, 

• develop a confidence in our school climate and culture where they feel safe and academics are priorities and individuality 

is valued. 

We believe that every student has the ability to learn at high levels, and that his or her intellectual, social and physical needs will be 
met to ensure they leave Veva Blunt with a solid foundation to continue to build upon as they move to middle school or other schools 
during their elementary education. 
 

2017-18 School Accountability Report Card for Veva Blunt Elementary School 

Page 2 of 11