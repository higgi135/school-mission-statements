Principal’s Message 
Elinor Lincoln Hickey Jr./Sr. High School is a community school dedicated to helping at-risk youth reach their full potential academically, 
behaviorally, and socially. While the focus at Elinor Lincoln Hickey Jr./Sr. High School is academic success, teachers and staff 
understand that before learning can take place we must first address the deeper issues that prevented success. Once emotional needs 
are addressed, students can increase their literacy and mathematics competencies, earn credit, gain job skills, graduate from high 
school and transition into a post-secondary environment. Besides providing strong behavioral supports, the staff works with students 
to develop an individual success plan created to help set realistic goals they can accomplish while at our school. Our students are 
exposed to data driven instruction designed to remediate and accelerate the curriculum based on the student’s individual strengths 
and weaknesses. We provide a small campus setting with a modified schedule that mirrors a comprehensive program while allowing 
staff and students to forge close, trusting relationships. Each student is encouraged to participate in mentoring and extra curricular 
activities. Guest speakers, assemblies, field trips, vocational goal setting, career exploration/readiness and academic tutoring are just 
a few opportunities for our students.