Principal's Message 
St. Helena High School (SHHS) is a small accredited school that offers rigorous coursework in both academic and Career Technical 
Education (CTE) arenas. Although our student population is small, the school boasts scholastic offerings similar to those of schools 
with large student populations. SHHS staff provides students with a quality education in a safe and nurturing environment. 
 
At SHHS, we strive to provide our students with an outstanding education that ensures each student will be prepared to meet the 
demands of their chosen postsecondary option. The importance placed upon perseverance and hard work echoes in every aspect of 
the curriculum and school culture. Students are encouraged to develop their unique abilities and interests in an atmosphere that 
celebrates their successes while challenging their intellectual, social and physical capacity. Our ultimate goals are to assist students in 
finding their voice in a community of learners while exploring areas of interest, ultimately ensuring college and career readiness. 
 
St. Helena High School offers Advanced Placement, Honors, college preparatory, Dual Enrollment, and elective courses and five CTE 
pathways. The school teaches students to be globally competitive citizens armed with the necessary 21st-century skills to be successful 
in postsecondary educational institutions or the workforce. The school has a vibrant CTE program as well as an AVID (Advancement 
Via Individual Determination) program that boasts a 95 percent college acceptance rate. Students are able to receive academic and 
social counseling with the assistance of two counselors and Student Support Services coordinator. 
 
In the 2011-12 school year, the high school implemented a two-day block schedule and incorporated weekly time for students to meet 
with their teachers for remediation, review or enrichment. In the 2012-13 school year, the high school continued to target students 
with academic difficulties and/or students in need of additional support to help them achieve their academic goals. Through ACCESS 
period, the continual monitoring of student progress and assessment data, adoption of PowerSchool Learning Management System, 
and Aeries Gradebook, the high school is working to increase student learning and school-to-home communication. 
 
St. Helena High School is an incredible educational institution with an outstanding teaching staff committed to student academic and 
personal success. Teachers receive professional development regularly, focusing on improving instructional practices through the 
Gradual Release of Responsibility model, Advanced Placement, infusing technology into the curriculum, etc. 
 
School Mission Statement 
The St. Helena Unified School District community is dedicated to providing opportunities and resources for all students to achieve 
their highest academic and social potential in a global society. 
 
School Vision Statement 
Our vision is to be an inspiring and innovative learning environment with the highest expectations for all students and staff so that 
they are successful academically, socially, emotionally and physically. 
 
 

2017-18 School Accountability Report Card for St. Helena High School 

Page 2 of 14