Horizon is a TK-5 elementary school that opened its doors to students on August 8, 2016. The student population as of October 2018 
is 511. The school has a total of 20 certificated teachers, a principal, a learning director, a counselor, a part time psychologist, and a 
part time speech therapist. We have 1 -6 hour instructional aide, 2-3.5 hour instructional aides, and a library technician. Horizon also 
benefits from a district technology coach who works directly with teachers to support the integration of technology into lessons. 
 
Vision 
We will help our students become confident, responsible, and productive citizens by providing a nurturing environment and a high 
quality, well-rounded, and innovative education. 
 
Mission 
We will maintain continual improvement in academic achievement and be one of the leading learning communities in Kern County.