Mission Statement: 
Culver City High School is a diverse, engaged and caring academic community that embraces the unique identity of each individual 
and their contributions to our shared experience. We believe in providing opportunities that cultivate and foster self-discovery through 
Academics, Activities, Arts, and Athletics. Students and staff are challenged to reach their full potential while building positive 
relationships and engaging in meaningful learning experiences to become contributing members of our community and beyond. 
#CentaurPride #CulverPride 
 
Five school-wide goals serve as the focus for our staff: 
 
1. All students w ill graduate. 
2. All students w ill be proficient in ELA and Math. 
3. All students w ill be college/career ready. 
4. All students in 9th grade w ill earn 60 credits. 
5. All students w ill participate in athletics and / or extra-curricular activities. 
 
Culver City High School is located in the heart of Culver City, nestled in among a residential neighborhood bordering the Ballona Creek. 
Sony Pictures Studios is the largest neighboring industry, and has been a valued business partner for the school. The partnership has 
fostered the development of the CCHS Academy of Visual and Performing Arts and the development of career technical education 
(CTE) courses with a focus on media. Culver City High School (CCHS) is the sole four-year comprehensive high school within the Culver 
City Unified School District and is fed by one middle school and five elementary schools. CCHS is a bastion of diversity where students 
from all backgrounds find comfort, safety, and acceptance. This infusion of students from all over the county of Los Angeles provides 
for a unique and embraced opportunity for CCHS to be one of the most diverse large comprehensive high schools in the nation. 
 
CCHS focuses on readiness for college, career and life as an actively involved citizen in a rapidly changing society. CCHS is driven by a 
desire to help all students reach their full learning potential while meeting the school’s agreed upon goals and student outcomes. 
While Culver City High School prides itself on a rigorous academic program with 29 Honors and Advanced Placement (AP) courses 
offered to students, the school places equal emphasis on educating the “whole child” by offering a wide variety of college preparatory 
courses, career exploration coursework, the arts and athletic programs to all students. Culver CCHS believes strongly that all students 
must be involved in an extra-curricular, co-curricular after school programs, athletic programs, and/or be involved in an activities-
affiliated club. The 4 As - Academics, Activities, Arts, and Athletics drive all aspects of Culver City High School. School-wide decisions 
are based upon empowering each of these core principles.