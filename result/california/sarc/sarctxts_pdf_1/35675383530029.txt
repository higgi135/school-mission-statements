San Andreas Continuation High School is operated by the San Benito County Office of Education. The staff believes all students can 
learn, given high expectations. Education is a shared responsibility which includes parents, students, staff, and the community. The 
mission of San Andreas Continuation High School is to provide students with a safe, positive, learning environment with modern 
resources and accommodations necessary to learn. The goal is to educate all students to their highest potential, so they will have the 
greatest range of personal options upon graduation. It is the 21st century vision of San Benito Alternative Education sites to deliver 
meaningful, accessible curriculum through multiple avenues in a safe, supportive, positive learning environment. With the help of our 
entire community we will empower our students by developing the necessary skills needed to join our complex world as an engaged 
citizen. 
 
San Andreas Continuation High School serves youths in grades 10-12. Students with probationary status who attend are admitted 
based upon of the following criteria: referral by San Benito County Juvenile Court School; referral by another school district when the 
student has been unsuccessful; recommendation from the San Benito County Student Attendance Review Board (SARB) for habitual 
truancy; when a student moves to San Benito County and had been attending a community day school in their previous county of 
residence. 
 
Average daily enrollment for San Andreas Continuation High School is approximately 90 students. However, over the course of a school 
year this site can serve approximately 120. 
 
Teachers at San Andreas Continuation High School consistently align their curriculum maps, lessons and formative assessments to the 
Common Core State Standards to achieve positive classroom results. San Andreas teaching staff provides core academic courses in 
English Language Arts (ELA), Algebra Readiness, Algebra, Algebra 2, Geometry, History/Social Science and Science. In addition, migrant 
education and special education services are available, as well as a long-term independent study program. Computers are available to 
students within the classroom as San Andreas Continuation High School embraces technology and has a Google Chrome Book for every 
student. Each student is assessed in English and math. Personal and/or crisis counseling is provided by the school counselor and San 
Benito County Behavioral Health. The principal and various other staff members provide additional mentoring and coaching in the 
areas of communication, grades and social development. San Andreas also uses Restorative Justice and is in the process of fully 
developing this program. 
 
San Andreas Continuation High School provides a safe, organized and supportive environment that offers challenging and equitable 
opportunities for all students, thereby promoting equity and diversity. It fosters academic achievement of all students, while 
developing vocational and interpersonal skills required for success in a rapidly changing and technological world. The staff of the 
Community School instills a strong work ethic and respect for the community effort, while preparing students for active and productive 
roles in society as adults.