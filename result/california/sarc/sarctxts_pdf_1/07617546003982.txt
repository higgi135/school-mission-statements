Dream · Challenge · Lead 
 
Working together as a community of learners, we will inspire and empower all students within a safe, supportive environment to 
develop 21st Century academic and interpersonal skills. 
 
March 21, 2014 
 
Our Mission: 
 
To provide students a learning experience that embraces high expectations for all. 
 
We focus on these goals: 

 

• Promote respect and acceptance of ourselves and others. 
• Provide a safe and secure environment for students and staff. 
• 

Establish inclusion that connects the students’ background and experience to the educational program so that every 
student reaches optimal success. 

• Provide effective two-way communication and interaction between school and home. 
• 

Empower staff and the school community to participate in the decision-making process. Foster pride in ourselves and in 
our school community.