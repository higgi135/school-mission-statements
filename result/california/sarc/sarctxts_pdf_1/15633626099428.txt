Actis Junior High School is committed to providing a learning environment and an instructional program that meets the needs of all 
learners. Our mission is to inspire all students to be respectful, responsible, and honest, in learning and in life. The goal of curriculum 
and instruction is to enable each student to acquire the academic, social and emotional skills necessary to compete successfully at 
higher academic levels and to exercise the rights and responsibilities of citizenship. To this end, school staff work cooperatively and 
collaboratively with parents for the benefit of all students. 
 
Specific academic goals are based on the District courses of study and various state frameworks. Progress towards the 
accomplishment of our goals is regularly monitored through performance indicators and annual self-studies. Regular meetings are 
held for staff, departments, leadership team, parent club, and site council, in order to assure that we are addressing the current needs 
of our students. Our well-defined vision and goals, help Actis students develop attitudes that enable them to be lifelong learners, 
engaged in their education through a variety of strategies. 
 
Actis takes pride in our outstanding academic climate and tradition of service to the school community, and we are dedicated to 
preparing students to be productive and informed citizens of the 21st century. Our united team of dedicated teachers, support staff, 
students, and parents welcomes you to a school where traditional activities and events augment students’ academic learning 
experiences. 
 
The School Accountability Report Card (SARC), which is required by law to be published annually, contains information about the 
condition and performance of each California public school. More information about SARC requirements is available at the California 
Department of Education (CDE) Web site at http://www.cde.ca.gov/ta/ac/sa/. For additional information about the school, parents 
and community members should contact the school principal or the District Office. DataQuest, an online data tool at 
http://data1.cde.ca.gov/dataquest/, contains additional information about this school and comparisons of the school to the District, 
the county, and the state. 
 
Actis Junior High School was established in 1979. The Actis staff is dedicated and focused on junior high students’ unique physical, 
emotional and social needs. Activities and curriculum are developed to assist our students’ needs and to support and maintain the 
concept of “Excellence in Education,” as defined by the Panama-Buena Vista Union School District. Our mission is to prepare all 
students for 21st century career and citizenship. We will accomplish this by providing a solid academic foundation, an awareness that 
we live in a rapidly changing technological world, the skills and critical thinking needed for success in higher education, the preparation 
necessary to enter the work force, a menu of exploratory electives, and the guidance necessary to ensure healthy choices.