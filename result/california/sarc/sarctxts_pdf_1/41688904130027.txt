We recognize the importance of offering an alternative learning environment which provides students with an opportunity to: 

complete the required academic course of instruction to graduate from high school. 

• 
• participate in a program designed to meet the educational needs of each pupil, including but not limited to, Independent 

Study, Work Experience and Career Technical Education as a supplement to classroom instruction. 
receive holistic support for their physical, emotional and mental health. 

• 

 

In this type of atmosphere, we believe we can help students to: 

• clarify their personal and educational values 
• develop a positive self-concept 
• achieve their personal and educational goals 
• become more productive citizens 

Lastly, the Pilarcitos High School family works together to create: 

• a peaceful learning environment 
• a safe setting to explore academic, social, and emotional success 
• an atmosphere of unconditional support and guidance to continue on our path of lifelong learning