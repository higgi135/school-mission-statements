Robertson Road Elementary is situated in the far west section of the city of Modesto in Stanislaus County. The school is over forty 
years old and is currently composed of four major buildings and several portable structures. Robertson Road is a traditional school 
site composed of approximately 400 students from various ethnic backgrounds. There is also a fully functioning Children’s Center 
providing full day preschool, Head Start, and enrichment programs for Robertson Road families 
 
The school is an integral part of the community, providing health and social services for students and families through the Healthy 
Start program and Golden Valley Clinic. 
 
The site also offers a Healthy Start Program which enables parents to become better educated to assist their own children with their 
education. This program provides classes for parents in both English and Spanish languages, enabling them to become more involved 
in their children’s education. Our top priorities and goals are Student Achievement, Safety and Attendance. 
 
It is the aim of Robertson Road School to develop our students’ skills, abilities, attitudes, and talents, empowering them to work with 
others successfully in meeting the challenges of an ever-changing world. It is also the aim of the Robertson Road Elementary School 
to help all students develop appropriate behavior, self- discipline, decision-making abilities, and respect for others