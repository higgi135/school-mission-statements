Sierra School's mission is to cultivate working partnerships with families and community members to establish and sustain an enriched 
educational environment for each child. We are committed to empower all children to meet the challenges of an ever changing world 
with unity, responsibility and success. 
 
Sierra School serves 459 Transitional Kindergarten through fifth grade children. Sierra is located in the foothills of the Sierra Nevada 
Mountains, in the small city of Placerville. Sierra maintains high academic expectations for all students, and has been rewarded for 
academic success at both the state and national level. Sierra was named a Title I Academic Excellence School in 2008, 2009 and 2010. 
Sierra was named a California Distinguished School in 2008, and in 2009 Sierra was awarded the highest honor when we were named 
a National Blue Ribbon School of Excellence. In 2009, 2013, and 2014 Sierra was also named a California Business for Education 
Excellence Honor Roll School. 
 
The staff at Sierra School believes that every student can learn. Systems and structures are in place to support students wherever they 
are in the learning process to insure they meet their full potential. Students performing below, at, and above grade level are supported 
in their learning through differentiation in the classroom and by our intervention team. 
 
Sierra has a full day kindergarten program and in 2013 added a transitional kindergarten class. Our full day program, which began in 
1996, has allowed for more rigorous academic development, while still allowing ample time for the developmental processes 
important to kindergarten aged children. Sierra’s reading program is strong in phonics throughout the first four years of a child’s 
education. Flexible reading groups are part of our reading program in grades first through third. Flex Reading groups are based on 
students’ reading levels and are across grade levels. Kindergarten students can also test into flexible reading groups if they already 
have knowledge of letter names and sounds. Flex reading classes meet Monday through Thursday. Intermediate grade teachers 
continue to reinforce phonics skills learned in lower grades. Fourth and fifth grade students are placed in flexible leveled reading and 
math groups. These groups are referred to as WIN (What I Need) Groups. These groups change on a regular basis as determined by 
grade level common formative assessment data. Students are provided with remediation, grade level practice, or enrichment 
depending on their needs. 
 
Sierra has 25 fully credentialed teachers serving children in grades transitional kindergarten through fifth. 
 
Teachers at each of Sierra’s seven grade levels work closely together on curriculum and planning in grade level teams. Children are 
given many and varied opportunities to become academically successful. The Learning Center and a self-contained Special Day Class 
serve the needs of Sierra’s identified special education students. Sierra has two fully credentialed teachers working with special 
education students. In addition to the special education offerings, there is a Title I program serving the needs of non-special education 
children who score below the proficient level on common formative assessments and on district benchmark tests. Teachers work 
closely to analyze, monitor, and respond to assessment data. 
 
Sierra has an extended day program beginning at 6:45 each morning. Students enrolled in the extended day program may remain 
after school until 6:00 p.m. The program operates full days for the same hours during summer months and many holiday periods. 
Sierra is also part of the ASES Grant which involves a community partner with Boys and Girls Club to provide quality after school care 
for our students. Enrollment fees for Sierra students are waived, and free transportation to and from the Club is provided by our 
district. 
 
Parents are very important to the school. They are active as classroom volunteers, in the Parent Club/Team Sierra, and on the School 
Site Council. Back-to-School nights, Open Houses, Family Reading Night, carnivals, ice cream socials and other school and Team Sierra 
activities are well attended and are an important adjunct to other school programs. Sierra is a school where parental involvement is 
desired and encouraged. Buildings and grounds are regularly used for community sports and by other community organizations. 
 

2017-18 School Accountability Report Card for Sierra Elementary School 

Page 2 of 10