Thomas Jefferson Elementary School is on of 19 schools in the Roseville City School District. It opened it doors in 2004 and our staff is 
honored to serve our neighborhood community by providing a quality transitional kindergarten through the fifth-grade program that 
is grounded in quality first instruction. Since the school’s opening, our staff has strived to be innovative and trendsetters with our 
teaching. The Thomas Jefferson curriculum is based on the rigorous Common Core State Standards. 
 
Thomas Jefferson is committed to the individual needs of our students. We are dedicated to improving student performance, teacher 
instruction, and the enhancement of knowledge and skills required for working, living, and learning in our ever-changing society. Our 
students participate in a range of programs including art, music, physical education, media, social-emotional learning, and technology. 
Each day our students are taught to model character, integrity, and maturity through our PBIS programs as they strive to grow into 
productive citizens. The Thomas Jefferson Students embrace the overarching PBIS expectations of R.I.S.E., where students are taught 
to and encouraged to; show Respect, show Integrity, be Safe and be Engaged. 
 
The Thomas Jefferson Mission Statement and collective commitments embrace the following: 
Thomas Jefferson's Mission: 
 

• 

Every student learns at high levels every day; in an educational community that believes in creating life-long learners and 
problem-solvers. 

 
Thomas Jefferson's Collective Commitments: 
 

• We are committed to creating an maintaining a nurturing, emotionally and physically safe learning environment. 
• We are committed to collaborating with our peers to improve our practice. 
• We are committed to utilizing data to improve instruction and student achievement. 
• We are committed to sustaining collaborative relationships amongst stakeholders to achieve common goals.