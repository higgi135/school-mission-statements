PRINCIPAL'S MESSAGE 
Green Valley Independent Study and the PEP+ program have a rich tradition of providing quality education in a caring, small, and 
flexible learning environment. Students attend weekly one hour appointments as part of their educational program. Highly qualified 
independent study teachers create individual educational plans providing courses that meet A-G requirements at the high school level. 
Students have the option to utilize traditional and/or online curriculum in the PEP+ program. 
 
MISSION STATEMENT 
Green Valley Independent Study and PEP+, through the support of educators, family, students, and community, are dedicated to 
maintaining a caring and flexible environment. We will promote growth in academic, social, and interpersonal skills that foster the 
creation of responsible citizens. 
 
DISTRICT AND SCHOOL PROFILE 
Located in San Bernardino County, nestled at the base of the San Bernardino mountains, the Yucaipa-Calimesa Joint Unified School 
District educated approximately 9,000 kindergarten through twelfth grade students from the diverse suburban communities of 
Yucaipa and Calimesa. The district is proud of its long tradition of academic excellence. The district is comprised of six elementary 
schools (grades TK-5); one dependent charter school (grades K-8); two middle schools (grades 6-8); one comprehensive high school 
campus (grades 9-12); a community day school (grades 7-12); an independent study program and PEP+ (grades K-12); a continuation 
school (grades 9-12); and an adult education school. Yucaipa-Calimesa Joint Unified School District is dedicated to educational 
excellence and the continuous academic growth of all students. 
 
Green Valley Independent Study and PEP+ operate as alternative programs for the Yucaipa-Calimesa Joint Unified School District. 
Students enrolled in the program must complete the same coursework required by all students in the district in order to graduate. 
The staff at Green Valley Independent Study are dedicated to providing an excellent academic program and personalized support so 
that students are successfully prepared for post-secondary education and the world of work. Green Valley Independent Study enrolls 
50 -75 students in grades K-12. 
 
A Message from the Superintendent 
 
Dear Yucaipa-Calimesa Community, Parents and Students: 
 
YCJUSD is honored to serve students in two wonderful communities. We, as a team, continue to prepare our students to be successful 
in the 21st century. We work collaboratively with community partners, businesses and colleges to provide the best possible education 
for each and every student. The district employs high quality employees, who care for student well-being as well as academics. The 
school sites have a variety of clubs and programs to suit student interests and all our elementary schools have an after school care 
program. We strive to provide a wide variety of high quality services and programs in a safe environment. 
 
As you become a partner of the YCJUSD, please take the opportunity to be involved. It is our desire to work hand in hand with parents 
to support our children. The best way to get involved is to start at the school site. Our principals can help guide you to the many 
opportunities that exist. No amount of involvement is too small! We also offer classes for parents that will help you and your child in 
their educational journey. We have the Family Learning Center which offers a host of classes that support parents in learning strategies 
to work with children in grades K-12. Please take an opportunity to view the website to learn more about the classes. 
 
The role of educating children in our two communities is taken very seriously and we appreciate your trust. My goal, as your 
Superintendent, is to ensure that high quality instruction is delivered daily, our campuses are secure and well maintained, money is 
spent wisely, and students graduate from Yucaipa High School prepared to be successful! 
 

2017-18 School Accountability Report Card for Green Valley Independent Study and PEP+ 

Page 2 of 13 

 

This school year is the opportunity to work with you in supporting education of our children. Please do not hesitate to contact your 
principal or the district office if you have questions about the district or how to become involved.