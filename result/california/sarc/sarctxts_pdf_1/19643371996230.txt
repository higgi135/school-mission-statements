Community Day School is designed to meet the needs of students of grades seven through twelve who are having social or behavioral 
difficulties or have shown serious or severe breaches of the Burbank Unified School District Code of Conduct. In a supportive and 
nurturing atmosphere, the students are encouraged and enabled to assume responsibility for their own behavior and consequences. 
The goal of this program is to help students in a setting which will allow their individual needs to be addressed daily, to overcome the 
problems that they have experienced and return back to the comprehensive school setting with a positive outlook on education.