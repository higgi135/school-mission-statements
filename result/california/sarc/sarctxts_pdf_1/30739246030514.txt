J.H. McGaugh School's mission is to provide all students with a positive and challenging learning environment which empowers them 
to become responsible and productive citizens. At J.H. McGaugh School, we believe that students learn best when a supportive, 
rigorous, and enriched learning environment is provided. The teachers, staff, students and community are committed to the following 
school goals: 
 
PROFESSIONAL LEARNING COMMUNITIES – Working together in grade level teams, across grade levels, and as a school to provide the 
best possible academic, artistic, social, and emotional experience for students. Teachers regularly spend time together sharing 
exemplary lessons, looking at the results of common assessments to guide instruction, planning instruction for their students, doing 
research, and discussing best practices. McGaugh administration is committed to supporting and fostering this collaboration. 
INTERVENTION – Recognizing that if students are not learning in class, it is our responsibility to teach them using alternate methods 
and additional instructional time. This includes using regular assessments to identify students who are not meeting grade level 
standards and then providing intervention opportunities for these students during the instruction day via a Response-to-Instruction 
(RTI) model. 
INCLUSION – Teaching, supporting, recognizing, and appreciating the large population of students with special needs that are part of 
our learning community. McGaugh hosts exemplary special programs that meet the needs of students at every level including 
academic, social, emotional, and behavioral. The McGaugh community seeks to integrate and naturally include students with special 
needs in the general school environment whenever possible. 
 
LANGUAGE ARTS – McGaugh provides a comprehensive and balanced literacy program which includes skill development, a love for 
reading and literature, and language-rich activities. The Writer's Workshop model and research based best practices are implemented 
in all classrooms to support the development of writing. The Reader’s Workshop model and comprehension strategies are used to 
create independent readers who use meta-cognition and expert level thinking to read and analyze texts. Thinking Maps and the Depth 
and Complexity Icons are used at all grade levels to support student writing and comprehensions skills. 
 
 MATHEMATICS – Providing a balanced program of skill acquisition through concrete experiences and problem solving strategies with 
an emphasis on real-life applications. Cognitively Guided Instruction (CGI) allows students to demonstrate and share their 
mathematical thinking, allowing the teacher to provide individualized and focused math instruction. The web-based MIND Spatial-
Temporal (JiJi) curriculum uses adaptive and interactive mathematical modeling to increase students’ conceptual and spatial 
understanding at all grade levels. 
 
SCIENCE – Ensuring that all students demonstrate mastery of the state science content standards by providing hands-on, inquiry-based 
learning experiences that incorporate science process skills and the scientific method. Science journals are used by students to 
document and extend classroom learning. Technology is used to give students access to scientific experiences, models, and current 
academic research pertinent to their learning. Additionally, all students in grades TK-5 have access to the innovation lab where hands-
on next-generation science experiments are offered. 
 
THE ARTS – Continuing the school tradition of arts excellence including our exemplary school music program, our outstanding art 
studio and instruction, and our annual Pageant of the Arts which has been the gold standard for original, school-based artistic 
performances in Orange County for the past thirty-five years. 
TECHNOLOGY – Increasing access and use of technology to enhance literacy and content knowledge while preparing students for life 
and careers in the 21st century. McGaugh's infrastructure has been upgraded to provide wireless access for students throughout the 
school. Each classroom is equipped with newly-installed short-throw projectors and accompanying document cameras. Teachers have 
access to six Chromebook carts, two iPad carts, and we have a 1:1 BYOD program in grade three through five. In addition, the campus 
has a 42-station desktop computer lab that is accessed by students in grades K-5 on a regular basis depending on grade 
level. McGaugh is also using several researched based software programs such as Reading Plus Intervention, MIND ST Math, and 
MIND Fluency to support and extend student learning. 

2017-18 School Accountability Report Card for J. H. McGaugh Elementary School 

Page 2 of 11