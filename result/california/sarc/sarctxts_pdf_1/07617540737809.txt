Knowledgeable 

 
At Ygnacio Valley we are a community of learners who demonstrate respect for others through kind words and actions. We put forth 
our best effort, work hard and strive for excellence in all fields. We learn by taking risks and learning from our mistakes. 
This is who we are, even when no one is looking. This is the Warrior Way. 
 
At Ygnacio Valley, We Value... 
Taking Risks 
Resiliency 
Integrity 
Building Relationships 
Empathy 
 
Expectation of an Ygnacio Valley Graduate 
An Ygnacio Valley graduate will be able to develop their intellectual skills of critical thinking, communication, creativity, and 
collaboration throughout the school day. These skills broaden the students’ cognitive competencies including their capacity to be: 
• 
• Reflective 
• 
• 
• 
 
Not only do we want our students to develop their intellectual abilities, our school promotes the development of each student’s 
character. These learner profile traits also aim to develop the students’ dispositions and attitudes including their ability to be: 
• 
• Open-minded 
• 
• Balanced 
• Risk-takers 
 
Throughout Ygnacio Valley , our goal is to promote these ten attributes. Whether students are in the classroom, library, or quad, we 
believe all students can develop these traits so they are prepared for college, career, and beyond.