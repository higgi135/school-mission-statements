Casa’s Vision: Upon graduation, all Casa Grande students will be broadly literate, civically engaged, highly employable, and have a 
personal vision for their future. 
 
Casa’s Mission: Casa Grande High School believes that education is a shared responsibility involving students, staff, family and the 
broader community. Our mission is to foster strong relationships and the build a safe, inclusive, and collaborative environment where 
all students are engaged and challenged to achieve their highest potential. 
 
Casa Grande has a reputation for excellence in academics. In 2015, Casa was named a California Gold Ribbon Distinguished High School 
by the CA State Department of Education. In 2016, Casa was recognized as a California Honor Roll High School. 
 
In February 2016, Casa Grande’s Academic Decathlon Team won the county competition for the thirty-fourth consecutive year and 
went on to place seventh at the State level. Casa’s AcDec Team has won at the State level for eight years. The journalism class produces 
the Gaucho Gazette, and has won the Press Democrat Overall Excellence Award fourteen years 
in a row. The paper placed seventh in the nation at the 2017 Journalism Education Association Conference in Seattle, WA. 
 
Casa Grande offers 18 Advanced Placement (AP) level classes in six departments, including AP Human Geography, which was added 
in 2017-18. 
 
Casa continues to maintain an outstanding music program. The band students win numerous awards and the symphonic band earned 
a unanimous superior rating for the sixteenth year straight at the CMEA band festival. Casa’s Chamber Choir earned a unanimous 
superior ranking for the first time at the CMAs in Spring, 2017. Both band and choir students were selected for the All-state Honor 
Band and Honor Choir. 
 
With the assistance of a US Department of Education Small Learning Communities (SLCs) grant (2008-2013), Casa Grande created a 
small learning community structure that in many ways has transformed the school. The goals of SLCs are to provide students with 
increased personalization, relevance, and rigor, so that they can easily utilize real-world applications while developing critical thinking 
and problem solving skills. 
 
Ample research shows that reducing the size of schools can positively impact the achievement of students. It is the goal of CGHS to 
keep all the advantages of a large comprehensive high school—such as diverse offerings of coursework, teaching methodologies, and 
strong athletic and extracurricular programs—while utilizing the benefits that come with smaller schools. SLC benefits include 
increased student achievement, lower dropout rates, common teacher planning time and curriculum collaboration, and increased 
parent involvement. Casa currently has four SLCs with specified college and career themes for 11th and 12th graders: Health Career 
Pathway, Social Justice Academy, Innovation (STEAM) and International House. 
 
Ninth and tenth grade students are organized into SLCs called “houses”. These consist of 90-120 students who share common teachers 
for English, social studies and science in classes that average 30 students per class. 
 
Casa’s students are supported by a highly engaged, professional and experienced staff of academic counselors (5), as well as a team 
of MFT counselors and interns (4). Casa also has two school psychologists and a speech and language specialist. The art program has 
also been expanding in numbers of students taking the courses as well as new course offerings, including 3D:Maker, a maker class that 
includes elements of fine art, design and engineering. 
 

2017-18 School Accountability Report Card for Casa Grande High School 

Page 2 of 15

Since 2015, Casa has also added courses in Robotics, Introduction to Media and Broadcasting, Computer Science (Intro and AP), and 
Success 101, a semester-long course for ninth graders focused on college and career preparation. Casa Grande High School meets the 
needs of its diverse student community through a variety of programs. As stated earlier, all freshmen and sophomores are part of an 
SLC in which the teachers have a greater opportunity to know their students. The curriculum continues to reflect student interest with 
a diverse array of electives for students. These electives include: Anatomy & Physiology, Auto Mechanics, HOSA, Sports Medicine, 
Culinary Arts, 3D Animation, Human Geography AP, Environmental Science AP, Introduction to Psychology, Introduction to Sociology, 
Journalism, Public Speaking, Legal Studies, Photography, Graphic Design, and United Anglers. 
 
Casa’s Career Technical Education programs have recently received significant funding from the State’s CTEIG grant and also from the 
local CTE Foundation. Funds total over $300,000 and have been used to expand Casa’s Auto Mechanics, Digital Media, 
Entrepreneurship, and Culinary Arts programs. In 2017-18, Casa has launched the Casa-Collective, an e-commerce platform that 
markets and sells student-made goods and services to the community. 
 
In recent years, Casa has placed considerable emphasis on providing support for students in ELD. Students are enrolled in ELD 1/2 
(newcomers) and ELD 3 and 4. Casa also has Spanish for Native Speakers through the Advanced Placement level, which an increasing 
number of ELD students take. Casa redesignates as English Proficient an average of 30-=50 students per year. In 2016, 50 students 
were redesignated. 
 
April 2018 marked the twenty-fourth anniversary of the opening of our Natural Resources Program’s fish hatchery, the only campus 
facility where students are licensed to handle endangered species. The United Anglers Adobe Creek Restoration Project continues to 
receive national and international recognition for local restoration and conservation efforts. Recently, the Petaluma Water Agency has 
offered to team with the United Anglers and our other environmentally themed courses to develop a partnership that will enhance 
students’ understanding of the challenges of balancing environmental protection and human encroachment. Our expanded 
environmental science course has developed a native plant nursery and our culinary arts program has incorporated an organic garden 
into its curriculum. 
 
Senior Projects are an integral component of the senior English curriculum that helps students focus on career exploration, community 
involvement, and personal growth. Students are offered an opportunity to select an area of study about which they are passionate 
and design a research paper and project around that interest. The students design a project, research and write a paper, work with an 
advisor to learn about and develop the project, and present their work to a panel of community members. In 2016-17, over 250 
parents, community, and business leaders joined with Casa staff to participate as Senior Projects mentors, tutors, and judges. The 
projects encompass a wide range of interests and are a wonderful reflection of all that is successful about the students at Casa Grande. 
Together, graduates of the Class of 2017 volunteered over 8,000 hours to the community. 
 
The show of support by our community with the passage of Measure K provides us with approximately $48,000 a year to renew and 
upgrade our technology, $20,000 a year to help equip our visual and performing arts programs, and $46,000 a year for our library. 
Additionally, Casa is a member of the Northern California Career Pathway Alliance, which includes a cohort of high schools who receive 
direct support to expand services to prepare students for college and career readiness. Casa has invested an estimated $50,000 into 
the College and Career Hub on campus and has expanded the hours of service of its College and Career Counseling Coordinator. 
 
Although we are proud of the success of our students and staff in all of these areas, we constantly strive to improve both our practice 
and our results through the study of student data and current research. We expect that the continued implementation of SLCs, as well 
as other interventions, will help us ensure that every student is prepared with the 21st Century Skills necessary to succeed in 
postsecondary education, training, and/or employment. 
 
Casa Grande High 
333 Casa Grande Rd. 
Petaluma, CA 94954-5706 
Phone: 707-778-4677 
E-mail: ebackman@petk12.org 
 
 

2017-18 School Accountability Report Card for Casa Grande High School 

Page 3 of 15