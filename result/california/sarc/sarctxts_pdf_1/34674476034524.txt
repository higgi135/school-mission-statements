Dyer-Kelly’s mission is to provide a psychologically safe place where students, parents, and teachers work collaboratively to provide a 
nurturing learning environment that enables all learners to reach their full potential. Dyer-Kelly Elementary School, a year five and 
beyond Program Improvement school, has two specific school goals in order to increase student achievement. Students will thrive in 
our diverse community by taking personal responsibility for their education and developing healthy, supportive relationships with 
others in our community of learners. We believe our students can: 

effectively communicate their thoughts and feelings 
resolve conflicts peacefully and ethically 

• 
• 
• work collaboratively with others 
• 
• 
• 
• 

contribute positively to their communities 
think critically and creatively 
excel on the Common Core State Standards and be on the path to be career, college and citizen ready 
acquire the language skills necessary to function at a high level within the global economy in school and work settings 

School Profile: 
Dyer-Kelly is a Pre-K-5 Title 1, neighborhood school located in Sacramento, California, within the San Juan Unified School District. Dyer 
Kelly Elementary is one of 41 elementary and K-8 schools in the San Juan Unified School District. In addition, it is one of 20 schools in 
the district that receive Title 1 Funds. There are at least 10 languages that are spoken at Dyer-Kelly. The curriculum provided is aligned 
to the California Common Core Standards. The school supports cultural awareness on a daily basis through its diverse literature 
selections and other school activities. 
 
Dyer Kelly Elementary School has an academic focus and stresses balance. We strive daily to ensure that our students have the basic 
skills that they need as well as critical thinking and reasoning skills needed to be competitive and skilled individuals who are college 
and/or career ready. 
 
At Dyer-Kelly Elementary School, we believe that students need to develop basic habits to help them become successful, life-long 
learners. As they develop academically, we believe all students can become: 

• CRITICAL THINKERS who use 21st century skills to reason, make decisions, and solve complex problems in a variety of 

contexts and have the higher-level thinking to be proficient and above on the Common Core Standards. 

• COLLABORATIVE WORKERS who use effective leadership and group skills to develop and manage interpersonal 

relationships within culturally and organizationally diverse settings. 

• COMMUNITY CONTRIBUTORS who contribute their time, energies, and talents to improving the welfare of others and the 

quality of life in their diverse communities. 

• EFFECTIVE COMMUNICATORS who share their thinking and feelings with others through writing, speaking, artistic 

performances and productions, and through discussion and conversation. 

Dyer Kelly Elementary School is a place where: 

• Parents, staff, and students have mutual respect, work together to solve problems, and take responsibility for our actions. 
• Parents, staff, and students work collaboratively with the best interest of our students in mind. 
• Parents and community are involved in the learning process. 
• Students feel psychologically and physically safe. 
• Students have multiple opportunities to succeed. 
• Teachers teach grade level, standards-based curriculum. 
• Teachers differentiate instruction. 
• Teachers hold students to high academic, social, and behavioral standards. 
• Staff works toward continual improvement in the educational process. 

2017-18 School Accountability Report Card for Dyer-Kelly Elementary School 

Page 2 of 12 

 

Our school program includes, but is not limited to, the following components: 

• Bridges After School Program (creative and supportive enrichment) 
• Classes for special needs students from across the district 
• Student Ambassadors 
• Positive Behavior Intervention Support (PBIS) 
• Responsive Classroom 
• Multiple opportunities for family engagement 
• Co-teaching model with more than one teacher in each classroom to support more intensive language instruction 
• Newcomer ELD classes to support English Language Development for our newest students 

Principal's Message: 
Welcome to Dyer Kelly Elementary, located in the City of Sacramento. We are an elementary school that serves Pre-school– 5th grades 
that embraces many cultures, languages and backgrounds. We are focused on achieving student success by providing all students with 
access to quality, rigorous curriculum based on the California Common Core Standards. We believe our students can: 

resolve conflicts peacefully and ethically 

• effectively communicate their thoughts and feelings 
• 
• work collaboratively with others 
• contribute positively to their communities 
• 
• excel at the Common Core Standards and be on the path to be career, college and citizen ready 
• acquire the language skills necessary to function at a high level within the global economy in school and work settings. 

think critically and creatively 

We are committed to the individual needs of each of our unique students. We are a community of learners and our goals include: 
improving student performance through high quality instruction, and building independent learners with the knowledge and skills 
required for working, living and learning in our ever-changing society. When one can no longer predict which students are most likely 
to succeed, then we will know that we have provided each child an equitable opportunity to reach their long term goals. Dyer-Kelly 
students have the opportunity to participate in a range of enrichment programs including: Education Through Music (ETM), Science, 
Social-Emotional Learning and use of technology. We also offer extra-curricular activities such as soccer, basketball, yearbook, and 
cheerleading. Our staff and students strive to grow each day by being Respectful, Owning our choices, Always being safe, and being 
Responsible (ROAR). Finally, it is of the utmost importance to us that our families are partners in the educational process, so we offer 
many ways for families to participate such as Academic Parent Teacher Teams (APTT), English Learner Advisory Committee (ELAC), 
Family Nights, Coffee with the Principal, School Site Council, and many volunteer opportunities. Families may call us at 916-566-2150 
in order to inquire about ways to become partners in the educational process. 
 
Yours in education, 
 
Cassandra Bennett Porter