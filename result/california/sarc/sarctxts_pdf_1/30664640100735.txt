Mission Statement: 
IT IS THE MISSION OF CANYON VISTA ELEMENTARY SCHOOL TO PROVIDE EACH STUDENT A DIFFERENTIATED EDUCATION THAT 
PROMOTES SELF-DISCIPLINE AND STRONG CHARACTER DEVELOPMENT IN A SAFE LEARNING ENVIRONMENT. THROUGH 
COLLABORATION WITH THE TOTAL SCHOOL COMMUNITY, ALL STUDENTS WILL MEET THE GOAL OF INCREASED STUDENT 
ACHIEVEMENT IN ACADEMIC, BEHAVIOR AND SOCIAL-EMOTIONAL COMPETENCY SO THAT THEY ARE COLLEGE AND CAREER READY. 
 
Vision Statement: 
WE BELIEVE THAT THE MOST PROMISING STRATEGY FOR ACHIEVING THE MISSION OF OUR SCHOOL IS TO PROVIDE A MULTI-TIERED 
SYSTEM OF SUPPORT THAT IS COLLECTIVELY MONITORED BY A PROFESSIONAL LEARNING COMMUNITY. 
 
WE ENVISION CANYON VISTA ELEMENTARY SCHOOL AS A PLACE WHERE STAFF: 

 

• WORK TOGETHER IN COLLABORATIVE TEAMS. 
• MAKE DATA DRIVEN DECISIONS TO PLAN INSTRUCTION (CFAs/PLC). 
• 
• DEMONSTRATE A PERSONAL COMMITMENT TO THE ACADEMIC, BEHAVIORAL, AND SOCIAL-EMOTIONAL WELL-BEING OF 

IMPLEMENT STRATEGIES TO MONITOR AND IMPROVE STUDENT ACHIEVEMENT (GFI/PLC). 

ALL STUDENTS. 
ENCOURAGE RESPONSIBILITY, FAIRNESS, CARING, CITIZENSHIP, TRUSTWORTHINESS, AND RESPECT. 

• 
• PROVIDE A SAFE AND NURTURING LEARNING ENVIRONMENT. 

Canyon Vista Elementary School (CVES) is nestled in the hilltop community of Aliso Viejo next to Soka University, our community 
partner. With a breathtaking view of Wood Canyon and the hilltops of Laguna Beach, the school opened its doors in August of 2003. 
As a “walking school”, all of the families live within the local community and walk in neighborhood groups to the beautiful school. The 
facility, housing grades K-5 and district's Deaf and Hard of Hearing program, is comprised of thirty classrooms, a multi-purpose room, 
and a computer lab. The school includes internet access in every classroom and wireless access throughout the school, as well as 
Chromebook Carts in grades 3-5. 
 
Students at Canyon Vista are motivated and encouraged to think critically, communicate effectively, and be active participants in 
successfully achieving their academic and personal growth goals to become lifelong learners. Canyon Vista teachers follow the district 
and state guidelines with a shared commitment to implement the best practices and strategies based on multiple measures of data, 
while utilizing instructional time efficiently, and creating a safe and supportive environment. 
 
The staff has a commitment to excellence and to developing the whole child. We believe that every child can and will learn the grade 
level standards. We accept high levels of learning for all students as the fundamental purpose of our school and are willing to examine 
all practices in light of their impact on learning. We value the home/school partnership to ensure that each child develops the 
intellectual, physical, and emotional capacities to be a life-long learner. 
 
 

2017-18 School Accountability Report Card for Canyon Vista Elementary School 

Page 2 of 11