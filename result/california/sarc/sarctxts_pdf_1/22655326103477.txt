Lake Don Pedro Elementary School is a rural school located in the foothills of northwestern Mariposa County. It began as a necessary 
small school but has grown rapidly over the past years to the size it is today. Beginning in the 1990-91 school year, Lake Don Pedro 
Elementary became a K-8 school. The school serves approximately 170 students. Every classroom has a digital whiteboard and digital 
projector. Our computer lab has 35 laptop computers with internet connection. All rooms are wired for wireless internet connection. 
In 3rd-8th grades we are 1:1 with chromebooks and we utilize Ipads & Ipad minis for classroom instruction in K-2nd grades. The 
multipurpose, administration, and kitchen building opened in September of 2010. In December of 2010, we were awarded the After 
School Education and Safety (ASES) Program. The goals of this program include; To provide academic support and academic 
enrichment to support the core program, increase instructional support to EL learners, to promote wellness in each student, to educate 
students in the areas of Science, Technology, Engineering and Math, and to create a culture of respect and student leadership. 
Approximately 95 students benefit from ASES on a daily basis. The educational program focuses on the development of reading, 
writing, spelling, math, history, science, art, and physical education based on the California curriculum standards and frameworks. In 
January of 2011, our school implemented an RTI (Response to Intervention) model of instruction. Each student in grades K-8th grade 
receives small group instruction in the core curriculum during RTI time. 
 
“We, at Lake Don Pedro, will not rest until every student achieves at least a year’s progress or more that is measurable by state 
standards. We will accomplish this by collaborating to make the best use of our personnel, time and resources.” 
 
The mission of LDPE is to serve as a community and educational resource to help all stake holders grow as individuals and lifelong 
learners. To accomplish our school mission: 
• We are a community of students, staff, families, and the public dedicated to helping each other grow as independent and life-

long learners. 

• We prepare our school community for ever-changing advancements by using current strategies, information and technology. 
• We challenge students by providing a rigorous academic program. 
• We foster a nurturing and safe environment where all students can reach their academic, emotional, social and physical potential. 
• We take coyote pride in our school, develop civic responsibility, maintain a high standard of behavior, and show respect for 

ourselves and others. 

• We promote cultural awareness and ethical attitudes towards all people. 
• We create an environment that experiences and appreciates the arts. 
• We celebrate our growth and achievements as a community. 
 
 
 
 

2017-18 School Accountability Report Card for Lake Don Pedro Elementary School 

Page 2 of 10