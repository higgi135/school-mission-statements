Crowne Hill Elementary School is one of TVUSD's newest schools, opening in August 2005. Current enrollment for the 2018-19 school 
year is approximately 634 students. 
 
Vision 
 
 Instruction: 
 Teachers are 
 -committed to collaborating to plan and implement quality New State Standards based instruction that is consistent from classroom 
to classroom. 
 -teaching according to the Common Core Standards, implementing the 4 C's: Critical Thinking, Collaboration, Creativity and 
Communication. 
 -implementing the newly adopted Language Arts Curriculum, Wonders. 
 -continuing to implement the math program that was adopted last year, Pearson. 
 -teaching to the objective with a high level of rigor. 
 -engaging students by skillfully utilizing a variety of effective strategies that incorporate multiple modalities. 
 -differentiating and scaffolding instruction to meet the unique needs of our students. 
 -utilizing technology effectively to enhance learning. 
 
 Learning: 
 Students are 
 -thinking critically and creatively to solve complex and interesting problems. 
 -reading for meaning and high levels of comprehension. 
 -communicating effectively both verbally and in writing. 
 -collaborating to enhance their learning in all areas of the curriculum. 
 
 Behavior: 
 Students are 
 -respectful, responsible and demonstrate self-control. 
 -self-motivated and willing to take risks. 
 -caring and accepting of others. 
 -problem solvers. 
 -participants of the PBIS Behavior Program. 
 
 
Mission 
 
 The purpose of Crowne Hill Elementary is to develop lifelong learners, who are critical thinkers and effective communicators and 
collaborators. Crowne Hill students will become productive citizens of exceptional character, who positively impact and inspire others. 
 
 
 
 
 

2017-18 School Accountability Report Card for Crowne Hill Elementary School 

Page 2 of 12