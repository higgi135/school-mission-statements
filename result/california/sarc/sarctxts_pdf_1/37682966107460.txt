Bernardo Heights Middle School, one of six middle schools in the Poway Unified School District, is located on the boundary lines of 
Rancho Bernardo and the City of Poway and is approximately 25 miles northeast of San Diego. Our community and school is a 
destination for families seeking maximum opportunities for their children to realize their full potential. Our school serves a culturally 
and ethnically diverse population of approximately 1,550 students whose households represent 41 different languages and a diverse 
variety of values and belief systems. Our community consists of an established middle and upper-middle income area and a planned 
retirement community and continues to expand in diversity of cultural and socio-economic backgrounds with current changes in 
housing and family income. Our students come to us from five feeder elementary schools (Chaparral, Highland Ranch, Painted Rock, 
Turtleback, and Westwood) and most of our eighth graders will attend Rancho Bernardo High School which is located immediately 
next to BHMS. Some students will have an option to attend Del Norte High School located in 4S Ranch. We have been recognized as 
a California Distinguished School and as a National Blue Ribbon School. To achieve high academic standards and to support the 
progress of all learners, the school staff, parents, district, and members of the community work together to meet the needs of all 
students so that they are on target for College and Career readiness. 
 
With a student population of nearly 1,550 students, Bernardo Heights Middle is the largest middle school in our District. Students 
experience a rigorous and challenging academic curriculum. Results of state assessments are consistently above the state-wide 
average. Last year, approximately 70% of all students met or exceeded the standards in Language Arts and 60% met or exceeded the 
standard in Math. Our school continues to work together with the support of District and County resources to support the progress 
of all students and to close the gap in student achievement that exists for traditionally under-represented populations such as students 
with disabilities, English Language Learners, and those from households of low socio-economic status. About 34% of our students 
participate in special programs: Gifted and Talented Education (15%), English Learner (7%), and Special Education (12%). The ethnic 
representation of BHMS is 48% Caucasian, 16% Asian, 16% Hispanic, 5% Filipino, and 2% African American and 6% Multi-ethnic. 
Approximately 17% of the student body qualifies for low socio-economic status. The BHMS staff is comprised of 70 teachers, two and 
a half counselors, three administrators, one part-time librarian, one school psychologist, one speech pathologist, and 42 classified 
staff. Actively involved parents volunteers of PTSA, the BHMS Education Foundation members, and participants on the School Site 
Council and English Language Advisory Council serve as partners in our meeting the needs of our school. 
 
Bernardo Heights offers an inclusive and nurturing environment in which to foster the development of our students' values, interests, 
and skills to prepare them to be contributing members of a democratic society. We are committed to fostering school engagement 
and connection our school. Our instrumental and choir music programs are articulated between elementary and high school and 
regularly receive recognition for excellence in local and state performances and competitions. Our technology elective, through Project 
Lead the Way, exposes students to applications of technology to academics and industry and regularly benefits for partnerships with 
regional technology industry. Advancement Via Individual Determination (AVID) elective provides a targeted population of 
traditionally under-represented students as well as any self-selecting students extensive preparation for coursework with an emphasis 
on college readiness academic focus and skills. Other offerings include Spanish Language, Art, Drama, Leadership, Associated Student 
Body (ASB), California Junior Scholarship Federation (CJSF), and Musical Theater. In addition to our elective offerings and school day 
programs, our staff, parent community, and high school students provide our students the opportunity to explore of personal interests 
and leadership skills by serving as mentors and advisers for over 39 student-initiated academic, co-curricular, and extra-curricular 
clubs and activities. Many of these clubs change from year to year; currently offerings include Speech and Debate, Musical Theater, 
Color Guard, Best Buddies, Math Olympiad, Science Olympiad, Astronomy Club, Magic the Gathering, Leo Club, Fellowship of Christian 
Athletes, and Yugioh Club. The average daily attendance of our students over the last few years is 96%. The BHMS average dropout 
rate for the same period is 0%. 
 
 Our school website is: http://www.powayusd.com/pusdbhms/ 
 

 

2017-18 School Accountability Report Card for Bernardo Heights Middle School 

Page 2 of 11 

 

 
 BHMS Mission Statement: 
 
 TO ENSURE OUR STUDENTS ARE COLLEGE AND CAREER READY, BERNARDO HEIGHTS MIDDLE SCHOOL WILL: 

• Maintain a partnership among students, parents, teachers, staff, and community 
Provide a safe, positive, and challenging environment for all children to succeed 
• 
• Offer diverse opportunities for intellectual and personal growth for all members of the school community 
• Develop multiple literacy skills to ensure our students are effective users of ideas, information, communication, and 

technology 
Promote productive, responsible, respectful, and healthy citizens for a global society 
Encourage a passion and curiosity for life-long learning 

• 
•