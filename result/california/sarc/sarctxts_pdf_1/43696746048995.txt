It is the mission of Ponderosa Elementary School to develop a caring community of life-long learners that fosters continual growth. 
 
Ponderosa Elementary School has a student body of approximately 650 ethnically, socially and academically diverse students in grades 
K-5. 
 
30 students are identified RSP/SAI. We have three SAI classrooms (Specialized Academic Instruction). Ten students are currently in the 
K-1 SAI, 12 in the 2/3 SAI and 14 in the 4-5 SAI. 
 
Our staff includes 25 full time and part time classroom teachers, 1 teacher on special assignment of school support, 5 SAI teachers, 1 
part time library media specialist, 1 P.E teacher and 1 P.E. paraeducator, 1 full time speech/language specialist, 1 part time 
psychologist, 2 reading intervention specialists, numerous paraeducators and student attendants supporting SAI classrooms, 1 clerk, 
1 secretary, 1 ELSAT, 1 cafeteria manager, 1 cafeteria assistant, 2 part time counselors, 1 part time PIP aide, 1 day custodian, and 2 
night custodians. Physical education, creative/performing arts, and library skills are taught by highly qualified paraprofessionals. The 
Ponderosa staff also includes a nurse and a health clerk, each of whom is on campus one day per week. Ponderosa has 27 classrooms, 
a library media center, one computer lab, a multi-purpose building, and an administration building which houses the school office, a 
nurse/first aide station, and teacher work areas. The school facility was modernized in 2004. There is a facility for preschool and 
daycare. 
 
Ponderosa was named a California Distinguished School in 1997 and 2004. The school welcomes visitors with several year-round 
blooming flower gardens.