Welcome to Hidden Valley! We are a diverse and dynamic learning community focused on preparing our students for leadership and 
success in the 21st Century. Hidden Valley consists of talented, eager learners, a dedicated, creative staff, and active, supportive 
families. We provide a rigorous standards-based curriculum, and have high academic and behavioral expectations for all children. We 
are dedicated to fostering an environment that supports strong character, and promotes content mastery, while providing students 
with opportunities to develop their curiosity, creativity, collaboration, and critical thinking skills. 
 
At Hidden Valley, we believe that each child is a unique and special person capable of high levels of academic achievement. We strive 
to maintain a safe, positive, and friendly school environment, and value the caring, ongoing collaborative relationships that have been 
developed with our school community. 
 
Hidden Valley is an exciting and rewarding place for students. Throughout the school year, students have opportunities to go on field 
trips, take part in musical performances, and participate in various other activities. With the support of our active Parent Faculty Club 
(PFC) our Soul Shoppe anti-bullying program provides opportunities and support for our students’ social and emotional development. 
In addition, this year is Hidden Valley's fourth year of implementation as a PBIS school. 
 
If you are new to our community, we hope you will come visit our campus. Volunteers are a valued part of our school community. We 
encourage parents and guardians to take every opportunity to join us throughout the school year to help in classrooms, attend 
conferences, and participate in family activities. Parents are also encouraged to be part of our Parent Faculty Club. Our students 
benefit tremendously by the involvement of all. 
 
We are proud of our school and the growth our students have accomplished. We look forward to seeing you and working together to 
create and maintain a successful 21st century learning community. Hidden Valley’s success is a testimony to our shared vision and 
commitment to supporting our children in becoming the best that they can be. 
 
Hidden Valley's shared vision is to create a positive and collaborative environment for students and staff to grow and flourish. 
Collectively, we refer to our vision as "The Hidden Valley Way". Our mission is to build and support a community of successful learners 
where all participants are able to share their competency and capacity as reciprocal teachers and learners. We strive to maintain an 
environment that supports all members of our diverse community through mutual respect. In sum, we are committed to meeting the 
social and academic needs of all students.