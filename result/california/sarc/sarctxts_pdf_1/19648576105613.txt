School Mission: 
The mission of Cactus Middle School is to implement our vision while providing a rigorous and relevant education, social skills, and a 
safe learning environment through continued collaboration with parents, students and staff so our student can reach their full 
potential. 
 
School Vision: 
Cactus Middle School students will leave with the skills to succeed in high school and beyond: higher education, career, and global 
society. 
 
School Description: 
Cactus Middle School provides an educational environment rich in literacy and problem solving to ensure that all students are prepared 
for the rigor of high school and the challenges of the future. Cactus students will achieve the Common Core State Standards through 
a sequential, balanced, and rigorous curriculum which includes the strategies of the 4 C's - communication, collaboration, creativity, 
and critical thinking. The partnership of community, parents, and staff will provide continuing support to ensure that all students 
become proactive citizens. 
 
Cactus Middle School is located in Palmdale, California, in the High Desert area north of Los Angeles. The school is one of 28 in the 
Palmdale School District, and one of five middle schools in the district. The school consists of 42 regular classrooms, a library, computer 
lab, science lab, teacher lounge, media center/teacher workroom, cafeteria, with a stage, gym, and office area. 
 
Cactus Middle School supports the Palmdale School District Vision and Mission. 
 
Core Values: 
1. EXCELLENCE: We will only accept excellence in our pursuit of student achievement. 
2. ACCOUNTABILITY: We are responsible for unleashing the unlimited potential for each student, recognizing that we each play a 
critical role in his or her success and we are obligated to aspire to be our best as we pursue excellence in student achievement. 
3. DIVERSITY: We embrace and celebrate the diversity in our community, valuing our cultural richness, multiple perspectives, and the 
varied contributions we all make to advance student achievement. 
4. INTEGRITY: We treat each member of our whole community with dignity and respect, valuing relationships based on honesty and 
compassion, as we work collectively to advance student achievement. 
5. COMMUNITY: We will be productive, honorable members of our community, advancing the interest of student achievement, civic 
pride and active participation in our democratic process. 
6. TRANSPARENCY: We are dedicated to open and honest communication as we make all decisions impacting student achievement. 
We value input from our community as it relates to our pursuit of educational excellence. 
 
Guidelines For Student Success: 
1. Friendly: Be nice, outgoing, positive, and helpful. Say “hello.” Care for and respect others. Earn their trust. 
2. Inclusive: Everyone is a part of our school. Accept others. Participate. Get involved. 
3. Engaged: Always be focused on learning something new. Be committed and get involved in activities. Be here and be a part of 
school. Willing to learn 
4. Responsible / Respectful: Follow the Golden Rule. Be accountable, reliable, and take care of each other. Be organized and bring 
the things you need for school. Be prepared 
5. Creative: Use your imagination and think outside the box. Be original. Figure it out and be a problem solver. 
6. Educated / Excel: Learn, listen, practice, grow. Advance in anything you set your mind to and be exceptional. Learn and study to 
be the best. Never give up and reach your goals. 
 

2017-18 School Accountability Report Card for Cactus Middle School 

Page 2 of 13 

 

CACTUS AREAS OF FOCUS 
1) Enhancing literacy, writing and number sense campus wide so that students are prepared to be successful in high school and in life. 
2) Identifying behavior expectations campus wide and incentives that promote positive scholarly conduct. 
 
In addition to a Principal and two Assistant Principals, Cactus has a teaching staff of 41 teachers. Thirty one of our teachers hold single 
subject credentials, four have multiple subject credentials, and six have specialist credentials in Special Education. All are CLAD certified 
or SDAI trained. 
 
The staff at Cactus plans to continue to refine the work we have done in Professional Learning Communities. Teachers collaborate, 
plan, analyze data, and create Common Formative Assessments for intervention or enrichment. Teachers will utilize the training they 
received to continue with EDI and the TESS framework supported by Administration. Cactus staff will continue to work with the 
Palmdale School District to provide professional development on the Common Core State Standards. 
 
Student data, such as CFAs, CELDT scores, PSD Local Progress Monitoring assessments and prior year grades, are used to place students 
in programs best suited to their academic needs. District and state approved curriculum, extended time on task, and intervention help 
support students' academic achievement. 
 
All students are administered regular assessments to monitor progress. Students' change of placement is based on academic 
performance on benchmark assessments and teacher recommendation. 
 
Enrollment for the 2017/18 school year is 852. The student population includes 6% Caucasian, 71% Hispanic, 19% African American, 
and 4% other. In addition, Students with Disabilities is 14%, 19% are English Language Learners, 24% of our students are Reclassified 
English Language Proficient, and 6% are GATE. 
 
97.12 of the students qualified for free and/or reduced meals. This means approximately 827 students receive free and/or reduced 
meals. 
 
At Cactus Intermediate School, highly trained and dedicated staff offer rigorous curriculum that is research-based and proven to be 
highly effective. Teaching strategies capitalize on varied learning styles of students to develop the academic, social, emotional and 
physical dimensions of every child. School leadership, teachers, students, and parents form a community of learners working together 
to achieve world-class standards. 
 
Expected Schoolwide Learning Results (ESLRs) have been created and written by teachers, parents, and students. Students will acquire 
the cognitive skills that will enable them to participate successfully in the educational mainstream through extensive emphasis on 
language development, literacy and critical thinking skills. Students will develop interpersonal skills that will enable them to interact 
productively with children and adults of varied economic and/or cultural backgrounds through instructional programs that promote 
multicultural appreciation and cooperative learning. Students will also develop a high self-esteem and personal standards and be 
technologically literate for the 21st century. 
 
Teaching and learning is supported by optimum conditions in school facilities, climate and safety. Facilities are modern and technology 
is advanced, incorporating equipment and other teaching tools that prepare students to enter higher education and /or the workplace 
with levels of competence for immediate success. The Cactus campus and classrooms are clean, attractive and well maintained to 
provide an environment where students can achieve at the highest levels, and staff can deliver services at their maximum efficiency 
and effectiveness.