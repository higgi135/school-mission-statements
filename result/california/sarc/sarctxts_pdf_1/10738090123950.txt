Principal's Message 
The Firebaugh Community Day School serves students, grades 1-12, who have been expelled from other schools within the 
Firebaugh-Las Deltas Unified School District or have moved to the community of Firebaugh and were previously under an expulsion 
placement. Firebaugh Community Day School also offers an alternative program for students with serious attendance issues or as an 
alternative educational environment for students needing a smaller, quieter environment due to behavioral or socio-emotional 
issues. Firebaugh Community Day School (FCDS) offers an alternative plan for academic instruction. FCDS is self-contained and uses 
guided instruction through technology from a fully-credentialed teacher. Students of similar abilities are able to work in small 
groups that are teacher-monitored as part of our self-directed study component. We believe that students will achieve the most 
when their own desires and abilities drive their progress. Our staff consistently shares the message with our students and their 
parents that they can succeed at Firebaugh Community Day School regardless of the prior experiences they may have had in other 
school settings. 
 
Major Achievements 

 

• 

Full-day instruction continues to be successful and allows high school students to maintain their credit-earning 
potential. 

• All teachers have received professional development training in integrated instructional technology, English learner 

instructional strategies/techniques, and Professional Learning Communities (PLC) collaboration. 

• Our students can obtain a high school diploma from our comprehensive high school once they are eligible for 

readmission, but students that prefer an alternative education setting are able to earn a high school diploma from our 
continuation high school which is ACS/WASC accredited. 

• A district-wide commitment to the implementation of Positive Behavior Intervention Systems (PBIS) has drastically 

reduced the incidence of expulsion referrals. 
Students now have greater access to behavioral counseling due to staffing improvements in that area. 

• 

Focus for Improvement 

• The teachers will continue to build instructional expertise through active participation in the PLCs with core subject 
teachers at either the Firebaugh Middle School or the Firebaugh High School. The focus is on student engagement 
through strategies and activities that will deepen understanding and prepare students for the Common Core 
Assessments (Smarter Balance Assessment Consortium, or SBAC). 

• Professional development focused on English learners needs, especially for long-term English learners, will continue to 

expand in order to facilitate the eventual reclassification of EL students to fluent English proficient. 

• Students will be given regular access to their academic counselors so that they can plan and monitor their academic and 

vocational goals. Counselors will more accurately monitor student progress and expedite interventions, especially for 
students designated as English Learners and other at-risk students. 

 

 

 

2017-18 School Accountability Report Card for Firebaugh Community Day School 

Page 2 of 11