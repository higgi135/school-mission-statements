The Sam Vaughn staff is committed to the philosophy that every child is regarded as gifted, because every child is a gift. Our district 
vision statement, Committed to Excellence, Responsive to Every Student, is what we strive to do on a daily basis. Our district mission 
statement reflects the staff's commitment to our profession. 
 
Sam Vaughn Elementary School is located in the heart of California’s San Joaquin Valley and is surrounded by the suburban community 
of Ceres. The school opened in September 1994 and is a place of pride for the staff, students and community. The school includes 29 
classrooms, a cafeteria with a stage, a library, an administration building, and an amphitheater. The campus is located on the east side 
of Ceres and most students walk to and from school. 
 
Sam Vaughn is a Kindergarten through sixth grade school of approximately 650 students. School staff includes: a Principal, an Assistant 
Principal, an Administrative Assistant, twenty-six certificated teachers, one resource specialist, eleven paraprofessionals, one 
library/media clerk, a 60% nurse, a 70% school psychologist, a health clerk, a speech teacher, an office manager, secretary II, a 50% 
clerk II, 2 full time custodians and 1 50%, a community liaison and an Itinerant music and P.E. teachers provide services 1- 2 days per 
week. 
 
The school year consists of 180 instructional days with ten minimum days for parent conferencing and staff development. The students 
in grades 1-6 are in school for 320 instructional minutes per day. K and TK students attend school for 310 minutes per day. The school 
has a Parent Teacher Club that supports extra-curricular programs, classroom budgets, and special events for our school and families. 
 
The student population is diverse at Sam Vaughn. Of the approximately 650 students, 77% of the students receive free or reduced 
cost breakfast and lunch, Approximately 70% come from homes where the primary language is other than English, and 37% are English 
Learners (EL.) The ethnic make-up includes 69% Hispanic, 15% White, 1% African American, 11% Asian, 1 % Filipino 1% Pacific Islander. 
2% of our students are migrant students. Approximately 1% of the intermediate students are identified as GATE students. 
 
Speech and language development services, adaptive PE, visually impaired services, 1 severely handicapped student classroom, and 
resource programs are provided as part of the Special Education Services. Supplemental programs include the following: After School 
Academic Intervention Program (AIP), after school English Language Development classes (ELD), After School Education and Safety 
program (ASES), Social Skills groups and a Student Support Specialist for students identified with needs, Migrant Education, and Indian 
Education. Differentiated Instructional Time (DIT) is incorporated into the regular school day. English Language Learners are provided 
English Language Development. Extra-curricular activities include: chorus, music, beginning/advanced band, Safety Patrol, a club that 
meets on Fridays after school, and serving on the Student Council. Furthermore, there is a Free Breakfast for All program offered 
before school for each student. The staff is supported in their educational efforts by both the School Site Council (SSC) and the Parent 
Teacher Club, made up of staff and family members of the students. 
 
Sam Vaughn receives supplementary funding from two sources: Title I and LCAP. Sam Vaughn’s program is designated as a School-
Wide Program which allows the total population to be eligible for services. The staff is supported in their educational efforts by the 
School Site Council (SSC), ELAC, and Family Engagement Team. Family gatherings for technology, school wide information, and 
Kindergarten parent support are held annually. 
 
 
 

2017-18 School Accountability Report Card for Sam Vaughn Elementary School 

Page 2 of 10