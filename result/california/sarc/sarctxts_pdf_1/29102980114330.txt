Nevada City School of the Arts (NCSA), charter #869, is a public charter school serving students in Transitional Kindergarten through 
eighth grade. The school is located in a forested setting approximately ten miles from Grass Valley. NCSA was first sponsored by the 
Nevada City School district in 1994, then sponsored by Twin Ridges Elementary District from 1996 through 2005, and is currently 
sponsored by the Nevada County Superintendent of Schools. The Shared vision of NCSA is to provide a rigorous academic environment, 
using art as a lens to shape curriculum. The strong emphasis on learning through the arts means art, dance and music are woven into 
the curriculum to support and enhance studies. Nevada city School of the Arts has been four times honored: as a California 
Distinguished School, as a California Award Recipient, as the first charter school to be awarded the National Blue Ribbon School Award, 
and most recently voted as Best Charter School by readers of the Parent Resource Guide, a Sierra Foothills Magazine four years in a 
row.