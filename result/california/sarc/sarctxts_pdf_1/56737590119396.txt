MISSION STATEMENT 
 
EARTHS’ (Environmental Academy of Research Technology and Earth Sciences) mission is to provide an environment where students 
are actively engaged in a comprehensive, inquiry-based classroom using the lens of environmental studies, where students study the 
earth, life, and physical strands of science. EARTHS promotes learning as an integrated, interdisciplinary process rather than as a series 
of isolated subjects. Discrete skills are taught rather than discrete subjects. By delving more deeply into subjects with a cross-curricular 
approach, EARTHS helps students acquire the tools of inquiry and problem solving so that each may become an imaginative and 
independent thinker. 
 
SCHOOL DESCRIPTION 
 
EARTHS Magnet School with an enrollment of 543 students. Our school serves students in grades K-5. Students attend school 385 
minutes per day. The average class size is 22 students in grades K-3 and 32 in grades 4-5. Kindergarten students participate in an 
extended day program. The Conejo Valley Neighborhood for Learning Preschool is located on the EARTHS campus and has an 
approximate enrollment of 40 students split between a morning and afternoon sessions. 
 
EARTHS promotes learning in a seamless, integrated, interdisciplinary process built on the Common Core standards. The focus on earth 
sciences and technology provides students with an opportunity to learn through active engagement and allows students to receive an 
in-depth science exposure. By delving more deeply into subjects with a cross-curricular approach and using earth science and 
technology as a unifying theme for exploration, EARTHS helps students acquire the tools of inquiry and expression so that each student 
can construct a personally meaningful understanding of the world and become an imaginative, independent thinker. This approach, 
coupled with the strong presence of parents through a minimum of three volunteer hours per month per family, along with our 
community partners, creates an environment where students develop a capacity for service learning, cooperation, empathy, and 
responsibility. 
 
A highlight of our educational program is an outdoor education program created in partnership with the Santa Monica National Park 
(SAMO) Rangers. The program ties classroom instruction to hands-on learning at Satwiwa National Park. The program is divided into 
three grade level groups: SEEDS (Students Experiencing the Environment by Doing Science) for K-1, SPROUTS (Students Practicing 
Research Outdoors Using Technology and Science) for grades 2-3, and SHRUBS (Students Helping Restore Unique BiomeS) for grades 
4-5. These programs integrate grade level standards with service learning, character building, and environmental awareness.