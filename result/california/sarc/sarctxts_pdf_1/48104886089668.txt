The Solano County Community School, otherwise called Golden Hills Community School, provides a well-rounded curriculum for 
expelled and probation students residing in the Fairfield-Suisun School District boundaries. Our main goal is to assist students with 
getting back on track with their academics and discipline. We are dedicated to helping students successfully return to a comprehensive 
high school in the district where they can finish their education. We have our initial WASC accreditation so students who finish at 
Golden Hills can now be issued a high school diploma. The class size at our Community School is limited to 24 students. We currently 
have one classroom ranging from 7 through 12th grade. The class has a teacher and a paraeducator. Also on site is a Student Support 
Specialist and the Program Administrator for Educational Options. The Student Support Specialist provides case management and 
social emotional intervention support services. This individual may conduct home visits, coordinate services with the Juvenile Justice 
System and when appropriate, participate as an Individualized Education Program (IEP) team member. This individual provides one-
on-one and small group counseling for students after school. This person is the liaison with the Fairfield-Suisun school district. All our 
textbooks are state approved and standards-based. 
 
The Community School focus has shifted over the last four years from a program primarily based on Literacy and Mathematics to a 
more broad-based approach, including all four core subjects- Mathematics, English, Science and History-Social Science. Text book 
adoptions in all four areas have made this possible. 
 
The Solano County Office of Education's Distance Learning Program is open to eligible students from the county's six school districts 
who have not been successful in their district's alternative school settings. Students may be referred to the program by their district's 
Student Services Department. This program is designed to allow students access to the Learning Lab at least three days per week, 
with a minimum requirement of meeting with the teacher once a week. Each student is required to complete a minimum of 20 hours 
per week of on-line school work. We encourage students to exceed this amount of time so that more credits can be earned. This is a 
program built on self-motivation and individual success. Therefore, parental support is critical to support student learning.