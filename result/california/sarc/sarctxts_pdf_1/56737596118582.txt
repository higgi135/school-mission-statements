MISSION STATEMENT 
 
Sycamore Canyon fosters a safe, collaborative, and innovative K-8 learning community that enables its students to strive for excellence 
in academics, citizenship, creative expression, and physical fitness. 
 
SCHOOL DESCRIPTION 
 
Sycamore Canyon School supports the District's mission by providing a comprehensive and rigorous, yet balanced educational program 
with an on-going support system that enables all students to grow academically and socially in a setting that fosters mutual respect 
and good citizenship. We celebrate learning, literacy, and community outreach. 
 
Our vision for Sycamore Canyon School is to continually strive for success of every student. We will continue as a California 
Distinguished School - designated in 2008, 2013, & 2018 and four-time California Honor School as a premier provider of a quality 
inclusive education that meets the needs of all students, K-8. In partnership with families and the community, Sycamore Canyon School 
provides a safe and engaging environment that cultivates the fundamental skills of thinking, learning, problem solving, and 
communication. We provide a comprehensive, rigorous, yet balanced and fun educational program that enables all students to grow 
academically and socially and develop into lifelong learners with unlimited potential. 
 
Our Vision Through Core Values 
 
Sycamore Canyon will continually strive to fulfill this vision by: 

• Maximizing each student’s potential through the sharing of best practices, providing engaging and dynamic learning 

activities, and consistently choosing to do what is in the best interest of each student 

• Promoting students’ belief in themselves, stretching them beyond the curriculum through differentiated teaching and 

• 

• 

• 

learning, and fostering developmentally appropriate critical thinking skills 
Encouraging students in their development of honesty, courage, humility, justice, compassion, leadership, service, and a 
respect for others as we contribute to our school community and beyond 
Inviting parents as partners in the educational process through meaningful and timely communication, useful 
opportunities to volunteer, and a valuable voice in their child’s development and learning 
Fostering participation in a wide range of purposeful classroom and extracurricular activities designed to encourage, 
challenge, and enrich the lives of all students 

• Continuing to build a safe and inclusive environment where staff encourages a balanced life, responsible choices, and 

overall wellness 

Our Vision Through Student Goals 
 
What will I learn at Sycamore Canyon? 

• Academic Excellence: I will demonstrate proficiency and confidence that I have knowledge, critical thinking, and study skills 

• 

• 

necessary for my education today and tomorrow. 
Citizenship: I will demonstrate an ability to express compassion and show respect for others, including myself, while in the 
classroom, elsewhere on campus, or within our community, and to offer my time, abilities, and service along the way. 
Communication: I will demonstrate an ability to communicate effectively by reading, writing, listening, and speaking 
critically and reflectively. 

• Research and Technology: I will demonstrate proficiency with the use of technology to conduct research, enhance learning, 

teach others, and creatively solve problems. 

• Wellness: I will demonstrate an ability to make positive choices with my time and interactions with others to live a healthy, 

physically fit, and balanced life. 

2017-18 School Accountability Report Card for Sycamore Canyon School 

Page 2 of 15