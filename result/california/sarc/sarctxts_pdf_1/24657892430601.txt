Principal's Message 
Greetings Atwater High School stakeholders. The AHS school community is going to begin to integrate our Schoolwide Learner 
Outcomes (SLOs) into what we do every day. We will begin drawing a nexus between school, goals, and future endeavors. We want 
students who attend AHS to understand the impact of our 4 SLO’s on their futures. 
 
AHS Schoolwide Learner Outcomes 18.19 
1. WE MAKE OUR COMMUNITY STRONG BY WORKING TOGETHER 
Student expectations-contribute to class discussions with enthusiasm and become engaged with other students; put in extra work 
more than the minimum required in order to make new discoveries; recognize the value of collaborating with people from differing 
belief systems, etc. 
 
Staff expectations-have a clear set of “agreements” to create a learning environment that invites students to try on different roles; 
model leadership that reflects open-mindedness, collaboration and innovation; allow students to make critical decisions throughout 
the process of completing an assignment; promote self-advocacy to develop the power within, etc. 
 
After 4 years at AHS, we want our students to understand that pride in one’s community and working together to continually improve 
our home is and should always be the goal. 
 
2. WE CELEBRATE OUR DIVERSE CULTURES AND PERSPECTIVES 
Student expectations-assess relationships with others to determine who are good partners; be active listeners in class or small group 
discussions; draw on outside sources for information and personal experiences, recognizing the value of collaborating with people 
from differing belief systems, etc. 
 
Staff expectations-celebrate, explore, and incorporate the cultural diversity of the school community at every opportunity. 
 
After 4 years at AHS, we want our students to not only respect themselves and others, but to celebrate different cultures and 
perspectives. Respecting self includes making healthy choices and engaging in the educational process. Celebrating diverse cultures 
and perspectives leads to valuing the opinions of others. 
 
3. WE CREATE CHANGE BY SHARING INNOVATIVE SOLUTIONS 
Student expectations-when problem solving consider a full range of resources; formulate a complete, comprehensive hypothesis as 
well as a potential solution to the task, etc. 
Staff expectations-value and reward curiosity-praising students’ questions, explorations, and investigations that contribute to their 
own or classroom learning, etc. 
 
After 4 years at AHS, we want our students to have the skills to solve problems and/or deal with situations that may not have an 
immediate solution. They must have the skill set to effectively deal with old and new dilemmas. 
 
4. WE USE OUR IMAGINATIONS TO PURSUE OUR OWN PATHS 
Student expectations-embrace the idea that attempting/experimenting is an important part of the path of success, and approach 
opportunities with an understanding that many failed attempts are likely; apply creative ideas to make a real and useful contribution 
to the work; articulate thoughts and ideas effectively using oral, written, and nonverbal communication, etc. 
 
Staff expectations-provide authentic tasks involving messy data to encourage quick thinking and improvisation during learning 
activities, etc. 
 

2017-18 School Accountability Report Card for Atwater High School 

Page 2 of 17 

 

After 4 years at AHS, we want our students to be able to use their imaginations to create a future for themselves that will lead them 
to being healthy, productive and self-sustaining. We want our students to understand that the careers that await them may not have 
been created yet. They will need the faculty to form new ideas based on fascination, attention, and curiosity. 
 
Lastly, our school-wide learner outcomes exist under the umbrella of our overarching goal of our mission statement: The mission at 
Atwater High School is to provide a diverse educational experience that develops academic, technical and social skill sets in preparation 
for college, careers and life. 
 
School Description 
Atwater High School is one of six comprehensive high schools in the Merced Union High School District. The school is rich in diversity: 
the multitude of ethnicities, backgrounds, and cultural heritages represented in the student population is a source of strength and 
learning at the school. Built in the northwest section of the city of Atwater in 1958, it covers 60 acres. AHS has a permanent capacity 
of 1800 students. AHS receives federal funds (Title 1, Title III) and is one of two comprehensive high schools in Atwater. The significant 
languages spoken by the student body at AHS are English, Spanish, and Hmong, representing the variety of cultural backgrounds of 
central California.