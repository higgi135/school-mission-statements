Our mission is to provide every child with regard to diversity in ability (e.g. physical, social/emotional development, cognition), socio-
economic status, language, gender identity, race, family structure, residence, and religion, with an opportunity to develop essential 
skills and acquire the knowledge based on high standards and expectations. Student needs are met through a coordinated effort by 
the school, home, and community in an environment that promotes respect, encouragement and direction. Student academic 
achievement and social growth is measured based on school, district, and state expectations.