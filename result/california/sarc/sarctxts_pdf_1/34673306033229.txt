Rancho Cordova School has a student body enrollment reported on the California Basic Educational Data System (CBEDS) in October 
2017, of 410 students Transitional Kindergarten through Fifth Grade students, representing the diverse cultural background of the 
Rancho Cordova community. The school campus houses a library, multipurpose room, cooking kitchen, STEM lab, large playground 
with updated equipment, and 20 classrooms. Additionally, Rancho Cordova has a state preschool and two classes for students with 
mild to moderate learning disabilities. Rancho Cordova Elementary teams with a state program, ASES, to be able to offer an after 
school program to support students on campus until 6pm Monday-Friday. 
 
The academic program at Rancho Cordova Elementary School stresses the importance of every child gaining proficiency with the 
Common Core State Standards. Student progress is continually monitored with intervention programs specifically designed to provide 
additional support. Building reading skills and proficiency in mathematics is our top priority. Support for students goes beyond the 
core classroom experience. A coordinated effort between teachers, a Title 1 Resource Teacher, intervention teachers, and instructional 
assistants is aimed at success for all students. 
 
Our school community provides a safe and caring environment where all children are encouraged to do their personal best. Character 
education skills are taught regularly and behavioral expectations are high. Students at Rancho Cordova participate in district-wide 
Anti-Bullying curriculum and implement PBIS strategies to reinforce the importance of developing Pro-Social Skills and a strong sense 
of character. This curriculum supports our efforts to identify and reduce incidents of bullying or potential bullying. 
 
Students at Rancho Cordova have access to state-of-the-art technology including a STEM lab with a supplementary curriculum and 
Chromebooks. All teachers have computers which are used for electronic attendance and record keeping. All classrooms have 
interactive Smart Boards and document cameras which are used for lesson delivery and enrichment. Students also have the ability to 
practice and expand their skills using classroom computers and wireless Chromebooks. Students are encouraged to participate in 
extra-curricular programs that promote an appreciation of the arts and music. The school is well-known for its Cinco de Mayo 
performance in May. 
 
Parents, staff, and community work together to provide each student the opportunity to experience school success. Volunteers are 
welcomed daily and provide valuable support to students as well as to teachers staff. Our PTA provides funding for field trips and 
additional experiences that connect families to the school such as family nights and assembly programs. We have a very successful 
Spanish Heritage Language Saturday School. Students come together to further their academic success.