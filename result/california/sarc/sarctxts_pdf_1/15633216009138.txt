The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters. As you read the 
Report Card, you will gain a better understanding of Owens Primary as a school with a record for improvement, a faculty that is 
professionally skilled and personally committed to meeting the learning needs of students and a student body which is enthusiastic 
and motivated to perform well. 
 
Owens Primary School is named in honor of Bessie E. Owens who was a well-known educator most famous for her work as a public 
school teacher for forty-five (45) years. Owens Primary School is located on 10 acres of land in southeast Bakersfield. 
 
Mission: Owens Primary School's mission is to create a collaborative culture between families, school, staff and community to ensure 
all students learn to their highest potential. 
 
Vision: Owens Primary School is a learning community committed to achievement, inspiration, and the celebration of learning.