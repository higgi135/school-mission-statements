At Indian Hills, our dedicated staff implements a data-driven, common core standards-based curriculum along with the very best 
educational teaching methods in reading, writing, and math in order to provide to all of our children an exemplary education. Indian 
Hills Elementary School supports and implements the LCAP Overarching Goals: All students will be college and career ready, all 
students will have a safe, orderly, and inviting learning environment and all students will benefit from the district engaging and 
sustaining the trust and involvement of parents and community in the educational process. As indicated by research, "learning" needs 
to be the focus in an effective educational process, and the Indian Hills staff is dedicated to helping students develop as intrinsically 
motivated and independent learners. Common Core Standards-based instruction promotes student learning to mastery and is utilized 
as a basis for expanding critical thinking skills; ethical, cultural, social and economic literacy; and behavior and values that are essential 
to a democratic society. 
 
We, the staff, families, students, and community members of Indian Hills Elementary School come together as a team to take an active 
and responsible role in setting high expectations for learning. We will provide a quality education in a safe and nurturing environment, 
thereby empowering future generations to become productive citizens of our society.