Built in 2001, our award-winning school was designed to provide students with a facility to succeed in the 21st century while keeping 
the heritage of the original 1960's site. The school is surrounded by trees and "red hill," which is connected to the strong community 
history of Tustin. 
 
The mission of Red Hill Elementary School is being committed to providing an engaging and challenging instructional program for all 
students. We provide a safe and nurturing environment which promotes character-building and establishes a foundation for life-long 
learning and achievement. We are a dedicated community of students, staff and parents who work collaboratively for the success of 
all Red Hill students. 
 
Our focus is student success! High standards, quality instruction, strong collaboration, and a strong sense of community are what 
guide Red Hill in achieving our mission. 
 
Our strength comes in the dedication all members of the Red Hill community have to providing an outstanding educational experience 
through strong programs for our students. The curriculum is both rigorous and comprehensive. Differentiated instruction is essential 
to meeting each student individually. Our instruction is designed to engage students as we prepare them to be the leaders of 
tomorrow. Our practices are in Balanced Literacy and Cognitively Guided Instruction (CGI). We want to build independent readers 
and writers who analyze their learning. We also want mathematicians who are able to reason and problem solve in various math 
experiences. Coding and robotics are additional components to our school curriculum. 
 
Parents are an integral partner in the success of our school. Countless hours of volunteering in the classrooms throughout the years 
have built a strong relationship with teachers to support our students. Continuous PTA support through volunteering and fundraising 
provide enriching learning opportunities and materials such as educational assemblies, Art Masters, the Red Hill Garden, and upgraded 
technology throughout the campus. 
 
As our community moves forward in reaching our mission of learning, our Red Hill Rockets will soar!