Our Mission Is Learning 
“We will ensure that all our children will learn more today than yesterday, and more tomorrow than today.” 
 
Our school facilities include 57 classrooms, small group instruction rooms, teacher workrooms, science labs, as well as labs for culinary 
arts, computers, visual arts, and technology. The facility is equipped with locker rooms; a lighted, covered basketball court; a 
fitness/weight center and dance studio; instrumental, band and choral music rooms; media center; large multipurpose room; health 
office; attendance office, and administrative offices. 
 
Las Flores Middle School has been a California Distinguished School twice and is an exciting learning environment. 1:1 technology 
provides opportunities for students to conduct research, develop and synthesize data relevant to their studies. Our computer labs 
contain PC and Apple computers. All classrooms are connected to the Internet through wired and wireless connections, allowing 
students to stay at the forefront of our ever changing digital world. Nearly all "core" subject classes have Chromebooks for student 
use on a daily basis. Classrooms without an assigned cart of Chromebooks are able to check a cart out for use.School Loop on the web 
offers students and parents the ability to check grades and assignments from home, stay connected to their teachers and learn of 
upcoming events. 
 
The focus of our school program utilizes a team approach. Teachers collaborate and review student data regularly. A clearly established 
set of academic and behavioral expectations for students are positively recognized and consistently reinforced by our incredible staff. 
The focus of our PE program is lifelong fitness. LFMS offers a dynamic electives program including world languages, art, culinary arts, 
computers, media, photography, ASB, PAL, drama, orchestra, choir, and band. 
 
A student study team meets regularly to design a system of interventions for students who are referred by our staff members. Las 
Flores Middle School has a number of unique programs and offerings for students, including a Bear News Network, Medieval Faire, 
Ancient Wonders of the World, field trips, and lunchtime activities. Students are also encouraged to compete in the annual Spelling 
Bee and the Reflections visual and performing art contest, both of which are supported by our active PTSA. Our school also has an 
active Associated Student Body which organizes fundraisers, dances, assemblies, and spirit activities. LFMS also has a Peer Assistance 
& Leadership (PAL) class to support student initiatives towards making our school and community a welcoming environment. 
 
For additional information about school and district programs, please visit www.capousd.org