Built in 1961, Piedmont Middle School is the oldest middle school in Berryessa Union School District. Piedmont has a long-standing 
tradition of preparing students for a successful high school experience. Staff members are highly committed to the overall well being 
and success of our students. During the summer of 2017, Piedmont Middle school underwent a modernization project that updated 
all the classrooms and common areas. A new Flexible Instruction Space is currently under construction and is scheduled to be opened 
in November 2018. 
 
Piedmont Middle School is comprised of a diverse student body of students in grades 6, 7, and 8. The ethnic breakdown of students 
at Piedmont is 38% Asian, 35% Latino, 14% Filipino, 5% White, 3% African American, and 5% of Two or More Races. As a Title I school, 
Piedmont Middle School has 45% socio-economically disadvantaged students and 19% EL students. Demographically the staff at 
Piedmont closely mirrors the demographics of our student population with Filipino, Latino, Chinese, Indian, Vietnamese, Eastern 
European, and Caucasian staff members. 
 
The surrounding Berryessa/Piedmont community is supportive of the school’s Mission and Vision. The PTA is an active support 
mechanism that connects our school with the community through various fundraising efforts. Piedmont Middle has a full and 
successful athletics program that hosts several co-ed sports teams, including cross country, track, and wrestling, as well as girls softball, 
girls basketball, boys basketball, girls and boys soccer, and girls and boys volleyball. During the school day, Piedmont students are 
afforded the opportunity to participate in Intermediate and Advanced Band and Orchestra, Beginning, Intermediate, and Advanced 
Choir, Yearbook, Accelerated Math 7 and Accelerated Math 8. Student clubs include ASB Leadership, Gay-Straight-Alliance Club, Youth 
Act Community Service Club, and Math Competitions. 
 
Piedmont students and staff regularly use technology in teaching and learning. Currently, the ratio of students to Chromebooks is 
slightly better than 2:1. Our teaching staff utilizes technology more and more in a variety of ways in their daily instruction practices 
(Apps, websites, online resources, etc.) in an effort to align teaching practices with the Common Core State Standards. Piedmont 
Middle School is developing a distinct positive student culture through the implementation of PBIS Tier 2 and Restorative Justice 
practices, along with targeted student support groups. 
 
Piedmont Middle School MISSION STATEMENT 
Piedmont Middle School strives to provide a diverse and inclusive education and environment that promotes 21st Century Skills to 
prepare our students for the future. 
 
I Innovative 
D Diversity 
E Engagement 
A Achievement 
 
Piedmont Middle School VISION STATEMENT 
The mission of Piedmont Middle School is to embrace a school culture of diversity and achievement by offering a holistic 21st Century 
education that gives our students... 
 

• A rigorous California state standards-aligned curriculum 
• 
• 

Engagement in personal development 
The opportunity to become socially and academically prepared community leaders. 

 
 
 

2017-18 School Accountability Report Card for Piedmont Middle School 

Page 2 of 11