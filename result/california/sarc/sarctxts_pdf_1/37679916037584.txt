Vision Statement: 
 
Cajon Valley Middle School's students and staff thrive in diverse and global environments. We possess critical thinking and problem 
solving skills that enable us to embrace and promote change and innovation with confidence. We are inquisitive and have passion for 
learning. We interact respectfully and ethically with others. 
 
Mission Statement: 
 
In collaboration with our families and community, Cajon Valley Middle School provides a balanced, relevant and rigorous education in 
an environment of caring and supportive relationships. 
 
Our Shared Commitment: 

• Accept no limits on the learning potential of any child. 
• Meet the individual learning needs of each child. 
• Create engaging classroom learning environments. 
• 
• Hold students, parents, and each other to the highest standards of performance. 
• Collaborate regularly with colleagues to seek and implement more effective strategies for helping each child to achieve 

Treat students, parents, and colleagues with courtesy and respect. 

his or her academic potential. 

• Do whatever it takes; go the extra-mile to ensure that every student achieves.