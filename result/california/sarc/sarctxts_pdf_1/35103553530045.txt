San Benito County Juvenile Hall Court School is operated by the San Benito County Office of Education and serves youth in grades 5-
12 that have been sentenced by the San Benito County Juvenile Court and are detained at Juvenile Hall. The staff believes all students 
can learn, given high expectations. Education is a shared responsibility which includes parents, students, staff, and the community. 
The mission of San Benito County Juvenile Hall Court School to provide students with a safe, positive, learning environment with 
modern resources and accommodations necessary to learn. The goal is to educate all students to their highest potential, so they will 
have the greatest range of personal options upon graduation. It is the 21st century vision of San Benito Alternative Education sites to 
deliver meaningful, accessible curriculum through multiple avenues in a safe, supportive, positive learning environment. With the help 
of our entire community we will empower our students by developing the necessary skills needed to join our complex world as an 
engaged citizen. 
 
Teacher(s) at San Benito County Juvenile Hall Court School consistently align curriculum maps, lessons and formative assessments to 
the Common Core State Standards to achieve positive classroom results. Teaching staff provides core academic courses in English 
Language Arts (ELA), Algebra Readiness, Algebra, History/Social Science and Science. In addition, migrant education and special 
education services are available. Computers are available to students within the classroom as San Benito County Juvenile Hall Court 
School embraces technology. Each student is assessed in English and Math. Personal and/or crisis counseling is provided by the school 
counselor and San Benito County Behavioral Health. The principal and various other staff members provide additional mentoring and 
coaching in the areas of communication, grades and social development. San Benito County Juvenile Hall Court School staff receives 
professional development in Restorative Justice and is in the process of implementing elements of this program. 
 
San Benito County Juvenile Hall Court School provides a safe, organized and supportive environment that offers challenging and 
equitable opportunities for all students, thereby promoting equity and diversity. It fosters academic achievement of all students, while 
developing vocational and interpersonal skills required for success in a rapidly changing and technological world. The staff of San 
Benito County Juvenile Hall Court School instills a strong work ethic and respect for the community effort, while preparing students 
for active and productive roles in society as adults. 
 
Prior to the 2015-16 school year, Pinnacles Juvenile Hall Court School and Pinnacles Community School shared a CDS code. As a result 
one SARC was completed for both schools. Data for the 2015-16 SARC based on 2014-15 and earlier reflects both sites. Data for 2015-
16 is based solely on the Juvenile Hall Court School.