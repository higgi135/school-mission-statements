Don Stowell Elementary School opened it's doors in August of 1998. It is named after a man who devoted 39 years of his life to the 
Merced City School District students and staff. Stowell Elementary School is a neighborhood school on a traditional calendar. The 
school serves students in grades preschool through sixth grade. Stowell is located in an economically challenged area in south Merced. 
Most students attend Hoover Middle School or Tenaya Middle School upon completing sixth grade at Stowell Elementary School. A 
top priority, the entire staff of Stowell School strives to meet the academic, social, emotional, and physical needs of the students. The 
school is particularly proud to be the flag ship school on the Galen Clark Educational Complex. Included on the Clark Complex are 
multiple preschool classes, county preschool classes, and many special education classes through Merced County Office of Education. 
Don Stowell Elementary School is dedicated to providing a safe and nurturing student-centered atmosphere for all students. The staff 
is committed to providing a challenging and rigorous learning environment for student success as they strive to meet and exceed State 
curriculum standards. We stand together as we prepare our students for their future. Many voices, one vision of excellence!