Principal's Message 
Our objective at York School is to prepare every child to achieve success. I believe that all students can learn and I am grateful for the 
opportunity to work with your children. The York staff provide a safe, challenging and effective educational program for students. The 
ultimate goal is for all students to develop their capabilities to their maximum potential. To achieve this goal, we must work as a team. 
The York staff is excited to provide every possible opportunity to come together with families and the community to enhance the 
educational experience for all children. York School received the Gold Ribbon Award and Title I Achievement Award from the California 
Department of Education. I'm honored to be the principal of York School and I look forward to working with your children. 
 
Mission Statement 
To maximize each student’s potential to achieve educational excellence. 
 
School Vision 
A diverse community of lifelong learners who excel and positively contribute to an advancing global society.