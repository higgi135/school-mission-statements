The mission of Citrus Elementary School is to provide a safe, positive environment in which individual students have the opportunity 
to meet their full potential academically, socially, emotionally, and creatively. Students will learn to respect themselves and others, as 
well as the world in which they live. The responsibility for helping students develop these essential skills will be shared by parents, 
school staff, community members and the students themselves. Citrus is committed to technology and has equipped the school, 
teachers, and students with the upgraded technology to be successful in a global society. We use a variety of resources and strategies 
to work toward this goal. 
 
 
 
Citrus Elementary is a neighborhood school located in an older part of Chico. The main school building was built in 1936 making it the 
oldest operating school in the Chico Unified School District. We serve students in grades TK - 5 who come to us with a variety of 
backgrounds given the ethnically and socio-economically diverse make-up of the neighborhood. 
 
 
 
Citrus is a school-wide Title I school. We receive federal categorical funding for Title I, and Title II, as well as funding to support our 
breakfast and lunch programs. Additionally, Citrus is able to provide extensive after school programs through the ASES Grant where 
students receive academic assistance and enrichment for three hours each afternoon.