Pacific High School is one of six traditional comprehensive high schools in the San Bernardino City Unified School District (SBCUSD) in 
San Bernardino, California. Located south of Perris Hill Park in the northeastern section of the city, the 46 acre campus boasts a 
spectacular view of Perris Hill and the San Bernardino mountains. The school has 105 classrooms, a library, an auditorium, a 
multipurpose room, and an administration office. The campus was built in 1952 and modernized in 1987 and 2013. Five new special 
education classrooms were built in 2007. The facility strongly supports teaching and learning through its ample classroom and athletic 
space, and a staff resource room. 
 
Pacific has two vice principals, one principal, and 80 teachers and support staff that serve a culturally diverse population of 
approximately 1,300 students. The school provides an enriching learning environment through a full range of academic and 
extracurricular activities in college prep, honors, and advanced placement classes; athletics; art; music; drama; and numerous 
organizations and clubs. Pacific High School has two career pathways—Digital Design & Communications and Biomedical Engineering. 
All students participate in the pathway of their choice. Each pathway leads to both college and career options, including options for 
vocational certificate programs. In addition, various intensive instruction classes and after school tutoring are available to assist 
students struggling with coursework and to prepare students to attend a four-year college or university. Pacific High School is 
dedicated to providing each student with the opportunity to acquire the knowledge and skills necessary to achieve personal, 
educational and career goals. 
 
Vision Statement 
Our vision is to be an exceptional school, committed to preparing all students to be socially responsible lifelong learners, who are 
ready for college, and a career in a globally competitive world. 
 
Mission Statement 
Pacific High School provides many opportunities to ensure all students graduate with the experience necessary to meet the needs of 
a global society. Digital Design, Biomedical, and Engineering students will be prepared to successfully think, act, and communicate in 
college, career, and life. 
 
Expected Schoolwide Learning Results: 5 Cs 
 
Effective Communicators who: 
· Develop effective presentation and communication skills 
· Convey ideas through coherent and focused writing 
· Defend positions with evidence-based statements that are appropriate in personal and professional settings 
 
Complex Thinkers who: 
· Apply knowledge, skills and experience to solve problems relevant to academic content and a broad spectrum of careers 
· Synthesize information, make connections, and draw conclusions 
· Persevere in solving problems 
 
Collaborative Workers who: 
· Value the individual contributions made by each team member and understand how to build consensus for decision making 
· Use social awareness and interpersonal skills to establish and maintain positive relationships 
· Demonstrate integrity, respect and ethical behavior 
 
College and Career ready students who: 
· Participate in a Linked Learning academy to develop career readiness skills 
· Set and pursue realistic and challenging educational goals for learning, growth, and success 
· Exhibit regular attendance, appropriate appearance, and goal-driven behavior 

2017-18 School Accountability Report Card for PACIFIC HIGH SCHOOL 

Page 2 of 19 

 

 
Community Participants who: 
· Make a positive contribution with others 
· Appreciate the contributions of others 
· Respect diversity"""