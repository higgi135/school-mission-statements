MISSION STATEMENT 
 
The mission of the Conejo Valley Unified School District is to meet the academic, cultural, social, and individual needs of students in 
order to prepare them to make a meaningful contribution to a democratic society. The Westlake High School mission reflects the 
District's mission through our School-Wide Learning Outcomes (SLOs). 
The Westlake High School School-Wide Learning Outcomes (SLOs) are that our students be: 
 
1. Informed and Independent Learners 
2. Effective Communicators 
3. Active Community Members 
4. Innovative and Engaged Scholars 
 
SCHOOL DESCRIPTION 
 
Westlake High School offers a full range of programs in academics, fine and performing arts, and athletics. In the academic arena, we 
offer 24 Advanced Placement courses and a full range of college preparatory classes. During the 2017-18 school year, WHS 
administered 1,984 Advanced Placement tests with an 86.1% pass rate and 93% of our graduating seniors continue their education at 
a college or university. This is a remarkable achievement for our students and staff. In conjunction with our academic offerings, our 
Westlake Academy, which is a California Partnership Academy, provides students a school-within-a-school experience with major 
emphasis on technology. In addition to our Academy, we have a full time Career Education Coordinator who oversees the 
development and sustainability of all Career Technical Education (CTE) programs, including our CTE pathways in Patient Care, 
Environmental Research, Software & Systems Development, and Production & Managerial Arts. 
 
With this strong push on academics, we are proud to offer academic support through our academic support centers in Math, Science, 
World Language, Social Science, and Writing. These are peer-to-peer tutoring centers so all students can receive help from peers that 
have been in the class before. In addition, our counseling department realizes the importance of supporting our students with the 
social-emotional aspects that our students face on a daily basis. Our counselors utilize their training along with district and county 
resources to help support our students and families through the ups and downs of high school. 
 
In addition to our strong focus on academics and the support that our students and families need, we realize the benefits of well-
balanced students that are participating in school outside of the classroom. We are proud that over 70% of Westlake students 
participate in co-curricular activities including athletics, ASG, clubs, community events, band, choir, drama, dance, and orchestra. 
Being involved in these outside activities allows our students to realize the positive benefits that come from working with others in 
various environments and situations. 
 
Lastly, Westlake High School possesses a highly qualified and effective teaching staff, an active PTSA, and a high degree of parental 
involvement. All of these components add to the outstanding support and experiences that is created for our students on a daily basis 
and allow Westlake High School recognition as a 2016 Nation Blue Ribbon School for academic excellence and a 2017 California Gold 
Ribbon award! 
 
 

2017-18 School Accountability Report Card for Westlake High School 

Page 2 of 18