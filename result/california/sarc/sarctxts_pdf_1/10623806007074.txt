Raisin City Elementary School District is located in the unincorporated area of Raisin City, California in Fresno County. The School 
serves as the community center. Agriculture is the only industry in the community. The formal structure of the town includes three 
grocery stores, three churches, a post office, and the school. Most students are bused to school through a contract with Southwest 
Transportation Agency. The School is on ten acres and is surrounded by grape vineyards on the north, west, and south sides. On the 
east side of the school is the residential area of the town. Most business at the school is conducted in both Spanish and English, since 
we have a very large bilingual population. The student population in 2017-18 was 298 students in grades kindergarten through eighth 
grades. All the classes are self contained and taught by highly qualified teachers. Raisin City Elementary School District promotes an 
environment for success and creates partnerships with parents and the community so all students will achieve their full potential to 
become lifelong learners and responsible and productive citizens and leaders. Our vision is for all students to be proficient at their 
grade level in language arts, mathematics, science, social studies, physical education and visual and performing arts.