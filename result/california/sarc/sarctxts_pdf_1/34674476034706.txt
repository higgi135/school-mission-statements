PRINCIPAL'S MESSAGE 
At Mariemont Elementary School we pride ourselves on truly being a community school! We value and nurture our relationships with 
our parents and families who are our partners in everything that we do each and every day. Staff, students and parents celebrate 
diversity and demonstrate that everyone will be treated with dignity and respect at all times. We are dedicated to providing a 
comprehensive, standards-based education for our students in a positive, safe, and caring environment. At Mariemont, great emphasis 
is placed on acquiring the skills necessary to become productive citizens in our ever-changing world. We strive to encourage, challenge, 
and inspire our children to be creative and responsible students who are committed to life-long learning. 
 
MISSION STATEMENT 
The mission of Mariemont Elementary School, created collaboratively with staff, parents and community through site strategic 
planning is as follows: 
 
As a collaborative community committed to excellence, we at Mariemont Elementary School educate and inspire each student to 
become self-reliant, well-rounded, critical thinkers, and life-long learners, who positively contribute to society by providing all students 
with challenging, personalized instruction that integrates current technology and encourages innovation and creativity, in a diverse, 
inclusive and dynamic learning environment. 
 
SCHOOL PROFILE 
Mariemont Elementary is one of 41 elementary/K-8 schools in the San Juan Unified School District. 
 
The curriculum provided is aligned to the California Content Standards. The school supports cultural awareness on a daily basis through 
its diverse literature selections and other school activities. Mariemont’s programs reflect the interests and needs of its students and 
the surrounding community. 

• Mariemont is a district site for the Special Education Full Inclusion model. Our program serves 19 identified students each 

year. We also support two SDC classes which serve students with disabilities in the moderate to severe range. 

• Mariemont’s parent volunteers assist with our reading program, serve as tutors, edit and publish both our newsletters, 

• 

and participate in the planning of the many activities that occur each year. 
Intervention is provided for K-5 students, as needed, in the areas of reading through small group instruction and the Fontas 
and Pinnell Intervention System. 

• Performance opportunities are built into the classroom experience as well as various school-wide opportunities such as 

• 

• 

classroom/grade level plays or musicals, band 4-5, and choir 4-5. 
Family activities include, but are not limited to, Fall Carnival, Kids Helping Kids Night, Book Fair, PJs & Pancakes, Science 
Fair, Family Science Night, Art Extravaganza, band/choir concerts, and Dinner/Open House. 
Enrichment is offered after school throughout the year. Incredible Adventures focuses on science, Kids Art, Missoula 
Theater Company, Spanish, Robotics and many more opportunities are provided throughout the year. 

 

2017-18 School Accountability Report Card for Mariemont Elementary School 

Page 2 of 11