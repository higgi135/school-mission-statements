History 
Founded in 2002, KIPP Bridge Academy is a K-8 public charter school located in West Oakland that is free and open to all students. 
With an extended school schedule, high expectations, and a focus on results, KIPP Bridge Academy students are proving the possible. 
KIPP Bridge Academy is a California Distinguished Charter School and is in the top ten percent of all public schools in the state. 
 
Vision 
The vision of KIPP Bridge Academy is to provide all students with an outstanding education that emphasizes critical thinking, reading, 
and writing. This education, along with the promotion of excellent citizenship and the appreciation of diversity, will develop college-
bound community leaders. 
 
Mission 
The mission of KIPP Bridge Academy is to develop the academic knowledge, skills, and character traits necessary for students to 
achieve success in the finest high schools and colleges, and the competitive world beyond.