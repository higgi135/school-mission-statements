Welcome to Flowery School! At Flowery we are proud of our dual immersion (DI) program. Imagine a school…..in which children from 
all backgrounds excel. Imagine a school that treats all children as gifted, building on their strengths by integrating technology, garden, 
music, library and art into the core subjects. Imagine a school that does this in both English and Spanish. Imagine a school in which all 
members of the school community contribute to the vision of their ideal school and in which they collaborate to achieve that dream. 
Imagine a school where ideas count and where students are educated for success in a global society. Let your imagination go as far as 
it can, and you have discovered Flowery School. 
 
Our Mission 
 
Flowery School will foster students who: 

• have strong literacy skills and are capable of reaching high academic standards; 
• 
• 
• will have the opportunity to acquire a second language. 

are productive members of society to whom all choices for higher education, employment and life-long learning are open; 
are responsible citizens who have the necessary skills to live cooperatively in a diverse world; 

All members of the Flowery community – parents, staff, neighbors and students - collaborate in creating program choices to respond 
to student need. 
 
Our Core Values: 

• We believe in students, their families and the community and value its diversity. 
• We believe that adults and students work better as part of a team. 
• We value the dedication and commitment of our unique staff. 
• We believe that making curriculum meaningful, creative and accessible reaches all levels of learners. 
• We value our traditions and positive experiences that foster life-long learning and create bilingual, bicultural students. 
• Each child matters and deserves physical and emotional safety. 
• There is mutual respect among students and adults. 
• We are dedicated to developing responsible behavior among all students.