Bell Hill Academy is located in the historic Sierra Nevada Foothills community of Grass Valley. We are a small K-4 school with an 
enrollment of approximately 220 students in 10 classrooms. We have a before school program and a fully-enrolled after school 
program on site. 
 
At Bell Hill Academy, our goal is to prepare students to be active, knowledgeable citizens of a global society. Through our school-wide 
Global Studies curriculum, students develop an understanding and respect for their own and other cultures. Our focus is on the 21st 
century skills that our children will need to participate successfully in a global marketplace: critical thinking, collaboration, creativity, 
and communication. We recognize the importance and benefits of learning to communicate in a second language. In our Dual 
Immersion track, core academic instruction is delivered in Spanish. 
 
The Dual Immersion (Spanish-English) program began as a single Kindergarten classroom in 2012, and expanded to Lyman Gilmore 
Middle School in the 2017/18 school year. The Dual Immersion instructional program is unique in Nevada County. Bell Hill Academy's 
program brings together both Spanish-speaking and English-speaking students in a carefully planned educational program in which 
they learn and achieve in two languages. Children from both language groups work and study together throughout the school day. In 
an increasingly diverse and multicultural world, our school's program offers English-speaking children one of the best opportunities to 
acquire a necessary second language to a high degree of proficiency. Spanish speaking students have the opportunity to achieve at a 
high level of proficiency in English, as well as to maintain and fully develop their native language. 
 
Our Vision: Bell Hill Academy students will develop cross-cultural awareness, and prepare for success as responsible and contributing 
citizens in a global society. 
 
Our Mission: Bell Hill Academy provides a 21st century education that prepares students for responsible decision-making and 
citizenship in a global society. We provide learning opportunities and language experiences that broaden students’ understanding of 
world issues. Through lessons and a variety of cultural experiences, students develop Global Awareness, with a focus on valuing the 
commonalities and differences among different cultures. We encourage and equip students to be academic risk takers through a 
curriculum that develops the 21st Century Skills of critical thinking, cross-discipline thinking, creativity, innovation, collaboration, 
communication, problem solving, Growth Mindset as well as information and technology literacy.