MISSION STATEMENT 
Albert Schweitzer Elementary, an exemplary model of sophisticated student expression, empowers each child to collaborate and 
problem solve as a compassionate and highly literate 21st century citizen through innovative, research-based, instructional strategies 
that incorporate strong character values in partnership with families in a nurturing environment. 
 
SCHOOL PROFILE 
Albert Schweitzer Elementary is one of 41 elementary schools in the San Juan Unified School District. The 370 students attending 
Schweitzer are 60% white, 24% Hispanic or Latino, 3% African American, 3% Asian, 1% American Indian, and 9% two or more races. 
Our school provides a comprehensive academic program based on the Common Core State Standards. Schweitzer has one SDC LH, 
3rd-5th grades, and 1 CH classroom, K-2nd grades. We also have an Early Childhood Education preschool on our campus serving pre-
K students with language/speech needs. Our school has a comprehensive balanced literacy program based on the reading and writer's 
bookshop model, critical literacy Schweitzer Elementary utilizes the district adopted English Language Arts curriculum Benchmark for 
all grade levels. Our math program uses components of critical math integrated into the district's adopted math curriculum, enVision, 
for all grade levels. As a team of committed educators, we strive for research based professional development to enrich our instruction 
such as Project GLAD (guided language acquisition design) that provides learning strategies based on student collaboration that 
supports both English learners and English only students by accessing and mastering academic content standards. All grade levels 
have received responsive reading instruction with K-2nd grades completing their fourth year, working on the personalization and 
differentiation of reading instruction through the use of formative assessment tools, shared, and guided reading. We believe students 
can achieve at high levels and we tailor our instructional practices to challenge and support all students. 
 
An important component for our students to learn is to create a safe and risk-free environment that every child feels valued and 
successful in school academically, socially, and emotionally. A cohort of our teachers will begin this work with implementing PBIS, 
Positive Behavioral Intervention and Supports, along with bringing Mindfulness activities into the classroom with school-wide 
professional development to deepen our learning on the multiple students' behaviors hat can affect their learning. 
 
The goal of Schweitzer School is to provide each student with a challenging and rigorous curriculum appropriate to his/her academic 
level. We believe every child can achieve academic success. To this end, every child is provided with quality instructional experiences 
which recognize, support and maintain high expectations for all students. 
With parents and teachers working together as a team towards the same goal, every child will succeed. In order to give each child the 
best opportunity for success, we keep children first in all that we do. This is our core value and common cause. 
 
We also believe that children need exposure to instruction via the arts. We hold a bi-weekly whole school “sing” where we sing 
traditional and new songs. This has been in place for many years and is a well-loved tradition of our school. PTA directs a school play 
every year that includes all students interested in participating. In spring a production by Starstruck, a dance program that each 
classroom has an individual dance routine, is performed at two night shows. Students in grade 1 through 5 have one art class and two 
to three PE classes per week. At Schweitzer School, we take the business of loving and teaching children seriously. Each person at our 
school approaches his/her task with a keen understanding of the honor that it is to be able to touch the future through our children. 
 
 

2017-18 School Accountability Report Card for Albert Schweitzer Elementary School 

Page 2 of 11