San Lorenzo Valley Middle School: Working Together to Ensure All Students Learn 
 
San Lorenzo Valley Middle School is located in the beautiful redwood forests of the Santa Cruz Mountains. A school construction bond 
passed by the citizens of the San Lorenzo Valley enabled SLVMS to remodel the entire school in 2005, including twelve new classrooms, 
two new science labs, and two new restrooms. A second community bond led to the a new state-of-the-art library and computer lab, 
which opened in December of 2011. San Lorenzo Valley Middle School values the continued support of its community and parents. 
 
San Lorenzo Valley Middle School is a comprehensive middle school that offers language arts, math, science, social studies and physical 
education at each level, grades six, seven and eight. Our students are able to take advanced courses in mathematics and language arts 
in 7th and 8th grade. In physical education, students have access to a state-of-the-art turf field, an all-weather track and an Olympic 
swimming pool. SLVMS offers a full range of exploratory electives, including art, band, choir, drama, Spanish, technology, life skills, 
building and engineering, leadership, and robotics. In 2013-14, SLVMS incorporated an AVID program, beginning at the 8th grade level. 
SLVMS offers a varied Pyramid of Intervention for at-risk students. RTI is a school-wide focus. SLVMS offers intervention courses in 
Reading Support, ELD, Academic Support, and Directed Studies for both regular education and special education students. Team-
taught Language Arts and Math courses are offered, which creates fully-mainstreamed schedules for all resources students. We offer 
after-school homework centers, drama productions, after-school clubs, GATE activities, and a comprehensive, cost-free school sports 
programs. The school supports cultural awareness through its diverse literature selections and curriculum. 
 
SLVMS is dedicated to creating a positive, safe, and caring atmosphere in which students and staff are encouraged to reach their 
highest personal and educational potential. We recognize each person as unique with special needs and talents. Above all else, we 
foster high academic achievement, self-discipline, self-esteem, and self-knowledge. 
 
San Lorenzo Valley Middle is the only traditional middle school that serves 6-8 students in the San Lorenzo Valley Unified School 
District. Curriculum is focused on the Common Core standards. 
 
During the 2017-18 school year, 535 sixth through eighth grade students were enrolled at the school, with classes arranged on a 
traditional schedule/year-round calendar. 
 
SLVMS has been recognized as a 3 time Middle School to Watch, a California Distinguished School and a Gold Ribbon School. 
 
San Lorenzo Valley Middle School is working together with the San Lorenzo Valley community to ensure that all students learn.