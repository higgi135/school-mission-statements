Paul Ecke Central Elementary School is located in the western section of the City of Encinitas and serves approximately 650 students. 
Teachers and staff are dedicated to ensuring the academic success of every student and providing a safe and productive learning 
experience. The school has developed educational programs designed to provide the skills and tools necessary for students to explore 
their creativity while developing a strong educational base. 
 
Learning is facilitated through meaning-centered 
instructional strategies which utilize critical thinking, cooperation, and 
communication. Students develop self-respect, acceptance and appreciation for others in our diverse community. Paul Ecke Central 
School is committed to instilling a rich foundation of life-long learning, where character, creativity and risk-taking are taught, 
encouraged and valued. Students will become productive citizens skilled to meet the challenge of an ever-changing world. We offer a 
dual language strand where we provide literacy and content instruction in two languages (Spanish/ English) and integrate native 
English speakers with native Spanish speakers with the goal of bilingual and bi-literate students. 
 
Our mission is to prepare our children to be successful, contributing members of school, work and society. Working as a nurturing 
team of staff, parents and community members, we provide challenging bi-cultural and bilingual learning experiences. Our students 
are the focus of all site-based decisions. This strategy helps us educate children to become effective communicators, collaborative 
team members, constructive thinkers and problem solvers, self-directed learners, quality producers, and responsible members of 
society.