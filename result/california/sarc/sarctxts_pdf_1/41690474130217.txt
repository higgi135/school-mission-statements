Aragon High School's motto, "Excellence and Equity," was born from our vision that all students deserve an academic environment 
that offers them respect, rigor and high expectations. The entire Aragon community works in collaboration to create a school where 
all students achieve to their academic potential and develop skills necessary to realize their post-secondary goals. The Aragon 
community spends considerable time ensuring students master state standards. Teachers meet in learning teams to develop 
strategies that address students’ needs and move them towards academic excellence. The faculty and staff promote and encourage 
students to respect themselves and others, appreciate the value of diversity and creativity, and recognize problems as opportunities 
for change. Students are encouraged and supported to become lifelong learners who can explore a variety of academic, athletic, 
artistic and extracurricular areas and understand key facts and concepts in each discipline they study. 
 
Vision Statement 
Aragon supports all members of our community to realize their individual potential as productive participants in a diverse society and 
to pursue dynamic educational and career goals. 
 
 
Mission Statement 
To provide the highest quality academic and co-curricular programs that foster intellectual, emotional and physical well-being. 
 
The value system that underlies efforts to accomplish this mission includes these beliefs: 
 
 
· A school community should be a safe and caring environment that promotes respect, self-worth, creativity, and academic growth. 
 
· All members of the school community should work collaboratively. This process includes good communications, shared decision-
making and accountability. 
 
· Learning is a process in which all can participate and succeed. Inherent in the process is innovation, risk-taking, and the challenge 
of one’s personal limits. 
 
· Education must be responsive to the changing needs of our students, staff, and community. 
 
· The school community should foster the intellectual, emotional, and physical well-being of each student. 
 
· Diversity should be welcomed for the strength it brings to the education of all members of the school community. 
 
· Education should be celebrated as a lifelong process that fosters personal and positive growth. 
 
 
 
 
 
 
 
 
 

2017-18 School Accountability Report Card for Aragon High School 

Page 2 of 12