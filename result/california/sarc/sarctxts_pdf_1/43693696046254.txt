It is an honor to serve the McCollam School community. The Mccollam staff is committed to ensuring that all students receive an 
outstanding learning experience. McCollam Elementary School has an exceptional staff that provides quality instruction to each 
student. Our staff cares about students and their individual learning and social needs. 
 
I am very proud of the students at McCollam and their academic accomplishments to date. Our staff will continue to inspire and guide 
our students to even greater academic success. We will ensure that best practices, strategies, and approaches are being used to bring 
about an optimal learning environment for every student. Throughout teamwork, and a dedication to continuous improvement, 
students at McCollam will find their experience to be rewarding, challenging and enjoyable.