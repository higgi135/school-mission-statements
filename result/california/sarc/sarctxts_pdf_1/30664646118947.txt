Our mission is to ensure all students become contributing members of society, empowered with skills and knowledge necessary to 
excel in a changing world. 
 
Our vision is to instill the enjoyment of lifelong learning. 
 
As of September 2001, Ladera Ranch became home to Chaparral Elementary School. Ladera Ranch also holds the distinction of being 
California’s first connected community — where every home is wired with full Internet access. Chaparral Elementary School capitalizes 
on this technological infrastructure with e-learning opportunities for all students. 
 
Setting high standards and expectations for all students, the Chaparral staff works with the students, parents, and community to offer 
a sound educational program that really works. The Chaparral staff is truly outstanding. Approximately 800 students in preschool 
through Grade 5 work with our staff of 26 general education classroom teachers. We believe in the importance of student acquisition 
of basic skills and the ability to use those skills in all subject areas, including language arts, math, science, social studies, and the arts. 
Special emphasis is placed on reading; writing, listening, speaking, critical thinking, problem-solving, and student work products 
because, ultimately, students need to communicate their knowledge with others. Chaparral connects people to create a sense of 
community; connects the curriculum to achieve coherence; connects classrooms and resources to enrich the climate; and connects 
learning to life to build character. Chaparral promotes instantaneous e-learning with access to the Internet and web-based research 
for real-time learning. 
 
At Chaparral School, both students and teachers use a variety of technological resources as tools to increase student learning 
throughout all areas of the curriculum in an exciting, dynamic environment. Students use e-tools to produce quality work products 
and presentations. The third, fourth, and fifth grade classrooms have one-to-one Chromebook devices. Students in grades K-2 have 
access to Dell computers in the lab and Chromebook devices to share in the classrooms. Students use the devices in a wide variety of 
ways, including: writing, editing, revising, making presentations, Internet research, learning how to type, etc. Chaparral Elementary 
School promotes strong parent, community, and business involvement. This takes the form of continual school-home communication, 
parent volunteers, a great PTA, community volunteers, an on-campus YMCA for before- and after-school child care, plus the active 
seeking of donations and business partnerships. In essence, Chaparral Elementary builds a strong sense of community — a place where 
everyone comes together to promote teaching and learning. Character education and community service are important aspects of 
what we do and what we stand for at Chaparral. The 3-R’s of our Character Education Program are to be: Respectful, Responsible, and 
Ready to Learn. 
 
For additional information about school and district programs, please visit www.capousd.org 
 
 

2017-18 School Accountability Report Card for Chaparral Elementary School 

Page 2 of 11