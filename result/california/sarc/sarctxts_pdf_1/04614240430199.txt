Oakdale School is a K-12 independent study school within CUSD. It was created in October of 2001. Oakdale is accredited by the 
Western Association of Schools and Colleges. The school serves a diverse student population. 
 
 
 
Oakdale grades K-5 are housed at an online school, Oak Bridge Academy, located in the Chico Mall. Grades 6-12 are located on the 
High School campuses. Oakdale students meet the same requirements for grade promotion and graduation as the rest of the students 
in the Chico Unified School District. Oak Bridge Academy is our online instruction option for students, which opened August, 2018. 
 
 
 
Mission - Provide all students with differentiated instruction through a variety of high quality activities and lessons designed to produce 
competent, engaged citizens. 
 
 
 
Vision - Oakdale Secondary will be a thriving, dynamic and inspiring environment that will enable students to flourish as respectful, 
responsible and resilient citizens. 
 
 
 
Credit System - Credit in independent study is awarded according to the amount and quality of work completed for each assignment. 
Weekly homework approximates the number of hours students would otherwise be attending a comprehensive school setting. 
 
 
 
Oakdale School motto reflects the 3Rs: Respect for self and others, Responsibility for one's actions and Resiliency, the ability to bounce 
back. 
 
 
 
 
 
 
 

2017-18 School Accountability Report Card for Oakdale School 

Page 2 of 12