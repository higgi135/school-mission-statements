The mission of Marsh Elementary School is to provide engaging academic and collaborative learning activities that are guided, 
collaborative, and rigorous for our students. This will lead to independent and interactive learning experiences. These best practices 
will be designed to promote student inquiry and discovery about the world in which they live. Marsh Elementary School is the oldest 
elementary school in Antioch, California. It was dedicated in 1948. Marsh is located in central Antioch, surrounded by single dwelling 
homes, condominiums, and apartments. Marsh serves approximately 610 students. Marsh is a Title I site and receives federal funding. 
CBEDs data reflects that our student population is culturally diverse: 56.1% Hispanic, 26.3% African American, 6.4% White, 0.8% Asian, 
3.5% Pacific Islander, 0.3% American Indian or Alaska Native, 2.2% Filipino, and 4.3% Two or more races. Approximately 43% of 
students are English learners; 9.1% of the English learners have been redesignated as Fluent English Proficient. English learners have 
the following primary languages: Spanish, Tongan, Filipino, Vietnamese, Burmese, French, and Urdu.