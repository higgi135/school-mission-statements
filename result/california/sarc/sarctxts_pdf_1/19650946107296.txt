Welcome to Edgewood Middle School... 
 
EMS has been serving our community as a middle school since 1988. We are committed to serving the needs of our middle school 
students and using the District and State standards as a basis for our teaching. We strive every day to create a challenging curriculum 
that addresses the developmental, academic, social and emotional needs of our diverse student population. We encourage you to 
visit our school, our school website (edgewoodib.wcusd.org) and our classes to see the positive environment EMS provides for your 
child. We are looking forward to an exciting year and welcome you to be part of our learning community. Your involvement does make 
a difference! 
 
There are 183 days in our school year. School begins the third week in August and ends the first week of June. The school day starts 
at 8:00 a.m. and ends at 2:59 p.m. The Administrative Office opens at 7:30 a.m. and closes at 4:00 p.m. daily. As of August 2018, there 
are approximately 560 students enrolled at EMS, 7% of whom are English Learners and 17% who are GATE. 
 
Mission Statement: 
 
Edgewood Middle School is a Middle Years Programme IB school, committed to building a globally aware community of lifelong 
learners who achieve high academic standards. 
 
Edgewood provides a diverse, challenging curriculum that is student-centered and develops inquisitive, knowledgeable, and 
empathetic students who actively engage in and contribute to their family, community, and the world around them. 
 
Vision Statement 
 
As a school, we respond to the needs of our entire learning community. Students, parents, school personnel, and the community are 
empowered inclusively to make important educational decisions. We take responsibility for the implementation and outcomes of 
those decisions. Building on our strengths, the Edgewood Middle School community forms partnerships and collaborates to develop 
effective educational programs, resulting in improved student learning and achievement. 
 
Major Focuses: 

• We have been authorized as International Baccalaureate (IB) Middle Years Programme (MYP) School. 
• 

Teacher collaboration is a major catalyst for school-wide academic improvement. We have implemented the Professional 
Learning Communities (PLC) to allow teachers to collaborate on lessons, build common assessments, analyze data, and 
develop next steps. 

• We have intensive reading and mathematics instruction embedded in the school day for all students. 
• We have targeted intervention programs in place to improve student achievement in all academic subjects and to prepare 

for CAASPP testing. 

• Our parent involvement continues to increase. 

 

 

2017-18 School Accountability Report Card for Edgewood Middle School 

Page 2 of 14 

 

Focus on Continuous Improvement in 2018-2019: 
 
We are dedicated to accurately assessing our strengths and challenges so that student learning can be optimized. To achieve this goal 
we continue to refine the instructional program in the following ways: 

Increased focus on MYP Inquiry-Based and Conceptual-Based Instruction 

• 
• Continue the implementation of International Baccalaureate Middle Years Programme units, philosophies, and 

• 
• 
• 

performance tasks 
Implementation of write across all content areas 
Increased focus on higher level thinking skills and depth of knowledge in lesson planning and instructional delivery 
Increased incorporation of technology to enhance instructional delivery and opportunities for students to utilize 
technological resources during the instructional day 
Increased focus on IB Approaches to Learning and Teaching skills 
• 
• Continued implementation of Thinking Maps in all content areas 
• Continued implementation of Pictorial Math/Multiple Representations instructional strategies in Math courses 
• Continued development of attendance, behavioral, and academic incentives 
• Continued focus on test-taking strategies in core content areas 

Systems, Structures, and Supports to Foster Continuous Improvement Throughout 2018-19: 

• Monthly department chair, department, and staff meetings to hone teacher instructional efforts by focusing on research-

based strategies and student data 

• Weekly early release collaboration meetings to provide teachers the opportunity to plan lessons, share instructional 

strategies, and analyze student achievement data in order to increase content mastery. 

• Weekly IB MYP to develop and support the creation of the IB unit plans 
• Weekly Professional Learning Communities (PLC) to support student learning through the use of data 
• Maintenance of a "Leadership Team" comprised of teacher leaders who frequently collaborate with their departments on 

matters of instruction, budget, students, and parents. 

• Providing student support through our counseling team which has been recognized by the State for their commitment to 
improving student services at EMS. Counselors meet with all students and their parents to discuss matriculation through 
EMS, their transition into high school and their college/career plans. 

Students in need of specialized instruction are mainstreamed as much as possible into the general education setting per their IEP 
goals. The Special Education teachers work collaboratively with the General Education teachers to plan instructional opportunities 
aligned with the core curriculum. Modifications are made to make the core curriculum accessible to students. In addition, as 
appropriate, our Special Education students attend an Extended School Year (ESY) program in the summer to help maintain the 
knowledge learned during the regular school year. The WCUSD Foster Liaison and Academic counselor (Site Foster Youth Liaison) host 
a welcome meeting and develop an academic plan for foster youth enrolled in our school. They meet with foster youth to ensure they 
have all the necessary resources to be successful at Edgewood. Moreover, they monitor their academic progress and recommends for 
intervention as needed to help address any achievement gaps.