Our students will learn in a positive, personalized setting that promotes academic, social, emotional and personal success. 
 
This school serves as an alternative setting for students who have been expelled, truant and/or are on probation. This is a small school 
setting with a very transient population. The goal is for students to earn their way back to district schools. Students do not tend to 
stay at this school longer than one school year, and many are only here for one semester. Our purpose is to help students overcome 
obstacles and re-establish a connection to school. Two common obstacles are non-compliance with school rules which results in 
suspension/expulsion, and not attending school which results in school failure and dropping out of school. 
 
In order to accomplish our purpose, we maintain a small staff:student ratio and employ a student-centered, semi-individualized 
approach, providing academic instruction and support, as well as social-emotional counseling services.