AT LRES our Mission Is learning and growing. 
 
Our students enter to learn and leave ready to achieve. 
 
It is the mission of Ladera Ranch Elementary School to meet the individual needs of each student and to provide students with 
opportunities that promote growth so that they can reach their greatest potential - academically and socially - by embodying the 
principles of Respect, Responsibility, and Readiness. 
 
We believe that students will meet their greatest potential if we function as a Professional Learning Community. We envision a school 
in which staff: 

 

• unites to form a clear vision and goals 
• works together collaboratively 
• 
• works as a team to monitor student progress 
• demonstrates a commitment to doing what is best for each child academically and socially 

continually studies and implements best practices 

Ladera Ranch Elementary is located at 29551 Sienna Parkway in Ladera Ranch, California: and opened on August 27, 2003, serving 
students from the community of Ladera Ranch. It is the goal of the Ladera Ranch School to create a community of learners in which 
teachers, students, and parents work as a team to create a positive and safe environment so that all students learn. 
 
The Ladera Ranch School campus is a beautiful state-of-the-art facility. In a first for Orange County, Ladera Ranch School entered into 
a partnership with the Orange County Public Library, and the campus houses the city’s branch of the library. 
 
Ladera Ranch School is committed to supporting students in all curricular areas with the tools necessary to be successful. Our District 
has provided an opportunity for a Learning with Laptops program in which all fourth and fifth grade students are provided with a 
Chrome book. With the support of our PTA first - third grade students are also provided with a Chrome book for the grade level. 
 
For additional information about school and district programs, please visit www.capousd.org