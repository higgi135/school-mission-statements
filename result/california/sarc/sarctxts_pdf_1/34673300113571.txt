The commitment of Vista del Lago High School to its students and community is best represented by our mission statement, written 
by Vista’s staff and administrators: 
 
Mission: Vista del Lago High School is committed to providing a positive and relevant learning environment where every student is 
prepared for 21st century college and career success. 
 
Vision: Vista del Lago High School is a Professional Learning Community dedicated to ensuring that each student gains the knowledge 
and skills necessary to demonstrate outstanding academic and personal achievement. In partnership with parents and community, 
we are dedicated to developing engaged and ethical young adults who approach learning with courage, compassion and resilience in 
a diverse and changing global community. 
 
Vista del Lago High School is one of two comprehensive high schools in the city of Folsom. Our city is situated in a prime location at 
the foot of the Sierra Nevada Mountains, approximately 30 miles east of California’s state capitol. Settled during the California Gold 
Rush, Folsom has a rich and colorful history steeped in ranching and gold mining. Our student enrollment, reported on the California 
Basic Educational Data System (CBEDS) in October 2017 was 1,800. 
 
Folsom Cordova Unified School District is comprised of two communities located in Sacramento County along US Highway 50. With a 
population of approximately 20,500 students, schools are an average of twenty miles from downtown Sacramento and just over 100 
miles from San Francisco. Folsom Cordova Unified School District enrolls Preschool through Adult. There are twenty elementary 
schools, one charter elementary school, four middle schools, three comprehensive senior high schools, three alternative high schools, 
and an adult school.