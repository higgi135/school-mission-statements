Cirby Elementary School opened in 1954 and is one of nineteen schools in Roseville City School District. 
 
The Cirby School Mission Statement is: Cirby School exists to educate, encourage, support, and inspire all children to become 
responsible, confident, and independent life-long learners and leaders. 
 
Cirby School's vision is one in which the staff, students, parents and community will collaborate to provide a learning community of 
professional learners that: 
 
1. Focuses on high quality instruction with on-going reflection on student achievement. 
2. Modifies instruction and uses research-based interventions based on student needs determined through the use of continuous 
progress monitoring of student achievement. 
3. Maximizes the education of each student by teaching the essential standards in preparation for students to be college and career 
ready.