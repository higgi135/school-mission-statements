Principal's Message 
I'd like to welcome you to Bridgeport Elementary School's Annual School Accountability Report Card. In accordance with Proposition 
98, every school in California is required to issue an annual School Accountability Report Card that fulfills state and federal disclosure 
requirements.Parents will find valuable information about our academic achievement, professional staff, curricular programs, 
instructional materials, safety procedures, classroom environment, and condition of facilities. 
 
Bridgeport Elementary School provides a warm, stimulating environment where students are actively involved in learning academics 
as well as positive values. Students received a standards-based, challenging curriculum by dedicated professional staff and based on 
the individual needs of the students. Ongoing evaluation of student progress and achievement helps us refine the instructional 
program so students can achieve academic proficiency. We have made a commitment to provide the best educational program 
possible for Bridgeport Elementary School's students, and welcome any suggestions or questions you may have about the information 
contained in this report or about the school. Together, through our hard work, our students will be challenged to reach their maximum 
potential. 
 
Mission Statement 
A community ascending from varied pasts toward future promise, is to ensure that each student achieves academic and personal 
excellence, through a unique educational system distinguished by student centered learning environments with no boundaries, 
devoted and passionate staff, engaged partnerships within our communities, technology that bridges the gaps with all communities 
and the world and innovative risk in a secure environment. 
 
School Profile 
Bridgeport Elementary School is located in the central region of Bridgeport and serves students in grades TK through eight following 
a traditional calendar. At the beginning of the 2018-19 school year, 59 students were enrolled, including 20% in special education, 8% 
qualifying for English Language Learner support, and 58% qualifying for free or reduced price lunch.