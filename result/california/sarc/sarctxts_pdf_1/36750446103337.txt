PRINCIPAL'S MESSAGE 
I'd like to welcome you to Maple Elementary School's Annual Accountability Report Card (SARC). In accordance with Proposition 98, 
every school in California is required to issue an annual SARC in order to fulfill state and federal disclosure requirements. Parents will 
find valuable information about our academic achievement, professional staff, curricular programs, instructional materials, safety 
procedures, classroom environment, and condition of facilities. 
 
Vision Statement: 
The vision of Maple Elementary School is to produce a solid foundation, academically and socially, for students so they can be 
successful, productive, and contributing members in a democratic society. 
 
Mission Statement: 
The mission of Maple Elementary School is to produce innovative learners who thrive in a collaborative environment. 
 
SCHOOL PROFILE 
Hesperia Unified School District is located in the high desert region of San Bernardino County, approximately 40 miles north of the 
Ontario/San Bernardino valley. More than 20,000 students in grades kindergarten through twelve receive dedicated and highly 
qualified professionals. The district is comprised of 15 elementary schools which includes 3 choice schools. At the secondary level 
Hesperia has 3 middle schools and 3 high schools. 
 
Maple Elementary is located in the northwest area of Hesperia and serves students in grades transitional kindergarten through six. 
During the 2018-19 school year, 873 students were enrolled, including 6% in special education, 27% qualifying for English learner 
support, and 100% qualifying for free or reduced-priced lunch.