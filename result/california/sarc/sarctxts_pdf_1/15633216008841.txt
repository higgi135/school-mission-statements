The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters. As you read this 
Report Card, you will gain a better understanding of Chipman as a school with a record for improvement, a faculty that is professionally 
skilled and personally committed to meeting the learning needs of students and a student body which is enthusiastic and motivated 
to perform well. 
 
It is the mission of Chipman Junior High School to provide a safe, rigorous, and nurturing learning environment that will ensure all 
students are equipped to become productive citizens in the twenty-first century.