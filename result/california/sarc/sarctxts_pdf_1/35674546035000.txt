Bitterwater-Tully Union School District consists of one TK-8 Elementary School. It is located 15 miles east of King City and 43 miles 
south of Hollister. Bitterwater-Tully School is one of the only schools in the extreme southern reaches of San Benito County and is a 
center for many community activities. The school has two classrooms and a developing athletic program that competes with other 
local schools. 
 
In 2018-2019, two fully credentialed teachers provided instruction to Bitterwater-Tully students. One teacher taught grades TK-3; the 
other taught grades 4-8. The primary grade teacher also served as the principal. The teachers regularly attend professional 
development training to increase their content knowledge and to keep current on educational research and effective instructional 
practices. 
 
Both the TK-3rd and 4th-8th grade classrooms have extensive classroom libraries and computers with internet access. Every month, 
the Bookmobile comes from the County Library with library books/media for the students/families to check out. 
 
Health screenings, Special Education, and other Student Support Services are provided by the San Benito County Office of Education. 
 
Mission Statement: 
The mission of the Bitterwater-Tully Union Elementary School District, in partnership with home and community, is to challenge every 
student to learn the skills, acquire the knowledge, and develop the insight and character necessary for a productive and rewarding life 
through a quality instructional program, a positive, safe, stimulating "small school" environment, with a clear commitment to the 
worth of every individual.