John F. Kennedy Elementary School is one of eight elementary schools in the Newark Unified School District. Class size in kindergarten 
through third grade will be no larger than twenty-six students and no more than thirty-one students per classroom in grades four 
through six. All of the classroom teachers, and the principal are NCLB compliant and CLAD or equivalent certified. 
 
The school provides California Common Core Standards-based instruction in all the academic areas. State and federal accountability 
reports demonstrate the continued academic progress and success; Kennedy is not under any state or federal sanction. 
 
Kennedy School celebrates our diverse ethnic population including: African-American, Asian, Hispanic, and White students. Seventeen 
languages are represented. The school provides differentiated instruction for English Learners, Students with Disabilities, and students 
working above grade level. 
 
The City of Newark is located in Alameda County, and is home to 42,573 residents. This 150 year-old community is situated on the 
southeastern edge of the San Francisco Bay directly east of Interstate I-880 and south of Highway 84. With its close proximity to San 
Jose, Oakland, and San Francisco, Newark residents have easy access to the benefits of “big city” life, while offering the comfort and 
safety of a small town atmosphere. Newark Unified School District is comprised of eight elementary schools, serving students from 
transitional kindergarten through sixth grade, one pre-school, one junior high serving grades seven and eight, one comprehensive 
high school, and an alternative program. All of the schools maintain a shared commitment to challenge the students to strive for 
excellence. Our schools support students to become academically skilled and community-minded with a wide array of post-high school 
educational and career options. 
 
School Mission Statement: 
It is the mission of Kennedy Elementary School to ensure high levels of learning for all students and to foster a community built on 
respect for one another.