Mission 
Andrews School's mission is to empower students through knowledge and learning to become productive, confident, and impactful 
leaders in the 21st Century. 
 
Vision 
Andrews vision is based in the belief that every student is respected as an individual and taught to value independence in learning, 
responsible use of technology, and work towards preparing to participate in a global society and workforce. It is our goal to be 
progressive and well-informed educators that can then provide students with a well-rounded, rigorous curriculum that allows them 
to experience multiple perspectives, develop critical thinking skills, collaborate in learning, and maintain social-emotional well-being. 
Students leave Andrews with the skills and confidence to become impactful leaders in the 21st century. 
 
Located in the North Whittier community, Andrews School serves approximately 639 students in Kindergarten through 8th grade and 
includes three Moderate-Severe Special Day Classes. Additionally, Andrews is identified as a Title I Program school. Approximately 
62% of students at Andrews qualify for free or reduced lunch, and approximately 9% of students are identified as English Learners. 
Andrews has a dedicated staff made up of highly qualified new and veteran teachers. Our school continues to partner with education 
consultants to strengthen the implementation of the Balanced Literacy approach in Reading and Writing. Andrews also continues to 
partner with the Cotsen Foundation as an alumni school. Through Cotsen, Teacher Fellows participate in high quality professional 
development in Reading and Writing, Mathematics, and Technology instruction and have opportunities to collaborate with educators 
across the region. All students have access to Common Core aligned instructional materials as well as access to a 1:1 iPad program 
and technology. 
 
Andrews has fully transitioned into a K-8 learning academy. Our 6th-8th Grade students are offered a well-rounded and rigorous 
academic program to prepare them for the high school transition. Included in the instructional day are learning opportunities that 
include Accelerated Math, separate period classes dedicated to Reading and Writing, and elective courses that include Science 
Olympiad, Chorus, Guitar, and Art. Students with diverse learning needs are provided reading interventions, English Language 
Development, and Special Education within the school day, and an after-school intervention program is offered throughout the school 
year. Additionally, Andrews implements units of study within the narrative, informational, and argumentative/opinion writing genres. 
The Writer's Workshop approach is utilized to deepened our knowledge of effective teaching practices within writing. Andrews 
continues to implement the Common Core State Standards in Mathematics through the Eureka Math curriculum in grades K-5 and 
CPM (College Preparatory Mathematics) curriculum in grades 6-8. 
 
Our school motto, "Learn Today, Lead Tomorrow" captures what we envision for our students' futures. We believe it is our 
responsibility to prepare children for the 21st century and for productivity in school and life. At Andrews, we provide children with 
the strong academic foundation to ensure future success. Regardless of curriculum mandates, high stakes testing or changing 
standards, teachers at Andrews have stayed true to the belief that we truly shape the futures of our students and have made 
instructional decisions that support high academic achievement in all students. Andrews School was named a 2014 National Blue 
Ribbon School, a 2014 California Distinguished School, and a 2014 California Title I Academic Achievement School. Andrews continues 
to grow and succeed as a result of the dedication and perseverance of our students, families, and staff. 
 
 

2017-18 School Accountability Report Card for Andrews School 

Page 2 of 12