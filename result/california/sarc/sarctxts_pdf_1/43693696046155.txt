Meyer's Vision is to provide an environment that provides students opportunities to be creative, collaborative, and confident 
individuals with the competencies that will enable him or her to thrive in a diverse and competitive world. 
 
Core Values: 

• Highly qualified veteran teachers 
• High Expectations 
• Generational community school 
• Caring and supportive environment 
• 

Safe place 

The staff at Meyer Elementary School is qualified, dedicated, knowledgeable, enthusiastic, and available for the students. Meyer offers 
personal growth to everyone. It is a good place to learn, to work, and to be. It is a place where everyone can feel secure in his or her 
mind, body, and surrounding environment. 
 
As principal of Meyer Elementary School, my goal is to continue to provide support and instructional leadership to the Meyer 
community as we continue our growth in the development of a high-quality educational enterprise that prepares all of our students 
for life in a fast-changing and complex world; for our students are the most important people in our school.