Sequoia High School, Home of the Trailblazers, is a community day high school for at-risk freshmen and sophomores. "Changing 
Behaviors, Changing Students" is the school's vision. We focus on preparing students to make positive choices in all aspects of their 
lives. We have various programs to connect students to school and support their efforts to improve. We offer intramural athletics; 
ASB Leadership, hands-on electives such as horticulture, cooking, art, driver's ed, and journalism. There are various counseling 
supports available to all students and additional district-level supports for our foster youth and McKinney-Vento students. Sequoia 
provides frequent extended learning opportunities in which students have experienced hiking National Parks, the art of glass-blowing, 
fishing and water conservation, visits to the Monterey Bay Aquarium and live performances at the theater in Merced. 
 
Credit recovery is the primary goal for students attending Sequoia. The school operates on a block schedule, affording students the 
opportunity to earn up to 90 credits in a school year, as opposed to 70 at a comprehensive school site. Adding CTE courses in the 
upcoming years is a major goal. The school's mission statement represents our overall purpose: "To instill the will and skill, in at-risk 
teens, to succeed in school and in life." We currently serve students from 6 comprehensive school sites spanning the communities of 
Livingston, Atwater, and Merced. Sequoia is often used as a transition school for students entering the district from Juvenile Hall. 
Sequoia High School completed its last full WASC review in 2013-14 with a mid-cycle review in 16-17. 
 
We use Parent Square as a means of communicating with parents regarding upcoming events and emergency announcements. If you 
would like to access our web page, you will find it at http://shs.muhsd.org/.