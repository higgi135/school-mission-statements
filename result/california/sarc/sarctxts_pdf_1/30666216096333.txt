The purpose of the School Accountability Report Card is to provide parents with information about the school’s instructional programs, 
academic achievement, materials and facilities, and the staff. Information about Orange Unified School District is also provided. 
 
Imperial is located in a residential area in North Orange County within the city of Anaheim, and maintains modified traditional calendar. 
Approximately 485 students were enrolled in grades transitional kindergarten through six during the 2018-2019 school year. Imperial 
is one of 27 elementary schools in the Orange Unified School District. Since opening in 1976 with the open classroom concept, some 
walls have been added however enough openness remains to allow for convenient teaming and inter-class groupings. Imperial 
celebrates 43 years of serving the community. 
 
At Imperial we are dedicated to providing a high quality education for all children. We foster creative, confident students who are able 
to think critically and communicate through collaborative learning, enabling them to become productive members of the global 
community, leaving a lasting imprint on our society. 
 
Our main purpose at Imperial is to ensure a safe and challenging learning environment for the care, development and maximum 
learning of every student. A comprehensive educational program is in place, using state and district curricular standards, to meet the 
needs of all students. Staff members work together to choose the most effective instructional strategies and methods providing 
multiple opportunities for every child to succeed. Community and parent partnerships are integral in Imperial's ability to further 
support student citizenship and character development, student interest and engagement, and opportunities for extended learning.