Creekside Elementary School is part of the award winning Poway Unified School District. Creekside has the distinction of being the 
21st elementary school in the district and the first school to open in the 21st century. Creekside was named a California Distinguished 
School in 2014 and a National Blue Ribbon School in 2016. Creekside was built with a vision for the future and preparing our students 
to be productive citizens and experience successful careers. The physical structure of Creekside is unique. It consists of 30 permanent 
classrooms organized into five villages in addition to one temporary classroom. Each of the villages contains six teaching stations or 
classrooms, student restrooms, a staff restroom, as well as a workroom and a large village "square" or common area. The common 
area is used for individualized instruction, groups to collaborate, space for technology, and parent volunteers to work. The main 
building houses the administrative office, a full size multi-purpose room, library, and the RSP special education program. The 
before/after school care and the preschool are also located in the center of the campus adjacent to the cafeteria and lunch area. 
 
The staff of Creekside represents a group of outstanding individuals who are well educated and trained to maximize equity and access 
for each child. Staff, parents, and students come together to create a positive learning environment that provides numerous 
opportunities for all children to experience rigor, relevancy, and relationship building. With the support of our PTA and Creekside 
Educational Foundation, we are able to provide our students with enrichment opportunities in technology, math, art, and music. The 
support of parents and the numerous volunteers on campus each day contribute to the success of our students. 
 
Mission Statement: Creekside Elementary School is a community of lifelong learners committed to high academic achievement and 
respect for all in an environment that fosters responsibility, collaboration, communication, and celebrations. Vision: Every student. 
Every day.