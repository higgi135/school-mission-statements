SCHOOL MISSION 
The Etiwanda Intermediate staff, with the support of the community, is dedicated to a student-centered, diverse program of academic 
excellence. The students, staff, and community are working together in a partnership to develop capable, responsible, and productive 
members of society. 
 
SCHOOL VISION 
Etiwanda Intermediate’s vision is based around building meaningful relationships with students and families, delivering rigorous 
curriculum tailored so all students can be successful, and using relevant resources to stimulate multiple learning styles. With the 
academic, social, and emotional development at the forefront, all students will feel supported and confident to continue their passion 
for learning. Etiwanda Intermediate is the Emerald of Etiwanda where students and teachers want to come to school. 
 
DISTRICT & SCHOOL PROFILE 
Etiwanda School District serves over 14,000 TK-8 students residing in the cities of Rancho Cucamonga, Fontana, Alta Loma, and 
Etiwanda. The district currently operates thirteen TK-5 elementary schools and four intermediate schools (grades 6-8) and a 
Community Day School. Etiwanda’s graduating eighth-grade students are served by Chaffey Joint Union High School District for grades 
9-12. Homeschooling program, preschool program, and childcare are provided at some schools within the district. More information 
is available on the district website or by contacting the district office at (909) 899-2451. 
 
The district’s commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high-quality 
educational program. Etiwanda School District appreciates the outstanding reputation it has achieved in local and neighboring 
communities. Consistent success in meeting student performance goals is directly attributed to the district’s energetic teaching staff 
and strong parent and community support. 
 
Etiwanda Intermediate is sensitive to the developmental needs of students at this transitional time before entering high school. The 
school educates students in grades six through eight. Great emphasis is placed on character-building as well as on education. Since it 
opened its doors over 100 years ago, the school has been committed to providing students the necessary skills to be productive in the 
present and in the future. 
 
The staff at Etiwanda Intermediate School believes each child is unique and deserving of a rich educational environment. Students 
have access to rigorous core curriculum in language arts, mathematics, science, and social studies. Etiwanda Intermediate is fortunate 
to have a staff of experienced and knowledgeable teachers eager to make a difference for students. The school is built on beliefs that 
a "student-centered" approach provides an atmosphere in which a child’s social, emotional, and intellectual needs are equally 
important. All students have special talents and are given the opportunities to develop those talents. During the 2018-2019 school 
year, Etiwanda Intermediate School serviced over 1,400 students.