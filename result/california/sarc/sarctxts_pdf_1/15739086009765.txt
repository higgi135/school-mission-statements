Vision 
Kern Avenue strives to provide innovative educational programs through a variety of approaches that enrich children’s lives socially, 
emotionally, and academically. 
 
Mission 
The mission of Kern Avenue Elementary School is to work together to create the next generation of technologically skilled leaders, to 
instill a love of lifelong learning, and encourage perseverance while providing a safe learning environment that promotes 21st Century 
skills. 
 
Our mission can be accomplished through: 

 

Integrate and coordinate student services and provide standards based materials for grade level instruction. 
Instruct to district expectations and to state standards and challenges. 

• Grade-level performance of all students in reading, writing, and math at their instructional level. 
• 
• 
• Provide meaningful and regular opportunities for parents and community members to become partners in education. 
• 

Ensure access to core curriculum by budgeting for materials, by providing staff development, and by monitoring 
instruction. 
Improve accountability by gathering data of subgroup populations and by writing specific improvement plans for those 
subgroups. 

• 

• Develop student pride in achievement, responsibility, ethnicity, culture, heritage, and primary language proficiency of self 

and of others. 
Lead proactively and with high expectations of staff, of student achievement, and of parent involvement. 
Ensure all students will be educated in learning environments that are safe, drug free and conducive to learning. 

• 
• 
• All Limited English proficient students will become proficient in English and reach high academic standards, at a minimum 

attaining proficiency or better in reading language arts and mathematics. 

Kern Avenue is a TK-5 elementary school. It has a standard academic calendar with a 3 week break during the winter. As of October 
2017 Kern Avenue's enrollment is 690 students. Kern Avenue was built in 1936 and is located 28 miles North of Bakersfield. 
 
The school has a total of 34 certificated staff, including a Principal, Learning Director, 1 mild to moderate special education teachers, 
1 moderate to severe education teachers, a speech pathologist, psychologist, and a Counselor. Our school district employs an ELD 
Coach and Technology Support Team that provide service to all district school sites. A school library clerk and 5 special education 
instructional aides provide direct instructional support for our students. An additional 5 safety aides help to maintain supervision of 
our students outside of the classroom. 
 
Kern Avenue students are primarily of Hispanic descent and make up the largest sub-group of students, accounting for 
approximately 98% of the total student population. Approximately 67% of the Hispanic sub-group are identified as EL learners. 
Kern Avenue is a school-wide Title I school. Approximately 100% of the students have been identified as socio-economically 
disadvantaged and the ethnic composition of the school staff mirrors that of the student population with the majority of 
the staff being of Hispanic descent. 
 
 

2017-18 School Accountability Report Card for Kern Avenue Elementary School 

Page 2 of 10