The geographic and symbolic heart of Sebastopol, CA, Analy High School is located on the beautiful Laguna de Santa Rosa and gateway 
to the coast, about an hour north of San Francisco. One of two comprehensive high schools in the West Sonoma County Union High 
School District, Analy is located in a town of approximately 8,000 people which spans through small farms; open land; vineyards, and 
dairy land. Many Analy parents choose to raise their families in the rural community while commuting to their jobs in the Bay Area. 
A progressive, diverse community, Analy embraces a tradition of academic excellence in a safe and positive learning environment. 
The Analy High School community and alumni are proud to lend their unwavering support to over 100 years of excellence. In a desire 
to seek the best education opportunities available for their students, 25% of our student population attend Analy on inter district 
transfers. In addition to community support, Analy High School proudly recognizes our in house, parent support groups which include: 
Band Wagon, Ag Boosters, Education Foundation, Athletic Boosters and ELAC. Analy High School offers a wide variety of elective 
programs based on student interest. 45% of Analy High School graduates choose to continue to higher education at a four year college 
or university while others choose to attend Santa Rosa Junior College and transfer to the college or university of their choice from the 
Junior College. A quarter of Analy students participate in the Advanced Placement Program with an overall pass rate of over 72%; 
many of these students take Honors Courses in preparation for the rigorous, college level, Advanced Placement Courses. Recognizing 
that many of our students have career interests beyond the classroom, Analy High School is in the process of creating CTE Pathways 
to address academic and career readiness requirements at graduation. Administered to all 11th grade students, Analy High School 
students represent some of the best pass rates in the County for the CAASPP (California Assessment of Student Progress and 
Performance) in English and Math; 74.4% of all juniors met or exceeded standards in English and 46.4% of juniors met or exceeded 
standards in Math during the 2017-2018 testing cycle. Our "Expected Student Learner Outcomes" (SLO's) - "Cooperation, Thinking 
and Problem Solving, Communication, Technology, Career Preparation, and Diversity/Social Personal Responsibility" are born out of a 
desire to create a well rounded student and lifelong learner.