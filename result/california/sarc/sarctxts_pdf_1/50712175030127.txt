Del Puerto High School is the alternative school for the Patterson Unified School District. It serves students 16 years of age and older 
who have not been successful in a “traditional” high school setting. At Del Puerto High School, every student matters. We believe that 
each student is unique and each student can succeed. Through a process of discussion, testing, and other activities, DPHS is able to 
individualize the learning process and develop an “Individualized Learning Plan” for each student. We test and re-evaluate 
continuously to insure student understanding and achievement. 
 
Del Puerto High School annually reviews the Expected Schoolwide Learning Results (ESLRs) and identifies ways to incorporate them 
into the school culture and environment. Each year discussions are held on what ESLRs mean, and how can we increase awareness of 
them amongst our students, parents, and stakeholders. In previous years, the ESLRs were included in the group parent orientation 
sessions which were held prior to the start of the school year. The group orientation sessions were created to improve efficiency of 
our enrollment process at the start of the school year and was used not only to complete the enrollment process, but also to provide 
parents with information on expectations at Del Puerto High School, ways to participate in their child’s educational experiences and 
the ESLRs. Del Puerto High School's ESLR's are: 
 
Read and Communicate Effectively 
 
Make positive growth in the areas of reading, listening, speaking and writing Comprehend and apply information 
 
Demonstrate competency in the use of a variety of technological devices 
 
Set Realistic Goals 
 
Identify and construct academic career goals 
 
Work Cooperatively 
 
Contribute and function in various group roles (classroom, school wide, community wide) Exhibit socially acceptable behavior 
 
Participate in the Community 
 
Contribute time, energy, and talents to improve the quality of life for themselves, the school and the community 
 
District Vision Statement 
 
Ensure excellence in education and cultivate healthy, contributing citizens! 

a safe learning environment 

• Board of Education Commitments life-long learning 
• 
• developing responsible and accountable students communication 
• healthy behaviors 
• motivating students to maximize their potential 

 

2017-18 School Accountability Report Card for Del Puerto High School 

Page 2 of 11