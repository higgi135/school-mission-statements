Shasta Lake School offers an educational setting where each student’s academic, emotional, social, and developmental needs are 
addressed in a supportive environment. Our teachers set high academic and behavior expectations and provide a challenging 
curriculum for all students. Our educational family, which includes teachers, counselors, paraprofessionals, secretarial staff, cooks, 
custodians, the assistant principal, and the principal, works in partnership with parents and community members to provide an 
exceptional learning experience for all students. 
 
Shasta Lake School has a rigorous curriculum that is designed to address all of the California Content Standards through daily 
instruction in mathematics, language arts, history/social studies, science, physical education, reading, and elective/enrichment 
courses. We pride ourselves on offering a comprehensive curriculum to students of all academic levels. Through a comprehensive 
assessment process using regularly scheduled formative assessments, teachers track student progress carefully in order to re-teach 
any missing skills. Shasta Lake School continues to work in Professional Learning Communities to implement a comprehensive 
Response to Intervention support program. Shasta Lake School has been utilizing a small group teaching approach to providing 
reading instruction in kindergarten through third grade and groups students according to ability. Students receive intensive reading 
instruction for one hour daily at their own readiness levels. 
 
At Shasta Lake School, we have made a commitment to provide the best educational program possible for our students. We are 
dedicated to ensuring that Shasta Lake School is a welcoming, stimulating environment where students are actively involved in learning 
academics as well as positive values. 
 
Shasta Lake School has made continual improvements in our programs by keeping our instructional materials up to date and replacing 
technology on a regular basis. Shasta Lake School has made a commitment to providing our students with the technology base they 
will require to be college and career ready. Every classroom on our campus is equipped with an inter-active white board (Promethean 
Board). We provide a rich learning environment with technology to enhance learning. Every first through eighth grade student has 
access in their classrooms to Chromebooks (1:1) and our kindergarten students have Chromebooks for small group instruction. Our 
facilities are very well maintained with a $6.4 million gymnasium. 
 
Mission Statement 
The Gateway Unified School District, in partnership with parents and community members, will provide a challenging, successful 
education, instilling the virtues of good citizenship and lifelong learning for all students in a safe and caring environment. 
 
 

2017-18 School Accountability Report Card for Shasta Lake School 

Page 2 of 10