Whitmore Charter High School is located in the city of Ceres in the central San Joaquin Valley, 80 miles south of Sacramento and 95 
miles east of San Francisco, in the heart of Stanislaus County. 
 
Whitmore Charter High School first opened in 2002 with 5 students and currently has an enrollment of approximately 165 students. 
Currently there are 14 full and part time teachers, 1 principal, 1 assistant principal, 1 full time learning director, 1 resource specialist 
and 1 school psychologist (shared with other schools in the district). Some of our staff members also share their positions with 
Whitmore Charter School of Arts and Technology, which is also on the same campus. 
 
Whitmore Charter High School serves a broad cross section of students residing throughout Stanislaus and surrounding counties. 
WCHS offers a rigorous college prep program for students in grades 9-12 who desire a customized approach to high school. In 
partnership with advisory teachers, students and parents are encouraged to create a personalized plan designed to meet the unique 
needs of every student. 
 
The mission of Whitmore Charter High School is to provide students in grades 9-12 and their parents the opportunity to create a 
customized educational plan for their high school experience. The student, the parent or guardian, and the Advisory Teacher work in 
partnership to develop a unique learning plan which is based on the student’s interests, abilities, and educational goals. Whitmore 
Charter High School provides students with a wide range of innovative instructional modes from which to choose in designing their 
academic plan. These modes may include home schooling, on-campus classes, online/distance learning, and/or vocational 
apprenticeships. Graduates of the Whitmore Charter High School are prepared to enter the 21st Century as self-motivated, 
independent, critical thinkers and decision makers, who recognize that education is a life-long process, and who are prepared to enter 
their adult lives with vision, passion, and productivity.