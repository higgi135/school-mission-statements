Ohlone Elementary School offers a unique approach to education and is officially designated as an alternative school rather than a 
neighborhood school. Students are admitted to Ohlone via an equal access lottery. Parents are required to attend an orientation 
session and school tour prior to making a commitment to Ohlone’s guiding principles and practices. 
 
Ohlone’s core values form the basis of its educational and community structure. The Core Values are: 
 
Trusting and Respecting Each Individual 
Developmental Approach 
Growth and Assessment 
Meaningful, Relevant Curriculum 
Multi-dimensional Learning 
Cooperation and Collaboration 
Student-Teacher-Parent Partnership 
 
Ohlone students experience a developmental and multi-dimensional approach to teaching and learning. Students engage in authentic 
ways with the curriculum and learn through relevant real-world experiences, simulations, and project-based learning. We encourage 
our students to think independently, aspire to a growth mindset, and to take responsibility for their learning. Students see themselves 
as life-long learners who are comfortable with making mistakes. 
 
Social-emotional learning is at the heart of our instruction at Ohlone. Kindness, respect, compassion, resiliency, and environmental 
and global citizenship are among many of our goals. We value mutual respect, trust, and honesty. In our multiage setting, students 
learn to work with their peers across a developmental continuum that provides opportunities for differentiation in meeting students’ 
individual and developmental needs. Each student is appreciated as an individual with a unique set of gifts. We are an inclusive 
community that welcomes and supports all learners.