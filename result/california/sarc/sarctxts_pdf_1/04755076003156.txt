McKinley Primary School was built in 1937. The main building of McKinley Primary School contains eight classrooms, boys and girls 
restrooms, a staff restroom, a staff room, and various offices. In addition to this main building, there are seven relocatable classrooms 
which house students. In the summer of 2016, McKinley Primary School removed 3 portable classrooms and added 3 state of the art 
portables, including staff restrooms and a restroom attached to our Severely Handicapped classroom. The entire playground blacktop 
was refurbished and new sod was installed in the grass area. 
 
Our mission is to provide a safe, nurturing, child centered environment that ensures a developmentally appropriate education for all 
children. We are committed to a partnership with parents that foster a curriculum, which prepares students to be responsible citizens 
in our changing world.