The teachers, staff and parents of Joe Henderson Elementary School believe their primary objective 
is to the development of well-rounded children with an active interest in learning, intellectual 
exploration, and social involvement. The school's intention is to create an environment where each 
child is encouraged and supported in reaching their highest potential as individuals, while 
interacting, contributing to, and embracing their community of student peers. The school prepares 
children for future challenges in academics and in their social environments. The school places 
great emphasis on developing and modeling exemplary habits of study and intellectual discovery, 
good citizenship and positive character traits supported by our community. As educators, we also 
recognize the value of both practical application of knowledge, and the necessity of monitoring 
progress through testing and other evaluative methods to ensure the challenges of academic 
growth are being met successfully by each individual student. It is through these values and actions, 
that we at Joe Henderson Elementary School intend to shape our community's children into self-
confident, self-knowledgeable young people with promising futures in their academic careers, as 
well as their chosen fields of endeavor outside of school. 

 

 

----

---- 

Benicia Unified School District 

350 East K Street 
Benicia CA, 94510 

(707) 747-8300 

www.beniciaunified.org 

 

District Governing Board 

Gary Wing 

Sheri Zada 

Diane Ferrucci 

Mark Maselli 

Stacy Holguin 

 

District Administration 

Dr. Charles Young 
Superintendent 

Dr. Leslie Beatson 

Assistant Superintendent, Ed. 

Services 

Dr. Khushwinder Gill 

Assistant Superintendent, Human 

Resources 

Tim Rahill 

CBO 

Dr. Carolyn Patton 

Special Services Director 

 

2017-18 School Accountability Report Card for Joe Henderson Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Joe Henderson Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

22.75 22.75 

26 

0 

0 

0 

0 

0 

0 

Benicia Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

215 

3 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.