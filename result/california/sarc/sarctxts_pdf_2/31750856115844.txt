Twin Oaks Elementary School is one of sixteen schools in the Rocklin Unified School District. The 
school serves students in kindergarten through sixth grade, with approximately 565 students. The 
school is located in the center of an established and supportive community with a high level of 
family participation. Twin Oaks opened in 1999. 
 
The staff provides a comprehensive learning environment. Academics and Leadership are the 
primary focus of the school. The staff understands that a caring environment fosters self-worth and 
individual development is essential to realize academic success. The students are encouraged to 
approach all facets of their learning with confidence to take an active role in the learning process. 
As a Leader in Me school, Twin Oaks Elementary School has developed a foundation of Leadership 
based on The 7 Habits of Highly Successful People and the Baldrige Criteria for Performance 
Excellence. 
 
Rules of conduct are simple and consequences are reasonable, related and respectful. The staff 
welcomes the direct involvement of the community in these efforts. Parents and staff are dedicated 
to creating a safe and orderly learning environment in which students interact positively with 
others and are able to develop responsibility, confidence, and self-worth while achieving their 
highest academic potential. 
 
Mission Statement: 
The mission of Twin Oaks Elementary, a Leader in Me Lighthouse School, is to challenge all students 
to develop their unique potential, become life-long learners, and value their community through a 
school distinguished by: 
A culture of personal leadership development 
Engaging and relevant instruction with high academic expectations 
Respectful collaboration and communication 
 
Twin Oaks. Growing tomorrow's leaders today. 
 

2017-18 School Accountability Report Card for Twin Oaks Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Twin Oaks Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

27 

28 

29 

0 

0 

0 

0 

0 

0 

Rocklin Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

590 

5 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Twin Oaks Elementary School 

16-17 

17-18 

18-19