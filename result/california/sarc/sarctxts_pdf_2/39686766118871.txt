At Huerta Elementary, we are guided by our mission statement "to prepare all students to be 
college and career ready through rigorous education. We strive to create problem solvers who are 
independent thinkers and productive members of society." Our vision is to provide a high level of 
instruction by maintaining consistency to the standards in a positive, safe and respectful learning 
environment. 
 
Huerta School strives to assure the healthy development of every child so that each has the 
knowledge, skills and resiliency to reach their full potential and be successful in a rapidly changing 
world. We have been successful in creating a safe school environment for our students. We have a 
strong student support team that believes wholeheartedly in increasing academic achievement by 
incorporating Professional Learning Communities, Positive Behavior Intervention Systems, and 
AVID strategies in the classroom so that all students can reach their full potential. School wide we 
promote SOARing. This includes being Safe, Organized, Attentive, and Respectful in all actions and 
decisions we make. We look forward to working with the parents in our community to to create 
new opportunities and new accomplishments for our students each year. 
 
Ariana Casillas, Principal 
 

 

 

----

---- 

----

---- 

Stockton Unified School District 

701 North Madison St. 

Stockton, CA 95202-1634 

(209) 933-7000 

www.stocktonusd.net 

 

District Governing Board 

Cecilia Mendez 

AngelAnn Flores 

Kathleen Garcia 

Lange Luntao 

Maria Mendez 

Scot McBrian 

Candelaria Vargas 

 

District Administration 

John E. Deasy, Ph.D. 

Superintendent 

Dr. Reyes Gauna 

Assistant Superintendent of 
Educational Support Services 

Craig Wells 

Assistant Superintendent of 

Human Resources 

Sonjhia Lowery 

Assistant Superintendent of 

Educational Services 

 

2017-18 School Accountability Report Card for Dolores Huerta Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Dolores Huerta Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16 

14 

18 

4 

1 

6 

0 

1 

0 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Dolores Huerta Elementary 
School 
Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

16-17 

17-18 

18-19 

1 

1 

0 

2 

2 

0 

0 

0 

1 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.