Taft Montessori Vision 
 
Our vision is to prepare and maintain a safe school environment where all students, parents, and 
staff are able to develop and nurture the curiosity, motivation, flexibility, and enthusiasm needed 
to engage and actively participate in our professional learning community, so every child will 
successfully reach their highest academic potential. 
 
 
 
Taft Montessori Mission Statement 
 
Taft Montessori serves culturally diverse students in a Pre-K through 8th public Montessori 
program, where all students and staff are respected and honored. We educate the "whole child" 
through individualized and differentiated instruction that embraces the Common Core State 
Standards and the Montessori Method of teaching. We educate all students to reach full academic 
potential in preparation to become active and responsible citizens. 
 
 

-

--- 

----

---- 

Stockton Unified School District 

701 North Madison St. 

Stockton, CA 95202-1634 

(209) 933-7000 

www.stocktonusd.net 

 

District Governing Board 

Cecilia Mendez 

AngelAnn Flores 

Kathleen Garcia 

Lange Luntao 

Maria Mendez 

Scot McBrian 

Candelaria Vargas 

 

District Administration 

John E. Deasy, Ph.D. 

Superintendent 

Dr. Reyes Gauna 

Assistant Superintendent of 
Educational Support Services 

Craig Wells 

Assistant Superintendent of 

Human Resources 

Sonjhia Lowery 

Assistant Superintendent of 

Educational Services 

 

2017-18 School Accountability Report Card for Taft Elementary 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Taft Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

18 

15 

19 

4 

0 

7 

0 

6 

2 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Taft Elementary 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

1 

0 

0 

2 

0 

2 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.