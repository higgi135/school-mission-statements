The mission of Gerber Elementary 
School is to team with families to 
create a college-oriented culture 
where students are safe, respected, 
and empowered to contribute in a 
global community. 
 Our vision 
statement is "Inspiring students to 
dream big..." and our motto 
is 
"Work Hard, Dream Big!". 
 
It is our belief that knowledge is 
power and that our students will 
have greater opportunities in life if 
they attend college. We are a proud 
member of 
the No Excuses 
University network that promotes 
college and career readiness and is 
built around six systems that lead to 
student success. The six systems 
are: creating a culture of universal 
achievement, 
collaboration, 
standards alignment, assessment, 
data 
and 
interventions. We portray powerful 
college symbolism 
in classrooms 
and around the school. Examples of 
this 
each 
classroom has adopted a university 
to represent; university flags hang 
proudly in our classrooms and main 
hallway; 
teachers have 
painted their university emblem on 
the outside of their classroom 
doors; and the staff and students 
wear NEU shirts on Mondays, 
college shirts on Wednesdays, and 
school colors on Fridays.