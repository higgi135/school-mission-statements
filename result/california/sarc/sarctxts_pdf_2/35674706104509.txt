The Calaveras School community is located in the city of Hollister on Buena Vista Road, sitting in 
the middle of orchard land on one side and the school neighborhood on the other. Hollister sits in 
northern San Benito County where it is only 47 miles from the San Jose metropolitan area, 39 miles 
east of the Monterey Peninsula, and 90 miles south of San Francisco. The region still retains the 
agricultural and cattle ranching ambiance of its heritage. The campus is shared with the 
Accelerated Achievement Academy, a separate magnet school for gifted and high achieving 4th-
8th grade students in the county. 
 
During the 2017-18 school year Calaveras Elementary School served over 500 students from 
transitional kindergarten through eighth grade. Demographics report that 63% of the students are 
English language learners and that 85% of the students come from low income families. 
 
Our mission statement became a living document where the staff dedicated itself to "enlighten, 
engage and educate all learners in a safe and nurturing environment so they will be successful 
thinkers and contributors to society. This will be accomplished through research-based instruction, 
family participation and a partnership with the community." We know that all children can learn 
at high levels. At Calaveras School we know our students by name and by need. 
 

 

 

----

---- 

-------- 

Hollister School District 

2690 Cienega Rd. 

Hollister, CA 95023-9687 

(831) 630-6300 
www.hesd.org 

 

District Governing Board 

Stephen Kain, President 

Robert Bernosky 

Carla Torres-Deluna 

Jan Grist 

Elizabeth Martinez 

 

District Administration 

DIego Ochoa 

Superintendent 

Jennifer WIldman 

Assistant Superintendent 

Educational Services 

Erika Sanchez 

Assistant Superintendent, Human 

Resources 

Gabriel Moulaison 

Assistant Superintendent, Fiscal 

Services 

Barbara Brown 

Acting Director, Director, Student 

Support Services 

John Teliha 

Director, Facilities 

Jr. Rayas 

Director, Technology & Innovation 

Caroline Calero 

Director, Learning & Achievement 

Ann Pennington 

Director, Student Nutrition & 

Warehouse 

 

2017-18 School Accountability Report Card for Calaveras Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Calaveras Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Hollister School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

26.5 

20 

24 

4 

0 

2 

0 

1 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

240 

35 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Calaveras Elementary School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.