Cuyama Valley High is much more than a school; it is a community. This School Accountability 
Report Card (SARC) highlights our current achievements and outlines our plans for improvement. 
We strive to establish a closer relationship with our students and parents as we strengthen the ties 
between school and community. We invite greater involvement from all stakeholders in an effort 
to showcase the energy and professional dedication of the faculty and staff. Our teachers’ 
continued training in technology, curriculum, and classroom strategies will ensure that students 
are prepared for the challenges and promises of tomorrow. 
 
An intense focus on academic achievement is evident at Cuyama Valley High School. The faculty 
remains committed to serving the needs of all students. Becoming a vital center for learning that 
provides the best educational choices for all students in our attendance area is our most important 
goal. With an honest appraisal of the entire school program, teachers are building a dynamic 
environment that prepares all students for the ever-changing demands of our society. 
 
 
Our District 
Cuyama Joint Unified School District serves a dynamic, rural community centrally located in the 
beautiful Cuyama Valley, a remote northeastern section of Santa Barbara County. The area is very 
rural with farming and natural resources the key industries. It is the District’s philosophy that public 
education is of fundamental importance to a free society and to the continued development of 
democratic values, individual liberty and an appreciation for cultural diversity in society. The 
District’s objective is to provide the guidance and resources necessary to insure an environment 
conducive to learning. In order for education to succeed, there must be an ongoing partnership 
between parents, students, educators, and the community. It is important to emphasize that the 
goal of our educational system is not to supplant parental responsibilities throughout the learning 
process. Rather, it is the policy of the District to foster parental participation in order that an 
educational climate is created which reinforces and fosters the positive and healthy development 
of the child. The District's first goal is to provide each student with the basic skills necessary to 
participate and function effectively in society. The District is committed to the goal of achieving 
academic excellence through a program of instruction and experiences which offers each child an 
opportunity to develop to the maximum of his or her individual capabilities. 
 

2017-18 School Accountability Report Card for Cuyama Valley High School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cuyama Valley High School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

8 

0 

0 

6 

0 

0 

5 

1 

0 

Cuyama Joint Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

16 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Cuyama Valley High School 

16-17 

17-18 

18-19