Marshall Elementary is a “community of learners” as all stakeholders show their commitment to 
academic excellence for our students. Marshall is a safe place where students feel comfortable, 
relaxed and eager to learn. They consistently put forth effort to do their best. In this supportive 
environment, high academic standards are established, students are appropriately challenged and 
staff creates a nurturing learning community for a diverse population. Our school community is 
focused upon the academic and social development of every child and is an AVID (Advancement 
via Individual Determination) learning community. 
 
Marshall’s staff delivers lessons designed to facilitate standards attainment for each student. 
District standards, developed from state standards, are the driving force of the school’s curriculum 
and instructional planning. Teachers use research proven strategies and provide lessons that foster 
maximum student achievement. Marshall Elementary staff focuses on Professional Learning 
Community concepts, and works to collaborate at the grade level and at the site level. Marshall 
Elementary School is working toward becoming the first AVID (Advancement via Independent 
Determination) Elementary School, providing students with strategies for success in our global 
society. Teachers and staff members are accustomed to making data driven decisions regarding 
student performance and to implement research proven strategies for student mastery of 
curriculum. Marshall Elementary embraces and appreciates family involvement. Marshall 
encourages parent participation and provides many opportunities for parent involvement and 
leadership. The wide range of activities serves to maintain the community spirit and broaden 
students’ experiences with one another. Students are increasingly successful as they benefit from 
the caring relationships between their families and teachers. Maintaining a comfortable 
atmosphere with high academic expectations enhances student performance. 
 
Marshall Elementary was built in 1949 and includes the three original classroom wings, a 
multipurpose room, administrative office and two wings of portables containing four classrooms 
and a science center. Marshall Elementary School currently has 18 regular classrooms, 2 special day 
classrooms, a library, a Speech and Language office, counseling office, an RTI intervention room, 
and a resource room. Marshall Elementary does not have a computer lab, instead, each classroom 
has a dedicated chromebook cart. Grades 3-5 are 1:1 with a chromebook for each student, grades 
K-2 have a minimum of one chromebook for every 2 students. The Science Center was completed 
in November of 2003 equipped with user-friendly modern technology. Marshall Elementary has 
been modernized and classrooms were wired for high speed wifi Internet access. Two brand new 
play structures were built in the summer of 2014, one in our main playground and another smaller 
structure in our Kindergarten play area. Measure G funds supplied a brand new larger play 
structure in the Kindergarten area in the summer of 2018. Students also enjoy a large blacktop 
playground of approximately 33,000 square feet where students play a variety of organized games. 
Our black top area currently houses 5 portables in anticipation of further Measure G construction. 
We currently utilize Marshall Fields and the blacktop adjacent to for physical education. Our 
Marshall Field blacktop was resurfaced and painted during the August 2018 to support physical 
education. We utilize a number of portable basketball and volleyball courts as well as the five 
baseball diamonds and a large landscaped field that can be used for a variety of games and team 
sports daily. 
 
 

 

2017-18 School Accountability Report Card for Marshall Elementary School 

Page 1 of 9 

 

Mission Statement: 
Marshall Elementary, a community of learners, values diversity and promotes equity. We will work together to achieve our goals of 
meeting and exceeding standards. Marshall Elementary School will support our students in becoming responsible and productive citizens. 
We are Marshall Elementary, nothing less than our very best.