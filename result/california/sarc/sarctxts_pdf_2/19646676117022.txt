Crossroads School has a unique role in the district. The Community Day School staff works closely 
with students and their families, other district schools, and the School Attendance Review Board 
(SARB) to serve those who will benefit from a Community Day School environment. 
 
Crossroads is a school where curriculum is delivered to promote student achievement, motivation, 
and responsibility. The cognitive, social, and emotional strengths of each student are the beginning 
point for instruction that is based on current research and practice. Students are expected to 
internalize a vision of success and apply what they have learned at Crossroads to their education 
as it continues beyond the Community Day School setting. 
 
Crossroads School expects all students to meet local and state academic standards and to 
understand the relationship of school achievement to life goals. Crossroads School students are 
expected to behave in ways that reflect a growing understanding of themselves and their 
responsibility to family, school and community. 
 

 

 

----

-

--- 

-------- 

Lancaster School District 

44711 Cedar Avenue 
Lancaster, CA 93534 

(661) 948-4661 
www.lancsd.org 

 

District Governing Board 

Greg Tepe, President 

Keith Giles, Vice President 

Diane Grooms, Clerk 

Sandra Price, Member 

Duane Winn, Member 

 

District Administration 

Dr. Michele Bowers 

Superintendent 

Lexy Conte 

Deputy Superintendent 

Human Resources Services 

 

Bart Hoffman 

Assistant Superintendent 

Educational Services 

 

Dr. Larry Freise 

Assistant Superintendent 

Business Services 

 

 

2017-18 School Accountability Report Card for Crossroads Community Day School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Crossroads Community Day School 

16-17 17-18 18-19 

Teacher Credentials 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Lancaster School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

6 

0 

0 

6 

0 

0 

6 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

572 

46 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.