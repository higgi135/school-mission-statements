Superintendent’s messageLEUSD is well positioned for the 2018 school year! The collaboration 
between voters, parents, teachers and staff has resulted in student achievement growth, improved 
facilities and playing fields under Measure V, and new 
instructional technology for 
classrooms.Under our state accountability system, the Fall 2017 update to the California School 
Dashboard was recently released. The Dashboard provides teachers and principals with valuable 
performance data that is examined weekly during their PLC collaboration time, and used to guide 
instruction. The current Dashboard shows LEUSD schools are making progress.LEUSD improved in 
several areas, though English Language Arts and Math growth indicators are little changed from a 
year ago, a trend statewide. The Dashboard is powered by six state indicators and four local 
indicators, along with a variety of custom reports by which to compare schools, districts, and 
student subgroups. Dashboard color codes reflect status and change to depict achievement growth 
ranging from Red (lowest) to Blue (highest).LEUSD performance highlights:SUSPENSION RATES—by 
lowering suspensions, results for the 'Suspension' indicator have IMPROVED from orange to yellow, 
changing from a high suspension rating to a medium suspension rating.‘EL’ PROGRESS—English 
Learners IMPROVED from yellow to green, changing from 'Medium' to 'High' as a result of an 
additional 3.1% students making progress towards English proficiency.GRADUATION RATE—this 
indicator has IMPROVED from green to blue. The District continues to have a 'High' rating due to 
an increase in graduating students of 1.5%.COLLEGE/CAREER PREPAREDNESS—growing College & 
Career Preparedness is an area for increased attention. The CA School Dashboard shows 35.2% of 
LEUSD graduates as being 'Prepared.” The State will not have a color indicator for College & Career 
Preparedness until 2018, but notably, LEUSD 11th grade students' ELA and Mathematics scaled 
scores increased in both areas respectively by 0.3 points and 4.2 points, a positive college readiness 
indicator.CHRONIC ABSENTEEISM—for the first time, the CA Schools Dashboard includes District 
and school Chronic Absenteeism rates, though a Chronic Absenteeism color indicator does not 
appear on the Fall 2017 report. District wide, LEUSD’s Chronically Absent statistic is 12.8%.LEUSD 
met all local indicators for implementing state standards, providing safe school facilities, adequate 
books and instructional materials, as well as meeting indicators for school climate, and student and 
parent engagement. View how LEUSD is performing at www.caschooldashboard.org.These are 
positive indicators, so let’s be mindful of the many positive accomplishments of 2017 to help set 
the bar high for 2018.Sincerely,Dr. Doug Kimberly,Superintendent 
 
Principal’s Message 
The School Accountability Report Card (SARC) is an analysis of resources, data, and educational 
programs and gives an accurate picture of the combined efforts of Temescal Canyon High School 
(TCHS) students, staff, and administration for 2015 - 2016 school year. The Federal Government 
requires all teachers, in core subject areas, to meet certain standards to be Highly Qualified. Those 
requirements are: 

• Possess a Bachelor’s degree 
• Possess a California Teaching Credential 
• Demonstrate competence in core academic subjects 

 
Temescal Canyon administration makes every effort to recruit and retain the most highly qualified 
California credentialed teachers. This professional staff continues to train and concentrate in 
processes, which will result in improved student scholastic and social achievement. TCHS staff 
shares a vision of success in the social, emotional, and academic development of our students. 

2017-18 School Accountability Report Card for Temescal Canyon High School 

Page 1 of 12 

 

 
As principal, I invite all parents and guardians to join the staff at Temescal Canyon High School (A California Distinguished School and an 
IB World School) in the preparation and education of all graduating students, allowing them to meet the challenges of the 21st century. 
 
Each year, the Temescal Canyon Staff reviews the commitment to our students and agrees to follow clear expectations. Our school goal 
is for each Titan graduate is to be an: 

• 
• 
• 
• 
• 

Individual with academic skills 
Individual who is healthy 
Individual who possesses lifelong skills 
Individual who works collaboratively 
Individual who contributes to our community 

 
School Vision Statement: 
The vision of Temescal Canyon High School is to provide interdisciplinary instruction and curriculum that is challenging, innovative, and 
globally minded. Each student’s success, both in high school and in future endeavors, reinforced through the partnership of home, school, 
and community.