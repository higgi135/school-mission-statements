Sowers Middle School - Setting the Course for 21st Century Leaders 
 
Isaac L. Sowers students will experience an academically challenging and supportive learning 
environment which promotes the realization of all students’ full potential. 
 
Beliefs 

• Developmental Awareness: Our dedicated and knowledgeable staff recognizes the 
developmental uniqueness of the middle school child and demonstrates a commitment 
to foster the conditions necessary for student achievement. School personnel models 
the qualities and characteristics they expect to instill in students. 

• 

• 

• 

• Nurturing Learning Environment: We offer an 

inviting, safe, and purposeful 
environment which is conducive to learning. Parents and staff will work together to 
provide students with the time, support, and encouragement they need to achieve. 
Supportive Structure and Schedule: We are organized to promote meaningful 
professional collaboration with a focus on learning and providing access for all 
students. Teams are viewed as the building blocks for a strong Professional Learning 
Community. We place a high priority on protecting instructional and collaborative 
planning time, maintaining a reasonable class size, and providing supportive student 
services during the regular school day. 
Standards-based Curriculum: We will provide a core curriculum that can be enriched, 
extended, or differentiated to meet the individual learning needs of students. 
Education in the fine and practical arts, physical education, and language are valued as 
integral parts of the instructional program. We will provide a standards-based learning 
environment which clearly establishes criterion for subject mastery and identifies what 
students will know and be able to accomplish as a result of completing the instructional 
program. The scope and sequence of the curriculum will reflect a continuum of 
offerings that challenge all students. 
Focus on Best Practices of Instruction: We will develop and implement a wide array of 
strategies to optimize student learning. Technology will be integrated into the 
instructional process as a means to achieve specific curricular outcomes. We value an 
on-going process of vertical articulation with elementary and high school colleagues to 
ensure a continuum of instruction. 

• Clarity of Assessment Goals and Practices (Progress Indicators): We will employ a 
variety of methods to measure the learning progress of every student. Teachers, in 
collaboration with their departments, will develop consistent grading practices in 
support of the instructional program. Assessment information will be used to guide the 
learning process, refine instructional plans, and inform parents of their child’s academic 
progress. 
Emphasis on Active Participation: We will promote active student participation in co-
curricular programs. The middle school years offer students their first real opportunity 
to enjoy a range of extracurricular activities, engage in exploratory study, and 
participate in the performing arts. We recognize the potential of these programs to 
enhance the health and fitness of young adolescents, improve academic performance, 
and build positive links between school, families, and the local community.

• 

2017-18 School Accountability Report Card for Isaac L. Sowers Middle School 

Page 1 of 10 

 

 

Sowers Middle School opened in 1971, as a 6th through 8th grade middle school. Sowers is located in a city characterized by miles of 
beautiful Pacific shoreline, bicycle paths, parks, and an ecological preserve. The school is one of two middle schools and seven elementary 
schools serving approximately 6,800 students in Huntington Beach. The school facility is attractive, well-maintained, and situated in a 
residential area adjacent to a park. There are 43 classrooms which are allocated for general, elective, and special education classes and 
support services. The school also offers a Multipurpose Room (room 402), and separate boys and girls Physical Education locker rooms. 
Assemblies, student programs, and large activities are conducted in a spacious interior school mall complete with stage and sound system. 
Technologically, Sowers is fully networked in all classrooms with both hardwired and wireless networking capabilities. 
 
Sowers Middle School faces many of the same challenges and opportunities confronting schools throughout Southern California. Sowers 
offers a rich elective program along with school clubs, committees, and activity groups for students to join. In addition, pyramids of 
intervention classes have been designed to help students experiencing difficulty achieving school success. Sowers Middle School also 
offers CORE areas of English Language Arts and Social Studies in block periods with GATE/Honors classes at all grade levels. Along with 
our regular math curriculum, an accelerated math program is offered in grades 7th and 8th for eligible students to participate based on 
district criteria. Services are provided in designated special education programs with a continuum of support which includes co-teaching 
with one special education and one general education teacher in CORE classes, two special day classes, and guided support in math. We 
also provide social thinking classes for students with needs in socialization. A school-wide positive behavior system is in place in which all 
students, staff, and community follow the ISMS Way demonstrating Integrity - Safety - Making Responsible Choices - Showing Respect. 
 
A talented and enthusiastic staff makes Sowers a school where students take pride in learning and growing socially. Parents feel supported 
and teachers have the tools to help students learn. Our instructional team, work collaboratively under the construct of Professional 
Learning Communities(PLC) developing and implementing programs that motivate and engage all students. Our teachers care deeply 
about all aspects of the school programs, participate in shared decision-making, and engage in professional development activities to 
build skills to meet students' needs, understand the uniqueness of the middle school child, and perform with distinction. Our support staff 
works together to nurture the intellectual, physical, and emotional capacities of each child. Sowers staff members are well trained, 
experienced, and are enthusiastic about meeting student needs. Our Student Body of approximately 1,150 regular and special education 
students represent a cross section of cultural, racial, and ethnic backgrounds, as well as an array of ability levels from the learning 
handicapped to the intellectually gifted. The virtues of mutual respect and recognition of personal dignity are fostered and nurtured by 
students and staff. The contributions, achievements, and progress of ALL students are valued and respected. The parents and community 
are an integral part of the Sowers School community and are welcomed to participate as volunteers in school, speakers, committee 
members, PTSA, and School Site Council. The local community and family involvement is vital to the success of our educational program 
at Sowers.