By providing an equity-driven education based on the California Content Standards, by promoting 
personal development, and by offering a safe and encouraging environment, Piute Middle School 
prepares students to work cooperatively, think critically, and problem solve. 
 
Piute students are community members who are responsible, respectful, fair, and trustworthy and 
they leave Piute ready to succeed at future endeavors in their career choice and college. 
 

 

 

----

-

--- 

-------- 

Lancaster School District 

44711 Cedar Avenue 
Lancaster, CA 93534 

(661) 948-4661 
www.lancsd.org 

 

District Governing Board 

Greg Tepe, President 

Keith Giles, Vice President 

Diane Grooms, Clerk 

Sandra Price, Member 

Duane Winn, Member 

 

District Administration 

Dr. Michele Bowers 

Superintendent 

Lexy Conte 

Deputy Superintendent 

Human Resources Services 

 

Bart Hoffman 

Assistant Superintendent 

Educational Services 

 

Dr. Larry Freise 

Assistant Superintendent 

Business Services 

 

 

2017-18 School Accountability Report Card for Piute Middle School 

Page 1 of 8