John C. Fremont Elementary School (Fremont Elementary) is located on the corner of West 10th 
Street and English Street, just north of West Civic Center Drive. Fremont Elementary is a self-
contained, open space building with partitions serving as room dividers. There are 25 classroom 
spaces, a library, a music room, a computer lab, and a variety of offices. All classrooms have a 
SMART Board, 4 student computer stations and each room has at least 2 data drops for internet 
access. Every student in grades K-5 has their own assigned Chromebook. In addition, Kinders have 
access to 60 iPads. Our school computer lab has 50 computer stations. Fremont Elementary School 
is a thriving and exciting place for students to learn. It is the goal of all staff members to provide 
each child with the academic, social, and emotional tools necessary to becoming competent, 
capable, and concerned 21st Century citizens. Everyone is committed to providing all students with 
the best and sound instructional education and are passionate about ensuring academic success 
for all. 
 
In the 2017-18 school year, Fremont Elementary's focus was to use instructional strategies that 
engage students in learning activities that help them meet the Common Core State Standards in 
both English Language Arts and Mathematics preparing them for college and career. Through data 
analysis of student assessments, the instructional staff has identified the need for a literacy 
campaign to increase reading comprehension and fluency. The staff is committed to creating a 
positive and safe school climate through Positive Behavior Interventions and Supports (PBIS). In the 
2017-18 school year, Fremont Elementary increased by 11% proficient in English Language Arts and 
13% proficient in Mathematics on the Smarter Balanced Assessment (SBAC) and is recognized as a 
Platinum School by the California PBIS Coalition for implementing PBIS with fidelity to the national 
framework. 
 
Fremont Elementary students are offered a variety of programs that provide them with both 
enrichment and intervention. All students use technology for personalized learning through 
Spacial-Temporal Math (ST Math - also known as JiJi), Accelerated Reader (AR), Lexia and/or 
Reading Plus. Throughout the school year, teachers offer students before or after school tutoring 
or enrichment. The school provides a full inclusion Mild-to-Moderate Special Education program in 
several service models, pull-out and/or push in collaboration with a specialist. The Engage 360 
after-school program offers academic enrichment, homework support, and physical activity to 
approximately 200 students. During the summer break, students can participate in Engage 360, 
Summer Enrichment, or Extended School Year. Students are given the opportunity to develop their 
leadership skills through student council and a variety of school based programs. In addition, 
students in grades 2-5 are provided with music classes and students in grades TK-5 are provided art 
classes. There is a preschool program and an early literacy mommy and me class offered on campus 
to help prepare young children for kindergarten. Parents are our partners and are always welcome 
on our school campus as parent leaders, volunteers, or participants in the learning opportunities 
being offered. There is a community worker 3 days per week and a counselor 2 days per week to 
assist and help meet the social and emotional needs of our students and their family. 
 
School Mission 
Fremont Elementary School promotes high academic achievement through an integrated, 
California Common Core State Standards based curriculum. To accomplish this, accountability will 
be consistently monitored through a systematic assessment process. Parent and community 
partnerships will form another integral component that maximizes every child's individual 
academic, social, and emotional growth. 

2017-18 School Accountability Report Card for John C. Fremont Elementary School 

Page 1 of 11 

 

 
School Vision 
Fremont Elementary School expects that all students will achieve high academic success, enabling them to prepare for college and career 
and to develop into socially responsible, contributing members of society with a life-long commitment to learning. 
 
Statement of Behavior Purpose 
Fremont Elementary is a community of scholars whose goal is to learn and to help each other to be good citizens.District ProfileSanta Ana 
Unified School District (SAUSD) is the seventh largest district in the state, currently serving nearly 49,300 students in grades K-12, residing 
in the city of Santa Ana. As of 2017-18, SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 alternative high 
schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or Reduced Price Meal program, 39% 
qualifying for English language learner support, and approximately 13% receiving special education services. Our district’s schools have 
received California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I Academic Achieving Schools, and 
Governor’s Higher Expectations awards in honor of their outstanding programs. In addition, 20 schools have received the Golden Bell 
Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have developed and maintained high 
expectations to ensure every student’s intellectual, creative, physical, emotional, and social development needs are met. The district’s 
commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high quality educational 
program. Consistent success in meeting student performance goals is directly attributed to the district’s energetic teaching staff and 
strong parent and community support. 
 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently serving nearly 49,300 students in grades K-
12, residing in the city of Santa Ana. As of 2017-18, SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 
alternative high schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or Reduced Price Meal 
program, 39% qualifying for English language learner support, and approximately 13% receiving special education services. Our district’s 
schools have received California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I Academic Achieving 
Schools, and Governor’s Higher Expectations awards in honor of their outstanding programs. In addition, 20 schools have received the 
Golden Bell Award since 1990.Each of Santa Ana Unified School District’s staff members, parent, and community partners have developed 
and maintained high expectations to ensure every student’s intellectual, creative, physical, emotional, and social development needs are 
met. The district’s commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high 
quality educational program. Consistent success in meeting student performance goals is directly attributed to the district’s energetic 
teaching staff and strong parent and community support.