Welcome to Heritage High school! Heritage High School is home of the Patriots, a Title I Academic 
Achievement School, 2013 CIF Football Champions, and an AVID National Demonstration School. 
We also have an exemplary counseling program, that was recently recognized as a Model of 
Excellence for Riverside County Office of Education. Our Agriculture/FFA program is the 7th largest 
in the country and the program has also received numerous awards. Our population of 
approximately 2,800 students continue to create a culture of LEGACY. Each day, our students and 
staff focus on building a LEGACY through Leadership, Excellence, Generosity, Academics, Charter, 
and Opportunity. Building a LEGACY is our credo and something special that we truly strive to 
uphold on a daily basis, not just something we talk about. It is our mission and core belief system. 
We hold ourselves to high standards, and take responsibility to live up to the tenets of LEGACY to 
continue building a tradition of success. 
 
Heritage High School has amassed a number of accolades during our short history as a school. We 
attribute this to our outstanding staff, a dedicated student body and incredible families who 
support academic, artistic and athletic achievement. We have built a partnership with our families 
as we realize a successful education is a joint venture between school and community. Our staff 
believes we have a responsibility to deliver on the expectations we promote through LEGACY. 
 
Whether you are a student, parent, or community member, you will find a wealth of important 
information on our website including registration materials, activities calendars, newsletters, clubs, 
links to staff contact information, as well as club and program pages to name just a few. Please take 
time to explore our website, review our programs, the student/parent handbook and other 
communications that exists. Again, welcome to Heritage High School and we start another great 
school year. Be a part of the Patriot Family, and help us "Build a LEGACY" of success! 
 
 
 
Mission Statement 
The mission of Heritage High School is to provide a rigorous academic curriculum in a safe and 
caring environment that promotes all students’ maximum success in the larger community. 
 
 

2017-18 School Accountability Report Card for Heritage High School 

Page 1 of 13 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Heritage High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

120 

117 

112 

6 

0 

0 

0 

1 

0 

Perris Union High School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

431 

7 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Heritage High School 

16-17 

17-18 

18-19