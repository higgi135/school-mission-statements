The mission of ASPIRE Community Day School is to engage and support all students with high-
quality instruction and targeted behavior intervention in order to develop the skills and foundation 
necessary to create a life as successful and responsible students and adults. 
 
ASPIRE CDS utilizes Project Based Learning as a primary element in instruction. Students learn how 
to communicate effectively as they work with one another utilizing verbal, written and 
technological skills that they develop as they engage in challenging projects and curriculum. 
Systems and Supports for students at ASPIRE include mandated counseling, for every student, 
where they can be seen one on one by a counselor or in group settings on topics like Anger 
Management, Decision Making, or Substance Abuse. ASPIRE CDS strives for excellence in 
maintaining a positive culture and climate that supports all our students and families so that they 
can be successful. Utilizing restorative practices and staffs' intentional building of relationships to 
assist in maintaining a positive environment where students learn to trust more and eventually 
perform better academically. 
 
 

 

 

-------- 

----

--

-- 

Hemet Unified School District 

1791 West Acacia Ave. 
Hemet, CA 92545-3632 

(951) 765-5100 

www.hemetusd.org 

 

District Governing Board 

Ms. Stacey Bailey 

Mr. Rob Davis 

Mrs. Megan Haley 

Mr. Gene Hikel 

Mr. Vic Scavarda 

Mr. Patrick Searl 

Mr. Ross Valenzuela 

 

District Administration 

Ms. Christi Barrett 
Superintendent 

Mr. Darrin Watters 

Deputy Superintendent 

Business Services 

 

Mr. Darel Hansen 

Assistant Superintendent 

Human Resources 

 

Mrs. Tracy Chambers 

Assistant Superintendent 

Educational Services 

 

Dr. Karen Valdes 

Assistant Superintendent 

Student Services 

 

 

2017-18 School Accountability Report Card for ASPIRE Community Day School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

ASPIRE Community Day School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

11 

12 

12 

1 

3 

0 

6 

0 

7 

Hemet Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1098 

40 

118 

Teacher Misassignments and Vacant Teacher Positions at this School 

ASPIRE Community Day School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.