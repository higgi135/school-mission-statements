Hidden Springs Elementary School is located within a planned community in the northwest section 
of the Moreno Valley Unified School District. Hidden Springs Elementary School was dedicated on 
June 12, 1993. Our district motto, "Excellence on Purpose," is designed to convey the value placed 
on student potential and the nurturing atmosphere of the school community. 
 
Hidden Springs School has 21 general education classes, a Transitional Kindergarten class, two 
special education classes for moderate/severe students, and one full-time Resource Specialist 
teacher (RSP). Our part-time staff includes a counselor, a speech therapist, and a school 
psychologist. 
 
Hidden Springs is a school with a diverse population in which eight languages are spoken. The 
diversity of our school community offers opportunities for expanded cultural experiences that 
enhance the lives of our students. Parent volunteers are an integral and essential part of the 
school's instructional program. Our school has parents and staff who comprise an active and 
supportive Booster Club, School Site Council (SSC), District English Learner Advisory Committee 
(DELAC), and African American Advisory Council (AAAC). 
 
Our goal is to provide an educational program, based on State and District curriculum standards, 
which fosters the total development of our students. We envision our school as a partnership with 
families, promoting the intellectual, social, emotional, cultural, and physical development of all 
students. Together we define expectations, provide inspiration, and share a collective responsibility 
in the education our students. 
 
Hidden Springs has received the honors of California Distinguished School, Riverside County's 
Models of Academic Excellence, California School Board Association's Golden Bell Award, and 
California Gold Medal PBIS School. 
 

2017-18 School Accountability Report Card for Hidden Springs Elementary School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Hidden Springs Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

22 

21 

25 

0 

0 

0 

0 

0 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.