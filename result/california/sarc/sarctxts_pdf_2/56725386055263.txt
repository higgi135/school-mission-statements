Curren School is a K-8 school which serves approximately 1100 students and offers a number of 
academic programs including AVID, Dual Language Immersion (50/50 model), Structured English 
Immersion, Designated and Integrated ELD, extra curricular activities and clubs. The Curren 
Academy focus is Enriching Youth via Environmental Studies (EYES). 
 
Curren Mission and Vision: The EYES academy along with AVID strategies prepares students to be 
college and career ready. We develop students’ understanding of their role as responsible, 
empathetic stewards of our world. The Environmental Studies Academy focus incorporates the 
social, political, and scientific aspects of caring for each other and our world. There is a single K-8 
plan of curriculum design which builds upon the various aspects of environmental studies as the 
students progress throughout the years. 
 
At Curren K-8 School we are constantly striving to reach the mission of Oxnard School District. 
Teachers provide students with rigorous and rich instruction in the Common Core State Standards. 
We continue to implement strategies that will help our students increase their academic 
achievement as measured by reading progress and test scores. We are very proud of our students! 
The Curren community of parents and teachers are a dedicated, hard working and committed 
group who work together to support every student to grow academically and reach his/her dreams. 
 
As you read this report, a picture will emerge of a school dedicated to student success, a qualified 
faculty that is professionally and personally committed to meeting the learning needs of students, 
and a student body which is motivated to perform well. 
 

 

 

----

---- 

----

---- 

Oxnard School District 

1051 South A Street 

Oxnard, California, 93033 

(805) 385-1501 

http://www.oxnardsd.org/ 

 

District Governing Board 

Veronica Robles-Solis, President 

Monica Madrigal Lopez, Clerk 

Debra Cordes, Trustee 

Dr. Jesus Vega, Trustee 

Denis O'Leary, Trustee 

 

District Administration 

Dr. Cesar Morales 
Superintendent 

Janet Penanhoat 

Assistant Superintendent, Business 

Services 

Dr. Jesus Vaca 

Assistant Superintendent, Human 

Resources & Support Services 

Dr. Ana DeGenna 

Assistant Superintendent, 

Educational Services 

 

2017-18 School Accountability Report Card for Curren School K-8 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Curren School K-8 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Oxnard School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

50 

48 

47 

0 

0 

0 

0 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

47 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Curren School K-8 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

1 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.