Our vision is to provide a public Waldorf high school education ensuring that our students can 
successfully engage the world. 
 
George Washington Carver School of Arts and Science is a four-year high school with an enrollment 
at 270 in grades 9 through 12. We anticipate growing each year until we reach capacity at 400 
students. George Washington Carver is a college preparatory school and stresses the value of 
higher education. To that end, we have more graduation requirements than other high schools in 
Sacramento. We pair our graduation requirements with the University of California a-g entrance 
requirements. 
 
George Washington Carver is proud of its cultural and socioeconomic diversity. Currently, 41% of 
the student population participates in the free or reduced lunch program. The ethnic makeup is 
comprised of 6 nationalities, with the largest groups being Caucasian, Asian, African-American, 
Russian and Hispanic. 
 
George Washington Carver receives the Action Civics grant acknowledging the powerful education 
students receive in citizenship and contributing to their community. All faculty attend Waldorf 
professional development throughout the school year through the WEST program (Waldorf 
Education Seminar for Teachers). Our program is enhanced by we have planted a substantial 
garden and small orchard. Students participate in sowing and harvesting our vegetables. Ww also 
have installed a professional cable TV broadcasting studio in partnership with Access Sacramento. 
 
George Washington Carver School of Arts and Science has two aims: to prepare students to be 
successful in college and to help them learn about the world so they will come to know themselves. 
 
To achieve this vision, we help students develop critical thinking and creative problem-solving skills 
using a rigorous college-preparatory curriculum that integrates the arts and issues of social justice 
and environmental stewardship. Our dedicated teachers act as guides pointing the way so that 
students can find their own unique path toward becoming intelligent, self-confident, and socially 
responsible. 
 
 

---

- 

----

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Mary Harding Young 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for George Washington Carver School of Arts and Science 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

George Washington Carver School of Arts and 
Science 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

15 

13 

13 

0 

0 

0 

0 

1 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.