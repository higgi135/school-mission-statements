The vision of Monte Vista School is to create a safe learning environment where all students master 
common core grade level standards. We believe that all students can learn, that success breeds 
success, and that we control the conditions of success. 
 
Principal's Message 
Monte Vista is one of six elementary schools in the South Whittier Elementary School District. We 
are located in South Whittier, a dynamic community of economic and social diversity. Monte Vista 
opened in 1969 and continues to serve students in fourth through sixth grade. The main ethnic 
group in the community is Hispanic/Latino. Specially funded programs include school-wide Title 
One, School Improvement funds, the Resource Specialist Program, Speech and Language Specialist, 
Adaptive Physical Education, Special Day Classes, and Full Inclusion. Supplemental services are 
provided based on student and school needs. Intervention and enrichment programs that meet the 
needs of all our students are available during, before, and after school. Monte Vista School 
envisions the home, school, and community working together to promote self-confidence, a 
positive attitude, and success in learning for each child within a safe and secure learning 
environment. It is our goal to provide all students with an educational environment that will create 
lifelong learners. We will develop habits of the mind that will lead students to be flexible thinkers, 
problem solvers, and team players in order to become productive, contributing members of the 
global community. To create an environment that promotes powerful learning, we will provide a 
standards-based curriculum that is meaning-centered, addressing the various learning modalities 
to meet the needs of all learners, and enable students to be creative problem solvers. We will also 
guide our students to develop an appreciation of and respect for cultural diversity. Students who 
attend Monte Vista will become proficient in reading and math through the standards-based 
reading and math programs. 
 
Following the ancient adage, “It takes a village to educate a child,” we believe the parents and 
community play an integral part in the success of our students. Therefore; we encourage the 
participation of parents, community members, and business partners. Parent education classes are 
offered throughout the school year to help parents support the academic success of their children. 
 
To our students and their families we pledge to provide an enriching education which includes the 
following: 
 
A standards-based reading and math program with an appropriate assessment of student 
performance to guide and modify instruction 
Staff development to ensure the most qualified teachers with professional development 
A safe, nurturing, caring, and orderly environment for all students and staff 
A healthy and nutritious meal program 
Continuous Communication with families and community stakeholders 
An academic program that is enriched through fine arts: music and art 
An academic program that provides safety nets, interventions, and a support network during and 
outside of the school site 
Permanent and Portable Tech Labs with laptop computer stations, iPads, and portable 
computer/laptop units (Chromebooks) 
Daily morning and after-school program 
 
Andrea Larios, Principal 

2017-18 School Accountability Report Card for Monte Vista School 

Page 1 of 7 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Monte Vista School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

13 

13 

13 

0 

0 

0 

0 

0 

0 

South Whittier School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

138 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Monte Vista School 

16-17 

17-18 

18-19