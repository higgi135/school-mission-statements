It is my pleasure to introduce this annual School Accountability Report Card. The data and 
information contained within these pages will prove useful in informing you about our school, 
including but not limited to academic standings, curriculum and instruction, school facilities and 
safety, budget, and facility enhancement. 
 
As you read our School Accountability Report Card, you will see a school that ensures every child 
has equal access to a rigorous core curriculum in language arts, mathematics, science, and social 
studies. The hard-working staff is both skilled and dedicated to the success of our students. The 
parents and the community organizations are very supportive. 
 
Our goal in presenting you with this information is to keep our community well informed. If you 
have any questions or are interested in making an appointment to discuss this report, please call 
our school. 
Karen Trejo, PRINCIPAL 
 
 

 

 

----

--

-- 

Downey Unified School District 

11627 Brookshire Ave. 
Downey, CA 90241-7017 

(562) 469-6500 
www.dusd.net 

 

District Governing Board 

Tod M. Corrin 

Donald E. LaPlante 

D. Mark Morris 

Giovanna Perez-Saab 

Barbara R. Samperi 

Martha E. Sodetani 

Nancy A. Swenson 

 

District Administration 

John A. Garcia, Jr., Ph.D. 

Superintendent 

Christina Aragon 

Associate Superintendent, Business 

Services 

Roger Brossmer 

Assistant Superintendent, 

Educational Services - Secondary 

Wayne Shannon 

Assistant Superintendent, 

Educational Services - Elementary 

Rena Thompson, Ed.D. 

Assistant Superintendent, 

Certificated Human Resources 

Veronica Lizardi 

Director, Instructional Support 

Programs 

Marian Reynolds 

Administrator, Student Services 

Jennifer Robbins 

Director, Elementary Education 

Patricia Sandoval, Ed.D. 

Director, Special Education 

 

2017-18 School Accountability Report Card for Ward Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ward Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

20.0 

19 

21 

0 

0 

0 

0 

0 

0 

Downey Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

861 

13 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ward Elementary School 

16-17 

17-18 

18-19