Carlsbad Alternative School / Carlsbad Seaside Academy (CSA), shares a campus with Carlsbad 
Village Academy at 3557 Monroe Street in Carlsbad. It is a California public school for grades 9-12. 
The high school program provides diversified instruction which meets the needs of individual 
students. 
 
The Independent Study program provides and educational alternative for secondary students who 
have the ability, study skills, motivation, and self-discipline to learn independently under the 
supervision and support of a credentialed secondary education teacher who assigns and evaluates 
all coursework. Independent Study students may desire an accelerated learning program, a flexible 
schedule to pursue a career goal, or may simply learn better in an independent program with one-
to-one interactions with a master teacher. 
 
To register or if you would like to know more about our program at Carlsbad Seaside Academy, 
please contact Jorge Espinoza at 760-331-5299 or via email at jespinoza@carlsbadusd.net 
 
 

 

 

----

---- 

----

---

- 

Carlsbad Unified School District 

6225 El Camino Real 
Carlsbad, CA 92009 

760-331-5000 

www.carlsbadusd.k12.ca.us 

 

District Governing Board 

Mr. Ray Pearson, PRESIDENT 

Ms. Kathy Rallings, VICE PRESIDENT 

Mrs. Veronica Williams , CLERK 

Mrs. Claudine Jones, MEMBER 

Mrs. Elisa Williamson, MEMBER 

 

District Administration 

Benjamin Churchill, Ed. D. 

Superintendent 

Mr. Chris Wright 

DEPUTY SUPERINTENDENT, 
ADMINISTRATIVE SERVICES 

Mr. Rick Grove 

DEPUTY SUPERINTENDENT, 
ADMINISTRATIVE SERVICES 

Dr. Robert Nye 

ASSISTANT SUPERINTENDENT, 

INSTRUCTIONAL SERVICES 

 

2017-18 School Accountability Report Card for Carlsbad Seaside Academy 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Carlsbad Seaside Academy 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

3 

0 

0 

0 

0 

0 

3 

0 

0 

Carlsbad Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

574 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Carlsbad Seaside Academy 

16-17 

17-18 

18-19