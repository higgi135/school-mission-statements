Principal’s Message: 
Dear Parents/Guardians/Community Members, 
 
Welcome to Valle Vista Elementary School of Hemet Unified! We take great pride in our school and 
believe it is one of the finest elementary schools in the San Jacinto Valley. We have a very dedicated 
staff who believes that ALL children can learn. Various intervention and enrichment programs are 
offered throughout the year aimed at meeting the needs of all students. We offer an English 
Language Development program in grades TK-5. Students are prepared for the technology age by 
means of 1-1 technology for our upper grades and computers in every classroom. Each classroom 
is equipped with at least seven laptop and/or Chromebook computers for student learning, and 
more than half our classrooms have SmartBoard interactive whiteboards and/or promethium 
boards. Additionally, we offer our students access to computer-based enrichment and intervention 
programs for both at school and at home, such as Imagine Learning, Imagine Learning Math, ST 
Math, and Accelerated Reader. 
 
We are looking forward to a very productive year in which all our Viking students are successful. 
Teachers and students work diligently with California State Standards-based curriculum and 
SmarterBalanced assessment programs. One of our goals this year is to continue to improve on our 
past successes and ensure a high level of achievement for all students. With our experienced staff 
and supportive parents, we believe that we can, not only accomplish our goals, but exceed them. 
 
In order to support a safe and welcoming school environment, we practiced school-wide Positive 
Behavior Interventions and Supports, where we set explicit expectations for our students, teach 
necessary skills to meet those expectations, and positively reinforce when expectations are being 
met. 
 
We encourage all parents to become active in their child's education. This can be accomplished 
through volunteering, attending parent-teacher conferences, open house, back to school night, and 
other evening events. Your involvement is critical in the success of your child. I look forward to 
meeting and working with our parents and community, and if you would ever like, please stop by 
the school, visit our school website at vallevista.hemetusd.org, send an email, or call us at (951) 
927-0800. 
 
Our Mission: Valle Vista's mission is to create a culture of universal achievement. 
 
Our Vision: At Valle Vista Elementary, we envision a clean, safe, and respectful environment with 
happy, enthusiastic children who are supported both by involved families and highly qualified and 
engaged staff, in an effort to achieve common educational goals. 
 
Our Core Values: 
1. We believe it is the student's, parent's, staff's, and community's responsibility to ensure every 
student, with appropriate support, becomes successful, lifelong learner. 
2. We believe all students have the right to a safe, orderly, supportive and positive school 
environment. 
3. We believe in the power of collaboration among and between grade levels.

2017-18 School Accountability Report Card for Valle Vista Elementary School 

Page 1 of 10 

 

 
School Profile: 
This annual report describes Valle Vista Elementary as a school where staff, students and parents work together to foster high expectations 
and a commitment to academic excellence. Valle Vista Elementary prides itself on meeting the needs of all students with a vision towards 
preparing children to face the demands of the 21st century. 
 
Valle Vista Elementary School is located in the eastern San Jacinto Valley and is part of the Hemet Unified School District. The community 
of Valle Vista is in the Hemet Post Office jurisdiction but represents a long established agricultural area of the valley, in transition to a 
more urbanized bedroom community for the city of Hemet. Valle Vista Elementary School is located at the corner of Fairview and 
Mayberry Avenues.