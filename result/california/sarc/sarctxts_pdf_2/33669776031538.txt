Foothill Elementary School is located in the western region of Riverside and serves students in 
grades kindergarten through five following a traditional calendar. 
 
Our vision at Foothill Elementary School is that our school embodies a community spirit of caring 
built through the cooperative efforts of students, staff and community in a safe and nurturing 
environment. We endeavor to achieve high standards for all students. 
 
This yearly School Report Card is provided to our families and the public as a beginning step in 
helping you to get to know us better. We are proud of our staff and students at Foothill and invite 
you to become involved at our school. There is a spirit of cooperation and progress at our school, 
and we want to share that with you. 
 
Mission Statement 
Alvord Unified School District, a dynamic learning community that embraces innovation, exists to 
ensure all students attain lifelong success through a system distinguished by: 

• Active and inclusive partnerships 
• Relationships that foster a culture of trust and integrity 
• High expectations and equitable learning opportunities for all 
• A mindset that promotes continuous improvement 
• Multiple opportunities for exploration and creativity 
• Professional development that promotes quality teaching and learning 
• Access to learning experiences that promote a high quality of life 

 
In addition, all schools strive to attain the Alvord vision that all students will realize their unlimited 
potential. 
 

--

-- 

----

---- 

Alvord Unified School District 

9 KPC Parkway 

Corona, CA 92879 

(951) 509-5070 

www.alvordschools.org 

 

District Governing Board 

Robert Schwandt, President 

Carolyn M. Wilson, Vice President 

Lizeth Vega, Clerk 

Julie A. Moreno, Member 

Joanna Dorado, Ed.D., Member 

 

District Administration 

Allan J. Mucerino, Ed.D. 

Superintendent 

Dr. Robert E. Presby 

Assistant Superintendent, Human 

Resources 

Susana Lopez 

Assistant Superintendent, Business 

Services 

Julie Kohler-Mount 

Interim Assistant Superintendent, 

Educational Services 

Susan R. Boyd 

Assistant Superintendent, Student 

Services 

Kevin Emenaker 

Executive Director, Administrative 

Services 

 

2017-18 School Accountability Report Card for Foothill Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Foothill Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

28 

28 

27 

0 

0 

0 

0 

0 

0 

Alvord Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

665 

5 

37 

Teacher Misassignments and Vacant Teacher Positions at this School 

Foothill Elementary School 

16-17 

17-18 

18-19