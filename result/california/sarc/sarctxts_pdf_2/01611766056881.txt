The staff at Walters Junior High School is dedicated to the growth and development of all our 
students. Our goal is to provide our students with an appropriate and rigorous curriculum. We 
believe each child can achieve academic success when provided quality educational experiences 
that recognize and maintain high expectations. To support our students we provide a wide range 
of services and programs that address both academic and social development for junior high 
student's success. Our academic programs are before, after, and during school. Our many clubs 
meet through the week, and cover a range of topics. Our lunch time activities, and after school 
sports continue to add school spirit and relationship building. 
 
Walters Junior High is a dynamic, exciting place to be! It is with pleasure that we present our School 
Accountability Report Card to you. 
 

 

 

-
-
-
-
-
-
-
- 

----
---- 
Fremont Unified School District 

4210 Technology Drive 

Fremont, CA 94538 

(510) 657-2350 

www.fremont.k12.ca.us 

 

District Governing Board 

Yang Shao, Ph.D. 

Michele Berke, Ph.D. 

Desrie Campbell 

Ann Crosbie 

Larry Sweeney 

 

District Administration 

Kim Wallace, Ed.D. 

Superintendent 

Raul A. Parungao 

Associate Superintendent 

Debbie Ashmore 

Assistant Superintendent, 

Instruction 

Raul M. Zamora, Ed.D. 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for WALTERS JUNIOR HIGH SCHOOL 

Page 1 of 9