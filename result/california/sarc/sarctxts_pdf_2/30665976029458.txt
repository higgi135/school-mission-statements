Paularino Elementary serves pre-kindergarten through sixth grade students from diverse 
backgrounds, including Hispanic, white, Asian, and African-American students. Almost half of our 
students are English learners and more than two thirds of our students are low-income. The campus 
houses six Applied Behavior Analysis classrooms for students with Autism Spectrum Disorder and 
moderate to severe needs. Regardless of the obstacles, staff, parents, and students believe that 
each student is capable of meeting academic standards in reading, writing, and math and that 
collectively the community has the power to help students reach their dreams. 
 
To help each student reach academic goals, teachers and support staff analyze data, discuss and 
share best practices, participate in professional development, and establish individual and school-
wide goals on a weekly basis. State-of-the-art technology is integrated throughout the curriculum, 
and grades 2-6 students have 1:1 technology devices to provide easy access to the Internet and 
supportive software to complete assignments in all subjects. Students participate in weekly music, 
library, and technology programs. In addition, more than 150 students participate in after school 
academic and enrichment classes aligned to STEM and the arts. Unique to Paularino is a thriving 
garden that supports “farm to table” learning experiences aligned to California State Standards and 
Next Generation Science Standards. 
 
Underpinning academic achievement is a robust character education program with a focus on 
Positive Behavior Intervention and Support (PBIS). Student council skits at flag deck and use of 
literature and displays in the classroom promote the virtues of being fair, responsible, respectful, 
caring, trustworthy, and a good citizen. Students demonstrating these virtues, such as taking turns 
and sharing, receive a Panther Paw with one copy to take home to parents and another copy for a 
bucket. Winners of twice-a-month bucket drawings receive such prizes as extra recess or a book. 
 
Also supporting academic achievement is an involved school community. They work with students, 
help in the library, and help with various school improvement projects. The PTA raises funds for 
supplemental supplies, assemblies, and special events, including events for the entire family. 
Paularino collaborates with a variety of community organizations, such as No Excuses University 
and outside vendors that provide both academic and enrichment support. 
 
Every Friday, our school community gathers for a celebration. Before returning to the classrooms, 
we end with a spirited recitation of the school pledge, “Good better best, never let it rest until our 
good is better and our better is best!” Paularino is a great place to learn! Together, our enthusiastic 
students, talented staff, and supportive parent community are dedicated to preparing students for 
college and careers. 

--

-- 

----

---- 

Newport-Mesa Unified School 

District 

2985 Bear St. 

Costa Mesa, CA 92626 

(714) 424-5033 
www.nmusd.us 

 

District Governing Board 

Charlene Metoyer, President 

Martha Fluor, Vice President 

Dana Black, Clerk 

Ashley Anderson, Member 

Michelle Barto, Member 

Vicki Snell, Member 

Karen Yelsey, Member 

 

District Administration 

Dr. Frederick Navarro 

Superintendent 

Mr. Russell Lee-Sung 

Deputy Superintendent, 
Chief Academic Officer 

Ms. Leona Olson 

Assistant Superintendent, 

Human Resources 

Mr. Tim Holcomb 

Assistant Superintendent, 

Chief Operating Officer 

Dr. Sara Jocham 

Assistant Superintendent, 

Student Support Services/SELPA 

Mr. Jeff Trader 

Executive Director, 

Chief Business Official 

Dr. Kirk Bauermeister 

Executive Director, 
Secondary Education 

Dr. Kurt Suhr 

Executive Director, 

Elementary Education 

 

 

2017-18 School Accountability Report Card for Paularino Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Paularino Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

26 

25 

28 

0 

0 

1 

0 

0 

0 

Newport-Mesa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1105 

5 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

Paularino Elementary School 

16-17 

17-18 

18-19