“Our McKinley community SOARs: We are Safe, Organized, Accountable, and Respectful.” Thus, 
McKinley school staff takes responsibility in working with students in our community by providing 
the academic and social foundation necessary to guide them in being safe, organized, accountable 
and respectful (SOAR). We acknowledge and understand the importance of building strong positive 
relationships with students, parents, and community members. Our staff strives to provide a safe 
and nurturing learning environment so that our students are able to focus on learning. Teachers 
have adopted the implementation of the Professional Learning Community (PLC) system and 
concentrate on learning, collaboration, and data-driven instruction. McKinley teachers foster 
academic achievement and deliver rigorous instruction to prepare each student for their journey 
towards college and career readiness. In addition, our school is the only school within Stockton 
Unified School District to implement a Late Exit Bilingual Program in grades Kindergarten through 
sixth grade. McKinley School teachers implement and embrace Advancement Via Individual 
Determination (AVID) strategies and philosophy. In addition, McKinley’s staff instills academic and 
social responsibility, mutual respect, and self-confidence through the implementation of Positive 
Behavior Intervention & Support (PBIS). Each student can and will learn at McKinley School because 
we are committed to the development of the whole child and celebrate their individual growth. I 
am very proud to represent the community of McKinley Elementary School. 
 
Mission: 
McKinley Elementary staff delivers rigorous instruction which prepares each student for their 
journey towards college and career readiness. We are committed to the development of the whole 
child and celebrating their individual growth. 
 
McKinley Vision Statement 
Our McKinley community SOARs: We are Safe, Organized, Accountable, and Respectful. 
 
 
 
 

2017-18 School Accountability Report Card for McKinley Elementary 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

McKinley Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

29 

30 

27 

1 

0 

3 

0 

9 

0 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

McKinley Elementary 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

1 

1 

0 

2 

2 

0 

1 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.