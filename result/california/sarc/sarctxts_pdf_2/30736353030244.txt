Laguna Hills High School is a public high school located in Laguna Hills, California which serves the 
Saddleback Valley Unified School District. The school has the smallest student enrollment of the 
district’s four schools at approximately 1,600 students. The mascot for Laguna Hills is the Hawk, 
and the official colors are brown and gold. 220 credits are required to graduate from Laguna Hills 
High School. 94% of LHHS graduates are accepted at 2 or 4-year colleges. 
 
The innovative programs at Laguna Hills High School include the Golden-Bell Award Winning Two-
Way Language Immersion Program; Computer Science Pathway; Culinary Arts Program; a four-year 
Model United Nations program; an interdisciplinary senior humanities program; twenty-five year 
established International Baccalaurate (IB) program, and robust Careers in Technical Education 
pathway. 
 
Mission Statement 
At Laguna Hills High School, our mission is to provide a rigorous and accessible curriculum in a small-
school, “family” environment. Welcome to the Nest! 
 
Vision Statement 
By fostering creativity, communication, collaboration, and critical thinking skills, we will prepare 
students for college and career, empowering them to build a better world through mutual respect 
and intercultural understanding. 
 

-------- 

 

 

----

-

--- 

Saddleback Valley Unified School 

District 

25631 Peter A. Hartman Way 

Mission Viejo CA, 92691 

(949) 586-1234 
www.svusd.org 

 

District Governing Board 

Suzie Swartz, President 

Dr. Edward Wong, Vice President 

Amanda Morrell, Clerk 

Barbara Schulman, Member 

Greg Kunath, Member 

 

District Administration 

Crystal Turner, Ed D. 

Superintendent 

Dr. Terry Stanfill 

Assistant Superintendent, Human 

Resources 

Connie Cavanaugh 

Assistant Superintendent, Business 

Laura Ott 

Assistant Superintendent, 

Educational Services 

Mark Perez 

Director, Communications and 

Administrative Services 

Dr. Ron Pirayoff 

Director, Secondary Education 

Liza Zielasko 

Director, Elementary Education 

Dr. Diane Clark 

Director, Special Education 

Rae Lynn Nelson 
Director, SELPA 

Dr. Francis Dizon 

Director, Student Services 

 

2017-18 School Accountability Report Card for Laguna Hills High School 

Page 1 of 13 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Laguna Hills High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

65 

65 

68 

0 

0 

0 

0 

0 

0 

Saddleback Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1,157 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Laguna Hills High School 

16-17 

17-18 

18-19