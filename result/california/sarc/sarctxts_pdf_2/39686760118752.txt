At Spanos Elementary we are guided by our mission statement of "All students learn at the highest 
levels and are inspired to become life-long learners." At Spanos School we, 

• Collaborate around common data to implement best practices. 
• 
Facilitate purposeful learning which leads to high achievement. 
Foster strong community connections which creates a welcoming, respectful, and safe 
• 
learning environment. 

Mastery of Common Core grade level standards in reading, writing, and mathematics forms the 
foundation of the instructional programs. Science, social studies, physical education and visual and 
performing arts rounds out our curriculum. 
 
We are proud to offer our students the opportunity to work one-to-one with Chromebooks during 
the instructional day. We continue to grow our collection of library books each year, to ensure that 
our students have access to multiple genres and endless choice of titles to enjoy. 
 
We also continue to build strong collaboration and partnerships with our parents and families of 
the students we teach. Your involvement and participation is needed and valued. Communication 
is a vital component to the success of our students. School wide communication folders go home 
each Wednesday and weekly information can be found in this communicator, our website and on 
the marquee in the front of our school. For more information, pictures, and celebratory news, 
please continue 
site at 
http://www.stocktonusd.net/Spanos as it is updated regularly. We look forward to working with 
you to create new opportunities and new accomplishments for our students this year. 
 
Danielle Valtierra, Principal 
 

to view our Alex G. Spanos Elementary School web 

 

 

----

---- 

----

---- 

Stockton Unified School District 

701 North Madison St. 

Stockton, CA 95202-1634 

(209) 933-7000 

www.stocktonusd.net 

 

District Governing Board 

Cecilia Mendez 

AngelAnn Flores 

Kathleen Garcia 

Lange Luntao 

Maria Mendez 

Scot McBrian 

Candelaria Vargas 

 

District Administration 

John E. Deasy, Ph.D. 

Superintendent 

Dr. Reyes Gauna 

Assistant Superintendent of 
Educational Support Services 

Craig Wells 

Assistant Superintendent of 

Human Resources 

Sonjhia Lowery 

Assistant Superintendent of 

Educational Services 

 

2017-18 School Accountability Report Card for Spanos (Alex G.) Elementary 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Spanos (Alex G.) Elementary 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

19 

19 

20 

0 

0 

0 

0 

1 

0 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Spanos (Alex G.) Elementary 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

1 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.