Victory School is a Preschool through 8th grade elementary school located in Stockton, California 
in San Joaquin County. The school serves over 500 students on a campus with 28 classrooms, 
library, resource rooms and a science lab. In 2014 the school was modernized, completely updating 
the interior of the school, including all classrooms, offices and multipurpose room. We created a 
new library to be used by all Kindergarten-8th grade students. The property also has three 
basketball courts, a baseball/soccer field, and an extensive playground with tether-ball, volleyball, 
slides, bars and other activities for students to enjoy. 
 
Our staff is committed to working with the educational community to provide all students with the 
opportunity to reach his/her potential. Together, we collaborate and make a difference for our 
students. The staff at Victory School is very dedicated to providing the best possible educational 
experience for each and every child. Our belief is to do “whatever it takes” to meet a student’s 
educational and emotional needs. Our teachers deliver instruction using Common Core State 
Standards adopted by Stockton Unified School District and the State of California. The staff works 
diligently to implement rigorous curriculum and monitors student progress on a regular basis. 
Academic intervention and small group instruction are provided for students who are not meeting 
grade level standards through our Learning Center. Available to all students are reading and math 
interventions using computer based programs: ST Math, Imagine Learning and Compass Learning. 
For academically advanced students and GATE level students, enrichment curriculum activities are 
embedded into daily instruction and lessons. 
 
Victory students also benefit from a variety of programs to meet individual needs after school. Our 
after school program, which runs until 6:00 p.m. daily, provides our students with additional 
academic tutoring, as well as physical activities in a safe and structured environment. Victory 
currently has a full time Assistant Principal, full time Program Specialist, full time Resource 
Specialist Program teacher, full time Instructional Coach, one and a half counseling position plus 
one position as a half time counselor and half time School Psychologist, an 80 percent Speech 
Therapist, and 3 ½ hours a day of a Library Media Assistant. We encourage a site visit for anyone 
interested in learning more about Victory School and the educational opportunities that await 
students. 
 
Victory School’s Vision – Our vision is to provide a safe, caring, disciplined and stimulating 
environment where children will achieve their fullest potential so they can make their best 
contribution to our world. We value our school community and recognize the role it plays in 
realizing our vision. 
 
Victory School’s Mission – Our mission is to empower students to reach their highest potential, 
inspire academic and behavioral excellence, and encourage members of the school community to 
participate in our endeavor. 
 
EVERY STUDENT, EVERY DAY, WHATEVER IT TAKES! 
 
 
 
 

2017-18 School Accountability Report Card for Victory Elementary 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Victory Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

23 

24 

24 

4 

0 

3 

0 

5 

0 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Victory Elementary 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

1 

2 

1 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.