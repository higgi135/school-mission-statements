Welcome to Palmquist Elementary! 
 
Our hope is that you and your child will find our school to be joyful and welcoming. As school is a 
“home away from home”, we provide your child with a safe and nurturing environment. Our 
incredible staff will ensure that your child has an amazing school year by engaging them in hands 
on learning experiences, which will tap into their innate curiosity and support your child in making 
connections between the concepts and skills they are learning in the classroom, and how it 
connects to the real world applications. We are an Inquiry Based Learning School, with an 
emphasis on STEM (Science, Technology, Engineering, Mathematics) education. Our goal is for your 
child is to develop the habits of a life long learner and to feel excited about coming to our school 
each day! 
Some of the learning opportunities available for students center around our vision for learning, 
where "Our students students to be innovative critical thinkers who: ask and answer complex 
questions, investigate real world problems, and design solutions to challenges based on their 
understanding and application of science, technology, engineering, and math (STEM)." Education 
at Palmquist Elementary is child-centered and focuses on teaming with parents and community 
members to prepare all students to be competent, responsible, and contributing members of 
society. The Palmquist staff is dedicated to creating a learning environment that fosters a students’ 
innate curiosity and encourages the joy of discovery. Our skilled staff members provide a rigorous 
academic program that emphasizes the application of an inquiry-based learning model. Students 
become motivated, engaged learners ready to face the challenges of the 21st century.through the 
instruction of creativity, communication, collaboration, and critical thinking, 
 
In 2011 the Oceanside Unified Board of Education designated Palmquist Elementary as a STEM 
school. To advance the instruction of science, technology, engineering, and math, Palmquist has 
implemented the inquiry-based learning curriculum: Project Lead the Way. The curriculum 
integrates Common Core State Standards and Next Generation Science Standards. Our hands-on 
life science, media production studio and maker space labs, as well as our greenhouse and garden 
offer students the opportunity to apply skills learned during core instruction to real-world 
problems. The introduction of 1:1 iPads in grades 3-5 and 2:1 in grades K-2 has enhanced student 
use of technology as a tool for creation not just consumption. In addition this year, Palmquist offers 
Inquiry Labs in which students use the inquiry model to explore areas of interest to them. For 
example this year, we are offering collage art, music production/appreciation, Global Citizenship, 
Drama, product engineering and design, high tech farming, sensors in the green house, and 
creative writing during the school day in grades 3 – 5. 
For over five years Palmquist has offered the largest after school program in the district. Palmquist 
Roadrunner Enrichment Program (PREP) offers over 20 classes. For more information about this 
PTO sponsored program, visit the school website. Palmquist was the first district school to become 
a Zero Waste School. Presently, we are working on the development of an organic and a hydroponic 
farm. The farm's produce is used in the school cafeteria as part of a partnership with the school 
district Farm to School Program. Starting in 2016, Palmquist will begin a student-developed 
Community Sponsored Agriculture program. We welcome all visitors and community members who 
are interested in learning more about Palmquist to tour the school. 
 
 
 
 

2017-18 School Accountability Report Card for Palmquist Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Palmquist Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

33.00 

0.0 

0.0 

33 

0.0 

 

 

 

 

Oceanside Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

30 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Palmquist Elementary School 

16-17 

17-18 

18-19