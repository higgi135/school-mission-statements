John J. Montgomery is one of 49 schools in the Chula Vista Elementary School District, including 
charters. The school was built in 1945 and it was completely modernized in the summer of 2018. It 
has 16 classrooms, including four relocatables added in 1999-00, a library, auditorium/cafeteria 
and administrative offices. 
 
Montgomery’s vision is to foster a community of learners who embody the core values of integrity, 
collaboration, perseverance, justice, lifelong learning, service, and innovation. At Montgomery, we 
put students first. Our diverse community is comprised of students, teachers, parents, volunteers, 
and community members. We provide a safe, clean, nurturing learning environment where true 
learning and personal growth can take place. 
 
Montgomery has a culture of achievement. We develop innovative thinkers and lifelong learners. 
Our students learn in a way that is meaningful and relevant to ensure that they are college and 
career ready. Montgomery’s students are effective communicators and creative problem solvers. 
They are self-motivated, responsible, and gritty. Montgomery Eagles are technologically savvy 
students who value literacy and education. They are active participants in their own learning. 
 
At Montgomery, we develop the whole child and believe in the arts. Montgomery is a safe haven 
where teachers and students exemplify integrity, strength of character, and health. We embrace 
students’ unique qualities and build on their strengths. Students are enriched by the arts and 
extracurricular activities at Montgomery. Our students have the opportunities and tools to lead 
healthy, balanced lifestyles. 
 
Montgomery is committed to creating the leaders of tomorrow as we soar from good to great! 
 
 
 

----

---- 

Chula Vista Elementary School 

District 

84 East J Street 

Chula Vista, CA 91910-6100 

(619) 425-9600 
www.cvesd.org 

 

District Governing Board 

Leslie Ray Bunker 

Armando Farias 

Laurie K. Humphrey 

Eduardo Reyes, Ed.D. 

Francisco Tamayo 

 

District Administration 

Francisco Escobedo, Ed.D. 

Superintendent 

Jeffrey Thiel, Ed.D. 

Assistant Superintendent, Human 
Resources Services and Support 

Oscar Esquivel 

Deputy Superintendent, Business 

Services and Support 

Matthew Tessier, Ed.D. 

Assistant Superintendent, 

Innovation and Instruction Services 

and Support 

 

2017-18 School Accountability Report Card for John J. Montgomery Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

John J. Montgomery Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

17 

0 

Teaching Outside Subject Area of Competence 

NA 

17 

16 

0 

 

0 

 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.