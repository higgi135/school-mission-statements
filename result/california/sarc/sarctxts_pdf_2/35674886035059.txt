2018-2019 
Jefferson Elementary, the only school in the Jefferson School District, is located in Paicines, 
California near Pinnacles National Park. There are no businesses or services in this small, rural and 
somewhat remote district with the nearest city, Hollister, located 35 miles away. The school 
consists of two buildings one of which contains a large K-8 classroom and office, while the second 
building, a multipurpose room, doubles as a community center after school hours. Eight students 
in grades K-8 are currently enrolled at Jefferson. Enrollment fluctuates only slightly year to year 
with many of the students starting in kindergarten and remaining through eighth grade. 50% of 
Jefferson students are Hispanic and 50% are White. 25% of students are English Learners, and 25% 
receive Special Education services. One full time credentialed teacher and one full time teaching 
assistant work collaboratively to provide instruction to the students. The teacher also serves as the 
principal and superintendent. 
 
The Educational Program, 2018-2019 
 
Though the school is small, the instructional program is rigorous, engaging, and focused on student 
growth and academic achievement. California Core State standards are taught in all subjects and a 
variety of data is utilized to set goals, drive instruction, gauge student progress, and measure 
program effectiveness. Assessments include formative and ongoing assessment through STAR 360, 
annual tests such as ELPAC and CAASPP, as well ongoing assessment utilized within lessons each 
day. Based on that data, instruction is highly differentiated for every students throughout the 
school day within small group, whole group, and one on one instruction from the teacher and 
Instructional Aid. Core and supplemental programs include Lucy Calkin’s Writer’s Workshope, 
Fountas and Pinnell guided reading intervention, Envision math, and Houghton Mifflin ELA. Every 
student has a new laptop and technology is integrated in teaching and learning on a daily basis. 
Finally, the students receive a broad curricular experience through science and social studies 
instruction, experiments, field trips, STEAM activities, art projects, and musical performances. 
Jefferson Elementary truly provides an enriching and safe experience for all students. 
 
School Goals, 2018-2019 
 
Goal 1: Ensure that all students have access to the “conditions of learning that form the necessary 
foundation for student achievement to include: fully credentialed and appropriately assigned 
teachers; a broad course of study that includes standards-based instruction in all core subjects; 
access to standards-based texts and instructional materials from the latest adoption cycle for all 
core subjects; and a clean, safe, well-maintained facility in which to learn. 

2017-18 School Accountability Report Card for Jefferson Elementary School 

Page 1 of 9 

 

Goal 2: Ensure that by May 2020, 60% of all students will met grade level standards in ELA; 45% of EL Students will meet grade level 
standards in ELA; 100% of EL students enrolled on Census Day will increase one level on any of the ELPAC sub-tests; and beginning in 17-
18. 85% of EL students, continuously enrolled for 48 months, will qualify for re-designation within that time. 
 
Goal 3: Ensure that all parents have the support and opportunity to strengthen their connection to the school, participate in all school 
programs and provide input into school decisions. 
 
Goal 4: Ensure that all students have the opportunity to develop their intellectual, artistic, physical and social capacities within a positive, 
safe, trouble-free school climate where students are engaged in their learning and connected to their school.