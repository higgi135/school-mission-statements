Eureka Union Elementary School District encompasses 14.8 square miles in Granite Bay and parts 
of Roseville. The district is comprised of five elementary schools, two junior high schools, and one 
preschool. Greenhills School has an enrollment of 440 pre-kindergarten through third grade 
students. 
 
Greenhills School enjoys a reputation as having a strong academic and elective program with a 
capable, caring staff and administration. Greenhills is a CSBA Golden Bell Award recipient, 
recognized for its excellence in education and program development in the state of California. The 
staff at Greenhills works together to live our vision statement: “Greenhills is a community that 
works together to foster a sense of belonging. We are committed to every child’s emotional and 
academic success. We believe family and community involvement is integral to our students’ 
growth and achievement. Our highly qualified staff is dedicated to ongoing professional learning. 
Our traditions strengthen long-lasting relationships. We take pride in nurturing the spirit and joy of 
childhood." 
 
Principal Kim Gerould leads a devoted staff of 24 teachers and 25 classified employees dedicated 
to academic excellence and being responsive to each individual learner. 
 

----

---- 

Eureka Union Elementary School 

District 

5455 Eureka Rd 

Granite Bay 

(916) 791-4939 

www.eurekausd.org 

 

District Governing Board 

Andrew Sheehy - Board President 

Renee Nash - Board Clerk 

Jeffrey Conklin 

Ryan Jones 

Melissa MacDonald, Ph.D. 

 

District Administration 

Tom Janis 

Superintendent 

Melody Glaspey 

Chief Business Officer 

Kelli Hanson, Ed.D. 

Director of Human Resources 

Kristi Marinus 

Director of Student Services 

Ginna Guiang-Myers, Ph.D. 

Director of Curriculum, Instruction, 

Professional Development and 

Student Assessment 

 

2017-18 School Accountability Report Card for Greenhills Elementary School 

Page 1 of 8