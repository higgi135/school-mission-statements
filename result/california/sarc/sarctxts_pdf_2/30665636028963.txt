At Ladera Palma, we are extremely proud of our spectacular Dual Immersion Program. It was 
established in 2014 and has grown yearly! We now are able to offer Dual Immersion TK to 4th 
grades on one campus. Our Dual Immersion (DI) offers native Spanish speakers and native English 
speakers an opportunity to maintain and develop their first language while acquiring native-like 
communication and literacy skills in a second language. The Dual Immersion Program follows a 
90:10 model that focuses on Spanish Immersion first with sequential bilingual/bi-literate skills 
acquired in English. English is added to the curriculum incrementally so that by fourth grade, 
students are receiving instruction for 50% of the day in both English and Spanish until Eighth grade. 
Dual Immersion staff is committed to continue to incorporate Visual and Performing Arts, Project 
Based Learning and Coding to our students’ curriculum through the target language, Spanish. 
Additionally, we have also opened the first TK Dual Immersion program in our surrounding area! 
 
Our program's foundation is aligned with the Three Pillars of Dual Language Education. As well, our 
school goals are aligned to these tenets. The first goal is that our students achieve bilingualism and 
bi-literacy. Our goal is for all of our students to be able to speak, understand, read and write in both 
languages. Our second goal is that all of our students reach high academic achievement levels in 
both languages. We measure and monitor our students' skills continuously, using assessments in 
both languages. Our third and final goal is that all of our students are able to develop cross-cultural 
competence skills and understanding. Through culturally responsive instruction and curriculum, we 
explore various cultural celebrations and education, focusing specifically on the culture of Latin 
America. 
 
To reach these goals, it is very important that the entire school work as a team. The Ladera Palma 
staff demonstrates a deep commitment to working as a united Professional Learning Community 
(PLC). To this end, our teachers meet regularly to discuss student achievement; plan 
collaboratively; follow professional lines of inquiry to further their professional learning; and work 
collaboratively to review and revise instructional plans and goals. Additionally, our teachers 
exemplify being life-long learners as they continuously search for effective instructional pedagogy 
and support research to further our goals as a Dual Immersion school. 
 
Our school is also committed to empowering and engaging our students' families and community. 
Our students can succeed in a community that supports and nurtures their quest for a multi-lingual, 
multi-literate and multicultural education. To this end, our staff works diligently in collaboration 
with our parents to create an academically and culturally-rich learning environment. Parents are 
offered a variety of opportunities to become involved in their school through volunteering, 
community events, parent education classes, clubs and through our PTA. We realize the power of 
working collaboratively with our families to augment our students' education. Additionally, we 
work closely with our community institutions to provide support or extension opportunities for our 
students and their families. 
 

2017-18 School Accountability Report Card for Ladera Palma Elementary School 

Page 1 of 6 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ladera Palma Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

17 

18 

14.5 

0 

0 

0 

0 

0 

0 

La Habra City Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1.6 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ladera Palma Elementary School 

16-17 

17-18 

18-19