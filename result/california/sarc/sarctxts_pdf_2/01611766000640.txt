Welcome to Harvey Green School! Our school has a long history of serving our neighborhood, and 
many of the parents of our students were once students at Green themselves. Our school is named 
for the first teacher in Fremont, honoring the tradition of improving lives through education. Our 
school motto is: Educating Today's Learners for Tomorrow's World. All members of the Green 
community share our school vision: In order to prepare all students for success in college and 
career, we provide an educational environment that challenges and motivates all students. To 
achieve this, we believe our mission is to develop students who: 

• 
• 
• 
• 
• 

are cooperative learners able to listen and to exchange ideas effectively. 
take responsibility for their work and their actions. 
respect and accept the differences, strengths, and weakness of themselves and others. 
value learning and have mastered the academic knowledge to succeed in the world. 
exhibit the self-confidence to problem solve and to make good decisions. 

 
Our unique blend of cultures, talents, and support services provides an academically challenging, 
safe, and nurturing environment, which allows each and every child on our campus to thrive. Our 
approximately 500 students represent more than two dozen nationalities and a wide variety of 
languages. Awareness and appreciation of diverse backgrounds and traditions are interwoven into 
the curriculum and school wide activities. We consider students for whom we have special concerns 
as individuals with both strengths and needs. Approximately 30 percent of our students are eligible 
for the free or reduced-price lunch program and 16 percent are English Learners. 
 
Our students with disabilities benefit from a similarly innovative set of programs. Our Resource 
Specialist Program (RSP) staff uses a collaborative model to help students within the regular 
classroom setting and in the RSP center. We offer speech and language support services to our 
elementary students and to eligible preschool students from across the district. 
 
Our counseling enriched program for students diagnosed with severe emotional disturbances 
operates as a school within a school. The program, a partnership between Fremont Unified School 
District, Seneca Center, New Haven Unified School District, and Newark Unified School District, 
blends mental health services with academic instruction for students whose emotional needs 
interfere with their success in learning in a full-time general education classroom. 
 
We are solidly focused on implementing the California State Standards. We continue to examine 
and modify our teaching practices as a Professional Learning Community to ensure that each 
student is ready for success in junior high, high school, college, and their career. The excitement 
around this change is palpable; both students and teachers emerge from lessons energized and 
excited about the learning that is taking place. 
 
We consider education a partnership between school and home, and we invite parents into the 
school as volunteers and participants in school decision-making processes. We offer parent 
education and family nights, and have an active PTA and School Site Council. Programs run by the 
PTA, such as our Math Club, Debate Club, Science Clubs and more, enrich our students' educations 
outside the school day. We seek to weave the many strands of our school community into a bright 
and vibrant tapestry that encompasses the well-being of all our students.

2017-18 School Accountability Report Card for GREEN ELEMENTARY SCHOOL 

Page 1 of 9 

 

 
Our school is moving in sustainable directions, taking advantage of technology and focusing on recycling. Our students sort their lunch 
trash, recycling more than 80% of what used to be thrown away. Information is shared with families and the community through our 
school web site and email. Families receive our newsletter electronically, saving hundreds of sheets of paper each year. Email is used 
extensively to communicate with parents. 
 
We are proud of our school!