The Advanced Learning Academy (ALA) aims to provide SAUSD students with personalized, student-
centered instruction that accelerates learning. Students and teachers have the opportunity to be 
innovators and collaborators. The school is focused around unique instructional methods that 
support competency-based learning, coupled with project-based learning. Both students and 
teachers are given the space and support to “fail fast, and fail often", as we innovate new paths in 
learning. Students will learn daily, iterate continuously, and engage in skills that support 21st 
Century Learning. 
 
Our Mission is for ALA students to learn 21st Century skills through meaningful hands-on 
experiences that prepare them for college and career. 
 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently 
serving nearly 49,300 students in grades K-12, residing in the city of Santa Ana. As of 2017-18, 
SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 alternative high 
schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or 
Reduced Price Meal program, 39% qualifying for English language learner support, and 
approximately 13% receiving special education services. Our district’s schools have received 
California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I 
Academic Achieving Schools, and Governor’s Higher Expectations awards in honor of their 
outstanding programs. In addition, 20 schools have received the Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have 
developed and maintained high expectations to ensure every student’s intellectual, creative, 
physical, emotional, and social development needs are met. The district’s commitment to 
excellence is achieved through a team of professionals dedicated to delivering a challenging, high 
quality educational program. Consistent success in meeting student performance goals is directly 
attributed to the district’s energetic teaching staff and strong parent and community support. 
 

----
---- 
Advanced Learning Academy- 

Santa Ana Unified School District 

1601 East Chestnut Avenue 
Santa Ana, CA 92701-6322 

714-558-5501 
www.sausd.us 

 

District Governing Board 

Valerie Amezcua – Board President 

Rigo Rodriguez, Ph.D. Vice – 

President 

Alfonso Alvarez, Ed.D. – Clerk 

John Palacio – Member 

 

District Administration 

Stefanie P. Phillips, Ed.D. 

Superintendent 

Alfonso Jimenez, Ed.D. 
Deputy Superintendent, 

Educational Services 

Thomas A. Stekol, Ed.D. 
Deputy Superintendent, 
Administrative Services 

Mark A. McKinney 

Associate Superintendent, Human 

Resources 

Daniel Allen, Ed.D. 

Assistant Superintendent, K-12 

Teaching and Learning 

Mayra Helguera, Ed.D. 

Assistant Superintendent, Special 

Education/SELPA 

Sonia R. Llamas, Ed.D., L.C.S.W. 
Assistant Superintendent, K-12 
School Performance and Culture 

Manoj Roychowdhury 

Assistant Superintendent, Business 

Services 

Orin Williams 

Assistant Superintendent, Facilities 

& Governmental Relations 

 

2017-18 School Accountability Report Card for Advanced Learning Academy 

Page 1 of 9 

 

• 

•