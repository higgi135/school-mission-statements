Butterfield is a TK-5 elementary school in the MVUSD. 
 
At Butterfield Elementary School, our vision is to support students' academic and social growth by 
instilling the skills necessary for intellectual and personal excellence. 
 
Butterfield's mission is to work collaboratively with parents and the community as a whole to 
ensure that our students are provided with the highest quality education so that they can achieve 
their best in all areas of life. 
 
In order to fulfill our mission, we: 

Provide ongoing professional development for our staff 
• 
• Use PBIS to promote a safe and positive school climate 
• 
• Monitor student progress on an ongoing basis and implement student interventions based 

Facilitate collaboration among staff, students, parents, and community stakeholders 

on those results 
Promote college awareness 

• 
• Utilize research-based instructional strategies, practices, and resources 

 
Our school strives for excellence by providing unique and varied opportunities for students to 
succeed regardless of social, economic, language, or racial differences. The instructional program 
in each classroom is based on State content standards. Students are provided a rigorous curriculum 
and assessed through a variety of means including common formative assessments, MAP, and 
CAASPP assessments. 
 
District adopted curriculum is used to provide a strong base for our instructional program. Our 
school receives Title I - Improving the Academics Achievement of the Disadvantaged, and LCFF 
funds. Supplementary resources from our School Improvement Programs, assemblies, field trips, 
and staff development assist teachers in maximizing the potential of each of our students. 
 
In addition to our academic program and Character Education, we have activities such as grade 
level parent education meetings, which strengthen the partnership between school and home. 
 
Title I Program: The following goals have been established for our school-wide Title I Program: 

----
---- 
Moreno Valley Unified School 

District 

25634 Alessandro Blvd 

Moreno Valley, CA 92553 

(951) 571-7500 
www.mvusd.net 

 

District Governing Board 

Susan Smith, President 

Jesus M. Holguin, Vice-President 

Cleveland Johnson, Clerk 

Gary E. Baugh. Ed.S., Member 

 

District Administration 

Martinrex Kedziora, Ed.D. 

Superintendent 

Maribel Mattox 

Chief Academic Officer, 

Educational Services 

Tina Daigneault 

Chief Business Official, Business 

Services 

Robert J. Verdi, Ed.D. 

Chief Human Resources Officer, 

Human Resources 

 
 

 

Students will develop a positive commitment to learning. 
Students will master basic skills in language arts and mathematics. 
Student will benefit from increased parent education and involvement. 

• 
• 
• 
• All students identified by District criteria will receive Title I* services. 
• 

Strategically selected students will participate in a program that is coordinated between 
the classroom teacher and Title I staff. 
Students will benefit from a program in which staff will improve their skills and knowledge 
through staff development. 
Title I students will benefit from services provided by a staff qualified to meet their needs. 

• 

• 

 

2017-18 School Accountability Report Card for Butterfield Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Butterfield Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

30 

32 

38 

0 

0 

0 

0 

1 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Butterfield Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.