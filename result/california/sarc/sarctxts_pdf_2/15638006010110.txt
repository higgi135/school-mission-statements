Principal's Message 
Conley School was built in 1913 and is located in South Taft, California. The neighborhood 
surrounding the school is in disrepair but our school building is an amazing historical building that 
houses hard working students and dedicated staff. Conley School is one of four primary schools in 
the Taft City School District. Conley serves approximately 300 students on average in the 
kindergarten through third grades. The staff at Conley School is committed to meeting the needs 
of our students by providing an effective instructional program that bolsters their academic, social, 
physical, and emotional growth. Our staff and volunteers are dedicated to making every students' 
day the best it can be. Tremendous changes have been implemented in the areas of curriculum 
planning, staff development, student assessment, and intervention. Conley School has the Teacher 
Induction Program as well as mentor support for our new teachers. We have a strong group of 
teacher leaders in each grade level who support the implementation and development of our 
curriculum and testing. Conley School is proud that we were the awarded the 2016 CA Honor Roll 
Award for students achievement on our state test scores. 
 
Conley Elementary School’s Vision and Mission Statements 
 
Mission 
Taft City K-3 Schools as a learning environment will empower our students to be successful, 
responsible, and well-educated citizens. 
 
Vision 
Taft City K-3 schools will set the standard for excellence in public education, now and in the future. 
 

2017-18 School Accountability Report Card for Conley Elementary School 

Page 1 of 9