Superintendent’s messageLEUSD is well positioned for the 2018 school year! The collaboration 
between voters, parents, teachers and staff has resulted in student achievement growth, improved 
facilities and playing fields under Measure V, and new 
instructional technology for 
classrooms.Under our state accountability system, the Fall 2017 update to the California School 
Dashboard was recently released. The Dashboard provides teachers and principals with valuable 
performance data that is examined weekly during their PLC collaboration time, and used to guide 
instruction. The current Dashboard shows LEUSD schools are making progress.LEUSD improved in 
several areas, though English Language Arts and Math growth indicators are little changed from a 
year ago, a trend statewide. The Dashboard is powered by six state indicators and four local 
indicators, along with a variety of custom reports by which to compare schools, districts, and 
student subgroups. Dashboard color codes reflect status and change to depict achievement growth 
ranging from Red (lowest) to Blue (highest).LEUSD performance highlights:SUSPENSION RATES—by 
lowering suspensions, results for the 'Suspension' indicator have IMPROVED from orange to yellow, 
changing from a high suspension rating to a medium suspension rating.‘EL’ PROGRESS—English 
Learners IMPROVED from yellow to green, changing from 'Medium' to 'High' as a result of an 
additional 3.1% students making progress towards English proficiency.GRADUATION RATE—this 
indicator has IMPROVED from green to blue. The District continues to have a 'High' rating due to 
an increase in graduating students of 1.5%.COLLEGE/CAREER PREPAREDNESS—growing College & 
Career Preparedness is an area for increased attention. The CA School Dashboard shows 35.2% of 
LEUSD graduates as being 'Prepared.” The State will not have a color indicator for College & Career 
Preparedness until 2018, but notably, LEUSD 11th grade students' ELA and Mathematics scaled 
scores increased in both areas respectively by 0.3 points and 4.2 points, a positive college readiness 
indicator.CHRONIC ABSENTEEISM—for the first time, the CA Schools Dashboard includes District 
and school Chronic Absenteeism rates, though a Chronic Absenteeism color indicator does not 
appear on the Fall 2017 report. District wide, LEUSD’s Chronically Absent statistic is 12.8%.LEUSD 
met all local indicators for implementing state standards, providing safe school facilities, adequate 
books and instructional materials, as well as meeting indicators for school climate, and student and 
parent engagement. View how LEUSD is performing at www.caschooldashboard.org.These are 
positive indicators, so let’s be mindful of the many positive accomplishments of 2017 to help set 
the bar high for 2018.Sincerely,Dr. Doug Kimberly,Superintendent 
 
We believe that the most promising strategy for achieving the mission of Ortega High School is to 
develop our capacity to function as a Professional Learning Community: 
 
A: Organization: Vision and Purpose, Governance, Leadership and Staff, and Resources 

• Unite to achieve a common purpose and clearly defined goals 
• 
• Provide for and expect high levels of commitment, collaboration, and communication 

Set and maintain appropriately high expectations for one another 

among all stakeholders 

B: Standards-based Student Learning: Curriculum 

• 

Join together to engage in a rigorous, equitable, and relevant curriculum which 
addresses individual needs 

• Articulate with one another to create a climate for academic success 
• Engage parents and other members of the community in supporting the State standards 

and the classroom learning environment 

2017-18 School Accountability Report Card for Ortega High School 

Page 1 of 10 

 

 
C: Standards-based Student Learning: Instruction 

• Create a learning environment that offers continuous opportunity for academic success 
• Provide various strategies that will engage all learners in a nonrestrictive environment 
• Promote personal responsibility for behavior and education 

D: Standards-based Student Learning: Assessment and Accountability 

• Provide a program and curriculum to our students that is guided by reliable and timely assessment information 
• Employ various assessment tools to evaluate student growth and progress toward mastery of the State standards 
• Collect, disaggregate, analyze, and report student assessment data to the District, staff and community 
• Evaluate assessment tools and resources to determine their reliability and usefulness for our school’s need 

E: School Culture and Support for Student Personal and Academic Growth 

Involve students, parents, and community on campus through a variety of events 

• 
• Maintain a clean and safe school environment for all students, staff, parents, and visitors 
• Ensure that the master schedule allows for equal access to classes, support services, and activities/opportunities on campus 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ortega High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

14 

14 

0 

0 

0 

0 

 

 

 

Lake Elsinore Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ortega High School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.