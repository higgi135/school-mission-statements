The Buckeye Union Mandarin Immersion Charter School began in August 2018 with 42 students in 
Kindergarten and First Grade. Students in Kindergarten and 1st grade receive instruction in 
Mandarin for 80% of the school day for Mandarin reading and writing, mathematics, science and 
social studies and 20% of their instructional day is in English for English Language Arts and 
Mathematics vocabulary. Other pull-out programs including P.E., library, and computer lab are 
taught in English. As the grade levels increase instructional time split will change to 70/30 in 2nd 
grade, 60/40 in 3rd grade, and 50/50 in 4th and 5th grade. Students are taught by bilingual teachers 
with multiple subject credential and supplemental authorization in Mandarin for their Mandarin 
instructional time and a credentialed multiple subject teacher for their English portion of the day. 
Additional staff will be added each year until the school reaches full capacity K - 5th grade. 
 
This mission of the school is to provide an immersion program where all students reach 
Intermediate-High (Level 6) ACTFL Benchmark Level in reading, writing, listening, and speaking in 
Mandarin by the end of 5th grade as well as meeting or exceeding grade level standards in all 
curriculum areas in English as well as learning associated Mandarin vocabulary and characters in 
core content areas. 
 

-------- 

Buckeye Union Elementary 

School District 

5049 Robert J Mathews Parkway 

El Dorado Hills, CA 95762 

(916) 985-2183 

www.buckeyeusd.org 

 

District Governing Board 

Brenda Hansen-Smith 

Winston Pingrey 

Kirk Seal 

Gloria Silva 

Jon Yoffie 

 

District Administration 

David Roth 

Superintendent 

David Roth, Ph.D. 
Superintendent 

Patricia Randolph 

Director of Student Services 

Nicole Schrader 

Director of Student Services 

Jan Blossom 

Director of Fiscal Services 

Brian McChon 

Director of Facilities, Maintenance 

and Operations 

Nancy Ryan 

Director of Transportation 

Kim Adreasen 

Director of Food Services 

 

2017-18 School Accountability Report Card for Buckeye Union Mandarin Immersion Charter School 

Page 1 of 7