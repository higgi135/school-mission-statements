Diamond Elementary School is school located on South Center Street and just north of West Edinger 
Avenue. Diamond Elementary serves students from transitional kindergarten/kindergarten 
through fifth grade. Our district Early Childhood Education (ECE) program offers a preschool 
program for children ages three to five located at our site. Additional schooling programs operate 
daily before school and from the end of the school day until 6:00 p.m. Students are provided 
homework assistance, standards based instruction and enrichment opportunities while attending 
these programs. 
 
School staff, parents, and students interact in a manner that is built upon mutual respect, 
appreciation, and cooperation. Diamond Elementary is a community of Enthusiastic learners. We 
have a positive Attitude to achieve our Goals and become strong Leaders. We put forth our best 
Effort to soar like EAGLES. 
 
School Vision: 
Diamond Elementary School's vision is to ensure our students achieve their optimum potential 
while demonstrating safe, responsible and respectful behavior. Our staff consistently use research 
based, standards driven instructional practices and maximize effective use of time. We empower 
parents as well as foster community involvement and partnerships in order to assist our students 
in attaining state standards. 
 
School Mission: 
Diamond Elementary School, through a partnership involving students, parents, staff and 
community, empowers all students to develop the skills and strategies necessary to maximize their 
success in all curricular areas and become productive and contributing members of society, while 
being provided a safe, nurturing environment with high expectations for academic and social 
growth. 
 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently 
serving nearly 49,300 students in grades K-12, residing in the city of Santa Ana. As of 2017-18, 
SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 alternative high 
schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or 
Reduced Price Meal program, 39% qualifying for English language learner support, and 
approximately 13% receiving special education services. Our district’s schools have received 
California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I 
Academic Achieving Schools, and Governor’s Higher Expectations awards in honor of their 
outstanding programs. In addition, 20 schools have received the Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have 
developed and maintained high expectations to ensure every student’s intellectual, creative, 
physical, emotional, and social development needs are met. The district’s commitment to 
excellence is achieved through a team of professionals dedicated to delivering a challenging, high 
quality educational program. Consistent success in meeting student performance goals is directly 
attributed to the district’s energetic teaching staff and strong parent and community support. 
 

----

---- 

Santa Ana Unified School District 

1601 East Chestnut Avenue 
Santa Ana, CA 92701-6322 

714-558-5501 
www.sausd.us 

 

District Governing Board 

Valerie Amezcua – Board President 

Rigo Rodriguez, Ph.D. Vice – 

President 

Alfonso Alvarez, Ed.D. – Clerk 

John Palacio – Member 

 

District Administration 

Stefanie P. Phillips, Ed.D. 

Superintendent 

Alfonso Jimenez, Ed.D. 
Deputy Superintendent, 

Educational Services 

Thomas A. Stekol, Ed.D. 
Deputy Superintendent, 
Administrative Services 

Mark A. McKinney 

Associate Superintendent, Human 

Resources 

Daniel Allen, Ed.D. 

Assistant Superintendent, K-12 

Teaching and Learning 

Mayra Helguera, Ed.D. 

Assistant Superintendent, Special 

Education/SELPA 

Sonia R. Llamas, Ed.D., L.C.S.W. 
Assistant Superintendent, K-12 
School Performance and Culture 

Manoj Roychowdhury 

Assistant Superintendent, Business 

Services 

Orin Williams 

Assistant Superintendent, Facilities 

& Governmental Relations 

 

2017-18 School Accountability Report Card for Diamond Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Diamond Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

20 

20 

19 

0 

0 

0 

0 

0 

0 

Santa Ana Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1986 

0 

11 

Teacher Misassignments and Vacant Teacher Positions at this School 

Diamond Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.