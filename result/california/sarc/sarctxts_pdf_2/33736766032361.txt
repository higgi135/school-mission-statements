Oasis Elementary School is situated in the heart of the Oasis Community. The Oasis community is 
predominately made up of farm and field laborers with a significant migrant population. Most of 
our families are living at the poverty level (97.1% of students receive free and reduced lunch) and 
the vast majority of our students live in trailers. The conditions of the trailer parks that house our 
families range from extremely poor to moderate. The school building serves as the unofficial 
community center, and the facilities are not only used for school events, but also non-school events 
such as the FIND Food distribution and ESL classes. Many of our students rely on the school to 
provide two to three nutritious meals a day. Additionally, our students need a significant amount 
of training in social skills, which we provide with school-wide Positive Behavior Intervention and 
Supports. 
 
Oasis Elementary School provides instruction for students in grades TK-6. During the 2017-18 
school year, a total of 660 students were enrolled. The school’s enrollment for 2017-18 was 
comprised of 97.1% free and reduced price meal students, 6.8% students with disabilities, and 
80.9% English Learners, and consisted of the following number of students per grade level: 
 

• 
• 
• 
• 
• 
• 
• 
• 

Transitional Kindergarten - 23 
Kindergarten – 66 
First Grade – 80 
Second Grade – 92 
Third Grade – 85 
Fourth Grade – 98 
Fifth Grade – 117 
Sixth Grade – 99 

Assistant Superintendent, Business 

Services 

Dr. Josie Paredes 

Assistant Superintendent, 

Educational Services 

Dr. Maria Gandera 

Assistant Superintendent, Human 

Resources 

 

 

2017-18 School Accountability Report Card for Oasis Elementary 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Oasis Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

34 

34 

32 

0 

0 

0 

0 

0 

0 

Coachella Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

811 

14 

1 

Teacher Misassignments and Vacant Teacher Positions at this School 

Oasis Elementary 

16-17 

17-18 

18-19