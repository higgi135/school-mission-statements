Haydock Academy of Arts and Sciences is a 6th-8th grade magnet school with a focus on Visual and 
Performing Arts and Environmental Science and is open to students interested in the arts from 
throughout the Oxnard School District through the district’s open enrollment process. Haydock 
Academy maintains a learning environment that encourages high expectations for all students and 
has established an academic goal of increased achievement and continuous growth for all students 
in the areas of reading and math. The Accelerated Reader program is utilized school-wide to 
support academic gains. 
 
Haydock Academy promotes a safe, orderly, caring and supportive learning environment that 
prepares students for college and career success. Student well-being is fostered by positive 
relationships with other students and staff through focused community-building efforts. The 
CHAMPS program is utilized throughout the campus to support PBIS and school climate goals. 
 
The Arts program at Haydock provides students with a variety of elective class offerings including 
band, guitar, choir, piano, fine arts, drama, dance and mariachi. Haydock students have access to 
the Oxnard Scholars After School Program (ASP), which offers additional support for students 
throughout the year. The ASP provides academic assistance, enrichment activities, and a year-long 
sports program. 
 
Vision Statement - Empowering, Inspiring, and Motivating Students to Become Creative and 
Productive Global Citizens 
 
Mission Statement - We provide a safe, healthy, positive, and respectful environment where 
creativity, critical thinking, and responsibility are fostered in all students. 
 
 

2017-18 School Accountability Report Card for Richard B. Haydock Academy of Arts and Sciences 

Page 1 of 9