Rancho-Starbuck Intermediate School is located in the unincorporated area of southeast Los 
Angeles County. We are part of the Lowell Joint School District and serve 780 students in the 7th 
and 8th grades. Rancho-Starbuck was constructed in 1958 and has enjoyed continuous upgrades 
and maintenance over the years. Our school offers a well stocked, upgraded library and computer 
technology center with 45 computer stations for individual and classroom use. In addition, our 
campus offers a separate computer lab for student use with 40 computers. Our Science, 
Technology, Engineering, and Mathematics (STEM) lab is complete with 30 computer work stations 
and professional engineering software. Our STEM lab and Art Department use a 3D printer for 
instructional purposes. All classrooms provide chromebooks for students (1 to 1) and are Google 
Classrooms. Our technology is regularly updated including document cameras in every classroom. 
Aeries, a parent/student portal, keeps students and parents regularly updated regarding student 
progress. 
 
The staff at Rancho-Starbuck School currently has 28 fully-credentialed teachers teaching in their 
subject area and three part-time educators. Our school team consists of three STAR resource 
center specialists, one moderate Special Day Class (SDC) classroom, a school psychologist, a part 
time English Language Development (ELD) teacher, a speech and language specialist, a part time 
choral music teacher and an instrumental music teacher. There are two administrators: a principal 
and assistant principal. A full time academic counselor is on staff. Additional support is provided 
by an office manager, school clerk, attendance clerk, a part-time nurse, full-time librarian, three 
STAR Center aides, instructional aides, three custodians, five noon duty aides and 5 days of 
counseling intern support. 
 
Rancho-Starbuck Intermediate is known for its safe and orderly learning environment. The record 
of suspensions/expulsions is low at 1.4% and the school institutes a comprehensive school safety 
program that includes a School Resource Officer. At the same time, the staff places a high priority 
on developing responsible citizens through our Character Education and Responsibility * Effort * 
Attitude * Character * Honor (REACH) programs. Through this program, we emphasize the 
importance of each of these themes and we recognize students for their achievement. In addition 
to Rancho-Starbuck’s strong academic program, we continue to provide outstanding programs in 
technology, music, art, drama, and a variety of elective offerings. Rancho-Starbuck offers a STEM 
program complete with a technology lab with classes in Robotics, Voice Activated Technology, and 
Music Production. Rancho-Starbuck also offers Design and Construction, a course where students 
actually design and build, getting a glimpse of future careers in construction. Rancho-Starbuck is 
proud to offer a Pre AP/IB Computer Science course designed to prepare students for the AP exam. 
Through this class, students gain extensive knowledge in coding and can earn college credit. 
 
Our school offers a variety of leadership opportunities for students through our active ASB 
program. After school curriculum enrichment activities help to keep students involved and 
encourages each student to develop skills and interests including Jazz Band, Drumline and Robotics. 
Additional after school opportunities include Science Olympiad, Junior Achievement, Competitive 
Drum Line, Yearbook and National Junior Honor Society. A mentoring program called REACH Circle 
offers students an opportunity to enjoy a mentor relationship with school staff and an opportunity 
to offer community outreach opportunities. After school sports offers our students another way 
to get involved on campus. A variety of after school sports are offered during three sports seasons. 
Rancho-Starbuck believes in fostering a school wide college atmosphere.

2017-18 School Accountability Report Card for Rancho-Starbuck Intermediate School 

Page 1 of 8 

 

 
All students have an opportunity for success at Rancho-Starbuck. Flex classes offer students the opportunity to have additional instruction 
during the school day. Students are also offered additional enrichment classes once standards are mastered. Learning Targets in core 
subjects help monitor student progress on state standards. Before school tutoring through Power Start is offered to all students. 
Intervention classes during the school day are offered in Math, ELD support for our Language Learners and a reading intervention class 
using Read 180 curriculum. Literacy is promoted across the campus and there is a school wide focus on California State Standards and 
21st century learning with an emphasis on writing skills. The mission statement promotes the development of character, passion and 
academic excellence in all students. These characteristics are promoted in all areas on the Rancho-Starbuck campus. In addition, Honors 
students are offered an opportunity to be part of The Academy and/or the Conservatory of Fine Arts. Through this extension activity 
students will complete an 8th grade Capstone Project where their imagination and creativity can help them soar to new heights. 
 
Rancho-Starbuck is proud to be a California Distinguished School, Gold Ribbon School and a four time recipient of the California Business 
for Education Award of Excellence. Rancho-Starbuck is a 2017 National School to Watch and a 2017 Golden Bell recipient. Our staff is 
dedicated and nurturing; Rancho-Starbuck is a great place to be!