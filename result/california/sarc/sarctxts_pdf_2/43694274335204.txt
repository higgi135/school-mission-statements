Oak Grove High School is a comprehensive public high school (grades 9-12) with 80 teachers on 
staff. It is one of eleven comprehensive high schools in the East Side Union High School District, 
which also has five alternative education schoolsl. Oak Grove High School occupies forty-three acres 
of land in South San Jose and first opened its doors on January 2, 1968. The school is home to 
approximately 1850 students. Oak Grove has an extremely diverse student population with over 
twenty-five major languages represented. 
 
Oak Grove is committed to fostering a safe and inclusive learning environment that provides high-
quality instruction to all students. 

• Our school culture is built on a foundation of mutual respect, compassion and 

dedication to learning. 

• We empower students to perform complex tasks and think critically by redefining the 

roles of teachers and students. 

----
---- 
East Side Union High School 

District 

830 N. Capitol Avenue 

San Jose, CA 95133 

(408) 347-5000 
www.esuhsd.org 

 

District Governing Board 

Frank Biehl 

J. Manuel Herrera 

Van Thi Le 

Pattie Cortese 

Lan Nguyen 

 

District Administration 

Chris D. Funk 

Superintendent 

Glenn Vander Zee 

Assistant Superintendent 

Educational Services 

 

Chris Jew 

Associate Superintendent 

Business Services 

 

Dr. John Rubio 

Associate Superintendent 

Human Resources 

 

 

2017-18 School Accountability Report Card for Oak Grove High School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Oak Grove High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

92 

78 

78.4 

2 

0 

3 

0 

3 

0 

East Side Union High School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

991.5 

50.6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Oak Grove High School 

16-17 

17-18 

18-19