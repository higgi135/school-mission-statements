Ridge Crest, established in 1990, is a TK-5 elementary school. We are dedicated to providing a rich 
educational experience for all of our students and staff as a Professional Learning Community. 
 
Common Core State Standards, frameworks, and texts provide a strong foundation for our 
instructional programs. We receive Title I and LCFF funding. These resources supplement the 
Common Core Curriculum program in areas such as equipment/materials, intervention programs, 
parent involvement, and staff development for student achievement. 
 
Our School Plan for Student Achievement identifies the actions to be taken to raise student 
academic performance and improve the school's educational program. 
 
Vision: 
Ridge Crest School is a school where motivated teachers, staff, parents and administrators, inspire 
students with diverse needs to succeed academically and socially, ensuring that all students are 
college, career ready, and life-long learners. 
 
Mission: 
At Ridge Crest, we are a community of leaders. We recognize, honor, and celebrate the leaders 
within us. 
 
High expectations, a commitment to faithfully teaching the Common Core State Standards and 
exemplary parent support provide a strong base for our instructional program. Our parent groups; 
PTA, AAPAC, and ELAC assist in providing assemblies, field trips, and family activities that help 
connect students to their learning that goes beyond the classroom. 
 
Our Vision for Ridge Crest Elementary School is to work cooperatively as staff, students, parents, 
and community to provide a safe and positive environment; recognizing the uniqueness of all. Our 
commitment is to provide a challenging STEAM curriculum while encouraging continuous learning 
and developing responsible citizens for the 21st Century through career and college readiness. 
 

2017-18 School Accountability Report Card for Ridge Crest Elementary School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ridge Crest Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

22 

22 

26 

0 

0 

0 

0 

0 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ridge Crest Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.