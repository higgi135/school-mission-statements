Jackson Heights Elementary enjoys a school population that is economically, as well as multi-
culturally, diverse. Our school serves approximately 475 students each of whom deserves the best 
possible educational experience we can provide. We are a ‘No Excuses University’ school that 
actively promotes a comprehensive model of college readiness to all students. We have a well-
defined process for identifying and creating the six exceptional systems of a culture of universal 
achievement: collaboration, standards alignment, assessment, data analysis and interventions. We 
work diligently to expose students to powerful college symbolism. This symbolism is seen in the 
college flags and banners hung in every classroom, felt through the close partnerships forged 
between classrooms and universities, and heard in the college chants at each grade level. 
 
The Jackson Heights Elementary School staff is a dedicated group of professionals who share the 
core belief that every child can succeed and reach his/her full potential. Our role is to provide the 
academic and supportive climate to make this possible. The Jackson Heights staff believes that we 
make the difference by creating an environment where all children can grow, develop and be 
successful. 
 
Every student succeeding! Confident, Caring, College Ready! 
 

 

 

----

--

-- 

----

---- 

Red Bluff Union Elementary 

School District 
1755 Airport Blvd. 
Red Bluff, CA 96080 

(530)-527-7200 
www.rbuesd.org 

 

District Governing Board 

Sharon Barrett 

Steven Piffero 

Adriana Griffin 

Heidi Ackley 

Doug Schreter 

 

District Administration 

Cliff Curry 

Superintendent 

Claudia Salvestrin 

Assistant Superintendent 

 

2017-18 School Accountability Report Card for Jackson Heights Elementary 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Jackson Heights Elementary 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

25 

22 

21 

1 

0 

2 

0 

3 

0 

Red Bluff Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

90 

10 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Jackson Heights Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.