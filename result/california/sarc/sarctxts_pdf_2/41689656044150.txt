Hillview Middle School is the sole middle school in Menlo Park City School District, serving over 950 
students in grades 6 - 8. A 1:1 iPad school with a focus on design thinking, competency- and 
mastery-based grading, social emotional learning, and asset development, we focus on the whole 
child in fulfilling our mission, "The Hillview Community inspires and empowers all students to be 
curious and resilient problem solvers, compassionate and constructive contributors, and lifelong 
learners during their individual and collective journey of academic and personal growth." Or school 
goals reflect our district LCAP. As such, we look to improve the academic outcomes of all students, 
especially those who are underrepresented in the UC and Cal State systems. In addition, student 
wellness and parent engagement are of paramount importance. Finally, we are engaged in efforts 
to personalize learning, using both technological platforms and project-based learning as lenses for 
this work. Our Site Council monitors progress of our school goals, and faculty participate in 
Collaboration Around Student Outcome days to review student evidence of learning and respond 
with adjustments to curriculum, interventions, and tighter progress monitoring. W e are proud to 
offer such programs as over 20 lunchtime clubs, strong visual and performing arts electives, a 
vibrant Associated Student Body, a focus on service, restorative practices that co opt students into 
repairing harm to the community, and collaborative structures that allow our teachers to meet 
weekly to discuss students. 
 
 

-

--- 

----

---- 

Menlo Park City Elementary 

181 Encinal Avenue 
Atherton, CA, 94027 

650-321-7140 

district.mpcsd.org 

 

District Governing Board 

David Ackerman 

Caroline Lucas 

Stacey Jones 

Sherwin Chen 

Scott Saywell 

 

District Administration 

Erik Burmeister 
Superintendent 

 

2017-18 School Accountability Report Card for Hillview Middle 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Hillview Middle 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

51 

52 

55 

0 

0 

0 

0 

3 

0 

Menlo Park City Elementary 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

204 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Hillview Middle 

16-17 

17-18 

18-19