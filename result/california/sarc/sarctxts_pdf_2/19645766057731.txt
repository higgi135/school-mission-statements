Principal's Message 
It is an honor to serve as the principal of Robert H. Goddard Middle School, a California 
Distinguished School and Gold Ribbon School, with a tradition of high academic standards 
encompassed by a positive school culture. At Goddard Middle School we believe all students can 
meet high expectations on a daily basis. We focus on the academic, social and emotional success 
of our students. Our goal is to cultivate well-rounded individuals who will become productive 
members of our society and future leaders. 
 
Our academic standards are among the highest in the state and our dedicated teachers and support 
staff are among the best the profession has to offer. Our teachers routinely collaborate to analyze 
data and improve their practice so they can provide rigorous, challenging and engaging lessons to 
our students. Goddard embeds positive recognition and extra-curricular activities throughout the 
entire school community. We continually strive to improve our program and to provide our 
students with many academic and social opportunities. In addition to rigorous curriculum, we offer 
academic clubs, STEAM clubs, athletics, intramurals, AVID, WEB, Honor Society, a fully networked 
technology center, and an exchange program with the city of Moka, Japan. Goddard strives to 
create a culture where every student has opportunities to belong and feels like a member of our 
Goddard family. 
 
We are always willing to answer questions and to listen to suggestions for strengthening our 
program of powerful teaching and learning … “The Goddard Way.” Visit us on the web at 
www.goddardtitans.net and check out our many social media accounts or Goddard Middle School 
smart phone application. 
 
Jennifer Prince 
Principal 
 
Mission Statement 
Goddard Middle School will set high student expectations on a daily basis, focusing on student 
proficiency in California State Standards and curriculum. We will strive to cultivate well-rounded 
individuals who will be productive members of our society, and we are committed to supporting 
our students’ efforts toward accomplishing these goals by providing a safe and supportive learning 
environment. 
 

2017-18 School Accountability Report Card for Goddard Middle School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Goddard Middle School 

With Full Credential 

Without Full Credential 

16-17 17-18 18-19 

43 

0 

43 

1 

42 

0 

Teaching Outside Subject Area of Competence N/A 

N/A 

N/A 

Glendora Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

335 

5 

N/A 

Teacher Misassignments and Vacant Teacher Positions at this School 

Goddard Middle School 

16-17 

17-18 

18-19