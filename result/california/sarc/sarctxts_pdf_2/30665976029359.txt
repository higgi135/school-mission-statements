Kaiser Elementary School, home of the Knights, is centrally located in Costa Mesa. The school serves 
approximately 670 students in grades three through six. Housing in our area is diverse, consisting 
of single-family homes, apartment homes, and motels in the cities of Newport Beach and Costa 
Mesa. Second-grade students "graduate" from nearby Woodland Elementary School and begin 
attending Kaiser in grade three. After students complete sixth grade at Kaiser Elementary School, 
they attend Horace Ensign Intermediate School for grades seven and eight, and then Newport 
Harbor High School. 
 
In addition to traditional and essential instruction associated with elementary school, students at 
Kaiser experience enriched music and STEM programming. We offer chorus and orchestra in 
addition to general music instruction. The Kaiser-Woodland Schools Foundation underwrites a 
STEM teacher whose current emphasis is on engineering and the engineering design process. As a 
1:1 Chromebook school, students learn in a technologically enhanced environment. Kaiser supports 
an active after school robotics program along with other school- and PFO-sponsored enrichment 
activities. Many Kaiser students also participate in after school robotics competitions and the 
Science Olympiad. Our school benefits tremendously from a strong community of caring parents 
who support a variety of enrichment opportunities throughout the school year. As a Positive 
Behavioral Supports and Interventions (PBIS) school, students focus on Common sense, 
Accountability, Respect, and the idea that Everyone matters - At Kaiser, we C.A.R.E.! 
 
Kaiser's vision: All Kaiser students will be academically prepared, personally successful, and ready 
to pursue their dreams. To achieve this vision, we subscribe to the following mission: Kaiser 
teachers and staff provide the highest levels of instruction to students to maximize future academic 
and life opportunities. Teachers continually learn and add to their "tool kits" to provide students 
with outstanding instruction. 
 
 
 
 

----

---- 

Newport-Mesa Unified School 

District 

2985 Bear St. 

Costa Mesa, CA 92626 

(714) 424-5033 
www.nmusd.us 

 

District Governing Board 

Charlene Metoyer, President 

Martha Fluor, Vice President 

Dana Black, Clerk 

Ashley Anderson, Member 

Michelle Barto, Member 

Vicki Snell, Member 

Karen Yelsey, Member 

 

District Administration 

Dr. Frederick Navarro 

Superintendent 

Mr. Russell Lee-Sung 

Deputy Superintendent, 
Chief Academic Officer 

Ms. Leona Olson 

Assistant Superintendent, 

Human Resources 

Mr. Tim Holcomb 

Assistant Superintendent, 

Chief Operating Officer 

Dr. Sara Jocham 

Assistant Superintendent, 

Student Support Services/SELPA 

Mr. Jeff Trader 

Executive Director, 

Chief Business Official 

Dr. Kirk Bauermeister 

Executive Director, 
Secondary Education 

Dr. Kurt Suhr 

Executive Director, 

Elementary Education 

 

 

2017-18 School Accountability Report Card for Kaiser Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Kaiser Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

32 

32 

32 

0 

1 

0 

1 

0 

1 

Newport-Mesa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1105 

5 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

Kaiser Elementary School 

16-17 

17-18 

18-19