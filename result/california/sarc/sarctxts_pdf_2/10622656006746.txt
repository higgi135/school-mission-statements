School Description Our students will learn in a safe and nurturing environment where they will be 
challenged to meet the rigorous demands needed to be successful in college and future careers. 
Citrus serves 676 students in grades 6-8 and includes a teaching staff of 27 teachers. It is one of 
three middle schools in Kings Canyon Unified School District. Other middle school students are 
served in four K-8 schools. 
 
School Mission Statement 
The mission of Citrus Middle School, in partnership with the family and community, is to provide a 
rigorous and positive learning environment. All students are expected to meet district, state, and 
common core standards to achieve personal and academic growth. We commit to providing a 
comprehensive system of individualized support, interventions and opportunities to ensure that all 
students can succeed. 
 

 

 

----

-

--- 

----

---- 

Kings Canyon Joint Unified 

School District 

1801 10th Street Reedley CA. 

Reedley, CA 93654 

559.305.7010 

www.kcusd.com 

 

District Governing Board 

Craig Cooper 

Robin Tyler 

Manuel Ferreira 

Noel Remick 

Sarah Rola 

Clotilda Mora 

Jim Mulligan III 

 

District Administration 

John Campbell 
Superintendent 

Roberto Gutierrez 

Deputy Superintendent, Human 

Resources 

Monica Benner 

Assistant Superintendent, 
Curriculum and Instruction 

Mary Ann Carousso 

Administrator, Student Services 

Jose Guzman 

Administrator, Educational 

Programs 

Adele Nikkel 

Chief Financial Officer 

 

2017-18 School Accountability Report Card for Citrus Middle School 

Page 1 of 8