South Ranchito Dual Language Academy is located in Pico Rivera, California and has been a 
landmark in the Pico Rivera community for many years. Demographic information for South 
Ranchito Dual Language Academy consists of 539 students enrolled, which 87% are from a low 
socioeconomic status. The school has a population of 41% English learners. Of the 495 students 
enrolled at South Ranchito, 488 are Hispanic or Latino. 
 
Our classes consist of general education classrooms, Dual Language immersion classrooms, and 
our Transitional Kindergarten class, and we also have three Autism Focus classes servicing students 
in K-5. South Ranchito Dual Language Academy offers a strong curriculum focused on the California 
Common Core State Standards. Reading, English Language Development, Writing, Mathematics, 
Science, Social Studies, and physical education provide the foundation for student success. We 
implement Project-Based Learning to enrich 21st Century Skills. 
 
School Mission Statement 
The mission of South Ranchito Dual Language Academy, in partnership with the community, is to 
provide a quality education that encourages creative and critical thinking in a supportive 
environment. We are committed to providing an enriched curriculum with high expectations for all 
students which emphasizes the skills, concepts and processes necessary for the technological and 
cultural challenges of the 21st century. Our commitment is to prepare students to be productive 
citizens and lifelong learners in a culturally diverse and a technologically advanced society. 
 
 
 

----

--

-- 

El Rancho Unified School District 

9333 Loch Lomond Dr. 
Pico Rivera, CA 90660 

(562) 801-7300 
www.erusd.org 

 

District Governing Board 

Dr. Aurora R. Villon 

Gabriel A. Orosco 

Lorraine M. De La O 

Dr. Teresa L. Merino 

Jose Lara 

 

District Administration 

Karling Aguilera-Fort 

Superintendent 

Mark Matthews 

Assistant Superintendent, Human 

Resources 

Jacqueline A. Cardenas 

Assistant Superintendent, 

Educational Services 

 

Dora Soto-Delgado 

Director, Student Services 

Reynaldo Reyes 

Director, Alternative/Adult 

Education 

Dean Cochran 

Director, Special Education 

Roberta Gonzalez 

Director, Early Learning Program 

 

2017-18 School Accountability Report Card for South Ranchito Dual Language Academy 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

South Ranchito Dual Language Academy 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

22 

22 

22 

1 

0 

0 

0 

0 

0 

El Rancho Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

355 

13 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.