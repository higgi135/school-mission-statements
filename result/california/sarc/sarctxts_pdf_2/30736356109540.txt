The mission of Cielo Vista Elementary School is to celebrate each student and promote excellence 
through a safe learning community that empowers each student to achieve success and centers on 
respect for all aspects of the learning program. 
 
 At Cielo Vista students will become: 

Effective Communicators 

• Respectful & Responsible Citizens 
• 
• Collaborative Workers 
• 
Independent Learners 

 
Our Vision: 
Students will successfully progress in becoming college and career ready and demonstrate the 21st 
century skills of critical thinking, communication, collaboration, and creativity. The environment at 
Cielo Vista will encourage effective interaction between student, teacher and parents. Cielo Vista 
focuses on intentional lesson design, engaging strategies, and developing innovative practices that 
create successful students. We believe in creating a learning environment that guides students to 
develop their maximum potential in academic, physical, and social areas of individual growth. Cielo 
Vista supports the use of multiple assessments to recognize, monitor, report and improve student 
achievement. Collaborative analysis of school assessment data will guide the systematic planning 
for intervention, remediation, and acceleration. 
 
Parents, teachers, and staff work together to create programs that meet district and state 
standards. Our staff takes pride in supporting all of our students. We have received training as a 
professional learning community, and we look at student data to guide our instructional programs. 
We provide a range of services for students at all levels, including programs for English Learners 
and academically gifted students. 
 
Cielo Vista is an educational, social, and cultural hub for our community. We focus on providing a 
safe and effective learning environment through HAWKS, a comprehensive school-climate 
program. We offer programs before, during, and after school, including the Parent Teacher 
Association (PTA) , Meet the Masters, Student Council, Peer-Assistance Leadership (PAL), and a 
variety of enrichment programs in different subject matters. Cielo Vista is an AVID (Advancement 
Via Individual Determination) certified school that prepares our students to be college and career 
ready. We believe in building resiliency in all students promoting student voice and choice 
throughout the TK-6 educational experience. Cielo Vista also offers STMath and Reading Eggs to 
extend our on-line instruction into the home and our Accelerated Reader Club to foster lifelong 
literacy. As our school teaches the California Common Core Standards, we pride ourselves in 
preparing Cielo Vista students to succeed in the 21st century! 
 
Fran Hansell, Ed.D., PRINCIPAL 
 

----

-

--- 

Saddleback Valley Unified School 

District 

25631 Peter A. Hartman Way 

Mission Viejo CA, 92691 

(949) 586-1234 
www.svusd.org 

 

District Governing Board 

Suzie Swartz, President 

Dr. Edward Wong, Vice President 

Amanda Morrell, Clerk 

Barbara Schulman, Member 

Greg Kunath, Member 

 

District Administration 

Crystal Turner, Ed D. 

Superintendent 

Dr. Terry Stanfill 

Assistant Superintendent, Human 

Resources 

Connie Cavanaugh 

Assistant Superintendent, Business 

Laura Ott 

Assistant Superintendent, 

Educational Services 

Mark Perez 

Director, Communications and 

Administrative Services 

Dr. Ron Pirayoff 

Director, Secondary Education 

Liza Zielasko 

Director, Elementary Education 

Dr. Diane Clark 

Director, Special Education 

Rae Lynn Nelson 
Director, SELPA 

Dr. Francis Dizon 

Director, Student Services 

 

2017-18 School Accountability Report Card for Cielo Vista Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cielo Vista Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

30 

33 

29 

0 

0 

0 

0 

0 

0 

Saddleback Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1,157 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Cielo Vista Elementary School 

16-17 

17-18 

18-19