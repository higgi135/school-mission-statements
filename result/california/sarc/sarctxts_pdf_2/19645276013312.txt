Principal’s Message 
The purpose of the School Accountability Report Card is to provide students and parents with 
information about our school’s instructional programs, academic achievement, materials, facilities, 
and staff. Durfee Elementary School continues to have the privilege of serving a community that is 
very supportive of the school’s vision to provide a quality, well-rounded education to every child. 
 
Parents and the community play an important role in our school by volunteering for morning drop-
off duty, in the classroom, contributing to fundraisers, and supporting the school’s programs. The 
school staff strives to inform the community about the school’s educational program, student 
achievement, and curriculum development. 
 
Students at Durfee Elementary School are guided by specific rules and classroom expectations that 
promote the six pillars of Character Counts: Trustworthiness, Respect, Responsibility, Fairness, 
Caring, and Citizenship. Our goal is not only to encourage student achievement, but to also provide 
students with opportunities to learn self-discipline through a system of consistent rewards and 
consequences for their behavior. The staff has been trained and holds weekly class discussions 
regarding the school’s/ district’s bullying policy, how to treat others and, above all, that their 
“Character Counts Everywhere, All the Time!” Durfee Elementary participates in the Positive 
Behavior Intervention and Supports program. It is now going on its second year. Durfee received 
the Silver PBIS Award in 2017 and 2018 
 
We have made a commitment to provide the best educational program possible for all students at 
Durfee Elementary School. We are a dedicated staff who is highly committed to providing a 
stimulating environment where students are actively involved in the learning process as well as 
promoting positive values. Together, through our hard work, our students will be challenged 
through their potential. 
 
Durfee Elementary School’s Vision and Mission Statements 
 
Durfee Elementary School Mission Statement 
The mission of Durfee Elementary School, is to instill in each child a sense of self-worth, 
independence, and responsibility which will 
enable each student to become a life–long learner, a successful citizen, and be able to reach his or 
her potential. Durfee’s teachers 
are committed to providing all students with a positive learning experience that is developmentally 
appropriate, promotes safety 
and responsibility, is supported by all necessary staff, materials and equipment, and welcomes 
parents as an integral part of the 
learning process. 
 
Durfee Elementary School Vision Statement 
Durfee Elementary will provide an educational program that will promote the Pillars of Character 
and create a partnership with 
parents, community and staff. We will actively engage all students in developing 21st century skills 
including critical thinking, 
problem solving, creativity and collaboration to ensure college and career readiness. 
School 

2017-18 School Accountability Report Card for Durfee Elementary School 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Durfee Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

16 

16 

16 

0 

0 

0 

0 

0 

0 

El Rancho Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

355 

13 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Durfee Elementary School 

16-17 

17-18 

18-19 

 

 

 
Sam Genis, Principal