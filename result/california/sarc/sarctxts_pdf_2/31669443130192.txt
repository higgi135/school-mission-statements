Cold Stream Alternative School serves high school students both in Tahoe City and in Truckee. Cold 
Stream offers an innovative program developed to meet the needs of families who want more 
choices and flexibility in their child's education. We provide small class sizes and personal 
instruction along with a rigorous, yet flexible schedule that allows students to work from home or 
while traveling. Cold Stream offers a project-based, hands-on curriculum. This program gives 
students the flexibility to design an educational path that works for their needs. Students meet half 
days on campus Monday-Thursdays, so that they can also take classes at Sierra College or at one of 
Tahoe Truckee Unified School District traditional high schools. Our program affords flexibility for 
students to pursue other interests or to participate in extra-curricular activities or sports. 
 
Climate for Learning 
At Cold Stream Alternative we value student voice. Opportunities for input are provided through 
student clubs, community service, sports, student assistance programs, and the development of 
healthy living practices. 
 
Students at Cold Stream Alternative are guided by specific rules and classroom expectations that 
promote respect, cooperation, courtesy, and acceptance of others. The school’s discipline 
philosophy promotes a safe school, a warm, friendly classroom environment, and demonstrates 
that good discipline is a solid foundation on which to build an effective school. Students and 
teachers worked together to determine CSA's core values of respect, responsibility, team work, and 
fun, which serve as the foundation for the school’s educational and social climate. School rules are 
shared with students and parents in the student handbook and are reinforced throughout the year 
at assemblies, on the school website, in school publications, and through parent-teacher 
conferences. 

2017-18 School Accountability Report Card for Cold Stream Alternative 

Page 1 of 9 

 

School Leadership 
School Leadership at Cold Stream Alternative is a responsibility shared among District administration, the principal, instructional staff, 
students, and parents. Staff members are encouraged to participate on various committees that make decisions regarding the priorities 
and direction of the educational plan. These committees ensure that instructional programs are consistent with students’ needs and 
comply with district goals. Avenues of opportunity include: Student Study Team, Leadership Team, Safety Committee, English Learner 
Advisory Committee (ELAC), and School Site Council.