Hendrick Ranch is a TK-5 elementary school. Established in September, 1989, Hendrick Ranch 
students were housed at a temporary site. The school moved to its present site in September, 1990. 
There are currently 23 general education classes, all of which are SEI and include one transition 
Kindergarten (TK) class, four kindergarten classes and 18 first through fifth grade classes. 
Additionally, Hendrick Ranch offers 2 special day classes in 2/3 and 4/5 for students requiring 
special education services. The following programs are also provided: Title 1 tutoring, speech, 
adapted PE, occupational therapy services; resource specialist services, English language 
development, and counseling. The dominant language is English; however, 7 other languages are 
represented by students at Hendrick Ranch School. 
 
Hendrick Ranch Elementary School is committed to the vision that all students will leave fifth grade 
with the skills necessary to achieve at the highest levels in middle school and high school. Our moto 
is “Building Leaders Through Equity” as we build our 7 Husky Habits. 
 
Hendrick School’s mission is to provide an environment where students, staff, and community are 
involved in a positive, supportive manner to develop and maintain successful educational 
opportunities. The parents, students, and staff of Hendrick Ranch School will collaborate to: 

• Achieve excellence in student performance 
• Respect individual differences and the rights of others 
• Develop student acceptance of personal responsibility in the educational process and 

in their everyday lives 

• Promote pride in the school’s achievement of high standards, both academic and 

personal 
Encourage students to work to their fullest potential 

• 
• Promote good citizenship 

----
---- 
Moreno Valley Unified School 

District 

25634 Alessandro Blvd 

Moreno Valley, CA 92553 

(951) 571-7500 
www.mvusd.net 

 

District Governing Board 

Susan Smith, President 

Jesus M. Holguin, Vice-President 

Cleveland Johnson, Clerk 

Gary E. Baugh. Ed.S., Member 

 

District Administration 

Martinrex Kedziora, Ed.D. 

Superintendent 

Maribel Mattox 

Chief Academic Officer, 

Educational Services 

Tina Daigneault 

Chief Business Official, Business 

Services 

Robert J. Verdi, Ed.D. 

Chief Human Resources Officer, 

Human Resources 

 
 

 

2017-18 School Accountability Report Card for Hendrick Ranch Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Hendrick Ranch Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

25 

24 

27 

0 

0 

0 

0 

0 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.