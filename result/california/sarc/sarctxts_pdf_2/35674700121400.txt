The Accelerated Achievement Academy (AAA) is an extraordinary learning environment for 
students and a great partner with our families and San Benito Community. 
 
The AAA is one of the highest achieving schools in the state. Our 4th and 5th graders are the highest 
performing students in San Benito County. Our middle school was the 5th highest achieving school 
in California! Our elementary school was the 14th highest achieving school in California. AAA 
earned a California Distinguished School Award in 2013. The Educational Results Partnership uses 
nationally normed student assessment data to recognize AAA several times as a top performing 
California Honor Roll STAR School. 
 
The AAA is not resting on credentials or awards. Our teachers are using evidence based 
professional development and instructional practices to grow our students even more. We have 
embraced Project-Based Learning school-wide and are daily pushing students for deeper and more 
complex understandings of the world and their place in it. That does not mean assigning more 
homework, but using the best research based instructional practices in education*. 
 
The AAA staff embraces bright and high achieving students and helps grow them into successful, 
kind and compassionate people. We are building our relational skills to raise the social and 
emotional capacity of every student. Our entire school trained this August in Capturing Kids' Hearts 
(CKH) with the Flippen Group. Capturing Kids' Hearts is a national program that strengthens 
student connections to each other and their teachers. Students and teachers build Social Contracts 
(agreement of behavior) that are used daily in and out of the classroom. 
 
This year we are striving to be even better partners with our families and the community. We are 
working closely with our Parents' Club to send our students to world class learning centers and 
cultural institutions to expand their horizons. We are also building strong partnerships with local 
businesses and high school to gain access to hands on science and labs. 
 
The future of the AAA is bright. Our students, families, teachers and community are all committed 
to making advanced students grow even more and our aim is to make AAA the best school in the 
world! 
 
 

2017-18 School Accountability Report Card for Accelerated Achievement Academy 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Accelerated Achievement Academy 

16-17 17-18 18-19 

Teacher Credentials 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Hollister School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

3 

2 

0 

5 

0 

0 

6 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

240 

35 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.