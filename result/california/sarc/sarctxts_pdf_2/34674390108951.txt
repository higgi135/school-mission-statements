Arthur A. Benjamin Health Professions High School is a NAF Certified and Linked Learning Certified 
small innovative high school in the Sacramento City Unified School District. The school opened in 
2005 with support from the Bill and Melinda Gates Foundations, the Carnegie Foundation, the 
James Irvine Foundation and the California Endowment. The school moved into a new 17 million 
dollar facility in 2006. The school is centered around the implementation of a national health 
sciences curriculum across all of the core and elective classes in the school. Teachers are not only 
experts in their content area but they receive specialized training to teach using project-based units 
that integrate healthcare topics with Common Core State Standards. Students at Health Professions 
access an education that sets the bar high for coursework and goes beyond the content areas to 
bring in real life learning connected to their classroom. This integrated curriculum combines 
academics and the health sciences helping to engage students by demonstrating real life 
applications through the use of healthcare issues, practices and processes. In addition to learning 
about the importance of high academic and behavioral achievement, students also have numerous 
opportunities to work side by side healthcare professionals and visit healthcare settings through 
career explorations, internships, and job-site tours. These experiences provide insight into 
available careers, work expectations and educational requirements. In order to achieve these goals 
the school partners with regional health care providers and post-secondary partners like UC Davis 
Medical Center, Kaiser Permanente and Sacramento City College Allied Health Department, just to 
name a few. Students interact with professionals active in health care fields to understand 
professional expectations and gain experience in the field. The school has a strong Dual Enrollment 
partnership with Los Rios Community College District to engage students in completing college 
courses before they graduate from high school. 
 
Students who graduate from Health Professions do so with a plan for life after high school and 
experiences that will prepare them for the demands of college and the working world. Our mission 
is: To provide students with an outstanding education, rich with relevant academic, application and 
leadership experiences - using healthcare as a theme. 
 
 

-

--- 

----

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Chad Sweitzer 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for Arthur A. Benjamin Health Professions High School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Arthur A. Benjamin Health Professions High 
School 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

9 

2 

1 

11 

17 

1 

0 

1 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.