A Message from the Principal... 
We are proud of Niemes Elementary. The administration and staff welcome this opportunity to 
provide you a glimpse of what we have to offer and to tell you more about our educational program. 
 
Niemes Elementary is located in the City of Artesia, and hosts one of the largest elementary school 
enrollments in the ABC Unified School District. Niemes Elementary has been honored as a 
California Gold Ribbon school, CA Title 1 Academic Achievement School, as well as a Magnet School 
of Distinction by Magnet Schools of America, the federal magnet school governing association. 
Niemes has both a comprehensive environmental science and technology program as well as one 
of the few Award-winning Spanish-English Dual Immersion programs in the area. Niemes offers 
numerous after-school programs and activities, including an After School YMCA program, Track 
Club, Green Kids, Gardening Club, Botanical Society, Extended Day Care Program, Intervention 
Tutoring, as well as various activities and clubs throughout the school year. Our Environmental 
Nature Center allows students to learn about native California habitats and "green" activities. 
 
It is not often that you find elementary students taking nature walks to identify plant life and parts, 
conducting on-going successional garden surveys in a nature habitat with wireless technology, 
investigating rocks and minerals, nurturing and raising plants and live animals while studying their 
development, and participating in “green” or recycling projects and after-school clubs. Here at 
Niemes, Environmental Science comes to life. The students at Niemes Elementary School have 
successfully demonstrated academic achievement all the while keeping with our focus of 
environmental science and technology. Students make connections between decisions we make 
today and the ramifications they have on the environment tomorrow and into the future. “We’re 
Green” is our school motto, and is embedded into our school culture. In an era when teachers find 
less and less time available for science instruction, students at Niemes attend science lessons in a 
state of the art, hands-on environmental science lab in addition to environmental science 
instruction across all curriculum areas. To prepare students for a future filled with continuous 
advances in technology, the elementary school has a modern computer lab with Mac computers, 
while each classroom has a projection system, technology centers and access to mobile computer 
labs containing enough wireless laptops and iPads for each student to research and produce 
authentic multimedia projects at the same time. Technology is integrated into the everyday 
curriculum, including a school-wide online reading program (Accelerated Reader). Niemes has been 
awarded the Verizon Innovative Schools Grant! Only 12 schools were awarded in the United States, 
and 1 school in California. Our teachers and staff have attended and been provided professional 
development to successfully implement this new technology. All 5th & 6th-grade students and staff 
are issued a device to expand educational opportunities. 3rd and 4th-grade students have 2:1 
access to devices. Niemes is the leader in technology in ABC; we have adopted the first 1:1 device 
initiative and hosted various guests to learn about what we are doing in our classrooms. We are 
excited to move forward in this endeavor and lead this innovative program. 
 

2017-18 School Accountability Report Card for John H. Niemes Elementary 

Page 1 of 9 

 

I am pleased to announce that Niemes Elementary has been awarded the Golden Bell Award sponsored by the California School Boards 
Association for our Environmental Science Fair and Festival Program. This is one of the most prestigious awards honoring outstanding 
programs in California. Only about 45 programs are recognized in all of California. Additionally, our Dual Language program was 
nominated to be in the top 10 for an Award of Excellence from the Spanish Embassy. Our Dual Language Program was awarded the Seal 
of Excellence from CABE (California Association for Bilingual Education) for an outstanding program, only 1 school was recognized in 
California. 
 
Niemes has officially been awarded the California Physical Activity and Fitness award by the CDE, as well as the bronze and gold level 
recognition by the Alliance for a Healthier Generation, Red Cross Association and the Clinton Foundation. Niemes Elementary is now a 
Nationally Recognized Healthy School of America. Our School Site Council has implemented a School Wellness Policy to promote our 
movement. We have also partnered with our PTA to provide nutrition experiences. 
 
The staff and I value working closely with families to develop high functioning, responsible youngsters that have knowledge of current 
environmental issues. Students at Niemes are held to high standards, in addition to these morals and values taught. PTA has grown well 
over the last 10 years, and now sponsors and organizes various school events including the fall festival, 1K, Red Ribbon week, Kindness 
week, movie nights, dances, help for the needy, teacher appreciation luncheons, and helps support student incentives and school 
programs. Teachers are continuously involved in staff development, data analysis and team meetings to make instructional decisions as 
well as to keep up to date with current trends, recent research, educational practices and Common Core Standards. 
 
 
Niemes Elementary School of Environmental Science and Technology, located in the city of Artesia opened in 1962. We serve over 700 
students in grades kindergarten through six on a traditional calendar system. Niemes Elementary School is dedicated to ensuring the 
academic success of every student and providing a safe and comprehensive educational experience. We are a magnet school of 
environmental science and technology with full-time science and computer teachers on campus who serve K-6th-grade students. We 
offer both a traditional English Program, as well as a K-6 Dual Language Program with a focus to promote bi-literate 6th-grade students 
in English and Spanish. To accommodate working parents, Niemes has two child care programs (YMCA and Extended Day Care). 
 
John H. Niemes Elementary is a school community where the highest expectations are maintained. Niemes utilizes all available resources 
to enable students to become life-long learners who possess the ability to achieve their utmost potential. We promote a safe, nurturing, 
and stimulating environment that invites students to actively participate in educational opportunities. Niemes provides a welcoming 
atmosphere in which parents and community members are encouraged to become immersed in student's educational endeavors. 
Individual academic and social needs of staff and students are met creating a desirable environment where all participants are successful. 
 
 
Mission Statement 
Our Mission Statement reflects our high expectations and parent partnerships for all of our students. The staff and parents of Niemes 
Elementary Magnet School of Environmental Science and Technology are committed to providing an effective instructional program that 
will equip our students to be productive citizens in a culturally diverse society. We will encourage continuous individual development with 
an emphasis on Environmental Science and Technology. Our vision is to provide a challenging and innovative learning environment that 
promotes excellence and empowers students with a high-quality education, focused on the needs of the 21st century.