Sunnymead Elementary School is a K-5 school. We offer Structured English Immersion (SEI), 
Alternative Course of Study (ACS) (bilingual), English Language Mainstream (ELM), as well as Gifted 
and Talented Education (GATE) cluster classes. The new school site opened in August 2006. 
 
Mission Statement and Goals 
The mission and vision for Sunnymead Elementary School is to hold high expectations for all 
students and to identify essential factors for educational success at all levels. Sunnymead 
Elementary School emphasizes the importance of including equity as a criterion for excellence in a 
diverse population of learners. We commit to promoting fairness, positive interactions, and 
personal safety while fostering the development of student's analytical and critical thinking skills. 
Our vision is to increase student learning as determined by the interaction between home, school, 
and the community in an environment where teachers will utilize research-based, proactive 
instructional strategies and resources to support high expectations. WE DREAM, WE BELIEVE, WE 
ACHIEVE; WE ARE SHINING STARS. 
 
By building and maintaining a partnership amongst our staff, parents, and community members, 
we will continue to excel and improve Sunnymead Elementary. With this goal in mind, I wish to 
extend an invitation to all our parents to be part of their child's education by becoming actively 
involved in Parent Teacher Association (PTA), English Language Advisory Committee (ELAC), School 
Site Council (SSC), African American Parent Advisory Council and other school events. I assure you 
that through this participation, you will feel more connected to the school and our students. 
 
We have a strong, dedicated staff that is here to help as well as to keep you informed. Please feel 
free to contact your student's teacher as well as the front office regarding concerns, information, 
or comments by calling (951) 571-4680. 
 

----
---- 
Moreno Valley Unified School 

District 

25634 Alessandro Blvd 

Moreno Valley, CA 92553 

(951) 571-7500 
www.mvusd.net 

 

District Governing Board 

Susan Smith, President 

Jesus M. Holguin, Vice-President 

Cleveland Johnson, Clerk 

Gary E. Baugh. Ed.S., Member 

 

District Administration 

Martinrex Kedziora, Ed.D. 

Superintendent 

Maribel Mattox 

Chief Academic Officer, 

Educational Services 

Tina Daigneault 

Chief Business Official, Business 

Services 

Robert J. Verdi, Ed.D. 

Chief Human Resources Officer, 

Human Resources 

 
 

 

2017-18 School Accountability Report Card for Sunnymead Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Sunnymead Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

29 

30 

34 

0 

0 

0 

0 

0 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Sunnymead Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.