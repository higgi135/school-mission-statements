Tahoe Truckee High School is a learning community that offers students rigorous curriculum 
through collaborative teaching. Students are encouraged to challenge themselves in their 
academics and in extra-curricular activities. Our school community is actively involved and 
supportive of our students’ social and cultural growth as lifelonglearners. We believe students at 
Truckee High School come to school as special human beings, unique in their own physical, social, 
intellectual and emotional development. We believe that students have the right to an 
environment, that encourages student development through a rigorous, relevant, and relationship 
based learning opportunities. An environment that is safe, clean and drug free, celebrates social, 
emotional, academic, and athletic achievement and where people are treated equally, fairly, 
respectfully and courteously. To ensure these rights, students will be encouraged and supported to 
develop into responsible global citizens so they may reach their full potential, strive to acquire an 
understanding that learning is a lifelong process and achieve academic excellence by developing 
skills necessary to participate successfully in society as responsible citizens with character and 
integrity. Stand for integrity, honesty, and ethical treatment of all, be partners in the learning 
process and take responsibility for themselves and their learning. 
 
Vision Statement: 
Truckee High School will be a safe, healthy and collaborative educational environment where staff, 
students, parents, and community contribute to the academic, social/emotional, and physical 
development of our students. The school will provide a variety of learning opportunities through 
rigorous coursework and co-curricular activities in a culture of connected relationships. Students 
will be thoughtful, resilient, productive and contributing members of their communities who take 
pride in their school. Parents will be engaged, involved, and supporting partners of the school 
community. The community will partner with THS to support students and offer them life skills to 
be successful in a changing world. 

2017-18 School Accountability Report Card for Tahoe Truckee High School 

Page 1 of 11 

 

Mission Statement 
The mission of the educational community at Truckee High School is to inspire, prepare and empower students for college, career and a 
purposeful life. 
 
Climate for Learning 
At Truckee High School we value student voice. Opportunities for input are provided through student government, clubs, community 
service, sports, student assistance programs, and the development of healthy living practices. 
Students at Truckee High School are guided by specific rules and classroom expectations that promote respect, cooperation, courtesy, 
and acceptance of others. The school’s discipline philosophy promotes a safe school, a warm, friendly classroom environment, and 
demonstrates that good discipline is a solid foundation on which to build an effective school. Students and teachers worked together to 
determine THS's core values of respect, responsibility, team work, and fun, which serve as the foundation for the school’s educational 
and social climate. School rules are shared with students and parents in the student handbook and are reinforced throughout the year at 
assemblies, on the school website, in school publications, and through parent-teacher conferences. 
 
School Leadership 
School Leadership at Truckee High School is a responsibility shared among District administration, the principal and assistant principal, 
instructional staff, students, and parents. Staff members are encouraged to participate on various committees that make decisions 
regarding the priorities and direction of the educational plan. These committees ensure that instructional programs are consistent with 
students’ needs and comply with district goals. Avenues of opportunity include: Coordinated Care Team, Student Study Team, Leadership 
Team, Safety Committee, English Learner Advisory Committee (ELAC), and School Site Council.