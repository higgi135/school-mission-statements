Gale Ranch Middle School, home of the Gryphons, was established in 2008 and serves the 
Dougherty Valley communities of the San Ramon Valley Unified School District. It is the newest of 
eight middle schools in the district and currently employs 59 certificated and 19 classified personnel 
to serve 1,262 sixth, seventh and eighth grade students enrolled for the 2018/2019 school year. 
 
Gale Ranch Middle School opened August 25, 2008 with 6th and 7th graders and completed our 
first year on June 11, 2009. Our staff, students and parent community work very hard to achieve 
not only academic excellence, but overall good citizenship through our active character building 
activities and programs. The school supports cultural awareness on a daily basis through its diverse 
literature selections and community involvement. To further this endeavor, we have worked 
extensively with Dr. Sharroky Hollie to become more proficient at cultural responsiveness. 
 
We are proud of our satellite English Learner program that began in 2013. The program targets 
Limited English Proficient students who are at the beginning, early intermediate and intermediate 
levels as identified by the California English Language Development Test (CELDT). Students are 
clustered in ELD support classes as well as in Specifically Designed Academic Instruction in English 
(SDAIE) classes for core academics. Case managers track and monitor the progress of each student. 
So far, we have seen significant progress with students within the program. 
 
Gale Ranch Middle School staff foster individual student achievement by making collaboration a 
top priority. Collaboration occurs on a myriad of levels: department level; grade level; school staff 
leadership level (e.g Principal's Cabinet); and also among students. Responding to the needs of our 
students is a top priority and our RtI Tutorial Program is one way in which we accomplish this. Each 
week we have two, 35 minute periods dedicated to the re-teaching of skills for all students not 
showing mastery. Other students can self-select a Tutorial offering for which they want to gain a 
deeper understanding of the curriculum. 
 
Gale Ranch Middle School is also preparing to educate students in the 21st century through an 
expanded focus on technology. Community partnerships with organizations such as the PTSA and 
the Education Fund have worked with our administration to provide technology, such as IPads, 
Google Chrome Books and other devices to further enhance our students' educational experiences. 
Teachers at Gale Ranch have consistently been proactive in being learners in this ever-changing 
technological world we live in and our students consistently reap the benefits. 
 
Gale Ranch Middle School also has an expanded electives program which boasts advanced tech and 
robotics classes; baking, and language classes that include both beginning and advanced for French, 
Spanish and Mandarin. 
. 
Gale Ranch is proud to be named a California Golden Ribbon School in 2015 and a California 
Distinguished School for the 2012/2013 school year. 
 
 

2017-18 School Accountability Report Card for Gale Ranch Middle School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Gale Ranch Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

53 

52 

50 

1 

0 

1 

0 

3 

0 

San Ramon Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1525 

56 

18 

Teacher Misassignments and Vacant Teacher Positions at this School 

Gale Ranch Middle School 

16-17 

17-18 

18-19