Hurd Barrington was built in 2011 and is one of four Transitional Kindergarten through fifth grade 
elementary schools in the Newman-Crows Landing Unified School District. Our enrollment is 
approximately at 484. Our demographics includes nineteen General Education classes, one 
Resource class and one Special Day class. The demographics of our student body consists of 68.3% 
Free/Reduced School Lunch, 70% Socioeconomic Disadvantaged, 36% English Language Learners, 
and 8% Students with Disabilities. 
 
We have a multipurpose room with a stage that is used as our main cafeteria and is shared by 
Afterschool program for academic supports and enrichment activities. We have a STEAM lab that 
is opened to teachers and students to teach robotics and STEAM standards. Teachers have a 
designated block of library time that is dedicated to encourage reading. Students get to check out 
books as well as get to listen to popular books read to them by our librarian. Our library is also open 
to students before school, recess, lunch and after-school so that students are given the opportunity 
to read and take Accelerated Reading (AR) tests and simply socialize with peers to build confidence. 
Along with classroom P.E. time, students at Barrington get one day of P.E. time by our district P.E. 
team that focuses on physical fitness standards and nutrition. In addition, we have a conference 
room in the main office that is designated to hold our Professional Learning Communities (PLC's) 
meetings, Individual Educational Plan (IEP's), Student Study Team (SST), and training. 
 
This year we are fortunate enough to have a counselor that is designated to Barrington only. The 
counselor supports students individually, whole class and by referral basis. Topics and support 
range from academics, anger management, anxiety, emotional, self-esteem, health concerns, and 
social skills to simply having someone to talk with. Collaboratively we strive on implementing a 
Positive Behavior Intervention Program, PBIS for all our students. 
 
We have a great partnership with our After-school program. They offer a safe environment for 
students to continue their learning while enriching them with music, art, athletics, computer 
literacy, homework support and academics. We believes in nurturing and developing well-rounded 
learners. 
 
Barrington focuses on rigorous and relevant instructional programs that fully implements State 
Standards and educates our students towards the path of College and Career readiness. The 
standards are a road map to what all students are expected to know at each grade level and be 
able to do. It also helps guides our teachers in developing meaningful lessons. Response to 
Instruction and Intervention and GATE programs are provided to offer support and challenge all 
our students. 
 
Part of developing rigorous and relevant instruction is building relationships with all school 
members especially our students. Our site has embraced that by creating a team that consists of 
instructional aides, yard duty, cafeteria staff, custodians, teachers and administrators we can 
support our students in many capacities. They all have assumed responsibility of implementing 
academic standards while meeting the differentiated needs of all our learners. We continue to work 
collaboratively to implement the standards by creating and implementing skill-specific lessons, and 
using cutting-edge materials and assessments to monitor student progress. Barrington focuses on 
ensuring a professional learning environment where our students can thrive and want to continue 
to be life-long learners. 
 

2017-18 School Accountability Report Card for Hurd Barrington Elementary School 

Page 1 of 7 

 

Mission: 
 
Hurd Barrington Elementary School has high expectations for all students through rigorous instruction, that allows for individual 
differences and learning styles. Our school promotes a safe, caring, and supportive environment. We seek to build positive relationships 
with students, parents, and staff. We strive to have our parents, teachers, and community members actively involved in our students' 
learning. 
 
Our ultimate goal is always safety first, building literacy and numeracy across all content areas, enhancing technology knowledge,ensure 
safe and welcoming facilities will build relationships with parents, students, teachers, administration and community.