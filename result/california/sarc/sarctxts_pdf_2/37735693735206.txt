Oceanside high school is a collaborative community of persevering critical thinkers with PIRATES' 
core values who are goal-oriented, life-long learners contributing to the global society. Our 
Instructional Focus is Relationships, Professional Learning Communities and Critical thinking. Our 
work aligns with the overall vision for students in our district that all students graduate college and 
career ready, prepared to be responsible global citizens and ambitious future leaders and we design 
and orchestrate rigorous and relevant learning experiences to inspire and empower all students. 
School year 2018-19 we implemented Career pathways across campus, Public Service Pathway with 
an Academy of Justice, Health Careers Academy and the Academy of Education. Environmental 
Science and Engineering Pathway, Arts, Digital Media and Design Pathway and Business Innovations 
and Global Entrepreneurship Pathway. All pathways are structured in the Linked Learning Pillars: 
Rigorous Academics, Technical Skills, Work-Based Learning and Personalized Supports. All 9th and 
10th grade students are enrolled in Career Technical Education courses that align with the pathway 
of their choice. This year we have leveraged CTE funding to make sure students have the most 
current and up to date technology and software, such as the implementation of Project Lead The 
Way for our engineers and Apple computers for our designers. In 2016-17 our Health Careers 
Academy was recognized as one of only a dozen in the state of California as a distinguished 
academy. Our other academies are the Academy of Justice and Teach 2 Learn. In 2016-17 we 
administered 940 AP exams. We also have a very well developed and competitive athletics 
program. 
 
The mission of Oceanside High School is to ensure every member of the school community is 
achieving in the challenging and comprehensive environment as determined by state and district 
standards. Support systems will ensure student's maximum preparation for their post-secondary 
pursuits. 
 
 
 
 

2017-18 School Accountability Report Card for Oceanside High School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Oceanside High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

87.19 

1.0 

5.0 

89 

0.0 

 

 

 

 

Oceanside Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

86.06 

 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

Oceanside High School 

16-17 

17-18 

18-19