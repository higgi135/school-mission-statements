We recognize our vital role in the community and it is our mission to educate our students to be 
well-informed, responsible citizens ready to meet the challenges of the twenty-first century. 
 
Our vision is aligned to our district vision. All students graduate from High School prepared to 
successfully enter into higher education and/or a viable career. There are three areas of focus that 
we strive for at Valley View High School: 
 
 
1. Curriculum and Support 
 
We have: 
 
High expectations for all students through rigorous, 21st century standards, 
Curriculum that integrates and blends technology, team building skills and makes real-world 
connections, 
Clear and focused academic goals, 
Data that is used to support student learning, 
Opportunities for students to be college and/or career ready. 
 
2. Student Support: 
 
We provide: 
 
Frequent and strategic monitoring of student progress, 
Academic and behavioral support systems for all students, 
Targeted use of research-based best practices. 
 
3. Positive Educational Environment: 
 
We offer: 
 
A safe and orderly educational environment, 
A positive school culture 
A kind and supportive staff 
 
We are not only positioning our students to be prepared but also to be able to compete with any 
student within our district, county and the nation. 
 
 

2017-18 School Accountability Report Card for Valley View High School 

Page 1 of 18 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Valley View High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

90 

91 

115 

0 

0 

0 

0 

2 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Valley View High School 

16-17 

17-18 

18-19