Alpha Technology Middle School is proud to present our current School Accountability Report Card. 
Students and their families, faculty members and staff, work diligently to make education the 
highest priority of our community. Elverta provides a safe, caring environment which enables 
learners to reach their individual potential, including literacy, knowledge and skills needed to 
exercise the rights and humanitarian responsibilities of citizenship and the ability to compete in a 
global economy. Staff supports the view that emotional, psychological and social needs of students 
are intrinsically related to academic achievement and fully promote this approach. Elverta School 
Site Council, in conjunction with all share-holders, develops and applies the vision for the District. 
Originally, the Lincoln Elementary School, now called the Elverta Joint Elementary School District, 
was constructed in 1855 on homestead land donated by David Strauch located on the present 
Strauch ranch west of the Western Pacific Railroad tracks. The district which was then named 
Lincoln School District went as far as the Archway, now Rio Linda, north to the county line, east to 
the Center Joint School District, and west to the American River. About 1904, more people came 
to this area, including the Northern Electric Railroad which came through Elverta going as far as 
Chico, and a new school district began to form. The original school house was a one-room wood 
building, with no electricity or plumbing. Very few of the students could speak English because the 
predominate language was German. Some of the old families who had children attending the 
school were Jacob Scheidel who came from Alsace-Lorraine, Germany in 1860, Charles 
Schmittmeyer and David Strauch from Bavaria, Germany. In 1897, the school house was moved on 
an area of land donated by V.F. Strauch now located on the corner of Elverta Road and Elwyn 
Avenue. The “Deed”, dated October 19, 1901, was in consideration of $10 to F. Strauch, W.S. Wait 
and C.T, Horgan, from the Trustees of the Lincoln School District for one acre of land, more or less. 
In 1911, a new and larger one room school house was constructed on that same site. In the early 
1920’s, a new school was built on the present site in Rio Linda Boulevard, and the old school on 
Elwyn Avenue and Elverta Road was later used as a lodge hall for the Modern Woodsmen of 
America. The School which was wood and stucco had one small classroom and one large classroom 
with large folding doors to make two classrooms when needed. In the larger classroom there was 
a stage where silent movies were screened for the community for night recreation. Desks were one 
piece, bolted to the floor. The school was heated by coal stoves, and in 1927, the school burned to 
the ground as a result of one of the coal stoves. Classes were held in various homes until the school 
was rebuilt on it’s current location in the fall of 1928. On October 2, 1941, the woman of the Elverta 
School District met in the Elverta Elementary School for the purpose of organizing a Mothers Club. 
The current version of the P.T.A. replaced the Mothers Club in the early 1980’s. In 1955, the 
community club of Elverta changed the name of the school from Lincoln to Elverta Elementary 
School, Elverta School District. On December 6, 1954, two classrooms were added; on November 
15, 1957, three more were completed; and on November 15, 1960, three classrooms, a 
kindergarten, administrative office, multipurpose room and kitchen were added, giving a total of 
13 classrooms. The Cornerstone Ceremony for the Elverta Elementary School was held Saturday, 
April 30, 1977. The Alpha School District joined the Elverta District in 1965, when Alpha School was 
built on Elwyn and Artesia. Ten acres of land were purchased for $18,000. The original Alpha School 
in Placer County was a two-room school house on Baseline Road between Elder Street and Pleasant 
Grove Road. At this time the word “joint” was added to the District’s official name because the 
District now included parts of two counties. In 1981 Alpha Elementary School was converted to an 
intermediate school for seventh and eighth graders. In 1998, Alpha Intermediate School’s name 
changed to Alpha Technology Middle School. In 2005 it was named a Distinguished California 
Middle School. 

2017-18 School Accountability Report Card for Alpha Technology Middle School 

Page 1 of 7