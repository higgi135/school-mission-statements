John Muir Fundamental Elementary (Muir Fundamental) is located at 1951 North Mabury Street 
between Catalina Avenue and East 19th Street. As a school of choice, students residing within the 
district's attendance boundaries are eligible for enrollment through a lottery system. The school 
was recognized with the California Distinguished School Award in 2008, 2012, and 2018. Muir 
Fundamental has also been recognized with the Title I Academic Achievement School Award for 
the previous eight years in a row. Additionally, Muir Fundamental received the California Business 
for Educational Excellence Award for the past nine years for demonstrating consistent growth in 
closing the achievement gap for at risk students. In September 2011, Muir Fundamental was 
recognized as the recipient of the National Blue Ribbon Award - the highest honor a school can 
receive for academic excellence. 
 
Muir Fundamental hosts two classrooms of students with moderate to severe needs addressed 
through special education. The school staff embraces the benefits of the inclusion model for both 
regular and special needs students. 
 
School Mission 
The mission of Muir Fundamental School is to ensure academic success through the design and 
implementation of effective lessons. Muir Fundamental staff and parents are committed to a TK-5 
instructional program that provides all students with the opportunity to acquire the knowledge and 
skills needed to become lifelong learners, to develop problem-solving and critical thinking skills, 
and to gain the cultural understanding, democratic principles and civic values necessary for 
effective participation in a democratic society. Muir Fundamental staff strives to ensure that all 
students exiting Muir Fundamental in fifth grade are on track with the skills necessary for future 
entry into meaningful careers and/or successful college admission. 
 
School Vision 
Muir Fundamental staff and parents are committed to a TK-5 instructional program that provides 
all students with the opportunity to acquire the knowledge and skills needed to become lifelong 
learners, to develop problem-solving and critical thinking skills, and to gain the cultural 
understanding, democratic principles and civic values necessary for effective participation in a 
democratic society. The success of this program is based on a continuum of learning experiences 
that address the special academic, social and physical needs of all students; the access of all staff 
members to staff development opportunities to expand the knowledge and skills required to meet 
the identified needs of students; and a strong collaborative partnership with parents and the 
community. 
 

----

---- 

Santa Ana Unified School District 

1601 East Chestnut Avenue 
Santa Ana, CA 92701-6322 

714-558-5501 
www.sausd.us 

 

District Governing Board 

Valerie Amezcua – Board President 

Rigo Rodriguez, Ph.D. Vice – 

President 

Alfonso Alvarez, Ed.D. – Clerk 

John Palacio – Member 

 

District Administration 

Stefanie P. Phillips, Ed.D. 

Superintendent 

Alfonso Jimenez, Ed.D. 
Deputy Superintendent, 

Educational Services 

Thomas A. Stekol, Ed.D. 
Deputy Superintendent, 
Administrative Services 

Mark A. McKinney 

Associate Superintendent, Human 

Resources 

Daniel Allen, Ed.D. 

Assistant Superintendent, K-12 

Teaching and Learning 

Mayra Helguera, Ed.D. 

Assistant Superintendent, Special 

Education/SELPA 

Sonia R. Llamas, Ed.D., L.C.S.W. 
Assistant Superintendent, K-12 
School Performance and Culture 

Manoj Roychowdhury 

Assistant Superintendent, Business 

Services 

Orin Williams 

Assistant Superintendent, Facilities 

& Governmental Relations 

 

2017-18 School Accountability Report Card for John Muir Fundamental Elementary 

Page 1 of 11 

 

Fundamental Policy 
Muir Fundamental School is an alternate program offered by the Santa Ana Unified School District. It is not a neighborhood school. Parents 
choose to enroll their children in Muir Fundamental and agree to abide by policies that stress high expectations for academics, dress code, 
homework, discipline and attendance. Parents are responsible for the transportation to and from Muir Fundamental. Parents of a Muir 
Fundamental student must agree to: 
 
1. Provide positive support of the goals, philosophy and program of the school. 
2. Assist their child in achieving the required academic standards necessary for promotion. 
3. Participate in conferences with school personnel as requested and support mutually agreed-upon decisions. 
4. Know and enforce the school's Homework Policy and provide conditions at home conducive to study. 
5. Be responsible with their child for reading the school's Code of Student Conduct and abiding by all the school policies and procedures 

concerning academics and behavior. 

6. Be responsible for their child's regular attendance, including arrival on time before school and prompt pick-up after school. 
 
Parent Involvement 
Research has shown that parent involvement is the foundation of a vital, strong partnership between the school staff and parents and is 
crucial to student success in school. Muir Fundamental is pleased to offer many different ways for parents to participate and support their 
children's efforts and achievements as they grow both socially and academically. 
 
1. Support school policies and procedures as outlined in the Muir Fundamental Agreement signed by parents and students each year. 
2. Provide their child with a quiet, supervised place to do homework. 
3. Praise their child's efforts and achievements. Focus on the positive and believe in their ability to succeed. Help them learn from their 

mistakes. 

4. Participate in the school decision-making process through involvement in the Parent-Faculty Organization (PFO), School Site Council 

(SSC), English Language Advisory Council (ELAC), and parent trainings. 

5. Be involved in their child's classroom, library, fundraisers, PFO activities, etc. 
 
As part of the Muir Fundamental Agreement, each family is asked to volunteer at least 12 service hours per year. Muir Fundamental 
parents are also required to attend Back to School Night, parent conferences, Open House and a minimum of 3 parent meetings per year. 
 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently serving nearly 49,300 students in grades K-
12, residing in the city of Santa Ana. As of 2017-18, SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 
alternative high schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or Reduced Price Meal 
program, 39% qualifying for English language learner support, and approximately 13% receiving special education services. Our district’s 
schools have received California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I Academic Achieving 
Schools, and Governor’s Higher Expectations awards in honor of their outstanding programs. In addition, 20 schools have received the 
Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have developed and maintained high 
expectations to ensure every student’s intellectual, creative, physical, emotional, and social development needs are met. The district’s 
commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high quality educational 
program. Consistent success in meeting student performance goals is directly attributed to the district’s energetic teaching staff and 
strong parent and community support. 
 
 

 

2017-18 School Accountability Report Card for John Muir Fundamental Elementary 

Page 2 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

John Muir Fundamental Elementary 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

43 

43 

37 

0 

0 

0 

0 

0 

0 

Santa Ana Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1986 

0 

11 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.