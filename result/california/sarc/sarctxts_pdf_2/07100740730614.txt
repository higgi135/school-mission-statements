Mission: The mission of Golden Gate Community School is to ensure academic improvement and 
successful transition to their district or college/career while promoting pro-social skills. 
 
Golden Gate Community School is a WASC accredited alternative education program serving the 
educational needs of students who have primarily been referred from their home districts. The 
school’s main purpose is to prepare the students to return to their home districts, graduate, or 
transfer to a local Community College or Career. All GGCS courses taken and credits earned are 
transferable to both high schools or community colleges. 
 
Golden Gate Community School offers a total of five sites. Classes are located in Martinez (1 class), 
Rodeo (1 class), Brentwood (1 class), and Pittsburg (2 classes). Golden Gate also offers an 
Independent Study Program for both secondary students as well as adults 18-24 years old at all of 
the above sites. We have four Independent Study teachers and students can attend class as late as 
5:00 pm. Regular school hours are 8:00 to 1:00 pm, with a minimum of 4 hours and 15 instructional 
minutes per day. 
 
Students are referred to GGCCS for the following reasons: 
 
1. Parent/Guardian/Student Choice. 
2. District students whose needs have been reviewed by the district’s School Attendance Review 

Board (SARB). 

3. District expelled students who may no longer attend district schools. 
4. Students who dropped out of school after age 18. 
 
If a student was placed at Golden Gate Community School by their respective districts, they can 
transition back to their appropriate educational setting upon completion of their contractual 
requirements. Students may choose to remain and complete all requirements in order to graduate 
with GGCCS if they complete the required 200 credits. The GGCCS is student-centered and adapts 
to meet students' individual needs. 
 
 

2017-18 School Accountability Report Card for Golden Gate Community School 

Page 1 of 10 

 

• 

•