Cloverdale Elementary School, established in 1987, serves approximately 760 kindergarten through 
fifth grade students. The Cloverdale staff is dedicated to providing a rich educational experience 
where all students strive to meet or exceed California content and proficiency standards. English is 
the primary language spoken by the majority of our students who represent many cultures and 
ethnicities as reflected in the demographic information provided in this document. 
 
California and MVUSD academic standards are used to provide a strong base for our instructional 
program. Supplementary resources from our School Improvement Program, English Learner, Gifted 
and Talented Education (GATE) funding, and fund-raisers support additional instructional materials, 
assemblies, educational field trips, and staff development. These supplemental funds support 
student achievement and enrich the educational environment for our students. 
 
The Cloverdale teachers, staff, and parents are also dedicated to providing a safe, caring learning 
environment with high standards and expectations for academic excellence and good citizenship. 
So that all students will achieve proficiency in language arts and mathematics, Cloverdale's mission 
is to work with staff, parents, and the community to prepare students to become socially and 
academically empowered so they may achieve their highest potential. 
 
Academic Goals: 

• We believe all students can succeed at high levels. 
• We hold high expectations for all students to meet or exceed Standards. 
• We will ensure a strong academic focus of challenging, Standards-based curriculum 
that enables students to be effective communicators and problem solvers in a 
technological world. 

• We will focus instruction on learning and on reducing barriers to learning to close the 

achievement gap. 

• We will use regular assessment to inform instruction and to foster continuous 

improvement. 

Citizenship Goals: 

• We will prepare students to become active, knowledgeable, and responsible citizens. 
• We will implement Positive Behavior Intervention Supports (PBIS) as the foundation for 

standards of behavior, daily interactions, and character development. 

School Environment Goals: 

• We will ensure a safe, orderly learning environment in a climate of mutual respect. 
• We will promote a caring atmosphere that supports the social, emotional, and 

intellectual growth of each child. 

• We are dedicated to equity and respect for all in our diverse community. 

Parent Involvement Goals: 

• We are committed to collaborative decision-making and shared responsibility. 
• We will maintain a partnership and regular communication with families, the 
community, and the school using Parent Links, the marquee, parent letters, Peach Jar, 
our school app and our website as our main forms of communication. 

• We will celebrate achievements with students and parents. 

2017-18 School Accountability Report Card for Cloverdale Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cloverdale Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

27 

27 

30 

0 

0 

0 

0 

0 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Cloverdale Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.