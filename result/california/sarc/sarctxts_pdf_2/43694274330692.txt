Welcome to Evergreen Valley High School - Home of the Cougars! Students and parents are very 
proud of the school’s reputation as an academically challenging and high performing school. 
Students are well prepared to attend four-year colleges and universities around the country. 
Challenging and rigorous curriculum is enriched by a variety of athletic teams, over 100 student 
clubs and community service organizations. An outstanding group of highly qualified teachers work 
collaboratively to ensure that all students realize their full potential. The school offers AP courses 
in French Language, Spanish Language and Literature, Chinese 
(Mandarin), Statistics, 
Macroeconomics, Computer Science, US History, American Government & Politics, World History, 
Psychology, English Language, English Literature, Chemistry, Biology, Physics: Mechanics, Physics: 
Algebra-based, Environmental Science, Calculus AB and BC. The performing arts department 
curriculum includes drama, choir, music appreciation, guitar, symphonic band, advanced band and 
marching band. World languages include Vietnamese, Mandarin, French, and Spanish. A well 
rounded, comprehensive, high school experience is waiting for each and every student. Our mission 
is that our "Students will be effective thinkers who thrive in society." At EVHS, we ROAR! 

----
---- 
East Side Union High School 

District 

830 N. Capitol Avenue 

San Jose, CA 95133 

(408) 347-5000 
www.esuhsd.org 

 

District Governing Board 

Frank Biehl 

J. Manuel Herrera 

Van Thi Le 

Pattie Cortese 

Lan Nguyen 

 

District Administration 

Chris D. Funk 

Superintendent 

Glenn Vander Zee 

Assistant Superintendent 

Educational Services 

 

Chris Jew 

Associate Superintendent 

Business Services 

 

Dr. John Rubio 

Associate Superintendent 

Human Resources 

 

 

2017-18 School Accountability Report Card for Evergreen Valley High School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Evergreen Valley High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

117 

105 

111.9 

2 

0 

7 

0 

7 

0 

East Side Union High School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

991.5 

50.6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Evergreen Valley High School 

16-17 

17-18 

18-19