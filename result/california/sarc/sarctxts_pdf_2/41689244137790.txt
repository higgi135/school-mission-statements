Westmoor High School is a large, comprehensive, and diverse public school of approximately 1525 
students. Westmoor High School is located in western Daly City just blocks from the Pacific Ocean 
and situated on an expansive 66-acre campus. The school is accredited by the Western Association 
of Schools and Colleges (WASC). In 2009, the California Department of Education honored 
Westmoor by bestowing the school with the California Distinguished School award. 
 
Vision 
At Westmoor, our students become learned, productive, and compassionate citizens upon 
graduation. 
 
Mission 
Westmoor High School is dedicated to fostering college and career-ready students, who are both 
academically prepared and socially responsible global citizens. 
 
Goals 
The goal of Westmoor High School is to address the diverse educational needs of our students 
through the integration of technology and a variety of teaching strategies. We engage the whole 
child and prepare students for success in advanced study and employment. 
 
Westmoor Schoolwide Learner Outcomes (SLO's): 
 
• 
 
• 

Students think critically and answer complex questions using multiple sources of information 
to develop an analytical response. 

Students communicate effectively orally and in writing using academic language. 

 
• 
 
• 
 
• 

Students develop the technology skills to be successful in the 21st century work environment. 

Students work collaboratively and cooperatively within a team of diverse members. 

Students possess the essential knowledge and skills of the academic content required for 
graduation and success in advanced study and employment. 

 
 
In support of the Vision Statement, Mission Statement, and Schoolwide Learner Outcomes, 
Westmoor provides students with a diverse array of courses and curricular offerings so they can 
achieve success. In this connection, Westmoor offers courses in a broad range of subject areas that 
includes courses in Advanced Placement, Career Technical Education, Theater Arts, Technology and 
the Vocational Arts. Many of the courses are UC/CSU-approved. 
 
Westmoor graduates have secured admission to prestigious Ivy League and elite private 
universities such as Stanford, Harvard, Brown, CSU,UC,USC, and NYU. As Westmoor students move 
towards graduation, the honors continue as our students receive community and national 
scholarships. Additionally, Westmoor is currently undergoing an extensive modernization program 
that has a 2021 completion time frame. 
 

2017-18 School Accountability Report Card for Westmoor High School 

Page 1 of 15 

 

Westmoor Communication Forums are both diverse and inclusive. They include; School Site Council, Parent Teacher Student Association, 
Principal's Cabinet, Department Heads, Children at Risk in Education Team, Administration Team, Activities Planning Team, Western 
Association of Schools and College Self Study focus groups, Staff Wide Collaboration, English Language Learner Advisory Council, District 
Leadership Team, District Professional Development initiatives and other community forums. 
 
Open communication between our school and our community is something we value. 
 
Respectfully submitted, 
Grace Strickland, Principal.