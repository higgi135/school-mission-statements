Rancho Pico Junior High, a two-time California Distinguished School and a California Gold Ribbon 
School, is founded on the unwavering belief that students and their families are at the very core of 
our decision-making process. As such, our staff is dedicated to creating a positive learning 
environment that encourages growth and an authentic learning experience for each student we 
serve. Here at Rancho Pico, we cultivate thinkers through our academics, build leaders through 
extra-curricular opportunities and nurture relationships by fostering a culture of respect and 
inclusion. 
 
As a team-based school, Rancho Pico continues to set the bar for academic excellence throughout 
the Santa Clarity Valley. Our curriculum is built upon project-based learning, collaboration, and 
crafting strong critical thinkers who are prepared for high school, college, and career while 
simultaneously nurturing the whole child. Beyond the classroom, creativity, citizenship, and 
character are emphasized as students enjoy the social aspects of junior high school. We are proud 
to offer a variety of specialty electives, exploratory courses, clubs, intramurals, brunch and lunch 
activities, school dances, and team events to our students. 
 
We recognize the importance of establishing meaningful partnerships with each family we serve. 
Our Parent Advisory Council (PAC) works closely with staff to support classroom activities, school 
events, and fundraising efforts to enhance instruction and curriculum. The combined effort of our 
staff, students and parent community is what makes Rancho Pico one of the top performing schools 
in the William S. Hart Union High School District. It is certainly good to be a Mustang! 
 
Sincerely, 
 
Mrs. Erum Velek 
PRINCIPAL 

---

- 

----

---- 

William S. Hart Union High 

School District 

21380 Centre Pointe Parkway 

Santa Clarita, CA 91350 

(661) 259-0033 

www.hartdistrict.org 

 

District Governing Board 

Linda Storli 

Bob Jenson 

Dr. Cherise Moore 

Steven M. Sturgeon 

Joe Messina 

Brennan Book, Student Board 

member 

 

District Administration 

Vicki Engbrecht 
Superintendent 

Dr. Michael Vierra 

Assistant Superintendent, Human 

Resources 

Michael Kuhlman 

Deputy Superintendent, 

Educational Services 

 

2017-18 School Accountability Report Card for Rancho Pico Junior High School 

Page 1 of 8