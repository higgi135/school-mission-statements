Although nestled on the outskirts of the small rural mountain community of Julian, California, Julian 
Union School District functions as the hub and center of the community. The school plays an 
integral part in the lives of the families, the neighboring businesses and the surrounding 
community. The quaint, rustic looking town of Julian is located sixty miles northeast of San Diego, 
and is an extremely popular tourist retreat throughout the year. Known as the apple country, the 
old fashioned, picturesque towns privacy and close knit community atmosphere is highly valued by 
the local townspeople, many of whose families have lived in the area for several generations. Julian 
Union is a school district consisting of one elementary and one junior high school. 
 
Since the attendance area encompasses 618.6 square miles, and its boundaries go from the desert 
floor to the top of the mountains, the school buses provide the lifeline between students and the 
schools. The community income is derived mainly from cattle ranching, apple farming, and tourism. 
Many parents commute off the mountain to work in the city since the community has no large 
business or industry. The school district is the largest employer in the community. The population 
has remained fairly constant the last few years, although the expectations for future growth, looms 
on the horizon. 
 
MISSION STATEMENT 
“To provide every student success in learning.” 
 
Our Mission is... To provide every student success in learning. We are dedicated to the individual 
development of attitudes, skills, know ledge, and responsibility essential to successful achievement 
in school and society. We actively involve parents and the community in supporting student 
learning and development. 
 
To Fulfill our Mission... We are committed to a strong and comprehensive educational program and 
learning environment which enhances character, provides role models, optimizes each child's 
potential, develops a lifelong love of learning, builds personal responsibility, provides opportunities 
to serve, and accentuates individual respect. 
 
Fundamental to our Mission... Is a commitment to the active involvement of a competent and 
caring staff, teamwork, shared leadership, the effective management of resources, and a safe and 
orderly learning environment. We pride ourselves on our dedication to our students, families, 
community, each other, our profession, and to continuous learning. 

2017-18 School Accountability Report Card for Julian Junior High School 

Page 1 of 7