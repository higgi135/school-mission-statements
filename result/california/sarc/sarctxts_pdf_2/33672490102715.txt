Estudillo Elementary School is in the east end of San Jacinto, in Riverside County. As one of seven 
elementary schools in San Jacinto Unified, Estudillo Elementary serves approximately 688 students 
in grades K- 5. 

 

Principal’s Message 
At Estudillo, our student’s success and well-being are at the heart of everything we do. We are 
committed to providing a world-class education by engaging every student in a rigorous curriculum 
aligned to the Common Core State Standards (CCSS). 

 

As a Leader in Me school, our school wide commitment is to ensure all Estudillo Eagles are 
prepared for their future success as innovative leaders in our ever-changing world. We truly 
embrace our mission statement and strive to enable all students achieve their potential. Through 
our Leader in Me implementation, we have defined our school’s mission and live it more fully. By 
creating a focus on leadership, our school culture continues to improve, and our students are not 
only learning the academic lesson they need but also acquiring the social emotional skills they need 
for their future. 

 

Utilizing the expertise of all staff on our campus and working as a collaborative team, we aim to 
empower our Eagles to soar. Together, we strive to educate all students to reach the highest levels 
to meet their individual educational needs. Our students are given a quality educational 
experience, every day at Estudillo. Through a strong partnership between parents, students, 
teachers, and staff, we will continue soar. “Learning is Power!” 

 

Recent Achievements and Focus for Improvement 
• 
• 

Estudillo Elementary is recognized as a Leader in Me School. 
For two consecutive years, (July 2017 and July 2018) Estudillo Elementary were recognized by 
the California PBIS Coalition for successful PBIS implementation at the Silver level reflecting 
excellence in the measurement of fidelity and implementing the core features of Positive 
Behavioral Interventions and Supports. 
Beginning December 2018, Estudillo Elementary will expand our school facilities with the 
addition of 6 modular classrooms. 

• 

 
 
 
 
 
 

San Jacinto Unified 

School District 

2045 S. San Jacinto Avenue 
San Jacinto, California 92583 

(951) 929-7700 

www.sanjacinto.k12.ca.us 

 

Board of Trustees 

John I. Norman 
Willie Hamilton 

Deborah Rex 
Jasmin Rubio 
Trica Ojeda 

 

Administration 

Diane Perez 

Superintendent 

dperez@sanjacinto.k12.ca.us 

 

Sherry Smith 

Assistant Superintendent 

Educational Excellence 

 

Matt Hixson 

Assistant Superintendent 

Personnel Services 

 

 Seth Heeren 

Assistant Superintendent 

Business Services 

 

Contents 

About this School 

Conditions of Learning 

Specialized Services 

Textbooks 

School Facilities 
Pupil Outcomes 

Parental Involvement 

School Climate 

Other Information 
School Finances 

Professional Development