Principal's Message 
I would like to welcome you to Sumner Elementary School's annual School Accountability Report 
Card. In accordance with Proposition 98 and to fulfill state and federal disclosure requirements, 
every school in California is required to issue an annual School Accountability Report Card. Parents 
and community members will find valuable information about our academic achievement, 
professional staff, curricular programs, instructional materials, safety procedures, classroom 
environment, and condition of facilities. 
 
Sumner Elementary School provides a warm, stimulating environment where students are actively 
involved in learning academics and positive values. Students receive a California standards-based, 
challenging curriculum from a dedicated and professional staff. Ongoing evaluation of student 
progress and achievement, in Professional Learning Communities (PLC), helps us refine the 
instructional program to meet the individual needs of students in order that all students can 
achieve academic proficiency. 
 
We have made a commitment to provide the best educational and technologically sound program 
possible for Sumner Elementary School's students. We welcome any suggestions or questions you 
may have about the information contained in this report or about the school itself. Together, 
through our hard work and commitment, our students will be challenged to succeed. 
 
Mission Statement 
Sumner Elementary School will provide challenging academic experiences that will encourage each 
child to reach his/her full academic, social, emotional, and physical potential. Learning will occur in 
a safe and caring environment that fosters excitement, enthusiasm, and discovery. Students will be 
encouraged to appreciate their own worth and importance as well as that of others. Sumner 
Elementary School will provide an atmosphere that will enable each child to develop a respect for 
the differences of others and nurture a sense of responsibility to the community and the 
environment. 
 
School Profile 
Sumner Elementary School is located in the central region of Claremont and serves students in 
grades transitional kindergarten through six following a traditional calendar. At the beginning of 
the 2017-18 school year, 513 students were enrolled, including 15.0% in special education,10.3% 
qualifying for English Language Learner support, and 50.5% qualifying for free or reduced price 
lunch. 
 
 

Claremont Unified School District 

 

170 West San Jose Avenue 
Claremont, CA 91711-5285 

(909) 398-0609 

www.cusd.claremont.edu 

 

District Governing Board 

Hilary LaConte, President 

Beth Bingham, D.Min., Vice 

President 

Nancy Treser Osgood, Clerk 

Steven Llanusa, Member 

Dave Nemer, Member 

 

District Administration 

James Elsasser, Ed.D. 

Superintendent 

Lisa Shoemaker 

Assistant Superintendent, Business 

Services 

Julie Olesniewicz, Ed.D. 

Assistant Superintendent, 

Educational Services 

Kevin Ward 

Assistant Superintendent, Human 

Resources 

Brad Cuff 

Assistant Superintendent, Student 

Services 

 

2017-18 School Accountability Report Card for Sumner Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Sumner Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

21 

22 

26 

0 

0 

1 

0 

1 

0 

Claremont Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

325 

5 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Sumner Elementary School 

16-17 

17-18 

18-19