Mission and Goals 
It is the mission of the staff to empower students to become functional, productive members of 
society by providing quality learning experiences in the academics, independent living skills, 
positive self-concepts and effective relationships with others. 
 
The vision of Oakendell Community School is to provide a quality educational program guided by a 
standards-aligned curriculum designed to address the individual needs of our youth. 
 
Program Description 
Oakendell Community School is a seat-based program serving 16-18 males in grades 6-12. The focus 
of the educational component is to remediate academic weaknesses while working with students 
known strengths. The program builds self-image and personal worth. This is accomplished through 
a structured behavior management system, student assessment, and positive reinforcement. 
 
Educational Component 
The Calaveras County Office of Education provides Oakendell Community School with a teacher, 
instructional aide and special education services. Florida Virtual School is the standards-based 
online curriculum utilized. 
 

 

 

----

---- 

Calaveras County Office of 

Education 

185 South Main St. 

Angels Camp, CA 95221 

209-736-6025 

www.ccoe.k12.ca.us 

 

District Governing Board 

Steven Looper 

Marilyn Krause 

Valerie Tudor 

Louis Boitano 

Marti Crane 

 

District Administration 

Karen Vail 

Superintendent 

Karen Vail 

Assistant Superintendent 

Instructional Services 

 

2017-18 School Accountability Report Card for Oakendell Community School 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Oakendell Community School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

1 

0 

0 

1 

0 

0 

1 

0 

0 

Calaveras County Office of Education 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

29 

1 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Oakendell Community School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.