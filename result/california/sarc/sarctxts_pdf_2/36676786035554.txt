Glenmeade Elementary School is located in Chino Hills in an established residential area. The school 
site was developed on approximately 9.2 acres of land overlooking the Chino Valley. The land was 
purchased in 1966 and the first buildings were completed in 1967. School opened for the first time 
for Glenmeade students in January 1968. Additional classrooms and a multipurpose room were 
added to double the student capacity in 1973. In 2012, Glenmeade went through a modernization 
project that include updating classrooms and repairing the slopes. Glenmeade is included in future 
construction under Measure G to create a more secure campus per parent input. 
 
The mission of Glenmeade Elementary School Staff is to work as partners with the students, the 
parents and the community, to provide a safe and secure environment, a positive school climate, 
and increase student achievement. Our goal is to inspire students to do their best in the present, 
and prepare for their dreams for the future, knowing along the way that they are able to make a 
difference. Our school motto is “Champions of the R.A.C.E.”. A group of Roadrunners is called a 
“race”. We use the word race as an acronym to stand for Responsibility, Achievement, Character 
and Empathy. Glenmeade Elementary is a S.T.E.A.M. school meaning that science, technology, 
engineering, art and math are woven across the curriculum and is a focus for our student's 
education. The staff at Glenmeade understands the importance of S.T.E.A.M. in education for 
developing 21st century learners. We also have implemented Positive Behavior Intervention and 
Supports across all grades. Positive behavior expectations are posted in all classrooms and all 
common areas. We have a school wide reward system to recognize our student's great behavior 
called a “Roward”. 
 
Glenmeade’s mascot is the “Roadrunner.” The front of the school has a bright mural depicting this 
indigenous bird with a school pennant with our school’s name. Students and staff show their school 
spirit by wearing the school logo and the school colors of yellow and royal blue. Our primary 
purpose is academic and our curriculum is rigorous. Our teachers are committed to the belief that 
all children can succeed and learn, and they work to motivate students to reach their highest 
potential. It is our highest goal that every child is challenged and successful. 
 

----

--

-- 

Chino Valley Unified School 

District 

5130 Riverside Drive 
Chino, CA 91710-4130 

(909) 628-1201 

www.chino.k12.ca.us 

 

District Governing Board 

James Na, President 

Irene Hernandez-Blair, Vice 

President 

Andrew Cruz, Clerk 

Christina Gagnier, Member 

Joe Schaffer, Member 

Alexi Magallanes, Student 

Representative 

 

District Administration 

 

Superintendent 

Sandra Chen 

Associate Superintendent, Business 

Services 

Grace Park, Ed.D. 

Associate Superintendent, 

Curriculum, Instruction, 
Innovation, and Support 

Lea Fellows 

Assistant Superintendent, 
Curriculum, Instruction, 
Innovation, and Support 

Richard Rideout 

Assistant Superintendent, Human 

Resources 

Gregory J. Stachura 

Assistant Superintendent, 

Facilities, Planning, and Operations 

 

2017-18 School Accountability Report Card for Glenmeade Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Glenmeade Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

22.5 

24.4 

25 

2 

0 

0 

0 

0 

0 

Chino Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1199.9 

22 

2 

Teacher Misassignments and Vacant Teacher Positions at this School 

Glenmeade Elementary School 

16-17 

17-18 

18-19