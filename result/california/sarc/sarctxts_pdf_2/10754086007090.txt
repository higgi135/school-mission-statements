Principal’s Message: 
Fipps Primary School is located in the heart of Riverdale, serving approximately 460 grade K-3 
students. Teachers and staff are dedicated to providing a safe and productive learning experience 
for all students, regardless of ethnicity, gender, disability, or religion. Operating on a traditional 
calendar, the educational programs are designed to provide the skills and tools necessary for 
students to explore their creativity while developing a strong educational base. 
 
Mission Statement: 
Students of the school shall be competent in the subjects offered by the district and shall have a 
positive attitude toward self and others. The curriculum offered shall reflect the subjects and 
activities approved by the District Governing Board and shall reflect the California State Standards. 
 

----
---- 
Riverdale Joint Unified School 

District 

3160 W. Mt. Whitney Avenue 

Riverdale, CA 93656 

(559) 891-4300 

http://www.rjusd.org 

 

District Governing Board 

Paul Brooks, President 

Charles Cox, Clerk 

Dan Conway, Member 

Anita Cuevas, Member 

Connor McKean, Member 

John L. Mendes, Member 

Andy Rollin, Member 

 

District Administration 

Jeff Percell 

Superintendent 

Jeff Moore 

Assistant Superintendent 

Brian Curwick 

Director of Instructional Services 

 

2017-18 School Accountability Report Card for Fipps Primary School 

Page 1 of 8