Ophir Elementary School opened its doors in 1856 as a single school district. On July 1, 2008 Ophir 
Elementary School became the sixth school in the Loomis Union School District. Ophir Elementary 
currently serves approximately 230 students in kindergarten through eighth grade. In addition, 
Placer County Office of Education’s Deaf and Hard of Hearing program is also located on the Ophir 
campus. Ophir Elementary School is a family and community oriented school that takes great pride 
in maintaining many of the Ophir community traditions dating back to the Gold Rush era. 
 
Teachers, staff, parents and students work collaboratively to create a safe, nurturing and 
academically engaging school environment, which support the tenets of the Science, Technology, 
Engineering, Arts and Mathematics (S.T.E.A.M.) program. Through primary music, instrumental 
band, the choral music program, American Sign Language instruction (K-8), Art Docent programs, 
and enrichment opportunities in computer programming/robotics and the engineering design 
process (during and after school) students are actively involved within the school community of 
learners. 
 
For the 2018-2019 school year, Ophir Elementary will focus on the continued student growth 
towards mastery of the California Common Core State Standards, as measured by the California 
Assessment of Student Performance and Progress (CAASPP) for English Language Arts and 
Mathematics. With an emphasis on professional development, the continued implementation of 
the Multi-Tiered System of Support (MTSS) and ongoing collaboration, data analysis, high quality 
core instructional practices, targeted student supports and progress monitoring, all students will 
receive a rigorous instructional program that meets their individual needs. 
 
School Mission: 
As a staff, we are committed to providing and maintaining a highly rigorous and relevant curriculum 
through: the implementation of the California Common Core State Standards, the continued 
development of the Science, Technology, Engineering, Arts, and Math (S.T.E.A.M.) program, the 
implementation a Multi-Tiered System of Support which utilizes regular collaboration opportunities 
to allow for ongoing analysis of student assessment data to provide differentiated learning 
opportunities based upon individual student needs, and fostering a safe, happy and inclusive 
environment with an emphasis on the Ophir Core Values (creativity, achievement, responsibility 
and empathy). Ophir Elementary School, in partnership with its community, is committed to the 
school vision of empowering every student to become a life-long learner who develops their 
creative potential, their critical thinking and problem solving skills, and their collaboration and 
communication skills to become responsible citizens within the community. 
 

2017-18 School Accountability Report Card for Ophir Elementary STEAM Academy 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ophir Elementary STEAM Academy 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

12 

13 

11 

1 

0 

0 

1 

0 

4 

Loomis Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

133 

3 

20 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ophir Elementary STEAM 
Academy 
Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

16-17 

17-18 

18-19 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.