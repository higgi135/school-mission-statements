Principal’s Message 
Welcome to Los Olivos Elementary School, home of the Spartans, a National Blue Ribbon School 
Recipient in 2016 and a five-time California Distinguished School. We are a semi-rural school 
community serving 150 students in kindergarten through eighth grade since 1888. Many of our 
students are fourth and fifth generation Spartans from neighboring ranches and vineyards. 
 
Our School Accountability Report Card (SARC) is designed to provide you with a snapshot of 
information about our school programs and students, as well as our successes and challenges. It is 
our hope that after reading our SARC you will have an appreciation for the depth and breadth of 
talent and dedication within our teaching and support staff. 
 
The staff at Los Olivos Elementary School is continually recognized throughout the county for their 
dedication to excellence in student learning. The Los Olivos Elementary School staff believes that 
we are a welcoming, small town community of connected, creative, hardworking learners, who 
encourage one another to be positive, to be exploratory, to be risk-takers, and to embrace our 
differences. We believe every experience is an opportunity for growth and a path to success! In 
support of this philosophy our teachers endeavor to provide students with a balanced educational 
program that is as rich in academic rigor as it is in the variety of performing arts classes that are 
available to all students throughout the week. If you wish for additional information about the Los 
Olivos Elementary School, please feel free to contact us to experience the Spartan spirit. 
 
Bridget Baublits, Superintendent/ Principal 
 
Mission Statement 
Learning Today … Leading Tomorrow 
The Los Olivos School District is a dedicated partnership of students, families, teachers, staff, 
administrators, school board, and community members that creates a safe and positive learning 
environment. 
 
This partnership develops the full potential and promotes the well-being of all students, enabling 
them to meet the challenges of our ever changing world. 
 

2017-18 School Accountability Report Card for Los Olivos Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Los Olivos Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

12 

11 

11 

0 

1 

0 

1 

0 

0 

Los Olivos Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

11 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Los Olivos Elementary School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.