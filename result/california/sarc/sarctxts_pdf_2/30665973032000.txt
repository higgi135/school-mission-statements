Established in 1965, Estancia High School is located in Costa Mesa, California, and is part of the 
Newport-Mesa Unified School District (N-MUSD), which serves approximately 22,000 students who 
reside in the cities of Newport Beach and Costa Mesa. N-MUSD is a community funded school 
district and does not receive funding based on average daily attendance as the local property taxes 
exceed the district’s revenue limit. Estancia High School is one of four public high schools in the 
district and serves approximately 1,200 students grades nine through twelve making the campus 
the second smallest comprehensive high school in Orange County. The rather small size of the 
student population in relation to surrounding schools and the indoor campus allows Estancia High 
School to retain a familial atmosphere. The campus sits on a coastal bluff surrounded by man-
made and natural points of interests such as the newly revitalized Fairview Park, the Talbert Nature 
Reserve, and the Costa Mesa Golf Course. 
 
Table 1. City of Costa Mesa 
U.S. Census Bureau Quick Facts 2010 Costa Mesa, CA 
Population 111,600 
Population under 18 years 21.5% 
Gender 50.9% male, 49.1% female 
White persons not Hispanic 51.8% 
Persons of Hispanic of Latino origin 35.8% 
Asian persons 7.9% 
Foreign born persons 2007-2011 26.4% 
Language other than English spoken at home 38.4% 
High school graduate or higher (age 25+) 85.8% 
Bachelor’s degree or higher (age 25+) 34.3% 
Median value of owner-occupied housing units $617,000 
Median household income $65,471 
Persons living below the poverty level 13.1% 
 
As Table 2 demonstrates; the school lies between two distinctively different neighborhoods in 
terms of demographics and the socioeconomic levels of the local community varies from working 
class (on the Westside - 92627) to upper middle class (on the Eastside - 92626). 
 
Table 2. Selected socioeconomic variables by feeder zip codes 
U.S. Census Bureau – 2010 Zip Code 92626 Zip Code 92627 
 
Total Population 49,341 61,610 
White 56.4% 49.3% 
Hispanic or Latino 25.0% 42.6% 
Asian 12.6% 5.1% 
English only spoken at home 67.5% 56.7% 
Median family income $75,674 $61,716 
 
The Mission of Estancia High School is to ensure that all students demonstrate high academic 
achievement in order to sustain successful post-high school pursuits. To accomplish this mission, 
Estancia will maintain an ethical, nurturing, and challenging learning environment. 
 

2017-18 School Accountability Report Card for Estancia High School (EHS) 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Estancia High School (EHS) 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Newport-Mesa Unified School District 
(NMUSD) 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

60 

57 

50 

0 

1 

0 

1 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1105 

5 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

Estancia High School (EHS) 

16-17 

17-18 

18-19