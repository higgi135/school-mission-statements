Lydia Romero-Cruz Elementary (Romero-Cruz) is located at 2701 West Fifth Street in Santa Ana 
California. Romero-Cruz serves students in grades TK, K, 1, 2, 4 and 5. Romero-Cruz fourth and 
fifth grade students primarily feed from George Washington Carver Elementary School and are 
taught in Structured English Immersion or Mainstream English classes. Romero-Cruz TK through 
2nd grade students are enrolled in a Dual Immersion Spanish/English program and are from 
surrounding areas inside and outside of the SAUSD boundaries. Current student enrollment is 194 
with a student population comprised of Hispanic/Latino, socio-economically disadvantaged, and 
English Learners. 
 
School-wide Goals 
 
Goal 1: Romero-Cruz students will experience success bridging into Intermediate School and 
beyond. 
Goal 2: Romero-Cruz students will meet grade-level content and linguistic demands. 
Goal 3: Romero-Cruz school community will continue to build a culture of high expectations, 
collaboration, and success for all students. 
 
School Mission 
Romero-Cruz Elementary school is committed to providing personalized learning and socio-
emotional services to all students in order to meet their individual needs and reach their academic 
goals. Teachers, parents, and staff engage in the decision-making process to ensure access to a 
variety of standards- based, research-based resources and real-world experiences to assist students 
in developing 21st Century skills. It is our belief that all students can learn and experience success 
in college or a career of their choice when they are provided with the appropriate tools, strategies 
and supports. 
 
 
 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently 
serving nearly 49,300 students in grades K-12, residing in the city of Santa Ana. As of 2017-18, 
SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 alternative high 
schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or 
Reduced Price Meal program, 39% qualifying for English language learner support, and 
approximately 13% receiving special education services. Our district’s schools have received 
California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I 
Academic Achieving Schools, and Governor’s Higher Expectations awards in honor of their 
outstanding programs. In addition, 20 schools have received the Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have 
developed and maintained high expectations to ensure every student’s intellectual, creative, 
physical, emotional, and social development needs are met. The district’s commitment to 
excellence is achieved through a team of professionals dedicated to delivering a challenging, high 
quality educational program. Consistent success in meeting student performance goals is directly 
attributed to the district’s energetic teaching staff and strong parent and community support. 
 

----

---- 

Santa Ana Unified School District 

1601 East Chestnut Avenue 
Santa Ana, CA 92701-6322 

714-558-5501 
www.sausd.us 

 

District Governing Board 

Valerie Amezcua – Board President 

Rigo Rodriguez, Ph.D. Vice – 

President 

Alfonso Alvarez, Ed.D. – Clerk 

John Palacio – Member 

 

District Administration 

Stefanie P. Phillips, Ed.D. 

Superintendent 

Alfonso Jimenez, Ed.D. 
Deputy Superintendent, 

Educational Services 

Thomas A. Stekol, Ed.D. 
Deputy Superintendent, 
Administrative Services 

Mark A. McKinney 

Associate Superintendent, Human 

Resources 

Daniel Allen, Ed.D. 

Assistant Superintendent, K-12 

Teaching and Learning 

Mayra Helguera, Ed.D. 

Assistant Superintendent, Special 

Education/SELPA 

Sonia R. Llamas, Ed.D., L.C.S.W. 
Assistant Superintendent, K-12 
School Performance and Culture 

Manoj Roychowdhury 

Assistant Superintendent, Business 

Services 

Orin Williams 

Assistant Superintendent, Facilities 

& Governmental Relations 

 

2017-18 School Accountability Report Card for Lydia Romero-Cruz Elementary 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Lydia Romero-Cruz Elementary 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

11 

11 

0 

0 

0 

0 

9 

0 

0 

Santa Ana Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1986 

0 

11 

Teacher Misassignments and Vacant Teacher Positions at this School 

Lydia Romero-Cruz Elementary 

16-17 

17-18 

18-19