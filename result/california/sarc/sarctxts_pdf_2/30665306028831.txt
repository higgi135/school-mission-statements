Vision - High Academic Achievement for All Students 
 
Mission - The Relentless Pursuit of Learning 
 
Smith Elementary School is located in the southern section of the City of Huntington Beach, three 
blocks from the Pacific Ocean. It was built in 1950 and was originally intended to be an extension 
of the current K-8 school named, "Central School" as an overflow for their crowded campus. As the 
population of the surrounding neighborhood grew, Smith continued to add more classrooms to the 
campus. In September of 1965, Smith became an elementary school and Central School became a 
junior high school. Smith is named after its first principal, Agnes L. Smith, an instructional leader, 
dedicated to helping students become life-long learners and productive citizens. Smith School is a 
learning community of 742 students in grades K-5, teachers, para-educators and support staff, and 
a thriving extended family of parents and community members. The classrooms are equipped with 
Smartboards, document cameras and multiple computers. Our school is an inviting, safe, clean, 
attractive campus that provides an optimal learning environment and is an asset to our community. 
The school is a Title 1 school serving a fair population of low socioeconomic students and English 
learner students. The Smith staff is diligent in its pursuit of optimal learning for all students, being 
recognized as a California Gold Ribbon School and Title 1 High Achievement School in 2016, the 
most recent year for elementary level recognition. Smith Surfers continually strive to be the very 
best as we "Ride the Wave to Success". 
 

----
---- 
Huntington Beach City School 

District 

8750 Dorsett Drive 

Huntington Beach, CA 92646 

(714) 964-8888 
www.hbcsd.us 

 

District Governing Board 

Bridget Kaub 

Shari Kowalke 

Rosemary Saylor 

Paul Morrow, Ed.D 

Ann Sullivan 

 

District Administration 

Gregory Haulk 
Superintendent 

Jennifer Shepard 

Assistant Superintendent 

Educational Services 

 

Patricia Hager 

Assistant Superintendent 

Human Resources 

 

Jon M. Archibald 

Assistant Superintendent 
Administrative Services 

 

 

2017-18 School Accountability Report Card for Agnes L. Smith Elementary School 

Page 1 of 9