Vista Heights Middle School is a candidate school for the International Baccalaureate Middle Years 
Program (IB MYP). This school is pursuing authorization as an IB World School. These are schools 
that share a common philosophy - a commitment to high quality, challenging, international 
education that Vista Heights Middle School believes is important for our students. 
 
All students at Vista Heights Middle School are included in the IB Middle Years Program. "The aim 
of all IB programs is to develop internationally minded people who, recognizing their common 
humanity and shared guardianship of the planet, help to create a better and more powerful 
peaceful world." Students will develop the IB learner profile attributes: inquirers, knowledgeable, 
thinkers, communicators, principled, open-minded, caring, risk-takers, balanced, and reflective. 
They will participate in eight types of curricular classes: Language and Literature (Language Arts), 
Mathematics, Science, Individuals and Societies (Social Studies), Physical and Health Education, 
Language Acquisition (world languages ,including Spanish), Arts (Visual and Performing Arts, 
including art, 
instrumental music, vocal music, drama, digital photography), and Design 
(computers, STEAM, AVID, yearbook publication, ASB). 
 
 

----
---- 
Moreno Valley Unified School 

District 

25634 Alessandro Blvd 

Moreno Valley, CA 92553 

(951) 571-7500 
www.mvusd.net 

 

District Governing Board 

Susan Smith, President 

Jesus M. Holguin, Vice-President 

Cleveland Johnson, Clerk 

Gary E. Baugh. Ed.S., Member 

 

District Administration 

Martinrex Kedziora, Ed.D. 

Superintendent 

Maribel Mattox 

Chief Academic Officer, 

Educational Services 

Tina Daigneault 

Chief Business Official, Business 

Services 

Robert J. Verdi, Ed.D. 

Chief Human Resources Officer, 

Human Resources 

 
 

 

2017-18 School Accountability Report Card for Vista Heights Middle School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Vista Heights Middle School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

47 

48 

61 

0 

0 

0 

0 

1 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Vista Heights Middle School 

16-17 

17-18 

18-19