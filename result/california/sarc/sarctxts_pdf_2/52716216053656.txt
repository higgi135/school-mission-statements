We are a 6th through 8th grade Title 1 school. Our goal is to improve student learning through the 
use of effective teaching strategies and best practices developed through collaboration 
opportunities and staff professional development. Our dedicated staff focuses on building strong 
connections and relationships with our students to support student learning. We believe that all 
students will excel when academic excellence is expected, expectations are consistent and taught 
regularly, and enrichment and supports are provided to engage and support learning academically 
and socially to prepare them for high school and beyond. 
 
We have partnerships with community agencies to provide social emotional support and learning 
for students, college and career learning opportunities, and field trips that allow students to explore 
our local colleges. Through our electives and curriculum, students are exposed to and explore 
STEAM (Science, Technology, Engineering, Art and Mathematics). Our 8th grade leadership club 
also provides students with opportunites to create student rallies, develop spirit weeks, run food 
drives, and much more. 
 
Our vision is to have students... 
 
"Learning Today, Leading Tomorrow" 
 
 

 

 

----

 

----

---- 

Red Bluff Union Elementary 

School District 
1755 Airport Rd. 

Red Bluff, CA 96080 

(530)-527-7200 
www.rbuesd.org 

 

District Governing Board 

Steve Piffero 

Sharon Barrett 

Adriana Griffin 

Heidi Ackley 

Doug Schreter 

 

District Administration 

Cliff Curry 

Superintendent 

Claudia Salvestrin 

Assistant Superintendent 

 

2017-18 School Accountability Report Card for Vista Preparatory Academy 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Vista Preparatory Academy 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

25 

26 

24 

3 

0 

3 

0 

3 

0 

Red Bluff Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

90 

10 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Vista Preparatory Academy 

16-17 

17-18 

18-19