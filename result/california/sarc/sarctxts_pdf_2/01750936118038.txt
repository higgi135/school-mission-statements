James Dougherty Elementary is located in the center of the tri-valley corridor of the East Bay. The 
school is positioned in the middle of a residential community, and only a few blocks from the 
entertainment center of the City of Dublin. The community that we serve is highly educated, values 
excellent schools, and hold high expectations for their children. 
 
At the core of our mission is the belief that we are here to serve every student. James Dougherty 
Elementary realizes this by being committed to differentiating instruction to ensure that every 
student will achieve and work to their greatest potential. As a professional learning community, we 
use collaboration time and staff meetings to develop strategies to reach every student and make 
instruction not just rigorous but also engaging. It is this work that earned James Dougherty the 
California Distinguished School status in the spring of 2012, National Blue Ribbon status in the fall 
of 2015, and California Gold Ribbon in the spring of 2016 . At Dougherty we are also strong 
advocates for the social and emotional well-being of all students and celebrating the rich diversity 
of our community. Through our Dublin Integrity in Action character education program, we 
promote core character values. These values are highlighted throughout the school year through 
monthly awards that are displayed in our brag board and school spirit student activities. 
 
The essence and strength of Dougherty is in its community. We have an involved Parent Faculty 
Club (PFC) that raises money to support programs and provide teachers with materials to enhance 
instruction. Dougherty also works with the City of Dublin to promote student activities like Walk 
and Roll to School, St. Patrick's Day Parade or the city’s Tree Lighting Ceremony. This support, 
coupled with the number of parent volunteers that help in our classrooms make Dougherty truly a 
community cornerstone. 
 
James Dougherty’s high performance and overall success is directly linked to the fact that our 
fundamental purpose is providing education and enrichment for each and every student. It is the 
efforts of our dedicated staff to serve every student in every classroom, along with our community’s 
support. This is what makes James Dougherty a true distinguished school. 
 
School Vision 
Dougherty Elementary School provides a safe, positive learning environment that develops self-
motivated, independent, and enthusiastic learners who strive toward academic excellence, social 
responsibility, and personal growth. The goal of the faculty, staff, and parents is to provide a 
program designed to meet the academic, social, emotional, and physical needs of each of our 
students. 
 
Mission Statements 
At Dougherty Elementary, we believe an exemplary and equitable educational program is the 
collaborative result of students, staff, parents and community working together to develop ethical 
citizens and life long learners. We move forward with this focus by: 
1. All students are provided with and acquire the knowledge needed to meet Common Core 
standards and the opportunity to thrive. Students are guided through a well-planned 
comprehensive core curriculum. 
2. All students are routinely assessed formally and informally to check for understanding and 
proficiency on Common Core standards. Students are expected to model, synthesize, and apply 
information into concrete or collaborative project-based learning.

2017-18 School Accountability Report Card for James Dougherty Elementary School 

Page 1 of 9 

 

 
3. All students in need of academic support are provided modifications as well as intervention services such as push-in programs, pull-out 
programs, on-line curriculum, on-line resources, Response to Intervention time and after school programs. 
4. All students demonstrating mastery in core curriculum areas are provided with greater depth and complexity. 
5. All students receive strategies that foster a safe environment in which staff and students respect the uniqueness of individuals and their 
differences.