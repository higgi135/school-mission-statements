Premise: 
 
Through a multi-faceted learning process, students will develop ownership of their academic, 
behavioral, and emotional choices, which will empower them to make a positive impact on their 
lives as well as on the lives of others. Gahr High School’s vision is to utilize our core values (Student 
Learning Objectives—SLOs), a rigorous and comprehensive curriculum, and the talents and abilities 
of all staff members and the community to provide an effective, safe, and supportive learning 
organization where all students meet standards required to graduate and succeed academically, 
personally, and socially in a 21st Century environment. 
 
Our desire is to prepare graduates who connect their comprehensive education to real world 
learning through the recognition of relevant opportunities for all students. We believe that 
students, parents, staff, and community form a team to educate and prepare students, as they 
become productive and successful members of our society. 
 
Vision: 
 
Gahr High School's staff believes that ALL STUDENTS CAN AND WILL LEARN. We strive to provide 
the highest academic, technological, athletic, moral, and social standards. The school staff is 
committed to making a positive impact on the lives of students and fellow staff members. Gahr 
High School is a healthy, successful, safe, effective, and motivational environment for students. It 
is in this context that Gahr High School offers rigorous and challenging programs for every student 
to be prepared for college and/or career pathways. 
 
Mission: 
 
All Gahr High School students graduate with academic and social skills of success, in the position to 
choose college or career pathways. Teachers will lead student centered instructiona focused o the 
content specific writing that can be used in any post high school experience. 
 
GOALS: 

• Build a sense of unity among staff, students, parents, and community through a 

commitment to maximize learning. 

• Develop in each student an appreciation for the privileges and responsibilities of life. 
• Actively engage students in learning individually, collaboratively, and technologically to 

equip them for college and career pursuits. 

• Appreciate the uniqueness and diversity of cultures and ensure that each student and 

staff member is valued and treated with dignity and respect. 

Student Learning OBJECTIVES: 

• 

Increase the access to and use of technology for staff and students through core 
curriculum integration. 

• Build a repertoire of effective instructional strategies and assessments that provide 
opportunities for students to think critically, use creativity, and act strategically in a 
technological environment. 

• Explore opportunities to improve academic achievement through investigation, 

collaboration, problem-solving, and decision-making processes. 

----

--

-- 

ABC Unified School District 

16700 Norwalk Blvd. 
Cerritos, CA 90703 

(562) 926-5566 
www.abcusd.us 

 

District Governing Board 

Ernie Nishii, President 

Dr.Olga Rios, Vice President 

Sophia Tse, Clerk 

Christopher Apodaca, Board 

Member 

Leticia Mendoza, Board Member 

Maynard Law, Board Member 

Soo Yoo, Board Member 

Leticia Mendoza, Board Member 

 

District Administration 

Dr. Mary Sieu 
Superintendent 

Dr. Valencia Mayfield 

Assistant Superintendent, 

Academic Services 

 

Toan Nguyen 

Assistant Superintendent, 

Business Services 

Chief Financial Officer 

 

Dr.Gina Zietlow 

Assistant Superintendent, 

Human Resources 

 
 

2017-18 School Accountability Report Card for GAHR HIGH - STEAM Magnet School 

Page 1 of 12 

 

• Establish activities that nurture the cultural and social environment for Gahr students both on campus and in the global 

community. 

• Enhance communication among all stakeholders through a variety of media and formats. 
• Expand the base of parental and community involvement.