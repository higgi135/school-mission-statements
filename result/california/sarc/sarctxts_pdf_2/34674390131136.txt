I. NEW JOSEPH BONNHEIM COMMUNITY CHARTER VISION STATEMENT: “Our NJB vision is to create 
a quality education program through a variety of learning intelligences to prepare scholars for the 
21st Century.” 
1. Improve academic and social outcomes through ethical practices. 
2. Create a school culture that emits social and educational responsibility and involves parents, 
scholars and staff. 
3. Foster communication and positive relationships between school personnel, scholars, parents 
and community. 
4. All persons will take personal responsibility and accountability for their actions and the actions 
of others. 
5. Have a school climate in which every scholar, parent, and teacher is willing to help one’s 
neighbor, respectful of all people around them, and is willing to be the light in the darkness. 
 
II. NEW JOSEPH BONNHEIM COMMUNITY CHARTER MISSION STATEMENT: Our mission is to 
develop responsible, respectful, and proactive citizens to become caretakers of our community, 
our state, our country, and our planet. This begins with our concept for the New Joseph Bonnheim 
(NJB) being centered on agriculture and on the very community it serves. 
To succeed in accomplishing our mission, we will abide by our NJB Standards, utilizing the Five B’s, 
implementing a positive character education program, and creating a scholar- centered learning 
environment. 
 
III. NEW JOSEPH BONNHEIM COMMUNITY CHARTER STANDARDS: 
1. Commitment--- We will uphold a commitment to high academic and social expectations for all 
scholars. 
We will encourage a positive school climate and a strong sense of community. 
We will create an environment of trust and respect. 
2. Duty---We will work diligently with school personnel, parents and scholars to reinforce our vision. 
We will report improper conduct with procedural fairness and due process. 
3. Equity---We will strive for fairness and equity. 
We will consider the rights and needs of all parties affected. 
4. Integrity---We will remind those facing an ethical decision about the impact of its outcome, while 
at the same time provide them with the courage and support to make difficult decisions. 
We will uphold confidentiality. 
5. Ethical Responsibility---We will model appropriate ethical behavior(s) that will have an impact in 
the lives of others. 
We will abide by policies, procedures and school rules. 
6. Respect---We will recognize and acknowledge the worth of our school community members and 
remember to value them through what we say and do. 
We will maintain appropriate relationship with staff, scholars and parents/guardians. 
 
IV. NJB: Our 5 B’s (Basic School Rules) 
1. Be Safe, 
2. Be Productive 
3. Be Attentive, Listen and Follow Staff Directions 
4. Be Respectful and Responsible to Everyone and Their Property 
5. Be Kind to Other People 

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Christine Baeta 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for New Joseph Bonnheim Community Charter School 

Page 1 of 11 

 

The Joseph Bonnheim School Mission: As New Joseph Bonnheim Community Charter educators, we will provide standards-based 
curriculum and instruction that supports the learning of all students so that they meet or exceed grade level expectations. 
 
NJB is an excellent neighborhood charter school that is centered on agriculture and science, and on the very community it serves. With a 
dedicated and caring team of highly qualified teachers, support staff, wonderful children, involved and supportive parents, and 
partnerships, our mission is to develop responsible, respectful, and proactive citizens to become caretakers of our community, our state, 
our country, and our planet. 
 
NJB is deeply committed in all aspects of our scholars’ learning by providing a quality education using the Highly Effective Teaching model, 
LIFESKILLS to promote positive relationships, and implementing the Common Core State Standards. At NJB, scholars are fully engaged in 
their thinking and demonstrate mastery of learning effectively. Our goal is to guide children to become fully participating citizens by giving 
them a strong academic education in a nurturing environment that recognizes diversity, promotes healthy choices, positive mindset, and 
embraces community involvement. 
 
Our scholars receive a challenging and rigorous academic curriculum that is thematic and based in science and agriculture. Our project 
based learning and inquiry is supported with enrichment activities, hands-on and real-life experiences, community resources, and active 
parent participation and involvement. Parent and community partnerships are encouraged and continue to provide vital assistance to our 
educational program. 
 
In addition to instruction in the core curriculum and units of study in science, scholars are provided learning opportunities in our 
community garden, library, art and music, sports, technology, and various after school and enrichment programs. NJB also has smaller 
class sizes to support learning and every grade level has a bi-lingual teacher. 
To further optimize our scholars’ learning and development of staff, this year we are utilizing a year-round calendar designed to support 
a high level of on-going professional development without interfering with the instructional day. 
 
Other staff members at NJB provide support and are instrumental in helping scholars with their academic and social and emotional needs. 
These services include a speech and language specialist, resource specialist, counseling services and mentoring, and school psychologist. 
 
Scholars receive special recognition in monthly Super Bee assemblies, Perfect Attendance assemblies (monthly, trimester, and year-long), 
recognition for achieving benchmarks on periodic benchmark assessments in English Language Arts and Mathematics, and grade level 
standards in English Language Arts, Mathematics and Science based on the I-ready, SBAC, Science CST, and various data. Scholars' 
academic improvement is also acknowledged and highlighted throughout the school year. 
 
Our school’s Steering Committee, PTA, and other site committees are very active and highly committed to supporting and improving the 
educational program and school environment through its involvement with the school and its many planned school and family activities. 
 
New Joseph Bonnheim Community Charter is a wonderful neighborhood school establishing a tradition of curious intellectual learners, 
high scholar achievement and academic success for all scholars, and outstanding parent and community involvement. We believe in our 
scholars and their educational success is our priority. 
 
 

 

2017-18 School Accountability Report Card for New Joseph Bonnheim Community Charter School 

Page 2 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

New Joseph Bonnheim Community Charter 
School 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

13 

14 

18 

1 

0 

0 

0 

0 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.