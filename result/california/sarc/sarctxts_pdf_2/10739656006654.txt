Herndon-Barstow Elementary School's Vision and Mission is aligned with the Central Unified School 
District's Guiding Principles. 
 
Belief: Every student can learn 
Vision: Every student is prepared for success in college, career, and community 
Mission: Every student will engage in rigorous, relevent, standards-based instruction in every 
classroom, every day, to ensure student learning 
Core Values: Character, leadership, innovation, and continuous Improvement 
 
Herndon-Barstow (HB) Elementary School is located west of Highway 99 in northwest Fresno. It is 
part of Central Unified School District. The school's population is currently 687 students, 49.7% of 
whom are Socio-economically disadvantaged. 
 
Herndon-Barstow Elementary School houses many special populations. Fresno County Office of 
Education operates an severely emotionally disturbed classroom and an autism classroom. These 
students range in age from kindergarten through sixth grade. Central Unified has one State 
preschool program on the campus for four-year-olds, which serves two groups of twenty-four 
students. Herndon Barstow also houses Central Unified's Elementary Opportunity Program. 
 
The goals identified in this School Plan were established after reviewing multiple measures (ie. 
District Benchmarks, Fountas and Pinnell, Accelerated Reader and ST Math data). The data reflects 
the students' current performance levels, the current instructional practices of the teachers, and 
the beliefs and behaviors of our population and community. An in-depth analysis was made by 
reviewing various multiple measures to review the progress of the significant subgroups results, 
focusing specifically on the English Learner (EL), Foster Youth (FY), and Socio-economically 
disadvantaged (SED) subgroup results in English Language Arts (ELA),and, Mathematics. HB 
continues to work hard to ensure that all targets are met to ensure academic improvement. 
 
Our primary focus with staff development and professional growth is geared toward implementing 
"best first instruction." Instructional strategies include, but are not limited to, Common Core State 
Standards (CCSS) student engagement strategies, the 21st Century Skills, the 8 Math practices, 
Guided Reading, engaging students in rigorous, relevant, standards-based instruction, checking for 
understanding, using academic vocabulary, and the infusion of student technology. This will be 
accomplished using core materials and assessing students for mastery. 
 
The School Plan for Student Achievement was created with the input of SSC, School Site Leadership, 
ELAC, staff members, Student Council, and various other stakeholders. This input was extremely 
important as the categorical budget was formed to expend all of HB's funds: Title I Part A: 
Allocation; LCFF; and ASES funds. The current budget for Herndon-Barstow Elementary School is 
very healthy and will be expended for the purpose of improving student achievement, building 
teacher capacity and providing support for our beliefs and behaviors to ensure that "every student 
is prepared for success in college, career, and community" by ensuring that "every student will 
engage in rigorous, relevant, standards-based instruction in every classroom every day to ensure 
student learning". 
 

2017-18 School Accountability Report Card for Herndon Barstow Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Herndon Barstow Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

25 

25 

29 

0 

0 

1 

0 

1 

0 

Central Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

727 

20 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.