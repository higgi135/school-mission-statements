Fallbrook STEM Academy is in its second year of being designated a STEM (Science Technology 
Engineering & Math) Magnet School. Our instructional model utilizes the intrinsically motivating 
phenomenon of science, engineering, technology, and math to engage students in hands-on, 
relevant, and exciting learning. We use our time and resources to focus on innovative instruction, 
specific intervention, and creating and maintaining a positive school culture centered on student 
leadership. 
 
Our Mission is to energize teaching and learning in order to equip each child with the skills needed 
to create an exciting future! 
 
Our Core Values are: 
 
COLLABORATION 
We create together. 
We learn from one another. 
We hold each other mutually accountable to reach our common goals. 
We treat others with respect and value differences. 
 
GROWTH 
We strive for the highest levels of performance for our students and ourselves. 
We will embrace a growth mindset for our students and ourselves. 
 
INNOVATION 
We recognize innovation is a way of thinking that leads to authentic, participatory, relevant learning 
experiences for each student. 
 
RISK TAKING 
We explore new technologies, try new things, and embrace challenges in order to energize our 
instruction and student learning. 
 
We are honored to be recognized as: 
A Leader in Me, Lighthouse School 
A California Gold Ribbon School 
A California Title 1 Achievement School 
 

2017-18 School Accountability Report Card for Fallbrook STEM Academy 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Fallbrook STEM Academy 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

27 

28 

28 

0 

0 

0 

0 

0 

0 

Fallbrook Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

245 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Fallbrook STEM Academy 

16-17 

17-18 

18-19