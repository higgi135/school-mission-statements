Established 1912, Tomales High School is the sole comprehensive high school in the Shoreline 
Unified School District located in the town of Tomales, California, in the northwestern part of Marin 
County. A rural school district with a total enrollment of 508 students, Shoreline Unified School 
District is one of the smallest enrollments of any school district in California. However, it draws its 
students from approximately 450 square miles of pristine California coastal lands from the south 
beginning in the town of Olema, amidst ranch and dairy land, through the fishing village of Bodega 
Bay and up to the Russian River. 
 
Our School Vision Statement, updated in 2014 is as follows: Upon graduation, Tomales High School 
students will be prepared for the world they encounter with the skills necessary for future success. 
They will be socially aware, independent thinkers who are motivated to tackle and solve real world 
problems through communication, analysis, and collaboration. 
 
 

2017-18 School Accountability Report Card for Tomales High School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Tomales High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

15 

14.21 15.21 

1 

 

0 

0 

0 

0 

Shoreline Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

47.83 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Tomales High School 

16-17 

17-18 

18-19