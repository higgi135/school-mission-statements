California School is located on the west side of Costa Mesa in a close knit and supportive 
community. We work to provide a rigorous, well rounded educational experience to all of our 
students in all academic areas, athletics, performing arts, technology, and engineering. We are 
committed to our school’s vision of academic excellence through technology and performing arts, 
as well as our mission of challenging students in all academic areas and providing them 
opportunities to grow and learn in a safe environment. 
 
California School is a true community school. We have over a hundred registered volunteers that 
assist in the classrooms and help to bring programs to our students. Our parent groups volunteer 
their time to provide the absolute best to our students and bring them programs like Art Masters, 
Inside the Outdoors, Imagination Machine, and Walk Through History. Our volunteer parent groups 
also host family oriented carnivals and movie nights, as well as supporting the growth of technology 
and performing arts in our school. 
 
We offer opportunities for students to grow and learn through performing arts with two levels of 
show choir and band. Show Choir is open to all 1st- 6th grade students. In the past, students have 
put on incredible performances of Annie, Willy Wonka, 101 Dalmatians, and Beauty and Beast. Our 
bands perform across the community at high school football games, district music festivals, Knott’s 
Berry Farm, and Costa Mesa City Hall. 
 
California School offers a number of enrichment and support programs, in addition to our core 
curriculum. Our science teacher provides hands on standards based lessons in our science lab for 
students in grades TK-6. Sixth grade students are introduced to robotics through lessons and hands 
on building as teams of students work together to create a robot. 6th grade students also attend 
outdoor science school at Irvine Ranch. Students in grades K-6 receive weekly lessons in the 
computer lab to work on in-depth projects utilizing technology and curricular standards. Students 
in 5th and 6th grade each have a Chromebook that is used every day to access the core curriculum 
and extend their learning in the classroom. We have an open library system that allows students to 
fully utilize our schools Accelerated Reader Program, and we use Reflex Math and Front Row as a 
supplement to our math program. Art Masters provides monthly art lessons and instruction for all 
students. In weekly music lessons, students learn how to read music, play instruments and perform 
musical productions. 
 
We believe our school provides the best education to all students in providing them opportunities 
to grow as individuals, to be challenged in all academic areas, and in giving them a diverse 
educational experience that enables them to develop socially, behaviorally, and academically. 
 
 

----

---- 

Newport-Mesa Unified School 

District 

2985 Bear St. 

Costa Mesa, CA 92626 

(714) 424-5033 
www.nmusd.us 

 

District Governing Board 

Charlene Metoyer, President 

Martha Fluor, Vice President 

Dana Black, Clerk 

Ashley Anderson, Member 

Michelle Barto, Member 

Vicki Snell, Member 

Karen Yelsey, Member 

 

District Administration 

Dr. Frederick Navarro 

Superintendent 

Mr. Russell Lee-Sung 

Deputy Superintendent, 
Chief Academic Officer 

Ms. Leona Olson 

Assistant Superintendent, 

Human Resources 

Mr. Tim Holcomb 

Assistant Superintendent, 

Chief Operating Officer 

Dr. Sara Jocham 

Assistant Superintendent, 

Student Support Services/SELPA 

Mr. Jeff Trader 

Executive Director, 

Chief Business Official 

Dr. Kirk Bauermeister 

Executive Director, 
Secondary Education 

Dr. Kurt Suhr 

Executive Director, 

Elementary Education 

 

 

2017-18 School Accountability Report Card for California Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

California Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

21 

22 

21 

0 

0 

0 

0 

0 

0 

Newport-Mesa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1105 

5 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

California Elementary School 

16-17 

17-18 

18-19