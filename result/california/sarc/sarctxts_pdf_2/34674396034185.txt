The Oak Ridge staff is committed to building a community of well-rounded, confident, critical 
thinkers who are intrinsically motivated and empowered with a passion for learning. Students with 
positive character who are prepared to collaboratively work with others for high levels of success 
and happiness in life's path. 
 
We continue to have our school goals reflect our redesign and mission in an effort to better meet 
the learning needs of our students. Our school is focused on three school-wide priorities: 
 
1. 

Providing rigorous student-centered instruction in the areas of Common Core Standards and 
Social Emotional Learning 
Developing high functioning collaborative teams 
Developing practices that enhance positive school-home relationships 

2. 
3. 
 
Our work is focused on providing meaningful learning experiences for our students. We recognize 
that our students need to master basic skills while simultaneously developing higher order thinking. 
We aim to achieve this by incorporating the Common Core State Standards thru an Inquiry Driven 
Learning approach that allows students to dive deep into an essential question designed to have 
students make change for their community. 
 
Our overall approach stresses challenging and refining our belief systems, creating a healthy 
academic climate, and providing engaging instruction. We believe this will help all students thrive 
academically. We encourage students to make responsible choices through our Social Emotional 
Learning program that consists of a character education program, Restorative Practices, and 
educational resources to support our school wide themes on these topics. Students receive 
SUCCESS tickets by demonstrating safe, respectful, and responsible behavior. 
 
Something genuinely special and unique about Oak Ridge is the support we get from our 
community partners. We appreciate the strong bonds that have been formed with Soil Born Farms, 
Orrick Law Firm, Way Up Sacramento, and others. Our Partnerships work closely with the school to 
ensure that every student receives a quality learning experience. 
 
We recognize that, above all, our single most important partnership is the one we foster with each 
of our families. Parents, grandparents, caregivers, foster parents, guardians, and extended family 
members are encouraged to be present at school in a variety of ways. We welcome warmly the 
support we receive from families who read and do homework with their children, volunteer for 
yard duty, attend monthly community meetings, support our Family Teacher Academic Team 
(FTAT) family nights, and serve on traditional committees such as our School Site Council or English 
Learner Advisory Committee. We truly value our families, who walk their children to school day in 
and day out, attend parent/teacher conferences, and spend time helping in classrooms. 
 
Our expectations for each child’s personal and academic growth are backed by our commitment to 
ensuring each child receives the best education we can possibly deliver. 
 

2017-18 School Accountability Report Card for Oak Ridge Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Oak Ridge Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

20 

20 

21 

3 

0 

3 

0 

2 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Oak Ridge Elementary School 

16-17 

17-18 

18-19