Jim Thorpe Fundamental Elementary (Thorpe Fundamental) is located on W. Alton Avenue 
between S. Greenville Street and S.Raitt Street. Thorpe Fundamental is a school of choice; students 
residing in the district's attendance boundaries are eligible for enrollment through a lottery system. 
Founded by parents seeking a traditional educational program for their children, Thorpe 
Fundamental's standards-aligned program is focused on the schoolwide theme of College Bound 
Scholars. 
 
School Vision 
Thorpe Fundamental Elementary School staff and parents are committed to a TK-5 instructional 
program that provides all students with the opportunity to acquire the knowledge and skills needed 
to be college and career ready and become productive citizens in the 21st century. This includes 
becoming life-long learners, developing problem-solving and critical thinking skills, and gaining 
cultural understanding and civic values necessary for participation in a democratic society. The 
success of this program is based on the continuum of learning experiences that addresses the 
special academic, social, and physical needs of all students; the access of all staff members to staff 
development opportunities to expand the knowledge and skills required to meet the identified 
needs of students; and a strong, collaborative school partnership with parents and community. 
 
School Mission 
Jim Thorpe Fundamental Elementary School's mission is to collaborate with our parents and the 
community in order to support a respectful, responsible, honorable, and safe learning environment 
in order to empower students to grow intellectually and soar to their highest expectations as 
college and career ready citizens. 
 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently 
serving nearly 49,300 students in grades K-12, residing in the city of Santa Ana. As of 2017-18, 
SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 alternative high 
schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or 
Reduced Price Meal program, 39% qualifying for English language learner support, and 
approximately 13% receiving special education services. Our district’s schools have received 
California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I 
Academic Achieving Schools, and Governor’s Higher Expectations awards in honor of their 
outstanding programs. In addition, 20 schools have received the Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have 
developed and maintained high expectations to ensure every student’s intellectual, creative, 
physical, emotional, and social development needs are met. The district’s commitment to 
excellence is achieved through a team of professionals dedicated to delivering a challenging, high 
quality educational program. Consistent success in meeting student performance goals is directly 
attributed to the district’s energetic teaching staff and strong parent and community support. 
 

2017-18 School Accountability Report Card for Thorpe Fundamental Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Thorpe Fundamental Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

37 

37 

36 

0 

0 

0 

0 

0 

0 

Santa Ana Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1986 

0 

11 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.