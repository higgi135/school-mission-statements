Marce Becerra Academy is a continuation high school offering a meaningful course of study aimed 
at meeting the needs of students who desire an alternative educational environment, a workforce 
preparation curriculum, and/or who would benefit from opportunities to recover credits. We are a 
small, innovative learning community. Our mission is to prepare students for the post high school 
world, and being 21st century, life-long learners. 
 
We promote the belief that all students can develop their full potential regardless of their history. 
We strive to provide an educational environment which promotes lifelong learning for our students 
and staff. We believe in creating an environment that challenges students to develop a practical 
body of knowledge and a set of problem-solving skills which will serve them in the workforce, 
college, and life in general. Students are expected to learn to use technology as a key tool to support 
continuous learning. We provide students with opportunities to gain real work experiences. 
Students utilize online learning to accelerate credit recovery. 
 
We strive to create a supportive environment where staff and students build positive relationships 
and access resources at all levels of the community. We provide positive role models and 
motivational experiences which help students to realize their potential. We promote an 
environment which encourages positive risk-taking. We strive to provide a multicultural 
environment where staff and students learn to value the dignity of each person, the uniqueness of 
one’s self as well as others, and recognize the strength that exists in this diversity. 
 
All MBA students also attend at least two classes at HHS, and all MBA students take their math class 
from an HHS teacher. All MBA students are require to take at least one CTE course each year and 
are encouraged to complete a full CTE pathway. 
 

----

---- 

Healdsburg Unified School 

District 

1028 Prince Street 

Healdsburg, CA 95448 

(707) 431-3488 
www.husd.com 

 

District Governing Board 

Vince Dougherty 

Judy Velasquez 

Donna del Rey 

Jami Kiff 

Aracely Romo-Flores 

 

District Administration 

Chris Vanden Heuvel 

Superintendent 

Steve Barekman 

Director of Business Services 

Diane Conger 

Director of Student Support 

Services 

Erin Fender 

Director of Curriculum & 

Instruction 

Francesca Whitcomb 

Human Resources Manager 

 

2017-18 School Accountability Report Card for Marce Becerra Academy 

Page 1 of 9