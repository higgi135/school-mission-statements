Junction City School is a 2018 Distinguished School and Exemplary District! 
 
We are a small, one-school district serving about 65 kids in grades TK-8. We are located in the small 
mountain town of Junction City in Trinity County, California. We are nestled at the base of the 
beautiful Trinity Alps Wilderness, along ten acres of the Trinity River. 
 
Our staff and community strive to work together to create programs that produce articulate, 
confident, and skilled students. There is a strong focus on a well-balanced education for every 
student with high standards aimed at excellence and student empowerment. 
 
All Junction City School students participate in a curriculum that is based on Common Core 
Standards and is enriched with STEM, Social Emotional Learning (SEL), and Visual and Performing 
Arts. Our After School Program offers electives, academic enrichment, and homework assistance. 
Our intervention program offers support for students that need additional assistance with skills. 
 
The mission of Junction City School is to improve the academic and social skills of all students, and 
to create a school climate conducive to learning by encouraging good citizenship, good attendance, 
and high academic standards. It is our intent to provide an environment that fosters in children the 
ability to recognize and accept responsibility to the end that they may be active participants in our 
global society. 
 
Together, these programs and philosophies help to promote a whole school family atmosphere 
with an emphasis on our students' success and well-being. 
 
We are excited about our dynamic learning community, and look forward to sharing it with you! 
 
 

2017-18 School Accountability Report Card for Junction City Elementary School 

Page 1 of 7 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Junction City Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

4 

0 

0 

4 

0 

0 

4 

0 

0 

Junction City Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

4 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Junction City Elementary School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.