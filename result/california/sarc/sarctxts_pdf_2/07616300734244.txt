Miramonte High School, located in Orinda, California, was founded in 1955 and is a Western 
Association of Schools and Colleges accredited school. Miramonte has been recognized as a Gold 
Ribbon School and is routinely acknowledged by US News & World Report in their ranking of the 
Best High Schools in the state and country. 
 
Miramonte is committed to providing rigorous academics, a safe learning environment, and 
opportunities for students to build successful relationships with adults and peers. Our rich and 
varied curriculum ensures an optimum learning experience for each student. Participation in a 
broad range of activities, including academic classes, visual and performing arts, sports, and 
volunteer opportunities develops our students into well-rounded citizens ready to build a stronger 
community. 
 
Mission Statements: 
The Miramonte High School community provides a supportive environment for students to grow 
intellectually, socially, physically, and emotionally, and challenges them to positively contribute to 
their community. 
 
Vision Statement: 
Miramonte students will be creative and innovative learners, collaborators, and communicators. 
We challenge our students to become compassionate and knowledgeable citizens who actively 
participate in our global community. 
 
Schoolwide Learner Outcomes: 
Miramonte High School graduates will be: 
 
Learners who: 
Demonstrate achievement of learning standards 
Are self-directed and are self-aware 
Use their creativity to solve problems 
Reason abstractly and quantitatively 
Make sense of problems and persevere in solving them 
Construct viable arguments and can analyze the reasoning of others 
Apply appropriate strategies, including technology, to learn new concepts and skills 
Set goals and employ time-management skills to maintain high standards 
 
Communicators who: 
Identify and appropriately address diverse audiences 
Effectively integrate oral, written, and research skills into their work 
Listen objectively and with empathy 
Express themselves logically and creatively 
Utilize technology appropriately to convey information and ideas 
 
Collaborators who: 
Solve problems cooperatively within diverse groups 
Use appropriate leadership skills to develop and maintain relationships 
Respectfully consider all viewpoints and are culturally sensitive 
Leverage appropriate technologies to enhance their work 

2017-18 School Accountability Report Card for Miramonte High School 

Page 1 of 13 

Citizens who: 
Take responsibility for their own actions 
Act with integrity and compassion 
Respect diversity and the opinions of others 
Practice ethical behavior in regard to the use of information technology and academic honesty