DISTRICT'S MISSION 
Corning Union High School's mission is to advance and support student acheivement in a safe 
learning environment. 
 
CENTENNIAL'S MISSION 
Centennial High School's mission is to foster the academic and social growth of life-long learners 
who in turn become contributing members of society. 
 
CENTENNIAL HIGH SCHOOL'S CORE VALUES 
To achieve our mission, we have developed these Poitive Norms of Interactions 
 
CORE VALUES/BELIEFS 
To achieve our mission, we hold these values for each staff member and student: 
 
Values 
 
I believe: 

• We can meet students where they are and help them move forward 
• All people have the right to be treated with respect 
• 
• 
• Acceptable Personal values are needed for each student 
• All people have the right to earn respect 

Every person has something they can contribute toward a common good or goal 
Students need to develop responsibility and accountability 

 
Success 
 
I believe: 

• Success means different things to different people 
• All people can grow, change and become better 
• All students have the potential to succeed 
• All students can succeed; but not at the same time or at the same rate 
• Students need to be held to consistent standards of achievement that encourage them 

to succeed 

• Students need to develop respect 
• Consistency and rules/structures tougher than the main campus’ will help students 

succeed

2017-18 School Accountability Report Card for Centennial High School 

Page 1 of 10 

 

 
Teaching/Learning 
 
I believe: 

• Students need to be educated and reinforced in the mores of etiquette of our society 
• Some students need extra supervision and guidance to be successful 
• 
• 
• 
• 
• 
• Students are entitled to appropriate and engaging lessons 
• Bell-to-bell instruction is important 

In teaching to the standards each period 
In establishing purpose each period 
In checking for understanding 
In assessing for learning 
In re-teaching, as needed 

 
Parents/Community 
 
I believe: 

• We should give back to others 
• Students need to work with and for the community—this will show responsibility and teamwork 
• Parents may need support and guidance at times 
• 

In community service projects 

 
Self-worth 
 
I believe: 

• All people need to do work that is important to them 
• Being a true individual is preparing for the future, learning from the past and not compromising your future 
• One should have pride in being a positive member of society and develop a sense of accomplishment 
• One should look for what he/she can change; not what others should change 

 
Environment 
 
I believe: 

• We have to work in partnerships 
In treating everyone with respect 
• 
• People work best in a safe environment where they trust each other 

 
Students/Human Needs 
 
I believe: 

• Students need an emotionally and physically safe environment 
• Students need teachers who actively teach 
• Staff and students need communication both ways (with district, maintenance, cafeteria staff, and transportation) 
• We need to attend useful workshops to gain ideas for teaching and to increase our skills 
• We need to present ourselves, students, school, and district in a positive manner 
• 
• Students model what they see adults do 
• Students need teachers that are positive, uplifting, and enthusiastic 

If we can take just one student and turn him/her into a productive worker; we have done our jobs 

 
Workability 
 
I believe: 

• Students need ROP—students need to work in the community with and without pay 
• Woodshop teaches job skills 
• Students need to develop good work habits 
• Students need to be prepared to be productive members of society 
• Students need fair and consistent application of discipline 
• Students need to know that self-esteem is built through accomplishment—both for self and others (work ethic) 

2017-18 School Accountability Report Card for Centennial High School 

Page 2 of 10 

 

 
Expectations of Administration: 
 
I believe: 

• Keep an open line of communication 
• Needs to foster teamwork 
• Should not be scared to say “no” 
• Stand behind teachers and staff 

 
Expectations of Teachers 
 
I believe: 

• Teachers need to be positive role models 
• Need to be consistent in the classroom 
• Need to foster success in every student 
• Do their job 
• Provide “opportunity to learn” every class, every day 
• Display professionalism 
• Work through conflicts 
• Talk directly to their administrator when there is an issue 

 
Expectations of Students 
 
I believe: 

• Students should learn the importance of education 
• Earn their privileges 
• Take responsibility for their surroundings 
• Show respect 

 
Professionalism 
 
I believe: 

• We should begin every day like yesterday never happened 
• Dress and look the part 
• Carry oneself in a manner that shows respect 

 
POSITIVE NORMS OF INTERACTION 
Centennial staff, students, parents and community members developed and approved the followed norms of interaction. The following 
describes how we strive to operate: 
 
1. Be professional 
 
2. Honest, direct, positive in communications 
 
3. Show respect for everyone 
 
4. No sidebars or phones 
 
5. Stay on-task and schedule 
 
6. One person speaks at a time 
 
7. Teamwork and flexibility 
 
8. Use parking lot (items to be discussed at later time) 
 
9. Everyone monitors the process and enforces norms 
 
10. Keep sense of humor 

2017-18 School Accountability Report Card for Centennial High School 

Page 3 of 10