Description 
Vista Square Elementary is one of 49 schools in the Chula Vista Elementary School District, including 
charters. It is one of the oldest schools in the district. The school lies on the west side of Chula Vista. 
Each of the four main buildings contains four classrooms. There are 15 more classrooms located in 
relocatables. Vista Square has 3 Mild Moderate SDC classes and a Resource Specialist Program 
(RSP). Vista Square has a full-time nurse, 4-day psychologist, and 2 full-time speech and language 
specialists. The school site also houses the Davila Day School for the Deaf and Hard of Hearing with 
a total of 43 students who are mainstreamed into the Vista Square program. 
 
Mission 
The school mission at Vista Square is to: 
Provide a wide variety of educational and life experiences in a safe environment encouraging active 
involvement and success for each child; 
Teach and encourage students to respect the rights and properties of others and be responsible for 
their own actions; 
Encourage full participation by students, parents, community, and staff in school wide activities; 
Continue an academically oriented, supportive environment which fosters individual development 
of a positive self-image 
Provide sequential curriculum and instructional methods which encourage students to reach their 
ever-increasing learning potential. 
 
 

----

---- 

Chula Vista Elementary School 

District 

84 East J Street 

Chula Vista, CA 91910-6100 

(619) 425-9600 
www.cvesd.org 

 

District Governing Board 

Leslie Ray Bunker 

Armando Farias 

Laurie K. Humphrey 

Eduardo Reyes, Ed.D. 

Francisco Tamayo 

 

District Administration 

Francisco Escobedo, Ed.D. 

Superintendent 

Jeffrey Thiel, Ed.D. 

Assistant Superintendent, Human 
Resources Services and Support 

Oscar Esquivel 

Deputy Superintendent, Business 

Services and Support 

Matthew Tessier, Ed.D. 

Assistant Superintendent, 

Innovation and Instruction Services 

and Support 

 

2017-18 School Accountability Report Card for Vista Square Elementary School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Vista Square Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

30 

30 

30 

0 

0 

0 

0 

0 

 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Vista Square Elementary School 

16-17 

17-18 

18-19