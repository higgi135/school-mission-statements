While the Parkway Heights Middle School student community is comprised of diverse learners, our 
students have very similar needs and aspirations. We are also committed to providing a rigorous 
standards-based curriculum that will prepare all of our students for high school success. Student 
achievement data is used to place students in classes and on an on-going basis, to evaluate the 
overall effectiveness of our instructional program. Our English learners gain access to the core 
curriculum by receiving instruction that is tailored to meet their academic needs. Our Special 
Education students also receive instruction that is individualized and is intended to make the 
California Common Core Standards accessible to them. 
 
Mission statement: 
We will work collaboratively to ensure that each student is prepared for high school and post-
secondary education. 
 
 

 

 

----

---- 

----
---- 
South San Francisco Unified 

School District 

398 B. Street 

South San Francisco, CA 94080 

650.877.8700 
www.ssfusd.org 

 

District Governing Board 

John C. Baker 

Patricia A. Murray 

Daina R. Lujan 

Eddie Flores 

Mina A. Richardson 

 

District Administration 

Shawnterra Moore, Ed.D. 

Superintendent 

Keith B. Irish 

Assistant Superintendent, Educational 

Services and Categorical Programs 

Jay Spaulding, Ed.D. 

Assistant Superintendent Human Resources 

and Student Services 

Ted O 

Assistant Superintendent, Business Services 

Leticia Bhatia, Ed.D. 

Director English Learner Programs, 

Categorical Programs and Special Projects 

Jason Brockmeyer 

Director of Innovation, Community Outreach 

and Special Projects 

Valerie Garrett, Ed.D. 

Director of Student Performance, Program 
Evaluation, and Instructional Interventions 

Ryan Sebers 

Director of Student Services 

Velma Veith, Ed,D. 

Director, Pupil Personnel Services and Special 

Education 

Bryant Wong 

Director of Technology 

Ronald Vose 

Director of Facilities and Safety 

Fran Debost, MS, RDN 

Director of Nutrition Services and 

Distribution 

 

2017-18 School Accountability Report Card for Parkway Heights Middle 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Parkway Heights Middle 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

33 

36 

31 

0 

0 

0 

0 

2 

0 

South San Francisco Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

412 

16 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Parkway Heights Middle 

16-17 

17-18 

18-19