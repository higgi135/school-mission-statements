Silver Wing is one of 49 schools in the Chula Vista Elementary School District, including charters. 
Silver Wing was built in 1968 and it was remodeled in 2003. Silver Wing is comprised of one one 
main building composed of administrative offices, cafeteria, library, auditorium, multipurpose 
room, and 20 classrooms. %v our students are Hispanic/Latino. More than one half of our students 
have a first language other than English and are classified as English Learners. Silver Wing is a 
Provision 2 school, with 100% of our students receiving free or reduced lunch. 
 
Mission and Vision 
The Silver Wing Elementary School community values the contributions of each stakeholder. 
Honesty, trust, mutual respect, personal responsibility, individual effort, and accountability are 
values that we believe are the foundation of a safe, pleasant, and stimulating learning and working 
environment. 
 
The Silver Wing Elementary School community is committed to creating a challenging and nurturing 
learning environment where all people are valued and respected. We believe that the diversity 
Silver Wing brings is our strength. Through the cooperative efforts of the community, students 
become high achieving, innovative thinkers socially responsible citizens. We take pride in 
developing each child's full potential. 
 
We are a community of learners. Learning is seen as a lifelong process that is meaningful and 
relevant to our changing world. The community is dedicated to ensuring a love of learning that 
challenges the status quo and embraces a technological world. The community envisions 
generations of children instilled with hope for their future. 
 
 

 

 

-
-
-
-
-
-
-
- 

----

---- 

Chula Vista Elementary School 

District 

84 East J Street 

Chula Vista, CA 91910-6100 

(619) 425-9600 
www.cvesd.org 

 

District Governing Board 

Leslie Ray Bunker 

Armando Farias 

Laurie K. Humphrey 

Eduardo Reyes, Ed.D. 

Francisco Tamayo 

 

District Administration 

Francisco Escobedo, Ed.D. 

Superintendent 

Jeffrey Thiel, Ed.D. 

Assistant Superintendent, Human 
Resources Services and Support 

Oscar Esquivel 

Deputy Superintendent, Business 

Services and Support 

Matthew Tessier, Ed.D. 

Assistant Superintendent, 

Innovation and Instruction Services 

and Support 

 

2017-18 School Accountability Report Card for Silver Wing Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Silver Wing Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

19 

0 

Teaching Outside Subject Area of Competence 

NA 

19 

19 

0 

 

0 

 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Silver Wing Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.