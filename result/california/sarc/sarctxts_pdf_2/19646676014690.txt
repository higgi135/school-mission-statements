Create OPTIONS and EXCELLENCE in Education! 
 
Joshua is a school dedicated to producing responsible members of our society that are well 
equipped to face the challenges of the 21st Century by developing their skills in critical thinking, 
communication, creativity, and collaboration. 
 
We will improve learning and skills for ALL students NOW! No Opportunity Wasted. 
 

 

 

----

-

--- 

-------- 

Lancaster School District 

44711 Cedar Avenue 
Lancaster, CA 93534 

(661) 948-4661 
www.lancsd.org 

 

District Governing Board 

Greg Tepe, President 

Keith Giles, Vice President 

Diane Grooms, Clerk 

Sandra Price, Member 

Duane Winn, Member 

 

District Administration 

Dr. Michele Bowers 

Superintendent 

Lexy Conte 

Deputy Superintendent 

Human Resources Services 

 

Bart Hoffman 

Assistant Superintendent 

Educational Services 

 

Dr. Larry Freise 

Assistant Superintendent 

Business Services 

 

 

2017-18 School Accountability Report Card for Joshua Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Joshua Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Lancaster School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

25 

25 

24 

0 

0 

0 

0 

2 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

572 

46 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Joshua Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.