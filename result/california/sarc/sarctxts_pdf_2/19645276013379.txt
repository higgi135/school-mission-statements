Rio Vista has been recognized as a 2014 National Blue Ribbon School. As well, Rio Vista has been 
recognized as an AVID Elementary Certified Site for the 2018-2019 school year. These two 
recognitions mark the significant efforts of students, teachers, staff and parents over the last 
several years. 
 
As we move forward we continue our commitment to maintain a safe, nurturing environment of 
high expectations in which all students can learn and succeed. We seek to create a climate where 
students not only feel welcome, but also are eager to participate in their education. We strive to 
provide students with opportunities that allow them to gain knowledge and experience beneficial 
to their future success. 
 
“In order to provide specific, targeted instruction to all students, we at Rio Vista will not rest until 
the needs of our students are met through consistent collaboration, individual flexibility and our 
complete trust in each other as a team.” 
 
Our uncompromising vision affirms that in working together, all students at Rio Vista will develop 
a positive self-worth and a passion for lifelong learning. 
 
We aim to build stronger partnerships with parents. We want to encourage and support them in 
taking leadership roles that support our students and community and desire their participation in 
their students' education. By working together we prepare our students for college and career 
readiness, building a strong foundation of academic skills and habits that contribute to life-long 
learning. 
 

----

--

-- 

El Rancho Unified School District 

9333 Loch Lomond Dr. 
Pico Rivera, CA 90660 

(562) 801-7300 
www.erusd.org 

 

District Governing Board 

Dr. Aurora R. Villon 

Gabriel A. Orosco 

Lorraine M. De La O 

Dr. Teresa L. Merino 

Jose Lara 

 

District Administration 

Karling Aguilera-Fort 

Superintendent 

Mark Matthews 

Assistant Superintendent, Human 

Resources 

Jacqueline A. Cardenas 

Assistant Superintendent, 

Educational Services 

 

Gregory Fromm 

Assistant Superintendent, Business 

Services 

Dora Soto-Delgado 

Director, Student Services 

Reynaldo Reyes 

Director, Alternative/Adult 

Education 

Roberta Gonzalez 

Director, Early Learning Program 

Roberta Gonzalez 

Director of Early Learning Program 

 

2017-18 School Accountability Report Card for Rio Vista Elementary School 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Rio Vista Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

20 

20 

21 

0 

0 

0 

0 

0 

0 

El Rancho Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

355 

13 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Rio Vista Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.