Harvest Park Middle School is located in Pleasanton Unified School District and is one of three 
middle schools out of a total of fifteen schools. Pleasanton is an upper middle class community in 
the San Francisco Bay Area that places great emphasis on and devotes available resources to 
benefit all students. 
 
Principal's Message 
Harvest Park received the distinction of a California Gold Ribbon School in 2017 for our 
implementation of technology in the classroom. Our high performance can be attributed to staff 
development in standards alignment, Professional Learning Communities (PLCs), blended learning, 
character education, our highly qualified staff, very supportive parents and hardworking students. 
The staff and community at Harvest Park Middle School strive to maximize dynamic opportunities 
while continuing to have the highest expectations for achievement and personal growth for its 
students. 
 
Mission Statement 
All Kids Come First at Harvest Park Middle School where teachers, students, staff and community 
collaborate to educate the whole child. 
 
Vision Statement 
Harvest Park Middle School will continue to build on its tradition of providing a smooth transition 
from elementary to high school. Our supportive school community will guide students to become 
lifelong,self-motivated learners and leaders who are productive, responsible members of society. 
The unique needs of our students will be addressed by a wide variety of innovative programs and 
teaching styles.As a learning community we are committed to developing the whole child by 
providing a safe environment focusing on academics, arts, attitude, athletics, and activities. The 
highest expectations for academic success, technological expertise, and moral and civic 
responsibility will be required of our students as our standards-based curricula prepare them to 
compete and thrive in a diverse and changing global society. 

---

- 

-------- 

Pleasanton Unified School 

District 

4665 Bernal Ave. 

Pleasanton, CA 94566-7498 

(925) 462-5500 

www.pleasantonusd.net 

 

District Governing Board 

Mark Miller, President 

Valerie Arkin, Vice President 

Joan Laursen, Member 

Jamie Hintzke, Member 

Steve Maher, Member 

 

District Administration 

David Haglund, Ed.D. 

Superintendent 

Micaela Ochoa, Ed.D 

Deputy Superintendent, 

Business Services 

 

Odie J. Douglas, Ed.D 

Assistant Superintendent, 

Educational Services 

 

Julio Hernandez 

Assistant Superintendent, 

Human Resources 

 

Edward Diolazo 

Assistant Superintendent, 
Student Support Services 

 

 

2017-18 School Accountability Report Card for Harvest Park Middle School 

Page 1 of 10