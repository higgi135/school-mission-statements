Calavera Hills Middle School is a community of students, families, teachers and staff who are 
dedicated to providing a caring and safe environment in which students can pursue opportunities 
for greatness and growth. 
 
Our Mission: Calavera Hills Middle School is a student-centered learning community powered by 
meaningful, relevant and rigorous content. We use critical thinking, collaboration, communication 
and creativity to instill college and career skills in our students. Our core values include 
Inclusiveness, Respect, Boldness, Engagement, and Community 
 

----

---

- 

Carlsbad Unified School District 

6225 El Camino Real 
Carlsbad CA, 92008 

760-331-5000 

www.carlsbadusd.k12.ca.us 

 

District Governing Board 

Mr. Ray Pearson, PRESIDENT 

Ms. Kathy Rallings, VICE PRESIDENT 

Mrs. Veronica Williams , CLERK 

Mrs. Claudine Jones, MEMBER 

Mrs. Elisa Williamson, MEMBER 

 

District Administration 

Benjamin Churchill, Ed. D. 

Superintendent 

Mr. Chris Wright 

DEPUTY SUPERINTENDENT, 
ADMINISTRATIVE SERVICES 

Mr. Rick Grove 

ASSISTANT SUPERINTENDENT, 

PERSONNEL SERVICES 

Dr. Robert Nye 

ASSISTANT SUPERINTENDENT, 

INSTRUCTIONAL SERVICES 

 

2017-18 School Accountability Report Card for Calavera Hills Middle School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Calavera Hills Middle School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

0 

0 

0 

0 

0 

0 

27 

0 

0 

Carlsbad Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

574 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Calavera Hills Middle School 

16-17 

17-18 

18-19