Valencia Elementary School is located in an established neighborhood in the most northern part of 
Upland, California. Currently there are 24 classrooms of students in transitional kindergarten 
through sixth grade, and two specialized academic instruction classrooms for students in grades 
first through sixth. The school community is very supportive, both through PTA and a large group 
of dedicated parent volunteers. The staff is committed to their students and strives to move all 
students towards the proficiency of standards, challenge advanced students, develop a 
Professional Learning Community and explore ways to support students through Multi-tiered 
System of Support and Universal Design for Learning. Resources have been aligned to support 
student achievement and staff development activities are numerous. Valencia Elementary School 
welcomes approximately 630 students from a variety of backgrounds. Our culturally diverse 
population consists of 4% American-Indian, 13.3% Asian, 4% African-American, 32% Hispanic or 
Latino, and 50.4% Caucasian. 
 
The Upland Unified School District’s mission statement is, “Every Single Student...Every Single Day.” 
Our goal is that all students will acquire and apply knowledge in preparation for college and career. 
Student achievement results will show mastery of standards by individual students as well as by 
student sub-groups. It is Valencia's goal to educate students to reach their highest potential, 
thereby ensuring a successful future. Valencia Elementary School is dedicated to promoting 
academic achievement and positive social behavior by creating a safe, nurturing community of 
students, staff, and families. We are here to learn, grow, and become good citizens. 
 

2017-18 School Accountability Report Card for Valencia Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Valencia Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

26 

27 

27 

2 

0 

1 

0 

0 

0 

Upland Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

495 

10 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Valencia Elementary School 

16-17 

17-18 

18-19