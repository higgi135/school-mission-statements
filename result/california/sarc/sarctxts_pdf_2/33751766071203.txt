Superintendent’s message 
LEUSD is well positioned for the 2018 school year! The collaboration between voters, parents, 
teachers and staff has resulted in student achievement growth, improved facilities and playing 
fields under Measure V, and new instructional technology for classrooms. 
 
Under our state accountability system, the Fall 2017 update to the California School Dashboard was 
recently released. The Dashboard provides teachers and principals with valuable performance data 
that is examined weekly during their PLC collaboration time, and used to guide instruction. The 
current Dashboard shows LEUSD schools are making progress. 
 
LEUSD improved in several areas, though English Language Arts and Math growth indicators are 
little changed from a year ago, a trend statewide. The Dashboard is powered by six state indicators 
and four local indicators, along with a variety of custom reports by which to compare schools, 
districts, and student subgroups. Dashboard color codes reflect status and change to depict 
achievement growth ranging from Red (lowest) to Blue (highest). 
 
LEUSD performance highlights: 
SUSPENSION RATES—by lowering suspensions, results for the 'Suspension' indicator have 
IMPROVED from orange to yellow, changing from a high suspension rating to a medium suspension 
rating. 
‘EL’ PROGRESS—English Learners IMPROVED from yellow to green, changing from 'Medium' to 
'High' as a result of an additional 3.1% students making progress towards English proficiency. 
GRADUATION RATE—this indicator has IMPROVED from green to blue. The District continues to 
have a 'High' rating due to an increase in graduating students of 1.5%. 
COLLEGE/CAREER PREPAREDNESS—growing College & Career Preparedness is an area for 
increased attention. The CA School Dashboard shows 35.2% of LEUSD graduates as being 
'Prepared.” The State will not have a color indicator for College & Career Preparedness until 2018, 
but notably, LEUSD 11th grade students' ELA and Mathematics scaled scores increased in both 
areas respectively by 0.3 points and 4.2 points, a positive college readiness indicator. 
CHRONIC ABSENTEEISM—for the first time, the CA Schools Dashboard includes District and school 
Chronic Absenteeism rates, though a Chronic Absenteeism color indicator does not appear on the 
Fall 2017 report. District wide, LEUSD’s Chronically Absent statistic is 12.8%. 
LEUSD met all local indicators for implementing state standards, providing safe school facilities, 
adequate books and instructional materials, as well as meeting indicators for school climate, and 
student and parent engagement. View how LEUSD is performing at www.caschooldashboard.org. 
These are positive indicators, so let’s be mindful of the many positive accomplishments of 2017 to 
help set the bar high for 2018. 
 
Sincerely, 
Dr. Doug Kimberly, 
Superintendent 
 

----

--

-- 

Lake Elsinore Unified School 

District 

545 Chaney St. 

Lake Elsinore, CA 92530 

(951) 253-7000 

www.leusd.k12.ca.us 

 

District Governing Board 

Stan Crippen, Trustee Area 1 

Susan E. Scott, Trustee Area 2 

Heidi Matthies Dodd, Trustee Area 

3 

Juan I. Saucedo, Trustee Area 4 

Christopher J. McDonald, Trustee 

Area 5 

 

District Administration 

Dr. Doug Kimberly 
Superintendent 

Dr. Gregory J. Bowers 

Assistant Superintendent 

Dr. Alain Guevara 

Assistant Superintendent 

Dr. Kip Meyer 

Assistant Superintendent 

Arleen Sanchez 

Chief Business Officer 

Tracy Sepulveda 

Assistant Superintendent 

Sam Wensel 

Executive Director 

 

2017-18 School Accountability Report Card for Elsinore Middle School 

Page 1 of 8 

 

 

 
Through collaboration with the entire professional educational community, the staff will provide a student-centered, consistent program 
of instruction, showing yearly academic gains in an environment that considers the emotional, social, psychological, and physical well-
being of each student. 
 
Principal's Message 
Every student is a learner at Elsinore Middle School. Our teachers and support staff are committed to ensuring that all students are taught 
common core state standards while developing the individual student to be a life long learner and reaching their maximum potential. The 
Elsinore Middle School teachers and support staff are committed to providing a world-class education for all of our students. Teachers 
plan instruction collaboratively to ensure students are both supported with interventions and acceleration opportunities as appropriate. 
Elsinore Middle School continues to implement STEAM (Science, Technology, Engineering, Art, and Math) based instructional practices to 
align with our common core instruction in all subject areas. STEAM based instructional practices are being implemented to best ensure 
students are receiving not only a well balanced education but also developing critical thinking and collaboration skills pessary for the 21st 
century. Teachers continue to participate in Professional Learning Communities on designated Wednesday afternoons to discuss what we 
want our students to know, data about student understanding, and how to adjust instruction accordingly. Mission Statement: Through 
collaboration with the entire professional educational community, the staff will provide a student centered, consistent program of 
instruction, showing yearly academic gains in an environment that considers the emotional, social, psychological, and physical well being 
of each student. Vision Statement: We at Elsinore Middle School believe that education is the foundational base on which our society is 
built. The goal of our standards based academic program is to help produce responsible citizens capable of achieving at the highest levels. 
To facilitate our vision, we have adopted both a response to intervention academic model as well as a school-wide positive behavior 
support program (ROAR). 
James Judziewicz, Principal