Millbrook Elementary School, one of eighteen schools in the Evergreen School District, opened in 
September 1985. A portion of our facility, the multipurpose room, is shared (joint use agreement) 
with the City of San Jose for classes, after school programming, and community activities/events. 
This includes a pre-school program and “Camp Rock,” an after-school homework and recreation 
program, which is also available to Millbrook students through the City of San Jose. Our campus 
serves a student population that is ethnically, culturally, and linguistically diverse. 
 
Common Core Standards and 21st Century Learning are pillars of our philosophy and include 
communication, creativity, collaboration and critical thinking into our instructional program. We 
continue to build relevance between the academics of school and real world experiences through 
meaningful lessons integrating technology. In recent years our teachers have implemented new 
curriculum programs in math and most noteworthy, last year, in language arts. A history/social 
science adoption is looming. We look forward to the Next Generation Science Standards in the near 
future. This includes the future addition of a STEM-type (science, technology, engineering and 
math) double-sized classroom to provide a lab setting where students can spread out and utilize 
unique space and resources to enhance their learning. As a school with a focus on the future, we 
are researching ways to transition to a global literacy focus where the goal is to develop the ability 
of all students throughout our school to identify, interpret, analyze, synthesize, create, use, 
communicate, and share new knowledge using the written word, multimedia, and communication 
technologies with a worldwide audience. The Millbrook working definition of global literacy 
includes the following: 
 
Global Literacy is an understanding of how the world is organized and interconnected and the 
possibilities and constraints facing its people. Globally literate students analyze and think critically 
about the world and their roles in it. They become fluent investigators of the world, celebrating our 
diversity as global citizens. 
 
It is evident, that Millbrook's global literacy specialty area, logically lends itself to our district's 
Profile of a Learner (POL) where the desired result is promoting students as learners, 
communicators, collaborators, critical thinkers, innovators, and advocates. Here, students are 
engaged in authentic learning that prepares them with the skills to be global minded citizens. 
 
Additionally, Millbrook School provides a family-oriented, nurturing, and safe place for students to 
learn, grow, and develop intellectually. Teachers, staff, and administrators continue to act on the 
principle that students come first. As we always tell our students, it is important to have high 
expectations and set goals. This year is no exception. In addition to strong foundation skills, the 4 
C’s (communication, collaboration, creativity, and critical thinking) through the Common Core State 
Standards will be incorporated across the curriculum in our efforts to prepare our students for 
college and workplace. Through strategic, collaborative planning with teachers and parent leaders, 
we will meet the individual needs of all students. Additionally, we will address the academic 
challenges of those aforementioned standards as demonstrated by the California Assessment of 
Student Performance and Progress (CAASPP) along with other equally critical assessment pieces. 
Working together, the sky is the limit in what we can accomplish. 
 

2017-18 School Accountability Report Card for Millbrook Elementary School 

Page 1 of 10 

 

School Mission Statement: 
The Millbrook community works together to provide students opportunities to take risks in a safe, nurturing environment. We provide 
challenging experiences, which address the needs of the whole child while promoting responsibility, respect, and compassion for others. 
 
School Vision Statement: 
Millbrook’s vision is to foster thoughtful, caring, and creative students. They will be critical thinkers and problem solvers. Students will 
be confident, prepared for future success and achieve their academic and social potential. 
 
Community & School Profile 
Evergreen School District, located in the City of San Jose, is comprised of fifteen elementary schools and three middle schools. Once a 
small farming city, San Jose became a magnet for suburban newcomers between the 1960s and the 1990s, and is now the third largest 
city in California, behind Los Angeles and San Diego. The city is located in Silicon Valley, at the southern end of the San Francisco Bay Area, 
and is home to more than 1,000,000 residents. 
 
Millbrook Elementary School is located on the east side of the district’s borders. In the 2018-19, school year, we serve 621 students in 
grades kindergarten through sixth on a traditional calendar schedule.