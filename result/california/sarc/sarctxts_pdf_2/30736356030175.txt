Olivewood Elementary School is a community of responsible 21st century learners. We use our 
creativity, collaboration, critical thinking, and communication skills to grow into positive, 
productive citizens who will succeed in tomorrow's world. We have Mustang P.R.I.D.E. which stands 
for "Prepared, Respectful, Integrity, Dependable, Engaged." 
 
Classroom teachers grades K-6 implement Common Core State Standards. All students, including 
grades TK and students in Special Education classes are taught to think about "Why?" an answer is 
correct and "How" they know it to be true. We support instruction with integrated and designated 
English Language Development (ELD) for second language learners at all grades, and Advancement 
Via Individual Determination (AVID) Elementary school-wide. Olivewood also houses the innovative 
iEngage program, a one-to-one mobile technology program that transforms the way that teachers 
teach and students learn. Students use iPads in all grade levels, grades 3-6 have Chromebooks one-
to-one. Grades TK-6 use technology to work on enhancing their ability to create, innovate, learn in 
order to be college, career, and life-ready. 
 
 

----

-

--- 

Saddleback Valley Unified School 

District 

25631 Peter A. Hartman Way 

Mission Viejo CA, 92691 

(949) 586-1234 
www.svusd.org 

 

District Governing Board 

Suzie Swartz, President 

Dr. Edward Wong, Vice President 

Amanda Morrell, Clerk 

Barbara Schulman, Member 

Greg Kunath, Member 

 

District Administration 

Crystal Turner, Ed D. 

Superintendent 

Dr. Terry Stanfill 

Assistant Superintendent, Human 

Resources 

Connie Cavanaugh 

Assistant Superintendent, Business 

Laura Ott 

Assistant Superintendent, 

Educational Services 

Mark Perez 

Director, Communications and 

Administrative Services 

Dr. Ron Pirayoff 

Director, Secondary Education 

Liza Zielasko 

Director, Elementary Education 

Dr. Diane Clark 

Director, Special Education 

Rae Lynn Nelson 
Director, SELPA 

Dr. Francis Dizon 

Director, Student Services 

 

2017-18 School Accountability Report Card for Olivewood Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Olivewood Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

20 

23 

19 

0 

0 

0 

0 

0 

0 

Saddleback Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1,157 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Olivewood Elementary School 

16-17 

17-18 

18-19