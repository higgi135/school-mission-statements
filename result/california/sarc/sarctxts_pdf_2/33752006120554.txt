Warm Springs Middle School serves students in grades six through eight. The mission of Warm 
Springs Middle School is the same as the district’s mission: to inspire every student to think, to 
learn, to achieve, and to care. At Warm Springs we do that with P.R.I.D.E. "Purpose, Rigor, Integrity, 
Determination, and Empowerment" 
 
Our school is AVID focused and we are in our second year of Site of Distinction. Wildcats believe in 
providing a nurturing and flexible environment where students build self-esteem and feel safe to 
explore and expand their educational boundaries. Warm Springs has adopted and supports 
Positive Behavioral Intervention Supports education programs. 
 
We believe in working together in professional learning communities. Wildcats believe in the 
growth mindset and that all things are possible. Wildcats believe in taking pride in the campus. We 
believe that a clean campus is a reflection of our pursuit of excellence. Wildcats believe in a school 
climate that is safe and secure, and establishes a culture that promotes active learning. We also 
believe in clear, proactive communication that promotes a healthy, positive environment where all 
opinions are respected. Wildcats respect themselves and others’ values, beliefs, ideas, and 
differences. We are compassionate and kind towards others. 
 
Terry Picchiottino, Principal 
 
Major Accomplishments/Achievements: 
 

• 

In the third year of the California Assessment of Student Performance and Progress 
(CASSPP) testing, 57.6% of our students met, or exceeded standards in English Language 
Arts and 38.6% of our students met, or exceeded standards in Math. 

• AVID Site of Distinction and one year out from Demo Status 
• Gold Ribbon School Award 
• Warm Springs was selected by the Campaign for Business and Education Excellence as a 

2016 Honor Roll School for raising student achievement and closing achievement gaps. 

• Warm Springs Middle School offers intramural athletic competitions in several sports 

which help motivate students and create a high level of school spirit. 

 

 

----

-

--- 

----

---- 

Murrieta Valley Unified School 

District 

41870 McAlby Ct. 
Murrieta, CA 92562 

(951) 696-1600 

www.murrieta.k12.ca.us 

 

District Governing Board 

Ken Dickson 

Paul Diffley 

Linda Lunn 

Oscar Rivas 

Kris Thomasian 

 

District Administration 

Patrick Kelley 

Superintendent 

Mary Walters 

Assistant Superintendent of 

Educational Services 

Bill Olien 

Assistant Superintendent of 

 

Maintenance 

Darren Daniel 

Assistant Superintendent of 

Human Resources 

Stacy Coleman 

Assistant Superintendent of 

Business Services 

 

2017-18 School Accountability Report Card for Warm Springs Middle School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Warm Springs Middle School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

36 

37 

34 

2 

3 

0 

0 

3 

3 

Murrieta Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

922 

27 

50 

Teacher Misassignments and Vacant Teacher Positions at this School 

Warm Springs Middle School 

16-17 

17-18 

18-19