The Lakeview Elementary School Community believes that students can reach their highest 
potential and become lifelong learners through a variety of challenging educational opportunities. 
Such opportunities include a rigorous standards-based curriculum, differentiated instruction, and 
visual and performing creative outlets. Our school continues to utilize technology as a tool which 
promotes critical thinking and discovery, and also makes learning more accessible to each student. 
Lakeview educators are committed to growing professionally by learning about innovative 
classrooms and teaching practices. This allows our classrooms to exemplify best teaching practices. 
Lakeview School enjoys strong support from our parent community. Our teachers, parents, and 
staff believe in supporting every student attending this school with the means necessary to ensure 
student success. We also sponsor monthly “Walk to School Days” and have joined with the "I Drive 
25" program to promote student safety. All of these activities are created and sponsored by 
parents, teachers, and students. And, they include participation from local agencies. These types 
of activities foster the strong sense of community and parent involvement that we have come to 
expect at Lakeview Elementary. In addition, as a result of our effort and participation in the Walk 
America program, Lakeview has been awarded four sidewalk grants that provide safe passageways 
for students and parents as they travel by foot to and from school. 
 
Lakeview School offers its students a comprehensive program to meet each child’s unique 
academic, physical and emotional needs. Our strong curricular program is a balance of basic skills 
instruction, critical-thinking activities, active participatory learning and challenging enrichment 
opportunities meeting all of the California Content Standards. Our utilization of technology is top 
notch, with a minimum of three online computers in every classroom. Every student in grades 2, 3, 
4, and 5 has access to an individual iPad, pre-loaded with standards-based learning content. In the 
remaining grade levels, students have group sets of iPads to use during the academic day. We 
incorporate SmartBoard technology and curricular enhancements in grades K through 5. Online 
programs allow us to implement an on-site “blended learning “ model for our students. In addition, 
we offer 11 Spanish Immersion classes. These students are taught part of the day in Spanish, and 
part of the day in English. Students in our immersion program develop life-long advantages by 
becoming bilingual, bi-literate and culturally competent citizens. We firmly believe in educating 
the whole student by offering Band and music to students in grades 2-5. All students receive the 
benefit of art instruction offered by trained art specialists through the Art Docent Program (grades 
K-5). District multiple measures are used to assess the academic progress of each student several 
times yearly and gauge the overall strength of our academic program. District testing includes the 
Performance Assessment of Writing Skills, our Reading Inventories, (grades 2–5), and the district 
adopted Reading Assessment (grades K-5). Results of these assessments and more are shared with 
parents and used as ongoing assessments to modify instruction. Teachers meet weekly at 
Professional Learning Communities to design instruction to meet the needs of students based on 
four primary questions: 1) What do we want our students to learn? 2) How will we know when they 
have learned it? 3) How will we respond when they haven’t learned it? 4) How will we respond 
when they already know it? We use the previously mentioned data as well as team designed 
formative assessment data to answer these questions and inform our instruction. Lakeview 
Elementary School has adopted a Positive Behavior Interventions & Supports (PBIS) program as a 
means to provide a positive, safe and respectful environment for all members of the school 
community. After-school enrichment programs for students include: aerobic dance, reading, math, 
music, and science classes, focusing on various topics, are offered throughout the school year. 
Leadership opportunities are provided offering students in serving the school community through 
such means as Safety Patrol, Peace Patrol, and Lakeview Leadership. 
 
Staci Arnold, PRINCIPAL 

2017-18 School Accountability Report Card for Lakeview Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Lakeview Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

32 

35 

34 

2 

0 

1 

0 

1 

0 

Lakeside Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

281 

7 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Lakeview Elementary School 

16-17 

17-18 

18-19