Davidson Middle School is a sixth- through eighth-grade, comprehensive school located in the 
central section of San Rafael. Davidson students represent diverse ethnic, social, and economic 
backgrounds. Davidson offers a challenging and comprehensive common core state standards 
aligned academic program, as well as a variety of elective classes and enrichment. It is the mission 
of Davidson Middle School to create college, career, and community ready students for life through 
a quality education, which emphasizes rigorous academics, wellness, diversity, persistence and 
resilience, cooperation, and problem solving. 
 
School Wide Focus Areas for 2018-19: 

• 

• 
• 

School Culture and Climate and continued implementation of PBIS, Capturing Kids’ 
Hearts, Challenge Days, and an increased counseling team. 
Year two Implementation of A/B master schedule for Intervention/Enrichment 
Instructional Coaching and site based professional development in PLC protocols and 
Response to Intervention strategies. 

 
Davidson Middle School provides an appropriate, challenging program of classes for every student. 
This is accomplished by using student data as the foundation for development of each year’s master 
schedule. For example, students who demonstrate the need for support in English or Math receive 
a support classes called Extended Math or ELA. We provide English Language Development classes 
for all levels of English Learners, and students with disabilities receive services as outlined in their 
Individual Education Plans. 6th grade students participate in a Skills Wheel that cover four areas - 
Brainology, Team Building, Digital Citizenship, and AVID Strategies. 
 
The school offers a comprehensive program of elective classes, including Beginning, Intermediate, 
and Advanced Band, Orchestra, Steel Pans, Chorus, Art, Maker Technology, Computer Multimedia 
, Spanish, Chess, and an Elective Wheel for sixth graders. Davidson continues to make great 
progress ensuring that all students have access to an enrichment. 
 

2017-18 School Accountability Report Card for Davidson Middle School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Davidson Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

San Rafael City Schools 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

60 

57 

60 

1 

3 

2 

0 

0 

3 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

248 

0 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Davidson Middle School 

16-17 

17-18 

18-19