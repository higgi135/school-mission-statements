This is the 88th year that Shafter High has been dedicated to providing all students the opportunity 
to become academically successful as well as supporting the personal and social development of 
our students. We began the 2017-2018 school year with a focus on educating the whole student 
and believe we do this each and every day. With the implementation of PBIS, Shafter PRIDE, and 
recognizing that educating the whole child involves both social and academic expectations we are 
creating a learning environment that will matriculate successful young adults. 
 
Shafter High School is currently one of eighteen comprehensive high schools in the Kern High School 
District, the largest 9-12 school district in the state of California. The campus sits on a 79.99 acre 
site located in the central boundaries of the City of Shafter. The school is comprised of 66 
classrooms; an on-campus intervention facility; two computer labs; a library that consists of a 28-
station computer lab; a 983-seat auditorium with fly system and stage lighting; a cafeteria; a 
student store; two gymnasiums; two weight rooms; an agricultural farm that includes a mechanics 
lab, animal husbandry lab, and hay barn; an administrative building; and numerous athletic facilities 
which include a football stadium, baseball fields, softball fields, outdoor basketball courts, tennis 
courts, and practice fields. 
 
Shafter High’s staff consists of a principal, two assistant principals, a dean, a Title I/ELL coordinator, 
an athletic/activities director, 6 full-time counselors, a certificated on-campus interventionist, an 
interventionist, 72 certificated employees, and 86 classified employees. Of these staff members, 
14 teachers and 12 classified personnel are proud alumni of Shafter High School. Based on the 
most current data the ethnic make-up of the teaching and support staff is 47% White, 40% 
Latino/Hispanic, and less than 1% African-American. Administrators, teachers, instructional 
assistants, campus security personnel, food service workers, custodians, clerical staff, coaches, and 
all other staff members are committed to providing a clean, safe, and positive learning 
environment. Shafter High School serves a rural community ranging from upper middle class 
farmers to migrant field workers. The majority of the student population is comprised of students 
from lower socioeconomic levels. Shafter High’s major feeder school is the local Richland Junior 
High School; however, Shafter High is also home for students from Buttonwillow Middle School, 
Maple Middle School and Rio Bravo Greeley Junior High School. The school’s current enrollment 
for the 2018-2019 school year is 1703, and the student body demographics are 90.5% 
Latino/Hispanic, 7.2% White, less than 1% Asian, African American, Filipino, and Pacific Islander. 
The English Learner (EL) population represents 16.6% of the school’s enrollment and 79% of the 
student body is eligible for the National School Lunch Program (NSLP). Many of our students are 
second or even third generation Shafter High students and are proud to say they are “General born 
and General bred.” 
 
Shafter High School is proud to offer an afterschool snack program and dinner program available 
to all Shafter students for no fee. These nutritious meals are available 5 days a week during the 
academic school year. 
 

----

---- 

Kern High School District 

5801 Sundale Ave. 

Bakersfield, CA 93309-2924 

(661) 827-3100 

www.kernhigh.org 

 

District Governing Board 

J. Bryan Batey, President 

Joey O' Connell, Vice President 

Jeff Flores, Clerk 

Cynthia Brakeman, Clerk Pro Tem 

Janice Graves, Member 

 

District Administration 

Bryon Schaefer, Ed.D. 

Superintendent 

Scott Cole, Ed.D. 

Deputy Superintendent, Business 

Michael Zulfa, Ed.D. 

Associate Superintendent, Human 

Resources 

Brenda Lewis, Ed.D. 

Associate Superintendent, 

Instruction 

Dean McGee, Ed.D. 

Associate Superintendent, 
Educational Services and 

Innovative Programs 

 

2017-18 School Accountability Report Card for Shafter High School 

Page 1 of 15 

 

The City of Shafter is committed to 
providing educational enrichment 
opportunities to local students and 
families. In September 2012 the City 
partnered with Shafter High School 
and provided finances, tutors, and 
resources to establish the After-
School Tutoring Program. This 
program continues to be held in the 
library and is available to all Shafter 
High students three days a week 
from 3:00 to 7:00. The City has also 
partnered with Richland School 
District and the County of Kern to 
create the City’s Learning Center. 
The Learning Center shares a 
building with the Shafter Branch of 
the Kern County Library and, for a 
small fee, offers advanced and 
enrichment classes to all students. 
Additionally, the City of Shafter has 
graciously supported Shafter High to 
create a scholarship program for 
Advanced Placement (AP) students. 
AP students who attend three study 
sessions beyond the school day will 
receive a scholarship which will be 
strictly used to offset the cost of 
their AP test fees. 
 
Vision 
 

We at Shafter High School will ensure that all students learn the skills necessary to achieve success, 
while promoting academic responsibility, integrity, enthusiasm, and build upon the community's 
long standing traditions. 
 
Mission 
 
We, at Shafter High School are committed to ensure that all students learn the skills necessary in 
order to achieve future success. We commit to increasing literacy, academic achievement, social 
responsibility, and critical thinking by working collaboratively across the curriculum. 
 
Shafter High has chosen to use the acronym PRIDE for both our School-Wide Outcomes and PBIS. 
We are currently in the process of making this transition complete. The representation below 
demonstrates the two concepts: 
 
 
Shafter High has chosen to use the acronym PRIDE for both our School-Wide Outcomes and PBIS. 
We are currently in the process of making this transition complete. The list below demonstrates 
the two concepts: 
 
 
General PRIDE in ACADEMICS 
 
Problem Solver: Applying knowledge and skills 
Responsible Member of Society: Personal and social responsibility 
Independent Learner: Resourceful and intrinsically motivated 
Demonstrated Academic Achievement: Literacy and critical thinking 
Explore Post-Secondary Options: Pursue pathways and school requirements 
 
General PRIDE in BEHAVIOR 
 
Pride in our Shafter community and school traditions 
Respect for the classroom and each other 
Integrity to be honest to your teachers and yourselves 
Determination to complete all that is put in front of you 
Enthusiasm to come with a great attitude, and make everyday a great day!