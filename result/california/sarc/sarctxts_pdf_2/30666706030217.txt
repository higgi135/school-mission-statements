Andrew Jackson Elementary School (Jackson Elementary) is a Transitional Kindergarten through 
5th grade school located at 1143 South Nakoma Drive in Santa Ana, California. The school offers 
students a variety of educational programs, including Gifted and Talented Education (GATE), 
Sheltered English Immersion, English Language Mainstream, Inclusive Practices, and other 
designated instructional services. Other programs include a daily Lunchtime Fitness Program, No 
Excuses University, extended day Kindergarten, Head Start, AVID, and Engage 360° after school 
program. Students also participate in project-based learning, STEAM activities, and after-school 
tutoring. Using a variety of platforms, technology is embedded throughout the instructional day 
across all grades and subject areas. 
 
Vision Statement: 
The scholars at Jackson Elementary will be academically prepared and technologically proficient to 
effectively compete in the twenty-first-century global economy. 
 
School Mission: 
The teachers and staff at Jackson Elementary envision their students in the twenty-first century as 
accomplished, literate and technologically proficient citizens of the world. The teachers and staff 
will foster, promote, and encourage all students to reach their highest academic potential while 
acknowledging that each student has unique educational needs. In order to assist students in 
attaining academic excellence and technological competence, student learning will be facilitated 
by adhering to meaningful lessons using state adopted curriculum with fidelity that honors 
students' rich cultural background and provides multiple opportunities for student learning. 
Challenge based, active learning environments will be created that support all students in a fair, 
equitable, and respectful manner in order to prepare them for the ever-changing challenges of life. 
 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently 
serving nearly 49,300 students in grades K-12, residing in the city of Santa Ana. As of 2017-18, 
SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 alternative high 
schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or 
Reduced Price Meal program, 39% qualifying for English language learner support, and 
approximately 13% receiving special education services. Our district’s schools have received 
California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I 
Academic Achieving Schools, and Governor’s Higher Expectations awards in honor of their 
outstanding programs. In addition, 20 schools have received the Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have 
developed and maintained high expectations to ensure every student’s intellectual, creative, 
physical, emotional, and social development needs are met. The district’s commitment to 
excellence is achieved through a team of professionals dedicated to delivering a challenging, high 
quality educational program. Consistent success in meeting student performance goals is directly 
attributed to the district’s energetic teaching staff and strong parent and community support. 
 

2017-18 School Accountability Report Card for Andrew Jackson Elementary 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Andrew Jackson Elementary 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

40 

40 

33 

0 

0 

0 

0 

0 

0 

Santa Ana Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1986 

0 

11 

Teacher Misassignments and Vacant Teacher Positions at this School 

Andrew Jackson Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.