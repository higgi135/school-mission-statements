Mountain View Elementary School is a No Excuses University school guided by the belief that all 
children deserve to be educated in a way that prepares them for college. Expectations and rules at 
the school promote a culture of high achievement, college readiness, responsibility, respect, 
aspiration and outstanding effort.The school’s discipline philosophy promotes a safe, civil and 
productive school environment providing a solid foundation on which to build an effective school. 
The school incorporates strategies from the Positive Behavior Intervention and Support program. 
 
To build self-esteem, promote achievement, and aid in the prevention of behavioral problems, 
students are encouraged to participate in the school’s additional academic and extracurricular 
activities. Positive reinforcements are issued frequently to reward students for good citizenship 
and achievements. These include: classroom certificates, recognition, and other awards, Wildcat 
ROAR tickets, notes/phone call home, verbal praise for good work and behavior, prizes from ROAR 
prize menu which include Lunch with Principal, Monthly ROAR Assemblies, end of grading period 
assemblies, and ST Math and AR celebrations. 
 
Leadership at Mountain View Elementary School is a responsibility shared among district 
administration, the principal, the staff, students and parents. Jenny T. Le is the principal of 
Mountain View . Ms. Le also has experience in the Baldwin Park Unified School District where she 
served as a Categorical Resource Teacher. 
 

 

 

----
---- 
Azusa Unified School District 

546 South Citrus Ave. 

Azusa, CA 91702 
(626) 967-6211 
www.azusa.org 

 

District Governing Board 

Xilonin Cruz-Gonzalez 

Jeri Bibles-Vogel 

Yolanda Rodriguez-Pena 

Adrian Greer 

Gabriella Arellanes 

 

District Administration 

Linda Kaminski 
Superintendent 

Arturo Ortega 

Assistant Superintendent, 

Educational Services 

Marc Bommarito 

Assistant Superintendent, Business 

Services 

Jorge Ronquillo 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for Mountain View Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Mountain View Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

19 

17 

21 

0 

0 

0 

0 

0 

0 

Azusa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

441 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.