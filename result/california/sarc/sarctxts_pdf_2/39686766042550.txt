Elmwood Elementary School serves approximately 875 students in grades Kindergarten through 
8th Grade. In grades K through 3rd grade the student : teacher ratio is 24 to 1. In grades 4th 
through 8th the student : teacher ratio is 32 to 1. Elmwood School's student demographics are as 
follows: 87% - Hispanic; 9% - White; 1% Asian; 1% African American; and 1% Native American. 
 
 
School Mission: 
The staff at Elmwood Elementary School believes unwaveringly that all students can achieve 
academic standards by which students will understand how to think deeply and then be able to 
apply knowledge in new ways. Through arts infusion Elmwood School is nurturing the creative and 
intellectual side of our students thus, educating the whole child. 
 
 

--

-- 

----

---- 

Stockton Unified School District 

701 North Madison St. 

Stockton, CA 95202-1634 

(209) 933-7000 

www.stocktonusd.net 

 

District Governing Board 

Cecilia Mendez 

AngelAnn Flores 

Kathleen Garcia 

Lange Luntao 

Maria Mendez 

Scot McBrian 

Candelaria Vargas 

 

District Administration 

John E. Deasy, Ph.D. 

Superintendent 

Dr. Reyes Gauna 

Assistant Superintendent of 
Educational Support Services 

Craig Wells 

Assistant Superintendent of 

Human Resources 

Sonjhia Lowery 

Assistant Superintendent of 

Educational Services 

 

2017-18 School Accountability Report Card for Elmwood Elementary School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Elmwood Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

29 

31 

29 

2 

0 

5 

0 

7 

0 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Elmwood Elementary School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

1 

1 

1 

1 

1 

0 

2 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.