Creekside Oaks Elementary is located in the center of Lincoln, California. It is an established school 
with successful programs that meet the needs of all students. Our mission at Creekside Oaks 
Elementary is to building a community while empowering students with skills needed to be 
successful lifelong learners. The entire COES staff works together as a team with parents in a 
positive, productive, and professional manner to benefit students. Creekside Oaks Elementary 
Schools motto is "Community of Empowered Students". Creekside Oaks vision is “Upon promotion 
from Creekside Oaks - students will have the academic, social, language, technological, and 
organizational skills to be successful middle school students”. 

----

---

- 

Western Placer Unified School 

District 

600 Sixth Street Suite 400 

Lincoln, CA 95648 

(916) 645-6350 
www.wpusd.org 

 

District Governing Board 

Paul Carras 

Brian Haley 

Kris Wyatt 

Damian Armitage 

Paul Long 

 

District Administration 

Scott Leaman 
Superintendent 

Kerry Callahan 

Assistant Superintendent 

Educational Services 

Audrey Kilpatrick 

Assistant Superintendent Business 

Services 

Gabe Simon 

Assistant Superintendent Human 

Resources 

 

2017-18 School Accountability Report Card for Creekside Oaks Elementary School 

Page 1 of 10