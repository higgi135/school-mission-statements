Newport Heights Elementary School, home of the "Sharks," is located at 300 E. 15th Street in the 
city of Newport Beach. The school serves 586 students in grades Transitional Kindergarten through 
Sixth Grade. The school vision is referred to as "Educating for Success" and aptly reflects the belief 
that all students can, and will, succeed in school. It also speaks to a focus on teaching strategies 
and hands-on technological opportunities that prepare students for a rapidly changing world. The 
Newport Heights staff is actively involved in continuous professional growth and is committed to 
excellence in instructional practice. The teachers are interested in the latest educational research 
and its application to teaching in order to provide an enriching, nurturing and safe environment, to 
provide instruction in meaningful and challenging lessons, and foster relationships that are caring 
and respectful. 
 
Newport Heights boasts of a strong partnership of staff, students, parents, and community. This is 
evident in the active and supportive PTA, the abundance of quality volunteers, our School 
Foundation, and the encouragement and participation of community businesses and residents. The 
students truly are benefactors of this talented, energetic, and dedicated team. 
 
Newport Heights School provides a comprehensive program in all curricular areas based on state 
and district standards. Teachers meet by grade levels to discuss program content, pacing, student 
progress and interventions. The curricular program is complemented by various software 
programs; e.g., Lexia, ST Math, Accelerated Reader and IXL. Our focus on STEM activities with our 
Math and Science Family nights, participation in regional Science fairs, and Robotics teams with 
competitive experiences. In the 2018-19 school year, we have opened a state-of-the-art "Shark 
Tank" with video production capabilities, interactive video display, a news desk for news 
production, robotics, and coding. This space was funded through a generous local philanthropist-
provided matching grant and parent support. This space has been designed to support students in 
creating content of their own design and expression, using a variety of modalities and platforms 
and serves students TK - 6th grade. 
 
MISSION: The mission of Newport Heights Elementary School is to provide a comprehensive and 
challenging education in a safe and nurturing environment by building a strong partnership of 
students, staff, parents, and community where everyone is treated with respect and dignity. 
 
VISION: Educating for Success. 
 
At Newport Heights Elementary, we 

• Value partnership among staff, students, parents, and the community. 
• Provide a safe, caring, positive, encouraging, and supportive environment that 

enhances the educational experience for students. 

• Acknowledge that individuals are accountable for their own decisions and actions. 
• Understand, promote, and use technology as an integral component of academic and 

• 
• 

creative expression. 
Embrace, appreciate and encourage diversity. 
Establish a sense of responsibility in our students toward self, each other, the 
community, and world. 

• Recognize that every person has inherent worth and is capable of learning. 
• Demonstrate that respect for others is primary and essential. 

2017-18 School Accountability Report Card for Newport Heights Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Newport Heights Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

32 

33 

33 

0 

0 

0 

0 

0 

0 

Newport-Mesa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1105 

5 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.