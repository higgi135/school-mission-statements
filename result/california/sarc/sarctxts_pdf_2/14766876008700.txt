COMMUNITY & SCHOOL PROFILE 
Bishop is located in Inyo County along Highway 395, at the gateway to the Inyo National Forest, 
with a population of nearly 4,000. Bishop Unified School District strives to provide a quality 
education for all its students, with a wide range of programs and a talented staff dedicated to the 
needs of its population. The District serves approximately 2,000 students in grades K-12. 
 
Home Street Middle School is collaborating with the Bishop Union High School to correlate policies 
and expectations for their students. The schools hope this partnership will improve the transition 
students face when moving from middle school to high school. 
 
SCHOOL MISSION STATEMENT 
The mission of Home Street Middle School is to develop lifelong learners who possess the skills to 
further their knowledge and are positive contributors to our society. Our goal is to nurture the 
development of the whole person and to provide a safe environment that is developmentally 
responsive, socially equitable, and academically excellent. 
 

 

 

-------- 

Bishop Unified School District 

301 N. Fowler Street 

Bishop, CA 93514 
(760) 872-3680 

www.bishopschools.org 

 

District Governing Board 

Steve Elia 

Kathy Zack 

Trina Orrill 

Taylor Ludwick 

Joshua Nicholson 

 

District Administration 

Barry Simpson 
Superintendent 

Dr. Gretchen Skrotzki 

Principal - Elm, Pine and 
Community Day School 2 

Garrett Carr 

Asst. Principal - Elm, Pine and 

Community Day School 2 

Pat Twomey 

Principal, Home Street and 

Community Day School 

Derek Moisant 

Asst. Principal - Home Street and 

Community Day School 

Randy Cook 

Principal - Bishop High and 
Community Day School 3 

Dave Kalk 

Asst. Principal - Bishop High and 

Community Day School 3 

Katie Kolker 

Principal - Palisade Glacier High, 
Bishop Independent Study and 

Keith Bright High School 

 

2017-18 School Accountability Report Card for Home Street Middle School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Home Street Middle School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

22 

20 

21 

0 

1 

0 

0 

0 

0 

Bishop Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

95 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Home Street Middle School 

16-17 

17-18 

18-19