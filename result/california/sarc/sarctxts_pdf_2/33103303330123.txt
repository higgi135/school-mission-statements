The Riverside County Office of Education (RCOE) Alternative Education Court School provides a 
comprehensive instructional program for students in juvenile hall operated by Riverside County 
Probation. The Court School enrollment is determined by the Riverside County Juvenile Justice 
system (Courts and Probation). There are currently four Court School sites located at four juvenile 
hall sites in Riverside County. 
 
The Court school program is WASC accredited and provides state and county board of education 
approved core academic programs designed to meet the academic requirements for high school 
graduation or high school equivalency. The instructional program is focused on the California 
standards along with rigorous and relevant learning activities that include project-based learning, 
multi-tiered systems of supports (MTSS), high-impact classroom strategies and routines, Positive 
Behavioral Supports and Intervention (PBIS) with restorative practices, inter-disciplinary thematic 
lessons, and literacy across the curriculum. Students are enrolled in UC a-g courses and have an 
option to enroll in a Career Technical Education pathway or dual enrollment classes. Students are 
supported by systems to assess college/career/workforce readiness skills and have opportunities 
to complete the High School Equivalency Test (GED and HiSET). 
 
The Court School staff members focus on RCOE’s Mission, Vision, and Pledge: 
 
 Mission: The mission of RCOE is to ensure the success of all students through extraordinary service, 
support, and partnerships. 
 
Vision: RCOE will be a collaborative organization characterized by the highest quality employees 
providing leadership, programs, and services to school districts, schools and students countywide. 
 
Pledge: All students in Riverside County will graduate from high school well prepared for college 
and the workforce. 
 
The Court School engages students in learning activities in order to meet the following Schoolwide 
Learner Outcomes (SLOs): 
 
Riverside County Court School Students will be: 
 
Self-Directed, Life Long Learners 

• Who are motivated to meet their maximum potential 
• Who persevere and demonstrate the tenacity to overcome obstacles 

Outstanding Communicators Prepared for the 21st Century Workforce 

• Who use effective communication skills to solve problems collaboratively in the real 

world 

• Who use technology to enhance their learning 

Academically Proficient Learners 

• Who use academic language skillfully and apply critical thinking skills 
• Who graduate from high school well prepared for college and careers 

Responsible and Productive Citizens 

• Who advocate effectively for themselves and others with integrity 
• Who demonstrate respect for individual and cultural differences, and their community 

2017-18 School Accountability Report Card for Riverside County Juvenile Court 

Page 1 of 13 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Riverside County Juvenile Court 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

12 

12 

0 

0 

0 

0 

9 

0 

0 

Riverside County Office of Education 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Riverside County Juvenile Court 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.