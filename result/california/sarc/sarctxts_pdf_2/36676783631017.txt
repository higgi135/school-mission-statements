CHHS opened its doors to the freshman class of 2005 in September of 2001, as a two story modular 
campus with 500 students and 35 staff members. After 4 years, construction of the permanent 
facilities was complete. Since then, we keep expanding to almost 2900 students and 173 staff 
members. Over 90% of our students are from Chino Hills, with about 7% from Chino, and the rest 
from Ontario. The student body reflects the relative ethnic and economic diversity of those 
communities. Our attendance area covers the southern portions of the 3 cities, from the hills to the 
agriculture preserve, and includes the last open land in this rapidly developing area. From the 
beginning, our focus has been to establish and promote a positive and welcoming school climate. 
The high levels of trust and respect between students, parents, staff, and administration, are the 
hallmark of our school culture. Students, staff, and community embrace the traditions of Husky 
Pride. Maintaining trust and improving communication between stakeholders will continue to be 
our priority, to help CHHS perpetuate its reputation as “the Pride of the Hills.” 
 
Now 18 years later, Chino Hills High School has worked tirelessly to ensure that all students are 
successful in all four corners of our school crest (Academics, Athletics, Visual and Performing Arts 
and Activities). Students, staff and community members worked collaboratively to create and 
implement school wide behavior expectations: P.R.I.D.E. These expectations are the foundation of 
our school wide MTSS. Collectively we look to PLC's to assist us with analyzing data to look for 
areas of growth and to assist us in ensuring that we remain at the forefront of many innovative 
practices/programs. 
 
Chino Hills High School's vision is to be a school that encourages personal and academic 
achievement and integrity for all students in a challenging, safe, and supportive environment. 
 
Chino Hills High School's mission is to provide an environment of respect and cooperative learning 
among students, staff, and parents, where all students engage in relevant standards-based 
curriculum and activities that fosters responsibility and academic excellence. 
 
Chino Hills High School Beliefs: 
We believe passion drives life-long learning. 
We believe respect fosters growth and unity. 
We believe integrity promotes positive character and good citizenship. 
We believe determination cultivates success. 
We believe that excellence is achieved when we reach our individual potential. 
 

----

--

-- 

Chino Valley Unified School 

District 

5130 Riverside Drive 
Chino, CA 91710-4130 

(909) 628-1201 

www.chino.k12.ca.us 

 

District Governing Board 

James Na, President 

Irene Hernandez-Blair, Vice 

President 

Andrew Cruz, Clerk 

Christina Gagnier, Member 

Joe Schaffer, Member 

Alexi Magallanes, Student 

Representative 

 

District Administration 

 

Superintendent 

Sandra Chen 

Associate Superintendent, Business 

Services 

Grace Park, Ed.D. 

Associate Superintendent, 

Curriculum, Instruction, 
Innovation, and Support 

Lea Fellows 

Assistant Superintendent, 
Curriculum, Instruction, 
Innovation, and Support 

Richard Rideout 

Assistant Superintendent, Human 

Resources 

Gregory J. Stachura 

Assistant Superintendent, 

Facilities, Planning, and Operations 

 

2017-18 School Accountability Report Card for Chino Hills High School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Chino Hills High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

121.7 120.1 113.25 

0 

1 

1 

0 

3 

3 

Chino Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1199.9 

22 

2 

Teacher Misassignments and Vacant Teacher Positions at this School 

Chino Hills High School 

16-17 

17-18 

18-19