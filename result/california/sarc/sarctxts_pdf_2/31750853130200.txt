Victory High School is an educational option/continuation school for grades 11 and 12. It is a high 
school diploma program designed to meet the needs of students aged sixteen and older who have 
not graduated from high school. Our students are referred to our program for a variety of reasons 
ranging from needing a smaller school environment, a range of emotional and family issues, to 
credit deficiency and danger of not graduating. Supplemental programs and services include 
Regional Occupation Programs, career and mental health counseling, and work experience. Our 
goal is to graduate each Victory High School student with a high school diploma. 
 
Mission Statement 
 
The mission of Victory High School, a leading center of excellence for unique student opportunities, 
is to ensure each student reaches their full individual potential academically, socially, and 
emotionally as well as discover their purpose and passions through a school community 
distinguished by: 
Focusing on individual student learning objectives, college and career goals and life skills 
Providing a safe environment for academic, social and emotional needs, including reduced class 
sizes 
Cultivating self-discovery and advocacy through constructive risk taking 
Respecting diversity and promoting positive peer and community relationships 
 
School Motto: Motivate, Educate, Graduate 
 
Objectives: 
Each student will... 
graduate based on their individualized Graduation Plan 
demonstrate continuous progress toward improving proficiency in core subjects 
develop a sense of direction to transition confidently into post-secondary life, including completion 
of the Graduation Portfolio in a timely manner 
build positive relationships with peers, families, staff, and community members 
have an increased awareness of on-campus and community-based wellness resources as compared 
to baseline, by the end of the 2018-2019 school year. 
Demonstrate progressive personal growth when actively participating in wellness resources 
 
 
 

2017-18 School Accountability Report Card for Victory High School 

Page 1 of 9