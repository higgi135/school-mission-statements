School Vision: Horner Junior High’s vision is to provide an outstanding and well-rounded education 
to all of its students. This will be done by ensuring equitable outcomes, a safe campus, highly 
qualified professional teachers and staff, and opportunities for social growth. 
 
Mission Statement: It is Horner’s mission to provide a safe, purposeful, and caring environment 
conducive to learning where all students: 
1. Acquire knowledge for personal growth and confidence. 
2. Develop effective study habits and successful social skills. 
3. Learn how to manage personal property, develop self-discipline, and make proper decisions. 
4. Increase understanding of personal differences and treat all others with respect. 
5. Expand on tools for successfully managing different teacher requirements and classroom 
environments. 
 
Description of the School Community: Horner Junior High School is one of five Junior Highs in the 
Fremont Unified School District. The student population is comprised of approximately 1,245 7th 
and 8th graders. The school has a rich history that spans over fifty years. Horner is equipped with 
modern science labs and a well-maintained library. 
 
The school curriculum includes core curriculum courses in English, Mathematics, Science, Social 
Studies. All students have a Physical Education course along with electives in the following 
disciplines: Spanish, Chorus, Band, Leadership, Journalism, Drama, Technology, and Arts and Crafts. 
Honors classes are provided for English and Science. An acceleration pathway is available for Math. 
 
Horner has an outstanding activities program to help develop social skills. The ethnic diversity 
among students is integrated into all aspects of school life. The balance between academics, 
exploration, social opportunities, and athletics, together with a firm, fair behavior policy, results in 
an orderly, safe school. 
 
As Principal at Horner Junior High School, I am delighted to be a part of this community and proud 
of the school's many accomplishments and the dedication of our students, teachers and parents. 
Horner Junior High School was recognized as a Gold Ribbon School in the Spring of 2015. This is a 
reflection of the school and the community commitment to the students. 
 
Mrs. Jana Holmes 
 

2017-18 School Accountability Report Card for HORNER JUNIOR HIGH SCHOOL 

Page 1 of 9