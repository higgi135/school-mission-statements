Reedley High School (RHS) was established in 1898. With 79 classrooms, a performing arts theater, 
and a host of athletic facilities – including a football stadium, aquatics complex, baseball and 
softball diamonds, and basketball gymnasium – the high school is a focal point for the communities 
of Reedley and the surrounding mountain areas. 
 
The school served 1,803 students in grades nine through twelve during the 2016-17 school year 
and included a staff of 85 teachers. Reedley High School teachers and staff are dedicated to 
ensuring the academic success of every student and providing a safe and productive learning 
experience. 
 
Mission Statement 
Reedley High School will collaboratively empower students who will graduate as informed, ethical 
and respectful decision-makers. RHS staff will commit to a system of inquiry that guides immediate 
interventions. RHS students will demonstrate academic, technological, and 
individual 
achievements, which meet or exceed standards. the learning community will systematically 
structure academic, social, and safety networks which provide for individual learning needs, styles, 
and diverse backgrounds to build a PIRATE community. 
 

 

 

----

-

--- 

----

---- 

Kings Canyon Joint Unified 

School District 
1801 10th Street 
Reedley, CA 93654 

559.305.7010 

www.kcusd.com 

 

District Governing Board 

Craig Cooper 

Robin Tyler 

Manuel Ferreira 

Noel Remick 

Sarah Rola 

Clotilda Mora 

Jim Mulligan III 

 

District Administration 

John Campbell 
Superintendent 

Roberto Gutierrez 

Deputy Superintendent, Human 

Resources 

Monica Benner 

Assistant Superintendent, 
Curriculum and Instruction 

Mary Ann Carousso 

Administrator, Student Services 

Jose Guzman 

Administrator, Educational 

Programs 

Adele Nikkel 

Chief Financial Officer 

 

2017-18 School Accountability Report Card for Reedley High School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Reedley High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

76 

67 

64 

0 

0 

4 

0 

6 

0 

Kings Canyon Joint Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

422 

38 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Reedley High School 

16-17 

17-18 

18-19