Alternative Family Education (AFE) is a homeschool for 165+ students in kindergarten through 
twelfth grade. This school is for families who prefer an individualized, home and community based 
approach to their children’s education combined with the support of school district resources. AFE 
students meet regularly with consultant teachers, who help outline educational goals and 
objectives. They can also participate in weekly enrichment academic classes, high school sports 
(volleyball, basketball, and softball), theater productions, and field trips, including an annual trip to 
the Shakespeare Festival in Oregon. 
 
AFE is located on the vibrant Branciforte Small Schools Campus (BSSC). Other schools sharing the 
campus are Ark Independent Studies High School, Costanoa High School, and Monarch Community 
Elementary School. 
 
Alternative Family Education’s Vision and Mission Statements: 
Inspired Purpose: 
AFE supports, nurtures and inspires families and students to discover and develop their potential 
through engaged learning, problem solving, and community involvement. 
Vision: 
AFE cultivates... 
the ability to use our minds well. 
the tools to choose our paths. 
the ability to act upon our passions. 
the desire to contribute to the local community and the world. 
the vision and heart to see and feel things from other perspectives. the ability to express our own 
voices. 
the habit of self-reflection. a sense of well-being. 
 
Leadership 
Michelle McKinney is the principal of the school. She has been a parent at AFE in the past. 
 
Our Parent Club and Community Council continue to play key roles in shaping our students’ 
educational experience. 
 
 

----

---- 

Santa Cruz City Schools 
133 Mission St. Suite 100 

Santa Cruz, CA 95060 

(831) 429-3410 
www.sccs.net 

 

District Governing Board 

Sheila Coonerty 

Deedee Perez-Granados 

Cynthia Ranii 

Jeremy Shonick 

Patricia Threet 

Deborah Tracy-Proulx 

Claudia Vestal 

 

District Administration 

Kris Munro 

Superintendent 

Dorothy Coito 

Assistant Superintendent 

Educational Services 

 

Patrick Gaffney 

Assistant Superintendent 

Business Services 

 

Molly Parks 

Assistant Superintendent 

Human Resources 

 

 

2017-18 School Accountability Report Card for Alternative Family Education/Branciforte Small Schools Campus 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Alternative Family Education/Branciforte Small 
Schools Campus 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Santa Cruz City Schools 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

7 

0 

0 

7 

0 

0 

6 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

314 

8 

6 

Teacher Misassignments and Vacant Teacher Positions at this School 

Alternative Family 
Education/Branciforte Small 
Teachers of English Learners 
Schools Campus 
Total Teacher Misassignments 

Vacant Teacher Positions 
* 

16-17 

17-18 

18-19 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.