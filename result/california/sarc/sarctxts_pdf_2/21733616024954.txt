West Marin School (WMS) is one of five schools in the Shoreline Unified School District. Located in 
Point Reyes Station, it is surrounded by the Point Reyes National Seashore and borders the Tomales 
Bay. WMS is an elementary school serving approximately 137 2nd-8th grade students in a 
traditional school year calendar. The demographics of our students are 57 percent Latino, 41 
percent White, and 2 percent Asian. English Learners make up 47 percent of the school and 52 
percent are eligible for free or reduced lunch. Students are served by seven regular classroom 
teachers, a special day class teacher, a resource specialist, a literacy intervention instructional 
assistant, a part-time speech and language therapist, a Spanish teacher, and a music teacher. Our 
counseling programs consist of a part-time counselor and a part-time school psychologist. Students 
and teachers are also supported by six classified staff, including an art instructor and a library clerk. 
WMS offers its diverse student body a wide variety of academic and athletic programs, including 
an iPad for each student to use at school and to take home, an art program which also partners 
with local artists, a full-time music program, Spanish instruction for each student, whole-school and 
grade level physical education program, cross country, football, and track and field, library time for 
each grade level, and a school garden that provides fruits and vegetables periodically to the lunch 
program and community. 

---

- 

----

---- 

Shoreline Unified School District 

10 John Street 

Tomales, CA, 94971 

(707) 878-2266 

http://www.shorelineunified.org 

 

District Governing Board 

Jill Manning-Satori 

Tim Kehoe 

Avito Miranda 

Jane Healy 

Vonda Fernandes 

Heidi Koenig 

Ethan Minor 

 

District Administration 

Bob Raines 

Superintendent 

Ormides Trujillo 

Interim Fiscal Director 

 

2017-18 School Accountability Report Card for West Marin School 

Page 1 of 7 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

West Marin School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

 

 

 

10.92 11.92 

0 

0 

0 

0 

Shoreline Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

47.829

9 
0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

West Marin School 

16-17 

17-18 

18-19