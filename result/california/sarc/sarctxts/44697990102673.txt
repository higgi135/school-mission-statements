Welcome to Landmark Elementary, our school is 15 years old this year. Landmark Elementary School is your place to soar! Since the 
school's opening, we have worked hard to build a school community where children can learn and grow to their full potential. Our 
students, teachers, and families are our primary focus. We have a strong academic program in all curricular areas for all students. We 
identify and support students who need extra help, and through flexible grouping and team teaching, we encourage students who are 
working at an advanced level to work at an even higher level. All students participate in enrichment classes, such as PE, music, art, and 
science, in addition to a rigorous program of reading, writing, and mathematics instruction. 
 
We offer a strong English Language Development (ELD) program, in which students are grouped and team-taught as a grade level. We 
are working with West ED to further refine our abilities to meet the needs of our English Learners. We promote academic language 
development throughout the school day. We have high expectations for our students, and our excellent teachers work hard to 
promote student achievement. We care about kids and learning. We are proud of our school program and look forward to welcoming 
new students into our Landmark School family. Please come and visit! 
 
We will provide opportunities for individuals to share their strengths and widen their experience in a safe and structured environment. 
We will model and always encourage the pursuit of excellence. We will do all of these things by working hard, collaborating, reaching 
out to the community, setting goals and always believing in ourselves, and the students we are here to educate. Success builds on 
success, we believe all students can achieve!