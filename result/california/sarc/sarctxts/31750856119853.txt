Rock Creek Elementary School is one of fifteen schools in the Rocklin Unified School District. The 
school serves students in Transitional Kindergarten through 6th Grade (including 3 Pre-school 
programs. 1 SDC/ED classroom). Enrollment is approximately 620 students. The school is located in 
the center of a dynamic and supportive community which expresses its values toward education 
through a high level of participation. Rock Creek Elementary School opened in August, 2002 with 
a culturally rich and diverse population representing multiple different languages. 
 
The staff of Rock Creek provides an exemplary learning environment. Academic learning is certainly 
the primary focus of the school, but commendably, the staff operates on the principle that a caring 
environment which fosters self-worth and individual development is the best road toward realizing 
such learning. The students are able to approach all facets of their learning with confidence and 
the assurance that this school provides a safe learning environment. 
 
We believe that everybody should feel safe, secure, and accepted, regardless of color, race, gender, 
popularity, athletic ability, intelligence, religion, and nationality. At Rock Creek, parents and staff 
together are dedicated to creating a safe and orderly learning environment in which students 
interact positively with others. Parents play an important roll in Rock Creek through active 
participation and involvement in School Site Council, PTC, and volunteering. As a united community 
(students, parents, and staff), Rock Creek continues to push for excellence both inside and outside 
the classroom. 
 
 
The mission of Rock Creek Elementary School, an innovative and collaborative community of 
learners, is to empower students to succeed, grow, lead, and learn by nurturing: 
academic and social-emotional growth 
productive and compassionate citizens 
mindful and critical thinkers 
family and community partnerships 
 
Objectives: 
Students will learn mindfulness to develop social and emotional growth. 
Students will be challenged through a variety of learning opportunities to achieve their personal 
academic goals. 
Students will learn the value of being compassionate and contributing citizens in their community. 
 
 
 

2017-18 School Accountability Report Card for Rock Creek Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Rock Creek Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

31 

32 

32 

0 

0 

1 

0 

1 

0 

Rocklin Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

590 

5 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Rock Creek Elementary School 

16-17 

17-18 

18-19