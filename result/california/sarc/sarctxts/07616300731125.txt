Founded in 1962, Campolindo High School is one of four comprehensive high schools in the 
Acalanes Union High School District. As a California Gold Ribbon School and a National Blue Ribbon 
School, Campolindo is a source of pride for the communities it serves: Moraga, Lafayette, and 
Orinda. Accredited through the Western Association of Schools and Colleges, Campolindo High 
School is building upon its rich tradition of educational excellence through rigorous and engaging 
programs. Students excel in curricular programs that foster preparation in the core academic 
disciplines, as well as the arts and technical fields. Campolindo has a strong college preparatory and 
Advanced Placement program. The school’s wide variety of classes and academic support services 
ensure that all students, no matter their academic level, receive an excellent education. Faculty 
members work to ensure that Campolindo’s academic programs not only provide core content 
knowledge, but promote critical thinking, collaboration, and creativity. The staff is also dedicated 
to maintaining a culture that provides every student with a safe and welcoming environment. 
 
Recognizing the importance of educational experiences outside of the classroom, Campolindo 
provides students with a full array of extra and co-curricular opportunities. By participating in over 
one-hundred school clubs, Campolindo students are able to pursue academic, community service, 
and cultural interests outside of their regularly scheduled classes. Clubs such as Debate, Robotics, 
Model United Nations, and Academic Decathlon have recently won regional, state, and national 
competitions. Campolindo’s student athletes participate in twenty-two sports and regularly 
compete for league, regional, and state titles. 
 
With high levels of academic achievement, strong student support systems, award-winning extra 
and co-curricular programs, and a dedicated staff of educational professionals, Campolindo 
continues to fulfill the school’s mission: 
Campolindo High School fosters academic achievement and cultivates personal growth in a 
supportive educational community to prepare all students for a successful future. 
 

 

 

----

-- 

----

--- 

Acalanes Union High School 

-

District 

1212 Pleasant Hill Rd. 
Lafayette, CA 94549 

(925) 280-3900 

www.acalanes.k12.ca.us 

 

District Governing Board 

Robert Hockett 

Kathleen R. Coppersmith 

Richard Whitmore 

Susan L. Epstein 

Nancy Kenzierski 

Komal Sethi, Student Member 

 

District Administration 

John Nickerson, Ed.D 

Superintendent 

Aida Glimme 

Associate Superintendent 

Educational Services 

 

Amy McNamara 

Associate Superintendent 
Administrative Services 

 

Julie Bautista 

Chief Business Official 

Business Services 

 

Karen Heilbronner 

Director, 

Special Education & Auxiliary 

 

Adriana Martinez, LCSW, PPSC 

Director of Wellness 

 

2017-18 School Accountability Report Card for Campolindo High School 

Page 1 of 13 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Campolindo High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

68 

73 

69 

0 

2 

0 

0 

1 

0 

Acalanes Union High School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

5 

3 

292 

Teacher Misassignments and Vacant Teacher Positions at this School 

Campolindo High School 

16-17 

17-18 

18-19