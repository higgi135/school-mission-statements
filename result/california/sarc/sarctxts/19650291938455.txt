Designated in 2013 as a California Distinguished School and in 2017 as a California Gold Ribbon 
School, South Pasadena High School is a public, coeducational, four-year comprehensive high 
school with an enrollment of approximately 1,450 students. There is a student/teacher staffing 
ratio of 32 to 1. Through special parcel tax funding, ninth grade English Language Arts classes are 
maintained at a size of twenty-five or fewer students. Classes meet for 100-minutes on an 
alternating block schedule with a daily seventh period that meets for fifty-minutes. Two semesters 
comprise the 180-day academic year. The co-curricular program is extensive, ranging from athletics 
to theater, music, art, dance, and numerous clubs of student interest. The school is accredited by 
the Western Association of Schools and Colleges (WASC) and has maintained an excellent record 
of accreditation throughout the school’s existence, with our last full accreditation review in the 
2016-2017 school year. The SAT continues to trend upward, both in scores and number of test 
takers, and the same is true of our Advanced Placement program that earned exclusive recognition 
from the College Board. South Pasadena High School is steeped in traditions that help us maintain 
our Tiger Spirit. At the same time, we are in a continuous improvement cycle, researching and 
implementing programs and techniques focused on student achievement and personal growth. 
Career Technical Education provides myriad opportunities 
in the areas of business, 
entrepreneurship, woodworking, sports medicine, computer animation and design, silk screening, 
photography, and more. In the 2016-2017 school year, the school was awarded its Gold Ribbon for 
an exemplary and comprehensive pathways program for students. 
 
Originally established in 1905, South Pasadena High School has served students in the community 
of South Pasadena in grades nine through twelve since 1984. Through its 112-year history, the 
school has grown and changed in many ways, including a 2000-2003 remodeling and modernization 
program brought about by a 1995 bond measure. Construction began in January 2018 on a new 
Mathematics and Science building that has four new science labs, six mathematics classrooms, and 
an engineering room. Students began the new semester in January 2019 in the new math/science 
complex. An additional life science lab is being added to our main science building. Teachers, staff, 
and administrators act on the principle that students come first. The educational programs at the 
school are tailored to meet the needs of each individual student. 
 
South Pasadena High School and South Pasadena Unified School District adhere to these core 
values: 
 
All students can learn and reach their full potential. 
Parent and family involvement in the student's education is critical to the success of students, of 
individual schools, and of the district as a whole. 
Building confidence and character are important to academic achievement. 
Mutually respectful relationships are essential in a diverse organization and community. 
Clear communication among administrators, teachers, parents, students and the community is 
essential to building trust. 
Transparency in governance and operations is essential to building trust. 
Timely and appropriate responsiveness to students, parents and families is essential. 
The work of all employees is indispensable to the well-being and success of students. 
Continuous improvement for all employees is critical to the success of the district. 
 

2017-18 School Accountability Report Card for South Pasadena Senior High School 

Page 1 of 10 

 

Mission Statement 
South Pasadena High School students are grown locally to make a positive impact globally, deep into the 21st Century. 

 

Vision Statement 
South Pasadena High School students develop the academic and interpersonal skills that help them make a positive impact as global 
citizens. Staff, parents, and the community provide the supportive environment in which students achieve their personal bests. 
 
South Pasadena High School is committed to the following propositions: 

1. All students are entitled to an interdisciplinary educational 

7. Students become lifelong learners when exposed to a wide 

foundation. 

range of ideas and disciplines. 

2. All students see themselves as productive and responsible 

8. A growth mindset inspires students to discover and 

global citizens. 

manifest their untapped potentials. 

3. All students are critical thinkers, self-advocates, and 

resilient problem-solvers. 

9. All 

students 
disagreement. 

recognize 

the 

value of 

respectful 

4. Diversity enriches our campus life. 
5. Students benefit from a nurturing environment. 
6. Students construct meaning through collaboration and 

interaction with others. 

 

Janet Anderson, Principal