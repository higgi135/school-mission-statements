Our mission is to provide a place where all students and teachers strive for excellence in academics, where social and emotional 
growth are nurtured, and habits of lifelong learning are developed. Moulton Hawks (students) will learn and grow in a positive 
atmosphere where teachers, staff, parents and students are enthusiastic about the learning and teaching process. 
 
Located on top of a scenic hillside near the Pacific Ocean in the City of Laguna Niguel, Moulton Elementary School has been successfully 
providing a high-quality educational program for students since 1975. 
 
When forming classes, staff members consider academic needs, language development needs, behavior considerations, and academic 
levels to place students in the best learning environment. Teachers differentiate their curriculum to meet the various academic levels 
of students in their classrooms. Students who qualify for the Gifted and Talented Education (GATE) program are placed in GATE cluster 
groups. Special Education services are also available to students with an Individualized Education Plan (IEP). 
 
Moulton School has a positive and influential impact on students, parents and community members. 
For additional information about school and district programs, please visit www.capousd.org