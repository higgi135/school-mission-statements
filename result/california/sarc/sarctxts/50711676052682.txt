Fairview Elementary School is a K-6 Elementary School located in South Modesto that also houses two Head Start classrooms. The 
school population includes, 83% Hispanic, 3% White, 2% African American and 12% other or did not state. 63% of our students are 
English Language learners. 96% of our student population is Socioeconomically Disadvantaged and 8% of our student make-up includes 
Students with Disabilities. The school currently has 835 students enrolled and participating in a traditional schedule. The site provides 
both sheltered English instruction as well as a focused character education program that is consistent across Modesto City Schools. 
Mission Statement: All Fairview Falcons, students and staff, will learn and grow every day. 
 
Fairview staff and administration are working in collaboration to become proficient in the Common Core State Standards. Working 
together to refine lesson delivery and design, teachers and students are able to benefit from a structured and rigorous educational 
environment that offers ongoing checks for understanding, continuous use of student engagement strategies and consistent 
implementation of instructional norms. Addressing areas of need through intervention strategically, while accelerating learning 
through extension provides differentiated instruction for the wide range of diverse learners we serve. Focusing on vocabulary 
development, building of academic language, skills, and concepts; Fairview is working to move our students into literacy and the 
development of critical thinkers.