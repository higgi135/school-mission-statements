Mission Statement: 

 

• At Cordova High, we lead by example. We are citizens of the world and show respect for others through kind words and 
actions. We put forth our best effort and work hard to achieve academic excellence. We grow by taking risks and learning 
from our mistakes. We believe laughter is an important part of learning. With courage, this is who we are, especially when 
no one is looking. 

Vision Statement: 

• At Cordova High School we question, learn, think, care, and act! 

CHS Goal: 

• College and Career Readiness for All! 

Cordova High School's vision and mission are based on our embracing of the IB Learner Profile as our new Schoolwide Learner 
Outcomes: 
 
INQUIRERS 
 
They develop their natural curiosity. They acquire the skills necessary to conduct inquiry and research and show independence in 
learning. They actively enjoy learning and this love of learning will be sustained throughout their lives. 
 
KNOWLEDGEABLE 
 
They explore concepts, ideas and issues that have local and global significance. In so doing, they acquire in-depth knowledge and 
develop understanding across a broad and balanced range of disciplines. 
 
THINKERS 
 
They explore concepts, ideas and issues that have local and global significance. In so doing, they acquire in-depth knowledge and 
develop understanding across a broad and balanced range of disciplines. 
 
COMMUNICATORS 
 
They understand and express ideas and information confidently and creatively in more than one language and in a variety of modes 
of communication. They work effectively and willingly in collaboration with others. 
 
PRINCIPLED 
 
They act with integrity and honesty, with a strong sense of fairness, justice and respect for the dignity of the individual, groups and 
communities. They take responsibility for their own actions and the consequences that accompany them. 
 
 
 

2017-18 School Accountability Report Card for Cordova High School 

Page 2 of 14 

 

OPEN-MINDED 
 
They understand and appreciate their own cultures and personal histories, and are open to the perspectives, values and traditions of 
other individuals and communities. They are accustomed to seeking and evaluating a range of points of view, and are willing to grow 
from experience. 
 
CARING 
 
They show empathy, compassion and respect towards the needs and feelings of others. They have a personal commitment to service, 
and act to make a positive difference to the lives of others and to the environment. 
 
COURAGEOUS (Formally "Risk Taker") 
 
They approach unfamiliar situations and uncertainty with courage and forethought, and have the independence of spirit to explore 
new roles, ideas and strategies. They are brave and articulate in defending their beliefs. 
 
BALANCED 
 
They understand the importance of intellectual, physical and emotional balance to achieve personal well-being for themselves and 
others. 
 
REFLECTIVE 
 
They give thoughtful consideration to their own learning and experience. They are able to assess and understand their strengths and 
limitations in order to support their learning and personal development. 
 
Our student enrollment reported on the California Basic Educational Data System (CBEDS) in October 2017 was 1771