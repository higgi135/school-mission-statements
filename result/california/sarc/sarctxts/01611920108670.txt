Leadership Public Schools is a network of urban charter high schools whose mission is to create 
educational equity. We empower students for college, career, and community leadership and share 
our practices on a national scale. 
 
LPS Hayward prepares all students to apply to, attend and successfully graduate from college. 
Through a rigorous academic program, students develop strong analytical and argumentative skills 
to be effective thinkers in any discipline. At LPS Hayward, students approach their academic work 
with a growth mindset and draw on a set of character strengths and habits to collaborate, problem 
solve and achieve their goals. We believe in the potential of every student to work hard, overcome 
challenges and live the life of their dreams. Every student is on a path to college. To support them 
on this journey, staff put students at the center of learning. Students work with peers to find 
evidence, discuss ideas, and solve problems. College counselors provide students with tools to learn 
about, apply to and prepare for college. Support staff work and connect with students to develop 
skills that will allow them to live full, independent lives. 
 
Program Highlights: 
LPS Hayward offers a 3-day leadership retreat at UC Santa Cruz for all incoming 9th graders 
95% of the 2018 CLASS has or is currently doing community service 
Our Maker/Design Lab is an art elective course where students become confident Makers and 
Designers through the Design Thinking Process 
 
Outcome Highlights: 
64% OF THE 2018 CLASS HAS OR IS CURRENTLY TAKING AN AP COURSE. 
94% OF LPS HAYWARD STUDENTS TOOK THE ACT EXAM COMPARED TO 31% OF STUDENTS IN THE 
REST OF THE STATE OF CA. 
98% OF OUR GRADUATES ENROLL IN COLLEGE. WE ARE COMMITTED TO ENSURING SUCCESS 
BEYOND HIGH SCHOOL. 
 

 
 

 

Leadership Public Schools 

99 Linden Street 

Oakland 

510-830-3780 

http://www.leadps.org 

 

District Governing Board 

Sandra Becker 

Chuck Bowes 

Brentt Brown 

Luis Chavez 

Lynne Dantzker 

Jesuscita Fishel 

Kelly Gulley 

Briggitte Lowe 

Tana Monteiro 

 

District Administration 

Patricia Saddler 
Superintendent 

 

2017-18 School Accountability Report Card for Leadership Public Schools - Hayward 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Leadership Public Schools - Hayward 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

22 

20 

5 

0 

9 

0 

19 

12 

0 

Leadership Public Schools 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

19 

12 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.