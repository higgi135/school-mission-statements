Monte Rio Union School District 
Vision Statement: 
 
Monte Rio Union School provides a safe and nurturing environment where all students are given the skills, opportunity, and 
encouragement to prepare for a successful future as critical and creative thinkers, productive citizens and lifelong learners. 
 
 
Mission Statement: 
 
Our mission is to ensure that every student receives a well-rounded, differentiated, academic program and develops a strong social 
and emotional foundation. 
 
We expect a high level of academic achievement in all students, as they perform the Common Core State Standards using 21st Century 
skills, such as critical thinking, collaboration, creativity and communication, as demonstrated via proficient performance on the 
Smarter Balance exam. 
 
Teachers, support staff, administrators, students, parents, and members of the community are committed to providing on-going 
support and enrichment to attain these goals. 
 
District/Board Goals: 
 
1. Students will have a safe, comfortable school, with an up-to-date infrastructure, and adequate indoor and outdoor equipment and 
facilities, with which to learn and thrive. 
2. Students will be proficient in grade level standards. 
3. Parents will be more involved on school campus at events and in the classrooms. 
4. Maintain a safe campus, where students feel safe and secure at school. 
5. Monte Rio School will promote wellness, nutrition and physical fitness among all students K-8. 
6. Monte Rio School will provide students with an enriched education including: performing & visual arts, PE, and technology 
 
 
 

2017-18 School Accountability Report Card for Monte Rio Elementary School 

Page 2 of 10