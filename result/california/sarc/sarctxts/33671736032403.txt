The mission of Cathedral City Elementary School is to provide and continually enhance a highly 
academic and safe environment through a collaborative partnership with staff, families and the 
community that, despite all challenges, results in students who experience excellence in education 
and technology, master state standards, succeed on standardized tests, and are prepared as 
responsible and independent members of society. 
 
Cathedral City Elementary School is located in Cathedral City, California within the Palm Springs 
Unified School District. Cathedral City Elementary School provides services for approximately 675 
students in grades TK-5th. 
 

----

---

- 

Palm Springs Unified School 

District 

150 District Center Drive 
Palm Springs, CA 92264 

(760) 883-2700 
www.psusd.us 

 

District Governing Board 

Richard Clapp, President 

John Gerardi, Clerk 

Karen Cornett, Member 

Madonna Gerrell, Member 

Timothy S. Wood, Member 

 

District Administration 

Sandra Lyon, Ed.D 
Superintendent 

Michael Swize, Ed.D 

Assistant Superintendent, 

Educational Services 

 

Tony Signoret, Ed.D 

Assistant Superintendent, 

Human Resources 

 

Brian Murray, Ed.D. 

Assistant Superintendent, 

Business Services 

 

 

2017-18 School Accountability Report Card for Cathedral City Elementary 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cathedral City Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

35 

33 

32 

0 

0 

1 

0 

2 

0 

Palm Springs Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1038 

18 

34 

Teacher Misassignments and Vacant Teacher Positions at this School 

Cathedral City Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.