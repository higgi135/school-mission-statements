In the 2016-17 school year, Hoover School re-opened it's doors after being closed for 43 years. This gave us the unique opportunity 
to build a school community from the ground up. Our overarching goals include: 
1. Building an inclusive community where students and parents feel welcome and part of the process. 2. Creating a nurturing and 
safe place for students to learn. 3. Building a strong academic program coupled with an emphasis on inspiration, self discovery, 
creativity and teamwork. 4. Preparing students for a modern world yet maintaining the spirit of community and connection to the 
past. 
 
At the start of our opening school year, we asked students, staff and parents to brainstorm words and short statements that we hoped 
would best describe our new school. These conversations and community experiences led to the creation of our vision - "Hoover 
Hawks ~ Together We Soar" - believing that with a strong community, we can accomplish all that we set out to do.