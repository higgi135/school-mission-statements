At Sleepy Hollow Elementary School we work together in order to provide a safe and caring 
environment for all students. The culture of our school reflects a commitment towards high 
academic achievement for all. 
 
We provide a variety of opportunities for students that create a positive learning environment and 
promotes student achievement. One such opportunity is our school-wide weekly assemblies. Each 
assembly is held to promote school cohesiveness, team effort, and friendship. During these 
assemblies, we sing our school spirit song, recognize special events, celebrate student 
achievements, and meet new people. In addition, behavioral expectations, school rules, and other 
important information are shared with the group. 
 
We are very proud of the many hours parent and community members provide for us each month. 
Parents are encouraged and welcomed to participate in the education of their children by serving 
on school committees or as classroom volunteers. Through parent donations and the funding from 
Education Foundation Orinda (EFO), enrichment opportunities are also provided for all students. 
These programs include weekly instruction in art, music, and physical education instructed by 
credentialed teachers. There is a library technician who supports teachers and children during their 
weekly visits to the school library. 
 
Our Character Development curriculum includes character education, focusing on the values of 
respect, responsibility, a positive attitude, honesty, empathy, and self-discipline. Students benefit 
from instruction from trained parent volunteers and teachers using the District-adopted Think First, 
Second Step program, a nationally recognized character education program. During the 2018-19 
school year, Sleepy Hollow students at every grade level have participated in multiple class visits 
from Kidpower, an organization that focuses on helping students to use their power to stay safe, 
act wisely, and believe in themselves. Classroom teachers have also implemented components of 
the Caring School Community program, which focuses on the benefits of creating a classroom 
community. 
 
We believe that when staff and parents work together to provide a positive learning climate, the 
children we share will become empowered individuals who can apply what they learn to become 
the problem solvers of tomorrow. 
 
School Vision Statement 
Our school’s Vision is to inspire students who are capable of meeting any academic challenge with 
openness, enthusiasm, and a willingness to solve problems. Our school community is committed to 
developing students who are resilient and equipped with the knowledge, skills, and character to 
become empathetic, visionary, and socially responsible contributing members of society. 
 

2017-18 School Accountability Report Card for Sleepy Hollow Elementary School-see attachment 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Sleepy Hollow Elementary School-see 
attachment 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

27 

24 

21 

0 

0 

0 

0 

0 

0 

Orinda Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

159 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.