MISSION STATEMENT 
Guiding the discovery of knowledge, Mission Avenue Open School inspires and engages all students to become responsible citizens 
and creative, confident, critical-thinking problem solvers through student-centered interdisciplinary learning, enhanced by 
experiential explorations in respectful collaborations within our community. 
 
SCHOOL PROFILE 
Mission Avenue Open School is an officially-designated alternative school in the San Juan Unified School District. 
 
Mission School is guided by a unified philosophy that provides a high quality, standards-based, and comprehensive elementary school 
education through experiential learning. This is called the Open Philosophy and is based, in part, on Piaget’s developmental learning 
theory, Dewey’s constructivist learning theory, Montessori’s use of manipulative materials, and Kohl’s thematic, integrated 
curriculum. In open classrooms, emphasis is placed on individuality and reaching each child’s potential. We have found that the best 
way to provide for this is through the following practices and themes: differentiation of instruction, experiential learning and 
constructivism, student-centered collaborative classrooms, field education, and parent involvement at all levels of the school program. 
 
We believe applications of technology are critical to teaching and learning at all grade levels. To support this belief we have a parent-
led group called MAST whose primary goal is to raise funds to outfit all classes with iPads, NetBooks, or desktop computers. 
Additionally each of our classrooms has an interactive whiteboard or an Apple TV set up. All classrooms are WiFi enabled as we 
recognize that use of the Internet is one technological key for students' success and, as such, plays an important role in hands-on 
learning, daily practical applications, and 21st century skills which are so important for each student’s future. 
 
To support our belief that participation in the visual and fine arts is a critical component in each child’s educational program, Mission 
School offers band and choir programs, as well as multiple opportunities for all children to perform in grade-level and school-wide 
performances. 
 
PRINCIPAL'S MESSAGE 
Children are a gift--each day that we see them engaged in learning alongside us, we recognize and are thankful for that gift! Our 
philosophy at Mission is much more than a slogan, much more than a sign we hang on the building - our philosophy, our community, 
and our teaching methods are our PRIORITY, and the success we see is the result. 
 
At Mission Avenue Open School, we are committed to the idea that parents are an integral part of the learning experience, both inside 
and outside the classroom. Learning is best accomplished when teachers, parents, and students work as a team. Hand in hand, as 
part of the team, making academic choices, setting goals, and evaluating progress, students become responsible lifelong learners. We 
continuously encourage parent volunteerism, and those hours extend into our experiential learning activities where children are on 
single day and multi-day field trips to take the learning outside of our four walls of the classroom. We call this an "Open Philosophy" 
and we are committed to the Open Philosophy because we know it works, and we believe it is what’s best for children. We welcome 
the participation of all families who embrace this philosophy, who recognize the remarkable, exciting, and distinguished learning 
community we've created at Mission Avenue Open School. 
 
During this school year our focus is upon K-2 guided reading, literacy, K-6 Math, and newly adopted materials which all support 
Common Core State Standards (CCSS) and rigorous instruction with powerful assessments that teach us more about what our students 
are capable of doing and what they are ready to do next. Staff are poised for engaging students in critical thinking work aligned with 
the Common Core State Standards, while continuing to guide children through learning experiences that are constructivist at the core. 
With much positive momentum, we are looking forward to our next steps with children. 
 

2017-18 School Accountability Report Card for Mission Avenue Open Elementary School 

Page 2 of 11 

 

At Mission Avenue Open School we thrive by working with our community to engage children in rich learning experiences. The learning 
here at Mission is unique and most certainly special. We engage children in parent-funded learning experiences that take place outside 
of the classroom, and we ask children to make meaning of their learning and put it into action. Our philosophy stretches across every 
aspect of what we do. If you are interested in attending the school site on a regular basis and find that our philosophy is a good match 
for your child, we encourage you to call or visit our site and learn more. You can also email us at: Margaret.terzich@sanjuan.edu