Vision Statement: 
San Joaquin High School's vision is to encourage, guide, and support each student to rise to their highest potential in 21st Century 
skills so that they may be effective communicators, innovators, and participants in the global economy. 
 
Mission Statement: 
San Joaquin High School's mission is to ensure each student graduates with a high school diploma and is ready to enter college and/or 
a career technical school. 
 
San Joaquin High School (SJHS) a continuation high school housed at the McFarland Learning Center campus. SJHS is part of the 
McFarland Unified School District with a student population of approximately 3,600. McFarland Unified School District is located in 
the small, rural, and predominantly agricultural community of McFarland California, population approximately 14,000. McFarland is 
located in the southern part of the San Joaquin Valley, thirty miles north of Bakersfield, adjacent to State Highway 99. The main 
industry of the area is agriculture with the primary crops being almonds, grapes, oranges, roses, and kiwis. 
 
Our role at SJHS is three-fold: 
1. To prepare students academically, emotionally, and behaviorally to return to McFarland High School or to stay on track to meet the 
requirement for earning a high school diploma 
2. To promote articulation of curriculum and teaching of “same skills courses” that match those at MHS 
3. To assist students in attaining sufficient credits and appropriate instruction to enable them to earn a high school diploma 
 
SJHS serves students who are at risk of not graduating from high school because they are unable or unwilling to attend McFarland 
High School, the MUSD comprehensive high school. Various reasons for choosing SJHS 
include: personal hardships, 
pregnancy/childcare needs, special needs for work scheduling, and a positive alternative to the larger high school setting. The SJHS 
student population is considered “transitional” in that student enrollment is usually short term, limited to or usually one or two 
semesters, to enable the student to either make-up missing credits in order to graduate with his/her original class, overcome personal 
crisis, finish up a work season, or to obtain one-on-one assistance. However, a considerable number of students have graduated from 
this school. 
 
An important factor in preparing students for return to MHS is that of being emotionally prepared. Some students attend because of 
behavior management challenges. Staff takes great pride in cultivating school culture and promoting the caring attitudes. We are able 
to assist our students in finding appropriate programs and services to meet their needs, assist them in developing appropriate 
behaviors and working to provide individualized assistance in overcoming difficulties (i.e. – counseling referrals, career counseling, 
and individual education planning) due to a higher teacher student ratio than the local traditional high school. Students with active 
IEPs receive additional support from the appropriate district specialists in special education and speech pathology in order to reach 
their goals and objectives. For students on probation, there is a continued collaboration between probation officers and our 
instructors. A team of classified employees support our certificated staff. District technology teams, psychologists, nurses, and other 
support staff are available on an as needed basis. 
 

2017-18 School Accountability Report Card for San Joaquin Continuation High School 

Page 2 of 12 

 

The curriculum of SJHS mirrors that of the comprehensive high school in that we follow the same course outlines in order to meet 
state standards across the curriculum. We routinely confer with MHS counselors and staff to be sure we are providing for the needs 
of our students to enable them to return to MHS or to meet graduation course requirements. We consider the attainment of 
proficiency level in the basic skills enhanced by technological fluency to be beneficial to successful employment as well as further 
education and training. SJHS staff members are well-versed in technology and are striving to integrate its use throughout the 
curriculum in all our courses. District continuing support of technology and several grants have provided computers throughout the 
campus to assist our students in the mastery of skills, research, and the streamlining of their work processes. Each student is also 
provided with a personal Chrome Book to use for web-based computer instruction available to them outside of regularly scheduled 
school hours.