Monroe enjoys a rich history of academic and cultural excellence that is the result of a collegial 
partnership between the school staff, parents and community partnerships who provide a diversity 
of experiences for the students. 
 
Monroe's administration, parents, teachers and support staff work as a partnership to promote a 
mutual respect between home and school, to respect and appreciate the diverse groups and 
cultures. Monroe School communication is vital between parents and school, we implement 
outreach strategies to provide information, educate and support parents. Monroe continues to 
build a strong collaboration and partnerships with parents and families of the students we teach. 
Our school is a K-8 school with full-day kindergarten and Head Start Preschool classes. Monroe 
strives to become a high-performing school, our goal is to provide an academically rigorous learning 
environment with mastery of the Common Core grade level standards in reading, writing, and 
mathematics which forms the foundation of the instructional program for all of our students. 
Monroe continues to strive for the one to one ratio with our touch screen computers. We continue 
to maximize our instructional minutes through targeted instruction and implementation of 
research-based instructional strategies. Monroe Teachers continue to grow professionally as they 
attend on-going Professional Learning Community trainings, work in Teacher collaborative Action 
Team (CAT) to discuss effective teaching practices, A-Z training in math and reading, district staff 
development and receive in class support from the Assistant Principal and Principal. Teachers use 
Common Formative assessments, benchmark tests and the Bader Assessment which are given to 
students throughout the year to monitor their academic progress. Teacher CAT (Collaborative 
Action Teams) meeting and RTI (Response To Intervention)Team use data to academically group 
students and drive instruction to meet the needs of our Monroe students. The Administrator, staff 
and Monroe's MTSS (Multi-tiered Systems of Student Support) Team provide a safe and orderly 
campus where your student may focus his/her energy on learning. All students have the 
opportunity to meet their maximum potential with the guidance and support of Monroe School’s 
fully trained teachers and support staff. Monroe School is currently certified in both AVID 
Elementary and Secondary. We prescribe to the AVID mission statement that says to support 
student learning and close the achievement gap by preparing all students for college and career 
readiness to be successful in a global society. 
 
Monroe looks forward to working with you to create new opportunities and new accomplishments 
for our students this year. 
 
 
Mary Lou Rios, PRINCIPAL 
 

2017-18 School Accountability Report Card for Monroe Elementary 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Monroe Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

20 

23 

23 

3 

2 

2 

0 

2 

0 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Monroe Elementary 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.