Florence Nightingale Charter is a Stockton Unified dependent charter school. Our students 
participate in Project-Based Learning (PBL), an innovative approach to learning that builds mastery 
in the core content areas of Reading and Language Arts, Mathematics, Science, Social Studies, and 
Technology. Through Project-Based Learning, our students build life skills of critical thinking, 
communication, collaboration, and creativity. 
 
Mission- Nightingale Charter will cultivate Outstanding, Wise Leaders and Scholars prepared for 
college, career, and beyond through Project-Based Learning and 21st Century skills. 
 
Vision- A Professional Learning Community dedicated to creating Outstanding, Wise Leaders and 
Scholars. 
 
Myra Machuca, PRINCIPAL 
 

 

 

----

---- 

----

---- 

Nightingale Charter School 

701 North Madison St. 

Stockton, CA 95202-1634 

(209) 933-7000 

www.stocktonusd.net 

 

District Governing Board 

Cecilia Mendez 

AngelAnn Flores 

Kathleen Garcia 

Lange Luntao 

Maria Mendez 

Scot McBrian 

Candelaria Vargas 

 

District Administration 

John E. Deasy, Ph.D. 

Superintendent 

Dr. Reyes Gauna 

Assistant Superintendent of 
Educational Support Services 

Craig Wells 

Assistant Superintendent of 

Human Resources 

Sonjhia Lowery 

Assistant Superintendent of 

Educational Services 

 

2017-18 School Accountability Report Card for Nightingale Charter School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Nightingale Charter School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

24 

16 

18 

6 

0 

5 

0 

6 

0 

Nightingale Charter School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Nightingale Charter School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

1 

1 

0 

1 

1 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.