: 
 
Edward C. Merlo Institute of Environmental Technology is a specialty high school that provides 
students with an engaging school environment . We are an Environmental Engineering high school 
that provides a focus on Environmental Advocacy and Engineering training to students. In addition 
to these, students receive core and A-G courses in preparation for college and career. Teachers 
ensure student mastery of standards through the frequent use of Common Formative Assessments 
and real life global experiences. 
 
The following are Merlot's mission and vision statements: 
 
Mission Statement 
The mission of Merlo Institute of Environmental Technology is to provide a safe, relevant and 
engaging environment for our students. Through the use of professional learning communities 
(PLC) and the implementation of Project Lead the Way and the National Academy Foundation (NAF) 
curriculum, students will experience real life situations beyond the boundaries of the classroom. 
Merlo graduates will be environmentally responsible citizens equipped with the knowledge, 
problem solving abilities, technology skills needed to succeed in post-secondary education, and an 
ever changing global economy. 
 
Vision Statement 
Merlo graduates will be motivated, empowered, adaptable, critical thinking students, who are 
prepared for successful post-secondary education, careers and global competition. 
 
Merlo offers Project Lead the Way engineering courses such as "Introduction to Engineering Design 
(IED), "Principles of Engineering". Environmental Sustainability will be offered in 2017/18 school 
year. Students demonstrate and experience environmental advocacy throughout all their courses. 
Teachers often collaborate in developing cross curricular projects at the various grade levels. This 
year, all Merlo students participated in Mock elections; teachers assigned opportunities for 
students to investigate the various propositions on the 2016 ballot. 
 
Merlo students regularly participate in outside competitions that require higher order reasoning. 
Contests include the "Chevron Design Challenge, Lenovo Scholar app-inventor contests and 
SkillsUSA. Our students have been known to receive medals at regional and state levels of these 
competitions as witnessed in the Lenovo Scholar Network 2016 "Fan Favorite" contest. Students 
have also gone on to design projects such as the underwater ROV featured in the Stockton Record. 
 
At Merlo, students and families remain our central focus. We continually provide opportunities to 
engage families, businesses and the community in educating our students. At our school we say we 
are a small school doing BIG things! 
 
Gamal Salama, PRINCIPAL 
 

2017-18 School Accountability Report Card for Edward C. Merlo Institute of Environmental Technology 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Edward C. Merlo Institute of Environmental 
Technology 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

9 

3 

0 

9 

2 

0 

10 

2 

0 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.