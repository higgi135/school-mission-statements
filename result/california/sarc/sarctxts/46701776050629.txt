Loyalton Elementary School is a small school with a big heart. Teachers and staff work hard to keep the small school atmosphere while 
providing academics and other programs comparable to larger schools. Our mission is to provide a primary education with a goal of 
all children achieving literacy. We provide children with the tools to encourage their total development, enhance their self-esteem, 
and realize their potential in a safe, secure environment. 
 
Loyalton Elementary School is located near the communities of Sierraville, Calpine, Beckwourth, Chilcoot, and Vinton. The school is 
one of five in the Sierra-Plumas Joint Unified School District. 
 
Soccer, baseball, volleyball and basketball are offered as extracurricular sports. Little League baseball occurs in the spring and AYSO 
soccer in the fall. Volleyball is a fall sport, and basketball is a winter sport. Basketball players take part in a four day basketball 
tournament at our site that is hosted by our sports club. 
 
Back to School night happens shortly after school starts. Every year the students and staff work hard on a holiday show which is 
presented just before the Winter Break. In the spring, the Science Fair, Art Show, Taco Feed and Open House all occur on the same 
big night! 
 
Loyalton Elementary School's Mission: 
Our Mission is to Provide a Primary Education with a Goal of All Children Achieving Literacy. We Provide Children with the Tools to 
Encourage Their Total Development, Enhance Their Self-Esteem, and Realize Their Potential in a Safe, Secure Environment.