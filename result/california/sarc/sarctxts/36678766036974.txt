Lytle Creek Elementary has 33 classrooms, a library, a computer lab and a staff room. It was last modernized in 2012. The facility 
strongly supports learning through ample and functional classroom and playground space. 
 
Lyle Creek Elementary is committed to the academic excellence of all students through focused, creative, and rigorous instruction 
within a respectful, orderly environment of high expectations. Available academic and enrichment programs include: * Creative After 
School Programs for Success (CAPS), * The Learning Center - Academic skill development, * Intensive Instruction - Homework 
assistance, * Positive Behavior Support System, AVID and No Excuses University.