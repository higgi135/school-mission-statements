College Park Elementary School — located in the village of College Park — has an enrollment of approximately 751 students in grades 
K-6. Although the majority of students speak English, a large number of students come from a variety of ethnic backgrounds and speak 
many different languages. This international population, coupled with a number of students with special needs, creates a rich and 
diverse student body. At College Park, our diverse population is embraced, different cultural backgrounds are appreciated, and 
character development is encouraged. College Park students truly live and receive a global education. Our school has two special 
programs – the newcomer program for students with an English Language Proficiency Assessment for California (ELPAC) (Scoring at 
the Novice or Minimally Developed level on the ELPAC) and the Behavior and Social Learning Center (BSLC) for students across the 
district who struggle with behavior and social issues in the mainstream classroom. College Park’s mission is to promote academic 
excellence and instill in students the technical skills necessary to succeed in the world of tomorrow. As a result, we hope that students 
will become more innovative, develop a "Growth Mindset," develop the skills necessary to think and express themselves clearly and 
acquire the values necessary to act well, appreciate life and contribute to society. 
 
The College Park staff, parents, and surrounding community share a common vision. High-quality classroom instruction, students who 
are motivated to learn, a warm and nurturing learning environment, and a supportive and encouraging parent-community help to 
make College Park a wonderful place to learn and work. It is our goal that all students will leave College Park able to read, write, and 
compute at high levels. By accomplishing this goal, students are afforded the opportunity to experience academic success at the next 
level and beyond. To ensure this success, the College Park staff is dedicated to using sound instructional practices and high-quality 
instructional materials. This includes the integration of technology and innovative practices throughout the curriculum. Our 
instructional programs combine explicit skill instruction, such as phonics and mathematics skills, and instruction that is embedded 
within a meaningful context. Students have the ability to go beyond the “basics,” through the use of technology, including Internet 
access, and a large inventory of instructional computer software, including a video-editing and production program, in addition to our 
new and growing innovation lab. 
 
It is due to our common vision, highly skilled staff, “state of the art” instructional materials, and commitment to providing students 
with an environment that encourages respect, responsibility, integrity, and safety that we are able to be successful with our mission.