Enchanted Hills Elementary School serves students in grades TK-6. Students, teachers, and families work together to provide a 
community school atmosphere to promote academic and social achievement. 
 
Enchanted Hills Elementary School is located in the city of Perris, a community fifteen miles southeast of Riverside. Our school draws 
students from diverse cultural and socioeconomic backgrounds in a suburban/rural environment. The total student enrollment is 509 
comprised of 3% African American, 3% white, and 94% Hispanic population. Our school educates 52% English Learners and 87% Low 
socio-economic families. 
 
Enchanted Hills Elementary School is dedicated to ensuring that all students receive the best possible education and reach their fullest 
academic potential to succeed in a global society. We challenge our students daily with a rigorous curriculum that is aligned with the 
California Content Standards. We emphasize reading, writing, mathematics, PE, Art, and English language development at all grade 
levels. The dedication of our teachers and our high expectations for our students breed academic success at Enchanted Hills 
Elementary. Enchanted Hills is in it's second year of implementation of the AVID Elementary program. 
 
Enchanted Hills Elementary School's School Plan for Student Achievement describes a total program plan for School Improvement, 
with goals with goals to increase student achievement in English Language Arts, Mathematics and to increase the achievement of our 
English Learners (EL). A school culture goal addresses parent involvement and creating a warm, safe environment for students and 
families. The staff and parents contributed recommendations based upon results from the annual school survey, School Site Council 
and ELAC recommendations, yearly Program Monitoring Review, and Coordinated Program Monitoring results. 
 
Parents and community members are an important part of our academic program. We encourage parents to participate in our school 
activities, events, and committees. Our entire staff looks forward to working with parents and community members to ensure another 
year of academic success. 
 
The vision statement of our school reads as follows: 
Enchanted Hills Elementary school is a collaborative learning community where staff, parents, and students are committed to being 
positive contributors to a respectful, academic culture. 
 
Our staff and community are invested in making student-centered decisions that will influence rigorous teaching through innovative 
and differentiated instruction. 
 
We are dedicated to ensuring that all students are actively engaged in achieving academic goals, preparing for college and becoming 
productive citizens of society in a safe climate of high expectations and responsibility. 
 

2017-18 School Accountability Report Card for Enchanted Hills Elementary School 

Page 2 of 11