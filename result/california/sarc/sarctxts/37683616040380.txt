PRIDE Academy is a K-8 school in the Santee School District. Our focus as a Title 1 school is to assure appropriate access to learning 
for all students through project-based learning and other activities. Location: 9303 Prospect Avenue, Santee, CA 92071 
PRIDE Academy inspires students to become compassionate global citizens, equipped to collaborate, think critically and communicate 
effectively within a society that continues to evolve.