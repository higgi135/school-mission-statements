Spring Valley School is a dynamic, growing elementary school dedicated to ensuring the academic 
success of every student and providing a safe and comprehensive educational experience. The 
school has an energetic staff who pride themselves in doing an excellent job. The Spring Valley 
family concept is incorporated in the daily routine of the staff and has become part of the school’s 
culture. The result is a warm and friendly atmosphere where parents and staff work together for 
the benefit of the Spring Valley School children. Our PTA has grown into one of the most involved 
and productive associations in our area. We have experienced an overwhelming response from 
parents inside and outside our attendance area who want their children to attend our program. In 
keeping with the Millbrae School District’s vision, Spring Valley School’s mission is to prepare 
students for leadership and responsible, productive participation in a changing world. This is 
accomplished by working in partnership with families and the community to help students become 
problem solvers and critical thinkers. We work together to promote the students' intellectual, 
physical, emotional, social, ethical, and cultural development and have implemented a Positive 
Behavior Intervention & Support model. We continue to refine our implementation of the 
California Common Core State Standards and are currently focusing on writing as a way to build our 
students' capacity to express their development as robust thinkers. Science is also a major focus. 
With an eye on developing the Next Generation Science Standards, all grade levels participate in 
related field trips and hands on experimentation. We include character education by focusing on 
specific character traits throughout the year. 
 
 

2017-18 School Accountability Report Card for Spring Valley Elementary School 

Page 1 of 9