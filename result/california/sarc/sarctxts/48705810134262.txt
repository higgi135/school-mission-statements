Description: Caliber Schools are free, public, non-selective charter schools offering a rigorous, personalized, college preparatory 
education for Bay Area students. Caliber: ChangeMakers Academy, which opened in August 2016, currently serves approximately 700 
students in Tk-8th grade. 
 
Mission: Caliber’s Mission is to achieve educational equity by shifting the experiences, expectations and outcomes for students in 
historically underserved communities. Our strengths-based educational program validates, affirms, respects and supports students, 
families and staff members to reach their full potential. 
 
We achieve this mission through 9 core components of our educational model: 
* Personalized learning 
* Project-based science and social studies 
* Blended English and Math 
* Collaboration in grade teams 
* Emotional intelligence 
* Daily writing 
* Longer school day 
* Enrichment 
* Computer coding and computational thinking