Yokayo Elementary School is a K-6 site that is located in the southwest sector of Ukiah. Our student 
attendance area extends from the District Fairgrounds to the North, Luce Ave. to the South, 
Orchard Ave. to the East. Our enrollment stated in the 2018-19 CBEDS is 508. Approximately 82% 
percent of the students receive free or reduced lunch. Our staff consists of 1 administrator, 26 
certificated staff, and 39 classified employees. Within the classified group there are 2 secretaries, 
3 cafeteria workers, 1 health technician, 1 librarian, 3 custodians, 1 Community/Family Liaison, 1 
Full-time Counselor, 1 part-time counselor, and 19 paraprofessionals that provide service and 
support to our Resource Services Program, Physical Education Program, and our Severely Disabled 
Program. Yokayo Elementary School has 20 regular education classrooms. There are 2 Resource 
Teachers, 1 Severely Handicapped Teacher, 1 part-time Speech Therapist, 1 full-time Physical 
Education Teacher, 1 Teacher on Special Assignment serving as the School Success Coordinator, 
and 1 Teacher on Special Assignment serving as the Reading Support Teacher. 
 
School Vision and Mission 
The mission of the Yokayo Elementary School community is to instill the appreciation of knowledge. 
The ability to effectively communicate, problem solve, collaborate, create, and innovate will 
support a rigorous, standards-based curriculum. The character traits of trustworthiness, respect, 
responsibility, fairness, caring, and citizenship (TRRFCC) will be steeped within our foundational 
learning. Our community will provide a nurturing environment committed to achieving excellence. 
 

2017-18 School Accountability Report Card for Yokayo Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Yokayo Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

25 

24 

21 

3 

0 

3 

0 

6 

0 

Ukiah Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

335 

22 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Yokayo Elementary School 

16-17 

17-18 

18-19