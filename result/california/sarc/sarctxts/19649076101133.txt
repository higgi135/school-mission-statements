Decker Elementary School is nestled in the community of Phillips Ranch which is within the city of Pomona. Parent engagement and 
community support are defining elements of Decker. Our parents value being positive, visible role models for all children. The Decker 
PTA is passionate about enhancing the academic and social-emotional experience by providing enrichment opportunities that 
compliment our core curriculum and supplemental programs. Decker is characterized by an extraordinary closeness and willingness 
to support one another which is evident in the tone and interactions between faculty, staff, students and parents. 
 
Our vision is that every child will be a productive citizen with the ability to communicate effectively, solve problems independently, 
and think critically. 
 
Our mission is to prepare students through social interaction, leadership opportunities, and rigorous academic experiences which 
include vertical articulation of instructional practices, collaboration of all staff, and support from all stakeholders. 
 
In order to fulfill our mission, we are committed to: 
Providing differentiated educational programs that are engaging, collaborative, and culturally responsive. 
Maintaining a collaborative learning environment that promotes responsibility, respect, and safety. 
Treating students in a positive manner that promotes confidence, independence, and empathy.