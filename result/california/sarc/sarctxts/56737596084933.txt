MISSION STATEMENT 
Wildwood Elementary School reflects the District's mission by encouraging students to become independent learners who will develop 
leadership skills. 
 
SCHOOL DESCRIPTION 
Wildwood Elementary School is a community-oriented school located in northern Thousand Oaks. Learning at Wildwood is a team 
effort between students, staff, and home. 
 
Our accomplished faculty has a united focus in providing challenging and meaningful standards-based experiences for our students. 
We view every child as an individual with unique qualities and needs. These individual differences are valued and nurtured through 
thoughtful and progressive teaching. We celebrate that children learn in a variety of ways and recognize the importance of presenting 
curriculum in multiple modalities. Special scheduling provides our teachers with grade level planning time to assist them in planning 
instruction appropriate to the level of each child. 
 
Our school has a high academic focus that is enhanced by technology in every classroom. Our curriculum provides thematic, meaning-
centered, integrated, hands-on experiences for all children in all areas. Children grow socially and emotionally through our emphasis 
on social responsibility and citizenship. We are proud that our students are inspired to become able, confident life-long learners who 
contribute to our society. 
 
It is our goal to develop proficient readers and writers who value language as a tool to express thoughts, beliefs, and ideas. Learning 
mathematical skills and engaging in creative problem solving will prepare students for real world application. Knowledge and learning 
are valued as students develop pride of workmanship and respect for others. The school-wide goals are developed by the teachers, 
parents, and principal follow State Standards and District LCAP. The staff believes in high standards, creating a culture where high 
expectations equal high results. Students not making expected progress are assisted by a strong intervention program. In addition to 
an effective academic program, Wildwood helps its students grow socially and emotionally through our emphasis on social 
responsibility and citizenship. Our students and staff are committed to following the ROAR Guidelines: 
Tigers succeed when we: 
 
R espect others and ourselves. 
O ffer kindness and encouragement. 
A lways do our best. 
R eady to learn! 
 
Our Character Education Program and monthly awards assemblies recognize positive behavior and academic accomplishment. The 
Wildwood spirit and sense of community are clearly shown through our Student Council, parent facilitated Art Masters program, after-
school activities, Jog-a-Thon, family movie nights, and other enrichment activities. Wildwood is devoted to exceptional teaching and 
community involvement. 
 
 

2017-18 School Accountability Report Card for Wildwood Elementary School 

Page 2 of 12