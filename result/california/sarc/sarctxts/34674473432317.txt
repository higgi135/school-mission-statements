Principal's Message: 
Welcome to El Camino High School's Annual School Accountability Report Card. This is where you'll find valuable information about 
our academic achievement, professional staff, curricular and co-curricular programs, instructional materials, safety procedures, 
classroom environment, and condition of facilities. The community of El Camino Fundamental High School is committed to high 
expectations, high achievement, responsibility, and mutual respect for all students. A safe, positive environment nurtures and enables 
all students to reach their potential as lifelong learners and responsible, enlightened citizens. El Camino maintains academic rigor in 
an atmosphere that promotes respect for one another, development of personal responsibility, and readiness for the world beyond 
the classroom. 
 
Mission Statement 
Embodying the Respect, Responsibility, and Readiness inherent in our fundamental spirit and tradition, the mission of the El Camino 
Fundamental High School Community is to challenge and inspire each student to be a creative, life-long learner, and enlightened, 
contributing citizen by ensuring that students construct meaning through rigorous 21st century learning in a caring, inclusive 
environment. 
 
School Profile 
El Camino Fundamental High School is one of 9 comprehensive high schools in San Juan Unified School District. El Camino offers an 
academic program, aligned with California Content Standards, which is designed to prepare students for success in the workplace, in 
postsecondary schooling, and in citizenry. El Camino offers a full complement of Advanced Placement and Honors classes; a four year 
Eagle Polytechnic Institute that prepares students to enter the field of pre-engineering or technology through project-based learning 
in core classes; a four year AVID (Advancement Via Individual Determination) program; and a rich visual, musical, and dramatic arts 
program.