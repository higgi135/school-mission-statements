Our Vision 
As a community, we prepare our students to be multilingual, multiliterate, and multicultural global citizens. 
 
Our Mission 
The Language Academy provides a creative learning environment where strengths and individuality are respected and students are 
encouraged to apply knowledge learned utilizing innovative methods. 
 
Our Guiding Principles- 
 
As a multilingual, multiliterate, and multicultural community we: 
help others become successful and responsible citizens of the world 
nurture diverse skills and personalities 
welcome and inspire creative ideas 
work as a team to innovate and solve real world problems 
exhibit positive attitudes and high expectations 
promote the development and pride of competencies for all 
 
We are a community!