Pacific Grove High School is a comprehensive 9-12 high school, which has served students since the late 1800’s. Our mission, in 
partnership with home and community, is to challenge every student to learn the skills, acquire the knowledge, and develop the insight 
and character necessary for a productive and rewarding life through a quality instructional program, a positive, safe, and stimulating 
environment, with a clear commitment to the worth of every individual. 
 
Vision Statement (August 2017): Pacific Grove High School is a community of learners committed to providing students with 
opportunities to steer their lives toward academic, career, and personal success. Through collaboration, a commitment to evidence-
based decision-making and a spirit of inclusion, PGHS aims to cultivate culturally aware, employable, healthy, active students who are 
eager learners, conscientious digital citizens, environmental stewards, and effective communicators.