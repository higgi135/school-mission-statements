Murphy Middle School is a California Gold Ribbon School that is located in the northern tip of 
Morgan Hill Unified School District in the southern area of San Jose. Our school is surrounded by 
the beautiful majestic mountains of Santa Teresa County Park. We have just over 750 sixth, seventh 
and eighth grade students engaged in a highly rigorous learning environment that promotes 
academic excellence. With the input of staff, students, and families, we went through the process 
of updating our mission statement and creating a list of core values last year. 
 
Core Values 
E - Enthusiasm 
M - Mindfulness 
P - Perseverance 
O - Ownership 
W - Work Ethic 
E - Equity 
R - Rigor 
 
Succinct Mission Statement: 
Empowering our students with the education and values needed for future success. 
 
Mission Statement 
Martin Murphy is dedicated, in partnership with parents and the community, to constructing a 
rigorous learning environment where ALL students are empowered to achieve, use creative and 
critical thinking skills, develop a global perspective, and to model the core values of respect, 
responsibility, and safety. 
 
School Vision 
Our students will be prepared to succeed in a diverse, global society and to make meaningful 
contributions to our community. Students will become digitally literate, critical thinkers, 
collaborators, and problem solvers who can meet the challenges of the 21st Century. The students 
of Martin Murphy will demonstrate perseverance and grit so that they may achieve success in life 
and a variety of situations, including college and career. 
 
School Slogan 
“Success is no accident!” 
 
Martin Murphy teachers participate in a Professional Learning Community model that promotes 
the sharing of best practices across all curriculum, the implementation of school-wide technology, 
and the reflection and analysis of student performance data and assessments. Our teachers meet 
weekly to review assessment data and utilize results to target student learning. This process 
ensures consistent monitoring and change to meet the needs of our students and promote 
academic achievement. All staff are trained on positive behavior supports (PBIS) to foster a positive 
school climate. Staff and students participate in OLWEUS bullying prevention curriculum, PBIS, and 
restorative justice practices. Additionally, we partner with Discovery Counseling services to provide 
on-site therapeutic one-on-one counseling support. Team members from Discovery Counseling 
also instruct our students in the Life Skills curriculum which prepares the students with skills they 
will need for college and career success. 
 

2017-18 School Accountability Report Card for Martin Murphy Middle School 

Page 1 of 9 

 

Each of our classrooms is equipped with a projector and all students have access to Chromebooks that help increase student engagement, 
academic rigor, content mastery, and 21st Century skills. All 7th and 8th grade students are assigned a Chromebook one-to-one, and the 
6th grade students have access to Chromebook carts in their classrooms. In addition to using Chromebooks daily in classes, students can 
extend their learning from home. Additionally, Teachers use the innovative learning center (I Center) to do projects, host guest speakers, 
and a variety of other engaging learning tasks. Teachers are equipped with tablets to increase their interaction with all students in class. 
Also, we fund an after school homework center and math tutoring program in the I Center. 
 
We have expanded the Advancement Via Individual Determination (AVID) Program to include all grade levels at Murphy. The AVID elective 
teaches the students to learn about higher education and to practice critical thinking that will lead them to success in college. AVID 
students visit the local high school and two colleges or universities each year to sample life in higher education and to learn the path to 
success as well as the challenges presented by it. 
 
New for this year and with the funding of the Live Oak Grant and extended day money, we are excited to enhance our Visual and 
Performing Arts opportunities for our students. We have reconfigured a classroom to be a black box theater, and we have purchased 
flooring, lighting, sound equipment and other supplies to create a new performing arts program that will include more arts based electives 
and student performances such as the Snoopy musical.