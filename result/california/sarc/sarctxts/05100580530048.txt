Calaveras River Academy's classroom program serves as an alternative school for students in grades 
six through twelve who reside in Calaveras County. The focus of the Community School is to re-
mediate identifiable weaknesses with the students' known strengths, while building self-image and 
personal worth. This is accomplished through a structured behavior management system, ongoing 
assessment and positive reinforcement. Calaveras River Academy believes in the worth of each 
student and strives to meet each individual's needs. Emphasis is also placed on community service 
and numerous activities are organized which involve students directly with community projects. 
Calaveras River Academy's independent study program serves as an alternative program for 
students in grades six through twelve who reside in Calaveras County. The focus of the program is 
to provide academic instruction for students needing a more independently based program due to 
family circumstances or transitional timing into mainstream schools. 
 
Vision Statement 
 
"The students of CRA will be responsible, productive citizens equipped with skills to thrive in the 
21st century." 
 
Mission Statement 
 
"The high expectations and academic focus at CRA is relevant and built on positive relationships. 
Students are empowered to be productive contributors within their community, innovative 
thinkers, and lifelong learners, who transcend boundaries to overcome obstacles in their path 
towards excellence". 
 

2017-18 School Accountability Report Card for Calaveras River Academy 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Calaveras River Academy 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

3 

0 

3 

5 

0 

0 

4 

0 

0 

Calaveras County Office of Education 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

29 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Calaveras River Academy 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.