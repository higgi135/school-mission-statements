Maxson Elementary is an exciting TK-6 school where children learn, have fun and participate in 
many events. We encourage early childhood education and houses 5 pre-school classes on our 
campus. Teachers, staff, students and parents all work together to create an atmosphere of 
“family.” 
 
We are “Committed to Excellence”, and we work hard to make the site a primary source for timely 
information for all users, and a main gateway for improved communication between parents, 
teachers, students and other members of our community. Our goal and responsibility is to help 
each student develop an enthusiasm for learning, a respect for self and others, and the skills to 
become a creative independent thinker and problem solver. 
 
The local community and Maxson Elementary benefit greatly from their collaboration and 
commitment to each other. We strive to provide a variety of educational and extra-curricular 
events for our students and families. Some of our activities include: Harvest Festival, Reading is 
Fundamental Book Give-A-Ways, Red Ribbon Week, Mini-Olympics, Read Aloud Week, Fiesta de 
Mayo, Student Council, Coding Club and Staff Appreciation Day, and a variety of study trips and 
6th grade promotional activities. 
 
Maxson Elementary School is located in the City of El Monte, in the eastern San Gabriel Valley. The 
community is highly supportive of the educational climate. El Monte has much more to offer than 
just ‘the mount,’ as its name suggests. Local community morale is soaring due to the rapid spread 
of industry and businesses. Maxson Elementary School serves grades kindergarten through six, as 
one of the ten elementary schools in the Mountain View Elementary School District. It is the third 
oldest school in our district as it was founded in 1948. Maxson Elementary School maintains a 
commitment to providing a strong instructional program and student academic excellence. 
 
Maxson is a PBIS School. We S.O.A.R Together! PBIS is Positive Behavior Intervention Supports. 
Maxson Elementary School is committed to creating an environment 
that encourages and empowers all students to aim high in all areas of their academic, creative, 
physical, and social skills, building future leaders! At Maxson, our Falcons S.O.A.R.! Each student is 
expected to Be SAFE , BE ORDERLY, BE ACHIEVERS AND BE RESPECTFUL. 
 
The school seeks to promote parental involvement in the educational process as well as staff 
members, offering comprehensive programs and guides for the benefit of both the student and the 
parent. Families and community members are encouraged to participate in our organized school 
activities such as our monthly Family Literacy Lunches and Read Aloud Week. 
 
 

2017-18 School Accountability Report Card for Maxson Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Maxson Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

24 

23 

22 

0 

0 

0 

0 

0 

0 

Mountain View School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

383.8 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Maxson Elementary School 

16-17 

17-18 

18-19