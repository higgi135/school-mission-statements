McFadden Intermediate School (McFadden) is located at the corner of W. Adams Street and S. Raitt 
Street in the city of Santa Ana. 
 
The academic emphasis at McFadden is on "Designing 21st Century Learning" in English language 
arts, mathematics, science, elective programs, physical education and social sciences. Students 
follow a six period schedule which allows students to have one period of an elective class. Several 
classes of intensive intervention for English language arts are also provided, allowing these students 
to have a period of ELA instruction support. 
 
The master schedule is organized so that the majority of teachers who teach the same subject(s) 
have the same preparatory period. This allows for professional collaboration on a daily basis. 
Teachers are strongly encouraged to use this time to develop common lesson plans, review student 
achievement data and share instructional strategies that are effective with middle school students. 
Upon completion of the eighth grade, students are expected to be able to successfully read for 
understanding and perform mathematical operations that include a mastery of algebraic 
computations. Students will also be prepared with 21st century skills to be college and career ready 
as individuals and be contributing and productive members of ever-changing schools, family, 
community, and society. 
 
McFadden also has a strong visual and performing arts program. Students are given the opportunity 
to explore and participate in visual art, choir, band or orchestra. The school's performing arts 
facilities feature both indoor and outdoor stages. Students share their talents at school and 
community-based events. McFadden also offers the oldest Dual Language Academy (D.L.A.) for 
intermediate schools in the Santa Ana Unified School District. Our D.L.A. students participate in a 
zero period physical education class so that they are able to take an extra elective class on top of 
their rigorous Spanish literature elective. McFadden has also added AVID to our 7th and 8th grade 
students and offers 6 sections of instruction. This year we continue to offer the McFadden Pre-IB 
Academy which concentrates in world language acquisition, STEM and the arts. 
 
A variety of instructional programs are provided for students outside of the school day. 
Approximately 135 students attend the after school program. Approximately 700 students 
participate yearly in a variety of intramural athletic activities. Students can also make up days of 
absence by attending the Saturday Attendance Recovery Program (W.I.N) which is offered every 
Saturday. Approximately 40 students meet regularly with our Robotics teacher and work in the 
areas of engineering, algebra and physical science. McFadden is also a Project Lead the Way school 
for Computer Science. The PLTW Gateway To Technology (GTT) program features a project-based 
curriculum designed to challenge and engage the natural curiosity and imagination of middle school 
students. They envision, design, and test their ideas with the same advanced modeling software 
used by companies like Lockheed Martin, Intel and Sprint. They study mechanical and computer 
control systems; think robotics and animation. Students also explore the importance of energy, 
including innovative ways to reduce,conserve and produce it using solar, thermal and wind power. 
The knowledge that students gain and the skills they build from GTT create a strong foundation for 
further STEM learning in high school and beyond. Engineering and design along with a robotics 
electives became available in the 2013-14 school year with the expansion to "Medical Detectives" 
and "Flight and Space" in 2014-15 and introduced Computer science 1 and 2 in 2015-16 as well as 
a Career Technical Education Digital Media class. 
 

----

---- 

Santa Ana Unified School District 

1601 East Chestnut Avenue 
Santa Ana, CA 92701-6322 

714-558-5501 
www.sausd.us 

 

District Governing Board 

Valerie Amezcua – Board President 

Rigo Rodriguez, Ph.D. Vice – 

President 

Alfonso Alvarez, Ed.D. – Clerk 

John Palacio – Member 

 

District Administration 

Stefanie P. Phillips, Ed.D. 

Superintendent 

Alfonso Jimenez, Ed.D. 
Deputy Superintendent, 

Educational Services 

Thomas A. Stekol, Ed.D. 
Deputy Superintendent, 
Administrative Services 

Mark A. McKinney 

Associate Superintendent, Human 

Resources 

Daniel Allen, Ed.D. 

Assistant Superintendent, K-12 

Teaching and Learning 

Mayra Helguera, Ed.D. 

Assistant Superintendent, Special 

Education/SELPA 

Sonia R. Llamas, Ed.D., L.C.S.W. 
Assistant Superintendent, K-12 
School Performance and Culture 

Manoj Roychowdhury 

Assistant Superintendent, Business 

Services 

Orin Williams 

Assistant Superintendent, Facilities 

& Governmental Relations 

 

2017-18 School Accountability Report Card for McFadden Intermediate School 

Page 1 of 10 

 

District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently serving nearly 49,300 students in grades K-
12, residing in the city of Santa Ana. As of 2017-18, SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 
alternative high schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or Reduced Price Meal 
program, 39% qualifying for English language learner support, and approximately 13% receiving special education services. Our district’s 
schools have received California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I Academic Achieving 
Schools, and Governor’s Higher Expectations awards in honor of their outstanding programs. In addition, 20 schools have received the 
Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have developed and maintained high 
expectations to ensure every student’s intellectual, creative, physical, emotional, and social development needs are met. The district’s 
commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high quality educational 
program. Consistent success in meeting student performance goals is directly attributed to the district’s energetic teaching staff and 
strong parent and community support.