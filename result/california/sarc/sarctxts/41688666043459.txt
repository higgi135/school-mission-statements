Fox School, located in the City of Belmont on the peninsula south of San Francisco, is one of six 
elementary schools in the Belmont- Redwood Shores School District. Students are promoted to 
Ralston Middle School, Nesbit School or Sandpiper School. The Fox School community consists of 
families for whom education is a priority. The school is the hub of the community for many of these 
parents. Families enthusiastically support the Parent Teacher Association, School Site Council and 
countless volunteer activities and projects. Pride in Fox’s distinguished school recognition is 
reflected from all members of the school community. 
 
School Goals 

• 
• 

• 
• 
• 

For all students to EXCEED the Belmont Redwood Shores core curriculum standards 
For each student to succeed beyond their potential intellectually, socially and 
academically 
For each teacher to provide the highest quality instruction possible 
To continually move past status-quo towards improvement 
To provide the best education possible to each individual child 

 

 

-------- 

Belmont-Redwood Shores 
Elementary School District 

2960 Hallmark Dr. 
Belmont, CA 94002 

(650) 637-4800 

http://www.brssd.org 

 

District Governing Board 

Rahila Passi, President 

Suvarna Bhopale, Vice President 

Amy Koo, Clerk 

Robert Tashjian, Member 

Huan Phan, Member 

 

District Administration 

Dr. Michael Milliken 

Superintendent 

Genevieve Randolph 

Assistant Superintendent 

Craig Goldman 

Chief Business Official 

Ching-Pei Hu 

Director of Educational Services 

Lara Goldman 

Director or Special Programs 

Jerome Simon 

Director of Technology 

 

2017-18 School Accountability Report Card for Fox Elementary 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Fox Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Belmont-Redwood Shores Elementary School 
District 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

24 

24 

24 

0 

0 

0 

0 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

207 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Fox Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.