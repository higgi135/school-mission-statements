“Empowerment through high expectations, achievement, and respect” 
It is our belief that everyone can achieve personal success. Through our daily efforts, all members 
of the Monte Vista Community will be empowered to reach school and individual goals. Monte 
Vista School maintains a commitment to providing a strong instructional program for our K-8 
students. Teachers, staff, parents, and administrators adhere to the principle of putting students 
first and tailor the educational programs and climate to meet the needs of an ever changing school 
population. All students at Monte Vista will experience the joy of learning in a safe and caring 
learning environment that maintains a balanced educational program. 
 
Monte Vista School works throughout the year on the instructional program, striving to improve 
upon the quality education that is provided. In order to provide students with the most 
comprehensive educational experience possible, all aspects of curriculum and instruction are 
aligned with State Common Core Standards and incorporating SEAL and AVID frameworks. Through 
the curriculum alignment process, teachers have developed instructional units designed to target 
instruction while adding breadth and depth to the teaching of state standards. Student progress 
toward meeting standards is monitored through frequent benchmark assessments conducted at 
various times throughout the year in order to ensure that students are making progress towards 
academic goals. Data reflection sessions are held after each diagnostic and benchmark assessment 
to discuss grade level and individual student results. With this information, teachers collaboratively 
design and plan teaching strategies and methodologies, as grade level teams. Grade level teams 
also analyze the results of these adjustments during the course of the year to confirm the positive 
effects of diversity in common core teaching strategies. 
 
As part of the schoolwide focus on English Language Development (ELD) and Academic Language 
Development (ALD), all students participate in either ELD or ALD sessions for 45 minutes each day. 
At each grade level students are grouped by English language proficiency and/or by reading 
proficiency during the ELD/ALD block. This allows teachers to specifically target the language and 
academic needs of the groups. Response to Intervention (RtI), in which student progress is 
monitored and adjusted based on need, is also incorporated in all classrooms. 
 
Various activities are available for student participation outside of the standard curriculum to 
enrich the programs and overall experience at Monte Vista School, including our Meet the Masters 
Art Program, Dance, Music, Puppetry and Theatre as part of our Visual and Performing Arts (VAPA) 
programs. Within the school is a library with extended after school hours that includes varied 
reading levels, chapter books, and resource materials. In addition to the library, Monte Vista hosts 
1 Chrome computer lab containing 32 computers and a mini-lab in the library with 7 additional 
computers. Students have weekly access to computer lab time in addition to the computers within 
every classroom. Through State ASES funding, Monte Vista students also have access to “THINK 
Together,” an after school program that provides students access to homework assistance, healthy 
lifestyle education, physical activity, citizenship building, technology, and enrichment. 
 
 
 

2017-18 School Accountability Report Card for Monte Vista School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Monte Vista School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

29.5 

28 

26 

0 

0 

0 

0 

0 

0 

Mountain View School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

383.8 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Monte Vista School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.