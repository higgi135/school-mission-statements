La Jolla’s mission, as a united Professional Learning Community, is to ensure that all students 
become proficient in language arts and mathematics. 

• Proficiency in the California academic standards 
• Positive self-esteem, self-discipline, and self-expression 
• 
• Critical/creative thinking, problem-solving, and focused decision-making 
• 

Individual excellence and a desire for lifelong learning 

Effective communication and social skills 

So, on behalf of the teaching faculty and support staff, let me welcome you to La Jolla Elementary 
School, home of the Patriots. 
 

 

 

----

---- 

----
---- 
Moreno Valley Unified School 

District 

25634 Alessandro Blvd 

Moreno Valley, CA 92553 

(951) 571-7500 
www.mvusd.net 

 

District Governing Board 

Susan Smith, President 

Jesus M. Holguin, Vice-President 

Cleveland Johnson, Clerk 

Gary E. Baugh. Ed.S., Member 

 

District Administration 

Martinrex Kedziora, Ed.D. 

Superintendent 

Maribel Mattox 

Chief Academic Officer, 

Educational Services 

Tina Daigneault 

Chief Business Official, Business 

Services 

Robert J. Verdi, Ed.D. 

Chief Human Resources Officer, 

Human Resources 

 
 

 

2017-18 School Accountability Report Card for La Jolla Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

La Jolla Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

33 

27 

29 

0 

0 

0 

0 

0 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

La Jolla Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.