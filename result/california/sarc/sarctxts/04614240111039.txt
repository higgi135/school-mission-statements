Academy for Change (AFC) is the community day school serving Chico Unified School District. Our students are referred to AFC for 
three reasons: expulsion, referral from the juvenile justice system as a result of formal or informal probation, or referral from the 
Student Attendance Review Board for habitual truancy. Students referred for expulsion may be students who were expelled from a 
Chico Unified School District school, or students who moved to the area after being expelled from another district. 
 
Academy for Change has three basic rules: Show Up, Be Cool, and Take Care of Business. Show Up refers to the importance of daily 
attendance. Many AFC students have had truancy issues at their previous school. AFC emphasizes the importance of daily attendance. 
Referrals to the office are kept to a minimum, indicating that students are following the Be Cool principle. Appropriate behavior inside 
and outside of the classroom are stressed along with all core academic areas. Take Care of Business points out the need for students 
to complete academic work in a timely and sufficient manner to promote grade levels and progress towards a diploma. 
 
AFC’s mission statement is: “Reconnecting Students with their Educational Responsibility and Future”. Our mission at AFC calls upon 
the educational strengths, unique backgrounds, and supportive nature of its staff to build an alternative education program that serves 
the needs of the community day school student population. The staff provides a program that opens its doors and works to reconnect 
young people to education. It is a program that values self worth and encourages student achievement, both personally and 
academically. 
 
The goal for all AFC students is to earn their way back to either a comprehensive high school or junior high school, or Fair View High 
School, the district’s continuation high school. The responsibility of AFC is to assist students in their progress towards responsibility 
as it relates to academic achievement and personal development.