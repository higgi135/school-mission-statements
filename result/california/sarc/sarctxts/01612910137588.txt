San Leandro High School’s mission is to prepare students to understand, contribute to, and succeed 
in a rapidly changing world and society. San Leandro High School will ensure that our students 
develop both the skills that a rich, culturally relevant, and robust education provides and the 
competencies essential for success and leadership in a diverse and creative world. We will also lead 
in generating practical and theoretical knowledge that enables our scholars to better understand 
our world and improve conditions for local and global communities. 
 
We will fulfill our mission by expanding San Leandro High as a learning community that is responsive 
to change and one that: 

• 

 

 

-Focuses on and provides students with engaging, responsive and personalized learning 
experiences that emphasize academic excellence and global competencies (i.e. critical 
thinking, problem-solving, communication, collaboration, creativity and civic 
engagement). 
-Prioritizes collaborative, project-based learning as part of each student's educational 
experience. 
-Prioritizes humanity and culture in designing systems and environments to improve the 
human condition, an approach that draws on personalized and real-world learning. 

 
San Leandro High School will continue to challenge and motivate each student to achieve his or her 
full potential as a responsible member of our diverse community and society. We offer a positive 
safe learning environment that promotes intellectual growth, health, creativity, and respect for self 
and others. Our vision is to model excellence and encourage academic achievement and personal 
success for all our students. At San Leandro High School students are prepared to be productive 
members of society. Students will be pushed to access their highest level of success whether that 
be a 4-year college, community college to four-year college transfer, or vocational or trade school 
admittance. Each will develop a "ten year" career plan which will outline a detailed pathway to help 
them transition to their next steps beyond high school. 
 
San Leandro High School is committed to building the capacity of staff, families, students and the 
community to establish partnerships that strengthen student learning and improve our school 
environment. We invite our families and community members to partner with us to ensure the 
success of all our students and community at large. Additionally, San Leandro High focuses on 
infusing our school virtues throughout our student body and staff to help foster character 
development and a positive school culture. 
 
Our guiding community virtues are as follows: 

Failure is not an option. 

 We are lifelong learners. 
 We are leaders in training. 
 Don't be part of the problem. Be part of the solution. 
 
 Discipline yourself so no one else has to. 
 
 
 We come from greatness. 

Stay calm at all times. 
If you have a goal nothing is impossible. 

 

2017-18 School Accountability Report Card for San Leandro High School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

San Leandro High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

118 

109 

125.3 

1 

1 

1 

1 

2 

1 

San Leandro Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

418.8 

8 

1 

Teacher Misassignments and Vacant Teacher Positions at this School 

San Leandro High School 

16-17 

17-18 

18-19