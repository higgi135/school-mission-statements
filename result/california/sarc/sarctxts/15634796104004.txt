THE SCHOOL 
Fruitvale Junior High consistently earns the top state test results in Kern County and was most recently awarded the State and National 
"School to Watch" in 2018 by the National Forum for Middle School Reform. FJH was also named a California Gold Ribbon School in 
2017. FJH has been recognized five times (1986, 1990, 1994, 1999 and 2013) as a California Distinguished School by the California 
Department of Education. In 1991, Fruitvale Junior High was named a National Blue Ribbon School by the U.S. Department of 
Education. 
 
Fruitvale Junior High students have repeatedly competed as California State Champions at the National History Day finals in 
Washington D.C. for the past 25 years. Students from FJH earned first place in 1990, 1991, 2002, 2009 and 2010,as well as second 
place in 1996, 1999 and 2001 in Group Performance. Students also earned first place in 2014 and third place in 2015 for an Individual 
Paper submission. Students also placed second in Group Documentary (1999 and 2002), Group Project (2000), Individual Performance 
(2002) and third in Historical Paper in 2001. Students placed first in the State Science Fair in 1997, 1998, 2012, 2013, & 2014. Students 
also annually compete at the State level in the Math Counts Competition. 
 
MISSION STATEMENT 
Fruitvale Junior High graduates will be exceptionally well prepared for success in high school and beyond. To help all students achieve 
this vision, the Fruitvale school community is committed to these goals: 

Specific standards of academic achievement 

Integration of technology as a learning tool throughout the curriculum 

• 
• A safe and orderly adolescent-centered environment 
• A comprehensive core curriculum 
• 
• High expectations for all learners 
• A wide range of co-curricular and extra-curricular experiences 
• 
• 
• 
• A professional development program that focuses on improving classroom instruction 
• An extensive support system for students & their families 

Teaching students responsibility & respect for individual differences 
Encouraging & motivating students to not only do their best, but to take risks 
School improvement based on continual review of student multiple assessment data & parent/staff survey results