Hillside High School is one of 14 schools in the Upland Unified School District. Our school is located 
at the base of the San Gabriel Mountains in the southwest section of the City of Upland. Hillside 
High School can be found among a mixture of residential single dwelling homes, apartments, 
townhome/condominiums, and commercial buildings. 
 
Our student population of 176 students, grades 11 through 12, reflects the changing demographics 
of our community. We value the rich experiences and opportunities that our diversity offers as we 
learn and work together. 
 
Hillside High School has built a tradition of academic excellence and a strong commitment to 
helping every student meet his/her potential. The Hillside staff believes that all children can learn, 
and to that end, our highly qualified professional staff strives to provide every student access to a 
rigorous, standards-based core curriculum. We are extremely proud of our staff’s hard work, our 
parents and community involvement, and of our students’ academic success. We have embraced 
Common Core and shifted to A thru G curriculum. We are incorporating more student-centered 
lessons driven by 21st Century technology that promote greater student achievement and increase 
student engagement. Our teachers are incorporating more student performance assessments and 
facilitating communication and collaboration within their classes. Hillside is embracing the 
necessary changes in education to build student proficiencies with 21st Century skills. I am proud 
to work with such caring and dedicated professionals, who take pride in the influences they have 
on our students. 
 
The dedicated staff of Hillside High School is committed to our District Vision: “All students will 
acquire and apply knowledge, develop understandings, learn skills and meet the challenges of a 
changing world." We at HHS truly care about our students and help them achieve success down the 
path they have chosen.The mission of Hillside High School is to provide a safe environment where 
students can re-establish their academic, personal, and career goals necessary for success in a 
global society. By providing a low student to teacher ratio, attention to the individual and 
personalized instruction, students are able to re-establish their academic and personal goals. 
 

2017-18 School Accountability Report Card for Hillside High School 

Page 1 of 10