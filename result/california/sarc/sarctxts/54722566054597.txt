Description of District 
The Visalia Unified School District is the oldest school district in Tulare County. The district is comprised of 25 elementary schools, 5 
middle schools, 4 comprehensive high schools, 1 continuation high school, 5 charter schools, 1 adult school, and a school that serves 
orthopedic handicapped students. Over 32,000 students Pre-K to adult are served through the Visalia Unified School District. 
 
Description of School 
Goshen Elementary School served approximately 800 students in grades Pre K through6 in 2016-2017. Our teachers and staff are 
dedicated to ensuring the academic success of every student and providing a safe and productive learning experience. The school 
holds high expectations for the academic and social development of all students. Curriculum planning, staff development, and 
assessment activities are focused on assisting students in mastering the state academic content standards, as well as increasing the 
overall student achievement of all subgroups. 
 
School Mission Statement 
Goshen Elementary School is dedicated to supporting all students with standards-based curriculum that will prepare them for a 
successful future in a rapidly changing world and global economy. We are committed to providing a safe, positive climate for learning 
in which all students can reach their fullest potential both academically and socially.