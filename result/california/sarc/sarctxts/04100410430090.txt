In 2017-2018, Learning Community Charter School (LCCS) operated two distinct educational programs with unique student 
demographics. Hearthstone School and STEP UP Academy each served students throughout Butte County and contiguous counties in 
rural Northern California. In Spring 2018, LCCS officially became Hearthstone Charter School after Step Up Academy was closed. In 
2018-2019, Hearthstone will establish new baseline data. 
 
Hearthstone School is a locally-funded, dependent, public charter school authorized by the Butte County Board of Education and 
administered by the Butte County Superintendent of Schools through Butte County Office of Education's Student Programs and 
Services Division. Hearthstone offers free and appropriate educational services through alternative instructional programs for those 
students who are not well-served by the existing comprehensive public school environment. 
 
Like all California Schools, LCCS is implementing the California Common Core State Standards (CCSS). LCCS believes that learning best 
occurs in an interconnection of home, classroom and/or community, where parents, students, teachers and community members, as 
educational partners, are mutually invested in student success. 
 
Hearthstone is designed to prepare students for life in the 21st century through development of strong academic skills and applied 
life skills, use of technology and the arts in learning and communicating, authentic and performance-based assessment, integration of 
a wide range of community resources, development of interpersonal and cognitive skills and growth of personal qualities. This program 
is designed to prepare students to become active agents in the lifelong learning process; to become responsible, effective and 
productive citizens; to exert influence responsibly and to affect positive, successful change in their lives. 
 
Hearthstone School's specific Mission and Vision are as follows 
Mission: To bring inspiration and personalization to every student’s educational experience. 
Vision: Establish collaborative partnerships that provide a safe, personalized learning experience where students develop special 
talents and individual responsibility that enable them to achieve their potential and become knowledgeable, productive citizens. 
 
 
 

2017-18 School Accountability Report Card for Hearthstone 

Page 2 of 15