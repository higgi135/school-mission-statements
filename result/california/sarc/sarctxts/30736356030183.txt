Vision 
Gates Elementary School is a snapshot of California's population. We serve students of diverse 
cultural, ethnic, and socio-economic backgrounds. The uniqueness of our student population is 
addressed through a stimulating learning environment with instruction that meets students' 
appropriate academic level. We strive for high academic standards and expectations for all 
students. 
 
Self-esteem is a critical facet in the education of the whole child. Through an environment of 
mutual respect, positive reinforcement and opportunities to build good character, our students 
develop a positive self concept. This positive self-esteem is the vehicle through which all students 
become responsible citizens and learn the skills necessary to adapt to a constantly changing society. 
 
We encourage our parents to be active partners in the education of their children by providing 
information and resources that allow them to take an appropriate role in supporting their child's 
learning. The team of child, teacher, and parent working together is key to the success of our 
students. With this team support, as students leave Gates School they are able to successfully meet 
the challenges that lie ahead of them in this multi-faceted world. 
 
Mission 
The mission of the Gates school community is to meet the needs of the whole child so that each 
child can be a successful individual and a contributing member of society. 
 

 

 

-
-
-
-
-
-
-
- 

----

-

--- 

Saddleback Valley Unified School 

District 

25631 Peter A. Hartman Way 

Mission Viejo CA, 92691 

(949) 586-1234 
www.svusd.org 

 

District Governing Board 

Suzie Swartz, President 

Dr. Edward Wong, Vice President 

Amanda Morrell, Clerk 

Barbara Schulman, Member 

Greg Kunath, Member 

 

District Administration 

Crystal Turner, Ed D. 

Superintendent 

Dr. Terry Stanfill 

Assistant Superintendent, Human 

Resources 

Connie Cavanaugh 

Assistant Superintendent, Business 

Laura Ott 

Assistant Superintendent, 

Educational Services 

Mark Perez 

Director, Communications and 

Administrative Services 

Dr. Ron Pirayoff 

Director, Secondary Education 

Liza Zielasko 

Director, Elementary Education 

Dr. Diane Clark 

Director, Special Education 

Rae Lynn Nelson 
Director, SELPA 

Dr. Francis Dizon 

Director, Student Services 

 

2017-18 School Accountability Report Card for Ralph A. Gates Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ralph A. Gates Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

38 

40 

36 

0 

0 

0 

0 

0 

0 

Saddleback Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1,157 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.