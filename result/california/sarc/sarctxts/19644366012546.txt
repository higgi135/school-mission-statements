Manzanita Elementary School is a TK-5th grade school located in the City of Covina that serves the communities of Irwindale and 
unincorporated Covina with an enrollment of approximately 305 and growing as we grow our Dual Immersion Magnet Program. 
Manzanita is part of the Covina-Valley Unified School District, which includes nine elementary schools serving grades TK-5, three 
middle schools serving grades 6-8, three comprehensive high schools, and one alternative school. We offer students a balanced, 
standards-based curriculum that promotes success and excellence in education. Manzanita teachers and staff focus on providing 
students with an array of opportunities to master the California State Common Core Standards in English Language Arts and 
Mathematics, in addition to the state standards for Science and Social Studies. Our school hosts the District Dual Immersion Magnet 
program and the Think Together after school care for the school age students that attend Manzanita. 
 
Manzanita Elementary is a school of excellence where every student, in all capacities, is highly valued. Our focus has continued to be 
on closing the achievement gap for all underachieving students and enriching instruction for all students proficient or above. Our 
credentialed teachers provide after school tutoring sessions to all students. Our school demographics include 89% students of Latino 
descent, 6% White, 4% Asian and 1% other. With 85% of our students socioeconomically disadvantaged and 22% learning English as 
their second language, the entire school community is committed to our students finishing their elementary education with their 
21st Century skills in place to guarantee their future success. 
 
Our three-fold goal focuses on the whole child through nutrition, physical exercise, and academic instruction. We are most proud of 
our “What I Need Now” (WINN) program, which targets achievement in ELA and math. We feel that WINN, our model practice, is an 
exemplary program. Through our daily instructional practices, which includes our WINN time, students in Tier I receive enrichment 
activities, those in Tier II receive specialized support, and those in Tier III receive intensive intervention. We are proud of our current 
journey towards closing the achievement gap for all students at Manzanita and assert that our innovative WINN will develop 
inquisitive and versatile lifelong learners who will achieve their academic potential. 
 
Our instructional focus was developed through collaborative meetings and PLCs to review our school wide data. We are committed 
that all Manzanita students will read closely to determine what the text says explicitly, make logical inferences from it, and cite specific 
textual evidence when writing or speaking to support conclusions drawn from the text as measured by i-Ready Standards Mastery 
Assessments. We are also looking how this can be used in Math to support students citing evidence when solving problems. We 
support our Dual Immersion students through the use of iStation which is a similar program to i-Ready and based on Spanish Language 
Arts Standards. 
 
Our goal is for every TK-5th grade student to infuse the use of Chromebooks/Lenovos into our instructional model. Teachers receive 
ongoing intensive professional development to infuse technology into their daily instructional model. We have implemented coding 
through Code to the Future coaching model. Each student in grades TK-5 focuses on a gradual release model of coding at their 
developmental level. Each trimester, we showcase our program through an Epic Build where student ambassadors highlight the 
program and invite parents to explore the learning in the classroom. 
 
In addition to these foci, Manzanita houses the District Dual Immersion Spanish/English 90:10 model. For the past two years, 
Manzanita has grown this model starting with Kindergarten and opening two 1st grade classrooms this year. Students are immersed 
in the target language and also gradually released to a 50:50 model by 4th and 5th grade. The goal of the program is to develop 
bilingual, bilateral and Multicultural citizens who are prepared to continue their language learning into middle and high school with 
the ultimate goal being the Seal of Biliteracy on their high school diploma. 
 
Manzanita Elementary School is one of nine computer science magnet schools in the Covina-Valley Unified School District. Manzanita 
is a one-to-one school where all students have their own Chromebook or Lenovo and are learning CODE as their second language. This 
year Manzanita is partnering with Code to the Future. Students in grades TK-5 use their 21st Century skills of collaboration, critical 
thinking, creativity, and communication in an environment where computer science is taught as a normal discipline in the classroom. 

2017-18 School Accountability Report Card for Manzanita Elementary School 

Page 2 of 12 

 

 
Manzanita also received a grant from Mr. Holland's Opus to bring a music program to all students in grades 3-5. Students are able to 
select Beats, Beginning Band or Advanced Band and Mr. Holland's Opus has provided instruments for any student wanting to 
participate. 
 
The Manzanita staff is highly qualified with 100% of the teaching staff NCLB compliant, fully credentialed, BCLAD or CLAD certified (or 
its equivalent). Our staff includes three Special Education teachers in our Specialized Academic Instruction Programs. In addition, we 
have a part time Speech and Language Pathologist, a part time Adaptive PE teacher, part time OT and a part time school psychologist 
. Manzanita also has a Title I Intervention Specialist who works with our socioeconomically disadvantaged population, coordinates and 
teaches our intervention program. 
 
Our parents and PTA are important partners in our students' education. Our parents volunteer in the classroom, organize special 
events, and raise money to provide extra things that make school special such as field trips, parent nights, assemblies, movie nights, 
Festivals, Trunk-or-Treat, and so much more. 
 
We take instruction and social development seriously and are dedicated to providing a rigorous academic program preparing students 
for success in college and beyond! 
 
MANZANITA VISION AND MISSION STATEMENT 
Manzanita Elementary is truly a school that instills a genuine sense of pride and success shared by all. Our goal for every student is to 
provide a strong academic education, develop critical thinkers who are prepared for the rigors of middle and high school, develop 
students who are college and career ready and prepared to be successful in a global society. 
 
MISIÓN Y VISIÓN DE LA ESCUELA MANZANITA 
La Escuela Primaria Manzanita es verdaderamente una escuela que inculca un genuino sentido de orgullo y éxito compartido por 
todos. Nuestro objetivo para todos los estudiantes es proporcionar una sólida educación académica, desarrollar pensadores críticos 
que estén preparados para los rigores de la escuela secundaria y preparatoria, desarrollar estudiantes listos para la universidad y 
preparados para ser exitosos en una sociedad global. 
 
MANZANITA MISSION OF THE DUAL IMMERSION LANGUAGE ACADEMY 
The mission of the Covina-Valley Dual Immersion Magnet Program at Manzanita Elementary School will be to develop bilingual and 
biliterate citizens who are well prepared to succeed in a global economy. Our purpose will be to teach students to embrace and 
celebrate diversity, to appreciate their home language and culture, and to learn and use a second language in their everyday lives. We 
will provide rigorous academic standards and excellent language models for our students. 
 
MISIÓN DE LA ACADEMIA DE DOBLE INMERSIÓN DE LA ESCUELA MANZANITA 
La misión de la academia de doble inmersión de la escuela Manzanita es para desarrollar a ciudadanos bilingües que estén bien 
preparados para tener éxito en una economía global. Nuestro propósito será enseñar a los alumnos a comprender y a celebrar la 
diversidad, para apreciar su idioma y cultura natal y para aprender o utilizar un segundo idioma en sus vidas diarias. 
Proporcionaremos estándares académicos rigurosos y los modelos excelentes del idioma para nuestros alumnos. 
 
INSTRUCTIONAL FOCUS STATEMENT 
All Manzanita students will read closely to determine what the text says explicitly and to make logical inferences from it; cite specific 
textual evidence when writing or speaking to support conclusions drawn from the text. These strategies will be applied in all content 
areas and supported through 3 research based instructional strategies: 1) Thinking Maps; 2) Close Reading Strategies as Supported 
through Nancy Boyles research; and 3) Depths of Knowledge Thinking Stems. 
 

2017-18 School Accountability Report Card for Manzanita Elementary School 

Page 3 of 12