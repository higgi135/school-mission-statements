San Mateo High School (SMHS) first opened in 1902 with an enrollment of merely 14 students and a staff of one principal/teacher and 
two assistant teachers. Now it is a vibrant and diverse community that thrives on Bearcat “peace, passion, and pride.” At San Mateo 
High School, staff and students unite around academic rigor and an unrivaled tradition of school spirit. SMHS stands out, even in the 
state of California, as a place where diversity is valued. Staff works constantly on being a cohesive whole, giving students opportunities 
to create lasting memories with people from a vast array of backgrounds. San Mateo High School is a passionate place, and the quad 
bustles with the energy of a college campus. The students, intensely spirited, strive to meet their school’s potential and to better their 
rivals. The staff, highly qualified, serves as both academic and community activists. The parents, highly involved, dedicate time to 
supporting the school’s ambitions. The community works to put words into action: reaching toward rising expectations, SMHS focuses 
on what is possible rather than on what is not. SMHS’s vision is to equitably prepare all students for a successful future. 
 
San Mateo High School excels in Advanced Placement and specialized courses, visual and performing arts, student leadership and 
government, and athletics. SMHS works to integrate the latest technology into the school facilities. While striving to prepare students 
for both college and the working world, SMHS maintains a commitment to community service and volunteerism. SMHS staff recognizes 
that the students have a wide array of needs, from acquiring a foundation of academic skills, to learning at an accelerated pace, to 
developing language, to overcoming disabilities. SMHS expects students to develop socially and academically, becoming responsible 
members of the community as well as productive and successful members of society. 
 
San Mateo High School’s vision is to equitably prepare all students for a successful future. 
 
San Mateo High School’s mission is to foster knowledge, growth, and personal responsibility so that our students will make purposeful 
academic, career, and life choices. 
 
Schoolwide Learner Outcomes 
 
PEACE 
 
SMHS students will: 

• Respect diversity and consider multiple points of view 
• Communicate clearly and conscientiously through multiple mediums 
• Resolve conflicts equitably 

PASSION 
 
SMHS students will: 

• Explore interests in class and through co-curricular activities 
• Think critically to pose thoughtful questions, define problems, analyze information, and propose solutions 
• Aspire to grow academically 

PRIDE 
 
SMHS students will: 

• Participate actively in the school community 
• Strive to reach personal goals and take on challenges 
• Graduate high school and pursue post-secondary options based on their strengths, skills, and interests 

 

2017-18 School Accountability Report Card for SAN MATEO HIGH SCHOOL 

Page 2 of 13