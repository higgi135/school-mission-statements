Welcome to Calero High School. Calero is a small Alternative High School of Choice for students 
who have not found success at our large comprehensive high schools. At Calero High School we 
believe that ALL students should graduate prepared for college and careers in a global society. The 
educational program at Calero is focused on collaborative learning in both classroom and online 
environments to provide an innovative and tailored learning experience for students. Students 
access all the courses required to both graduate and gain admission to universities. Calero's 
student population is small, serving just over 200 students with access to a personal and supportive 
learning structure. Each student has access to a personal computer in order to use technology as 
a 21st century tool for learning, communicating, organizing, collaborating, and presenting. At 
Calero, we believe parents/guardians are an integral part of the educational process and promote 
both communication and partnerships between parents and staff. Calero opened in the fall of 2013 
with 10th and 11th graders and added 12th graders in 2014. 
 
Calero's Mission is to help students recover units, build success habits, and plan for a successful 
future. 
 
What makes Calero special? 
 
Small learning environment 
Focus on preparing students for college and careers 
Online courses providing both enrichment and credit recovery 
Staff mentor for all 3 years 
Campus opens at 7:30 am to provide students access to the Firehouse Room with classes starting 
at 8:30 am 
Tutorial/Enrichment activities Monday - Thursday from 3:45 pm – 4:45 pm 
Each student will have access to a computing device 
Access to a summer program 
Summer Bridge Program for all incoming students 
Tailored to the struggling student who did not find success in the traditional high school 
environment 
 
The Calero site also hosts the Adult Transition Program for adults with special needs, ages 18 - 22. 
The goal of the program is to help prepare students for independent living and employment. The 
program enrollment is 130 students. 
 

2017-18 School Accountability Report Card for Calero High School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Calero High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

21 

19 

21 

1 

0 

3 

0 

1 

0 

East Side Union High School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

991.5c 

50.6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Calero High School 

16-17 

17-18 

18-19