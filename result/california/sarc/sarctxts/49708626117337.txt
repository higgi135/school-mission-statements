Mission Statement: 
At Crossroads we believe all students can succeed. Our mission is to cultivate a safe, supportive and nurturing community with positive 
and consistent structure which provides an opportunity for our students develop a strong socio-emotional foundation. We work 
closely with our stakeholders to provide a curriculum that is differentiated, engaging and relevant to real life and the world we live in. 
We expect high levels of academic achievement from our students as measured by performance standards, as well as increased 
attendance and improved citizenship.