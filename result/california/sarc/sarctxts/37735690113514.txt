Welcome to Foussat Elementary School! Our entire staff is committed to providing a quality 
learning environment for all students. Our skilled and caring teachers are dedicated to meeting the 
needs of all children. Our commitment to educational excellence is at the core of our instructional 
program. We strive to provide our students with the necessary skills to be college or career ready. 
We address these needs through the implementation of the Common Core State Standards, English 
Language Development standards and the 21st Century Skills of Communication, Collaboration, 
Creativity and Critical Thinking. 
 
Foussat Elementary has a rich and challenging curriculum that develops creative, confident, and 
capable students. Our students respect each other’s differences and show compassion through acts 
of kindness at school and in the community. Our students learn teamwork and collaboration 
through partnerships modeled by staff and parents. Foussat prides itself on its spirit of optimism, 
enthusiasm, determination, and on its ongoing quest for excellence. 
 
We are proud of our DREAMS Lab (Design, Research, Engineering, Math, Science) which is in its first 
year of operation. All students in Kinder through 5th grade rotate through the lab and are given 
the opportunity to investigate real world problems and design solutions. This helps to foster and 
promote students natural curiosity about the world around them. We strive daily to help our 
students reach their full academic potential and to know they can achieve anything they are willing 
to work for. 
 

 

 

----

---- 

----

---- 

Oceanside Unified School District 

2111 Mission Avenue 
Oceanside CA, 92058 

(760) 966-4000 
www.oside.us 

 

District Governing Board 

Eleanor Juanita Evans, President 

Mike Blessing, Vice President 

Eric Joyce, Clerk 

Raquel Alvarez, Member 

Stacy Begin, Member 

 

District Administration 

Julie Vitale, Ph. D. 
Superintendent 

Shannon Soto, Ed. D. 

Deputy Superintendent 

Todd McAteer 

Associate Superintendent Human 

Resources 

Mercedes Lovie, Ed. D 

Associate Superintendent Business 

Services 

 

2017-18 School Accountability Report Card for Louise Foussat Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Louise Foussat Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

34.00 

0.0 

0.0 

32 

1.0 

 

 

 

 

Oceanside Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

28 

1 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.