Located in the rural high desert of Southern California, 126 miles from Los Angeles and 144 miles from Las Vegas, Silver Valley Unified 
School District educates nearly 2,200 students in grades transitional-kinder through twelve. Covering an area of 3,200 square miles, 
the district serves the communities of Calico, Daggett, Fort Irwin, Ludlow, Newberry Springs, and Yermo. 
 
The district is comprised of seven school sites including three elementary schools, two middle schools, one comprehensive high school, 
and an alternative education center. The alternative education center is made up of four schools and includes Calico High School, 
Silver Valley Academy, Silver Valley Community Day School, and Silver Valley Adult Education all at one location 
 
Located in a rural country area 25 miles East of Barstow, Newberry Springs School served 105 transitional kindergarten through fifth 
grade students on a traditional calendar schedule during the 2016-17 school year. 
 
Mission 
Newberry Springs Elementary School is a community of optimal learning for every student "By Name, By Need, and By Skill". Through 
staff collaboration we will work together to promote a respectful, responsible and safe learning environment for all students. 
 
Vision 
All students at Newberry Springs Elementary School will be respectful and responsible students who strive to learn and grow into 
productive citizens in our community.