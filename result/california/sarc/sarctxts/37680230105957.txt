Liberty School was the 41st school in the Chula Vista Elementary School District, which now has 49 
schools, including charters. Liberty is located in an area of relatively new homes and new 
construction. We opened in July 2004 with 300 students and have grown over the years to a 
population of 749 students and currently at 729 students. Most of our students live in single family 
dwellings. Approximately 15% of our students qualify for free or reduced-price meals and 17% are 
English Learners. Our school population is rich in ethnic diversity. 
 
Mission 
Liberty School provides a safe, nurturing, supportive learning environment for every member of 
the school community. We foster the continuous academic and social growth of all students by 
engaging them in challenging, enriching learning experiences that meet their individual needs and 
prepare them for life in the 21st century. This is accomplished by immersing students in a 
standards-based curriculum that demonstrates relevance to their lives, provides in-depth 
experiences in all curricular areas, and provides a strong foundation for future learning. The 
building blocks of the academic program are commitment, competency, communication and 
collaboration. Staff members take responsibility for basing instruction on best practices by 
engaging in ongoing professional development through research, reading, and collaboration. 
Teachers are adept at using ongoing assessments and data to guide instruction. Techniques, 
strategies, and results are shared with parents on an ongoing basis, thereby giving parents the 
opportunity to be true partners in the academic and social growth of their children. 
 
Teachers use the results of standardized tests and other District and site-specific formative and 
summative assessments to identify learning strengths and gaps, to design the instructional 
program, and to determine staff development needs. 
 
 
 

2017-18 School Accountability Report Card for Liberty Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Liberty Elementary School 

With Full Credential 

Without Full Credential 

16-17 17-18 18-19 

32 

0 

32 

32 

0 

 

0 

0 

Teaching Outside Subject Area of Competence 

NA 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Liberty Elementary School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.