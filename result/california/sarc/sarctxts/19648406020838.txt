Welcome to Glazier Elementary School, home of the Gladiators, where we have made great 
progress toward 
increasing student achievement through our challenging academic and 
enrichment programs. Glazier Elementary is an exciting learning community that is student-
centered and provides an engaging environment. As a Title I school, we are committed to providing 
the best-quality education using the most effective instructional practices to boost student 
achievement. Glazier has shown evidence of closing the achievement gaps and increasing 
proficiency rates among minority students. 
 
In 2016–2017 our staff implemented Common Core curriculum to increase comprehension in all 
subject areas. A Collaborative Leadership Team comprised of administrators, literacy coaches, and 
teachers worked cohesively create a balanced literacy program for all levels that includes read-
aloud, shared reading, guided reading, independent reading, customized small-group instruction, 
and writing. Math Practices and routines have been embedded in our daily math block to increase 
fluency and understanding of mathematical skills. Our instructional program includes the 
implementation of English Language Development (ELD) and Academic Language Development 
(ALD), which provides explicit language instruction to our students. This program is designed to 
increase our students’ language skills and fluency. Glazier implemented professional development 
as we transitioned to the Common Core State Standards and utilized newly developed and revised 
curricular units in both Language Arts and Math. A school-wide intervention block called Response 
to Intervention (RtI) is incorporated in our daily schedule to target areas of need for all of our 
students. Technology-based programs and tools such as Smartboards and iPads have been 
integrated in the program to target instruction. The i-Ready online program provides teaching and 
assessment tools for students to use both at school and at home to individualize and target 
instruction based on each student's academic needs. 
 
Our Gladiators have been immersed into the wonderful world of the Arts. Glazier provides music 
instruction to all of our students in Transitional Kindergarten through 5th Grade. Our “Music Hall” 
curriculum focuses on Rhythm and Movement activities where students use a variety of 
instruments. Glazier also offers music classes in Flutes, Choir, and Rhythm Crew. Additionally, 
Glazier offers an amazing musical theater program which culminates in an elaborate stage 
production in the Spring. Glazier Elementary has been a lab school for the Music in Education in 
National Consortium. This partnership enabled our staff to design and implement a rich music-
infused curriculum across the grade levels. Through our “Meet the Masters” program, students and 
teachers are enriched with the Visual Arts by studying famous artists and creating their own 
masterpieces. We are extremely proud of our students and continue to see an increase in student 
achievement as a result of the Arts implementation. 
 
Our Glazier Certificated and Classified staff firmly believes in a Positive Behavior Support model 
whereby Glazier students are celebrated for exhibiting behaviors that are T.R.R.F.C.C. (Trustworthy, 
Respectful, Responsible, Fair, Caring, Citizenship). Students are recognized for demonstrating 
mastery and/or improvement in citizenship through our Student of the Month program. Students 
are also recognized as Golden Gladiators for making positive choices in all areas of the campus. By 
targeting our instructional practices and extracurricular activities, our staff and families are 
contributing to creating well-rounded children. 

2017-18 School Accountability Report Card for Anna M. Glazier Elementary School 

Page 1 of 8 

 

 
Parent involvement has been a key factor to increasing student achievement at Glazier. Parents are encouraged to volunteer in the 
classroom and on academic excursions, participate in school events (i.e. performances, Read Across America, Red Ribbon Week, Food 
Drives, fundraisers, Spring Festival, etc.), attend conferences and parent education classes offered on site, and participate in leadership 
through our School Site Council (SSC), English Learner Advisory Committee (ELAC), and Parent-Teacher Association (PTA). Our partnership 
continues to grow stronger every year and the staff at Glazier are honored to have such an involved community. We are proud to be 
Gladiators!