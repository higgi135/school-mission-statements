The passage of Proposition 98 included a requirement that each school in the state produce an annual Report Card to inform parents 
and the community about the school and its programs. As you read this, our 28th Report Card, you will understand more about our 
total school program. 
 
Our school staff included a total of 22 Credentialed Teachers, one Administrator, and the following support personnel: 13 Instructional 
Assistants, 2 Secretaries, 2 Bilingual Aides, a Library Clerk, 2 Custodians and 4 yard duty supervisors. Our school has an active Parent 
Club, School Site Council, and English Language Advisory Committee (ELAC). 
 
With the implementation of Common Core Standards there has been an emphasis on providing training for our teachers in this area. 
Along with district wide in-services, teachers have been provided the opportunity to attend multiple trainings on the Common Core 
Standards in both Language Arts, Math, and New Generation Science Standards (NGSS). 
 
The discipline philosophy of Love and Logic has been implemented school wide. This is a discipline philosophy that encourages 
relationship building with students and students being held accountable for the natural consequences that come with decisions they 
make. Teachers have been inserviced on the components of Love and Logic and are encouraged to use it daily with their students. Our 
library contains Love and Logic resources that are available to staff and parents to use. Teachers are encouraged to attend trainings 
and conferences involving Love & Logic. Love and Logic also supports our Multi Tier System of support that will assist our students 
with social and emotional needs. 
 
We strongly encourage positive character traits for our students. Each month a specific character trait is identified school wide and is 
discussed by the teachers with their students. Recognition of students who demonstrate positive character traits are done at all 
monthly assemblies. 
 
Our Mission Statement is: "We create critical thinkers to develop powerful leaders."