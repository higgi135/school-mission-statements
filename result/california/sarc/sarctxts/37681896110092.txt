Lemon Crest School was established in 1991 in Lakeside, California. Lemon Crest Elementary School 
provides a full academic program for grades K-5 general education students and special education 
students. 
 
Lemon Crest Elementary School is dedicated to providing all students with a rigorous curriculum 
with emphasis in the Arts and Sciences in a safe, supportive and disciplined environment. We are 
also dedicated to providing for the academic and social emotional needs of all students. Lemon 
Crest has established Professional Learning Communities in order to expand and enhance staff 
development and collaboration. Lemon Crest staff meets for 90 minutes per week in collaborative 
groups to discuss student academic performance and how to ensure each student is receiving the 
instruction they need to be successful. We have implemented research based intervention 
programs to meet the diverse needs of our students. English Learners and at-risk students in grades 
K-5 use Imagine Learning English in class, as well as during designated ELD time. At-risk students in 
Kindergarten through 2nd grade are utilizing web-based computer programs including Smarty Ants 
and RAZ Kids in their classrooms. At-risk students in grades 3, 4 and 5 are enjoying tremendous 
results using READ 180 and System 44, which are research-based curricula designed to increase 
student achievement in reading, written instruction, and vocabulary. We also have implemented 
the research-based Achieve 3000 software program as additional literacy support for our 2nd - 5th 
grade students. In addition, all of our at-risk students receive intensive small group instruction and 
intervention in conjunction with computer assisted learning from their teachers and Intervention 
teachers that push-in to their classrooms to provide support. Furthermore, Lemon Crest proudly 
launched a 2-way Spanish/English Dual Immersion program in the fall of 2015. We currently have 
a Kindergarten - Third grade Grade Dual Immersion classes, and will add a class each school year 
until we have a complete Kindergarten - 5th grade program. 
 
To meet the social emotional needs of our students, we enthusiastically participate in a 
combination of Positive Behavioral Interventions and Supports (PBIS) and Responsive Classroom. 
Through PBIS and Responsive Classroom, Lemon Crest will focus on four behavioral expectations 
that are positively stated and easy to remember. In other words, rather than telling students what 
not to do, we will focus on the preferred behaviors. These expectations will focus on building 
communication and collaboration skills, which in turn will strengthen school community as well as 
empathy. Lemon Crest will follow the following expectations: Lemon Crest Lions ROAR - Respect, 
On Task, Always Safe, and Responsible, as well as disagreeing appropriately, accepting criticism and 
consequences, and working with others. The students of Lemon Crest School also receive mental 
health services through access to our School Counselor who runs small group instruction for at risk 
students on social skills, conflict resolution and other skills necessary for school success. We also 
have an active Student Council. 
 
Lemon Crest Mission Statement: Lakeside Union School District dedicates itself to providing a 
nurturing and challenging environment that is committed to each individual student's education 
and development. The Lemon Crest Elementary School mission is to enhance student achievement 
by providing a safe, nurturing environment where children thrive and reach their full potential. We 
accomplish this with our Lemon Crest's Positive Behavioral Interventions and Supports (PBIS) 
program. We expect all of our students to ROAR by showing Respect, On Task, Always Respectful, 
and Responsible behavior. 
 
 

 

2017-18 School Accountability Report Card for Lemon Crest Elementary School 

Page 1 of 11 

 

Our vision: 
 
1) At Lemon Crest we strive to positively impact student achievement 
 
2) We will continue to partner with our community emphasizing respect, kindness, and responsibility 
 
3) Our learning community will be literacy rich, technology enhanced, and standards-driven and will incorporate the 4 C's and 21st Century 
learning opportunities 
 
4) We will prepare all our students for life outside the school day, focusing on the soft skills needed to be globally competent and 
successful. 
 
Mutual commitment to a quality educational program will ensure the best learning for our students. This agreement is a promise that 
school staff, students, and parents will work together for student success. 
 
THE SCHOOL PROMISE: 
 
We know the importance of a successful school experience for every student. Therefore, the staff promises to carry out the following 
responsibilities to the best of our ability: 

• We will teach grade level skills and concepts using effective teaching methods. 
• We will strive to address the individual needs of all students. 
• We will communicate frequently with parents regarding student progress. 
• We will provide a safe, positive, and healthy learning environment for our students.