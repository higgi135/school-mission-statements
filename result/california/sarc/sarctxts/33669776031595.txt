Wells Middle School, located in the central region of Riverside and serves students in grades six 
through eight following a traditional calendar, is a collaborative community focused on 
Relationships, Relevance, and Rigor. In this accountability report card, you will find valuable 
information about our academic achievement, professional staff, curricular programs, instructional 
materials, safety procedures, classroom environment, and the condition of our facilities. Our school 
mascot, the Phoenix, represents the spirit of our students and staff, and is summarized in our school 
motto, Soaring to Excellence! Rising Above! 
 
In the implementation of Common Core State Standards and the Next Generation Science 
Standards, our emphasis is on literacy, numeracy, and equity. Our teachers regularly utilize 
formative assessments, and are shifting from assessment of learning to assessment for learning. 
Student voice is paramount in all decisions made at Wells, and positive behavior is reinforced 
through restorative practices. Academic awards and positive incentives for attendance and 
citizenship promote a caring climate that enhances student learning and creativity. 
 
Exceptional programs at Wells include Promethean Academy, AVID, Project Lead the Way, CTE 
Woodshop, CTE Media Arts, physical education, and visual and performing arts. We have a campus 
wide focus on wellness, which brings us together as we emphasize the links between healthy bodies 
and healthy minds, and we are a gold level winner in the National Healthy School Awards, given by 
the Alliance for a Healthy Generation. Students have access to two full-time counselors and one 
half-time counselor and additional support programs that include counseling services, community 
partnerships, and Prime Time to assist them in meeting their academic, social and emotional needs. 
Our positive relationship with parents and our community is maintained through Wells Parent 
University, PTSA, ELAC, and School Site Council. Our highlights of community involvement are our 
Dia de los Muertos celebration in the fall and our Color Run in the spring. 
 
Wells Middle School, Home of the Phoenix, is committed to all students realizing their unlimited 
potential. Our collective efficacy comes from the dedication of all staff members in these 
important, transitional years between elementary school and high school. We are honored to 
support our students during this time, in collaboration with our parents and community. 
 
Mission Statement 
Alvord Unified School District, a dynamic learning community that embraces innovation, exists to 
ensure all students attain lifelong success through a system distinguished by: 

• Active and inclusive partnerships 
• Relationships that foster a culture of trust and integrity 
• High expectations and equitable learning opportunities for all 
• A mindset that promotes continuous improvement 
• Multiple opportunities for exploration and creativity 
• Professional development that promotes quality teaching and learning 
• Access to learning experiences that promote a high quality of life 

 
In addition, all schools strive to attain the Alvord vision that all students will realize their unlimited 
potential. 
 

2017-18 School Accountability Report Card for Wells Middle School 

Page 1 of 9