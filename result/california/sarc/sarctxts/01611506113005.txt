Jensen Ranch Elementary School is located in the easternmost side of Castro Valley, surrounded by 
picturesque canyons and winding walking trails, and the housing community of Palomares Hills. The 
name Jensen Ranch is reflective of a rich heritage that dates back to the time when Castro Valley 
was a rural farming community. 
 
Jensen Ranch exemplifies the quote, “It takes an entire village to raise a child.” Through cooperative 
efforts of parents, staff, and students, Jensen Ranch School maintains a learning community where 
school staff and families work together. Communication, respect, responsibility, an expectation of 
excellence, and openness to life-long learning permeate the school. 
 
Our teachers and staff recognize the unique gifts of each child, and through parent-teacher 
opportunities, strive to develop the emotional, social, physical, and intellectual growth of each 
student. We proudly present this edition of our “report card” to parents and the community. 
 
Mission Statement: 
 
Jensen Ranch School is dedicated to establishing and maintaining a learning environment where 
school, community, and individuals work together in an atmosphere of open communication, 
respect, and creative spirit to foster an expectation of excellence, responsibility, and life-long 
learning. 
 
We are dedicated to helping students reach their fullest potential by: 

• Achieving goals of education by providing for mastery of academic skills, the 

development of critical and creative thought, and an openness to life-long learning; 

• Promoting and modeling excellence and personal best; 
• Recognizing the unique value of each individual while nurturing the social, emotional, 

• 

physical and intellectual growth of each student; 
Encouraging 
responsibility, cooperation, 
compassion, and consideration of others within our classrooms, school, and 
community; and promoting a partnership between home, school, and community. 

the development of self-discipline, 

2017-18 School Accountability Report Card for Jensen Ranch Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Jensen Ranch Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

19 

20 

19 

0 

0 

0 

0 

0 

0 

Castro Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

443 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Jensen Ranch Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.