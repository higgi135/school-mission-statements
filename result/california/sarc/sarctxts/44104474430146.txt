The Alternative Education Department of the Santa Cruz County Office of Education offers 24 
unique academic programs at 19 different school sites throughout the county. The Alternative 
Education Court and Community programs serve students in grades 6 to 12 with the vast majority 
of students being in high school. The Alternative Education Program has steadily grown and evolved 
over the years to meet the ever-changing demands of our student population but our mission and 
philosophy remain the same. The ultimate goal of our program is to ensure that every student in 
our County has access to an educational program that suits the individual’s unique need. This is 
accomplished through a variety of locations, educational models, and programmatic structures. 
During the course of the school year, we serve anywhere between 600 and 900 students. The vast 
majority of our students come to us deficient in credits. Typically, our students are affected by one 
or more significant life challenges. Frequently, these include drug and alcohol abuse, homelessness, 
criminal activity, truancy, expulsion, poverty, lack of fluency in English, academic failure, and 
trauma. The majority of our students are continuously enrolled for less than an academic year. High 
student mobility and short length of enrollment make data collection for student outcomes 
problematic. 
 
Our Mission: The mission of the Santa Cruz County Office Alternative Education Program is to 
provide a safe, supportive learning environment that empowers students to achieve academic, 
social, and vocational excellence. 
 
Our Philosophy: We believe that the educational success of our students is dependent upon quality 
academic and effective programs, which are supported by a healthy organization, our students' 
families, and effective community partnerships. Our programs are student-centered and adaptive 
to meet individual needs. We value personal and professional development. Staff works 
collaboratively to facilitate learning and change. 
 
We Believe That: 

• All students can learn. 
• All students can grow socially and emotionally to become productive citizens. 
• 
• 

Each student should be given the opportunity to fully develop his/her potential. 
There is a need to facilitate learning by drawing on individual strengths and learning 
styles. 
Structured educational environments and programs help our students to learn. 
Each student has a right to a physically and emotionally safe environment that is 
conducive to learning. 
There is strength in diversity. 

• 
• 

• 
• Collaborative relationships are essential in delivering quality services and effective 

programs to our students. 

• We are accountable through evaluation of students and programs. 

We provide a number of services for our students intended to ensure that our students can benefit 
from the academic program. In addition to ensuring that all students have access to courses 
required for graduation, we provide a variety of services to meet student needs. These include: 
counseling, free and reduced priced meals, special education services, work based learning, and 
college and career counseling. Our programs include a site at Juvenile Hall, an independent studies 
center on the local community college campus, two green career center high schools, multiple 
middle school programs, thematic single classroom programs, conventional community schools, 
and an all girls program. 

2017-18 School Accountability Report Card for Santa Cruz County Court School 

Page 1 of 11 

 

 
Court School Programs Include: 
 
Robert A. Hartman School 
 
Robert A. Hartman School provides for the education of Youth detained at Santa Cruz County Juvenile Hall. Hartman School provides a 
highly structured learning environment that supports individualized and group learning. Curriculum is a blend of traditional text based 
learning as well as hands on kinesthetic learning. Named after a dedicated teacher, the school is open year round and employs a staff 
with extensive experience and training related to delinquent youth. 
 
Esquela Quetzal 
 
Escuela Quetzal is a school counseling program designed for youth who are willing to actively participate in counseling services to learn 
how to make empowered decisions toward positive personal growth. The program is made possible through collaboration between Youth 
Services and the Santa Cruz County Office of Education. Each day students attend core academic classes and receive group and individual 
counseling. Family and community involvement are also important components of the program. Along with academics, students are 
offered courses in art, music,vocational education, sports, an organic garden, and computer training. 
 
Freedom Community School 
 
Freedom Community is a single classroom site for students who excel in a smaller, supportive, and structured learning environment. Our 
vision is to empower intrinsic motivation to lead and educate family and peers.The teacher and staff meet each individual learner at their 
current academic and social levels in order to create attainable goals, which creates a positive learning experience. 
 
Sequoia Academy 
 
A self-contained classroom designed to assist students who need extra support with study skills and setting educational goals. Sequoia 
Academy is committed to Social, Emotional, Academic learning with an opportunity for vocational training and community involvement. 
 
Corralitos Community 
 
Corralitos Community supports students with their rehabilitation and credit recovery. Provides educational and empowering environment 
for high school students by supporting their achievements in personal growth, academics, and career-oriented goals.