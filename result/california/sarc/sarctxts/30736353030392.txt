Trabuco Hills High School (THHS) is a dynamic school. Trabuco’s mission statement challenges the 
school community to create an energetic and diverse educational environment for its students. 
THHS has readily accepted this challenge and has made a steady ascent to the peak of educational 
excellence. Since its doors opened in 1985, the school has grown steadily from 600 students to a 
current enrollment of approximately 2,850. Fortified by a string of statewide and national honors, 
the school has produced a long list of illustrious graduates, including educators, professional 
athletes, actors, lawyers, doctors, scientists, and independent business owners. The greatest 
strength of Trabuco Hills High School is the range of educational opportunities offered to our 
students. While offering the greatest number of Advanced Placement classes in the District, we 
also have the greatest number of ROP (Regional Occupation Program) and CTE (California Technical 
Education) classes as well. THHS effectively serves students moving on to highest level of 
universities as well as those being prepared to go directly to the work force with skills that prepare 
them for success. Bolstered by a highly qualified and accessible faculty and staff, THHS has built its 
reputation as a school with a broad range of educational experiences that help students fully 
explore their potential. Our vision is: We encourage, inspire, support, and empower our students 
to critically think about their place in the modern world and to give them the knowledge, skills, and 
competencies for their chosen vocation, to be well-balanced young adults who are prepared to 
positively impact the world and love learning as much as we do! 
 
Craig Collins, PRINCIPAL 
 
Trabuco Hills Mission Statement: 
By creating a dynamic and diverse educational environment, the Trabuco Hills High School 
community will provide its students with learning experiences that provide the Academic Base, Self-
Discipline and Social Skills to become life-long learners and productive citizens in our ever-changing 
world. 
 
 

----

-

--- 

Saddleback Valley Unified School 

District 

25631 Peter A. Hartman Way 

Mission Viejo CA, 92691 

(949) 586-1234 
www.svusd.org 

 

District Governing Board 

Suzie Swartz, President 

Dr. Edward Wong, Vice President 

Amanda Morrell, Clerk 

Barbara Schulman, Member 

Greg Kunath, Member 

 

District Administration 

Crystal Turner, Ed D. 

Superintendent 

Dr. Terry Stanfill 

Assistant Superintendent, Human 

Resources 

Connie Cavanaugh 

Assistant Superintendent, Business 

Laura Ott 

Assistant Superintendent, 

Educational Services 

Mark Perez 

Director, Communications and 

Administrative Services 

Dr. Ron Pirayoff 

Director, Secondary Education 

Liza Zielasko 

Director, Elementary Education 

Dr. Diane Clark 

Director, Special Education 

Rae Lynn Nelson 
Director, SELPA 

Dr. Francis Dizon 

Director, Student Services 

 

2017-18 School Accountability Report Card for Trabuco Hills High School 

Page 1 of 14 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Trabuco Hills High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

112 

112 

118 

0 

0 

0 

0 

0 

0 

Saddleback Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1,157 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Trabuco Hills High School 

16-17 

17-18 

18-19