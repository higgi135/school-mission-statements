Cottonwood Creek School was established in August 2018 to serve Kindergarten though Eighth 
grade general education and special education students. The school is located in the eastern side 
of Dublin in a growing housing community. The physical land encompasses 10 acres and contains 3 
bio-retention ponds to capture rain water into a retention tank incorporated into the design of the 
campus. Some of the many features of the campus include: a library with over 10,000 titles; two 
Flex rooms to house special events for students; a natural grass field surrounded by a two lane 
track; a music building with multiple practice rooms and instrument storage lockers; performance 
ability stage which can be accessed via indoors or outdoors for productions; garden boxes for 
student's to design and plant various crops; and technology available in every classroom for student 
daily use. Our student population is diverse and includes more than 100 English Language Learners 
speaking 26 different languages. As a faculty, the implementation of an International Baccalaureate 
program is being pursued for the academic benefits of our students in Kindergarten through the 
eighth grade. 
 
The Cottonwood Creek Vision and Mission statements will be created during the 2018-19 school 
year with input from the staff, parents, and students. 
 
 

-------- 

 

 

-------- 

Dublin Unified School District 

7471 Larkdale Avenue 

Dublin, CA 94568 

925-828-9551 

http://dublinusd.org 

 

District Governing Board 

Amy Miller 

Dan Cherrier 

Dan Cunningham 

Niranjana Natarajan 

Megan Rouse 

 

District Administration 

Dr. Leslie Boozer 
Superintendent 

Dr. Joe Sorrera 

Assistant Superintendent 

Business Services 

 

Leslie Anderson, Christine Williams 

Co-Acting Assistant 

Superintendents 

Educational Services 

 

Mark McCoy 

Assistant Superintendent 

Human Resources 

 

 

2017-18 School Accountability Report Card for Cottonwood Creek School 

Page 1 of 7