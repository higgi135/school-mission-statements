MISSION STATEMENT 
The mission of Weathersfield Elementary School is to provide innovative academic and co-curricular programs that provide students 
a lifelong love of learning and the opportunities to develop their interests, talents and abilities. In addition, we provide a variety of 
fine and performing arts and activities programs designed to enhance our students' academic experience and to develop the "whole 
child". 
 
SCHOOL DESCRIPTION 
Weathersfield Elementary is a 2014 California Distinguished School located in the northeast corner of the Conejo Valley. The school 
currently serves 318 students from transitional kindergarten through fifth grade. Weathersfield Elementary School is staffed with a 
dedicated group of professionals who are committed to providing a rich learning experience for our students. We expect students to 
maintain high standards of citizenship and scholarship. Our school has a strong academic focus which is enhanced by technology in 
every classroom including Promethean boards, hand held devices, and access to two computer labs. In addition, we have a new science 
lab and two music rooms used for music instruction, band and strings. 
 
At Weathersfield, through our Character Program, we emphasize the importance of making good choices and decisions to our students 
which will ultimately lead to being a good citizen and good friend. We offer a variety of rewards programs, including; “Caught Being 
Good” slips, classroom motivational acknowledgement programs, Principal Awards, Student-of-the-Month assemblies which include 
Star Students and Teacher Choice Awards, and Student Council sponsored activities and events. Students in grades three through five 
can join our Community Outreach program which provides the children with opportunities to work and connect with a variety of 
community service projects, both within the Conejo Valley and beyond. Program offerings include chorus, band and strings. We also 
have the services of a school counselor once every week. Policies and procedural updates along with upcoming school activities are 
regularly communicated through the use of our weekly newsletter (Eagle Flyer), phone messages, e-blasts, text messages, school and 
PTA websites, PTA Facebook and Twitter (@GoWeathersfield).