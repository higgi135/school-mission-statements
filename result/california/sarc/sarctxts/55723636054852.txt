Chinese Camp School joined the Jamestown School District in July 2009. Every effort was made to keep this quaint, family-oriented 
school operational to serve the community’s children. In the 2015-16 the school curriculum was changed to focus on projected-based 
science and the use of the Red Hills Habitat to engage students in group and individual projects to discover and explore science, 
technology, engineering, and math in a collaborative project-based environment. The school is known for its rich science curriculum 
in a closely knit community. 
 
District Mission Statement 
Jamestown School District offers a healthy, safe, enriching, learning environment to help each child grow and achieve. 
 
Community & School Profile (School Year 2017-18) 
Jamestown School District was established in 1855. Chinese Camp School joined the District in July 2009 when the Jamestown and 
Chinese Camp School Districts merged. The school is considered a small, necessary school because of it small size and rural location. 
It is situated in the Red Hills Habitat. It provides high quality learning experiences focused on science in a multi-grade setting.