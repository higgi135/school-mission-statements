James L. Day Middle School (DMS) first opened in August of 1999. In 2007, Day Middle School earned California Distinguished School 
recognition. Our mission is to consistently ensure that all students learn and continually improve with the collaborative support of all 
stakeholders within the school community while empowering all students to realize their potential in a rapidly changing, diverse, 
global economy. We believe that the most promising strategy for achieving the mission of James. L. Day Middle School is to 
develop the capacity to lead our students through our professional learning community and to promote and follow our 
universal school-wide expectations (P.R.I.D.E.). 
 
 We envision a school in which students and staff: - Promote student responsibility and accountability in order to aid students towards 
independence - Have a self expectation of academic success - Are guided in gaining independent responsibility for their learning. - 
Reinforce behaviors daily that encourage students to come to school prepared, organized, and motivated to learn. - Create self-
directed, self-starting, self-correcting learners by modeling high expectations.