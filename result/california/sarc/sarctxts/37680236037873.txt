Our mission is to ensure that ALL Harborside students receive a rigorous, high quality instruction in 
a collaborative learning environment where there is an emphasis on academics as well as social/ 
emotional learning. We want to ensure that all students develop the confidence and necessary 
skills to be independent thinkers, effective communicators, and be socially conscious and 
contributing global citizens. 
 
Our Vision 
We believe that Harborside Elementary School provides a safe and rigorous learning environment 
where our children are encouraged to be critical thinkers, leaders, collaborators, culturally 
proficient, and biliterate. 
 
We believe that every adult and every student will act with compassion, treat one another with 
respect, and model positive behaviors on a daily basis. 
 
We believe that every child is capable of learning and adults and children will actively support the 
learning efforts of others. 
 
 
 
 

-------- 

 

 

----

---- 

Chula Vista Elementary School 

District 

84 East J Street 

Chula Vista, CA 91910-6100 

(619) 425-9600 
www.cvesd.org 

 

District Governing Board 

Leslie Ray Bunker 

Armando Farias 

Laurie K. Humphrey 

Eduardo Reyes, Ed.D. 

Francisco Tamayo 

 

District Administration 

Francisco Escobedo, Ed.D. 

Superintendent 

Jeffrey Thiel, Ed.D. 

Assistant Superintendent, Human 
Resources Services and Support 

Oscar Esquivel 

Deputy Superintendent, Business 

Services and Support 

Matthew Tessier, Ed.D. 

Assistant Superintendent, 

Innovation and Instruction Services 

and Support 

 

2017-18 School Accountability Report Card for Harborside Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Harborside Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

33 

0 

Teaching Outside Subject Area of Competence 

NA 

34 

30 

0 

 

0 

 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Harborside Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.