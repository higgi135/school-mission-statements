Welcome to Mintie White School! We are a school with close to 700 students on average with 93% Free and reduced lunch and 82% 
ELs. Our vision is to create an environment of learning where all students feel safe to take academic risks, learn to use mistakes as 
an opportunity for growth, and be able to articulate where they are in their learning. We have implemented Positive Discipline and 
are working on connecting with students in order to correct behaviors. This is a school-wide endeavor and focus. We continue 
implementing Systematic English Language Development (SELD) lessons on a daily basis to support our English learners and vocabulary 
development for our EO students. This format is the Designated or Leveled ELD focus. Teachers will begin to use an Integrated ELD 
approach in all academic areas very soon with the new ELD Standards now available. 
 
Many parents continue being involved in parenting classes, meetings, and celebrations. Parents continue attending information 
seminars with their children on their child's progress and set academic learning goals with them. Our School Site Council (SSC) and 
EnglishLanguage Advisory Committee (ELAC) help raise money to send our fifth-grade students to Outdoor School and are an 
important part of our decision making body. 
 
Overall, our efforts have been supported by the after-school coordinator to ensure students receive, on a daily basis,the intervention 
support needed as well enrichment experiences. For more information, please contact the principal Vicki Hallof at 831-728-6321. 
 
Our mission and vision statements comprise the following core beliefs: 
 
Our Motto: At Mintie White all students can and will learn. Every child is important. We care about each other. 
 
Our Creed: 
Great and Mighty Eagles believe you should: 
Care about your school 
Care about others 
Care about yourself 
Be respectful, responsible and safe. 
It's the right thing to do....always in ALL ways. 
 
Our Pledge: 
I am a great and mighty eagle and because I care about my school I will obey my teachers, I will be respectful, responsible and safe. I 
will help my school be at it's best! 
I am a great and mighty eagles and because I care about others I will be nice to everyone. I will help those who need me, and I will set 
a good example. 
I am a great and mighty eagle and because I care about myself I will do my best work in school. I will rise to meet expectations I will 
try again when I make a mistake, and I will be brave and take risks to learn. 
 

2017-18 School Accountability Report Card for Mintie White 

Page 2 of 11