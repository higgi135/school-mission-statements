Central Valley High School Mission Statement: 
 
Working Daily to Improve Ourselves, Our School, and Our World 
 
General Overview 
Central Valley High School is located in the city of Ceres in the central San Joaquin Valley, 80 miles south of Sacramento and 95 miles 
east of San Francisco, in the heart of Stanislaus County. Ceres is home to one of the Central Valley’s richest and most diverse 
agricultural areas. While the town has approximately 46,700 residents, Ceres is still considered a small town. Originally, Ceres housed 
one high school but has since added a second comprehensive high school. 
 
The second of two high schools, Central Valley High School, opened in August of 2005, and graduated its first class of students in 2008. 
In its first year, 2005-2006, CVHS served approximately 800 9th and 10th grade students. This current school year (2017-2018) CVHS 
started with a student enrollment near 1,972. Once fully developed, CVHS will have the capacity to house approximately 2,500 
students. Our student body is comprised of 582 freshmen, 585 sophomores, 547 juniors and 455 seniors. The ethnic makeup of our 
student population is 82% Hispanic, 9% White, with smaller percentages of approximately 5% Asian, 2% African American, .5% 
American Indian, and 2% other ethnicities. Additionally, 86% of students are considered socioeconomically disadvantaged as 
determined by those who qualified for free or reduced meals, 9% are English Learners and 98% are Special Education. The entire 
student population is offered free breakfast daily. The many club offerings allow students the opportunity to be involved in school. 
There are thirty-two (32) clubs that students can join. 
 
Budgetary priorities have been established and the district, with state and community support, continue to provide the funds for CVHS 
to support and maintain a comprehensive program to meet the needs of all learners. Supplemental funds are provided by other 
sources, including Title 1, Carl Perkins Vocational and Applied Technology, EIA for English Learner Programs, and parent/community 
support groups. 
 
CVHS boasts a staff comprised of certificated staff members (teachers), 1 principal, 1 associate principal, 2 assistant principals, 5 
learning directors, 1 administrator in charge of activities, 1 administrator in charge of athletics, 1 school psychologist, 1 school nurse, 
and 1 school resource officer. CVHS also has classified staff members, which include instructional paraprofessionals, campus 
supervisors, clerical workers, food service, cafeteria employees, custodians, library/media clerks, and a groundskeeper. The teacher 
to student ratio is approximately 25:1. 
 
Central Valley High School is an exceptional place for students from diverse backgrounds to grow educationally, get involved, and have 
a meaningful high school experience. We pride ourselves on the rigor of our course offerings, preparing every student for college as 
we focus our instruction and supporting academic excellence. In every way that schools are measured, Central Valley High School 
excels. 
 
Central Valley High School is committed to providing students with opportunities to take the most rigorous courses. There are many 
different interventions that are in place to support students in being successful. With a large population of English language learners, 
the school provides support for the 280 students who are English language learners. The school prides itself on the sixteen (15) AP 
Advanced Placement courses that meet entrance requirements for the University California. The school also offers nine (8) Pre-
AP/Honors/Accelerated courses. One of the goals of Central Valley High School is prepare our students to be competitive in the process 
of college applications. 
 
CVHS went through an accreditation by the Western Association of Schools and Colleges in the spring of 2015 and was granted a 6-
year accreditation which extends through June 30, 2021. CVHS is scheduled for a Mid Term review in 2018. 
 

2017-18 School Accountability Report Card for Central Valley High School 

Page 2 of 15 

 

Central Valley High School uses site-based teams, with the goal of increasing communication and collaboration among all stakeholders. 
Teams comprised of teachers, administrators, classified staff, parents and students address specific areas of concern to the school 
community. Feedback is given to the Steering Committee and CARE Team (each comprised of a lead teacher from each department), 
the School Site Council and the English Learner Advisory Committee (ELAC), Family Engagement Committee, and Parents for a Better 
Future which then provide input and report back to the school staff. 
 
We strive to build relationships between students and our staff through relevant curriculum that is rigorous, course offerings, and co- 
and extracurricular opportunities. We continuously reflect on creating an environment where each and every student is given the 
attention necessary to achieve his/her individual success. Support is offered to struggling students through services provided by 
Center for Human Services, 9th grade mentoring program, school tutoring both before and after school, tutoring offered within the 
school day via the Learning Center, and a partnership with Youth for Christ. We are proud to have a staff that is dedicated to the 
success of every student.