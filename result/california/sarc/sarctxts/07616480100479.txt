Our Vision for Dallas Ranch: 
Dallas Ranch Middle School will be a school of excellence; strongly connected in the community where students achieve their full 
potential academically. We will focus on students’ positive character development so students are prepared for success in high school, 
college, and careers. We want students to make positive contributions in the community. 
 
Dallas Ranch Middle School Mission: 
Our responsibility is to prepare every student for success in high school and beyond. 
 
At Dallas Ranch Middle School: 
We believe that ALL members of the school community: students, parents, teachers, administrators, and support staff must model 
good character by displaying trustworthiness, respectfulness, responsibility, fairness, caring, and positive citizenship. 
 
We are a place that values diversity, where students are active participants and feel a sense of belonging and pride. Aviators learn to 
be responsible for the success for themselves and others. 
 
Students understand the importance of learning and have a desire to achieve to the best of their ability. They receive support in their 
academic endeavors and learn to be critical thinkers. 
 
Aviators are well-rounded individuals with opportunities to develop their bodies and minds through sports, clubs, electives, and other 
extra-curricular activities.