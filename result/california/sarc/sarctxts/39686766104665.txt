Vision: George Washington School is committed to Students' college and career successes by 
implementing Common Core Standards and AVID strategies with a focus on collaboration and 
organization. 
 
Mission: George Washington School creates a challenging learning environment that promotes 
high standards using Common Core instruction. Lifelong learning skills are fostered through 
technology and research-based instructional strategies including the AVID College Readiness 
System. George Washington School encourages sportsmanship, school spirit, and pride through 
daily goals and educational accomplishments. We urge parents and community members to 
actively participate in our students' learning and success. 
 
 

 

 

----

---- 

----

---- 

Stockton Unified School District 

701 North Madison St. 

Stockton, CA 95202-1634 

(209) 933-7000 

www.stocktonusd.net 

 

District Governing Board 

Cecilia Mendez 

AngelAnn Flores 

Kathleen Garcia 

Lange Luntao 

Maria Mendez 

Scot McBrian 

Candelaria Vargas 

 

District Administration 

John E. Deasy, Ph.D. 

Superintendent 

Dr. Reyes Gauna 

Assistant Superintendent of 
Educational Support Services 

Craig Wells 

Assistant Superintendent of 

Human Resources 

Sonjhia Lowery 

Assistant Superintendent of 

Educational Services 

 

2017-18 School Accountability Report Card for George Washington Elementary 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

George Washington Elementary 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

10 

11 

13 

1 

1 

0 

0 

0 

0 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

George Washington Elementary 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.