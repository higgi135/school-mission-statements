Rancho is located in the city of Hollister, California, which sits in northern San Benito County. 
Hollister is 47 miles from the San Jose metropolitan area, 39 miles east of the Monterey Peninsula, 
and 90 miles south of San Francisco. The region still maintains the agricultural and cattle ranching 
ambiance, which is its heritage. From the Gabilan Mountains in the west to the Diablo range in the 
east, San Benito County displays a wide range of California's geography of native oak and grassland 
ranges. 
 
Rancho San Justo Middle School, serving students in grades 6 - 8, is an AVID school community 
dedicated to ensuring opportunities for academic success for all students by providing a rigorous 
and comprehensive educational experience in our rapidly changing world. Rancho is a professional 
learning community (PLC) and utilizes a data driven improvement model by working collaboratively 
to identify student academic proficiency, as well as areas for growth. 
 
Student mastery of California State Standards is measured by district and school-wide benchmark 
assessments and PLC common assessments. California State Standards form the basis of our 
curriculum and department curriculum maps direct the curriculum pacing within our classrooms. 
We are continually improving a systematic collaboration practice among the staff to review 
academic growth and academic needs. Our staff participates in site and district committees and 
councils, such as our site-based Instructional Leadership Team, AVID, Positive Behavior 
Interventions & Supports (PBIS) and Capturing Kids' Hearts ((CKH), Technology, and Safety 
Committee. 
 
The mission of Rancho Middle School is to provide a rigorous academic and elective program in a 
safe and supportive environment to ensure the total development of each child: intellectually, 
socially, morally, emotionally, and physically. The school will advocate and empower students to 
think critically, communicate effectively, respect cultural diversity, be inclusive, and to work 
collaboratively and creatively to reach their full potential for a productive and meaningful life. 
 
The Rancho community is unified in its dedication to giving every student every opportunity for 
success, now and in the future. (Opportunity - Community - Unity) 
 
RSJMS is an AVID school committed to success for all students by preparing them for college and a 
variety of careers in a rigorous academic environment focused on collaborative and engaging 
instruction. 
 
Rancho's curriculum is designed to meet the various interests and needs of the students by 
preparing them for the 21st Century in today’s global society by offering a systematic program that 
includes Common Core State Standards Math and Language Arts. Intervention Labs per grade level 
utilize i-Ready as a diagnostic and prescriptive tool to supplement teacher-led instruction geared 
for student success. Elective classes offer a variety of opportunities for student enrichment, 
language development, and student engagement. 
 
Instruction: available for special needs through Special Education and English Language 
Development 
 
Co-curricular Programs: Woodshop, Art, Yearbook, Band, Leadership, Spanish, and Athletics 
 
Co-Curricular Activities: Students are encouraged to join clubs, sports teams, and campus 
organizations to promote leadership and service. 

2017-18 School Accountability Report Card for Rancho San Justo School Middle School 

Page 1 of 8