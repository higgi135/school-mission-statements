Del Rosa Elementary School has 23 classrooms, a library, a multiuse room, and an administration office. The campus was built in 1948, 
modernized in 1990 and 2012, and beautified in 2017. The facility strongly supports teaching and learning through its ample 
classrooms, playground space, a staff professional development room, and a parent center. 
 
Mission Statement: At Del Rosa our name is our mission: D for Determination, We persevere in all we do! E is for Exploration,We 
research, we study, we investigate! L is for Learning , we’re learning EVERYDAY! R for respect, it’s the right thing to do! O for open 
minded, We consider new ideas! S for success,We accomplish our goals! A is for accountability, We’re responsible for everything we 
say and do! At Del Rosa we are creating scholars one interaction at a time.