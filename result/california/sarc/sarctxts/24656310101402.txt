Atwater Senior Academy is an alternative educational setting that provides an instructional setting for students who have not been 
successful in a traditional school setting. Atwater Senior Academy helps students to achieve success in their regular school by teaching 
them study skills, organizational skills, and interpersonal skills, which ultimately helps them to make better life choices in their daily 
lives and find success in the regular classroom.