Our Mission is "Vicentia is a community of collaborative scholars and compassionate leaders who are preparing for a successful future 
and will contribute positively to society." 
Our Vision is "As we teach and model academic and social-emotional expectations, we create a positive, safe, and respectful school 
culture where students proudly take ownership of their learning." 
 
Year after year, the Vicentia staff continues to create an atmosphere of high expectations with a proactive, positive discipline system. 
We incorporate life-long character traits into the daily operation of our announcements, "family" meetings, classrooms and school 
activities. We are in our third year of implementing PBIS and our students and staff have really embraced our motto, "We are Viking 
S.T.R.O.N.G" - Scholar, Trustworthy, Respect, Organized, No Excuses, and Growth Mindset. Our motto is incorporated into our daily 
discussions with students, parents, and staff. 
 
The Vicentia staff gives our students Viking Coins when they are "caught" exemplifying the motto. We have switched from Viking 
Vouchers to Viking Coins to go along with our pirate theme this year. Students collect Viking Coins and use them in our PBIS school 
store to "buy" things. All positive and negative conversations with students, parents, and staff revolve around these shared values. It 
is, and will continue to be, a school-wide common language that gives our students the understanding of expectations that will guide 
them to success. 
 
 
Vicentia Elementary School is a school of approximately 630 students, located in the southern region of Corona. We follow a modified 
traditional calendar. Within our population, we have three significant subgroups that include Hispanic, Socio-Economically 
Disadvantaged, and English Learners. 
 
For the 2018-19 school year, we have 23 general education classrooms from Transitional Kindergarten to 6th grade. We have one 
Transitional Kindergarten class, three Kindergarten classes, three first grade classes, four second grade classes, and three each of third 
through sixth grades. We also have a 1st-3rd grade Special Day class and a 4th-6th grade Special Day class on our diverse campus. To 
assist our students, Vicentia has a Resource teacher, a Speech and Language Pathologist, and a part-time school psychologist. In 
addition, we have two part time counselors and STEPS instructional aide. 
 
As a Professional Learning Community, Vicentia Elementary strives to increase student achievement by 1) providing teachers with the 
largest amount of uninterrupted teaching time as possible, 2) providing students with daily intervention through small group 
instruction to address the needs of those students below grade level 3) providing teachers with common grade level collaboration 
time during the instructional day to analyze common assessments, review data, discuss instructional strategies, plan interventions, 
and discuss future instruction of the grade level, and 4) providing teachers with common English Language Development (ELD) time 
to address the various needs of our English Learners through the use of the ELA/ELD framework using our new Benchmark language 
arts series. We have extended the learning day for approximately 120 students through the Expanded Learning grant which provides 
academic support, homework support, and enrichment time. 
 
Expanded Learning is Vicentia’s after school program that operates daily for the entire school year from the end of the regular school 
day to 6:00pm. Through the Expanded Learning intervention program, we target students who are not meeting grade level standards 
or barely meeting grade level standards in English Language Arts and/or Math on grade level assessments, district assessments, and/or 
SBAC. Our Expanded Learning program consists of three parts: dedicated reading or read aloud time, homework time, and 
enrichment. Enrichment can include technology, art, STEM/STEAM activities, standards-based PE lessons, music, and theater activities 
to address the needs of the whole child and to provide a balanced program. Our TSA also works closely with the aides and Lead to 
help them provide opportunities for reading instruction, read alouds, and effective independent reading time. The goal is to get our 
students enthusiastic about their own education and let them see Vicentia as a place where they can enjoy learning while experiencing 
new adventures. 

2017-18 School Accountability Report Card for Vicentia Elementary School 

Page 2 of 14 

 

 
At Vicentia, we also use supplemental programs to support classroom instruction and improve skills throughout the day, and even 
into the Expanded Learning program. Our students have started using the new iReady software in both ELA and Math to increase 
skills in certain standards. The students take a diagnostic twice a year to find where their current levels are and are assigned lessons 
based on their skill level. We have seen much growth using this program. Reading Counts is another supplemental motivational 
reading program which is instrumental in increasing student achievement in reading. All students and teachers have embraced our 
Reading Counts program. Students choose books at their independent Lexile reading level and take quizzes that assess their 
comprehension of the text. The Lexile levels are also used to help determine interventions for students who struggle. At the end of 
the school year, we host our Reading Counts Celebration assembly to celebrate our students’ reading accomplishments. 
 
We are in our third year of implementing AVID Elementary. The AVID program is helping our 2nd-6th grade students become ready 
for higher education, including college and career. We have focused on three AVID goals this year; 1. Having an organized binder, 2. 
Using a daily planner, 3. organizational strategies. Our teachers explained the expectations of the binders and planners at Back to 
School Night, and parents have been willing participants in helping their student become college and career ready! All 2nd-6th grade 
teachers have been AVID Elementary trained, either at the Summer Institute in San Diego, or through a Pathways training. Our goal 
is to eventually have TK-1st grade students and teachers using AVID strategies in their classrooms as well. We are also in our third 
year of implementing PBIS schoolwide. We have an awesome PBIS team of 12 people (certificated, classified, support staff, and 
Expanded Learning staff) who have worked together to create our motto and determine what we stand for. The Viking S.T.R.O.N.G 
motto came from this work with our team and PBIS coach. We officially rolled out our motto at the start of the 17-18 school year and 
asked parents to become a willing participant in getting their students to have a positive out look on school. It's been a great process 
as we look for ways to positively impact students at school. Our work continues on! 
 
Last year, our district adopted a new Language Arts program called Benchmark. All of our teachers have attended multiple district 
trainings, as well as site based trainings. We have also trained our teachers on our supplemental computer program, iReady. We 
continue to train our teachers on how to be an effective PLC by attending trainings, as well as conferences. 
 
Vicentia has many fabulous classified staff members that work diligently to ensure that Vicentia provides the best possible 
environment for students to be safe and successful. They make sure that our campus is clean, well organized, safe, and inviting for 
our students. 
 
With continued support from Vicentia’s PTA and community partners such as Horace Mann, Circle City Kiwanis, and other outside 
donatations, Vicentia welcomes the successes and challenges of the school year.