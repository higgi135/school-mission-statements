The Professional Learning Community of Del Vista Math & Science Academy will provide high quality education for all pupils. 
 
Vision: 
At Del Vista Math & Science Academy, we will produce lifelong learners, critical thinkers, problem solvers, and citizens of positive 
character through a focused and caring learning environment with the ultimate goal of producing adults who will succeed in the global 
community. 
 
We are committed to… 
...ensuring that all students learn at their highest academic level. 
...providing engaging lessons that meet the rigor of Common Core. 
...preparing students for college and career. 
...maintaining positive, safe, and fair learning environments. 
...high expectations for ourselves and our students. 
…growing critical thinkers, problem solvers, and lifelong learners 
…supporting the academic and social behavioral focuses of DVMSA. 
 
At Del Vista Math & Science Academy, we are a mandatory uniform school. We are committed to developing the learning potential 
and academic achievement of each and every student. The staff works closely together to provide the best educational program 
possible to help achieve the following school-wide goals: expect all students to meet or exceed grade level standards aligned with 
Common Core; promote responsible citizenship; provide early intervention for students who have difficulty; and promote positive 
parent involvement. 
 
The students at Del Vista Math & Science Academy have many opportunities for extended learning. After school tutoring is offered 
for students in kindergarten through fifth grades. Migrant students attend the Migrant Extended Day classes which are held twice per 
week. The students in these classes receive extra help with homework and additional language instruction. Students participating in 
the GATE program also have after school sessions with the purpose of providing enrichment and culminating in a GATE festival where 
they showcase a project. The ASES POWER program is also available for student in grades 3-5.