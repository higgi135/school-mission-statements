Description 
Veterans Elementary opened in 2005, serving a K-6 population. The school is located in southeast 
of San Diego County in the city of Chula Vista. It is located in an area of new homes and apartments. 
Veterans is about five miles from the U.S./Mexico border. Veterans was the 43rd elementary school 
to open in the Chula Vista Elementary School District. The school has four permanent classroom 
pods, a library media center, an auditorium, and an administrative building. 
 
We have a very diverse student population. Approximately 23% of the students qualify for the free 
and reduced National School Lunch Program. Approximately 24% of the students are English 
Learners, 12% qualify for GATE, 40% are Filipino, and 40% are Hispanic. There are 34 general 
education teachers, 3 special education teachers. We have a full-time Associate Principal, a full-
time Resource Teacher, two Speech Teachers, 2.5 days/week with a nurse on site, and 4 days/week 
with a psychologist on site. 
 
Veterans' mission is to provide all children with a world class education. 
 
Veterans Elementary School provides a safe, nurturing, supportive learning environment for every 
member of the school community. We foster the continuous academic and social growth of all 
students by engaging them in challenging, enriching learning experiences that meet their individual 
needs and prepare them for life in the 21st century. This is accomplished by immersing students in 
a Common Cores Standards curriculum, which demonstrates relevance to their lives, provides in-
depth experiences in all curricular areas and provides a strong foundation for future learning. The 
building blocks of the academic program are commitment, competency, communication, and 
collaboration. Staff members take responsibility for basing instruction on best practices by 
engaging in ongoing professional development through research, reading, and collaboration. 
Teachers are adept at using ongoing assessments and data to guide instruction. Emphasis is placed 
on adherence to grade level content standards, ongoing assessment, differentiation, and grade 
level collaboration. Techniques, strategies, and results are shared with parents on an ongoing basis, 
thereby giving parents the opportunity to be true partners in the academic growth of their children. 
 
 

----

---- 

Chula Vista Elementary School 

District 

84 East J Street 

Chula Vista, CA 91910-6100 

(619) 425-9600 
www.cvesd.org 

 

District Governing Board 

Leslie Ray Bunker 

Armando Farias 

Laurie K. Humphrey 

Eduardo Reyes, Ed.D. 

Francisco Tamayo 

 

District Administration 

Francisco Escobedo, Ed.D. 

Superintendent 

Jeffrey Thiel, Ed.D. 

Assistant Superintendent, Human 
Resources Services and Support 

Oscar Esquivel 

Deputy Superintendent, Business 

Services and Support 

Matthew Tessier, Ed.D. 

Assistant Superintendent, 

Innovation and Instruction Services 

and Support 

 

2017-18 School Accountability Report Card for Veterans Elementary School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Veterans Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

38 

0 

Teaching Outside Subject Area of Competence 

NA 

38 

38 

0 

 

0 

0 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Veterans Elementary School 

16-17 

17-18 

18-19