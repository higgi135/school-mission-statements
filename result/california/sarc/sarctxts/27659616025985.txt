Bardin Elementary School, home of the "Chieftains" is located at 425 Bardin Road in the city of 
Salinas. Our school serves approximately 640 students in grades transitional kindergarten through 
sixth grade. The One Way Developmental Bilingual program exists at grades kinder through sixth. 
We have one special education day class serving General Academics, which support children in 
grades fourth through sixth. We at Bardin Elementary believe that ALL students are capable 
learners who have the right to a high-quality education, provided by qualified, passionate educators 
who embrace diversity within both the presentation of educational curriculum and the students 
themselves. Here at Bardin, we promote and build connections that integrate students, families, 
district and local/global communities. 
 
Much of the work completed at Bardin has been developed through our partnership with the 
Monterey County Office of Education via our Professional Learning Network (PLN). Our leadership 
team works together to identify areas of weakness in our practice and highlight the best practices 
that are positively influencing our students outcomes. Through this work we have developed our 
Problem of Practice and school wide implement classroom read alouds that are strategically geared 
to exposing our students to texts, while addressing their comprehension and writing levels. 
 
The instructional program is comprehensive in all curricular areas based on common core state 
standards and district guidelines. Teachers meet on a regular basis to align program content, 
pacing, and student progress through the Grade Level Team (GLT) release model. Our instructional 
program is supported by Accelerated Reader, LexiaCore5, Lexia PowerUP, and our Universal Access 
Intervention model which provides all students with 30 minutes of targeted instruction. In addition, 
children in grades kindergarten through sixth grade participate in the 1:1 technology program. 
Instructional practices include multiple opportunities for technology-based learning projects, in 
Kinder to second grade students utilize SeeSaw as their digital portfolio, while students in third 
through sixth utilize Google Classroom. The goal is to provide children with the opportunity to learn 
using the latest educational applications, which support the curriculum. 
 
This is the ninth complete year of our after school program. The program offers our students 
academic and enrichment activities, including structured tutoring and homework assistance, 
reading/language arts, English language development, and math. The enrichment activities include 
music; drumline and Ukulele lessons, and gardening. The Reading program includes Lexia, Reads 
Naturally, and Reading A-Z, depending on the child's needs. Math programs include Triumphs, and 
Rewards. The school offers extracurricular activities such as soccer, basketball, volleyball, and flag 
football. 
 
Bardin Elementary school has a strong partnership of staff, students, parent, and community 
through monthly meetings. The encouragement and participation in school activities supports the 
idea that every child can learn and every child will. 
 
Our Mission Statement: 
We at Bardin Elementary believe that ALL students are capable learners who have the right to a 
high quality education, provided by qualified, passionate educators who embrace diversity within 
both the presentation of educational curriculum and the students themselves. Here at Bardin, we 
promote and build connections that integrate students, families, district and local/global 
communities. 
 

2017-18 School Accountability Report Card for Bardin Elementary 

Page 1 of 7 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Bardin Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

31 

27 

20 

7 

0 

5 

0 

6 

0 

Alisal Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

9 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Bardin Elementary 

16-17 

17-18 

18-19