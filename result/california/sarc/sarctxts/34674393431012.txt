Luther Burbank High School is a comprehensive four-year high school located in South Sacramento. 
The school is dedicated to preparing our diverse student population for the challenges of college 
and career. In order to fulfill that promise the school provides programs that range from English 
Language Development to International Baccalaureate Diploma. Our Latino, African American, 
Pacific Islanders,South, South East and South West Asian students, among others, all share in the 
rich fabric of diversity the school culture provides. 
 
In an effort to create an educational environment based on relationships and individual learning, 
the school has been organized into Small Learning Communities (SLC). Rather than a single large 
school of approximately 1700, we are a campus of six small learning communities, each centered 
around an academic pathway theme, where students are placed into groups of approximately 300 
students and share the same “core” teachers, as well as a geographical area of the school site itself. 
The smaller learning communities allow for a more personalized instructional experience. In 
addition to more personalized instruction, the themes of the small learning communities allow the 
campus to better connect to the community at large, making instruction more relevant to students 
as they see, and experience, how curriculum translates to the world beyond our walls. The themes 
of our small learning communities are: Food and Health Sciences, Law and Social Justice, Media 
Arts and Technology, Building Trades Academy, World Cultures and Global Studies. 
 
For more information about the school, please visit our website at: www.Lutherburbankhs.com 
 

 

 

----

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Mary Harding Young 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for Luther Burbank High School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Luther Burbank High School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

65 

63 

85 

1 

1 

2 

0 

6 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Luther Burbank High School 

16-17 

17-18 

18-19