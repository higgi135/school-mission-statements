The mission of Valley Vista High School is to engage our students in their education while promoting mutual respect and providing 
ample opportunities for acquiring essential life skills. Our students participate in our Got My ACT Together program that documents 
their acquisition of 21st century skills that include personal accountability and achievement, effective communication and citizenship, 
use of technology and preparation for successful transitions throughout adult life as embedded in the curriculum. VvHS promotes 
opportunities that prepare all students for success as an individual, family and community member and global citizen. Our students 
will become productive self -directed achievers, effective communicators and collaborators, positive responsible contributors and 
creative strategic thinkers. 
 
Students who have had difficulties at a comprehensive high school have an opportunity at VvHS to experience success in an alternative 
school setting. Students learn in small, structured classes and earn credits based upon the successful completion of course work taught 
by a credentialed teacher. Final grades and variable credits of a minimum of one to a maximum of five per course are earned each 
quarter. Students have an opportunity to earn 100 credits per year. VvHS offers a program for pregnant and parenting teens, A fully 
equipped child development center called the NEST provides on-site child care for the infants and toddlers of our students. At the 
conclusion of four years of high school, students who have not met the graduation requirements are encouraged to transition to adult 
education to earn a high school diploma or prepare for the GED, enroll in community college or vocational training, or join the armed 
services and/or enter the workforce. 
 
In elective courses, we offer acting, animation and game design, current events, digital photography, guitar and music production, 
consumer and family education, foods, physical conditioning and weightlifting, wood shop, art, career exploration, and leadership. In 
addition, ROP classes offered at various sites are available after school.