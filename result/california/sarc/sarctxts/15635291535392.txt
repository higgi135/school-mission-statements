South High School is one of 18 comprehensive high schools in the Kern High School District (KHSD), 
the largest high school district by geographic area in California. The district encompasses most of 
the western part of Kern County located at the southern end of California’s Central Valley. 
 
South High School was established in 1957 surrounded by the farmland that was South Bakersfield. 
As the city and community have evolved around the campus, so too has the school. Today, "We", 
continue to offer the best of academic and extracurricular programs to enhance our students' 
school experience. South is a neighborhood school with three substantial feeder schools 
representing two different elementary districts. Many students transfer to South to participate in 
programs that include: the Criminal Justice Academy (CJA), Engineering and Industrial Technology 
Academy (MS3), or the Army Junior Reserve Officer Training Corp (JROTC). 
 
Approximately 48 students ride the bus to school. The effects of the District’s boundary changes 
over the past 18 years has resulted in the opening of several new high schools and a fluctuating 
population. As a result, South has a homogeneous population with respect to socioeconomic status 
(SES) levels; however, the student population is diverse in regard to religion, ethnicity, and country 
of origin. Essentially, South serves a neighborhood with low-income levels and all students, except 
46, walk to school from the surrounding homes. 
 
South’s 2017-2018 student ethnicity distribution figures were 82.6% Hispanic, 8.5% African- 
American, 4.2% White, and 4.7% of students were Asian, American Indian, Filipino, declined to state 
or were another ethnicity. 
 
Located in south central Bakersfield, South serves this multicultural population whose economic 
conditions range from middle-class to homeless families who come from a variety of 
neighborhoods within a mile radius of the school. In addition to English, families at South speak 
Spanish and a variety of additional languages in the home. Accordingly, honoring diversity and 
promoting appreciation and understanding of different cultures is a high priority. The 2017-2018 
English Learner (EL) population represents approximately 10.4% of South's enrollment. Also, during 
the 2017-2018 school year over 93.1% of South High students qualified for free or reduced lunch. 
 
South High School has a rich history of generational attendees who remain loyal to the school. They 
continue to provide support for the school's programs. South's alumni and parents provide financial 
assistance to programs such as the band, athletics, and drama. In addition, the South High Athletic 
and Band Boosters also donate their time to chaperone activities at the school. South’s parent 
groups include Athletic Boosters, Band Boosters, and MS3 Parent Club. 
 

----

---- 

Kern High School District 

5801 Sundale Ave. 

Bakersfield, CA 93309-2924 

(661) 827-3100 

www.kernhigh.org 

 

District Governing Board 

J. Bryan Batey, President 

Joey O' Connell, Vice President 

Jeff Flores, Clerk 

Cynthia Brakeman, Clerk Pro Tem 

Janice Graves, Member 

 

District Administration 

Bryon Schaefer, Ed.D. 

Superintendent 

Scott Cole, Ed.D. 

Deputy Superintendent, Business 

Michael Zulfa, Ed.D. 

Associate Superintendent, Human 

Resources 

Brenda Lewis, Ed.D. 

Associate Superintendent, 

Instruction 

Dean McGee, Ed.D. 

Associate Superintendent, 
Educational Services and 

Innovative Programs 

 

2017-18 School Accountability Report Card for South High School 

Page 1 of 16 

 

South High has a variety of business partnerships with organizations including the Bakersfield Lions Club, Aera Energy LLC, the Bakersfield 
Police Department, the Kern County Sheriff’s Department, Kern County Probation Department, California Correctional Officers, Pacific 
Gas and Electric, Kennedy-Jenks Consultants, Grimmway Farms, the Bakersfield South Rotary, and South Bakersfield Kiwanis. The local 
Food Bank, Gleaners, Clinica Sierra Vista and the South High community are regular supporters of the school in a myriad of ways. Each of 
these organizations support various school activities such as job shadowing, intern and extern-ships, providing glasses to needy students, 
service learning opportunities, and student scholarships. Many students also participate in the local Bakersfield Leadership program with 
business leaders throughout the community. Additionally, personnel from local law enforcement agencies, state corrections, the military, 
college and university admissions, and the medical industry, have participated in campus job fairs as classroom speakers and as career 
counseling consultants. 
 
Currently the University of California Early Academic Outreach Program ((UC)(EAOP)) office provides a strong partnership on campus. The 
purpose of this partnership is to assist more students in going to college after graduation. Funded through a partnership with the UC 
system, a full-time, on-site representative assists students with applications for scholarships, financial aid, vocational colleges, community 
colleges, CSU schools, and private universities. Due to budget cuts this program is no longer paid solely from the University of California; 
South High now assists with paying for this person to be on campus five days a week. There has been an increased number of students 
apply and be accepted to the University of California system in the past three (3) years. 
 
The process of aligning performance standards to curriculum, instruction and materials began in 1999 and has evolved to current 
practices. To meet the requirements of English Language Arts and Mathematics standards, release time is provided for South High 
teachers to develop curriculum maps and to create common formative (CFA) and common summative assessments (CSA). 
 
The school has implemented English Literacy classes for students who scored far below grade level on STAR Renaissance English test. 
Foundations 1 and Foundations 2 classes have been implemented for students who scored below grade level on STAR Renaissance Math 
tests. These classes provide remediation for students who lack the skills to succeed in English Language Arts and/or Algebra. All 
departments have revised their curriculum maps to align them to the new California Common Core State Standards or NGSS. As a result 
of these efforts, curriculum maps have been developed to provide an appropriate instructional timeline. The school-wide implementation 
of Professional Learning Communities (PLCs) allows teachers to make informed decisions using current student data. The school has 
banked minutes for PLC meetings three times a month. During these meetings PLC groups collaborate to modify curriculum and share 
successful re-teaching strategies. Teachers have been and will continue to be provided ongoing staff development in the effective use of 
data to modify instruction and provide differentiated instruction to increase student learning. 
 
PLCs also help facilitate the identification of achievement gaps within the curriculum and among students. From analysis of resulting data, 
curriculum adjustments are made, including changes to instructional delivery, which may result in the need for additional supplementary 
materials. Those materials are funded by the principal’s budget, Title I, and Lottery monies. Throughout the year, the Assistant Principal 
of Instruction, department chairs and the librarian conduct an annual review of adopted instructional materials and order as needed. 
 
South High offers students on average 9 Advanced Placement (AP) courses and 7 honors courses. The majority of all core general education 
classes are designated as college preparatory (CP) (UC/CSU “A-G” approved), as are the majority of the elective courses offered at our 
school. South High offers 6 different CTE sectors, with 20 individual CTE Career Pathways aligned with these offerings. 
 
AP Courses 
• 
• 
• 
• 
• 
• 
• 
• 
• 
 
Honors Courses 
Pre-calculus 
• 
• Accelerated Math 
• Advanced Algebra 
• Geology 
• 
• 
• 

 Statistics 
 Calculus 
 English Language and Composition 
 English Literature 
 European History 
 US History 
 Government 
 Spanish Language 
 Spanish Literature 

Chemistry 
Physics 
Spanish 3 

Performing Arts 

CTE Sectors 
• Video Production 
• 
• Business and Information Technology 
• Architectural Design 
• MS3 (Engineering) 
• 

Criminal Justice Academy (CJA) 

2017-18 School Accountability Report Card for South High School 

Page 2 of 16 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

South High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Kern High School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

84 

102 

84 

1 

0 

5 

0 

6 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1700 

216 

12 

Teacher Misassignments and Vacant Teacher Positions at this School 

South High School 

16-17 

17-18 

18-19