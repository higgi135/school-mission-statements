Message From Principal 
Welcome to the 2018-2019 school year! This is our sixth year serving students in the Brentwood 
Union School District. This year, our staff is working on four main goals: Having a deeper 
understanding of the state standards and the impact on instruction, refining our intervention 
systems, integrating more technology in all areas of instruction as well as bolstering our Positive 
Action character program. There is still much to accomplish at this new school. 
 
To date we now have close to 722 TK through Fifth Grade students. We are proud of the fact that 
we have increased access to chrome books and other forms of technology, especially for students 
in Grades 3 to 5. There are different ways for students to get involved with programs such as 
Service Club, Conflict Managers and band for Fifth Graders. 
 
One way to stay connected with the latest information at MCB is through our website. There, you 
will find a copy of our monthly newsletters, calendar of events and other announcements. You can 
find our website at: http://mcb-brentwood-ca.schoolloop.com/. I also encourage you to become 
familiar with our district website at: http://www.brentwood.k12.ca.us/ . There are additional 
announcements regarding district wide events, school board meetings and information about our 
State Standards. 
 
School Mission Statement 
At Mary Casey Black Elementary, we are committed to creating a safe and engaged learning 
environment centered around respect for all, responsibility, and the development of academic, 
physical and social skills. 
We provide our students with a strong foundation of critical thinking and problem solving skills that 
will promote academic achievement and will prepare them for a successful future. 
 
District Mission Statement 
The Brentwood Union School District will provide an exemplary education to all children in a safe, 
student-centered environment designed to nurture the whole child and partner with families to 
prepare globally productive citizens. 
 
 
 

----
---- 
Brentwood Union Elementary 

School District 
255 Guthrie Lane 

Brentwood, CA 94513-1610 

(925) 513-6300 

www.brentwood.k12.ca.us 

 

District Governing Board 

Carlos Sanabria 

Emil Geddes 

Jim Cushing 

Steve Gursky 

Scott Dudek 

 

District Administration 

Dr. Dana Eaton 
Superintendent 

Robin Schmitt 

Chief Business Official 

Roxanne Jablonski-Liu 

Assistant Superintendent, Human 

Resources 

Michael Bowen 

Director - Curriculum & Instruction 

Robert Brown 

Director - Maintenance & 

Operations 
Assistant 

 

Chris Calabrese 

Director - Student Services 

Jeff Weiss 

Director - Special Education 

 

2017-18 School Accountability Report Card for Mary Casey Black Elementary School 

Page 1 of 8