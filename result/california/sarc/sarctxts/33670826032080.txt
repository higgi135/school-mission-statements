The vision statement is: Cottonwood K-8 School students will be educated in a positive, highly 
engaging school environment to become connected, independent, and collaborative life-long 
learners that are prepared for high school. “Excellence - Every Person - Every Day.” Our mission is 
to educate all students in a safe and equitable environment that promotes academic excellence 
through critical thinking, problem-solving and positive behavior to prepare students to become 
competent digital and world citizens in a diverse society. All students are encouraged to do their 
best each day. We work with our families to promote a positive home/school relationship so that 
students see the collaborative spirit with both their parents and their teacher! 
 
Schoolwide, we will focus on the areas of Teaching and Learning to improve instruction, Multi-
Tiered Systems of Support to support the "whole" child in Academics, Attendance, and Behavior, 
and Culture and Climate to connect students and parents to school. 
 
We are a Positive Behavior Intervention School and all students will know and understand our core 
behavior expectations: 
 
Be Safe! Be Responsible! Be Respectful! 
 
This is what we now strive to become as a school in 2019 and beyond. It will take all of us working 
together with pride and dedication to move forward and continue the success of our wonderful 
school. I know we will succeed! 
 
 

----

---- 

Hemet Unified School District 

1791 West Acacia Ave. 
Hemet, CA 92545-3632 

(951) 765-5100 

www.hemetusd.org 

 

District Governing Board 

Ms. Stacey Bailey 

Mr. Rob Davis 

Mrs. Megan Haley 

Mr. Gene Hikel 

Mr. Vic Scavarda 

Mr. Patrick Searl 

Mr. Ross Valenzuela 

 

District Administration 

Ms. Christi Barrett 
Superintendent 

Mr. Darrin Watters 

Deputy Superintendent 

Business Services 

 

Mr. Darel Hansen 

Assistant Superintendent 

Human Resources 

 

Mrs. Tracy Chambers 

Assistant Superintendent 

Educational Services 

 

Dr. Karen Valdes 

Assistant Superintendent 

Student Services 

 

 

2017-18 School Accountability Report Card for Cottonwood School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cottonwood School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

16 

16 

16 

0 

1 

0 

2 

0 

2 

Hemet Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1098 

40 

118 

Teacher Misassignments and Vacant Teacher Positions at this School 

Cottonwood School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.