Del Cerro Elementary School is an exceptional place for student learning, parent involvement, and 
community activities. We believe that by empowering students with the necessary tools to be 
literate, thinking, technology-using citizens, we prepare them for the challenges of the 21st 
century. 
 
Our staff, parents, and community share a common vision, and together we create an academically 
rigorous education for all students. Our curriculum balances enriching learning opportunities and 
quality support programs. We are committed to implementing the California Content Standards in 
a systematic and meaningful way while providing an environment that encourages social growth 
and civic responsibility. 
 
Everyone has a place at Del Cerro. Parents, students, and community members working together 
to provide a positive, caring, and welcoming environment where all students learn. Please come 
and visit! We always welcome new parent volunteers. 
 
Kristin Thomsen, PRINCIPAL 
 

----

-

--- 

Saddleback Valley Unified School 

District 

25631 Peter A. Hartman Way 

Mission Viejo CA, 92691 

(949) 586-1234 
www.svusd.org 

 

District Governing Board 

Suzie Swartz, President 

Dr. Edward Wong, Vice President 

Amanda Morrell, Clerk 

Barbara Schulman, Member 

Greg Kunath, Member 

 

District Administration 

Crystal Turner, Ed D. 

Superintendent 

Dr. Terry Stanfill 

Assistant Superintendent, Human 

Resources 

Connie Cavanaugh 

Assistant Superintendent, Business 

Laura Ott 

Assistant Superintendent, 

Educational Services 

Mark Perez 

Director, Communications and 

Administrative Services 

Dr. Ron Pirayoff 

Director, Secondary Education 

Liza Zielasko 

Director, Elementary Education 

Dr. Diane Clark 

Director, Special Education 

Rae Lynn Nelson 
Director, SELPA 

Dr. Francis Dizon 

Director, Student Services 

 

2017-18 School Accountability Report Card for Del Cerro Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Del Cerro Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

22 

25 

19 

0 

0 

0 

0 

0 

0 

Saddleback Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1,157 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Del Cerro Elementary School 

16-17 

17-18 

18-19