The R.H. Dana Exceptional Needs Facility has been providing services to Special Needs students since 1975. Students from aged 3 
(Preschool) through 12 (Grade 5) are placed at our facility via the Individual Education Program (I.E.P.) process. We share a campus 
with R.H. Dana Elementary School and our students are mainstreamed for various activities, both educational and social, as 
appropriate. 
 
The CUSD Early Start program, that addresses the needs of Infants and Toddlers, 0-3 who have Solely Low Incidence Disabilities (vision, 
hearing and orthopedic impairments), is also based at the Exceptional Needs Facility. 
 
District - wide staff in the disciplines of Physical Therapists (PT), Occupational Therapists (OT) and Adapted Physical Education (APE) 
have offices on the ENF campus. 
 
There is a California Childrens' Service Medical Therapy Unit housed at ENF which serves eligible ENF students,, as well as members 
of the public, and students from across CUSD, who qualify for their services. 
 
Our Mission Is Learning 
“We will ensure that all our children will learn more today than yesterday, and more tomorrow than today.” 
The Mission of Richard Henry Dana Exceptional Needs Facility is to produce students who demonstrate the basic skills and attitudes 
that make each person a positive, productive, and contributing member of society. To achieve this goal, students will be provided 
opportunities to develop independence, inter-personal and social skills, and they are encouraged to develop to the best of their ability. 
They will show respect for themselves and others, maintain regular attendance, exhibit good citizenship, and show an appreciation of 
diverse cultures. 
 
The School's current emphasis is to promote communication for our students in whatever mode is individually appropriate for them. 
To facilitate this goal, our staff and students have been utilizing technology and communication devices across settings in a 
collaborative manner conducive to progress for all. 
 
For additional information about school and district programs, please visit www.capousd.org