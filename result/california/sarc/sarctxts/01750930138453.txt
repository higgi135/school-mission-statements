Students come to Valley High School (VHS) for a chance to turn their academic and personal lives 
around. Students apply to Valley when they decide that they need a smaller, more focused 
academic and social environment, or when they have fallen behind at a comprehensive high school 
and are lacking credits and potentially in danger of not graduating from high school. Also on the 
VHS campus houses the district transition program, for young adults who receive services through 
an Individualized Education Plan or IEP. 
 
VHS staff is a team of professionals, teachers and support staff, who are dedicated to giving each 
student the individual attention, academic support, social guidance and personal support that will 
help them to succeed in their goal to earn a high school diploma or a GED. Our team includes nine 
teachers (two are part-time), a part-time counselor, three instructional assistants serving the 
transition program, resource program and math support, a campus supervisor, an office 
manager/principal’s secretary and part time school secretary. 
 
Valley’s academic and elective courses focus on State Standards and use State and District adopted, 
standards-based text books and support materials. Classroom instruction is specialized to meet the 
needs of a wide range of students, including those who need an active, hands-on classroom 
experience. Valley offers all academic courses required for graduation and five elective courses. We 
have high academic expectations and support systems in place to support both academic and 
social/emotional growth. 
 
During 2010-11, Valley was again named a California Model Continuation High School. In Spring of 
2014 we received a full 6 year term of accreditation from WASC with a mid-term review. Our 
Academic Performance Index has been among the highest in Alameda County Continuation 
schools. 
 

-------- 

 

 

----

---- 

Dublin Unified School District 

7471 Larkdale Avenue 

Dublin, CA 94568 
(925) 828-2551 

www.dublinusd.org 

 

District Governing Board 

Amy Miller, President 

Dan Cherrier, Vice President 

Dan Cunningham, Trustee 

Niranjana Nataragan, Trustee 

Megan Rouse, Trustee 

 

District Administration 

Dr. Leslie Boozer 
Superintendent 

Joe Sorrera 

Assistant Superintendent 

Business Services 

 

Leslie Anderson, Christine Williams 

Co-Acting Assistant 

Superintendents 

Educational Services 

 

Mark McCoy 

Assistant Superintendent 

Human Resources 

 

 

2017-18 School Accountability Report Card for Valley High (Continuation) 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Valley High (Continuation) 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

10 

0 

0 

9 

1 

0 

9 

0 

0 

Dublin Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

553 

16 

1 

Teacher Misassignments and Vacant Teacher Positions at this School 

Valley High (Continuation) 

16-17 

17-18 

18-19