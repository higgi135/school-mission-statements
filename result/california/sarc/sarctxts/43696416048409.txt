Vision and Mission 
We support all PAUSD students as they prepare themselves to thrive as global citizens in a rapidly changing world. We develop our 
students' knowledge, critical thinking, and problem-solving skills, and nurture their curiosity, creativity, and resilience, empowering 
every child to reach his or her full intellectual, social, and creative potential. 
We are a public, pre-K-12 school district that unites our students, teachers, staff, and parents to deliver on our collective vision by 
offering a rich and challenging academic experience to all students within a supportive community, dedicated to preparing our youth 
for the challenges of living in a fast-changing world. 
 
The Walter Hays Way 
At Walter Hays, we strive to bring to life our district’s vision and mission statements every day. We partner to create a balanced 
environment for our children to learn and grow intellectually, socially, emotionally, and physically. 
Walter Hays Core Values: Respect, Responsibility, Integrity, Initiative, Perseverance, Resilience, Empathy Inclusion, and Cooperation. 
The students, staff and families of Walter Hays strive to live these values every day. We recognize that with consistent effort and 
practice, along with our best intentions, we support a safe and positive environment for all to learn and grow. Each month every 
classroom focuses on the core value. 
 
In order to prepare our student for the 21st Century work world, we have on going training of our teaching staff in the areas of English 
Language Arts, Mathematics, Social Studies and Science. The curriculum includes: 
Teachers College of Columbia University Units of Study Reading Curriculum 
Teachers College of Columba University Units of Study Writing Curriculum 
Bridges in Mathematics 
Social Studies Alive 
FOSS Science that is adjusted to NGSS, pending our Science Adoption 
 
Attendance rates are high. Class size varies from 19-24 students in grades K-5.