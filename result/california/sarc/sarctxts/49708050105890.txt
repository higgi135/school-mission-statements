We are the charter school for those who choose: 

Education through challenging academics 

• 
• An environment that is physically and psychologically safe 

Where: 

 

• Young adults are educated to become citizens who make a difference in the world 

Mission Statement: 
 
Mark West Charter School fosters a school culture where every member of the school community is provided powerful instruction in 
21st Century skills and common core academics. Progress will be measured by District Benchmarks, formative and statewide 
assessments, as well as performance tasks. We will use technology, project based learning, and a caring, dedicated staff to meet the 
social-emotional and academic needs of each and every student. We will provide differentiation during the instructional day to ensure 
students get the level of support they need to succeed. Students will participate in community service projects to find a connection to 
their surroundings and ways they can become active members in their community. 
 
The school is committed to: 

• Partnering staff, students, and community to create a unique, challenging, individualized learning environment, and an 

academically rich curriculum for all students 

• Developing self-motivated, self-disciplined and socially responsible students 
• Applying academic learning to real-life activities through project-oriented programs 
• Building programs that foster thinking which is original, critical, collaborative and reflective 
• Providing a safe, nurturing environment 

Those who wish to learn more are welcome to read our entire charter at the Mark West Charter School Office or on our website at 
mwcharter.org.