The Charter School’s Mission is to educate each child individually and personally to the goal of each child’s highest level of academic 
achievement, social and emotional growth enrichment. 
 
Vision Statement 
 
The Charter School envisions to progress along the continuum of the Mission to the goal of the Charter School’s highest level of 
achievement in the various aspects of the Charter School’s development. TFSCS envisions the lofty goal of preparing each child who 
attends the Charter School from Kindergarten through eighth grades (with parent participation and involvement) to be successful in 
high school and not only enroll in college, but finish college with a Bachelor’s and post graduate degrees. The Charter School envisions 
preparing its students to become successful, productive members of society. TFSCS students will volunteer to assist their home school 
at TFSCS and others in society as they continue their successful achievement and growth by being lifelong learners. 
 
The mission of TFSCS rests with a commitment to excellence in educating at risk students. First we must answer the question: “at-
risk” of what? We believe our students are at risk of not having the same opportunities to quality education with educated and 
interested teachers, access to proven educational curriculums and exciting learning alternatives, therefore creating a challenging 
environment to reach their fullest potential. We believe parents are at risk of not recognizing the impact they have on the shape and 
development of their children’s lives. We believe teachers are at risk of not believing all students can learn and are willing to provide 
instructional techniques to ensure students succeed in the classroom. Understanding what is at risk has enabled us to provide 
students, teachers and parents with an educational alternative built on clearly outlined personal and educational expectations and a 
proven, research based curriculum. 
 
Today’s Fresh Start Charter School’s student population includes up to 93% of socio-economically disadvantaged students. Most new 
enrollees have attended underperforming schools in the geographic area and are educationally disadvantaged. Due to these 
disadvantages, many of these elementary aged students are deemed at risk of failing and not succeeding in the skills of lifelong 
learning. By providing an educational alternative, qualified teachers and a diverse learning environment offers the goal of ensuring 
no child is left behind. The early detection of each child’s potential for success is a priority. Students have a rigorous, hands-on, 
comprehensive and performance based learning environment. The TFSCS curriculum is strongly reinforced with intervention and 
enrichment studies including after school tutoring, small group instruction, Saturday enrichment studies and two weeks concentrated 
study prior to the start of school for students who need more academic help (as funds are available). These sessions form a bridge for 
students to achieve academic excellence, linking them to current study needs and a jumpstart prior to starting the school year. All 
students who are second grade through eighth grades have their personal laptop for school use. Students in lower grades, transitional 
kindergarten, kindergarten and first grade use other types of technology aside from laptops, such as tablets. 
 
TFSCS uses a site-based model of instruction. Many of the public schools located in the area place in the bottom three declines of 
both the statewide and similar schools academics as shown on the California State Dashboard. Today's Fresh Start-Compton is a 
Community Eligibility Provision (CEP) approved school and therefore all students at TFSCS receive free breakfast and lunch. 
 
 

2017-18 School Accountability Report Card for Today's Fresh Start Charter School - Compton 

Page 2 of 11