Nuffer Elementary School has been serving children and families in the Norwalk community since 
1952. With over 350 students, our vision is that “Nuffer Elementary has a school with an unrelenting 
focus on learning.” Our teachers and parents work together through our School Site Council (SSC), 
English Learners Advisory Committee (ELAC), and the Parent Teacher Association (PTA) to support 
our school’s vision, to implement and monitor programs, and to ensure that all students’ needs are 
met. 
 
During the past four years, our school has significantly increased its focus on learning. To keep 
students engaged and achieving at high levels, our teachers use a variety of instructional strategies 
and resources. Through a systematic approach to intervention, our teachers are able to respond 
and provide additional support to those students who are not meeting the standards or are 
struggling with grade level work. With over 140 students who are English Learners, every class 
provides 30-minutes of English Language Development (ELD) every day to help students easily 
access the core curriculum and master the California Common Core Standards. As a result of all of 
these efforts, we have made great gains in student achievement. 
 
Unique to Nuffer Elementary is our Joan Sander Memorial Unit, which is a building dedicated to the 
education and care of students with special needs. Our school is committed to helping all students 
with varying degrees of disabilities to reach their individual learning plan goals. We believe that all 
students can learn and show progress despite the various challenges they may face. 
 
Regardless of which population of students we serve, Nuffer Elementary provides a safe and 
nurturing environment in which all students can learn and grow. We are committed to providing 
our students with a high-quality education; and we are confident that by the time our students 
leave our doors to promote to middle school, we will have prepared the whole child for success. 
 

----

--- 

Norwalk-La Mirada Unified 

-

School District 

12820 Pioneer Blvd 
Norwalk, CA 90650 

(5662) 868-0431 
www.nlmusd.org 

 

District Governing Board 

Ana Valencia Board President 

Jude Cazares Board Vice President 

Darryl R. Adams Board Member 

Jesse Urquidi Board Member 

Karen Morrison Member 

Chris Pflanzer Board Member 

Jorge Tirado Board Member 

 

District Administration 

Dr. Hasmik Danielian 

Superintendent 

Dr. Patricio Vargas 

Assistant Superintendent - 

Educational Services 

Estuardo Santillan 

Assistant Superintendent, Business 

Services 

John Lopez 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for John H. Nuffer Elementary STEAM Academy 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

John H. Nuffer Elementary STEAM Academy 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

19 

16 

19 

0 

0 

0 

0 

0 

0 

Norwalk-La Mirada Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

898 

2 

4 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.