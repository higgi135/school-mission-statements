Jersey Avenue Elementary School, located in the beautiful city of Santa Fe Springs, California, is a 
California Distinguished School serving students TK - 5th grade. Jersey was most recently named a 
California Distinguished School in 2018 and had previously earned this award in 2010. Jersey earned 
the Model Professional Learning Community at Work award in 2017 and has also been named a 
Title I Academic Achievement School four times since 2009. Additionally, the California Business for 
Education Excellence (CBEE) Honor Roll has been awarded to Jersey Avenue ten of the past eleven 
years (2007, 2008, 2009, 2010, 2011, 2013, 2014, 2015, 2016, and 2017). Jersey was also named a 
California Gold Ribbon School in 2016. These recognitions are significant indicators of the hard work 
and dedication of the teachers, staff, parent volunteers, as well as all of our community partners. 
While these state recognitions are outstanding, the most meaningful recognitions come from the 
parents and students that attend Jersey. Last school year, when surveyed about their satisfaction 
with Jersey Avenue Elementary School, parents responded with a resounding 97% satisfaction rate. 
These results are an outward expression of the inner drive and dedication of the teachers, staff, 
and parent volunteers that serve the students at Jersey Avenue Elementary School. 
 
With a primary focus on student achievement, Jersey Avenue Elementary School offers multiple 
programs to support student learning. These programs include core instruction following the new 
California State Standards. Jersey Avenue also offers the Arts for All music, art, and physical 
education program. Further, Jersey Avenue Elementary School is home to the THINK Together and 
Stone Soup after-school programs. These programs offer low and no-cost options for students to 
extend the educational program past the instructional school day. 
 
In addition to all this, Jersey Avenue has made a concerted effort to increase student use of 
technology in every classroom. To support the use of technology, Jersey offers every student, TK-
5, with online access to outstanding adaptive learning programs such as Reading A-Z©, Raz-Kids©, 
and iReady©. These resources are available to students while at school as well as through any 
Internet-connected phone, tablet, or computer to extend learning beyond the walls of the 
classroom. These highly engaging programs are made available through the funds that Jersey 
receives through the Local Control Accountability Plan (LCAP). 
 
Denisse Frenes-Gomez, Principal 
 
Mission Statement: 
 
WITH A SHARED VISION ON LEARNING, THE JERSEY COMMUNITY STRIVES TO CREATE AN 
ENVIRONMENT THAT FOSTERS AND DEVELOPS ACADEMIC AND SOCIAL ACHIEVEMENT FOR ALL 
STUDENTS THROUGH A BALANCED AND RIGOROUS CURRICULUM. 
 
CON UNA VISIÓN COMPARTIDA EN EL APRENDIZAJE, LA COMUNIDAD DE JERSEY SE ESFUERZA POR 
CREAR UN AMBIENTE QUE FORMENTA Y DESARROLLA LOGRO ACADÉMICO Y SOCIAL PARA TODOS 
LOS ESTUDIANTES A TRAVÉS DE UN CURRICULUM EQUILIBRADO Y RIGUROSO. 
 

2017-18 School Accountability Report Card for Jersey Avenue Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Jersey Avenue Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

21 

21 

21 

0 

0 

0 

0 

0 

0 

Little Lake City School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

9 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.