Greenfield Community Day School is an alternative placement school within the Greenfield Union 
School District. It is located near the southern outskirts of the Bakersfield city limits and receives 
students from all Greenfield school district’s eight elementary schools: Fairview, Granite Pointe, 
Horizon, Kendrick, Palla, Plantation, and Planz, Valle Verde, as well as three middle schools: 
Greenfield, McKee, and Ollivier. Enrollment fluctuates during the school year, which includes 
students with disabilities. Community School is designed to provide families with an education 
alternative within the district when other options have failed. The average length of stay for 
community school students in 2016-17 was 62 days. Greenfield Community Day School is a Title 1 
Schoolwide Program. We have students classified as English Language Learners. Through the 
Community Eligibility Provision, 100% of our students participate in the free or reduced lunch 
program. It is our goal to address the needs of the specific high-risk student population who 
chronically experience attendance and discipline problems as well as lower achievement levels. 
 
 
 
 

 

 

----

--- 

-

Greenfield Union School District 

1624 Fairview Rd. 

Bakersfield, CA 93307 

(661) 837-6000 

https://www.gfusd.net/ 

 

District Governing Board 

Richard Saldana 

Mike Shaw 

Melinda Long 

Kyle Wylie 

Dr. Ricardo Herrera 

 

District Administration 

Ramon Hendrix 
Superintendent 

Sarah Dawson 

Assistant Superintendent 

Curriculum 

 

Lucas Hogue 

Assistant Superintendent 

Personnel 

 

Rebecca Thomas/TBD 

Assistant Superintendent 

Business 

 

 

2017-18 School Accountability Report Card for Greenfield Community Day School 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Greenfield Community Day School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

4 

0 

0 

4 

0 

0 

4 

0 

0 

Greenfield Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

399 

52 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.