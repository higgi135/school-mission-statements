The school is located in southeast Bakersfield and is one of 12 schools in the Greenfield Union 
School District, consisting of eight TK-5 schools, three middle schools, and one community school. 
Palla Elementary has become more culturally diverse over the years w/ a . 
 
The enrollment is currently 931 TK-5th grade students. Palla students attend either Greenfield 
Middle, McKee Middle or Ollivier Middle at the completion of 5th grade. Over the years, the 
student body has become more culturally diverse. Palla Elementary has fully implemented the 
Common Core State Standards (CCSS) in the areas of Mathematics and English Language Arts. Our 
teachers are skilled at evaluating and monitoring student progress through reading screenings, 
observations and District Unit Assessments. Our school site has two learning centers where two 
highly qualified teachers with special education credentials assist those students with 
Individualized Educational Plans (IEPs) as well as regular education students who are in need of 
extra assistance. We are dedicated to support each child and help develop his/her talents to instill 
and develop their love for learning. 
 
Palla is a Title I school which means the focus is to use all funds (federal and state) available to 
provide our students with the most effective strategies and programs to ensure students achieve 
grade level standards. 
 
Any questions in regards to this document should be directed to Julie Billington. 
 

 

 

----

---- 

----

--- 

-

Greenfield Union School District 

1624 Fairview Rd. 

Bakersfield, CA 93307 

(661) 837-6000 
www.gfusd.net 

 

District Governing Board 

Mike Shaw 

Kyle Wylie 

Dr. Ricardo Herrera 

Richard Saldana 

Melinda Long 

 

District Administration 

Ramon Hendrix 
Superintendent 

Sarah Dawson 

Assistant Superintendent 

Curriculum 

 

Lucas Hogue 

Assistant Superintendent 

Personnel 

 

Rebecca Thomas/TBD 

Assistant Superintendent 

Business 

 

 

2017-18 School Accountability Report Card for Palla Elementary School 

Page 1 of 9 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Palla Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

39 

34 

40 

5 

0 

4 

0 

3 

0 

Greenfield Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

399 

52 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Palla Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.