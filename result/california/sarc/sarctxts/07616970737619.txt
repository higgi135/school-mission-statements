The Willow campus has three programs: Willow Continuation High School, the Independent Study program and Home/Hospital 
program. These three programs offered are alternatives to the traditional programs at the other three district sites. Alternative 
education programs are designed to meet the individual needs of the students in our community, challenge each student to excel in 
all areas and provide the opportunity to achieve competency in basic skills to earn a high-school diploma. The staff believes in and 
operates on the principle that it is important to know each student personally and maintain maximum individual contact in order to 
provide the necessary assistance to each student to achieve goals of academic, vocational, personal and social growth. 
 
Willow Continuation High School serves students who are at least 16 years old and need an alternative to a traditional high school 
program because of credit deficiency, a pattern of negative behavior or truancy. The Willow High program provides individualized 
learning plans for each student and provides students with tools they will need after high school. 
 
In the Independent Study Program, the student assumes the primary responsibility for his or her education. Students meet with their 
assigned teacher once a week for one hour to assess their academic work. 
 
The Home/Hospital program services students with a temporary medical issue that enables them to continue their education in their 
home environment. 
 
The Willow campus is a 1:1 Chromebook environment that provides students with a credit-recovery online option through Cyber High. 
Students can make up credits in courses the student is deficient in. Courses are English I, II and III; world history; American history; 
biology; physical science; and health. Here at Willow, we believe every student can succeed with support and the opportunity to 
demonstrate knowledge in alternative ways. We understand that students learn in multiple ways, and we encourage students to tap 
into that knowledge and learn to be successful.