Our Story 
Several years ago, a group of mothers in the West Adams community expressed an interest in creating a constructivist elementary 
school with a diverse student body and a project based curriculum. After many meetings, much research and multiple tours of other 
schools, the desire for this school to be dual-immersion was born. City Language Immersion Charter is the dream of community 
members, parents, educators, and experienced charter operators to start a smaller, alternative learning community in West Adams 
with a focus on dual language immersion. The dedication of hard-working volunteers and a growing number of families seeking a 
diverse neighborhood school has given rise to the conviction that CLIC will create an exceptional public school and an option of dual-
immersion that does not currently exist. 
 
Mission 
The CLIC School provides an exceptional education to a diverse student body. Through individual attention in a supportive and dynamic 
learning environment, students become creative and critical thinkers who ask questions, debate, and express ideas fearlessly and 
respectfully. With a focus on civic responsibility, dual language learning, and the written word, CLIC students are prepared for a lifetime 
of meaningful work and ongoing service to a cause greater than themselves. The dual language model we are implementing integrates 
the best of bilingual education for native Spanish speakers with second language acquisition for native English speakers. Language 
minority students (Spanish speakers) develop literacy in their first language before acquiring their second, resulting in higher levels of 
proficiency in both. Teachers differentiate instruction for children at different levels of language fluency and literacy. In addition, we 
believe that students should be given opportunities to direct their own learning. Our students will be asked to explore their interests 
through the process of choosing specific projects and formulating essential questions that drive their learning experiences. Current 
research shows that a constructivist approach to teaching and learning develops deep and long-lasting conceptual understanding in 
students. When coupled with data driven instruction and problem-based lessons, constructivism is even more effective.