Description of District 
The Visalia Unified School District is the oldest school district in Tulare County. The district is comprised of 25 elementary schools, 5 
middle schools, 4 comprehensive high schools, 1 continuation high school, 5 charter schools, 1 adult school, and a school that serves 
orthopedic handicapped students. Over 32,000 students Pre-K to adult are served through the Visalia Unified School District. 
 
Description of School 
Mt. Whitney High School served approximately 1,600 students in grades 9-12 in 2017-18. Our teachers and staff are dedicated to 
ensuring the academic success of every student and providing a safe and productive learning experience. The school holds high 
expectations for the academic and social development of all students. Curriculum planning, staff development, and assessment 
activities are focused on assisting students in mastering the state academic content standards, as well as increasing the overall student 
achievement of all subgroups. 
 
Mission Statement 
At Mt. Whitney High School, "We exists to provide students with an education that affords them limitless opportunities for the future." 
 
Vision Statement 
 
Schoolwide Learner Outcomes 
1. Communicate: Which will be evaluated by the student’s ability to: 
Use reading, writing, and oral language skills competently 
Demonstrate digital literacy through the use of appropriate technology in a variety of forms 
Demonstrate an ability to read, comprehend, and process complex text. 
 
 
2. Collaborate: Which will be evaluated by the student’s ability to: 
Exhibit the ability to access appropriate and reliable information in a variety of forms and use that information in new situations. 
Participate appropriately in a variety of educational, social, and team settings while demonstrating respect/concern for others within 
a small group. 
Question issues and justify own opinions or positions respectfully. 
 
3. Create: Which will be evaluated by the student’s ability to: 
Express self through a variety of forms such as art, craftsmanship, music, drama & foreign languages 
Show a curiosity and desire to explore and analyze the world around him 
Identify a particular or given problem, gather necessary facts and reliable resource and organize information in a usable manner. 
 
4. Critically Think: Which will be evaluated by the student’s ability to: 
Demonstrate the ability to critically evaluate challenges, information, solutions, and organize information is a usable manner. 
Demonstrate a growth mindset through an ability to initiate, define, establish, evaluate, and accomplish short and long term goals. 
Recognize and understand the need for academic, personal, professional goals. 
 
5. Be Civic-Minded: Which will be evaluated by the student’s ability to: 
Demonstrate responsible work ethic by respecting deadlines & due dates. 
Demonstrate academic honesty and integrity. 
Demonstrate personal responsibility and accountability in all situations. 
 
 

2017-18 School Accountability Report Card for Mt. Whitney High School 

Page 2 of 14