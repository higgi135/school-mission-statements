Principal's Message 
I would like to welcome you to Chaparral Elementary School's Annual School Accountability Report 
Card. In accordance with Proposition 98, every school in California is required to issue an annual 
School Accountability Report Card that fulfills state and federal disclosure requirements. Parents 
will find valuable information about our academic achievement, professional staff, curricular 
programs, instructional materials, safety procedures, classroom environment, and condition of 
facilities. 
 
At Chaparral Elementary School, we consider ourselves a community of learners, which includes 
students, staff, parents, and business/community partnerships. Through this process we are 
preparing our students to be critical thinkers and problem solvers. Our students experience a 
balanced, rigorous, and challenging curriculum that allows for individual differences and fosters 
responsible citizenship in a safe and orderly environment. 
 
Mission Statement 
Chaparral Elementary School strongly believes that "every student will experience success each 
day". The Chaparral community prepares its students to be critical thinkers and problem solvers. 
Our students experience a balanced, rigorous, and challenging curriculum that allows for individual 
differences and fosters responsible citizenship in a safe environment. 
 
School Profile 
Chaparral Elementary School is located in the northern region of Claremont and serves students in 
kindergarten through sixth grade following a traditional calendar. At the beginning of the 2017-18 
school year, 663 students were enrolled, including 11.3% in special education, 5.1% qualifying for 
English Language Learner support, and 22.9% qualifying for free or reduced price lunch. 
 

-------- 

 

 

Claremont Unified School District 

 

170 West San Jose Avenue 
Claremont, CA 91711-5285 

(909) 398-0609 

www.cusd.claremont.edu 

 

District Governing Board 

Hilary LaConte, President 

Beth Bingham, D.Min., Vice 

President 

Nancy Treser Osgood, Clerk 

Steven Llanusa, Member 

Dave Nemer, Member 

 

District Administration 

James Elsasser, Ed.D. 

Superintendent 

Lisa Shoemaker 

Assistant Superintendent, Business 

Services 

Julie Olesniewicz, Ed.D. 

Assistant Superintendent, 

Educational Services 

Kevin Ward 

Assistant Superintendent, Human 

Resources 

Brad Cuff 

Assistant Superintendent, Student 

Services 

 

2017-18 School Accountability Report Card for Chaparral Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Chaparral Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

26 

28 

30 

0 

0 

0 

0 

1 

0 

Claremont Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

325 

5 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Chaparral Elementary School 

16-17 

17-18 

18-19