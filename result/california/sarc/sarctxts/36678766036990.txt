Anderson School has 13 classrooms, a multipurpose room, and an administration office. The campus was built in 1971 and modernized 
in 2012, providing sufficient space for instruction. The program at Anderson serves students ages 12 to 22 years of age with moderate 
to profound disabilities. Anderson School strongly supports teaching and learning through a functional skills curriculum, modified 
classrooms, a multi-purpose room and ample playground space. 
 
The primary mission of Anderson School is to provide instruction to each student in order to develop the skills, knowledge, and 
understanding required to promote independence, personal development, and inclusion to the fullest extent for the individual’s 
potential at home, at school, and in the community.