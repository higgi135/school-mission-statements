Cultivating Greatness 
Marblehead Elementary offers an integrated study of the natural environment, where our students learn core academic subjects and 
engage in problem-based learning for real-world application. 
 
Marblehead Elementary School opened its doors for the first time in September 2001 and currently serves approximately 400 
kindergarten-fifth grade students. Situated on the highlands in northern San Clemente adjacent to a neighborhood park, Capistrano 
Unified School District’s first two-story elementary school is a unique design. Glimpses of the ocean can be viewed from several 
locations on the Marblehead campus. The “Aloha Spirit” of Respect, Responsibility and positive Relationships permeates the 
Marblehead Elementary school community. 
 
Staff members at Marblehead Elementary School form a highly-trained, dedicated teaching~learning community. The experienced 
teaching staff demonstrates the highest level of professionalism, dedication to continuing their own education and bringing a variety 
of specializations, including the Visual Arts, Science and English Language Development. 
 
All classrooms at Marblehead are connected to the Internet. Teachers use LCD Projectors and Document Cameras to engage students 
with visual aids to learning. Marblehead Elementary school has a school-wide computer lab with 30 computers. Every classroom has 
multiple laptop computers and every student in grades 2-5 has a Chromebook for use during the school day. Online programs such 
as STMath and Scholastic Reading Counts support student mastery of mathematics and reading. 
 
Marblehead students are able to work in a school garden. An outdoor classroom adjacent to the garden takes advantage of San 
Clemente's renowned climate and provides a unique setting for studying our environment. 
 
For additional information about school and district programs, please visit www.capousd.org