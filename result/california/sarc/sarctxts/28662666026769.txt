Alta Heights Elementary School 
 
MISSION 
The mission of Alta Heights Elementary School is to promote academic, social and emotional growth for all students, encouraging 
lifelong learning in an ever-changing world. 
 
VISION 
We believe that parents, teachers, staff and community members support our students with purposeful work and relationships to 
ensure our students leave our school as confident, creative, lifelong learners who communicate and collaborate effectively and think 
critically. 
 
COLLECTIVE COMMITMENTS/GOALS 
 
We Commit To: 
 
providing a strong foundation in ELA and math with an additional focus on hands-on science, engineering and art 
holding high expectations for all students and adult learners 
protecting collaborative Professional Learning Community meeting time 
communicating effectively with students, parents and each other 
using a variety of instructional strategies and groupings to meet the needs of all learners 
monitoring student progress by developing common formative assessments, collecting data, and using the results to plan student 
interventions and enrichment 
developing engaging and meaningful lessons for students. 
creating a safe and trusting environment and relationships 
celebrating students’ achievements 
aligning our school goals with the district goals to ensure that every student is career and college ready