Description of District 
In 2018-19 Lindsay Unified School District (LUSD) was comprised of six elementary schools, one comprehensive high school, and three 
alternative education schools. During the 2017-18 school year, the district served approximately 4,121 students in grades K-12. The 
LUSD Mission Statement is “Empowering and Motivating for Today and Tomorrow.” 
 
Description of School 
Lincoln Elementary School served approximately 424 students in grades Preschool-8 in 2018-19. Our Learning Facilitators and staff are 
dedicated to providing a positive learning environment and student-centered learning experience for all students. In addition to 
Lincoln Elementary School’s core academic program, a wide range of support services are available to students. All programs, practices, 
interventions, and supplemental activities are focused on ensuring student academic achievement and personal success.