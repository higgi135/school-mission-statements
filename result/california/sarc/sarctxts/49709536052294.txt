Sassarini’s experienced, dedicated staff is deeply committed to developing responsible citizens and ensuring academic success for 
every student. We have a very caring and positive environment, and visitors always comment on our students’ excellent behavior. We 
have sing-alongs and student recognition assemblies once a month, academic competitions, ample play yards, and a strong art and 
science program to compliment our Literacy and Math instruction and curriculum. We have intervention and enrichment programs, 
providing students with opportunities to make valuable gains in their learning. Students are leveled for literacy intervention and ELD 
between grade levels and math at the 4th and 5th grades. We reduce class sizes for students performing below proficiency by including 
our resource and special program teachers during this intervention time. We also enrich proficient students with advanced learning 
opportunities. 
 
Newly developed mission, vision and values: 
 
Mission: 
To inspire our community of learners to achieve their optimum potential. 
 
Vision: 
Every child, every classroom, every day. 
Sassarini is a proud learning community where diversity is welcomed and celebrated. 
 
Values: 
All students and staff have the right to a safe and respectful learning environment that cultivates positive connections 
We work together with our extended community to support students’ diverse academic, social, and emotional growth