Rocklin Academy is a public, tuition free, charter school, which was developed by parents and teachers for the Rocklin area. We are 
authorized by the Rocklin Unified School District. 
 
Mission Statement 
Rocklin Academy Family of Schools provides a distinct educational program strengthened by community and parent partnerships to 
achieve high standards, rich core content, and innovative learning. 
 
Vision Statement 
We envision a school community that inspires its students to excel academically, pursue their passions, and impact the world with 
excellence. 
 
Core Values 
1. The future we want to create includes a community of leaders who have strong shared beliefs and values that all students have the 
ability to learn at high levels and the expectations of our organization/schools to meet or exceed that level. 
2. The future we want to create includes a community of leaders who are data savvy; they embrace and monitor data, and use it to 
drive continuous improvement. 
3. The future we want to create includes a community of leaders who have a collaborative relationship and establish a strong 
communication structure to inform and engage both internal and external stakeholders in setting and achieving district-wide student 
learning and achievement goals. 
4. The future we want to create includes a community of leaders who are knowledgeable, ethical, responsible, critical thinking, and 
engaged members of society. 
5. The future we want to create includes a community of leaders who utilize research based, varied, differentiated and effective 
instructional practices to ensure all students learn at high levels.