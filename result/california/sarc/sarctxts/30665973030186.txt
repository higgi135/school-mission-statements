Monte Vista High School provides an independent study program as an option for students who 
need to complete their course work outside of the traditional classroom. The Education Code 
mandates that students enroll voluntarily. Students refer themselves and/or are referred by their 
high school counselor or Principal. Students enrolled in independent study choose this course of 
study for various reasons. Many students work, pursue athletic endeavors, or have health and 
personal issues that prevent them from being in class daily. Many students are also involved in 
activities in the community which prevent them from completing a traditional high school day. 
Students must be 16 to 19 years of age and have completed 120 credits, although we make 
exceptions when deemed necessary by administration. Students must be able to demonstrate the 
ability to earn high school credits with a 2.0 or better grade point equivalent. Students who do not 
meet the minimum requirements may apply if they have extenuating or unique issues and may be 
granted a waiver based on their needs. Monte Vista High School is not a credit retrieval program, 
and students enrolling must be within 20 credits of their grade level requirements. 
 
All students meet teachers during scheduled weekly appointments either individually or in a small 
groups. During appointments, instruction is provided, work is corrected, and new course work is 
assigned. Students must produce a minimum of twenty hours of work weekly and finish their class 
within a four to six week period to satisfy the minimum requirements approved by the Newport-
Mesa Board of Education. Students who want to "fast track" their course work can devote more 
time to their study packets, complete projects, tests and quizzes and complete the course in less 
than three weeks. Arrangements must be made with the course teacher and dates and hours of 
attendance will be flexed. Monte Vista was one of the first five California high schools specializing 
in independent study to receive WASC accreditation. In 2017, Monte Vista was accredited once 
again by the state of California for a six year term. Monte Vista staff, students, parents and 
community members recently revised our school vision statement and Expected School wide 
Learning Results (ESLRs). 
 
VISION Our vision is to provide all students with an individually challenging curriculum leading to 
a high school diploma, within a safe, supportive environment, and with flexible hours of attendance 
according to the students' individual needs. All course work is equivalent to the comprehensive 
high schools. Students will be able to arrange their scheduled appointments to meet their needs 
and support their out of school responsibilities. 
 
Expected School wide Learning Results: 
 
Students will be competent in listening, reading, writing and computation in order to meet the 
district's graduation requirements and California's state standards. 
Students will base life decisions on awareness of positive community role models. They will work 
effectively with others, and respect the diversity of others while demonstrating honest, ethical 
behavior. 
Students will be able to use the information and skills acquired in school and apply those skills in 
their lives to achieve success in future endeavors. 
 
 

2017-18 School Accountability Report Card for Back Bay Continuation High School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Back Bay Continuation High School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

10 

0 

1 

6 

0 

1 

0 

0 

1 

Newport-Mesa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1105 

5 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.