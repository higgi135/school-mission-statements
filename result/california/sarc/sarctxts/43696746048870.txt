George Mayne Elementary School is a Professional Learning Community of learners, leaders and thinkers focused on the belief in a 
growth mindset for parents, students and our school team. Our partnerships with parents, businesses and the broader community 
help us to provide a quality education in a supportive, multi-lingual environment where each child's worth and potential is valued. Our 
Mayne Mission is Equity and Excellence in Education which means we are dedicated to doing what is best for children in order to 
enable them to meet the challenges of the twenty-first century. The school promotes the District's goal to "prepare every student to 
succeed in an ever-changing world," by meeting State and District content and life-long learning standards and by providing a positive 
learning climate for social-emotional well-being and development. Our Mayne Vision: We are Eagles Soaring to Success is a reference 
to our school mascot, the eagle and our SOAR Program of Safe, Organized Achievers who are Respectful. It is also a description of our 
belief that every child (eagle) will soar (succeed) and has an unlimited potential to reach new heights of achievement. 
 
The school has an outstanding team of classified and certificated personnel who work together in grade level and group teams and as 
a site to constantly improve teaching and learning as well as comprehensive services for parents, students, and our community. We 
know that our students have to succeed in a competitive workplace where an emphasis is placed on high-level literacy, mathematics 
and communication skills, as well as the ability to work together. Students are being taught with an eye on having personal standards 
of excellence, motivation to succeed socially, emotionally and academically. Over time this has resulted in significantly raising 
achievement at our school. 
 
George Mayne School is one of only two schools in Santa Clara Unified that has both an English and a Bilingual Spanish/English 
Program. We have a history of successfully implementing a strong bilingual classroom program that supports our students and 
community as well as providing a fully included Special Education Program where students consistently show growth. 
 
Professional development efforts include training in Professional Learning Community or PLC strategies K-5, Sobrato Early Academic 
Language (SEAL) Strategies PreK-3, Advancement Via Individual Determination (AVID) Strategies K-5, Implementation of Positive 
Behavior Intervention Support ( PBIS) K-5, Cooperative Play K-5, ELD, Literacy, Math and Technology workshops.