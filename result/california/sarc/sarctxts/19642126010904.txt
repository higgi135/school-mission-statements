A Message from the Principal 
Carver Academy is known throughout L.A. county for its outstanding Schoolwide Enrichment 
Clusters Program. Recognizing that all learners are unique, learning experiences must take into 
account the abilities, interest, and learning style of individual students. Twice a year during the six 
week cluster, students spend one hour a week exploring a topic of interest with the assistance of 
knowledgeable community members. Along with gaining in-depth insight into a particular topic, 
students begin to take responsibility and interest in their own learning. The regular curriculum is 
supported, expanded and enhanced by children participating in enrichment activities and bringing 
their enthusiasm into the classroom setting. Student and cluster leaders communicate what they 
have accomplished at a schoolwide celebration to culminate the Enrichment Clusters Program. 
These educational opportunities, supported by an excellent teaching staff, create a first rate 
educational opportunity that is both challenging and enjoyable for Carver Academy students. Our 
school has received accolades from the California Business for Education Excellence (CBEE) Star 
School in Math and Science, Title 1 Academic Achievement Award, California Gold Ribbon, 
California Distinguished School, the California School Boards' Golden Bell, the New and Emerging 
Magnet School Award and the California Business for Education Excellence (CBEE) Star School. Also, 
a team of our fifth grade students competed and won the Science Olympics at the Los Angeles 
County Office of Education, where dozens of teams from districts throughout the county competed 
to take home the title. 
 
Carver Academy is located in the city of Cerritos and serves 651 students in grades preschool 
through sixth. Our calendar is based on a traditional school year from August to June. We are 
dedicated to ensuring the academic success of every student and providing a safe and 
comprehensive educational experience. 
 
Carver Academy is a school community where the highest expectations are maintained. All 
available resources are utilized to enable students to become life-long learners who possess the 
ability to achieve their utmost potential. We promote a safe, nurturing, and stimulating 
environment that invites students to actively participate in educational opportunities. Carver 
provides a welcoming atmosphere in which parents and community members are encouraged to 
become immersed in our students' educational endeavors. Individual academic and social needs of 
staff and students are met, creating a desirable environment where all participants are successful. 
 
We believe there are character traits that our students must learn to be members of the Carver 
Community and to achieve continual academic success. Our students know that Carver CARES! 
Carver Cubs are Compassionate, have A Positive Attitude, are Respectful, Enthusiastic and always 
Strive for Excellence. 
 
Mission Statement 
As effective worldwide communicators, Carver Academy students will be critical thinkers and 
inquisitive learners, with a sense of personal commitment to action and service. Our staff will 
provide preschool through sixth grade students with rich learning experiences in a nurturing and 
challenging academic environment. 
 

----

--

-- 

ABC Unified School District 

16700 Norwalk Blvd. 
Cerritos, CA 90703 

(562) 926-5566 
www.abcusd.us 

 

District Governing Board 

Ernie Nishii, President 

Dr.Olga Rios, Vice President 

Sophia Tse, Clerk 

Christopher Apodaca, Board 

Member 

Leticia Mendoza, Board Member 

Maynard Law, Board Member 

Soo Yoo, Board Member 

Leticia Mendoza, Board Member 

 

District Administration 

Dr. Mary Sieu 
Superintendent 

Dr. Valencia Mayfield 

Assistant Superintendent, 

Academic Services 

 

Toan Nguyen 

Assistant Superintendent, 

Business Services 

Chief Financial Officer 

 

Dr.Gina Zietlow 

Assistant Superintendent, 

Human Resources 

 

 

2017-18 School Accountability Report Card for Carver Academy 

Page 1 of 9 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Carver Academy 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

24 

24 

24 

0 

0 

0 

0 

0 

0 

ABC Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Carver Academy 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.