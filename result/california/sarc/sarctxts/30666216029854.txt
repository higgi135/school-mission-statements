Mission: 
In partnership with our community, Lampson Elementary offers a learning environment of excellence, with high expectations, to 
provide our students with the opportunity to be responsible, resilient, and productive world citizens in a changing and diverse society. 
Lampson Elementary strives to develop student literacy across all content areas as a foundation to inspire each of our students to 
pursue their dreams and develop into tomorrow’s community leaders. Lampson Elementary, a Professional Learning Community, will 
collaboratively examine and analyze student data to guide our instruction leading to continual increased student achievement. 
 
Vision: 
Fostering critical thinkers today to solve tomorrow's challenges. 
 
Lampson Elementary is one of the 27 elementary schools in the Orange Unified School District. The school was built in 1963. It is the 
only Orange Unified school located in Garden Grove, approximately one-mile north of the 22 freeway between Haster and Lewis 
Streets. 
 
At this time, approximately 800 students are enrolled in Transitional Kindergarten through fifth grades. Lampson also has 4 pre-school 
classes, 1 TK class, 2 SDC classes (primary and upper). Lampson operates on a modified-traditional Calendar divided into trimesters. 
The enrollment breakdown by ethnicity: 80% Hispanic/Latino, 11% Asian, 4% White, 0.5% African American, and 4.5% other. Lampson 
is a full-inclusion Title I school with a low SES population of 88%. 64% of the students are classified as English Language Learners. 
 
Examples of programs designed to support a positive learning environment include: a school-wide uniform policy for all students, the 
Multi-tiered System of Support, and school-wide behavior incentives designed to recognize positive behaviors. Lampson Elementary 
has also adopted the PBIS model and implemented our P.A.W.S. motto. This school-wide positive behavior program will help students 
define positive ways to behave and interact with their classmates, teachers, and their community. Our P.A.W.S. motto reflects values 
and conduct that are essential for students to be successful in our 21st Century global economy and be productive global citizens. 
Each letter of the P.A.W.S. acronym represents a behavior expectation that students will follow here at Lampson: P – Practice respect; 
A – Accept responsibility; W – Work hard; S – Safety matters. These behavior expectations are also supported by a P.A.W.S. Matrix 
which explicitly states how each of these behavior expectations should be modeled by students at six locations within the school: 
Classrooms, Lunch Area, Hallways, Playground, Restrooms, and at Assemblies. These behavior expectations are then reinforced by 
positive incentives and rewards which assist students in rapidly internalizing these expectations and progressive discipline practices 
which aim to teach student the correct way to behave. This system of school-wide positive behavior expectations, positive incentives 
and rewards, along with progressive discipline all in turn help to support Lampson’s Multi-Tiered System of Support (MTSS) which aims 
to support the whole child both behaviorally and academically. Our goal is to support and enhance student achievement and safety 
through P.A.W.S. Each Friday, students are rewarded using the Lottery ticket program and small incentives are provided to students 
who exemplify one of our monthly character traits. Lampson uses a wide range of interventions to promote academic, social, 
emotional and communication growth among all learners. The community is invited to participate in our monthly events which are 
publicized in our Parent Community Calendar. A classroom is devoted to Parent/Community Engagement and guest speakers are 
invited in monthly to serve the needs of parents. Career speakers address all 4/5 students monthly as well to promote college and 
career awareness and to encourage students to engage with guest speakers. 
 
One of the most important components is our partnership with our parents and our community. Lampson has a strong tradition of 
supportive, hard-working families and we believe that parents are valuable partners in their children’s education. Our goal is to 
educate parents on important school topics such as curriculum, funding, data and assessment so that they are able to share in the 
leadership decisions impacting our instructional program. Throughout the year parents have the opportunity to develop this 
partnership in a wide variety of ways. Families can join the Parent Teacher Association (PTA), volunteer in classrooms, serve on the 
School Site Council (SSC) and/or English Learner Advisory Committee (ELAC). 
 

2017-18 School Accountability Report Card for Lampson Elementary School 

Page 2 of 13 

 

Lampson students attend Portola Middle School from grades 6 – 8 and Orange High School from grades 9 – 12. 
 
Lampson has 26 regular classroom teachers, 2 SDC teachers who serve the needs of our special education students, 1 educational 
specialist for inclusion,1 RSP teacher who serves the needs of the specific learning disabled students in a pull out/ push in program, 2 
Speech and Language specialists, 2.49 categorically funded resource teachers who work with teachers and students in reading, 
language arts, and language acquisition, one full time district funded instructional coach who works with teachers in a coaching model 
to improve instructional practice. One assistant principal to provide support in testing, discipline, staff development, RTI services and 
other instructional tasks. Support staff include: an adaptive PE teacher (itinerant), a school psychologist (4 days/week), a nurse (3 
days/week), MHC (2 days/week), and classified staff including office staff, custodial staff, and a library/media technician. 
 
The school sits on 11.9 acres. 
 
Instructional minutes provided for Lampson students: K-3 = 50,800; and grades 4-5 = 54,250. 
 
Lampson is on a modified Wednesday schedule, where students are released early each Wednesday to provide teachers with staff 
development for professional growth during their regular work day schedule and professional planning time to review data and 
instructional practices. 
 
Lampson earned the 2016 California Gold Ribbon, 2016 Title 1 Academic Achievement Awards, and Common Sense Certificated School: 
Digital Citizenship for the 2016-2017 school year. At Lampson Elementary, we are committed to creating endless opportunities for our 
students by fostering a student-centered and collaborative culture.