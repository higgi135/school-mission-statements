School Mission Statement: Palmdale School District and Barrel Springs School will be recognized regionally as the finest educational 
organization, offering innovative, academic choices, and opportunities for success in the 21st Century. The mission of the Palmdale 
School District and Barrel Springs School is to provide each child with a rigorous and relevant academic education, a safe learning 
environment and the knowledge, skills and attitudes necessary for success in the 21st Century. 
 
Barrel Springs is a TK/K –5th grade school. It was founded in 1993 and moved to the site located at at 36555 Sunny Lane in Palmdale, 
CA in 2000. In 2005, it was relocated to its final permanent site at 3636 Ponderosa Way. Children are our most valued resource. At 
Barrel Springs, students are given equal opportunities to acquire a comprehensive education that enhances their ability to become 
productive citizens. We encourage children to succeed by creating a secure learning climate that values self-worth and celebrates 
diversity. Our ultimate goal is to prepare our students to become lifelong learners and to have the opportunity to be leaders of a 21st 
century society built on communication and technology. 
 
Barrel Springs is one of 27 schools in the Palmdale School District. Our enrollment for this school year is approximately 700 students 
and we have 22 General Education Teachers, 3 SDC Teachers, 1.5 full time RSP, 2.5 Speech Pathologist, two part-time school 
psychologists, a Parent Community Liaison (PCL), a Student Interventionist and a project clerk. We have a permanent teacher in every 
class and presently have a school-wide average class size of 30 students. Barrel Springs has been outfitted with a Computer Lab that 
contains 38 computers; a library with approximately 10,000 books, 8 computers, and a 4-hour librarian. Each classroom is equipped 
with at least two to three computers,a Promethean Board with document camera and projector. Barrel Springs' students will attend 
school for 180 days with five minimum days during the 2018-2019 school year. We have one TK/Kinder combination class, two full-
day Kindergarten classes and one Kinder/1st grade combination class. All students in grades TK/K, 1st-5th attend school on regular 
school days from 8:16 a.m.-2:54 p.m. On flex Wednesday all students TK/Kinder- 5th grades attend school from 8:16 am- 1:54 pm and 
on minimum school days from 8:16 a.m.- 1:01 p.m. 
 
Barrel Springs provides equitable additional support services which include: a Learning Support Teacher and a Student Interventionist. 
Itinerant support is offered in Occupational Therapy, DIS vision services, and Adaptive Physical Education. Based on our October 2017 
CBED’s report, our student population includes 6% Caucasian, 71% Hispanic, 19.5% African American, 44% English Language Learners, 
less than 1% Filipino, 6% Two or more races, 28% Students with Disabilities, and approximately 86% socioeconomically disadvantaged 
students in significant sub groups.