In September of 1973 Fairlands Elementary school opened with the chosen mascot, the Flyers, 
rooted in local history: the surrounding land, with its scenic hills, had long been a popular choice 
for hot air balloonists. Flash forward forty-five years to a bustling suburb with the school 
surrounded by Stanford Valley Care Hospital, Hacienda Business Park, the 580 freeway, and a 
myriad of single family homes, apartments, and newly built condominiums. Although much has 
changed, our priority at Fairlands remains the same: to serve and grow our students. Fairlands 
serves students in transitional kindergarten through fifth grade and follows a traditional school 
calendar. The student population at Fairlands has remained in the high seven hundreds over the 
last five school years, while the English Learner population has doubled since 2012-2013: from 
10.7% to 23.5% in 2016-2017. In the fall of 2017, 57.2% of the students entering Kindergarten were 
English Learners. The majority of our student body is of Asian decent representing a variety of 
cultures and countries throughout India and Asia. The number of students qualifying for free and 
reduced meals has stayed constant over five years and represents 6% of the total population. In 
addition 6.34% of students qualify for special education services. 
 
Student emotional and academic success is the utmost priority for our fifty-six member staff. Our 
Positive Behavioral Interventions and Supports (PBIS) framework dovetails with the “Community of 
Character” initiative, a partnership between the Pleasanton Unified School District and the City of 
Pleasanton emphasizing the six character traits of responsibility, compassion, self-discipline, 
honesty, respect, and integrity. To expand our PBIS framework in order to create Multi-Tiered 
Systems of Support (MTSS) we have embarked on our first year of implementation of Response to 
Intervention and instruction (RTI2). Consistent blocks of time for intervention and instruction are 
strategically scheduled by grade level. Instruction during this timeframe is designed using student 
data analysis, teacher collaboration, and curriculum planning. 
 
The commitment of our community to work together to support and enhance the learning 
experience of all students is evident in many ways. Our Parent Teacher Association works closely 
with the school to provide numerous family events and curriculum enrichment. Parent volunteers 
within the classroom are a constant. We offer parent education classes, including our School Smarts 
Parent Academy. Together we work to promote a positive and inclusive community honoring the 
diverse cultures and family heritages at Fairlands. 
 
Mission Statement 
Fairlands provides a positive, safe, and rigorous learning environment. We encourage independent 
problem-solving and school to home collaboration, while fostering the whole child's academic, 
social, and emotional development. 

2017-18 School Accountability Report Card for Fairlands Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Fairlands Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

33 

36 

33 

0 

0 

0 

0 

1 

0 

Pleasanton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

600 

11 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Fairlands Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.