Summit Public School: K2 is a public charter school authorized by the W est Contra Costa Unified School District. Summit K2 is located 
in El Cerrito. Summit K2 welcomed its first class of 7th graders in August of 2014, and will continue to add one grade level per year 
until fully grown to grades 7-12. 
 
Our Mission: 
To prepare a diverse student population for success in a 4-year university and to be thoughtful, contributing members of society. 
 
Our Core Characteristics: 
All students and teachers are expected to embody the six core characteristics: respect, responsibility, compassion, curiosity, courage, 
and integrity. 
 
Academic Program 
Every student completes a college-prep course of study that is aligned to the Common Core Standards, and prepares all students for 
AP classes beginning their junior year. 100% of Summit graduates meet or exceed UC/CSU entrance requirements; 100% of Summit 
seniors take the SAT or ACT exam and at least two AP exams. 
 
We focus on the four elements that are the foundation of college and career success in the 21st century: 
 
Cognitive Skills – The deeper learning, critical thinking, communication and problem-solving skills needed to succeed in today and 
tomorrow’s workforce Content Knowledge – Engaging in learning that is personalized for each student, filling learning gaps and moving 
students towards competency in all subject-areas. 
 
Habits of Success – Empowering students to self-direct their learning and develop the habits that are invaluable for college and life 
success Expedition – immersing in real-world experiences to discover and explore passions and careers, applying learning in authentic 
ways. Personalized Learning and Mentoring Program 
 
Every Summit student has a Personalized Learning Plan (PLP), where students set learning and personal growth goals, track progress, 
receive immediate feedback and are able to access learning resources at any time. The PLP is designed to be a dynamic tool where 
families and teachers alike can offer support and coaching. 
 
Summit students have mentors who individually support them through their goals. Mentors serve as college counselors, coaches, 
family liaisons and advocates, ensuring their mentees are excelling inside and outside of the classroom every day. 
 
Our Faculty: 
Our teachers are highly motivated, academically accomplished, innovative and creative. At Summit, teachers participate in 40 days of 
professional development each year, during which they work together to ensure students are always receiving the highest quality 
education. 
 
Extra-Curricular Activities 
 
Participation in extra-curricular activities is essential to developing a well-rounded, college and career-ready student. Summit focuses 
on developing happy, healthy students who have a well-balanced lifestyle that mixes academic classes with personal passions and 
enjoyable extra-curriculars. 
 
 
 

2017-18 School Accountability Report Card for Summit Public School K2 

Page 2 of 17 

 

Teaching and Assessment: 
Summit uses research-based instructional methods and leverages technology in all subjects. Students are immersed in project-based 
learning and learn content and skills by developing solutions to engaging, real-world problems. Instruction is rigorous for all students, 
regardless of previous preparation, and curriculum is differentiated to provide a personal pathway to success. 
 
Assessment 
Summit students demonstrate competency of content knowledge and cognitive skills compared to state, national, and collegiate 
standards. Culminating projects are used to assess the college-ready content knowledge and critical thinking skills. Letter grades are 
assigned for coursework and are consistent with state and college measures. Students take all state standardized exams, Advanced 
Placement Tests, and the ACT and SAT