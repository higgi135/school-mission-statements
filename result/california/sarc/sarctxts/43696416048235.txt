El Carmelo Elementary School, is an inclusive community which embodies a diverse group of students with varied experiences and 
backgrounds. Currently the school serves over 300 students kindergarten through fifth grade. The school serves families from a wide 
range of socio-economic backgrounds and from many difference countries of origin. The school neighborhood encompasses a social, 
economic, cultural, linguistic and ethnic diversity, which is valued by the school community. 
 
El Carmelo staff members implement a balanced educational program that encourages children to grow as individuals and enables 
them to assume responsibility for their role individually and within a group. The children, teachers, support staff, parents, and the 
community all contribute to our mission of educating each child according to their gifts, needs and challenges. 
 
Mission Statement: 
El Carmelo students will develop self-respect, value learning, and cultivate an understanding of others through a school experience 
which fosters high expectations for academic, social, and emotional development in a global community.