We at Ramona Elementary are committed to our students' educational excellence. Our number 
one priority is Student Learning and Academic Growth. With a dedicated and knowledgeable staff, 
working in collaboration with our school families and community, our goal is to provide 
opportunities for Purposeful Learning each and every day. We work as a team to help our students 
gain the confidence and skills necessary to become self-regulated learners; we know that when 
students are able to articulate where they are in their learning path and have a plan for where they 
need to be, the learning becomes a more personal and goal-oriented process. We are focusing this 
year on providing multiple opportunities each and every day for students to engage in rigorous 
thinking and learning and academic conversation about that learning. Ramona Elementary is 1:1 
with Chromebooks or iPads in every classroom and are focused on helping students become 
digitally literate citizens. 
 
Our staff takes great pride in providing a high quality education to all of our students. We are 
committed to delivering quality instruction in all subject areas and to infusing the most effective 
teaching practices to elicit academic growth for our students. 
 
Ramona Elementary has been educating children since 1896. The school site is located in the heart 
of Ramona, adjacent to the District Office and Montecito Alternative High School. There are 
approximately 440 students on campus, housed in 18 classrooms. Our staff also includes three 
Specialized Academic Instructors serving approximately 75 special education students. To meet the 
social and emotional needs of our students, a 50% counselor has been added to the staff; we 
maintain a partnership with communities services such as Vista Hill and Smart Care which provide 
social/emotional instruction, small group sessions and parent interactions. Additional buildings 
include a library, a computer lab, and a multipurpose room. 
 
In addition to the TK-6 program, Ramona Elementary is also the home to the special education 
preschool program and the elementary intensive outpatient program, which provide services to 
students throughout the district. For students who need extra support, we provide an after school 
program that offers homework assistance, and enrichment opportunities in computers, dance, 
science, crafts, gardening, reading support and athletics. In conjunction with Vista Hill/Smart Care 
we have been able to provide counseling for students and social groups to assist students with 
social growth and emotional growth. 
 
The mission of Ramona Elementary School is for each student to grow and excel academically, 
physically, socially and emotionally. We utilize data-driven instruction to ensure that our students 
have the knowledge, skills, confidence and values to become responsible, contributing citizens. We 
use ongoing assessments to measure student growth and guide instruction. We are committed to 
excellence using research-based strategies and work to utilize all available resources to meet the 
needs of our diverse community of learners. 
 
Pixie Sulser 
Principal 
 

2017-18 School Accountability Report Card for Ramona Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ramona Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

23 

25 

25 

0 

0 

0 

0 

1 

0 

Ramona Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

244 

6 

4 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ramona Elementary School 

16-17 

17-18 

18-19