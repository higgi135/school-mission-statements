Vision: BOCS empowers our children to be interdependent scholars, prepares them to be successful 21st Century citizens, and fosters 
leadership for life. 
 
Barack Obama Charter School Mission: 
 
WE RISE: 

Empowering leaders 

• Welcoming family environment 
• 
• Respecting all 
• 
• 
• 

Intrinsically motivated 
Students, staff, community, and family collaboration 
Excelling in performance and character 

 
Barack Obama is a charter that offers families a public education with a private edge. BOCS is a Title I school and receives funding to 
support high need student groups. In addition we are a part of the national free lunch for all programs, serving breakfast, lunch, and 
an after school snack to our students at no charge. Currently, the school sits on the border of Compton and Los Angeles. 
 
BOCS is working to implement trauma informed practices in order to support all students. Additionally, BOCS has adopted Cognitively 
Guided Instruction for math in order to improve student achievement.