Rich Hall is in his fourth year as principal at Cypress Elementary School. Rich has 17 years of 
experience in education, teaching grades fifth through eighth and as a vice principal at a K-8 school. 
 
The Cypress School staff believes in shared leadership that enables all staff members to provide 
input. Teachers assist with staff training, specifically in language arts, math, and best teaching 
strategies. The staff meets weekly in collaboration meetings to evaluate student data in order to 
guide instruction. Staff members take an active role in aligning instructional programs with the 
California Content Standards, and leadership teams guide literacy and discipline policies. The 
School Site Council meets a minimum of five times a year to evaluate and make recommendations 
for Cypress School. 
 
Mission Statement: At Cypress School, everyone's progress contributes to the goal of continuous 
improvement, while progress is recognized, acknowledged and celebrated. 
 
Vision Statement: Cypress School will strive to close the opportunity gap for every student by 
providing a balanced program that will maximize the potential of the whole child. 
 

----

---- 

Redding Elementary School 

District 

5885 East Bonnyview Rd 

Redding CA, 96099 

(530) 225-0011 

www.reddingschools.net/home 

 

District Governing Board 

Bruce Ross - Board President 

Stephen Martinez - Vice President 

Beckie Luff - Clerk of the Board 

Peggy O'Lea 

Nathan Fairchild 

 

District Administration 

Robert Adams 
Superintendent 

Robert Fellinger 

Chief Business Official 

Cindy Bishop 

Dir. Educational Services 

Cindy Trujillo 

Dir. Human Resources 

Tawny Cowell 

Dir. Facilities/Nutrition Services 

Kim Bryant 

Dir. Intervention Services 

Seth Hemken 

Dir. Technology 

 

2017-18 School Accountability Report Card for Cypress Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cypress Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

9 

1 

0 

9 

0 

0 

9 

0 

0 

Redding Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

143 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Cypress Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.