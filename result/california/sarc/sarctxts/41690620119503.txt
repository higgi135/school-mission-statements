Everest Public High School is a public charter school located in Redwood City and authorized by the Sequoia Union High School District. 
 
Our Mission: 
To prepare a diverse student population for success in a four-year university and to be thoughtful, contributing members of society. 
 
Our Core Characteristics: 
All students and teachers are expected to embody the six core characteristics: respect, responsibility, compassion, curiosity, courage, 
and integrity. 
 
Academic Program: 
Every student completes a college-prep course of study that is aligned to the Common Core Standards, and prepares all students for 
AP classes beginning their junior year. 100% of Everest graduates meet or exceed UC/CSU entrance requirements; 100% of Everest 
seniors take the SAT or ACT exam and at least two AP exams. 
 
We focus on the four elements that are the foundation of college and career success in the 21st century: 
 
Cognitive Skills – The deeper learning, critical thinking, communication and problem-solving skills needed to succeed in today and 
tomorrow’s workforce 
 
Content Knowledge – Engaging in learning that is personalized for each student, filling learning gaps and moving students towards 
competency in all subject-areas 
 
Habits of Success – Empowering students to self-direct their learning and develop the habits that are invaluable for college and life 
success 
 
Expedition – Immersing in real-world experiences to discover and explore passions and careers, applying learning in authentic ways. 
 
Personalized Learning and Mentoring Program 
Every Everest student has a Summit Learning Plan (SLP), where students set learning and personal growth goals, track progress, receive 
immediate feedback and are able to access learning resources at any time. The SLP is designed to be a dynamic tool where families 
and teachers alike can offer support and coaching. 
 
Everest students have mentors who individually support them through their goals. Mentors serve as college counselors, coaches, 
family liaisons and advocates, ensuring their mentees are excelling inside and outside of the classroom every day. 
 
Our Faculty: 
Our teachers are highly motivated, academically accomplished, innovative and creative. At Everest, teachers participate in 40 days of 
professional development each year, during which they work together to ensure students are always receiving the highest quality 
education. 
 
Extra-Curricular Activities: 
Participation in extra-curricular activities is essential to developing a well-rounded, college and career-ready student. Everest focuses 
on developing happy, healthy students who have a well-balanced lifestyle that mixes academic classes with personal passions and 
enjoyable extra-curriculars. 
 
 
 

2017-18 School Accountability Report Card for Everest Public High School 

Page 2 of 17 

 

Teaching and Assessment: 
Everest uses research-based instructional methods and leverages technology in all subjects. Students are immersed in project-based 
learning and learn content and skills by developing solutions to engaging, real-world problems. Instruction is rigorous for all students, 
regardless of previous preparation, and curriculum is differentiated to provide a personal pathway to success. 
 
Assessment: 
Everest students demonstrate competency of content knowledge and cognitive skills compared to state, national, and collegiate 
standards. Culminating projects are used to assess the college-ready content knowledge and critical thinking skills. Letter grades are 
assigned for coursework and are consistent with state and college measures. Students take all state standardized exams, Advanced 
Placement Tests, and the ACT and SAT .