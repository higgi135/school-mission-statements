Alvarado Intermediate School is an academically oriented school that gets results. Student achievement scores are strong due to the 
high quality of instructional programs and the efforts expended by students, parents, and staff. Learning is an exciting adventure, 
rather than a tedious exercise. Students actively explore critical ideas by creating, constructing, designing, producing, and performing. 
The school’s programs are student-oriented and directly address their needs. Staff members recognize the unique needs of middle 
school students and provide a nurturing environment where youngsters can find safety, security, and structure along with rich 
opportunities to explore and excel. 
 
Alvarado Intermediate is a recognized and award-winning "School of Excellence" on a national and state level. In 1996, Alvarado was 
one of 266 public and private secondary schools (grades 7-12) selected by the US Department of Education to receive the coveted 
National Blue Ribbon School Award. Also, In 2004, 2007, and again in 2010, Alvarado was recognized as a top middle school in the 
State, earning the distinction of A Model Middle School- A California School to Watch, Taking Center Stage recipient. Most recently 
Alvarado Intermediate also received the California Gold Ribbon School Award in 2017. 
 
Alvarado Intermediate believes that: 
 
1. All students are gifted and talented 
 
2. All students have futures 
 
3. Everyone needs a teacher 
 
4. Every day is an opportunity to be the greatest version of me!