The Albany Park School Professional Learning Community prides itself in the high achievement of our students and a commitment to 
excellence. At Albany Park, our focus is not only on academics, but also on building better humans who are healthy, happy, well-
behaved and responsible. Albany Park School is one of eight elementary schools in the Delano Union School District and is situated in 
the Northwest section of Delano. The school is surrounded by single family dwellings and a city park. Albany Park is home to 
approximately 340 students Kindergarten through fifth grade, has 14 classroom teachers, one mild/moderate teacher, one band 
teacher, and one site resource teacher. Albany Park also has the services of a school psychologist, speech therapist, school nurse, and 
a full-time Vice Principal. 
 
Albany Park has worked diligently to close the achievement gap of our English Learner (EL) students which represents 157 students 
(46%) of our total student population. We have 1 Foster youth student (less than 1%) and 22 students (6%) in special education 
(resource specialist program). Nine (3%) students are in Speech only. In addition, we have 18 students in the GATE program (5%) and 
24 students in the Migrant Education program (7%). Our area of focus for the 2017-2018 school year was English Language 
Development through designated and integrated ELD instruction, and Reading fluency and comprehension through phonics instruction 
and small group reading instruction. In addition PBIS implementation (now called MTSS) was also one of our goals for behavior. Our 
goal was to continue closing the achievement gap between our EL students and our school-wide population of students as well as 
develop a plan for positive behavior expectations on our campus. Our new goals for the 2018-2019 school year are stated in this 
document. 
 
At Albany Park, we as an entire school staff are dedicated to meeting the needs of every “Panther”. We are focused on learning, 
embody a collaborative culture and are focused on results. Teacher Teams work together to design standards based, grade level 
appropriate lessons that are grounded in research based best practices, techniques and strategies. Learning targets are delineated; 
lessons are taught to mastery and assessed regularly both formally and informally. If students struggle to master essential learning 
targets, a Pyramid Response to Intervention is employed. 
 
The Albany Park School Mission Statement is "Our students will acquire a strong foundation for their future college and career 
endeavors."