Profile 
At Lopez Elementary School, we strive to create an environment that maintains high expectations and gives every student the attention 
necessary for high academic achievement. At Lopez Elementary School we offer our students a 21st Century curriculum in a state-of-
the-art learning environment. 
 
Vision 
Through high-quality instruction and the use of instructional technology, Lopez Elementary staff will provide a differentiated 
instructional program that will address each student’s needs in a welcoming environment of respect, responsibility, and results. 
 
Lopez Elementary School Community will work together collaboratively to develop in each student the abilities to become successful 
socially responsible, self-empowered achievers and lifelong learners in a Multi-ethnic and diverse 21st-century society. 
 
Mission 
The mission of López Elementary School is that students receive a comprehensive, sequential curriculum, aligned with the State 
Standards. Our mission is that students demonstrate proficiency through ongoing school site, district, and State assessments. The 
López Elementary School Community will ensure that every student is given the opportunity to learn, master and exceed grade level 
standards. 
 
The mission of López Elementary School is to create an accountability system of curriculum, instruction, and assessment based on a 
thorough analysis, alignment, and communication of the State Standards, which will be demonstrated by increased student academic 
performance on the state content standards as demonstrated by state, district, and school site assessments. 
 
Focus Areas 
1. ELA – Reading comprehension and writing 
a. Provide focused small group instruction based on student needs 
b. Engage in PLCs on a regular basis to analyze student data examine student work to inform literacy and writing instruction. 
 
2. English Language Learners 
a. Implement ELD rotations with flexible groupings on a daily basis. 
b. Use the ELD Student Profile to monitor and document student progress and as an instructional planning tool. 
c. Increase opportunities for English Learners to read, write, listen and speak in all classes throughout the day. 
3. Mathematics 
a. Implement daily Number Talks 
4. School Climate 
a. Increase attendance and/or reduce tardies. 
b. Increase positive student behavior. 
 
 
 

2017-18 School Accountability Report Card for Lopez Elementary School 

Page 2 of 11