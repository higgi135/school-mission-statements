Calpella is a preschool though fourth grade school that serves a diverse group of families. We have 
20 regular classrooms and one State Preschool. The staff of 31 certificated, highly qualified 
personnel, including three reading resource teachers, a student success coordinator, a speech 
therapist, a social emotional counselor, a counselor to serve our Native American students, a school 
psychologist, a PBIS coordinator, and a state preschool teacher, dedicated to giving all students the 
opportunity to succeed. The 40 classified staff, which includes paraprofessionals, secretaries, 
custodians, and a garden technician, are all very supportive in the pursuit of academic success. Over 
80% of our students come from socio-economically disadvantaged families. 
 
School Vision and Mission 
Our mission at Calpella Elementary is to promote and maintain a safe supportive environment that 
encourages the success of the individual and involves the collaboration of the staff, family and 
community. Our staff cultivates a philosophy of learning as a life-long process. The school is 
committed to high academic expectations for all students while recognizing the necessity for safety 
nets to meet individual needs. The staff will teach and model universal principles that build 
character, encourage honor, and celebrate the diversity of individual. 
 

 

 

-
-
-
-
-
-
-
- 

----

---- 

Ukiah Unified School District 

511 South Orchard Ave. 
Ukiah, CA 95482-3411 

(707) 472-5000 
www.uusd.net 

 

District Governing Board 

Megan Van Sant 

Anne Molgaard 

Gail Monpere 

Beatriz " Bea" Arkin 

Carolyn Barrett 

Zoey Fernandez 

Tyler Nelson 

 

District Administration 

Debra Kubin 

Superintendent 

 

2017-18 School Accountability Report Card for Calpella School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Calpella School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

28 

27 

25 

2 

0 

3 

0 

1 

0 

Ukiah Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

335 

22 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Calpella School 

16-17 

17-18 

18-19