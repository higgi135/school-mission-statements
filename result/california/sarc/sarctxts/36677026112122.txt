SCHOOL MISSION 
Our mission is to assist the home in preparing each child to become a self-fulfilled productive citizen by promoting high levels of 
academic and social achievements in a positive environment. We serve grades TK through 5th grade. We take the most pride in Engage 
to the Power of 3 equals Success. We want students to engage with teachers in small group instruction, peers through cooperative 
learning, and text through RACER writing strategies. 
 
DISTRICT & SCHOOL PROFILE 
Etiwanda School District serves over 14,000 TK-8 students residing in the cities of Rancho Cucamonga, Fontana, Alta Loma, and 
Etiwanda. The district currently operates thirteen TK-5 elementary schools and four intermediate schools (grades 6-8) a Community 
Day School, and the EASE homeschool program. Etiwanda’s graduating eighth-grade students are served by Chaffey Joint Union High 
School District for grades 9-12. Homeschooling program, preschool program, and childcare are provided at some schools within the 
district. More information is available on the district website or by contacting the district office at (909) 899-2451. 
 
The district’s commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high-quality 
educational program. Etiwanda School District appreciates the outstanding reputation it has achieved in local and neighboring 
communities. Consistent success in meeting student performance goals is directly attributed to the district’s energetic teaching staff 
and strong parent and community support. 
 
Terra Vista Elementary is a neighborhood school in a planned community located in the southwest quadrant of the district boundaries. 
Approximately 980 TK-5 students residing in the city of Rancho Cucamonga are enrolled. Terra Vista Elementary School has an ongoing 
tradition of academic excellence.