Baldy View Elementary, built in 1954, is located in Upland, California at the base of the San Gabriel 
Mountains. Backed by Mt Baldy, hence our name, families are welcomed with open arms. We are 
one of 10 elementary schools in our district. We have 22 classrooms, which service Transitional 
Kindergarten through 6th grade students from our community. 
 
Our School Mission is: “The Baldy View students and staff pride themselves on being a community 
of learners with a strong commitment to reaching their full potential. The expectation is that 
students and staff meet academic standards, recognize and appreciate individual and cultural 
differences, and effectively learn to resolve problems that continuously challenge a global society. 
Students, as well as adults, collaboratively develop skills to become lifelong learners.” Baldy View’s 
staff is also committed to our District mission as well: “Upland Unified School District prepares and 
inspires all students to maximize their academic potential and to thrive in a complex global society." 
 
Each year we strive to better our programs for student learning. We are excited to continue our 
school wide implementation of Thinking Maps ( A common Language for Teaching and Learning), 
Daily 5/CAFE reading strategies and structure for Literacy for grades K - 3, and AVID strategies in 
grades 4 - 6. Teachers have ongoing training opportunities in these two initiatives. In the 2017-
2018, year the campus received a Silver Award in observance of our implementation of PBIS 
(Positive Behavioral Interventions and Supports) across the campus. Baldy View received a grant 
and district support in purchasing ST Math, an online "Spatial and Temporal" math program for 
grades K - 6, which is still in use and offers students experiences in problem solving and deductive 
thinking. To support literacy, Baldy View has a reading lab, led by a Reading Specialist, that offers 
both push-in and pull-out opportunities to support students in areas of phonemic awareness, 
phonics, vocabulary, and comprehension. In addition to the lab and our teaching staff, an RSP 
Teacher, Librarian, Instructional Aides, and Bilingual Aides provide the extra support needed in the 
classrooms where students require more assistance to improve their skills. 
 
With the generous support of our community and the adoption of Measure K, during the summer 
of 2011, we were able to increase our high level of learning by creating the “21st Century 
Classroom” throughout our campus. This classroom model includes an electronic teaching wall 
with interactive boards, projection/audio systems, and skylights. Currently, Grades K - 6 are one-
to-one ratio with Chromebooks and teachers throughout the campus work through Google 
Classroom. Additionally, teachers will check out the district Google Expedition kit to take students 
on virtual field trips to mountain ranges and libraries beyond our state, country, and even 
continent. 
 
Baldy View has provided many after school clubs for enrichment over the past few years. Some of 
the clubs that have been offered include: homework, garden, Coding & Chess, and math 
enrichment. We are excited to offer these programs, directed by teachers, support staff, and local 
consultants throughout the week for minimal or no cost to parents. Think Together is an after 
school program established through state funds to keep kids safe after school. This program has a 
maximum of 120 students with 5 trained adults. Think Together meets every school day from the 
end of the day until 6:00 p.m. During the afternoon, students work on homework for one hour as 
well as take part in additional enrichment programs. 
 
 
 

2017-18 School Accountability Report Card for Baldy View Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Baldy View Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

28 

25 

24 

0 

0 

0 

0 

0 

0 

Upland Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

495 

10 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Baldy View Elementary School 

16-17 

17-18 

18-19