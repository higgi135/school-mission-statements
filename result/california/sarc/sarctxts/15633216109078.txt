Thorner Vision: Dr. Juliet Thorner Elementary school cultivates a rich environment full of traditions, high expectations for student 
success, strong collaboration between teachers, and parent involvement while celebrating our many achievements. Thorner School’s 
goal is to challenge all students to reach their personal best through academics, sports, character building and performing arts. The 
entire Thorner community takes pride in accomplishments, distinguished awards, and academic achievement as we celebrate the 
uniqueness of each student. 
 
Thorner Mission: To have all students perform at or above grade level in English Language Arts and Mathematics. 
 
Thorner Motto: To succeed with pride and dignity.