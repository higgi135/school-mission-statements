School Motto: Dream, Believe, Achieve & Lead 
 
Vision: Unlocking the potential of the world’s future innovators, inventors, dreamers, and leaders 
in a technology-rich environment. 
 
Santee’s Mission: To develop the resourcefulness, resilience, and creativity necessary to be 
successful in today’s and tomorrow’s world, each child will engage in relevant project-based 
learning that incorporates science, technology, engineering, arts, and mathematics. Students will 
see themselves as community advocates and agents of change, who can positively impact their 
community. 
 
Core Instructional Program: 
Santee is a proud recipient of an Apple grant through the White House ConnectED initiative. By 
giving K-6 students access to the latest technology and powerful learning tools, Santee staff is 
working to transform the classroom into a place of deeper exploration and creativity. Together, 
Santee staff can unlock the potential of the world's future innovators, inventors, dreamers, and 
leaders. 
 
K-3 SEAL classrooms: Santee's SEAL classrooms bring to life the rigor and richness called for by the 
Common Core Standards. Language and literacy education is woven into all aspects of the day. 
Children use high-level, complex language to talk about what they are learning. Students actively 
collaborate, solve problems, and engage in whole- and small group activities. Teachers model rich, 
expressive language and create environments where academic vocabulary and concepts come to 
life. 
 
Santee offers a variety of programs that include: small group intervention for struggling readers in 
all grades during the day, extended day kindergarten program, educational and cultural 
Enrichment, After School CORAL Program (K-3rd grade), Homework Club (4-6th grade), Computer 
Lab (Before, during & after school), Progressions music class (2-3rd graders), Band (4-6th grade), 
and Kids Club. 
 
Positive School Climate 
We believe significant learning occurs with meaningful relationships. 
School Wide behavior expectations: Be Safe, Be Respectful, Be Responsible, Be Kind 
No PLACE FOR HATE SCHOOL: Anti-Bullying campaign and character development education 
Structured activities during recess and lunch time that teach students conflict resolution strategies 
and good sportsmanship 
 
Caring Adults: Staff dedicating their time, interest, attention, and emotional support to students 
 
 
 
 
 

----

---- 

Franklin-McKinley Elementary School 

District 

645 Wool Creek Drive 

San Jose CA, 95112 

(408) 283-6000 
www.fmsd.org 

 

District Governing Board 

Rudy Rodriguez, Board President 

George Sanchez, Vice President 

Maimona Afzal Berta, Board Clerk 

Kerry Rosado, Board Member 

Thanh Tran, Board Member 

 

District Administration 

Juan Cruz 

Superintendent 

Jason Vann 

Assistant Superintendent, Business 

Services 

Paula Boling 

Assistant Superintendent, Human 

Resources 

Dr. Norma Martinez Palmer 

Assistant Superintendent, Educational 

Services 

Karen Allard 

Director, Curriculum and Instruction 

Dr. Mariam Galvarin 

Director, Special Education 

Taylor Nguyen 

Administrator, Assessment and Special 

Projects 

Hung Nguyen 

Director, Technology Services 

Melinda Waller 

Director, Early Learning 

Jennifer Klassen 

Coordinator, Student Wellness and 

Support Services 

Joanne Chin 

Director, Fiscal Services 

Veronica Lanto 

Director, Maintenance and Auxiliary 

Services 

Marcela Miranda 

Director, Human Resources 

 

2017-18 School Accountability Report Card for Santee Elementary School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Santee Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

20 

24 

22 

0 

0 

0 

0 

0 

0 

Franklin-McKinley Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

.5 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Santee Elementary School 

16-17 

17-18 

18-19