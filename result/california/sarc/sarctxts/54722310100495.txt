It is the mission of Los Tules Middle School to ensure that all students are provided with a safe and 
positive learning environment. We are committed to providing students with an equitable and 
challenging standards-based education. Los Tules School will develop a social and moral conscience 
that will enable students to make good decisions in life. All students will be encouraged to continue 
their education as they complete their middle school years and set their sites upon high school, 
college, and career. 
 
Annual Schoolwide Plan Review 
Los Tules has implemented a rigorous intervention program during the school day. Based upon test 
scores, students are scheduled into a math intervention class or a reading intervention class to 
assist them in learning the skills necessary to meet academic standards. Also, students are 
encouraged to attend the before / after school tutorial classes that are available. 

 

 

----

---- 

----

---- 

Tulare City School District 

600 North Cherry Street 

Tulare, CA 93274 
(559) 685-7200 
www.tcsdk8.org 

 

District Governing Board 

Teresa Garcia 

Irene Henderson 

Melissa Janes 

Phil Plascencia 

Daniel Enriquez 

 

District Administration 

Dr. Clare Gist 

Superintendent 

Philip Pierschbacher 

Assistant Superintendent, 

Personnel 

Joyce Nunes 

Assistant Superintendent, 

Business/Psychological Services 

Paula Adair 

Assistant Superintendent, Student 

Services 

Brian Hollingshead 

Assistant Superintendent, 
Curriculum/Technology 

 

2017-18 School Accountability Report Card for Los Tules Middle School 

Page 1 of 9