Our school mission and highest priority is the quality education of our students so that they may be better equipped to achieve their 
life's goals and desires. At Miwok Valley Elementary Charter School we nurture our students, help build life-long friendships, and 
empower them to be independent and responsible thinkers, giving them skills to become citizens that can impact the world around 
them in a positive manner. 
 
Our school vision states, "Miwok Valley is the CORE of the community. We focus on language acquisition, giving literacy support for a 
diverse group of students. Through integrated, cross curricular projects focused on science, social studies, & language arts, we teach 
children to examine their community, physical environment, & place in the world. Offering unique experiences & building character 
are cornerstones of a Miwok education. In our multilingual neighborhood, we are the CORE of the community." 
 
We use the word "core" as an acronym to enact our school's mission and vision. CORE stands for the following: Community- creating 
a spirit of inclusion and belonging, Opportunity- students have opportunities to explore the world through service and adventure, 
Respect and Responsibility- our community honors the learning process and one another, Excellence in academics- students learn 
through a rigorous education and the joy of learning.