A Message from the Principal 
 
Aloha welcomes this opportunity to tell you more about our school. This year we have continued 
our focus on improving student achievement in reading, writing, and math. Our goal is for all 
students to meet or exceed the California Content Standards in English/Language and Math, as well 
as make growth along their academic journey here. In accordance with Proposition 98, every school 
in California is required to issue an annual Accountability Report Card. 
 
Aloha was awarded a federal magnet program grant during the 2010-2011 school year and has a 
health and medical theme. The federal grant awarded the school site with $1, 500,000.00 to create 
thematic units related to health and medicine, upgrade the school facility, and create a diverse 
school environment. The school has a science lab with a credentialed science teacher who provides 
science and health lessons, and utilizes a variety of technology with students. The goal is to increase 
student academic achievement, increase enrollment, and expose students to professions in the 
health and medical industry. 
 
The Aloha staff is dedicated to the academic success of each of our students. Each student has 
access to a standards-based curriculum in English language arts, mathematics, science and social 
science. We also know that each child is unique and we celebrate the diversity of our school and 
community. We welcome parent involvement as an important element of student achievement. A 
safe and orderly school environment and high expectations for behavior and achievement is a goal 
we strive to attain. School wide implementation of our site Positive Behavioral Intervention and 
Supports (PBIS) initiative promotes a caring community where students are safe to learn and grow. 
The Aloha staff knows that good teaching is the most important factor influencing student 
achievement. The skilled staff is committed to on-going professional learning focused on our goal 
of improved student achievement in all areas. Teachers regularly participate in professional 
learning in technology, Positive Behavioral Intervention and Supports (PBIS), and curriculum and 
instruction. In addition, we analyze assessment data and meet regularly as teams to examine the 
effectiveness of our teaching practices. Here at Aloha, we are very proud to be a part of a learning 
community with an on-going tradition of academic excellence. 
 
Children as Priority 
 
At Aloha, students, parents, staff, district, and the community work together to ensure that each 
student receives a rigorous, comprehensive, balanced and integrated educational program in a safe 
and caring environment. All children have the opportunity to develop their ability to think critically, 
solve problems, communicate effectively, work independently and collaboratively, take risks, make 
decisions, be creative and help others. With the children as our priority, the staff will continuously 
reflect on our teaching practices, share ideas and concerns honestly, communicate regularly with 
parents and community, and model a lifelong joy of learning through our own personal and 
professional growth. 
 
Our goal is to provide educational experiences that will: 

• Promote a literate student body 
• Promote a responsible confident attitude 
• 
• Develop a strong sense of right and wrong 

Establish an intrinsic desire for life-long learning 

2017-18 School Accountability Report Card for Aloha Health Medical Academy 

Page 1 of 10 

 

• 
• 
• 

Encourage students to accept new challenges and risk failure 
Encourage students to pursue academic excellence 
Teach students to value individual differences 

The entire staff is dedicated to providing a classroom environment that promotes and reinforces a safe and orderly campus so that each 
student can attain his or her potential. 
 
Aloha Health Medical Academy is located in the city of Lakewood and serves approximately 350 students in grades kindergarten through 
six on a traditional calendar system. Aloha is dedicated to ensuring the academic success of every student and providing a safe and 
comprehensive educational experience. Aloha offers a free after school child care program facilitated by the YMCA which serves 
approximately 100 students daily. 
 
Aloha Health Medical Academy is a school community where the highest expectations are maintained. Aloha utilizes all available resources 
to enable students to become lifelong learners who possess the ability to achieve their utmost potential. We promote a safe, nurturing, 
and stimulating environment that invites students to actively participate in educational opportunities. Aloha provides a welcoming 
atmosphere in which parents and community members are encouraged to become immersed in students' educational endeavors. We 
work collaboratively to meet the individual academic and social needs of staff and students, creating a desirable environment where all 
participants are successful. 
 
Mission Statement 
 
Aloha is a safe, encouraging learning environment that supports the behavioral and academic needs for all students through positive 
actions; where the students, community, and staff exhibit respect, responsibility, and safety.