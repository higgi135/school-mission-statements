Principal’s Message 
 
For the students and staff that walk the campus daily, Live Oak High School is “home”. Since its 
inception in 1922, Live Oak High School has been a city landmark and a community hub for the 
citizens of Live Oak California. Replete with a long history of tradition and accomplishments, Live 
Oak High graduates are well equipped to lead productive and successful lives in their personal and 
global communities. Indeed, many graduates choose to stay in the Live Oak area to live and raise 
their families, and they not only give back to their school, but they also become integral 
stakeholders in the procedures and processes of Live Oak High School. Academically, over the past 
decade, LOHS has received many prestigious awards such as the Title I Academic Achievement 
Award for two years in a row, a Bronze classification by the US News and World Report for achieving 
above the state average on the CAHSEE, and in 2009, was honored to be a California Distinguished 
School. Athletically, the sports teams have a long tradition of having strong, winning programs. 
LOHS also offers many extra and co-curricular opportunities as well. Performing Band and Choir, 
Drama, Spanish Honor Society, Key Club, Associated Student Body (ASB), Friday Night Live, 
Environmental Club, Block L.O., California Scholastic Federation (CSF), and Future Farmers of 
America (FFA). As stated earlier, Live Oak High is “home”, and “home” is a great place to be! Go 
Lions! 
 
District Mission Statement 
 
The mission of the Live Oak Unified School District, an ethnically diverse community, is to graduate 
students who have high moral and ethical standards, an enthusiasm for learning, and who are 
equipped with the skills and knowledge necessary to excel in a global society, through the 
implementation of a rigorous and meaningful curriculum that addresses individual differences, 
integrates technology, and is delivered in partnership between an effective staff and the 
community. 
 
Vision Statement: 
 
Live Oak High School is dedicated to prepare critical thinkers to face the future by cultivating 
excellence in every graduate through high academic expectations, ethics, personal success, and 
community involvement. 
 
Mission Statement: 
 
Live Oak High School, through a safe, caring, and supportive environment, in partnership with 
families, highly qualified staff, and the small community is committed to graduate all students 
while: 

• providing a rigorous academic environment through innovative college and career 

• 
• 

readiness programs; 
inspiring lifelong learners, ethical, self-motivated, and caring citizens; 
fostering creativity, critical thinking, and technological literacy for success in an ever-
changing world; 

• promoting school spirit and pride in our rural community; 
• 

and embracing diversity. 

Tony Walton, Principal 

2017-18 School Accountability Report Card for Live Oak High School 

Page 1 of 10