Principal's Message 
Sellers Elementary is known for high academic standards as we teach our students to be 
independent thinkers and productive members of our society. We model cooperation as we work 
together with parents and community for the success of every child. Our PTA and parent volunteers 
provide wonderful support in an effort to create and maintain a positive learning environment and 
promote success. The Sellers PTA is a vital factor in our school success. Our PTA provides a variety 
of events such as field trips, assemblies, and classroom help to keep our school special. 
 
Sellers staff provides a warm and caring atmosphere while striving for each child to reach their 
academic potential. Sellers Elementary School is comprised of a staff dedicated to the success of 
the student. It is a student-centered, standards-based educational environment where learning, 
citizenship, personal and social development are key elements. 
 
I encourage you to look at our school website by logging on to the Glendora Unified Schools site 
and going to the ‘Our Schools’ link. On our web site, you will be able to see the wonderful events 
that are taking place at Sellers as well as important information about our school. It is our Sellers 
community that makes it such a wonderful place. 
 
If I can assist you or your child in any way possible, please call me at (626) 852-4574 or email me at 
sbishop@glendora.k12.ca.us. 
 
Steven Bishop 
Principal 
 
Mission Statement 
Sellers Elementary School, through a partnership of students, parents, staff, and community, is 
committed to providing a challenging education that enables all students to reach their full 
potential, become independent thinkers, and demonstrate responsibility toward others. 
 

2017-18 School Accountability Report Card for Sellers Elementary School 

Page 1 of 7