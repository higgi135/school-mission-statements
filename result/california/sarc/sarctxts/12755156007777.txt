Mission Statement 
Grant School, an innovative educational community dedicated to excellence, prepares our students to be responsible citizens in this 
global society by engaging them in a creative and diverse curriculum that promotes exploration and celebrates learning. 
 
School Profile 
At Grant Elementary School, we emphasize high academic standards while supporting the social and emotional development of our 
students. We promote academic growth, focusing on written language and the development of higher level thinking. Our district has 
adopted Everyday Math and as a site we are focusing on implementing the Eight Mathematical Practices. We provide hands-on science 
instruction with our “Nature Trail” and implementation of the Next Generation Science Standards. We partner with Friends of the 
Dunes, Mad River Hatchery, Wolf Creek Educational Center, Eel River Estuary Preserve, and Redwood Environmental Education Fair 
to provide rich learning experiences for our students. Grant has also started a school garden that our students and staff are very 
excited about. Our teachers are committed to professional development and attend various trainings. Grant is in the process of 
becoming a schoolwide AVID Elementary and 2nd-5th grade teachers have attended AVID Institutes and are implementing AVID 
strategies in their classrooms. The AVID team meets monthly and the focus for 2018-19 is on providing students supports in learning 
note-taking, reflecting on their learning, and organizational skills. Grant's future goal is for all teachers to have the opportunity to 
attend the summer institute and incorporate AVID into all classrooms. Many staff members are also involved in the North Coast Arts 
Integration Project or the CREATE Grant in which they are working with instructional coaches to integrate the arts into the core 
curriculum. 
 
Major Achievements 
Positive Behavior Intervention & Supports for all students, Second Step Pro-Social Skills Curriculum for all students, Response to 
Instruction & Intervention in ELA , AVID Elementary 
 
Focus for Improvement 
Our focus for improvement this year is writing across the curriculum. We want to ensure that students can explain their thinking, 
written and verbally, and also try to understand the thinking of others. 
 
We will: 

 
 

 

 
 

 

Schoolwide writing opinion writing focus with common rubrics. 
Continue to use literacy assessments early in the year to provide students struggling in reading with support and to monitor 
their progress. Continue a school-wide academic intervention model utilizing our 1.5 Reading/EL Intervention Teachers, 
Literacy Techs, and classroom teachers. Within this model teachers collaborate regularly to make academic decisions about 
student learning and determine best practices for differentiating instruction. 
Implement schoolwide strategies to improve English learners’ academic and English language development as well as their 
reading comprehension. 
Increase technology/computer use in each classroom by students. 
Expand the use of research-based academic support throughout the school day to improve students’ reading skills and ensure 
our District Instructional Norms are incorporated into our daily instructional practices schoolwide. 
Continue to support each student’s personal, social, and emotional growth. 

 
We are committed to offering a complete educational experience at Grant. Our After School program provides enriching academic, 
sports and recreational activities. We make every effort to accommodate working parents. 
 
We personally extend an open invitation to all of you as we enjoy meeting new families and are committed to the education of each 
and every student at Grant School. We are a community of collaborative lifelong learners. 
 

2017-18 School Accountability Report Card for Grant Elementary School 

Page 2 of 11