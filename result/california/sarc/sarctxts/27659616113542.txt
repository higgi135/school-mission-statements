Our mission at César E. Chávez Elementary School is to nurture and teach students so that they will 
become confident, critical thinkers who possess a deep sense of responsibility to self and to their 
community. By creating a safe learning environment, our students will engage in a rigorous learning 
environment and learn to effectively communicate and collaborate with others. We encourage the 
development of technological and bi-lingual skills in order to keep up with the world's constant 
change and instill in them the appreciation of diversity by promoting positive interactions between 
all students, and encourage them to pursue their lifelong dreams. 
 
César E. Chávez Elementary School serves approximately 800 students from preschool through 
sixth grade. Our student population includes 98.5 percent Hispanic students and 85 percent English 
Learners. 
 
The Chávez teaching staff is committed to collaboration and articulation within and across grade 
levels. Our teaching staff is well trained to meet the needs of our English Learners as well as our 
English-speaking students. 
 
Our parent teacher committee (PTC) is very active; it closely partners with the school and 
community to work on projects during the school year. The School Site Council and Site Leadership 
Team meet monthly to review student achievement data, analyze the effectiveness of programs, 
determine the progress of actions in the school plan, and revise the school plan as needed. 
 
Every member of the Cesar E. Chavez School community works together to provide each child with 
a successful school experience. 
 

----
---- 
Alisal Union School District 

155 Bardin Road 
Salinas, CA 93905 
(831) 753-5700 
www.alisal.org 

 

District Governing Board 

Fernando Mercado, President 

Guadalupe Ruiz Gilpas, Vice 

President 

Robert Ocampo, Member 

Noemí M. Armenta, Member 

Antonio Jimenez, Member 

 

District Administration 

Dr. Héctor Rico 
Superintendent 

Mr. James Koenig 

Associate Superintendent, 
Business and Fiscal Services 

 

Mr. Quoc Tran 

Assistant Superintendent, 

Educational Services 

 

Mr. Ricardo Cabrera 

Associate Superintendent, 

Human Resources 

 

Dr. Jairo Arellano 

Assistant Superintendent, 

Whole Child Services 

 

 

2017-18 School Accountability Report Card for Cesar E. Chavez Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cesar E. Chavez Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

27 

31 

26 

7 

0 

5 

0 

2 

0 

Alisal Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

5 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.