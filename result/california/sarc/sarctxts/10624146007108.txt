Centerville's Vision is that Chargers will be compassionate, life-long learners by: communicating effectively, taking risks, and thinking 
critically to be prepared toward achieving college and career readiness. Teachers will support them by ensuring that Teachers will 
work collaboratively, set high expectations, and employ instructional practices that ensure student success. 
 
Centerville (CV) is located 16 miles east of Fresno, CA. It is a small town east of Sanger with a population of about 400. Centerville 
Elementary serves 280 students in a K-6 setting. One hundred percent of our students participate in the Free or Reduced-Priced Lunch 
Program. Approximately 25% of students are considered English Language Learners. The percent of Hispanic or Latino students is 58%, 
2.8% Asian, 32.8% White, and 2.4 Multiple. We service 6% students with disabilities and 1% migrant. 
 
We set high standards for our staff, students and community that go beyond academic success, standards which require everyone 
involved to set examples that the community can observe and respect. We are a community of caring! With the loving guidance of 
dedicated staff, our children make cross-curricular connections, while developing persistence and personal responsibility, qualities 
that are the foundation of academic excellence. This is evidenced in the classrooms where the Community of Caring (CoC) values are 
proudly displayed alongside the State content standards. While this application explains aspects of our school that deal with 
personal/social development and academic achievement, we further recognize that it is the commitment of caring people that 
provides is the cement that binds all else together. 
 
Parents are actively involved and serve in leadership capacities through the School Site Council (SSC) and the Parent Teacher 
Association (PTA). Parent volunteers are visible daily, providing support and assistance throughout the school. We have at least ten 
volunteers a day! 
 
Being committed to academic excellence, we offer no excuses for failure to achieve. While acknowledging problems and obstacles, 
our focus is on finding solutions through our Response to Intervention (RtI) services. Regular Professional Learning Committee (PLC) 
meetings and articulation across and amongst the grade levels allow us to plan and pursue academic goals, as we are constantly 
mindful of the fact that achievement is limited only by the size of our dreams and the amount of effort we are willing to put forth in 
pursuit of our goals. Collaboration, dialogue, reflection and accountability are the norm. In partnership with our Board and district 
administration, we regularly monitor the needs of our students, staff and parents through surveys and group meetings. Our School 
Plan for Student Achievement guides our path and helps us monitor achievement data. Our methods and results are routinely 
evaluated through site and district assessments. We have created a culture of success and an environment instilled with the standards 
of excellence necessary to provide students with a meaningful education, one that promotes academic achievement and social 
development and extends to a successful transition to middle school and beyond. 
 
The entire School Based Coordinated Program is managed between our supplemental Title I and Local Control Funding Formula 
Targeted Funds budgets and is monitored by the principal, and SSC/ELAC. The principal regularly monitors and coordinates budgets, 
programs, and implementation in the areas of the instructional program, academic support, professional development and parent 
and community involvement. This assures that all of Centerville School’s students are able to receive additional services to help them 
experience success in the core program. To guarantee student participation in all coordinated programs, the principal oversees a plan 
to involve staff and community in an effort to increase student attendance. 
 
As the hub of the community, Centerville is a place where families and community members gather regularly to support, celebrate 
and honor the diverse achievements of all our children. Partnerships with local businesses such as Vulcan Building Materials make 
many activities possible. Centerville is unique because parents, teachers and business leaders work togetherto inspire and promote 
academic excellence while honoring the hopes, abilities and talents of our children. Our school is a family, bound together through 
affection and shared goals, our history and a firm commitment to building a strong future. 
 

2017-18 School Accountability Report Card for Centerville Elementary School 

Page 2 of 12