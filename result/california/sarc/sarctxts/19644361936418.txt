Nestled in one of the many valleys of the San Gabriel Mountains, Northview High School sits on 43 acres in the northwest section of 
the City of Covina in the County of Los Angeles. Northview welcomed its first class of students in the 1959-60 school year, and it has 
been serving the community with excellence in education ever since. As one of three comprehensive high schools in the Covina-Valley 
Unified School District, Northview has experienced change and growth throughout its years of service to the community. The 
demographics of Northview shifted since the late to mid-eighties but has remained relatively constant in recent years. The English 
Learner (EL) population has increased recently with a current rate of 5.8% of the school population designated as EL. For the current 
school year, our demographics reflect the following: Hispanic/Latino 82.6%; American Indian/Alaskan Native <1%; Asian 2%; 
Black/African-American 3.8%; and White 8.2%. Northviews' Free and Reduced Lunch population decreased slightly to 72%. 
 
Orange groves once covered the area that is now inhabited by the school and as the physical landscape has changed so has the 
socioeconomic makeup of the surrounding community. Once populated by predominantly White middle-income property owners, 
the community is now largely made up of Latino-inhabited subdivisions and apartment buildings. In addition, to drawing students 
from the cities of Covina and Irwindale, the school also attracts students from surrounding cities, including Azusa, West Covina, and 
Baldwin Park for programs that are not offered at their zoned schools. As a result, Northview High School is home to an amalgamation 
of background, and it continues to strive for a definitive multicultural atmosphere. 
 
The campus, which is admired for its park-like atmosphere teeming with beautiful blossoming trees and shrubs, garden areas, and 
benches, accommodates a population close to 1,250 students and 123 staff/faculty/administrative members. The site is comprised of 
57 permanent classrooms and laboratories, 10 portable classrooms, 1 administrative building, a cafeteria, a gymnasium, a library, a 
music building, a track and field course, a junior varsity football field, a state-of-the-art varsity baseball field, a junior varsity baseball 
field, a junior varsity softball field, a varsity softball field, and multiple tennis courts. The “B” building houses a television studio as 
part of the FAME Academy (Fine Arts, Media, and Entertainment). Each facility provides students and faculty with the enrichment 
needed to succeed academically and athletically. Measure C has brought with it new athletic facilities, a pool, a new entrance set up, 
and updates to infrastructure such as roofing, paint, and carpet. The community has responded positively to the projects and is very 
supportive of the changes that are taking place. 
 
Northview High School has maintained strong ties with the major communities it serves. Irwindale, which lies to the northwest of the 
school, and Covina are the communities that provide an array of support to the school. The school works closely with both the 
Irwindale and Covina Lions Clubs through their Annual Scholarship Speech Contest. Northview also supports student involvement in 
both the Irwindale and Covina Teen/Youth Government Clubs. The communities work closely with the school to offer supplemental 
tutoring programs through the Irwindale and Covina Libraries, and a myriad of business, mostly from the beautifully scenic strand of 
Downtown Covina, provide our sports with sponsorships and meal services. Northview takes pride in working with the Rotarians and 
Kiwanis clubs in our community and will continue to reach out to all who are vested in the success of our students and our combined 
success as a community. 
 
Northview High School has been recognized by WASC for providing quality education for decades. Since 1970, each visiting committee 
has graciously seen fit to grant Northview six-year terms. Northview High School was both nominated for and received California 
Distinguished School status. As the school continues to hold these accolades as a barometer of success, the school stakeholders 
continue to work hard to create and maintain programs that will help the school garner continued success. 
 
The school’s vision statement is always a work in progress. As the school community strives and attains the vision it has created 
throughout the years, the vision begins to evolve as the community aspires to reach new heights in creating not only academically and 
socially savvy citizens, but also citizens who are prepared to enter a technologically calculating future. When Northviews' community 
of parents, business partners, teachers, administrators, and students gather to create a vision of what a future Northview should be, 
our purpose becomes clear. 
 

2017-18 School Accountability Report Card for Northview High School 

Page 2 of 14 

 

Much of the recent success at Northview can be attributed to a change in the school schedule and in teacher collaboration. For the 
past 11 years, Northview has been on a challenging seven-period day schedule that had been voted in by staff each year. This schedule 
was designed to assist students in need of intervention by allowing for tutoring and other forms of intervention within the school day, 
particularly those described as struggling. In addition, the schedule would allow NHS to pursue WASC Visiting Committee critical areas 
of need: increase achievement and reduce the gap between SWD and ELD students; increase rigor and achievement in ELA and Math 
with special emphasis on ELD and SWD students; and build an academic and college-going culture on campus. Students have the 
opportunity to take an intervention or enrichment course, or have a free period depending on his/her academic needs. Students are 
rewarded for their academic progress by earning an off-campus lunch pass. In addition, the 7th period day has allowed all departments 
to share common planning time in order to develop common formative assessments, examine data, and identify best teaching 
practices in their curriculum using the Professional Learning Community model. 
 
The percentage of students meeting CSU/UC requirements has steadily increased. The graduation senior class was at 30% in 2011, 
37% in 2012, 47% in 2013 and 53.3% in 2014, 55% in 2015, nearly 64% in 2016, 68% in 2017, and 63% in 2018. The increase has been 
remarkable. 
 
Based on previous WASC recommendations, the school and district culture are in support of building a college going student body that 
continues to support Advanced Placement (AP) opportunities for all. The AP program has increased in recent years through the hard 
work of our counselors and AP teachers who follow an open enrollment policy. Current offerings in Advanced Placement have been 
expanded to include AP Art 2D to increase students taking advantage of the rigorous curriculum in Fine Arts. We were recently 
accepted into the AP Capstone program and have implemented the first course, AP Seminar, this school year. 
 
We have remained committed to our WASC Critical Areas for follow-up including the following: 
#1: Northview needs to increase the number of students meeting A-G requirements. 
#2: Northview needs to incorporate differentiated instructional strategies in all departments. 
#3: Northview needs to increase the academic rigor for all students, including English Learners (EL) and students with disabilities. 
#4: Northview needs to create a college going culture to increase the number of students pursuing higher education. 
 
Professional Development is ongoing. Currently the school is transforming our focus to problem solving through our work with 
Focused Schools. We have committed to the implementation of schoolwide strategies that will promote critical thinking, collaboration, 
and communication. Our instructional focus is driving our professional development calendar. 
 
Through our work with Focused Schools, we began to analyze data and question what we wanted for our students. Over the course 
of several on site professional development meetings, it became sufficiently clear that every stakeholder wanted our students to be 
able to succeed in whatever endeavor he or she planned to take on after their four years at Northview High School. We collectively 
decided that in order for our population to in fact be successful we had to first define success. Through a series of workshops led by 
our Instructional Leadership Team (ILT), we developed the following Leadership Message on Success: Success is a personal journey, 
which requires self-discipline and perseverance; Success is embodied in personal and social responsibility, integrity, and commitment 
to the education necessary to attain one’s life goals. 
 
Once we were able to determine our Leadership Message on success, we then set out to determine how we, as a united staff, could 
insure this brand of success for all of our students. Led once again by our ILT, we came to the consensus that in order for every single 
student to be successful, they needed to be expert PROBLEM SOLVERS. Once this was agreed upon, we then set out to determine 
what makes an expert problem solver. Through extensive analysis of the work of John Hattie and his work on Visible Learning, and 
through our own analysis of what effective teaching strategies work on our campus, we determined that in order to teach our students 
to be expert problem solvers, we needed to insure that they could effectively collaborate, communicate and think critically. From this 
determination, our instructional focus statement was then drafted. 
 
NORTHVIEW VISION STATEMENT 
The school’s vision statement, updated yearly through collaboration between all members of the school community, businesses, 
parents, administrators, and students--encapsulates the school’s constant drive towards success: 
The staff has created a "Success Statement" that permeates throughout the campus and is foundational for what happens each day 
at Northview High School. It reads as follows: Success is a personal journey which requires self-discipline and perseverance; Success is 
embodied in personal and social responsibility, integrity, and commitment to the education necessary to attain one’s life goals. 
 
 
 
 
 

2017-18 School Accountability Report Card for Northview High School 

Page 3 of 14 

 

NORTHVIEW MISSION STATEMENT 
Northview High School is committed to providing its students with a comprehensive education that ensures a lifetime of excellence. 
We strive to establish a rigorous and progressive curriculum that is both challenging and relevant to all students, one which promotes 
within a safe learning environment the intellectual, creative, and emotional growth of each individual, so that each may attain the 
skills necessary to realize his or her dreams and goals for future success. 
 
NORTHVIEW LEADERSHIP 
Northviews' Instructional Leadership Team (ILT) has led the way with a focus on problem-solving. We are committed to creating expert 
problem solvers through the implementation of strategies that promote Critical Thinking, Collaboration and Communication as 
measured by department common assessments, districtwide writing assessments, A-G completion rates, D/F ratios, and CAASPP 
testing results. 
 
The focus on problem-solving and student success also appears in the school’s tenets, renamed on this campus to SLOs. Our current 
Student Learning Outcomes have been a cornerstone on our campus for many years. With the ushering in of Focused Schools, our site 
will be moving toward updating our SLOs to reflect the focus of students who can problem solve through collaboration, 
communication, and critical thinking.