Granite Bay High School opened its doors in 1996 and is the fourth of five comprehensive schools 
in the Roseville Joint Union High School District. GBHS is located in the foothills of the California 
Sierra Nevada mountains adjacent to the fast-growing technology industries of Placer County. 
Situated on a beautiful campus featuring state-of-the-art technology, multiple athletic venues, and 
a 500-seat performing arts theater, the school is the centerpiece of the community. We serve 
students in grades 9-12 in the community of Granite Bay as well as the southeastern part of the city 
of Roseville. Economically, Granite Bay is primarily an educated and affluent community. We are 
on a four-by-four block schedule with the school year split into two 18-week terms. Students are 
enrolled in four classes each term. Each semester-long course at GBHS is equal to a year-long course 
in a high school on a traditional six-period schedule. This schedule allows time for students to 
explore their passions, repeat courses for a more successful finish and/or accelerate their learning. 
Our student population is currently 2043 students, with a senior class of 498. 
 
Granite Bay has a robust college-preparatory focus with a meticulous approach to choosing the 
right pathway for each student toward secondary success. Currently, twenty-six percent of the 
senior class has a grade point average above a 4.0, and it is not unusual for our seniors to apply and 
be accepted to multiple universities. The International Baccalaureate (IB) diploma and certificate 
programs promote inquiring, knowledgeable and internationally-minded individuals, while our 
many Advanced Placement (AP) offerings allow students to challenge themselves in specific 
content areas. GBHS is part of the 1% of high schools nationwide who have true equity in the 
makeup of the students who are taking Advanced Placement and International Baccalaureate 
classes. In fact, the school now tests more students in AP/IB than anyone in Northern California. 
Granite Bay also has a strong career-preparatory focus in our Project Lead the Way (PLTW) 
engineering pathway, as well as in our Information Technology, Media and Business course 
pathways. 
 
Along with a tradition of excellence in the classroom, Granite Bay High School has many highly 
successful programs. Led by an award-winning 130 member marching band, our journalism, 
yearbook, speech and debate, theater and visual art departments annually sweep multiple 
competitions across the state and the nation. GBHS also boasts over 24 athletic programs and over 
50 extra-curricular clubs such as Speech and Debate, Future Business Leaders of America (FBLA), 
Robotics, and Design and Engineering consistently outscore their competitors and represent this 
school's commitment to high achievement. Furthermore, our Peer Connections, Leadership and 
Student Government courses all strive to reach students who might not have a niche yet, need a 
little extra emotional support and/or desire to give back to the school and community. 
 

----

---- 

Roseville Joint Union High School District 

1750 Cirby Way 

Roseville, CA, 95661 

(916) 786-2051 
www.rjuhsd.us 

 

District Governing Board 

Mrs. Julie Hirota, President 

Mr. Gary T. Johnson, Vice President 

Mr. Andrew Tagg, Clerk 

Mr. Scott E. Huber, Member 

Mrs. Paige K. Stauss, Member 

 

District Administration 

Dr. Denise Herrmann 

Superintendent 

Mr. Jess Borjon 

Assistant Superintendent, Curriculum 

and Instruction 

Mrs. Judy Fischer 

Executive Director of Equity & 

Intervention 

Mr. Joe Landon 

Assistant Superintendent, Business 

Services 

Mr. Brad Basham 

Assistant Superintendent, Personnel 

Services 

Mr. John Becker 

Executive Director of Personnel 

Services 

Mrs. Diana Christensen 

Director of Classified Personnel 

Mr. Craig Garabedian 

Director of Special Education Services 

Mr. Mike Fischer 

Director of Academic Instruction & 

Support 

Mr. Tony Ham 

Director of Technology 

Mr. Kris Knapp 

Director of Maintenance & Operations 

Mrs. Julie Guererro 

Director of Transportation 

Mr. Jay Brown 

Director of Food Services 

Mr. Scott Davis 

Director of Facilities 

 

2017-18 School Accountability Report Card for Granite Bay High School 

Page 1 of 11 

 

Steeped in this tradition of high achievement and strong work ethic, and celebrating 22 years of Grizzly Pride this year, Granite Bay High 
School continues to promote and foster greater student success. Our school was selected as a California Distinguished School in both 2002 
and 2007, as a National Blue Ribbon Award Winner in 2002, and as a Gold Ribbon School in 2017. Additionally, we received special 
recognition for our Visual and Performing Arts and Career Technical Education programs with our Gold Ribbon designation. Newsweek 
Magazine has also recognized Granite Bay as one of America’s best high schools, with the most recent ranking placing us in the top 2% of 
all high schools in the nation. In the spring of 2013, US News and World Report ranked Granite Bay as one of America’s top high schools 
and placed us in the top 2% of all high schools as well. Furthermore, based on state test scores, college readiness, graduation rates, 
SAT/ACT scores, and teacher quality, Niche recently rated GBHS as the number one public high school in the Sacramento Metropolitan 
Region. 
 
Mission Statement 
Granite Bay High School is a positive learning community of high expectations that prepares all students for post-secondary success. 
 
Guiding Principles 

• 

• 
• 

• 

• 

Students will be challenged by a relevant and rigorous curriculum that provides high standards and expectations for every 
level of ability and interest. 
Students will have multiple opportunities to make informed decisions in a supportive caring environment. 
Students will attend an emotionally and physically safe campus where students, staff, and the community promote social and 
individual responsibility as well as integrity in all areas. 
Students will be served through a process of continuous assessment and feedback that values the active participation and 
contributions of students, staff, parents, and other stakeholders. 
Students will be exposed to high-quality co-curricular programs that recognize and reward participation, personal growth, 
leadership, and achievement.