LHS is committed to creating college and career ready graduates. We are preparing tomorrow’s college student while simultaneously 
providing students with career ready skill sets needed in our competitive job market. Whether an LHS graduate goes on to college, 
chooses military service or goes directly to work, he/she is better prepared by earning of a high school diploma and gaining an 
understanding of the expectations of employers, colleges, universities and our community. LHS provides an opportunity for each 
student to explore and succeed. This is something that is not done alone, but rather with support of parents, school and community! 
 
Vision 
Love Purple, Live Gold 
 
Mission 
Livingston High School is committed to creating college and career ready graduates. 
 
LHS Core Values 
Respect 
Accountability 
Integrity 
Involvement 
 
Agreements 
1. All students can learn. 
2. We need to prepare students for college and career. 
3. Own it! 
 
Live Gold Traits 
Being a wolf means you have: 
Teachable Spirit 
Selflessness 
Leadership 
Confidence 
Mental Toughness 
Academic Responsibility 
Pride 
Discipline 
Integrity 
Character 
 
Instruction 2020 Goal: A flexible learning environment where ALL students become college and career ready through rigorous learning 
experiences that activate and allow students to pursue their passion. 
 

2017-18 School Accountability Report Card for Livingston High School 

Page 2 of 15