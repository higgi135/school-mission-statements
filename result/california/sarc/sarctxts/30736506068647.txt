“Turtle Rock is a multicultural community of learners who focus on flexible thinking, cooperative problem solving and creativity. 
Together we respectfully value each other and our unique contributions. We teach, practice and promote tolerance by embracing 
diversity for all. Each of us demonstrates integrity, honesty and accountability. We are global citizens preparing for the world of 
tomorrow." 
 
Turtle Rock has been recognized as a California Distinguished School in 2002, 2010, 2014, and most recently, in 2018. The award is 
based, in part, on several factors including outstanding student performance on statewide assessments , exemplary progress of English 
Language Learners on various academic indicators, and low rates of absenteeism and behavioral incidents. The stake holders of the 
Turtle Rock community consistantly express high levels of satisfaction with the school environment and quality of instruction teachers. 
 
Turtle Rock offers programs to meet the needs of diverse learners. The AAPAS (Alternative Program for Academically Advanced 
Students) Program is a magnet program that serves students who qualify for a challenge level environment. The SEI (Sheltered English 
Immersion) program serves those students who are learning English, The GATE (Gifted and Talented Education) program adds depth, 
complexity, creativity and rigor to the our general education classrooms. 
 
The Turtle Rock staff, students, parents, and community are to be commended for their cooperative spirit. It is with the parents’ 
unending support that we are able to accomplish our goals and help the students reach their full potential.