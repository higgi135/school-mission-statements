"Through collaboration we unite and lead all students to success". 
 
We are proud to be a California Distinguished School and a recipient of multiple California Business for Education Excellence Awards. 
Marco Forster serves over 1350 students in Grades 6, 7, and 8 from San Juan Capistrano, Dana Point and Capistrano Beach. Our school 
addresses the needs of the whole child, regardless of primary language or learning ability celebrating our cultural and individual 
diversity in an environment of inclusion. 
 
Our committed and enthusiastic staff works together to maintain high standards of academic achievement and behavior. Collectively 
we: 
~provide a meaningful education in a safe and supportive environment, consistent with each student's needs. 
~acknowledge, respect, and develop skills of resiliency, self-advocacy, and citizenship in all students to ensure continued success 
beyond middle school. 
~continuously collaborate to examine and adjust our practices and procedures to positively impact student learning. 
~create lessons that are relevant, rigorous, and purposeful. 
~frequently collaborate to develop and administer common formative assessments, analyze our results, and provide intervention. 
~commit to be open and supportive in a sharing challenges and strengths in order to increase student achievement. 
 
Standards-based instruction ensures students equal access to the curriculum evidenced by test scores which are consistently above 
the state and national averages. Marco Forster is an educational institution that sees its responsibility as reaching far beyond the 
classroom door. The strong support of parents, businesses, and community partnerships keeps students connected with the 
community in a positive way. 
 
Preparing students for the rigors of the 21st century is a site focus. In addition to two computer labs, all core academic classes are 
equipped with a class set of Chromebooks allowing teachers to easily incorporate technology into their curriculum. Marco also prides 
itself on the robust elective offerings available to students including musical theater, orchestra, band, art, and robotics, to name a 
few. Additionally, our school offers a middle school dual immersion program for students striving to become bi-literate during their 
school career. Intramural sports teams, including basketball, flag football, and soccer as well as after school groups such as the 
Students Run LA marathon team and Mock Trial enhance our students' academic day. 
 
For additional information about school and district programs, please visit www.capousd.org