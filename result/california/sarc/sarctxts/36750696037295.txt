Upland Junior High School is one of fourteen schools in the Upland Unified School District. Our 
school is located at the base of the San Gabriel Mountains in the south section of the City of Upland. 
Upland Junior High School can be found a few blocks from historic downtown Upland, among a 
mixture of historic single residential dwelling homes, apartments, town homes, condominiums, and 
commercial buildings. 
 
Our student population consists of approximately 773 students in grades 7 and 8 and reflects the 
changing demographics of our community. We value the rich experiences and opportunities that 
our diversity offers as we learn, play, and work together. 
 
Upland Junior High School has built a tradition of academic excellence and a strong commitment 
to helping every student meet his/her potential. The Upland Junior High staff believes that all 
children can learn, and to that end, our highly qualified professional staff strives to provide every 
student access to a rigorous, standards-based core curriculum. We also believe that it is never too 
early to start exposing students to the idea of attending college. Because of this, we became a 
California GEAR UP school in 2009. We remain committed to building a college going culture here 
at Upland Junior High, and this year we are proudly pledging that every single student at our school 
will have the opportunity to visit a college campus during the year. 
 
In 2014, Upland Junior High implemented Advancement Via Individual Determination (AVID) in 
grade 7 and in 2015 we implemented AVID in grade 8 to continue exposing our students to a college 
culture and to help to equip them with organizational and study skills they will need to be successful 
in college. In 2016 we became an AVID certified school. We have also added the AVID Excel program 
in 7th and 8th grade, which supports our English Learners to reach English proficiency and prepares 
them to enter the AVID program at the high school. This year we will offer the PSAT 8/9 to all 
students, furthering our message that the path to college and career starts now. 
 
We are extremely proud of our staff’s hard work, our parents and community involvement, and of 
our students’ academic success. In 2015, Upland Junior HIgh was awarded the Gold Ribbon Award 
and the High Achieving Title One School Award by the State of California for our AVID and Gear Up 
Programs and our commitment to creating a college-going culture. This year, we will apply for the 
AVID Site of Distinction award in recognition of our commitment college going culture and 
instructional practices of writing, inquiry, collaboration, organization, and critical reading school-
wide. 
 
We we also recognized as a "Silver" level of implementation for our Positive Behavior Interventions 
and Supports (PBIS). Combined with our academic program, this makes up our Multi-tiered System 
of Supports (MTSS). Our daily focus is to ensure ALL students are supported to build the social and 
academic skills they need to achieve success. 
 
The dedicated staff of Upland Junior High School is proud to be a professional learning community 
where we promote learning for all students and all students are connected to all staff members at 
school. We model a mindset of learning, growth, and perseverance through setbacks. We teach 
this to our students, and together we make continuous progress toward our ultimate mission: to 
ensure that all students learn the skills and behaviors to thrive in an every-changing world. 
 
 

2017-18 School Accountability Report Card for Upland Junior High School 

Page 1 of 9