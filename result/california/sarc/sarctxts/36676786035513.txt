E.J. Marshall Elementary students are expected to achieve high levels of learning in a positive 
environment through collaboration with faculty, students, parents, and the community. 
 
VISION & VALUES 
E.J. Marshall Commitments 
 
Staff Commitment: We are committed to creating a school environment where every student will 
SHINE. Every student will be successful, develop their personal character, and master academic 
skills. The elementary education we provide will serve as the foundation upon which our students 
will build their lives. 
 
Student Commitment: We, as students of E.J. Marshall Elementary, are committed to learning how 
to SHINE. We will demonstrate a positive attitude toward our learning, our fellow students, and 
teachers. We will work to develop our character and our academic skills. We will take responsibility 
for the choices we make and work together to make E.J. Marshall Elementary a safe place for us to 
grow. We will ask our teachers and family for support when we need it and always give our best 
effort. The education we earn at E.J. Marshall Elementary will be the foundation upon which we 
can build our lives. 
 
Family Commitment: We, as E.J. Marshall families, are committed to our children’s education. We 
will ensure that our children are present, on time, and prepared for school daily. We will be active 
participants in our children’s education both inside and outside of school. The support we provide 
our children will enable them to SHINE. 
 
 

----

--

-- 

Chino Valley Unified School 

District 

5130 Riverside Drive 
Chino, CA 91710-4130 

(909) 628-1201 

www.chino.k12.ca.us 

 

District Governing Board 

James Na, President 

Irene Hernandez-Blair, Vice 

President 

Andrew Cruz, Clerk 

Christina Gagnier, Member 

Joe Schaffer, Member 

Alexi Magallanes, Student 

Representative 

 

District Administration 

 

Superintendent 

Sandra Chen 

Associate Superintendent, Business 

Services 

Grace Park, Ed.D. 

Associate Superintendent, 

Curriculum, Instruction, 
Innovation, and Support 

Lea Fellows 

Assistant Superintendent, 
Curriculum, Instruction, 
Innovation, and Support 

Richard Rideout 

Assistant Superintendent, Human 

Resources 

Gregory J. Stachura 

Assistant Superintendent, 

Facilities, Planning, and Operations 

 

2017-18 School Accountability Report Card for Marshall (E.J.) Elementary school 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Marshall (E.J.) Elementary school 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

24.5 

25.5 

23.5 

0 

0 

0 

0 

0 

0 

Chino Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1199.9 

22 

2 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.