Our Roosevelt School Mission Statement: A community of caring individuals working together to 
achieve academic excellence for all students. 
 
Enrollment in Roosevelt School signals a significant transition for Taft students who have previously 
attended smaller, neighborhood primary schools since kindergarten. For many students, Roosevelt 
School provides their first opportunity to interact with classmates from all areas of greater Taft. 
They are also expected to undertake greater individual responsibility for schoolwork and 
homework. In spite of recent cutbacks, we are able to offer our students opportunities for 
individual exploration in music. 
 
The academic and personal success of our students continues to be a priority at Roosevelt. Scores 
from the SBAC are being evaluated, and Roosevelt has implement benchmark assessments, 
practice tests, and direct explicit instruction in technology which includes instruction in the tools 
utilized in the SBAC. 
 
Roosevelt is ever aware of the needs of all students, but in particular, our English Language Learners 
continue to struggle. During the summer of the 2017/2018 school year, two teachers were taught 
methods to implement in the classroom to promote English Learner success, as well as, success for 
all students. All Roosevelt teachers continue to receive training and coaching throughout the year 
on these methods that are currently being implemented in all general education classrooms. 
 
The Roosevelt teaching staff is provided with intensive training to teach classes composed of 
students learning English and to learn more about the academic standards for these students. ELD 
instruction is embedded within all core areas of instruction. Integrated and Designated ELD are 
being practiced school wide. School wide behavior intervention and conflict mediation plans have 
been implemented, and staff and students have been provided with training. The implementation 
of the Olweus Bullying Prevention Program has also been a focus, with weekly class meetings, Red 
Ribbon Week, Anti-bullying Week, The Great Kindness Challenge and school wide anti-bullying 
rules and protocol. Training for PBIS was implemented in 2015 with two teachers and the principal 
being trained. Implementation of PBIS continues to expand. 
 
Roosevelt students also have the opportunity to participate in the After School Education and 
Safety (ASES) program from dismissal time to 6:00 p.m. each day. Enrolled students are provided 
with academic support, physical education, and extracurricular activities under the supervision and 
direction of district personnel. 
 
After school Migrant Program is conducted at Roosevelt School. 
We are an AVID Elementary school, with five of our teachers that have attended the AVID summer 
institute. These teachers, in turn, provide the Roosevelt staff with PD in AVID strategies. 
 
School goals include continuing to modify school wide curriculum, pacing guides and assessments 
to ensure instructional consistency throughout the school while we work toward mastery of the 
Common Core State Standards. The Roosevelt staff has also identified ELA, academic vocabulary, 
use of complete, grammatically correct sentences when speaking, and mastery of basic fact families 
as areas of focus. We also strive to prepare students for the rigors of higher education and the 
responsibilities of citizenship and the working world. 

2017-18 School Accountability Report Card for Roosevelt Intermediate 

Page 1 of 10 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Roosevelt Intermediate 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Taft City School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

19 

19 

16 

5 

0 

5 

0 

8 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

110 

23 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Roosevelt Intermediate 

16-17 

17-18 

18-19