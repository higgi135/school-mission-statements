Janie P. Abbott Elementary School is located at 5260 E. Clark Street in Lynwood, California. During 
the 2016-2017 school year, 614 students in grades Pre-K-6th grade were enrolled. Demographic 
composition of student enrollment includes: 52% English Language Learners. In addition, 95% of 
our student population is Hispanic or Latino, 4% are African American and 1% are Caucasian. Our 
primary demographic subgroups are English Learners, Socioeconomically Disadvantaged, and 
Hispanic. 
 
Janie P. Abbott Elementary School is committed to providing students a rigorous and quality 
education. Abbott strives to develop inquisitive and versatile life-long learners who will achieve 
their academic and creative potential. Collaboration among staff, parents and students provides a 
caring, inclusive and rigorous learning environment, which will develop students of the utmost 
character, who value 
in their pursuit of academic excellence. Standards-based 
differentiated instruction ensures that the needs of all students are being met. Students who may 
have difficulties mastering grade level standards are provided additional support to achieve 
mastery. Teachers provide various instructional strategies, one on one assistance, and small group 
instruction to address the varied learning styles and needs of students. 
 
As a recipient of various accolades, including the 2008 and 2016 Title I Academic Award, Abbott is 
committed to continuing to provide our students with a quality education. Our staff members 
believe that each child has the right to a quality education and embraces the responsibility for 
helping promote the growth and development of contributing, productive citizens. 
 
Adolfo Herrera, PRINCIPAL 
 

integrity 

 

 

----

---- 

----

---- 

Lynwood Unified School District 

11321 Bullis Road 
Lynwood, CA 90262 

(310) 886-1600 

http://www.lynwood.k12.ca.us 

 

District Governing Board 

Gary Hardie, Jr. - President 

Maria G. Lopez - Vice President 

Briseida Gonzalez, MSW - Clerk 

Alfonso Morales, Esq. - Member 

Alma-Delia Renteria, M.Ed. - 

Member 

 

District Administration 

Gudiel R. Crosthwaite, Ph.D. 

Superintendent 

Shawna Dinkins Ed.D 

Assistant Superintendent 

Educational Services 

 

Nancy Hipolito 

Assistant Superintendent 

Human Resources 

 

Gregory Fromm 

Chief Business Official 

 

2017-18 School Accountability Report Card for Janie P. Abbott Elementary School 

Page 1 of 9 

 

Major Achievements 

 

Janie P. Abbott Elementary School was recognized as one of our 772 California Department of Education Gold Ribbon Schools. 
The award reflects our school’s success in creating a positive learning atmosphere for your students. 

 Abbott recognized as 2016 Title I Academic Achievement Award from the California Department of Education. 
 Abbott Elementary received Silver Level PBIS schools award 
 Abbott Elementary School was selected as a Turnaround Arts school, a prestigious honor further enhancing and expanding the 

 

arts for every TK-6 student on site. 
Through an intensive curriculum alignment process, Janie P. Abbott Elementary has ensured all instruction is aligned to support 
all students’ ability to graduate college and career ready. Curricular goals, objectives, and actions were created during the 
Curriculum Alignment process using state and content standards. 

 All students have access to appropriate grade level core materials. Additional resources are available for students who 

participate in the Special Education and English Learner programs. 

 Select math and English language arts teachers, including Special Day Class and Resource Specialist Program teachers, attended 
a six day LUSD institute to develop standards based benchmarks for their respective grade level. Teachers also attended 
Summer Institutes to help support English Learners and other students with ELD/Formal Academic English focus in closing the 
achievement gap. Our Instructional Lead Teacher participates in coaches’ training and conducts daily classroom coaching, 
modeling, and provides support to teachers based on data as well as provide follow-up professional development based on 
data to grade levels/departments or staff on a weekly basis. This multi-year professional development plan supports students’ 
needs and is based on student data. Additional trainings are provided by the site administrators. 

 Teachers have attended Summer Institutes to help support English Learners and other students in closing the achievement 
gap, particularly for students with disabilities and English Language Learners. At Abbott, we provide 45 minutes of strategic, 
daily intervention to support all students as Language Learners in the form of our Academic Language Development/English 
Language Development rotations. School administrators monitor implementation of the 50/50 instructional model, where 
half of the period is spent in a heterogeneous whole group and half is in small homogeneous leveled groups for targeted 
instruction, in an effort to support Abbott’s spectacular teachers and scholars. 

 
Focus for Improvement 

 Abbott Elementary School has made significant growth in English language arts and mathematics over the past three years. In 
order to foster continued growth, our collective focus must shift from a mindset of remediation to one of enrichment and 
depth. 

 Abbott will continue to deepen its school-wide focus on the development of data, continue to align our curriculum with 
essential state and Common Core standards, design interim assessments to determine student progress, participate in 
scheduled reflective meetings to determine instructional need and implement prescriptive intervention. Teachers will strictly 
adhere to our 50/50 instructional plan framed around teacher identified target standards. Center based instruction includes 
guided reading, which will address the variance in student reading levels. Thirty minutes of daily word study will be integrated 
into the schedule. 

 Additionally, Abbott instructional staff create team lesson plans and assessments. These assessments are comprised of target 
standards in both English language arts and math. In order to prepare our Abbott Scholars for the technological demands of 
the Common Core and Smarter Balanced Assessment formats, these periodic assessments for third through sixth graders will 
be administered and scored online. Common Core is a set of standards developed by the federal government in the hopes of 
unifying education standards nationwide. 

 Abbott regularly participate in collaborative grade level planning sessions, to support the cultivation of differentiated strategies 
for individual student needs. After school intervention and Saturday academies also further reinforce daily instruction and 
student mastery of key target standards. 

 Student progress is closely monitored using state, district benchmark, and site-based assessments. These assessment results 

are analyzed in conjunction with teachers’ observations. 

2017-18 School Accountability Report Card for Janie P. Abbott Elementary School 

Page 2 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Janie P. Abbott Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

29 

30 

28 

1 

0 

0 

0 

0 

0 

Lynwood Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.