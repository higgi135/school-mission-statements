Winton Middle School is one of four schools that comprise the Winton School District. We serve the sixth, seventh, and eighth grade 
students in the community of Winton. WMS students are challenged with a curriculum that is aligned with the California Common 
Core State Standards. 
 
The overall vision of WMS is to have all students performing at or above proficient levels in reading and mathematics. We also strive 
to give them life skills that will enable them to succeed in high school and beyond. We have an active Student Body organization, an 
award winning band, and a comprehensive sports program. Our school wide theme this year is Arcade Games. Our assemblies and 
competition will be based around this theme. We will have a reward trip each semester for the grade level who earns the most points. 
This is a friendly competition that promotes school spirit. 
 
Winton Middle School will be using one to one Chromebooks again in the classroom this year. We are fortunate that our district is 
able to be a leader in technology. Our teachers are very excited and have spent countless hours getting ready to use this technology 
in their rooms. We also have an new messenger system. This system will allow us to get messages sent home in a timely manner. 
Parents are also able to access student grades through the parent portal in our student data base.