Principal's Message 
 
Linscott Charter School is a K-8 parent participation school collaboratively governed by parents and staff. 
Dedicated teachers and supportive families collaborate to provide individualized, active, hands-on learning that fosters each child's 
academic success, encouraging excellence. Students become self-motivated, critical thinkers demonstrating creativity, environmental 
awareness, and empathy. Our focus on community, in and out of the classroom, develops students with a deep connection to the 
Charter School and the greater community, building tools and confidence to live a physically, emotionally, and socially healthy life. 
 
Julie Wiley, PRINCIPAL