Sierra Middle School is an International Demonstration Site for Advancement Via Individual Determination (AVID) as well as a 1:1 
digital device school. Sierra Middle School's mission is to educate all students with dignity and respect, and we are committed to 
challenging, nurturing and supporting all students in the academic, social and emotional development. 
 
 
 
Sierra Middle School prepares students to become purposeful contributors to a global society through learning experiences that 
promote student ownership of the path and pace of their education. In 2014-15, Sierra Middle School along with seven other schools 
in the district, applied and received the Bill and Melinda Gates Foundation grant to implement Personalized Learning. The five criteria 
of personalized learning (Learner Profile, Personalized Learning Plan, Flexible Environment, Competency Based Advancement, and 
Socially Engaged Contributor), provide voice, choice, pace, and path for students as they show competency of state content and 
practice standards.