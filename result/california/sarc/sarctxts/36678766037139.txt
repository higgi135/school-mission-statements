Thompson Elementary School has 28 classrooms, a library, a multipurpose room, and an administration office. The campus was built 
in 1966, and the facility strongly supports teaching and learning through its ample classroom and playground space, and a staff 
resource room. 
 
It is the mission of Thompson Elementary School to provide a learning experience to our students that affects a positive outcome upon 
the society of tomorrow. Moreover, it is our expressed objective to establish a solid foundation of knowledge that will support 
whatever endeavors they may choose in the future. We will initiate and direct a learning process that is liquid and flows to meet the 
diverse needs of all allowing them to reach the summit of their true potential. 
 
Accordingly, it is essential that our students: 
1. Acutely appreciate the on-going process of learning, 
2. Comprehend of the relationship between education and their future success, 
3. Actuate the dynamics of critical thought to analyze and evaluate all facets of life, 
4. Respect dissimilarities among people and embrace cultural diversity, and 
5. Lead by example and strive to make the world of tomorrow a better one.