Manzanita Elementary School, an innovative K-8 single-school district in an agricultural setting, graduates confident, responsible, 
entrepreneurial learners with strong academic and personal life skills, who are empowered to direct their own futures; we accomplish 
this through engaging, dynamic instruction delivered within a safe and caring environment using relevant technologies and a rigorous 
curriculum in a student- centered partnership with family, community, and a passionate, extraordinary staff.