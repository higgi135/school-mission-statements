Message From Principal 
Welcome to Bristow Middle School – A CALIFORNIA DISTINGUISHED SCHOOL and CALIFORNIA 
GOLD RIBBON SCHOOL. We are proud to share information about our great school through the 
School Accountability Report Card. 
 
At Bristow we believe that purposeful connections with students are a critical strategy to ensure 
success for all students. To continue to support student connections our mastery schedule is built 
around Interdisciplinary Teams, providing small 
learning communities for students. The 
department teams are provided a common preparation period to plan instruction and analyze 
student learning using a Professional Learning Community model. Both Interdisciplinary Teams and 
Departments also have regular Collaborative Learning Time a few times monthly to support 
student’s academic, social-emotional, and behavioral growth. Our Advisory program strives to 
build a positive school culture for the Bristow community. Advisory meets four days each week. 
 
Weekly, teachers meet during our Collaborative Learning Time (CLT). During CLT we work 
collaboratively in Professional Learning Communities to analyze student learning, share best 
practices, support students academically and 
 behaviorally, develop common formative 
assessments to analyze and guide student learning, strengthen English Language Development and 
Instructional Support programs, vertically articulate between grade levels, build our team structure 
and learning strategies to provide Math, English Language Arts, and behavioral interventions as 
needed. The Bristow staff is actively working toward raising student achievement. We are 
intentional in our work supporting BUSD Literacy Practices. Our Departmental teams engage in the 
Collaborative Learning Cycle to plan, implement, and reflect on student learning and instruction. 
 
Thank you for reviewing the Bristow School Accountability Report Card! 
 
School Mission Statement 
The mission of Bristow Middle School is to provide a safe and nurturing environment with high 
academic goals and behavioral expectations for all students supported by staff and the Bristow 
community. Positive student engagement empowers and fosters personal connections with all 
students which promote courage, perseverance, and success. All stakeholders inspire, encourage, 
and guide each other as a community of learners by making connections and building relationships. 
Our reciprocal culture of communication, accountability, and engagement will be measured, 
shared, and celebrated as a community. 
 
Values: Respect-Students First-Professionalism-Teamwork-Collaboration & Communication 
 
District Mission Statement 
The Brentwood Union School District will provide an exemplary education to all children in a safe, 
student-centered environment designed to nurture the whole child and partner with families to 
prepare globally productive citizens. 
 

2017-18 School Accountability Report Card for William B. Bristow Middle School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

William B. Bristow Middle School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

54 

54 

52 

1 

3 

0 

3 

1 

0 

Brentwood Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

52 

1 

2 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.