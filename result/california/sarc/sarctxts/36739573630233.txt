Serrano High School, established in 1977, is the sole comprehensive high school in the Snowline Joint Unified School District. Serrano 
is located in Phelan, CA in the high desert region of San Bernardino County and serves students from Baldy Mesa, Phelan, Pinon Hills, 
Oak Hills, West Cajon Valley, Wrightwood, and part of Victorville. We serve a rural area in which many of our parents commute to 
other locations for work. Serrano provides a comprehensive educational program which includes Advanced Placement, Career 
Technical Education, Agricultural Education, athletics, visual and performing arts, and multiple Pathway programs. Serrano has 
experienced continued declining enrollment since 2008 when student enrollment was 2,700. Serrano has consistently received 6-year 
accreditation terms and is ranked among America's Best High Schools by US News and World Reports. 
 
Mission Statement: 
Serrano High School will educate all students and academic-leaders for our community and world. We do this through our 
commitment to the transformative power of rigorous education at all academic levels. Providing endless possibilities. 
 
Vision Statement: 
Serrano will set the standard for academic excellence by maintaining consistency in mastery in the twenty-first century. We are 
committed to creating and sustaining the conditions that enable all Serrano students to experience an unparalleled educational 
transformation that is intellectual, socially, physically, and personally stimulating. 
 
School Profile 
Serrano High School is located in Phelan and serves students in grades nine through twelve following a traditional calendar.