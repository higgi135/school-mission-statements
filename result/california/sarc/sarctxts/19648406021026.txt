Welcome to Morrison Elementary School. We are a California Distinguished School and a California 
Title I High Achieving School. Morrison has also been awarded the prestigious Golden Bell Award 
for closing the achievement gap with English-Language Learners using English Language 
Development (ELD) and Academic Language Development (ALD). Our mission is to provide a caring, 
positive, and safe learning environment where each child is encouraged to make safe, responsible, 
and respectful choices, and to develop the academic skills necessary to reach their maximum 
learning potential. 
 
Students receive a rigorous academic program enriched by participation in the arts, especially 
music. Morrison has significant technology resources, which also support student learning.To 
achieve our vision for students, we support our staff members in the implementation of the 
Common Core State Standards to prepare students for college, career, and life. The standards 
clearly demonstrate what students are expected to learn at each grade level, so that every parent 
and teacher can understand and support their learning. 
 
Parents are always welcome on the campus to participate in their child’s education by reviewing 
homework and progress notes, volunteering in the classroom, attending school events and taking 
advantage of all the classes available to parents to further enhance the support given at home. 
Parents can help their children make the most of their education by sending them to school on time 
every day, by making sure that they are completing school and homework assignments, and by 
attending parent conferences. We believe that parents are our partners in providing a rigorous, 
quality education for our students. By working together, we can make a bright future for the 
children in our care. 
 

 

 

----

---

- 

----

--- 

Norwalk-La Mirada Unified 

-

School District 

12820 Pioneer Blvd 
Norwalk, CA 90650 

(562) 868-0431 
www.nlmusd.org 

 

District Governing Board 

Ana ValenciaChris Pflanzer 

Jude Cazares Board Vice President 

Darryl R. Adams Board Member 

Board President Board Member 

Karen Morrison Member 

Jesse Urquidi Board Member 

Jorge Tirado Board Member 

 

District Administration 

Dr. Hasmik Danielian 

Superintendent 

Dr. Patricio Vargas 

Assistant Superintendent - 

Educational Services 

Estuardo Santillan 

Assistant Superintendent, Business 

Services 

John M. Lopez 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for Julia B. Morrison Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Julia B. Morrison Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

31 

30 

31 

0 

0 

0 

0 

0 

0 

Norwalk-La Mirada Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

898 

2 

4 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.