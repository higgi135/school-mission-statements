Roseland Elementary is a transitional-kindergarten through sixth-grade elementary school located in southwest Santa Rosa. We are a 
diverse community of learners with staff, students and parents learning, growing and improving in relentless support of the academic 
and social success of all of our students. As with all schools in our district, we participate in the Accelerated Schools Plus Project. This 
school-governance system and model allows us to make systemic schoolwide improvements through the collaboration of teachers, 
students, parents and the local community alike. We believe that all of our students are gifted and talented, and we strive to give 
them meaningful, powerful learning experiences. We provide a rigorous, differentiated and standards based curriculum that is suited 
to meet each student’s individual needs. We have the highest expectations of our entire school community, and we are working 
together to further close the achievement gap for our English learners. 
 
HEART Vision 
Healthy Choices: We make the right choices to build healthy bodies and minds. 
Empowerment: We control our own path to success. 
Academic Achievement: We put forth our best effort to reach our individual learning goals. 
Respect and Responsibility: We are respectful and responsible leaders. 
Teamwork: We work together to be our best.