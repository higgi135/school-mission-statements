Forest Charter School is a WASC Accredited, Certified California Charter School that is committed to nurturing the love of learning in 
all students through parental choice in education. By cultivating a staff of dedicated personnel, Forest Charter School pledges an 
ongoing and expansive environment of educational excellence. The cornerstone of our Personalized Learning Program is positive 
collaboration on the part of participating students, parents, and professional educators. The ultimate mission of Forest Charter School 
is to honor the student’s learning style through parental options in the selection and implementation of effective curriculum to prepare 
him/her for success in the 21st Century.