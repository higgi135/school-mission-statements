2018/2019: 
Our mission at Robert Louis Stevenson Elementary School is to ensure the learning success of all of our students in a safe, nurturing 
environment, and to inspire them to become productive members of society who take an active role in positively changing the world 
around them. Our school motto is "Stevenson is Helpful, Friendly, Respectful." As a staff and community, we believe that all students 
will learn and achieve. We are committed to delivering a quality educational program to all students. Exemplary teaching occurs on a 
daily basis which focuses on standards-aligned curriculum. Differentiated instruction is presented by teachers in order to reach all 
levels, from those students who are below grade-level to those students who are exceeding grade-level standards. High expectations 
for student achievement and behavior are evident throughout the school. Our students build strong academic and social skills within 
a safe and caring community. Students leave Stevenson Elementary School with the ability to be proud, confident, successful scholars 
who are ready for the next step in their educational careers. 
 
Our vision is to continue to infuse the arts into our curriculum while growing in the area of technology. We embrace California Common 
Core State Standards which focus on multi-disciplinary study, while providing a variety of ways for students to access curriculum. These 
standards require analysis, synthesis, and promote depth of learning by creating opportunities for knowledge to be applied to real-
world context.