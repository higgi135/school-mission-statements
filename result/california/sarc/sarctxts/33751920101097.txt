French Valley Elementary School opened in August of 2003,and is currently serving around 900 students. 
 
 
OUR MISSION: 
 
 French Valley Elementary School serves to prepare well-educated, respectful students who make meaningful contributions to their 
global community. 
 
 
OUR SCHOOL VISION: 
 
• All students will learn a relevant and rigorous core curriculum which emphasizes interdependence among subject areas. 
 
• 
 
• 

Staff excellence will be supported through meaningful staff development, increased opportunities for decision making, and 
collaboration. 

The staff will provide a learning environment which supports the social, intellectual, physical, and emotional growth of every child. 

The school will use a system of authentic, performance-based assessments and use existing norm-referenced data to ensure 
accountability. 

The school community will foster students' ability to make wise decisions, practice ethical values and appreciate cultural 
differences. 

The school will continue to develop and maintain a comprehensive program to increase partnerships with parents, businesses, 
and the community. 

 
• 

 
• 

 
• 

 
 
 
 
 
 
 
 
 
 

2017-18 School Accountability Report Card for French Valley Elementary School 

Page 2 of 12