Children are first at James Marshall. Staff is engaging every student so they are challenged in a 
meaningful and captivating manner. There are strong community ties creating a happy and safe 
environment for students. 
 
James W. Marshall Elementary is located in the heart of the Rosemont community. The entire staff 
at James Marshall is committed to the academic success of every student. Teachers and support 
staff collaborate on a regular basis to review data to help guide daily instruction. We have several 
programs that enhance our daily classroom instruction. By effectively utilizing paraprofessionals 
and resource teachers, we provide leveled intervention in English-language arts for all primary 
students during the school day. This model allows for small group instruction at the students’ 
instructional level. James Marshall has a partnership with CSU Sacramento and benefits from two 
different programs. The CSUS Readers program provides one on one before school tutoring in 
reading and the Classroom Tutoring class provides college students who help in classrooms on 
campus. In addition, students are served by a variety of different Special Education classes on 
campus. 
 
In addition to students’ academic needs, James Marshall staff tries to focus on the whole child. We 
integrate character education into our daily teaching. Our focus for the past two years has been 
kindness. 
 
Additionally, we offer extracurricular activities whenever possible. We have an active Drama club, 
LEGO Club, Running Club, a basketball team and other fun filled and educational field trips and 
exciting assemblies such as “Fantasy Theater,” which exposes all students to the arts and music. 
We have an excellent Physical Education program as well. 
 
Parent involvement is encouraged at James Marshall. Parents are very active in our Parent Teacher 
Group as well as our Schoolsite Council and English Learner Advisory Committee. Parents are 
welcome on campus anytime and can volunteer in classrooms and are welcome to work or relax in 
our Parent Resource Center. The Parent Teacher Group and the staff at James Marshall work 
collectively to provide as many opportunities for parent education as possible, such as Family 
Science Night and an annual Art Show. 
 
James Marshall has Pre-School on site. Free after school care, academic support and enrichment 
activities are also available through the Target Excellence program. 
 

 

 

----

- 

----

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Tu Moua 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for James Marshall Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

James Marshall Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

15 

16 

20 

1 

0 

0 

0 

2 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.