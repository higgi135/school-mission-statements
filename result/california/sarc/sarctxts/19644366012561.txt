Mesa Elementary School currently serves 750 Transitional Kindergarten through fifth grade students with a strong emphasis on academic 
excellence and outstanding student achievement. Many families have had several generations of students attend Mesa Elementary School 
due to the quality education the school provides. A high level of community involvement and strong educational partnerships enrich Mesa’s 
learning environment. Hundreds of volunteers are dedicated to the ongoing support of the school’s vision as they donate thousands of hours 
each year. 
 
Mesa’s commitment to providing outstanding educational opportunities is reflected in the dedication of its teachers who routinely work well 
beyond the established school day. The staff is comprised of 25 regular classroom teachers, a Title I Intervention Teacher, two Special 
Education Teachers, a School Psychologist, a Speech and Language Specialist, two Transitional Kindergarten Teachers, five Dual Language 
Teachers, and 1 Adaptive Physical Education teacher. The Mesa staff is highly qualified with 100% of the teaching staff NCLB compliant, fully 
credentialed, and CLAD certified (or its equivalent). All of the teachers have been teaching five years or more. Mesa provides a strong support 
network for all teachers through collaboration and on-site mentorships. The staff is committed to preserving Mesa’s tradition of academic 
excellence by maintaining high expectations and standards for student achievement. Teachers employ a wide range of effective teaching 
strategies and instructional methodologies and engage in professional development activities to maintain a working knowledge of the findings 
of current educational research. Our critical priority this year is to provide effective first instruction, as well as develop critical thinking skills. 
Mesa teachers have been trained in Total Education System Support (TESS), and our teachers have helped the district develop and design 
lessons, pacing guides, and common assessments which will support Effective First Instruction. This year all of our teachers attended Common 
Core Training, Write from the Beginning and Beyond training, and Thinking Maps. All Mesa teachers will be trained for 12 hours throughout 
the year as we embark on a schoolwide mission to improve students' thinking and writing skills with an emphasis on narrative and expository 
writing (WFTBB). 
 
Mesa’s staff serves a diverse population as indicated by the 11 different languages spoken by Mesa students. Predominant groups include 
Hispanic (67%), Caucasian (14%), Asian (11%), Filipino (3%), and African American (3%). Services are provided for Gifted and Talented students 
(8%) who are clustered into GATE classes and receive differentiated instruction daily. English Language Learners (8%) are provided specially 
designed academic instruction in English by CLAD certified teachers. Our SED population has increased from last year with 48% of our students 
currently receiving Free & Reduced Lunch. We were awarded the Title I Achievement School Award, and this is the third year we will receive 
Title I funding. Students respect one another and take pride in their school. Mesa’s exemplary CAASPP scores are just one indicator of 
students’ determination to fulfill their highest potential. Mesa’s students continue to learn and have become student leaders, academic 
scholarship winners, and star athletes at the high school level. 
 
Mesa is proud of its many accomplishments. Through the fundraising efforts of dedicated volunteers, PTA has continued to provide students 
with access to specialized and up-to-date learning materials. Over the past three years our PTA and community have helped students raise 
over $45,000 to support educational opportunities including field trips for every classroom, as well as high quality assemblies. In 2003, Mesa 
opened a new and significantly larger library facility, which houses over 11,500 books, magazines, and reference materials for student use. 
Donations from our Spring Read-a-Thon purchased individual headphones for the incoming Transitional Kindergarten students, as well as 
computers, iPads, and new library books. Students have internet access in all classrooms. The Read-a-Thon, Book Fair, Family Movie Night, 
Walk-to-School Day, Book Character Parade, and Red Ribbon Week celebrations are just a few of the many activities regularly occurring on 
campus. 
 
The genuine caring and respect among the community, the skilled teaching staff, the extensive collection of educational resources, and the 
strong collaboration among all members of the Mesa community continue to support students in achieving academic excellence. 
 
MESA VISION AND MISSION STATEMENT 

2017-18 School Accountability Report Card for Mesa Elementary School 

Page 2 of 12 

 

Mesa Elementary School endeavors to provide a safe, educational environment which nurtures and empowers students to become balanced 
and productive citizens. As part of this vision, we hope to instill in our students a sense of integrity and compassion, a quest for knowledge 
and achievement, a respect for diversity, and an appreciation of their community. We strive to help students not only develop their 
intellectual intelligence, but their emotional intelligence as well. It is our expectation that when students leave Mesa Elementary School they 
will have a firm foundation of academic skills, as well as the ability to establish positive interpersonal relationships and collaborate with 
others. They will be able to discern right from wrong and make ethical judgments to become contributing members of society. Mesa 
Elementary School provides the keys to successfully unlocking the doors of the future and each child holds a key. 
 
 INSTRUCTIONAL FOCUS 
 Students at Mesa Elementary School will effectively communicate their reasoning through speaking, writing, and visual representation. These 
strategies will be applied in all content areas and supported through 3 research-based instructional strategies: 1) Thinking Maps; 2) Write 
from the Beginning and Beyond; and 3) ACE (Answer, Cite, Explain).