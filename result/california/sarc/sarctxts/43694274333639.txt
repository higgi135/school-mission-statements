James Lick High School, founding school of the East Side Union High School District, opened it's 
doors in 1950. The founding principles of the school were to focus on developing young adults to 
become impactful members of the Alum Rock and San Jose community through the focus on 
written communication, oral communication, and mathematical thinking and reasoning. While the 
demographics and times of have change in the past 68 years, the core values and dedication to 
serving the residents of East San Jose has not. In 2014, James Lick High School became James Lick 
High School - A New Tech School. James Lick is now part of a nationally recognized educational 
philosophy focused on preparing students for 21st century careers through the consistent focus 
around four major pillars: Communication, Collaboration, Agency and Growth Mindset. These 
pillars build on the founding members goals and aspirations. 
 
Mission 
The staff of James Lick High School – a New Tech School, empower students to own their futures 
through the application of skills and knowledge to solve real world problems 
 
Vision 
Every student who attends James Lick High School – a New Tech School graduates with the skills 
and knowledge to positively impact their community through their success in college and career 
 
Driving Question 
Individually we can dramatically impact a student’s life, what will the result be for our community 
if we align our individual efforts to support a shared goal? 
 

----
---- 
East Side Union High School 

District 

830 N. Capitol Avenue 

San Jose, CA 95133 

(408) 347-5000 
www.esuhsd.org 

 

District Governing Board 

Frank Biehl 

J. Manuel Herrera 

Van Thi Le 

Pattie Cortese 

Lan Nguyen 

 

District Administration 

Chris D. Funk 

Superintendent 

Glenn Vander Zee 

Assistant Superintendent 

Educational Services 

 

Chris Jew 

Associate Superintendent 

Business Services 

 

Dr. John Rubio 

Associate Superintendent 

Human Resources 

 

 

2017-18 School Accountability Report Card for James Lick High School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

James Lick High School 

With Full Credential 

Without Full Credential 

Teacher Credentials 

16-17 17-18 18-19 

57 

48.833 50.233 

5.3 

3 

0 

4 

0 

Teaching Outside Subject Area of Competence 

0 

East Side Union High School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

991.5 

50.6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

James Lick High School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

2 

0 

0 

2 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.