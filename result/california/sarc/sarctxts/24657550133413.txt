Creekside Junior High School opened its doors in August of 2016 to 700 students. It was built for a population of 1,100 students. 
Creekside Junior High School's enrollment has increased by 100 students each year since its opening in 2016. The Campus consists of 
four main buildings that include an Administrative building, two two-story classroom buildings and a multi-purpose room. There are 
a total of 34 classrooms, eight pull out rooms, community room, a band and choir room, and a library. Creekside Junior High School 
occupies 16 acres. 
 
Vision Statement: 
Creekside Students will demonstrate success through academics, critical thinking and social interaction. Creekside Students will excel 
in knowledge, technology and leadership to reach their highest potential and become respected members of our community 
 
Motto: 
Cougars go for the Gold! 
 
Creekside Junior High school staff is committed to ensuring that all Creekside students are independent thinkers who speak for 
themselves while understanding expectations and working to their highest potential. We are committed to ensuring that students and 
teachers have mutual respect creating a positive learning environment in which students feel safe to express themselves. We are 
committed to guiding, encouraging, motivating, and preparing all students for the real world. Creekside Junior High school works 
together to provide quality instructional programs including all core subjects, sports, the arts and music in order to produce a well-
rounded individual. Creekside Junior High School community works together in creating a positive, safe, supportive, compassionate 
and welcoming environment through implementation of PBIS (Positive Behavior Interventions and Support). We focus on fostering a 
sense of community to produce a well rounded individual through academics, critical thinking, proper social interactions and life 
lessons with real world applications. Creekside Junior High School provides a student-centered education through the implementation 
of common core standards through district created RCD units (Rigorous Curriculum Design). We focus on increasing the number of 
students that are eligible for college preparatory courses upon entering high school.