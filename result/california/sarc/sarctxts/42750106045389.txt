Cuyama Elementary is a rural school serving students from a forty mile radius, covering three 
counties. It is located in the scenic Cuyama Valley, educating kindergarten through eighth grade 
students taught by nine highly qualified teachers, one intervention teacher, and one special 
education teacher. Three instructional assistants work closely with teachers to address student 
needs. 
 
Cuyama Elementary School is dedicated to the ideals of academic excellence and to the personal 
and social development of our students. Academic integrity is fostered in a climate which respects 
the unique needs of each individual. Our students develop a positive self-image, respect for the 
rights of others, and the ability to communicate effectively, think critically, meet challenges, and 
accept responsibility. Cuyama has the following expectations consistently reinforced in all 
classrooms and areas of the school: Be Responsible, Excel Together, Actively Participate, Respect 
All, and Safety First. 
 
Our staff is continually looking to meet the needs of our students, forty-six percent of whom are 
English Learners, and eighty-two percent who are socioeconomically disadvantaged. Meeting 
those particular students’ needs have been a focus of staff professional development. One of 
Cuyama Elementary’s greatest strengths is the small class sizes. Students are respected learners at 
our school and develop personal connections with staff. 
 
Family involvement is increasing at Cuyama Elementary. Many parents are active members in 
School Site Council, the English Learners Advisory Committee, the District Advisory Council, and the 
Parent’s Club. There is a high participation rate in parent-teacher conferences and campus events. 
We are continually seeking avenues to encourage more family support. 
 
Rachel Leyland 
Principal 
 
 
 

2017-18 School Accountability Report Card for Cuyama Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cuyama Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

11 

10 

11 

0 

0 

0 

0 

0 

0 

Cuyama Joint Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

16 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Cuyama Elementary School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.