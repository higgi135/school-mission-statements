During the 2017-18 school year, the entire district staff embarked on a mission to revitalize the district by adopting a new overarching 
strategic plan to unify and provide common direction for the district’s elementary, middle, and high school. After exploring future 
trends for the year 2030 and beyond, analyzing student, community, and staff surveys and input, the staff cooperatively arrived at a 
focus on future Technology, Engineering, Arts, Mathematics, and Science (TEAMS)! This will help our current and future students to 
be prepared for participation in a changing global economy that extends far beyond the boundaries of their local community 
experience. A Visioning Task Force (teachers, classified staff, parent, administration, Board member) was assembled and met for two 
full-day meetings to discuss the TEAMS concept. The task force decided on a motto of, Maricopa Unified School District-Powered by 
TEAMS to guide, the T.E.A.M.S. approach. The Task Force developed a draft strategic plan based future trends to meet the needs of 
students and the community for the year 2030 and beyond. The Task Force reviewed, evaluated, and modified the district’s Vision, 
Mission Statements, and Tenets with the new TEAMS focus. 
 
OUR VISION (Promise for Tomorrow-2030 and Beyond) 
Maricopa Schools are TEAMS of adaptive learners, ready for future challenges and careers. Using Technology, Engineering, Arts, 
Mathematics, and the Sciences (TEAMS), our students will solve real-world problems. Through project-based learning, students are 
engaged, self- motivated, and self-directed. Students will gain technical expertise, balanced with collaborative communication skills 
to meet the everchanging demands of the future. 
 
Staff are facilitators in learning to support student achievement in all curriculum areas. Staff enhance technology competence by 
working with students to design high content, engaging, and satisfying student work. This supports the project-based learning involving 
TEAMS philosophies. Staff provides opportunities for students to create learning interests through content curriculum integration and 
connections. 
 
OUR MISSION (Promise for Today) 
The mission of the Maricopa Unified School District is to work in partnership with parents, students, and the community to provide an 
increasingly rigorous educational program. A caring and nurturing environment of belonging, promotes self-motivation, self-efficacy, 
and lifelong learning that will prepare students for the academic and technical expectations of higher education and industry. 
 
OUR TENETS (Promise of our Commitment) 
M-A-R-I-C-O-P-A USD is committed to support: 
 
Motivated Students 
Students will be self-motivated and excited to research and apply subject knowledge through innovative Technology, Engineering, 
Arts, Mathematics, and Science (TEAMS) teaching strategies. 
 
Academic Success 
Students will receive a personalized support program to ensure success in core curriculum content and future career exploration. 
 
Rigor 
Students are encouraged to apply critical thinking skills to identify and solve challenging real-world problems to meet rigorous state 
standards. 
 
Intelligence 
Students will foster different types of learning modalities to build intelligence and be well-rounded intellectually. 
 
Collaborative Communication 
Students will collaborate, express, and defend ideas using a variety of different communication techniques in a TEAMS environment. 

2017-18 School Accountability Report Card for Maricopa Elementary School 

Page 2 of 11 

 

 
Organized Students 
Students will be challenged to manage materials and be prepared to take ownership of their own learning, strategically and 
intentionally, to become self-directed learners. 
 
Prepared for College, Career, and Life Choices 
Students will progress through their educational career with the knowledge and practical experience to be successful for college and 
future career/life choices. 
 
Accountable Learners 
Students will be accountable for their learning and actions to become self-reliant and self-motivated life-long learners.