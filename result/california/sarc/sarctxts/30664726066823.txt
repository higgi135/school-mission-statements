School Description 
Los Coyotes Elementary School, located in the City of La Palma, is one of eight schools in the Centralia Elementary School District. Los 
Coyotes offers an exemplary program for students in kindergarten through grade six. Los Coyotes Elementary School is proud of its 
excellence in academic achievement. We believe that the success of our school directly correlates to a wonderfully supportive parent 
community, students with a true desire to learn, and a dedicated and highly skilled team of certificated and classified staff. Most 
recently Los Coyotes earned the prestigious California Distinguished School award in 2018 and the California Gold Ribbon award in 
2016. The school is a proud to be part of the OCDE Science Technology, Engineering and Math (STEM) program. Currently there are 
20 regular education teachers with a .5 special education teacher. 
 
A School-Wide Positive Behavior Intervention System (PBIS) program has long been established at Los Coyotes. The program focuses 
on instructing students in the school's core values; positive attitude, respect for self and others, integrity, dependability and excellence 
(or PRIDE). This program has tremendous staff and parent support, and focuses on reinforcing positive behavior and teaching 
appropriate behavior. In 2016-2018, Los Coyotes received a medals from the California PBIS Coalition's System of Recognition. 
 
A strong school community partnership exists allowing activities and organizations such as AYSO, Club Soccer, Little League, arts 
programs, the Jumping Coyotes, Girl Scouts and Boy Scouts to use our facilities. The Regional Occupational Program (ROP) available 
through Kennedy High School also provides instructional support services for students by qualified high school students during the 
instructional day. The La Palma Police Department provides instruction in drug prevention for our 6th grade students. The school 
participates in community events such as Red Ribbon Week Challenge in the Park, and various community-related philanthropies 
throughout the school year. 
 
School Vision 
As a leader in education, Los Coyotes School is dedicated to producing responsible, self-confident learners who master concepts, skills 
and positive character attributes through a comprehensive, technologically enhanced, standards-based curriculum. It is a positive, 
collaborative environment where new ideas are encouraged, and innovative practices are fostered. Student success is our top priority, 
and is valued by the entire community. 
 
Mission Statement 
In partnership with parents and the community, our school is dedicated to providing a challenging learning environment for all 
students which will enable them to reach their potential in becoming productive, contributing citizens. Staff members work 
collaboratively to provide students with a rigorous, standards-based curriculum and a safe place to learn. Individual creativity and 
contributions are promoted in a nurturing environment of cooperative spirit, positive attitude and mutual respect. Los Coyotes School 
provides exceptional staff, as well as opportunities for family and community involvement. Centralia School District is committed to 
meeting the diverse educational needs of all students and creating lifelong learners. 
 
Belief Statements 
All children can learn given time, tools & opportunity. 
All decisions should be student centered and data driven. 
Campuses should be safe, clean and inviting. 
Effective, highly-skilled staff should be employed. 
Technology should be available and utilized to enhance student learning and organizational efficiency. 
A systems approach organizational structure should be used for efficiency and clarity. 
All stakeholders: students, staff, parents and community members are valued and respected. 
 

2017-18 School Accountability Report Card for Los Coyotes Elementary 

Page 2 of 14