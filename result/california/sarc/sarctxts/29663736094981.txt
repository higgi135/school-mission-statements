Magnolia Intermediate School is located in the Lake of the Pines area between the communities of Grass Valley and Auburn. It is one 
of four schools in the Pleasant Ridge Union School District. Magnolia Intermediate has an enrollment of 387, serving students in grades 
six through eight. Zachary Pless is the Magnolia Principal and Rusty Clark is the Superintendent of the Pleasant Ridge Union Elementary 
School District. The school enjoys a stable and active community which supports the many and varied programs available for our 
students. Magnolia Intermediate School was recognized in 1988, 2001 and again in 2009 as a California Distinguished School, an honor 
earned by the district’s two elementary schools, as well. This recognition is awarded annually to only four percent of California’s 
schools. 
 
The educational needs of all children are recognized as extremely important. An accelerated math track is available for students who 
qualify. Teachers in other core curriculum areas differentiate instruction to address the needs and ability level of their students. In 
addition to the general education program, the special education program has been specifically designed to meet the needs of all 
special needs students through the collaborative educational expertise and specialized talents of our resource specialist designated 
instructional services, speech/language, and adaptive PE teachers and other necessary specialists. Magnolia has a diverse elective 
program that includes a vibrant performing arts program, bands, choir, and theater arts. Most seventh grade students take a 
College/Career Exploration class. The elective program includes Spanish, woodworking, art, yearbook, coding & robotics, and 
advanced computer skills and video production. 
 
An important element of the overall school program is the co-curricular and extracurricular activities that encourage and supports the 
involvement of more than fifty percent of the student body. In addition to the co-curricular components of the electives noted above, 
many students participated in interscholastic sports: volleyball, basketball, cross country, and track. Students are involved in 
countywide academic tournaments and the district's Odyssey of the Mind (OotM) program. 
 
Recognizing the need for additional time for students who may be falling behind or for students that have demanding extra-curricular 
schedules, we offer a lunch study hall available on a drop-in basis or as a mandatory assignment for make-up work two days per week. 
Magnolia's academic intervention programs are multi-faceted. Students deficient in basic skills are assigned to Academic Workshop 
classes designed to improve those skills. An opportunity center pulls students who are struggling to offer supports and goal setting 
interventions. Many teachers are available before or after school and during the lunch period to assist students. Sixth and seventh 
grade students who do not achieve at grade level are offered a three week summer school opportunity. 
 
There is a continued emphasis and focus on providing a successful academic and social environment for all students utilizing the latest, 
most attainable technology, innovative teaching strategies, and specific skills. The Rattler PIT and Den house Magnolia’s state of the 
art technology labs. Magnolia also has mobile iPad (30) and three Chromebook (90) labs. The labs are designed by teachers to be used 
for integrating technology with the daily curriculum. There are 105 Intel-based Macintosh iMac computers making it possible to teach 
three separate classes or accommodate three classes working on the same project. It is a project based lab, meaning that teachers do 
not have assigned times, but sign up by projects. 
 
Each year all sixth-grade students participate in a week-long science camp. This program provides the students in our community with 
an opportunity to gain an understanding of the relationships between man and his natural environment. Located in Sonoma County, 
Magnolia students work with University of California students at the UC Marine Biology Laboratory in Bodega Bay studying tide pool 
life, visiting Armstrong Redwoods State Park and participating in environmental study activities at Alliance Redwoods Camp. 
 
Magnolia Intermediate School is a Positive Behavior Intervention Support (PBIS) school. The School-Wide Positive Behavioral 
Interventions and Supports program is a systemic method for developing and supporting social competence and academic 
achievement. All students meet once a week in small groups to focus on social-emotional learning using videos, journaling, and 
restorative circles to facilitate this learning. 
 

2017-18 School Accountability Report Card for Magnolia Intermediate 

Page 2 of 10 

 

Mission Statement: Encourage, Engage, and Educate 
 
Vision Statement: Magnolia provides a safe and engaging environment where students develop academic, social and life skills to 
become productive, responsible citizens in a constantly changing global society.