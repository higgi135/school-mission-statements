Hunt Elementary is located in Newman, California, on the west side of Stanislaus County in 
Northern California. In its fifteenth year, Hunt Elementary is one of four, kindergarten through fifth 
grade, elementary schools in the Newman-Crows Landing Unified School District. The student 
enrollment at Hunt Elementary consists of 296 Transitional Kindergarten through Fifth grade 
students. In 2013 Hunt opened enrollment for transitional kindergarten students. The 
composition of our school includes fourteen General Education classes, one Resource class and one 
Intervention class. The demographics of our student body consists of 89.81% Free/Reduced School 
Lunch, 78% Socioeconomic Disadvantaged, 57% English Language Learners, and 10.2% Students 
with Disabilities. 
 
 
The campus at Hunt Elementary maintains a central office area, a library, and a multipurpose room 
which is used as our main cafeteria. In addition, we have a room designated to hold our 
Professional Learning Communities (PLC's) meetings, Individual Educational Plan (IEP's), Student 
Study Team (SST), and training. Hunt Elementary has an Intervention Classroom which houses the 
school-wide Intervention program as well as a STEM lab that is opened to teachers and students to 
teach robotics and STEM standards. Technology is integrated across the curriculum at Hunt 
Elementary, as every student from TK through grade 5 has access to a technological device. Every 
classroom has a block of library time that is dedicated to encourage a love for reading. Our library 
is also open to students before school, recess, lunch and after-school so that students are given the 
opportunity to read and take Accelerated Reading (AR) tests. Along with classroom P.E. time, 
students at Hunt get one day of P.E. time by our district P.E. team that focuses on physical fitness 
and nutrition. 
 
The after-school program at Hunt Elementary offers a safe environment for students to continue 
their learning while enriching them with music, art, athletics, computer literacy, homework support 
and academics. Hunt believes in nurturing and developing well-rounded learners. 
 
Along with rigorous instructional programs, Hunt Elementary creates a welcoming, thriving 
community for all students. We fully implement the Common Core State Standards, to educate our 
students. The standards define what all students are expected to learn at each grade level. 
Intervention and GATE programs are provided to offer supports as well as enrichment to students 
while technology is integrated into the instruction at all levels. Hunt Elementary has a PBIS program 
that offers social emotional supports as well as a counselor on-site three days a week to provide 
counseling services and assistance with behavioral interventions. Hunt maintains a character trait 
program which reinforces the core values that students need to become successful global learners 
of the 21st century. 
 
Hunt teachers, staff and administration uphold a collective efficacy as they work together to ensure 
that the California State Standards are implemented, differentiating instruction to meet the diverse 
needs of all learners. We continue to work collaboratively to implement the standards by creating 
and implementing skill-specific lessons, and using cutting-edge materials and assessments to 
monitor student progress. Our staff focuses on ensuring that a climate of professional learning is 
maintained, as well as climate which upholds the belief that all students can learn. With high 
expectations, along with best instructional practices, students at Hunt Elementary are ensured a 
learning experience that is full rigor and relevance. 
 

2017-18 School Accountability Report Card for Hunt Elementary School 

Page 1 of 8 

 

Hunt Elementary School maintains a vision that all students will graduate college and career ready, and that all students are prepared to 
be responsible global citizens and leaders. Through this vision, Hunt Elementary acknowledges and embraces the many cultural and 
ethnic similarities and differences of its students and staff, and encourages the contributions of all constituents towards a common goal 
of educational excellence. All site personnel nurture and cultivate core values such as kindness, responsibility, integrity, compassion and 
respect, while promoting a joy of lifelong learning. At Hunt Elementary, students, staff, families and community unite to achieve the 
common goal of academic success. Through rigor, relevance and relationships, the education at Hunt Elementary provides a learning 
experience which empowers and inspires all students to become successful lifelong learners.