Katherine Edwards Middle School serves approximately 785 students in 6th, 7th, and 8th grade students. The majority of students live 
within walking distance of the school, but many students are also bussed in from Pellisier and commute from uptown Whittier. Six 
elementary schools feed into Edwards at the conclusion of 5th grade: West Whittier, Mill, Orange Grove, Phelan, Jackson, and 
Sorensen. About 50% of Edwards graduates move on to Pioneer High School and 50% go to Whittier High School. Ninety-five percent 
of the students are Hispanic, 2% White, and 3% other ethnicities. Approximately 11% of the total student body are classified as English 
Language Learners. About 83% of the students receive Free and Reduced lunch. Edwards parents have a vast array of occupations, 
with 70% of the parents holding a high school diploma, and of these parents, 30% of those have attended some college and/or received 
a college degree. 
 
We have a highly qualified staff that offers a rich standards-based curriculum. Our master schedule consists of an 6 period day. 
Students are offered a variety of electives focused on our school theme, STEAM, such as beginning and advanced band, chorus, guitar, 
Engineering, Robotics, Forensics, Coding, Drama and Public Speaking, We are also a Dual Immersion Academy where some students 
take courses in both English (ELA, Math, PE, and elective) and Spanish (ELA, Science, Social Studies). Student Leadership (ASB), Media 
Art and Art. Additionally, students that require additional support in either math or language arts to meet grade level proficiency are 
scheduled into an intervention class during the school day. An Honors strand is provided at each grade level for our students excelling 
in Math and Language Arts. Our school recognizes the importance of literacy, therefore, there is a sustained focus on close reading 
and interacting with the text. Positive Behavior Intervention and Support is a focus on the campus to enable students to learn from 
their actions and grow in a positive learning environment. Students are taught lessons in tolerance, conflict resolution, social skills and 
individual accountability to take responsibility for their actions in helping maintain a safe environment for all students. 
 
MISSION STATEMENT 
Katherine Edwards Middle School is focused on providing academic achievement that promotes growth and success in the 21st century 
to enhance the abilities of all students in a dynamic global community. Building skills in Science, Technology, Engineering and Math 
provides the cornerstone for each student’s learning experience. Our school cultivates equity for all students while focusing on self-
esteem and confidence, patriotism, and acceptable behavior fostered in a safe environment. Our collaborative school culture is 
academically rigorous where every student is equipped with the tools to be both a life-long learner and a successful, productive, 
citizen.