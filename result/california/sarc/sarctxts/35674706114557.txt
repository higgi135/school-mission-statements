Gabilan Hills (GH) was built in 1997 and is a modern, beautifully maintained campus. Our 
community school serves students in grades K-5. Gabilan Hills currently shares the campus with the 
Hollister Dual Language Academy (HDLA). Gabilan Hills School staff knows each student by name 
and by need. 
Gabilan Hills (GH) is a Professional Learning Community (PLC). It is clear in our mission, vision, values 
and goals that we guarantee each student achieves grade level standards, we work collaboratively, 
we agree on essential learning that all students will acquire, we agree on how students will 
demonstrate their learning and agree on what to do if they did not learn it. 
Gabilan Hills Professional Learning Community (PLC) Mission: 
Our professional learning community (PLC) will ensure each student achieves high levels of 
academic excellence in an engaging and nurturing atmosphere. 
 
Gabilan Hills Professional Learning Community (PLC) Vision: 
 
Gabilan Hills learning community works in committed, collaborative teams to deliver interactive, 
skill-building lessons that enable students to demonstrate their understanding. Student success will 
be measured by formative, summative, and nationally normed assessments. Data teams will make 
changes for intervention, enrichment, presentation, and methodology to meet the needs of each 
individual student. Gabilan Hills students will attain foundations for success as they advance 
through grade levels, inspiring them to become life-long learners. 
 
Gabilan Hills Professional Learning Community (PLC) Values: 

• 

• 
• 
• 

Teams will commit to scheduled meetings with grade level and cross level teams to 
evaluate student needs. 
Teams will make curriculum adjustments to meet student needs based on data. 
Teams will commit to action plans developed by the PLC. 
Individual progress will be discussed and charted with students to realize personal 
growth. 
Student growth will be celebrated. 

• 
• We will maintain a positive atmosphere that encourages all students to succeed. 
• We will all adhere to school wide norms developed by the PLC. 

Goals: 

---

- 

-------- 

Hollister School District 

2690 Cienega Rd. 

Hollister, CA 95023-9687 

(831) 630-6300 
www.hesd.org 

 

District Governing Board 

Stephen Kain, President 

Jan Grist 

Carla Torres-Deluna 

Jan Grist 

Elizabeth Martinez 

 

District Administration 

Diego Ochoa 

Superintendent 

Jennifer Wildman 

Assistant Superintendent, 

Educational Services 

Erika Sanchez 

Assistant Superintendent, Human 

Resources 

Gabriel Moulaison 

Assistant Superintendent,, Fiscal 

Services 

Barbara Brown 

Acting Director, Student Support 

Services 

John Teliha 

Director, Facilities 

Jr. Rayas 

Director, Technology & Innovation 

• Each student will achieve grade level proficiency and beyond. Students will show 

Caroline Calero 

Director, Learning & Achievement 

Ann Pennington 

Director, Student Nutrition & 

Warehouse 

 

measurable growth in Language Arts and Mathematics. 

• Current data will determine intervention, support and enrichment. Intervention and 
support will be in the areas of Language Arts and Mathematics. Enrichment may include 
additional activities in Science, Social Studies, Technology, and the Arts. 

• Teachers use data to hold motivational discussions with students to set individual goals, 
helping them to be active, enthusiastic learners that are responsible for their own 
growth. 

• Teachers will maintain consistency of curriculum and core academic skills to ensure 

students successfully transition through grade levels. 

• Recognition of student academic or social achievements will be celebrated at least once 

monthly in the classroom and each trimester school-wide. 

Gabilan Hills students are ethnically and socio-economically diverse. Our school has an enrollment 
of approximately 209 students in 8 K-5 classrooms. Kindergarten classes provide a full day program. 
Approximately 78% of our students qualified for the Free and Reduced Lunch Program and 67% are 
English learners. 

2017-18 School Accountability Report Card for Gabilan Hills School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Gabilan Hills School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Hollister School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

13.5 

3 

0 

8 

2 

0 

9 

3 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

240 

35 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Gabilan Hills School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.