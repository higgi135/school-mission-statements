Our Mission Is Learning 
Empowering learners to be leaders in the 21st century through collaboration, creativity, and critical thinking. 
 
Las Flores Elementary School was named a California Distinguished School in 2002. We are proud of our academic achievement. The 
school is located at 25862 Antonio Parkway in the community of Las Flores. Las Flores Elementary School has approximately 500 
students enrolled in Grades Pre K-5. The campus is shared with Las Flores Middle School. The school houses a fee-based preschool, 
State-funded preschool, Special Day Class preschool and Transitional Kindergarten. 
 
Most students who attend Las Flores Elementary School are from families whose parents represent middle or upper income levels. 
 
Our school facility contains state-of-the-art equipment inside its 29 classrooms, four small group instruction rooms, and three teacher 
workrooms. We share administrative offices, a multipurpose room, and a library-media center with Las Flores Middle School. 
 
Our primary academic focus centers on the core academic areas of reading literacy and mathematics. In addition to learning the basic 
skills, our students participate in hands-on learning activities, simulations, dramatizations, assemblies, and field trips. Las Flores 
Elementary has been named on the California Business for Education Excellence Honor Roll and has twice received the $5,000 
Governor’s Reading Award. This award was the direct result of our students’ participation in the Reader Leader and Best Foot Forward 
reading programs—each of which encourage the support of reading at home. 
 
Technologically, computers are available in all classrooms. We have a PC lab accessible to students and staff. Grades 4 and 5 have 
Chromebooks for every student. Technology is a focus here and is integrated into the core curriculum. Our classrooms are equipped 
with a fiber optic delivery system and each room is connected to the Internet allowing students to research and extend learning 
beyond the traditional classroom. 
 
For additional information about school and district programs, please visit www.capousd.org.