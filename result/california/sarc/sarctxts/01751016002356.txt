I'd like to welcome you to Walnut Grove Elementary School's Annual School Accountability Report 
Card. In accordance with Proposition 98, every school in California is required to issue an annual 
School Accountability Report Card that fulfills state and federal disclosure requirements. Parents 
will find valuable information about our academic achievement, professional staff, curricular 
programs, instructional materials, safety procedures, classroom environment, and condition of 
facilities. 
 
It is a privilege and an honor to serve as the principal of Walnut Grove Elementary School. Our staff 
of 60 classified and credentialed professionals work collaboratively to meet the needs of over 750 
students. Through ongoing collaboration, our teachers have developed a learning environment that 
exemplifies high academic standards and age appropriate socio-emotional development. Our 
experienced staff meets student needs through both data analysis and 
individualized, 
differentiated instruction. Our school-wide TRIBES program is centered around the healthy 
development of every student so that each has the knowledge, skills, and resiliency to successfully 
meet the requirements of our rapidly changing world. In TRIBES learning communities, the power 
of being included and valued by peers motivates students to actively participate in their own 
learning. Our school community actively embraces the TRIBES philosophy and it is at the center of 
every educational and developmental decision making process that occurs at our school. Walnut 
Grove Elementary School received the 2013-14 California Distinguished School Award! 
 
We have made a commitment to provide the best educational experience possible for Walnut 
Grove Elementary School's students, and welcome any suggestions or questions you may have 
about the information contained in this report or about our learning community. 
 
Mission Statement 
To capitalize on our continuous improvement process to enhance student achievement. 
 
Our Goals… 

• 
• 

• 

• 

To create a collaborative and inclusive school culture 
To provide a balanced curriculum that attends to the arts, the sciences and to character 
development 
To promote high levels of academic achievement while meeting the needs of all 
learners 
To create children fit to command through the 21st century and beyond 

2017-18 School Accountability Report Card for Walnut Grove Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Walnut Grove Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

36 

33 

36 

0 

0 

0 

0 

 

1 

Pleasanton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

600 

11 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.