Steinbeck Elementary fosters a mutually respectful and positive learning environment promoting 
self-accountability through active engagement, effective and data driven instruction with 
opportunities for students to create, collaborate, communicate and think critically to achieve goals 
aligned with 21st century academic standards of success. The mission for Steinbeck in alliance with 
Central Unified's guiding principles vision and mission based on the belief that every child can learn 
while embedding 
innovation and continuous 
improvement. 
 
Steinbeck School is located within the city limits of Fresno and serves students in transitional 
kindergarten through sixth grade. Construction on Steinbeck was completed in Spring 1995, and 
the first students began in July 1995. The entire Steinbeck team is committed to providing a 
comprehensive academic program in a learning environment that maintains high academic and 
behavioral standards and promotes success for all students. Steinbeck Elementary School, with an 
attendance area of approximately 5 square miles, lies in a combination of an urban and agricultural 
setting. Steinbeck has 30 classroom teachers, one instructional support coach, one intervention 
teacher, a behavior intervention counselor and a school psychologist, a speech therapist, two music 
teachers, one physical education teacher, three custodians, a principal's secretary, a clerk typist II, 
a nurse and health aide, a guidance instructional advisor, and a principal. 
 

----
---- 
Central Unified School District 

4605 North Polk Ave. 

Fresno, CA 93722 
(559) 274-4700 

www.centralunified.org 

 

District Governing Board 

Mr.Jason R. Paul, Area 1 

Ms. Yesenia Z. Carrillo, Area 2 

Mr. Phillip Cervantes, Area 3 

Mr. Richard Atkins, Area 4 

Mr. Richard A. Solis, Area 5 

Mrs. Terry Cox, Area 6 

Mr. Naindeep Singh Chann, Area 7 

 

District Administration 

Andrew G. Alvarado 

Superintendent 

Mr. Kelly Porterfield 

Assistant Superintendent, Chief 

Business Officer 

Mrs. Ketti Davis 

Assistant Superintendent, 

Educational Services 

Mr. Jack Kelejian 

Assistant Superintendent, Human 

Resources 

Mrs. Andrea Valadez 

Administrator, Special Education & 

Support Service 

Mr. Paul Birrell 

Director, 7-12 & Adult Education 

Dr. Tami Boatright 

Director, K-8 Education 

 

2017-18 School Accountability Report Card for John Steinbeck Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

John Steinbeck Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

30 

29 

35 

1 

0 

1 

0 

0 

0 

Central Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

727 

20 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.