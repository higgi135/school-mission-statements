The Mission of Shadow Hills is to deliver rigorous, standards-based instruction to all students, while providing a safe environment and 
encouraging personal accountability in order to provide opportunities for success in society. 
 
The Shadow Hills Magnet Academy's staff will build a school atmosphere that is respectful and responsive. A warm, welcoming 
environment for all of the educational community will be fostered, along with an awareness of the fact the intermediate school 
experience is a vital bridge between Elementary School and High School. Priorities include reinforcing the Palmdale School District 
character traits of being trustworthy, responsible, respectful and fair. 
 
The Vision of Shadow Hills: Shadow Hills Magnet Academy engages students in a rigorous curriculum, emphasizing positive 
accountability and respectful interaction school-wide. 
 
School Description: 
Shadow Hills Magnet Academy is a sixth, seventh and eighth grade school located in Palmdale, California. Shadow Hills opened its 
doors in 1998, and currently serves a population of 1001 students. Our population is becoming more diverse with each passing year. 
Our student population is 16% African American, 1% Asian, 77% Hispanic/Latino, 1% American Indian, 1% Two or More Races, and 4% 
White. Twelve percent of our student population qualifies as Limited English Speaking Students, with the majority of them having 
Spanish as their native language. Nineteen percent of our student population qualifies for Special Education Services. Shadow Hills 
has 11% of their student population identified as Gifted and Talented. Due to our high percentage of students on free and reduced 
lunch (81%), Shadow Hills is a school wide Title I program school. 
 
Shadow Hills is very proud of its 44 member teaching staff. The average years of teaching experience at Shadow Hills is 16 years, with 
100% of teachers being fully credentialed, and 100% are CLAD certified. The school consists of 37 general education classroom 
teachers, and a Refocus Room teacher. High Achieving and GATE students are clustered together in Honors Classes broken up by 
grade level. Elective classes are provided in the areas of Music; Project Lead The Way - Medical Detectives, Flight and Space, Computer 
Programming, Automation; Business Computers; AVID; Robotic;, Graphic Design; Geometric Design, Art; Aviation; and Computer 
Game Design. Four of our teachers provide a well rounded physical education experience to all students. 
 
Special Education services are provided by 3 Special Day Teachers, 3 Resource Specialists, a shared Speech Teacher, and a Psychologist. 
Instructional aides/ paraeducators are provided for each of the Special Education classes. 
 
The office staff consists of a Principal, 2 Assistant Principals, Secretary, Media Technician, Attendance Clerk, Library Aide, Health Aide, 
Parent Liaison, Administrative Clerk, Guidance Counselor, and Social-Emotional Learning Specialist. Additionally, in accordance with 
the MSAP Grant, we have a MST (Middle School Teacher on Special Assignment). 
 
Shadow Hills Magnet Academy received a six year WASC accreditation in 2017. 
 
 
 

2017-18 School Accountability Report Card for Shadow Hills Magnet Academy of Engineering & Design 

Page 2 of 11