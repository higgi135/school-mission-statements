William Howard Taft Elementary School/Taft DHH School (Taft School) is located at the corner of 
Keller Avenue and Pasteur Street. The mission of Taft School is fulfilled by highly competent staff 
working in concert with parents and outside resources. School staff are committed to celebrating 
the uniqueness of the individual and guiding each child to fulfillment of his or her potential. All 
children can learn - it is the commitment of Taft School to educate its students for positive and 
varied roles in society. 
 
Taft School has a large concentration of students with special needs, including a regionalized deaf 
and hard of hearing program. Over 16 classrooms are dedicated to servicing these special education 
students based upon their individual needs and abilities. Taft School hosts a special education 
infant/preschool program for students up to age five. Identified students have an IFSP 
(Individualized Family Service Plan) or an IEP (Individualized Education Plan). 
 
School Vision: 
 
Our vision is to maximize student achievement by: 

• Providing high quality, standards-based instruction for students in the two programs 
offered at Taft School: Elementary TK-6 (general and special education) and Deaf and 
Hard of Hearing (DHH) 

• Providing a school-based system of accountability measures, which will ensure 

continuous progress for all students 

• Providing teachers with training, which includes research-based strategies and best 

practices 

• Providing innovative programs in order to give students the opportunity to excel in 

higher level thinking skills for all curricular areas 

• Partnering with the community to provide a wide variety of support for our students 

and families 

• Providing opportunities for parents to be meaningfully involved in the education of 

their children 

• Providing students with positive character trait expectations of being "respectful, 
responsible and safe," utilizing the tenets of the Positive Behavior Intervention 
Supports (P.B.I.S.) program 

• Providing a college/career focus by implementing S.T.E.A.M. (Science, Technology, 

Engineering, Arts, Mathematics) curricula 

 
School Mission: 
 
Our mission is to promote a partnership that includes students, staff, parents, and the community 
to ensure the academic success of all students. Teachers and support staff provide a rigorous, 
standards-based instructional program to Taft's diverse student population. Instruction is data-
driven to meet grade level expectancy and individual needs. A strong focus will be the Common 
Core State Standards as teachers are trained and implement this high rigor of instruction. 
 

----

---- 

Santa Ana Unified School District 

1601 East Chestnut Avenue 
Santa Ana, CA 92701-6322 

714-558-5501 
www.sausd.us 

 

District Governing Board 

Valerie Amezcua – Board President 

Rigo Rodriguez, Ph.D. Vice – 

President 

Alfonso Alvarez, Ed.D. – Clerk 

John Palacio – Member 

 

District Administration 

Stefanie P. Phillips, Ed.D. 

Superintendent 

Alfonso Jimenez, Ed.D. 
Deputy Superintendent, 

Educational Services 

Thomas A. Stekol, Ed.D. 
Deputy Superintendent, 
Administrative Services 

Mark A. McKinney 

Associate Superintendent, Human 

Resources 

Daniel Allen, Ed.D. 

Assistant Superintendent, K-12 

Teaching and Learning 

Mayra Helguera, Ed.D. 

Assistant Superintendent, Special 

Education/SELPA 

Sonia R. Llamas, Ed.D., L.C.S.W. 
Assistant Superintendent, K-12 
School Performance and Culture 

Manoj Roychowdhury 

Assistant Superintendent, Business 

Services 

Orin Williams 

Assistant Superintendent, Facilities 

& Governmental Relations 

 

2017-18 School Accountability Report Card for William Howard Taft Elementary School 

Page 1 of 11 

 

Positive Behavior Interventions and Supports: 
 
Taft has implemented Positive Behavior Interventions and Support (PBIS), a school-wide discipline program that utilizes proactive 
strategies for defining teaching and supporting appropriate student behaviors. At Taft, PBIS encompasses an integrated approach that 
focuses on prevention, instruction, and intervention by way of Taft's PBIS coaches, its PBIS team and staff members. A positive school 
climate and support for all students is implemented in both classroom and non-classroom areas utilizing "Tiger Tickets", students of the 
week and other positive reinforcements which are a daily part of life at Taft Elementary School. 
 
Taft and Target's (TnT) Fit to Read Lab: 
 
In January of 2012, Taft Elementary School was the recipient of a $100,000 unrestricted grant from Target and the Ellen DeGeneres Show. 
The funds have been used to create a one-of-a-kind exercise and reading lab that utilizes gym-quality recumbent bicycles coupled with 
student reading activities. The purpose of the lab is to measure and take advantage of the effects of exercise and reading. 
 
In October 2014, Taft opened up a completely remodeled state of the art library and digital learning center courtesy of 'Heart of America' 
along with volunteers from Target. 
 
Fun Fitness Fridays: 
 
As a support to both Taft's PBIS program and physical fitness goals, Taft has implemented Fun Fitness Fridays. This is a school-wide, 
physical education activity which focuses on 8 different centers of enriched physical activity where students are constantly engaged. The 
activities range from jump ropes to hula-hoops, kick boxing, and basketball drills. All activities are designed and led by student leaders 
from our 5th-6th grade classes. 
 
District ProfileSanta Ana Unified School District (SAUSD) is the seventh largest district in the state, currently serving nearly 49,300 students 
in grades K-12, residing in the city of Santa Ana. As of 2017-18, SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high 
schools, 3 alternative high schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or Reduced 
Price Meal program, 39% qualifying for English language learner support, and approximately 13% receiving special education services. 
Our district’s schools have received California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I 
Academic Achieving Schools, and Governor’s Higher Expectations awards in honor of their outstanding programs. In addition, 20 schools 
have received the Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have developed and maintained high 
expectations to ensure every student’s intellectual, creative, physical, emotional, and social development needs are met. The district’s 
commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high quality educational 
program. Consistent success in meeting student performance goals is directly attributed to the district’s energetic teaching staff and 
strong parent and community support. 
 
 

 

2017-18 School Accountability Report Card for William Howard Taft Elementary School 

Page 2 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

William Howard Taft Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

37 

37 

36 

0 

0 

0 

0 

0 

0 

Santa Ana Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1986 

0 

11 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.