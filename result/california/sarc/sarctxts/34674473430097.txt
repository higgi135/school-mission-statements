MISSION STATEMENT 
Recognizing the fact that individuals learn in different ways and at varied paces, the mission of SJUSD alternative high schools is to 
guide and inspire each student’s pursuit and discovery of knowledge and achievement of the skills necessary for responsible 
citizenship, meaningful employment, and lifelong learning through innovative, challenging curricula delivered in a dedicated and 
collaborative learning community. 
 
SCHOOL PROFILE 
La Entrada Continuation High School has been the district's primary continuation program for 16-18 year old students for over 20 
years. Students are primarily credit-deficient 11th and 12th graders who either enroll voluntarily for credit recovery, or are placed by 
the SJUSD's Student Review and Intervention Program. La Entrada's instructional model utilizes individual student learning on 
computers. Up to 60 students at a time are in a large computer lab/classroom. Instruction and academic tutoring is provided by two 
teachers and three instructional assistants. There are two 210 minute sessions each each school day, serving two different groups of 
students. 
 
La Entrada is the only self-contained continuation high in the San Juan Unified School District. The school serves grades 10 thru 12 
with the understanding that all students enrolling must be sixteen years of age. La Entrada serves the nine comprehensive high schools 
within our district as well as incoming out of district students who meet our criteria. Students enroll at our school either through a 
voluntary process mainly due to credit deficiency or students involuntarily due to discipline issues. Students at our school are 
representative of a wide diversity of cultures and socioeconomic groups. La Entrada is located at 10700 Fair Oaks, Fair Oaks, CA 95628. 
The school was established in 1973 on the former site of an elementary school. 
 
PRINCIPAL'S MESSAGE 
We are proud to be serving our most at-risk students in an alternative program that is tailored to the individual needs of each student. 
The La Entrada's curriculum model is designed to help each students reach the goal of high school graduation, advanced post-
secondary education, and career preparation. Our staff is trained to build positive relationships with students in order to gain trust, 
learn each student's needs, and guide them toward their goals.