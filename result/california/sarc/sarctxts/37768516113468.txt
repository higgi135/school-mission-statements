Vivian Banks is a very special place for learning. The setting is bucolic, with a magnificent view of snow-capped Palomar Mountain 
available most days and a rural setting that lends itself to a myriad of learning opportunities. It is on the grounds of the Pala Mission, 
an auxiliary site of the nearby San Luis Rey Catholic Mission. The School is named after Vivian Banks, a Native American who served 
on the District Board of Trustees from 1944 to 1990. 
 
As a small school VBCS is able to provide students an individualized and personal educational experience with small classroom sizes 
(average 17 students). Established in 2006, Vivian Banks Charter School has served generations of Pala families. Vivian Banks Charter 
School (VBCS) is located on the Pala Indian Reservation, within the Bonsall Unified School District (BUSD) existing boundaries. 
 
The Vivian Banks Charter School is a learning community committed to the cultural awareness, social and physical development, and 
intellectual growth of each student. It establishes a learning environment that fosters respect for self and others, responsibility, and 
enables students to meet present needs and future challenges in a complex, changing society. 
 
The VBCS educational program serves students with a diverse background and of all ability levels from kindergarten through fifth 
grade. Our students will be able to apply basic and higher order thinking skills, use technology, and develop effective interpersonal 
communication, particularly in the area of self-management. A variety of technologies are used to enhance teaching and learning. 
The school fosters unity by recognizing our common heritage and, within it, the unique contributions of each individual. 
 
Currently our school serves approximately 104 students. We provide a wide array of academic support programs for children who 
require special assistance in mastering academic standards. A Multi-Tiered System of Support (MTSS) is provided that includes an 
academic intervention program in English Language Arts and Math, Resource Specialist support, summer school, no-cost before and 
after school programs, small class sizes, bilingual English Learner support, full-time PE teacher, music teacher, iPads, Smart Boards, 1-
1 Chromebooks in grades 2-5, as well as extracurricular enrichment opportunities i.e. running club, student council, native language 
classes (Cupeno), parent classes, After School Education and Safety Grant (ASES). 
 
Vivian Banks also has a counselor one day per week to support students who need emotional and/or behavioral support. Depending 
on student need, the school counselor either provides one-on-one support or small group support focused on appropriate peer 
interactions. The school counselor also supports students in making positive choices as well as delivers push-in character lessons to all 
classrooms. 
 
VBCS staff not only cares about every child’s academic achievement, but also their social and emotional development. We have clear 
guidelines for behavior that are focused primarily on the Character Counts Traits. These traits include Citizenship, Trustworthiness, 
Respect, Caring, Responsibility, and Fairness. These positive behaviors are reinforced daily by student/staff interactions and classroom 
instruction by teachers and counselor. 
 
Over the past 20 years the Pala Band of Mission Indians has demonstrated an unwavering commitment to the education and social 
well-being of VBCS students. The Pala Band of Mission Indians plays an active role in its support and commitment to the school and 
the students who attend here. 
 
Visit our school and you will see an atmosphere of high energy and on-task learners, caring and expert instructional staff, and dedicated 
and caring parents who value the balance of academics and character development for all students. 
 

2017-18 School Accountability Report Card for Vivian Banks Charter School 

Page 2 of 11 

 

Mission Statement 
Vivian Banks Charter School will create an environment for learning where students and teachers are learners, where parents are fully 
involved, where thinking and character are values, where a relationship of trust and integrity exists, where risk is expected behavior, 
and where the culture and background of each learner is recognized and valued. 
 
Vision Statement 
“Academic excellence and support for all students to be highly competitive in their chosen career path and college" 
 
Philosophy 
Continuous Improvement