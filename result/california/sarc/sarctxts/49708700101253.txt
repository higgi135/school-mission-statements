It is the mission of Jack London Elementary School to be a caring community of students, educators, and families dedicated to the 
common goal of providing all students with the knowledge and abilities necessary to succeed academically, socially, and emotionally 
now and for the rest of their lives. 
 
Jack London Elementary School, the District’s fourth school, opened in September 2003. It is located on the edge of a residential 
neighborhood with rural areas surrounding it. Currently, there are approximately 100 new homes currently under construction. The 
school currently has 290 students in kindergarten through sixth grade. A YMCA childcare program is housed in one of the classrooms 
and the Boys and Girls Club also runs an after school program on the campus. The campus is also home to a separate charter school 
for seventh and eighth graders with 200 students in eight additional classrooms. 
 
Grades kindergarten through 3rd grade are capped at 24 students. Grades 4th through 6th have an average of 28 students per class. 
Instructional assistants offer instructional support to all students in each classroom based on the needs of our students, with an 
emphasis on supporting primary grades to build a strong foundation for students. Students have regular access to 1:1 digital devices 
in grades 2-6. Our kindergarten and first grade have access to 2:1 devices in the classroom and can access other digital tools in the 
library as needed. 
 
There are eleven regular classrooms teachers. Six classified staff members serve in combination as instructional assistants in regular 
classrooms, the resource specialist program, and the Title I and Title III ELL Assistant program as well as yard duty. We have full-time 
PE and library technicians who provide instruction, support and facilitate activities during lunch and recess. Some of our support staff 
are shared with the junior high school; our resource specialist, school counselor, licensed vocational nurse and lead custodian are 
shared with the charter school, as is our health technician who dispenses medications, updates records, and notifies staff of student 
medical information. We also offer music with a credentialed music teacher two days per week. 
 
Special programs include the Special Day Class (SDC), Resource Specialist Program, ELD, Speech and Language Program, and Adaptive 
Physical Education. Students in the Resource Specialist Program are seen in pull-out programs as well as through collaboration with 
the regular education teacher in the classroom. Our SDC classroom is a self-contained classroom with opportunities for mainstreaming 
throughout the day. Program Assistants work with classroom teachers in the regular classroom to support Language Arts instruction 
and provide both group and individualized intervention support. 
 
The school facility includes twenty-two regular classrooms (fourteen used by our elementary school and eight by the charter school); 
an RSP classroom; small instructional rooms for reading, speech, and counseling; and an SDC classroom and one other instructional 
classroom. There is a Creative Media building which has a library, creative lab, project room, staff room and staff workroom. Each class 
is scheduled to visit the library at least once a week. The facility also includes a full gymnasium with kitchen attached to heat prepared 
meals. 
 
 

2017-18 School Accountability Report Card for Jack London Elementary School 

Page 2 of 11