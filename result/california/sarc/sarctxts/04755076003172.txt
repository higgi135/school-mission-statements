Wilson Elementary School is located in Gridley, California, at the heart of the Sacramento Valley. With a student enrollment of 593, 
the staff and students work diligently to emulate the same small-town feel as its home city. The second through fifth grade school has 
a staff of 23 classroom teachers, 2.5 intervention teachers, three special education teachers, two physical education teachers, one 
counselor and a speech therapist. 
 
The design of our school supports high quality programs in a variety of ways including: 

Every classroom is equipped with 1:1 Chromebooks 

Storage areas for PE and recess equipment 

• Professional development room for staff training, meetings & grade level collaboration 
• 
• Well-stocked classroom libraries 
• 
• A playground area which includes: a grass field, two play structures, and asphalt surfaces 
• 
• 

Teacher computer workstations, projectors, and document cameras in each classroom 
School library equipped with books & iPads 

 
As Gridley Unified School District's mission statement claims, we are dedicated to ensuring a quality education in a safe, nurturing 
environment that produces responsible, compassionate individuals by providing them a relevant curriculum enabling all students to 
become productive citizens strengthened by the small town quality of life. 
 
Mission- Wilson Rams will succeed by being safe, kind and responsible! 
 
Vision- 

• Wilson Elementary School teachers create a safe, nurturing environment that challenges students to rise to their highest 

potential. 

• We strive towards the common goal of academic success and social/emotional well-being of every student. 
• Our staff, with the support of our parents and community, ensures that ALL students succeed and become critical and global 

visionaries through engaging and relevant experiences.