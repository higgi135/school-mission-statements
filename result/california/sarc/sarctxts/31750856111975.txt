Breen Elementary School opened in August 1994 and is one of seventeen schools in the Rocklin 
Unified School District. It serves students in kindergarten through 6th grade, with an enrollment of 
approximately 520 students. Breen is located in the center of Stanford Ranch, a dynamic and 
supportive community. The staff at Breen provides an exemplary learning environment that is 
positive and nurturing with very high academic and behavioral standards. Our primary core belief 
is that all children can learn at high levels. 
 
Parent participation is appreciated and strongly encouraged at Breen. We're fortunate to have 
hundreds of parent volunteers that work to improve the academic and social programs at the 
school. Parents also actively participate with the School Site Council and Parent Teacher Club. 
Parents and staff are dedicated to creating a safe learning environment in which students interact 
positively with each other and reach their academic potential. Breen’s Positive Behavior 
Intervention and Supports (PBIS) Program has enhanced this atmosphere and stresses positive 
relationships. Our Bobcat Awards reward program encourages students to be people of high 
character. Breen Elementary was awarded the Silver Level by the California PBIS Coalition for their 
implementation of PBIS. 
 
Classroom activities are designed to accommodate the variety of ways in which children learn best 
-- using all their senses while being actively engaged. Full time P.E. and VAPA (visual and performing 
arts) teachers provide students with standards-based P.E. and arts curriculum . Breen’s facilities 
are also used in the afternoon, evening, and weekends by members of our community for such 
activities as basketball, soccer, boy/girl scouts, etc. Breen was selected as a California Distinguished 
School in 2002 and 2008. 
 
Mission Statement 
The mission of Breen Elementary, a dynamic educational and nurturing school community, is to 
inspire our students to achieve their academic potential, to ignite a passion for life-long learning 
and to become responsible, well-rounded citizens by: 
1) fostering a collaborative environment where each student's unique potential is recognized, 
cultivated, and celebrated. 
2) developing a culture where innovation is paramount. 
3) creating strategic partnerships between the school, community and its families. 
 
 

2017-18 School Accountability Report Card for Breen Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Breen Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

26 

27 

25 

0 

0 

0 

0 

1 

0 

Rocklin Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

590 

5 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Breen Elementary School 

16-17 

17-18 

18-19