Our Vision: 
In partnership with our families and community, we are creating a culture of 21st Century Learning engaging our students in college 
and career readiness, where everyone is a learner, everyone is a teacher, and everyone is a leader! 
 
Our Mission: 
The mission of Serrano Elementary School is to become a high performing community school that engages all students in exemplary 
teaching and learning experiences that are rigorous, differentiated, meaningful and take place in a safe and orderly environment! 
Scholars will be well equipped to impact, influence and contribute to a global society, with the freedom to explore and develop their 
strengths! 
 
We will prepare our students for college and career readiness, which includes 21st Century Learning and increased academic 
achievements for ALL students. Through our collaborative Professional Learning opportunities we will develop instructional strategies 
via data collected from formative and summative assessments, provide strategic intervention opportunities to identified students, 
utilize interim assessments to measure growth and provide practice, progress monitor to ensure individual growth and use additional 
strategies via Serrano Multi-Tiered Systems of Support and CAST. 
 
ELA- Identified SBAC claims Speaking and Listening and Reading. We will deconstruct the standards and ensure specific speaking and 
listening opportunities are offered in the classroom every day. Teachers will emphasize focused, targeted reading instruction strategies 
daily. We will use the Reading Counts SRI scores to monitor and measure student growth. And, we will schedule SBAC Interim 
Assessments. 
 
Math - Identified SBAC claims Concepts and Procedures. We will deconstruct the standardsand align instructional strategies for SBAC 
readiness. Students will be able to use appropriate tools and strategies to think critically, solve problems and will be able to support 
mathematical conclusions. We will schedule SBAC and use Interim Assessments and analyze SBAC data to inform instruction.