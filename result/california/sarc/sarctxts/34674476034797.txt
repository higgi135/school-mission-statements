MISSION STATEMENT 
Parents were the catalyst in the 1970’s that led to the founding of Orangevale Open, and their involvement is a cornerstone of the 
institution. Families who send their children to our school understand this commitment, and they invest in the program. They attend 
monthly meetings led by officers they have elected from class parents. Parent participation is crucial to OVO and our mission: 
 
Learning by doing, together, the mission of the Orangevale Open K-8 Community is to cultivate in all students the curiosity that leads 
to creativity, lifelong learning, and responsible citizenship by connecting meaningful experiences to individual passions and motivation 
in a trusting, collaborative environment. 
 
Historically, Orangevale Open K-8's mission has been to ensure that our students 
 
1) feel excited about their education, 2) are prepared to meet new challenges positively, and 3) consider themselves capable, caring 
members of society. 
 
To achieve this, we have a learning community that aims 
To develop a love of learning within each child 
To extend the learning environment beyond the classroom and to link instruction to real life through field trips and 
community resources 
To promote personal best rather than competition and to involve students in establishing their own goals 
To create an environment that strengthens self-confidence and that allows students to take risks and to learn from 
mistakes 
To appreciate and celebrate uniqueness and to develop respect for self and others 
To learn through hands-on activities and experiences and to emphasize process rather than product 
To foster developmentally appropriate learning environments that encourage responsible choices and that provide 
opportunities for students with all learning styles 
To be a strong, caring community that feels like family 

• 
• 
• 

• 

• 
• 

• 
• 

SCHOOL PROFILE 
Orangevale Open K-8 (OVO) is one of eight K-8 schools in the San Juan Unified School District. OVO is unique in that we are the only 
K-8 which is an alternative school. We pride ourselves in maintaining a caring environment where children can reach their full potential 
while exploring ways to become increasingly responsible for their own learning using a curriculum that integrates skills and literacy, 
develops students’ unique abilities and enables students to learn through real life experiences. The instructional program, while 
transitioning from the California Content Standards to the Common Core State Standards, encourages students to explore their world. 
Learning activities are child-centered, creative, and authentic. Students go on frequent field trips. Teachers and parents plan activities 
that nurture community involvement. Visitors come to school to introduce students to varied influences and guest speakers provide 
presentations on topics of special interest. Students interact with children of all ages and stages of development. They are placed in 
active roles, and our curriculum is integrated and relevant. Time is flexible so that children learn at a pace that meets their needs. At 
OVO, students find freedom and acceptance to make mistakes and to learn from experience. The environment is rich with ample 
opportunities to develop knowledge and skills around Common Core Standards-based curriculum as well as to develop self-esteem, 
self-reliance, interpersonal relationships, and problem solving. 
 

2017-18 School Accountability Report Card for Orangevale Open K-8 School 

Page 2 of 12 

 

Our motto is "Learning by Doing, Together." We expect all adults in the community to be learners themselves. Our teachers facilitate 
Project-Based Learning across the curriculum to promote inquiry that stems from students' curiosity. We utilize the Balanced Literacy 
Model across the curriculum. Our teachers engage parents as partners in the classroom. Teachers monitor parent-run activities, 
provide direct instruction and coach students to support their personal and intellectual growth. Teachers guide children to set goals 
for themselves, and to evaluate their individual performance. Students learn to value quality for its own sake. Intrinsic motivation is 
fostered; competition is downplayed. We cultivate curiosity. Project-Based Learning is used across the curriculum. Parent volunteers 
are integrated into every grade level; their dedication is essential to OVO's instructional program. This includes after-school and 
extracurricular activities such as Drama, Band, and LegoClub. Our campus is wireless, and instructional technology, including tablets 
and laptops, is used in learning in classrooms, on the sports fields, and in the Nature Area. 
 
PRINCIPAL'S MESSAGE 
Orangevale Open K-8 continues to provide a unique, alternative education for students because of the investment of parents, teachers, 
and staff along with the support of San Juan Unified School District. With our Mission, Strategic Plan and School Plan for Student 
Achievement in place, we continue to focus on continuous improvement. Karen Chenoweth is a Writer-in-Residence at Education 
Trust, and in her book, Leading Academic Success in Unexpected Schools (2011), documents characteristics of effective schools. Many 
of those are traits of Orangevale Open K-8: 

• We teach increasingly complex and sophisticated material so that students exceed standards. 
• We have high expectations for students. 
• We use data to focus on individuals. 
• Students are our first priority. Every decision is informed by what is best for our students. 
• Time is used wisely - for engaging activities that support our mission. 
• We leverage community resources to benefit our school community. 
• Our struggling students get the most concentrated time and we develop mechanisms to expand their instructional time. 
• Rather than disciplining students who misbehave, we aim first to teach them and strengthen their understanding and our 

relationships. 

• We establish an atmosphere of respect (It's our tradition that everyone goes by first names). 
• There are many leaders in all stakeholder groups across our campus and within our community. 
• Teachers seek out professional development to address areas where their students are struggling. 
• Teachers know they must welcome and teach colleagues new to the Orangevale Open K-8 program. 
• Our entire staff is high-quality, dedicated, and competent, and they know they are a part of our educational mission. 
• Our school is a nice place to work. We collaborate to improve OVO. We have high and constantly increasing expectations 

for ourselves as professional educators; we strive to improve our skills and knowledge.