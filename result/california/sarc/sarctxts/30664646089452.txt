Our Mission Is Learning 
“We will ensure that all our children will learn more today than yesterday, and more tomorrow than today.” 
 
At Castille Elementary School our mission is to foster an environment where each child’s unique academic and social needs are met 
while establishing a foundation for lifelong learning. Utilizing state and district standards, educators encourage learning experiences 
which promote autonomy through critical thinking skills, creative problem solving, and opportunities for choice. Working as a team, 
staff, parents, and the community assist all Castille students to become contributing members of society empowered with the skills, 
knowledge, and values necessary to excel in a diverse and ever-changing world. 
 
Castille Elementary School opened its doors in September 1975 to 152 students in kindergarten through sixth grades. Due to its 
location in the heart of South Orange County’s Mission Viejo, the student population of Castille grew rapidly. During the early 1990s, 
the student enrollment soared to well over 900 students. As one of 56 schools in the Capistrano Unified School District, Castille 
currently enrolls 600 students in a typical middle class community. We serve neighborhoods adjacent to the school and to the north, 
including the Canyon Crest development, as well as to the south surrounding Newhart Middle School. The majority of the residences 
in the school communities are single-family detached homes and a few apartment dwellings. There is a fee-based preschool on site as 
well as the YMCA before and after school program, including Kindergarten Plus, an extension of the Kindergarten Day. 
 
At Castille, we are proud of our students and their behavior. High standards and expectations are set both academically and 
behaviorally. Students and teachers follow clear standards of behavior through a school wide discipline plan. The discipline plan is 
reviewed with students and parents at the beginning of each school year with a focus on anti-bullying. Character development and 
the enhancement of social skills are inherent in maintaining the school’s positive climate. The Coyote with Character program (tied 
closely to the City of Mission Viejo’s character program) serves as the backdrop for building sound character in students and guides 
them to make good choices. The program focuses on twelve character traits: Responsibility, Respect, Thankfulness, Caring, 
Perseverance, Unity, Integrity, Service, Moral Courage, Citizenship, Fairness, and Self-Control. In addition to providing our students 
with a safe and orderly environment, we place a strong emphasis on the recognition of student achievement, participation in academic 
competition, citizenship, community service, and positive character development. 
 
Castille Elementary School offers many exciting learning experiences and research-based instructional programs for children. We 
provide effective interventions for students not meeting grade level expectations and offer after school Homework help three days a 
week for select students. Our PTA provides enrichment in art and music, science field trips and assemblies, and other programs 
interwoven in a student's day. Through the combined efforts of an innovative staff, active PTA, involved parents, and broad-based 
community support, the school’s goals are achieved. 
 
Our goal is to challenge each student to progress in the state and district standards to achieve academic excellence. We facilitate an 
active and supportive learning environment by collaboration and by ongoing evaluation of student work and assessment results. 
Learning is stimulated by problem solving, critical thinking skills, and by opportunities for student choice through differentiation. As 
a STEM focus school, science, math, and technology are woven throughout your child’s day, and Castille students are afforded 
additional opportunities to participate in science learning with working scientists at universities and time in our Innovation and 
Robotics Labs. All students have access to technology both in the classrooms and in a computer lab. Teachers share their expertise 
with students after school by offering challenging math classes, including competitions and FSEA (Future Scientists and Engineers of 
America). We are a 21st Century Learning Collaborative. 
 
At Castille we foster a partnership of school, home, and the community through ongoing communication, an extraordinary number of 
parent volunteers and community events. At the home of the Coyotes, we all “HOWL” for learning. All students, teachers and parents 
work together for student success. 

2017-18 School Accountability Report Card for Castille Elementary School 

Page 2 of 11 

 

 
For additional information about the school, please visit the school’s website at www.castilleschool.com