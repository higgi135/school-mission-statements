The staff at Valleydale Elementary School is very committed to increasing student achievement by 
offering quality instruction through such programs as DBQ (document based questioning), PLC 
(professional learning communities), and quality intervention/ enrichment. Valleydale staff works 
to increase rigor in daily instruction by providing opportunities to collaborate around complex 
tasks, text, and problems. This focus has lead the staff to create higher level questions throughout 
the curriculum and across content areas to ensure that all students have an opportunity to engage 
in critical thinking. Instruction at Valleydale is centered around student opportunities to experience 
the Four Cs; collaboration, critical thinking, communication and creativity. These experiences will 
help student to be prepared for the demands of college and careers in the 21st Century. In addition 
we have a CCSS aligned English Language Arts and math curriculum. Students are supported with 
differentiated instruction through our school-wide intervention block. English Language learners 
are afforded both daily designated and integrated English Language Development instruction. We 
have also a designated Science classroom where students are able to explore science topics and 
teachers are able to provide STEM activities. In order to create a positive learning environment we 
have implemented Positive Behavior Interventions and Supports, PBIS. Staff, students, and the 
community are encouraged to follow three behavior expectations; show respect, solve problems, 
and make smart choices. Students are recognized for positive behavior choices and are motivated 
through incentive programs. Our staff recognizes the important role that self esteem plays in the 
success of each child. In partnership with McKinley Center we provide counseling services for 
students on campus. 
 
 
We are committed to providing our students and parents with a quality instructional program. We 
are proud to offer: 
 
• 
• 
• GLAD strategies used to assist students in developing language 
• A safe and clean campus that supports student learning 
• Wonderful library with thousands of titles including Common Core exemplar materials 
• Outstanding parent involvement through SSC, ELAC, Parent Nights 
• 
• Quality staff development for our certificated and classified staff members 
• 
 
At Valleydale Elementary we create a community that fosters a safe, respectful, positive school 
environment that empowers student growth in academics, community involvement, and personal 
relationships. 
 
En la Escuela Primaria Valleydale creamos una comunidad que fomenta la seguridad, respeto y 
ambiente escolar positivo que promueve el crecimiento academico del estudiante, la participacion 
de la comunidad y relaciones personales. 
 
 
 
 

Emphasis on a Common Core standards instructional program in English, Math and Science 
Focus on high expectations and achievement for ALL students 

Professional Learning Communities 

Think Together after-school program 

2017-18 School Accountability Report Card for Valleydale Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Valleydale Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

17 

13 

17 

0 

0 

1 

0 

0 

0 

Azusa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

441 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Valleydale Elementary School 

16-17 

17-18 

18-19