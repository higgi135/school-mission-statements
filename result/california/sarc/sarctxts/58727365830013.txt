The mission statement of Lindhurst High School is Guiding and Preparing ALL students for Success. 
 
Lindhurst High School is a very diverse school and culture. We feel that this is one of our greatest 
strength. In addition to cultural and socio-economic diversity we have over 50% of population 
identified with other needs such as English learners and students on Individual Education Plans. 
Our mission statement comes from the deep belief that our job is to ensure ALL students are ready 
for life after high school, whatever that path may be. This has influence practices within the 
classroom, professional development which is guiding our school to become a professional learning 
community and shaping our course offerings. To that end we have been developing strong 
articulated Career Technical Education pathways to provide students who choose to follow a path 
other than college the training and exposure to following a vocational post secondary education 
leading him or her into the workforce.