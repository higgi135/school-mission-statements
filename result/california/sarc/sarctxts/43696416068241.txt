Lucille Nixon School, located within the Stanford University community, is one of 12 elementary schools in the Palo Alto Unified School 
District. Nixon’s unique physical environment features a pod architecture with central library and computer lab, a variety of 
instructional spaces, and spacious multi-level grounds. 
 
Nixon serves the students from the communities of the Stanford campus and the foothills area west of Foothill Expressway, as well as 
transfer students from Palo Alto and East Palo Alto. The diversity of our school community has enriched the lives of all our children. 
 
Mission Statement: 
 
It is our mission at Lucille M. Nixon School that every child will leave Nixon with a love of learning, having met or exceeded all district 
benchmarks, knowing that it is effort, perseverance and resiliency that will lead to success throughout life. 
 
Vision Statement: 
 
At Lucille M. Nixon Elementary, our highest priority is the academic preparation of each of our students. To this end, teachers will 
continuously refine their practice. Teachers will pursue opportunities to learn and collaborate about strategies to enable all learners 
across the spectrum, from struggling to highly capable, to grow and succeed. We will extend our understanding of the implications 
for teaching from neuro-educational research and we will continue to investigate ways to use technology, and to teach technology 
skills to enhance learning in our classrooms. 
 
Our Nixon community is internationally and socio-economically diverse which provides experience for our students with peers of many 
ethnicities and perspectives. Our school strives to build interconnectedness across ethnic, socio-economic, language and geographic 
lines. In the coming years, we seek to develop more strategies to link ourselves together in a mutually supportive community. Our 
school’s success in educating children depends upon a strong partnership with parents. We expect to continue to cultivate a highly 
involved parent community, and to explore and develop ties to the broader community, including Stanford University. 
 
As a school community of parents, teachers and staff, we are committed to the social and emotional development of our students. 
We will offer our students opportunities to provide service to our greater community and encourage the development of “green” 
attitudes and healthy lifestyles. Teachers will foster students’ creativity, curiosity, inquisitiveness and independent thinking in the 
classroom. The Nixon community seeks ways to enhance the ‘developmental assets’ in each student through our Lifeskills program, 
to promote socially and emotionally well-rounded and confident individuals. We will teach strategies and support students to learn 
to resolve conflicts using problem-solving skills. 
 
We are a community of learners and, as such, we will continue to identify areas of needed growth and to develop strategies to address 
those needs. 
 
 

2017-18 School Accountability Report Card for Lucille M. Nixon Elementary School 

Page 2 of 11