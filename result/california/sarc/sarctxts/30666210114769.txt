Mission: 
Orange Community Day School (Orange CDS) serves students in grades seven through twelve. Orange CDS is built on the belief that 
all students can learn when given the appropriate nurturing environment, educational expertise, and opportunity. Each student 
regardless of ability, socio-economic, or cultural background should develop a sense of self-worth, accountability, responsibility, as 
well as the desire to successfully return to their comprehensive schools to perform as responsible citizens within their community. 
 
Vision: 
Community Day School will prepare students to successfully return to their comprehensive school setting by: 

Improving their academic proficiency in the core subject areas 

• 
• Developing skilled communicators who speak in a respectful, appropriate manner as well as demonstrate the ability to 

actively listen and follow direction. 

• Developing social and personal responsibility to take an active role in their academic progress and success. 

Students are placed at OUSD Community Day School as a result of the expulsion process, School Attendance and Review Board action, 
probation or juvenile court action or district level placement committee. Students generally spend two semesters at Community Day 
School and are then released either to the district continuation school for credit recovery or to the students school of residence if they 
are a Level 2 or Level 3 student.