Grimmer Elementary School is a high-performing California Distinguished School as well as a 
California Gold Ribbon School and a California Title I Academic Achievement Award recipient. We 
have been recognized by Innovate Public Schools as a Top Bay Area Public School for Underserved 
Students for mathematics two years in 2016 and 2017. 
 
In addition to our English program, Grimmer School also has a Two-Way Spanish Dual-Immersion 
program and a Moderate Special Day Program where all children can be successful. Families from 
many neighborhoods in Fremont choose Grimmer School for its wonderful diversity and many 
specialized programs. Our Two-Way Spanish Dual Immersion Program prepares children to become 
bilingual and bi-literate. Our Moderate Special Day Program provides inclusion opportunities which 
enhance the learning experiences of all of our students. Whether students participate in one of 
these programs or the English-only course of study, they receive much academic support, 
challenge, and intervention during the school day and after school. 
 
We are committed to ensuring the success of all students. Our academic growth trend has largely 
resulted from an outstanding instructional program, in which our highly trained teachers use the 
best research-based teaching methods. Our Professional Learning Communities allow teachers and 
staff meaningful collaboration time in planning for the educational needs of each individual 
student. 
 
Grimmer hosts the ASES after-school program run by the City of Fremont for 80+ students in grades 
two through six. This popular three-hour program provides an hour of homework assistance, an 
hour of physical activity, and an hour of enrichment classes. We encourage enrollment in this 
program. Grimmer has a unique and innovative approach to Anti-Bullying, which allows students 
to understand the meaning of bullying, and gives them support and solutions to avoid bullying. 
Grimmer also has an organized Student Council and Rainbow Club. These student groups arrange 
activities on campus for promoting kindness including monthly kindness challenges for all students. 
Adults and students are fully committed to the success of these programs which provide social and 
emotional support for students, parents, and Grimmer staff in these efforts. Students and parents 
also enjoy the arts at Grimmer. Our Arts Block program is a seven-week visual and performing arts 
elective held in late spring with a focus on rigorous art-based writing projects. Students participate 
in the Fremont Education Foundation sponsored after-school band program that meets once a 
week. 
 
In our Family Literacy Program, parents learn strategies to work effectively with their children at 
home to promote academic achievement. Childcare is provided while parents are in class. We host 
Family Literacy and Family Academic Nights to help parents learn special techniques to use at 
home. Each year we are proud to provide Parent/Family Nights at Grimmer where parents learn 
more about supporting their students academic success on the pathway to college and career 
readiness. Parent participation and attendance at school events are high. Parents have many 
opportunities to participate in their children’s education at school. Together, we work to build 
relationships and strengthen communication within the Grimmer community.

2017-18 School Accountability Report Card for GRIMMER ELEMENTARY SCHOOL 

Page 1 of 9 

 

At Grimmer School, we strive to meet the academic, social, physical, and emotional needs of each child. We do this within a warm, safe, 
and child-centered environment. We believe that learning should be transferable to real-life situations and should involve communication 
and interaction among students, parents, school, and the community. Staff members are dedicated, nurturing, and supportive of students 
and their families and work hard to improve student achievement and the school. 
 
GRIMMER SCHOOL MISSION: Inspiring a Community of Lifelong Critical Thinkers 
 
GRIMMER SCHOOL VISION: Grimmer School’s vision inspires a community engaged in the learning process: parents, students, teachers, 
and staff. Our school believes in sharing our passion for lifelong learning within a safe, caring community. We provide academic 
excellence, equity, and college/career readiness for all students. Grimmer scholars are Upstanders for a bully-free school. They think 
critically and take intellectual risks. As scholars, all students are motivated to produce learning goals. Our scholars analyze, comprehend, 
and demonstrate their learning, and perform at their highest potential. Ultimately, Grimmer students grow into a caring community of 
lifelong critical thinkers.