Potter Valley Community Unified School District was born out of a community commitment to local education. Once a part of Ukiah 
Unified Schools, community members came together in the late 1970’s to fight for the right to form our own school district under 
local control. This dream became a reality in 1977-1978 when Potter Valley officially de-unified from Ukiah and formed Potter Valley 
Community Unified School District. 
 
After over 39 years of operation, Potter Valley continues to proudly provide a high quality, local education for students, preschool 
through 12th grade. We are pleased to offer Spanish and Art throughout the district. We have an early release day on Thursdays for 
students which enables teachers to participate in Professional Development opportunities. 
 
We pride ourselves in providing a uniquely relational approach to education in contrast to the big-box schools that struggle to keep 
students from becoming a face in the crowd. The students, parents, and staff at Potter Valley Elementary School have committed 
themselves to making the necessary adjustments to our curriculum to meet the rigorous demands of standardized state testing, but 
also to remain true to our roots and keep the best interest of students at the center of everything we do. 
 
The school has made great strides with technology to enhance and enrich instruction. All classrooms have data projectors and 
document readers. Chromebooks are used in each grade level to enhance instruction. We have newly adopted writing, math, ELA 
programs that are aligned with Common Core Standards. 
 
Reading and Math intervention programs are used to promote reading and math growth and fill skill gaps for all students kindergarten 
through sixth grade. Data is used to diagnose reading and math strengths and weakness and to help form the best reading and math 
instruction for each student. 
 
Our motto, “Potter Valley Schools, a family rather than a factory approach to education,” expresses this focus, and inspires all 
stakeholders to work hard to maintain a school where “Community” is more than just a word in our district name; it is a description 
of how we approach education. 
 
For questions about our schools contact the school offices. Elementary Office 707-743-1115 Jr./Sr. High Office 707-743-1142