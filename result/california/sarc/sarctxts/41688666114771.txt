Sandpiper School is in the Belmont-Redwood Shores School District. It serves Transitional 
Kindergarten through 7th Grade students in the cities of Belmont and Redwood Shores, as well as 
portions of Redwood City, San Carlos, and San Mateo. Sandpiper School is located in Redwood 
Shores on the peninsula south of San Francisco, and east of Highway 101. It is one of two planned 
K-8 elementary schools, four traditional elementary schools, and one comprehensive middle 
school. Sandpiper serves 618 students in 24 general education classrooms, as well as one Learning 
Center Program (RSP/SDC). 
 
Vision Statement 
Sandpiper School is committed to working with our community to provide an innovative 
educational program which allows students to meet their academic potential in an environment 
that inspires creativity and a love of learning. Students will develop a strong work ethic, exhibit high 
self-esteem, learn self-management skills and accept responsibility for their actions. 
 
Mission Statement 
Sandpiper is a community of learners in which every person is well known and honored for their 
individuality. Students conduct themselves with pride, exhibit the highest standard of behavior, 
and demonstrate personal initiative in their education. Sandpiper works with the community in a 
spirit of unity and mutual respect to promote excellence in the learning environment. Decisions 
center on the needs of children to provide a challenging integrated educational program that 
emphasizes creativity, innovation and flexibility. Graduates possess the skills and knowledge to be 
successful in an interdependent world. More importantly, we have inspired students to be life long 
learners. 
 

2017-18 School Accountability Report Card for Sandpiper School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Sandpiper School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Belmont-Redwood Shores Elementary School 
District 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

24 

26 

26 

0 

0 

0 

0 

1 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

207 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Sandpiper School 

16-17 

17-18 

18-19