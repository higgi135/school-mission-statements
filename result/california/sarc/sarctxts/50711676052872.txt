School Mission Statement: 
It is the mission of Wilson Elementary that all scholars will grow and learn at the highest levels through a collective commitment of 
equity, advocacy, and responsibility. 
Wilson School is situated in the central east suburban section of Modesto in Stanislaus County. It is one of the twenty two elementary 
schools in the district and is composed of seven older classrooms built in 1929 and the newer rooms in the 1950’s. This year we will 
celebrate our 90th anniversary. The buildings are in excellent condition despite their age because of the care they receive from 
Wilson's meticulous custodial staff. Centrally located within a neighborhood of low to high income homes, the school serves a diverse 
group of approximately 270 preschool through 6th grade students. Students living in our attendance area are mainly Hispanic, White 
and African American. The income level of the families falls into low to medium socio-economic range with the majority of students 
meeting free or reduced meal eligibility. This year the CEP allows all students to receive two free nourishing meals each day. 
 
There are ten general education teachers, one resource teacher, and one pre-formal teacher. In addition to the K-6 staff, Wilson has 
one three day per week computer literacy teacher, two one day per week 45 minute prep providers (certificated teachers), one part-
time library media assistant, one part-time Speech and Language Therapist, one part-time school psychologist, one full-time Marriage 
and Family Therapist, one part-time Student Assistant Specialist, one Behavioral Consultation Model Clinician, one after school 
program director, two para-professionals, one administrative assistant, one attendance clerk, one part-time bilingual community aid, 
one part-time TC II, two full-time custodians, one part-time custodian, three food service employees, one campus assistant and four 
yard duties. Wilson's teachers range from second year to veteran teachers (two or more years of teaching experience). In the 
classroom, teachers teach English Language Arts with Macmillan McGraw Hill (MMH) for English Language Arts (ELA), Engage New 
York for Mathematics (1st-6th grades) and Early Math with Engage NY as supplemental material in kindergarten. The school 
population includes approximately 47 English Language Learners which is 19% of the student population. English Language 
Development (ELD) instruction is taught systematically throughout the day along with 30 minutes of designated ELD instruction daily 
in each classroom. 
 
During the 2017-18 school year, Wilson students in third through sixth grades completed the Smarter Balanced Assessment 
Consortium (SBAC) for the third consecutive year. The results have been reported and most grade level cohorts showed growth in 
both English Language Arts and math. Wilson third graders made double digit growth in ELA and math. Fifth graders continued to 
take the Physical Fitness Test, and they also participated in the pilot computerized science test. Last year, 8 out of 11 teachers taught 
afterschool intervention. In addition, school-wide interventions and enrichment periods were implemented four days per week during 
the school day. Wilson teachers will continue to provide students with interventions and enrichment throughout the school day as 
well as after school during the 2018-19 school year. Teachers will be using Phonics for Reading, Rewards, Language for Learning, 
Reading Mastery, Corrective Reading to teach in-school and after school intervention as well as enrichment. 
 
Last year, teachers were provided with collaboration time 14 times per year due to early release days provided by Modesto City 
Schools. In addition to the 14 collaboration times provided by the district, teachers were also given extra time (after district 
collaboration time) to meet with their grade level teams to discuss student data, design lessons from the CCSS and compose and 
review their SMART goals and common formative assessments. Furthermore, teachers were given the opportunity to take one 
professional development day per year to focus on CCSS and lesson design. This collaboration time was facilitated with the assistance 
of the three teacher leaders and the principal. During the 2018-19 school year, the district has offered teachers 15 collaboration times 
due to student early release days. In addition, following the 2016-17 format, teachers will continue to be given the option to meet 
with their grade level teams after district collaboration times to analyze data, design lessons and create common formative 
assessments. 
 

2017-18 School Accountability Report Card for Wilson Elementary School 

Page 2 of 15 

 

Wilson stakeholders remain committed to strengthening the character of every student through the promotion of the Modesto City 
School's Character Education program and the use of PeaceBuilder and SecondStep curriculum. Wilson also has the longest running 
mentorship program in the district with E&J Gallo. This year, we will celebrate our 21st year where Gallo employees come to Wilson 
to mentor our students. In addition, over the past two years, our Positive Behavior Intervention and Support (PBIS) team has been 
trained using Restorative Practices. Wilson's PBIS team remains committed to training all staff members regarding the importance of 
the development of the "whole" student by using various Restorative Practices strategies. During the 2018 -19 school year, there are 
professional development plans to teach one Restorative Practice strategy to teachers and support staff at our monthly meetings and 
assemblies. Both the intermediate and primary playgrounds have PEACE PATHS, a "safe place" where students go to resolve their 
own peer conflicts. When appropriate, some teachers opt to use the Restorative Practices circle talks and/or offer comfort corners (a 
temporary place for students to find comfort), and Peacebuilder Center/Reset Zone to students throughout the school day. Using one 
or more of the aforementioned strategies, Wilson's suspension rates were cut in half from the previous year. In addition, Wilson's 
PBIS team is committed to meeting five times per year to review discipline data and discuss strategies that promote civility and develop 
pro-social behaviors in student. The PBIS team consists of the principal, campus assistant, After School Program Director, Center for 
Human Services employees, and teachers. The following positive behavior activities will continue to be implemented at Wilson: Fun 
Dance Fridays, Game Days, Popcorn with the Principal, PeaceBuilder school-wide recognition, monthly positive playground rewards, 
PeaceBuilder bulletin board recognition and certificates for Praise Notes, and public recognition at Student of the Month Assemblies. 
 
Students have the opportunity to participate in many activities at Wilson. The ASES Program (after-school program) provides a safe 
environment to students after school hours in which they may participate in a variety of activities that include support during 
designated homework times, field trips and sports competitions with other MCS students. The director is in active communication 
with the school administration and there is a collaborative effort by both for the benefit of all students. There are also opportunities 
for students to participate in chorus, music (strings and/or band), Mighty Miler, 10K with a Cop, Garden Club, ASB, Wilson Baseball, 
Art Restores Kids, Traffic Patrol, Garden and Recycling Clubs. 
 
Wilson Elementary School has an extremely successful state preschool. Communication between Wilson's pre-primer teacher, 
principal and K-6 teachers is on-going. Beginning of the year Kindergarten assessment results from former pre-formal students was 
and will be an ongoing discussion with both teachers as Wilson strives to ensure all students enrolled in these programs are ready for 
Kindergarten. The transition to Kindergarten is enhanced by the continuity of our pre-formal program. Kindergartners are given an 
ELA assessment (DIBELS) at the beginning of every year. Most students who scored at or above benchmark on the aforementioned 
assessment were either enrolled in a Modesto City School's Transitional Kindergarten or came from a pre-school setting. During the 
2018-19 school year, Wilson's incoming kindergartners and parents of kindergartners were invited to attend four kindergarten 
readiness courses in the spring. This was the forth time for our pre-kindergartners to become familiar with kindergarten procedures 
and teachers. It was also a time for parents to become familiar with the Common Core State Standards, end of year outcomes, staff 
and general procedures. 
Parents are always welcomed onto Wilson's campus and considered an integral part of school and student success. Wilson's staff 
members actively encourage parent involvement. During the 2017-18 and 18-19 school year, parents were invited onto campus for 
the following meetings and/or events: Back to School Night, Pan Dulce with Parents, Dads and Donuts & Moms and Muffins, Picnic 
with the Parents, Jog-a-thon, Read Across America, Parent Involvement Committee, English Language Parent Partnership Meetings, 
Parent Teacher Association Meetings, SSC, Open House, Chorus Presentation, Student of the Month and PeaceBuilders Assemblies, 
End of Trimester Awards and Rewards (Dance and Movie days), PTA sponsored events and Bring Your Parent to School Academic 
Mornings. Parents are encouraged to visit Wilson's webpage, Facebook page and/or contact the school for more detailed information 
about their child’s educational programs at (209) 574-8452 between the hours of 7:30 am-4:00 pm. 
 
Modesto City Elementary: K-8 
 
Migrant Education /Title Part C supplemental instructional and support services are provided to the Modesto City Schools migrant 
students through a Memorandum of Understanding with the Merced County Office of Education/Migrant Education Region 3. These 
services are directly provided by Migrant Education Region 3 staff to preschool and elementary students. Migrant Education 
supplemental instructional services are provided through a combination of site and home base models. Migrant students are 
identified and recruited by two Support Services Liaisons that are housed at Pearson Education Center. The current migrant student 
eligibility list is maintained on a monthly basis and is cross referenced with the district student data base to keep student lists current. 
The Supportive Services Liaisons additionally provide referrals for supplemental health and social services to migrant families identified 
in the Modesto City Schools attendance area. The migrant parents assist the district and region in evaluating migrant educational 
services through their participation in the Migrant Parent Advisory Committee that meets six times per year. Migrant Education 
services are determined each year in collaboration with the Modesto City Schools District to maintain or modify them based on a 
review of prior year services and funding allocations. 
CONTACT PERSON and Phone: 
Mrs. Cathleen Schali-Lopez M.A., Principal 

2017-18 School Accountability Report Card for Wilson Elementary School 

Page 3 of 15 

 

(209) 574-8452