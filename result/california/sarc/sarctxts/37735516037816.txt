Valley Middle School is a California Distinguished School, and it was the first middle school in the 
Carlsbad Unified School District, opening more than 50 years ago. The school is organized into six 
villages. In each village, students receive instruction in language arts, mathematics, science, and 
social studies from a team of four teachers. Our teachers work cooperatively to address the 
academic, social, and personal needs of each student. Students move outside their village for 
physical education and a variety of electives, including visual and performing arts, technology, and 
world language. This structure enables students to develop closer relationships with a supportive 
team of teachers and a smaller group of peers. It also provides parents the opportunity to 
communicate with a close team of teachers that are specifically responsible for guiding their child’s 
education. 
 
Nicole Johnston, PRINCIPAL 
 
 

----

---

- 

Carlsbad Unified School District 

6225 El Camino Real 
Carlsbad CA, 92008 

760-331-5000 

www.carlsbadusd.k12.ca.us 

 

District Governing Board 

Mr. Ray Pearson, PRESIDENT 

Ms. Kathy Rallings, VICE PRESIDENT 

Mrs. Veronica Williams , CLERK 

Mrs. Claudine Jones, MEMBER 

Mrs. Elisa Williamson, MEMBER 

 

District Administration 

Benjamin Churchill, Ed. D. 

Superintendent 

Mr. Chris Wright 

ASSISTANT SUPERINTENDENT, 

BUSINESS SERVICES 

Mr. Rick Grove 

ASSISTANT SUPERINTENDENT, 

PERSONNEL SERVICES 

Dr. Robert Nye 

ASSISTANT SUPERINTENDENT, 

INSTRUCTIONAL SERVICES 

 

2017-18 School Accountability Report Card for Valley Middle School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Valley Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

46 

43 

43 

0 

0 

0 

0 

0 

0 

Carlsbad Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

574 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Valley Middle School 

16-17 

17-18 

18-19