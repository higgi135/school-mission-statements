Our mission as the Eader School Community is to provide a safe, creative, challenging learning 
environment for all students. We do this by working as a team, creatively using all of our resources 
available, and by modeling our expectations. Eader students will reach their full learning potentials 
and be assets to our society. 
 
Eader Elementary School is a 2016 California Gold Ribbon School, and was the first elementary 
school in the state to earn the recognition of being a California Civic Learning School of Distinction 
for teaching democracy and global awareness! Our school program consists of one three-year-old 
and one four-year-old Preschool Academy class, four Special Day Class (SDC) preschool classes, and 
nineteen general education classes. Programs are offered for Gifted and Talented Education 
(GATE) students, Specialized Academic Instruction (SAI) students and English Learner (EL) students 
supplementing the core curriculum, based on a traditional school calendar. Eader's staff 
implements Common Core standards-based education while responding to the individual needs of 
students. Student progress monitoring is reviewed regularly with district benchmarks, curriculum-
based measures on a trimester basis, and annually with statewide exams. Programs unique to Eader 
are the MIND Research Institute's STMath, which is a spatial-temporal reasoning math program, K-
5 Music Program, Science Works, Cognitively Guided Instruction (CGI), Thinking Maps, Write from 
the Beginning, and CATCH-PE which supplement our district-wide adoptions in English Language 
Arts, Math, Science, and Social Science. Teachers have been trained in DII, (Direct Interactive 
Instruction) for optimal delivery of instruction. Technology improvements are constantly being 
made with our second through fifth grade classrooms having 1:1 student to device ratios, and a 
computer lab for our kindergarten and first grade use, in addition to their individual classroom 
computer stations. There are Smartboards in every classroom including our conference room and 
music classroom. Our outstanding parent involvement extends student learning and community 
building activities such as the Family Fall Festival, Family Tile Night, field trips, assemblies, and Art 
Masters. Volunteers log in over 15,000 hours to programs such as Surf Tales, math centers, reading 
centers, organizing school and community fundraisers, and contributing to decision-making 
committees. 
 
 
 
 
 
 
 

----
---- 
Huntington Beach City School 

District 

8750 Dorsett Drive 

Huntington Beach, CA 92646 

(714) 964-8888 
www.hbcsd.us 

 

District Governing Board 

Bridget Kaub 

Shari Kowalke 

Paul Morrow, Ed. D. 

Ann Sullivan 

Diana Marks 

 

District Administration 

Gregory Haulk 
Superintendent 

Jennifer Shepard 

Assistant Superintendent 

Educational Services 

 

Patricia Hager 

Assistant Superintendent 

Human Resources 

 

Jon M. Archibald 

Assistant Superintendent 
Administrative Services 

 

 

2017-18 School Accountability Report Card for John H. Eader Elementary School 

Page 1 of 8