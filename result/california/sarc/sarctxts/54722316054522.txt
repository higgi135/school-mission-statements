: 
Named after President Woodrow Wilson, Wilson Elementary School opened it's doors to the public 
in 1920. As the oldest school in the district, members of our school community take great pride in 
the fact that many of Tulare's most notable citizens attended Wilson School. The list of famous 
alumni include: Admiral Elmo Zumwalt Jr., former Chief of U.S. Naval Operations; Two-time Olympic 
Gold Medalist and U.S. Congressman, Bob Mathias; Olympic Gold Medalist Sim Iness; and two-time 
winner of the Kremer Prize for human-powered flight and pilot of the first human-powered flight 
across the English Channel, Bryan Allen. 
 
Honoring our school's heritage and ensuring that the next generation of students are fully prepared 
to enjoy future opportunities, guides our daily decisions and long term planning. The mission of 
Wilson Elementary School is to provide all Warriors with a balanced educational experience based 
on the virtues of strong moral character and the development of creative problem solving skills. 
We are dedicated to closing any achievement gap through the delivery of rigorous curriculum and 
to provide students with the support needed to transform their behaviors and expectations 
regarding college and career preparation. Through a cooperative effort with the members of our 
school community, we will work together to ensure a bright and healthy future for all students. 
 
The Wilson staff consists of 24 classroom teachers. This number includes one RTI Teacher, one RSP 
instructor, and two Special Education teachers assigned to our Special Day Class program. In 
addition, Wilson School is the home to two Preschool classes. The program maintains two teachers, 
each assigned to one session in the morning and afternoon. Our instructional staff and students 
receive supplemental support and assistance from 12 part-time instructional aides and one library 
aide. Working together with the able assistance of a full time secretary, two custodians, and two 
lunch servers, the Wilson staff strives to meet the needs of our school community in a pleasant and 
efficient manner. 
 
The staff of Wilson School is dedicated to improving the academic performance of each student. 
Designated as an AVID Showcase School, we continue to explore and implement measures 
designed to increase student achievement. In addition to extensive AVID training and school-wide 
planning, all teachers are actively involved in the Instructional Rounds process. Through a strategic 
approach to the analysis of data and planning, staff members are dedicated to improving their 
ability to identify materials and strategies that best suit the individual needs of our students. 
 

2017-18 School Accountability Report Card for Wilson Elementary 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Wilson Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

26 

23 

23 

2 

0 

0 

0 

1 

0 

Tulare City School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

450 

27 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Wilson Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.