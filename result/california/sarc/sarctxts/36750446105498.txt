PRINCIPAL'S MESSAGE 
The purpose of the School Accountability Report Card (SARC) is to provide parents with information about Cottonwood Elementary 
School's instructional programs, academic achievement, materials, and facilities, and the staff. 
 
Parents and community play a very important role in our schools. Understanding our educational program, student achievement, and 
curriculum development can assist both our schools and the community in ongoing program improvement. 
 
We have made a commitment to provide the best educational program possible for our students, build around a theme of Social, 
Physical, and Academic Fitness. The excellent quality of our program is a reflection of our highly committed staff. We are dedicated to 
ensuring that our school provides a welcoming, stimulating environment where students are actively involved in learning academics 
as well as positive values and healthy lifestyles. Together through our hard work, our students will be challenged to reach their 
maximum potential. 
 
SCHOOL MISSION STATEMENT 
The mission of Cottonwood Elementary School is dedication to the highest quality of academic instruction and service to all members 
of the school community delivered with a sense of compassion, friendliness, individual pride, and school spirit. 
 
SCHOOL PROFILE 
Hesperia Unified School District is located in the high desert region of San Bernardino County, approximately 40 miles north of the 
Ontario/San Bernardino valley. More than 20,000 students in grades kindergarten through twelve receive a rigorous, standards-based 
curriculum from dedicated and highly qualified professionals. The district is comprised of 15 elementary schools. At the secondary 
level Hesperia has 3 middle schools, 3 comprehensive high schools, 1 alternative school, 2 continuation high schools, 1 community 
day school, and 6 charter schools. 
 
Cottonwood Elementary is located in the southwest area of Hesperia and serves students in grades K-6. At the beginning of the 2018-
19 school year, 875 students were enrolled, including 5% in special education, 27% qualifying for English Language Learner support, 
54% qualifying for free or reduced-price lunch.