Laton Unified School District 
 
MISSION STATEMENT 
 
The Laton Unified School District is committed to provide quality educational opportunities and support services, for all students to 
reach their highest learning potential, in a safe, caring, nurturing environment consistent with the limitations of the District’s financial 
and human resources. 
 
Laton Elementary School 
 
MISSION STATEMENT 
 
All students will receive a sound secure cooperative student centered education. The dignity and worth of every individual will be 
recognized. Students will be encouraged to develop to their highest potential and to attain the skills, knowledge, and the attitude 
necessary to become productive human beings and to meet all state and district curriculum standards necessary to insure academic 
success. 
 
SCHOOL VISION STATEMENT 

• Require each student to master basic learning skills; 
• 
• 
• 

Engage each student in work that requires mastery of higher order thinking and problem-solving skill; 
Enhance the character development and interpersonal skills of each student; and 
Encourage parents to actively participate in their child’s learning experiences.