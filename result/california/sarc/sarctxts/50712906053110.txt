Sylvan Elementary School opened its doors to educating children in grades Kindergarten through fifth grade in 1956. During the 2017-
2018 school year, approximately 328 students were enrolled in kindergarten through fifth grade. We believe that a child's education 
is fostered by the school and families working together to provide a learning experience where the students will excel. Our mission is 
to teach students how to learn, to help them gain knowledge, and to promote a positive self-image and growth in a culture of safety 
and respect. Utilizing district-adopted curriculum the staff employs the most effective teaching strategies along with grade level 
collaboration time to ensure the students are receiving quality instruction in all academic areas. Assemblies are a time when the 
entire school comes together to celebrate each other and accomplishments. Through the analysis of data and communication between 
staff members, students are provided with an educational experience that incorporates strong academic goals, physical education, 
comprehensive music and art programs, and the development of life-long character skills which will help develop our students into 
successful adults.