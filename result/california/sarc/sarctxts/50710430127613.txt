Lucas Elementary Dual Language Academy is committed to providing a quality, balanced education that promotes academic excellence 
in both Spanish and English languages in collaboration with home and community. Our school motto, “Valuing bi-literacy today, leading 
tomorrow” is what we strive to do on a daily basis. Our district mission statement, Committed to Excellence, Responsive to Every 
Student, reflects the staff's commitment to our profession, which is also reflected in our school's vision/mission of being " committed 
to academic excellence with high-quality instruction for all students, creating the opportunities to achieve bi-literacy and proficiency 
in a culturally valued and diverse learning environment; in which students, teachers, and parents believe and support a bilingual 
education where students are challenged with rigor and high expectations to become leaders of tomorrow." 
 
Lucas Elementary Dual Language Academy is located in the heart of California’s Stanislaus County and is surrounded by the suburban 
community of Ceres. Upon opening it's doors on August 14, 2013 with staff and students, it became 1 of 4 fully immersed dual language 
schools in the county. With over 95 elementary schools in the area, we are an optional educational opportunity for many students 
who either walk to and from school or are transported by their parents on a daily basis. Our campus is located on the southwest side 
of Ceres and it accommodates students from the entire region. Lucas is a Kindergarten through 6th grade school, of approximately six 
hundred sixty students. From the first day of school, Lucas Elementary included enough classrooms for four classes of Kindergarten 
and four 1st grade. Now, we have four classes in each grade level K-3, and 3 in each grade level 4th-6th grade. Lucas Elementary also 
includes a cafeteria, a bilingual library, and an administration office. School staff includes: a principal, an assistant principal, 25 
certificated teachers ( of which 18 have a Bilingual credential – BCLAD) one 20% resource specialist with 50% para II, one full time 
library clerk daily, a 20% nurse, a 6 hour health clerk daily, a 25% speech teacher, an office manager, secretary, administrative assistant, 
five instructional paraprofessionals, support staff in our cafeteria, two and a half custodians, and crossing/noon duty staff members, 
and P.E. teachers provide services 1 day per week for all primary students and 2 days for intermediate grades. 
 
The school year consists of 180 instructional days with seven minimum days for parent conferences. The students in first - sixth grades 
are in school for 310 instructional minutes per day. The kindergarten students are in school for 300 minutes per day all school year. 
The school has a Parent Teacher Club that supports extra-curricular programs, classroom budgets, special events, and facilities for the 
school. The culture is diverse at Lucas Elementary Dual Language Academy of the approximately 664 students, 63% of the students 
receive free or reduced-cost breakfast and lunch, 54% are English Learners (EL). School-wide reward systems revolve around a focus 
on life skills, good behavior, leadership habits and student greatness. Each month is dedicated to a designated life skill, which is 
reinforced, by teachers and presenters in our assemblies; students earning class rewards; monthly Owl Pride Assemblies; Students of 
the Month as well as trimester awards for attendance and academic achievement. Lucas receives supplementary funding from Title I 
due to being designated as "school-wide" which allows all students to be eligible for services.