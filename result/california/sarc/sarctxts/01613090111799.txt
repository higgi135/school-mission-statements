Principal’s Message 
East Bay Arts High School is a small college preparatory high school that that is a school of choice. We started in the fall of 2005 with 
a clear vision of the future for our students. We envisioned a safe school where every student could learn the habits of mind, body 
and character necessary for success. With college as the goal, our students master the common core standards, complete the UC/CSU 
a-g requirements and grow as an artist. Our cross curricular grade level projects are based in real world problems and include the arts 
to encourage student engagement and participation. At East Bay Arts students develop their creativity, sense of confidence, grow as 
a community, and learn the skills so they are college and career ready. 
 
The staff has worked to focus on development of Academic Discourse, both oral and written to ensure that students are able to clarify 
complex ideas and express various ideas. The staff have also focused on project based learning through the use of integrated 
curriculum and projects. 
 
The mission of East Bay Arts is to provide a safe place for all students to learn the habits of mind, body, and character necessary for 
success. Our central intellectual purpose is that all students will be problem solvers, researchers, communicators, users of technology, 
and productive, ethical citizens. Our goal is to provide an educational environment that is academically rigorous while also 
incorporating the visual and performing arts so as to ensure a safe, exciting, and interesting community while at school. Students 
graduate prepared for college, having fulfilled California's A-G requirements and the world of work. 
 
VISION: 
East Bay Arts is a college prep high school in the San Lorenzo Unified School District. Students at East Bay Arts are exposed to a variety 
of performing, visual, and electronic arts. The goal of East Bay Arts is to provide all students with a rigorous, engaging, and enjoyable 
education that prepares all students for college and life after graduation. 
 
 
District Mission Statement 
 
The San Lorenzo Unified School District teachers and staff will collaborate with families and the community to cultivate safe learning 
environments and ensure equitable opportunities and outcomes for all students 
All students will become engaged community members contributing to, and becoming good stewards of our changing world. 
All students will reach their highest potential as creative and critical thinkers prepared for college, career and life-long learning. 
 
District Vision 
Students will become creative, collaborative, compassionate, resilient, well-informed and socially responsible advocates for equity 
and social justice as a result of their education, experience and support from educators, families and the community. 
 
 

2017-18 School Accountability Report Card for East Bay Arts High School & Royal Sunset 

Page 2 of 14