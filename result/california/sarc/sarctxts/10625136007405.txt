The Washington Colony Elementary School District was organized May 15, 1879. The first school 
was located in a small 20' x 20' building on the southwest corner of Elm and Washington (now 
American) Avenues. In 1880 a two room school building was constructed on property donated by 
the Easton-Eldridge Company on the south side of Lincoln Avenue, one block east of Elm. In 1889 
this building was moved across the street: it became known as Easton Hall and served for a few 
years as a combination high school and grammar school. In the long tradition of Washington 
Colony, our school continues to occupy the Lincoln Avenue site. With the reorganization of our 
campuses in 1997, all Transitional Kindergarten through fifth grade classrooms are located on the 
North campus, while all sixth through eighth grade classrooms occupy the South campus. 
Washington Colony Elementary has historically benefited from a tremendous amount of stability, 
family and community support, and solid educational programs. 
 
Our mission, in partnership with students, parents, and community, ( all stakeholders) to deliver 
high quality learning experiences which promote lifelong success for all our students by providing 
educational choices consistent with our core values of a commitment to learning, and atmosphere 
of respect, community involvement, effective communication, and accountability. Also to maximize 
all students' potential by providing opportunities that ensure intellectual and emotional growth, 
resulting in productive citizens and persons of character. 
 
District goals and programs are developed in accordance with the following set of core beliefs: 
 
We believe: 

the school environment should be safe, respectful, loving, caring, and supportive. 

• 
• we should offer a positive learning environment and experiences that lead to success 

of our students. 
the school should provide a solid base of academics. 
all students should be challenged to their potential. 
every student has a right to an education and every student has a responsibility for 
learning; this right can be forfeited by failure to accept this responsibility. 
students, staff, and parents form a community of learners where learning never ends. 
in addition to educating children, the school serves the broader community with its 
facilities and resources. 
there is no "single" program or solution which will work for all students. 
relevant curriculum leads to motivated students. 
in being respectful of cultural differences. 

• 
• 
• 

• 
• 

• 
• 
• 

2017-18 School Accountability Report Card for Washington Colony Elementary School 

Page 1 of 7 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Washington Colony Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

24 

25 

25 

1 

0 

0 

0 

1 

0 

Washington Colony Elementary School District 16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

25 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Washington Colony Elementary 
School 
Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

16-17 

17-18 

18-19 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.