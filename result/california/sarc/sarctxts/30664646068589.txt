SCHOOL COMMUNITY 
Viejo Elementary School is located in the beautiful city of Mission Viejo in Southern Orange County and is a family-oriented community, 
with more than forty-one parks and five recreation centers to serve the tremendous amount of participation in youth sports. It is 
known for being a well-planned community and was recently named the safest city in the nation. According to a report published by 
the CQ Press, and FBI, Mission Viejo has the lowest crime rate in the nation for cities with a population of 75,000 or more. Mission 
Viejo has consistently been in the top ten rankings in the nation since the Safest City Awards began fourteen years ago. 
 
Viejo has five student subgroups including Hispanic, White, Socio-economically Disadvantaged, English Learners and Students with 
Disabilities. 
 
MISSION STATEMENT 
Vaquero Scholars collaborate to create a positive bilingual environment where we achieve high academic levels as we become global 
citizens. 
 
Colaboramos para crear un ambiente bilingüe positivo donde logramos altos niveles académicos de aprendizaje mientras nos 
transformamos en ciudadanos globales. 
 
VISION STATEMENT 
Scholars achieving at high levels 
 
Escolares logrando a niveles altos 
 
STUDENT DEMOGRAPHICS 
Viejo teachers believe in the importance of student acquisition of standards and skills, and the ability to use those skills in all subject 
areas. We know that all students, within our five subgroups, can meet and exceed academic goals and standards. Over the past few 
years our demographics have changed socio-economically and we have had a noteworthy increase of English Learners. Viejo teachers 
have continued our commitment to excellence and meeting the needs for all students within our changing population. Though our 
demographic population changes yearly, our students continue to meet and exceed academic goals and expectations. 
 
SCHOOL PROGRAMS 

• 

Two-Way Language Immersion Program - Viejo Elementary has implemented a K-5 Two-Way Language Immersion 
Program starting in the 2010-11 school year. The school has increased it's two way population in past few years and we 
now have students in kindergarten through eighth grade in the two-way program. The goal of this program is for students 
to be bilingual and biliterate by the end of their 5th grade year. Viejo Elementary is the 3rd school in the Capistrano Unified 
School District to implement the Spanish Two-Way Language Immersion Program. 

• Viejo Elementary has an all day kindergarten program. Viejo is one of only a handful of schools in Capistrano Unified that 

• 

offers this program. 
Technology - Viejo Elementary offers technology to all students. All of our classrooms are equipped with computers and 
internet access. Viejo Elementary has purchased Chromebooks and iPad carts to support instructional goals and curriculum 
standards throughout the school. 

• Mandatory School Uniforms - Viejo Elementary school has a mandatory school uniform program that has been in place for 
several years. Our program encourages appropriate dress and has been supported each year by our Parent Teacher 
Association. 

 

 

2017-18 School Accountability Report Card for Viejo Elementary School 

Page 2 of 11 

 

CITIZENSHIP, CHARACTER COUNTS AND COMMUNITY SERVICE 
The Viejo community prides itself on creating a safe and nurturing learning environment that enables each student to reach their 
maximum potential. At Viejo, each student, teacher and staff member strives to do his or her BEST! The following is the Viejo School 
Pledge: 
 
Viejo Student Pledge: 
I am proud to be a student at Viejo Elementary. 
 
With the support of my teachers and my family, 
 
I will work hard every day to build my character and my skills in Reading, Writing, and Math. 
 
I will work every day to prepare for my future. 
 
I am smart. I am important. 
 
My teachers believe in me. I believe in myself. 
 
I am COLLEGE BOUND!