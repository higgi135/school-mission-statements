Opened in 1959, Rubidoux High School (RHS) is the oldest comprehensive high school in the Jurupa Unified School District (JUSD). RHS 
is located in the City of Jurupa Valley and serves the communities comprising the eastern section of the district. Demographically RHS 
has an ethnically diverse enrollment of approximately 1,600 students (91% Hispanic, 4% White, 3% African American 2.8%, .5% Asian, 
and 1.5% other). Approximately 32% of our students are English Learners, 76% of our students qualify for free or reduced lunch. 
 
At Rubidoux, we are committed to providing our students with the opportunity to receive an exemplary education. We strongly believe 
that all students can learn and that the skills and concepts students need to go to college and/or have a career and succeed in life are 
accessible for all students. Rubidoux High School has implemented strong instructional programs and extracurricular activities that 
support the Common Core State Standards (CCSS) in English and Mathematics focusing on preparing our students to be college and 
career ready by the time they graduate from RHS. We have created a school culture of respect and responsibility based on our SLOs 
(Schoolwide Learner Outcomes) and the implementation of PBIS (Positive Behavior and Intervention Support System). We have a small 
learning community, RECHS (Rubidoux Early College High School), in partnership with Riverside Community College that engages and 
supports our students to take college-level classes. We accommodate individual learning styles while maintaining high, obtainable 
expectations for all students. Rubidoux High School is extremely proud of our rigorous academic programs (RECHS, AP and AVID) and 
career pathways (Agriculture, Culinary Arts, TV/Video Production, Graphic Design, Photography, Medical Assistant Administrative 
Professional, and ROP Auto body) that enhance the skills needed to be successful in today’s global economy. We offer the following 
extracurricular activities that actively engage all students and provide a well-rounded educational experience: various school clubs, 
ASB, AVID, athletics, band, choir, and cheer. Students who participate in extracurricular activities are more connected to the school 
community. 
 
Students at Rubidoux High School begin their school day at 7:15 a.m. and end their day at 2:10 p.m. after completing six periods of 
instruction. The school year is divided into two 18 week semesters. Successful completion of each course earns five credits per 
semester. Academic class size has a maximum enrollment of 36 students per class. All students also participate in an Advisory program 
on our campus that helps to give students information outside of the content area that focuses on increasing their literacy skills, 
writing skills, and helps to give them information that will prepare them for the future. Rubidoux High School is making gains to close 
the achievement gap. We are also seeing gains in the number of students pursuing post-secondary education options and high school 
career preparation programs. Parents are very important members of our school community and we invite them to actively participate 
in our school parents organizations like School Site Council, English Learners Advisory Council, and Booster Clubs, which meet regularly 
throughout the school year. We offer monthly Parent seminars as well as Coffee with Guidance Coordinators. RHS teachers, 
administration, parents and students work together, as a team, to create a learning environment that promotes academic and social 
development, teaching integrity and pride, and modeling learning as a lifelong adventure. We are excited about our school and 
welcome all to join in our efforts to create successful, responsible and productive members of our democratic society. The staff at 
Rubidoux High School is here to provide the best possible education to our students and the best possible service to our community; 
the education and the service they deserve! 
 
We believe we can best carry out our mission by focusing in the following areas: 

Student Involvement in His/Her Education Goals 
Fostering a Safe and Caring Learning Environment 

• Academic Achievement for All Students 
• 
• 
• Welcoming Parents as Partners in the Education Process 
• Demonstrating and developing perseverance, respect, integrity and dedication to excellence as a school community 
• Building positive relationships with students, staff, parents and the Rubidoux community 

To maintain effectiveness as an educational institution in the 21st century, Rubidoux High School has established small learning 
academies and is an on-site Early College High School program in partnership with the Riverside Community College District (RCCD). 
Students earn their high school diplomas while concurrently earning college credits. 
 

2017-18 School Accountability Report Card for Rubidoux High School 

Page 2 of 15 

 

In addition, we offer our students a variety of academic programs such as Advanced Placement, career preparation through Regional 
Occupation Program/Career Technical Education (CTE), curricular programs such as AVID, Agriculture, AFJROTC, Yearbook, Foreign 
Languages, Visual and Performing Arts to support the core curriculum. 
 
Our Vision: To provide a safe and dynamic learning environment through collaboration with students, staff, parents and the 
community. Our high expectations challenge all students to realize their full academic potential and become successful, contributing 
members of Rubidoux High School and society. 
 
Rubidoux High School’s core values are: Perseverance, Respect, Integrity and Dedicated to Excellence (PRIDE). You will find these core 
values modeled by the Rubidoux High School community and stakeholders. 
 
Schoolwide Learner Outcomes (SLOs) – All Rubidoux High School students will SOAR with Pride: 

 Seekers 

o 

Seek knowledge and new learning experiences 

o 

Seek and evaluate multiple solutions to a problem 

o 

Seek a variety of ways to communicate your thoughts and solutions 

 Outstanding Citizens 

o Demonstrate respect through compassion, tolerance, and integrity 

o Respect yourself and others 

o Work with others, respecting their thoughts and opinions 

 Achievers 

o Challenge yourself to achieve your full academic potential 

o 

Set and achieve worthwhile personal goals 

o Achieve and maintain wellness 

 Responsible Individuals 

o Responsible for your own lifelong education and growth 

o Responsible for your own choices 

 

 

 

 

 

 

 

 

o Responsible for contributing to the advancement of your school, community, state, nation and the world