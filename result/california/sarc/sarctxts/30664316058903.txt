Mission Statement: 
The mission of Walker Junior High School is to provide rigorous programs and curricula, in which our students are challenged to meet 
high levels of academic achievement. Students will become compassionate life-long learners as they develop a greater sense of their 
role in the global community. Walker will provide a comprehensive system of support services that will ensure that all students learn. 
 
General Information: 
Walker Junior High School is truly a community school with strong connections to both the local businesses, families and a strong 
alumni base. We are a two-year junior high school that offers seven class periods per day. Seven periods allows students to have two 
elective classes. As a result, in addition to a strong and rigorous core program, students have the opportunity to experience band, 
theatre, choir, art, Spanish, Korean, home economics, woodshop, computers, multi-media, STEAM (Science, Technology, Engineering, 
Arts, and Math), multi-cultural awareness, yearbook, and speech. We believe in exposing students to a variety of electives, so that 
they can experience an array of offerings, and see if there are particular pathways they are interested in following in high school. Our 
core classes teach the content standards through literacy and collaboration, communication, creativity and critical thinking. We strive 
to teach our students 21st century skills, so they are prepared to succeed in high school, college, and in their careers. 
 
Highlights: 
At Walker Junior High School, its STEAM Academy was recognized as a Gold Ribbon model program in 2015. The STEAM Academy, 
focuses on several STEAM-centric learning opportunities, including computer programming through the lens of video game 
development, electronics, and microcontroller programming culminating in robotics work. 
 
Demographic Information: 
Walker Junior High School, located in La Palma, California, serves 1,095 students, in which 48% participate in the free and reduced 
meal program, and 10% are English Learners. The demographic profile also indicates the following regarding student subgroups: 44% 
Hispanic, 18% Asian, 17% White, 10% Filipino, 4% African American, 1% Native American/Pacific Islander, and 6% indicate multiple 
ethnic groups.