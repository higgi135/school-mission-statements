School Description: 
 
Northgate High School opened in September, 1974, and is situated on forty bucolic acres at the foot of Mt. Diablo. Facilities include a 
multi-media center, teacher work area, a theater, five computer labs, fifteen Chromebook carts, an eighty seat lecture hall, 
gymnasium, aquatic facility, language lab, as well as graphics, photography and art studios. A new, state-of-the-art Technology Center 
was also just completed allowing for computer rendering, 3D printing, computer programming and robotics applications. 
 
The Northgate High School community believes that its task is to help students develop as involved learners and productive citizens 
through full access to, and participation in, a rigorous core curriculum supported by a safe environment. We encourage: 
 
A curriculum that challenges and engages all students appropriately while increasing their awareness of technical, professional, and 
recreational opportunities 
 
Mutual respect and civility including an appreciation for cultural, racial, religious, and socioeconomic diversity 
 
Support for all students as they face the emotional and intellectual challenges of school, extra-curricular activities, and transition 
beyond high school A sense of responsibility for self, others, and our environment which results in cooperation among parents, 
teachers, students, and the community 
 
Northgate's Mission Statement: 
 
The mission of Northgate High School is to develop informed, responsible and productive citizens who are prepared to contribute and 
thrive in a global society. 
 
Northgate's Mantra: 
 
Excellence for, and from, all students. We support happy, healthy, and innovative people. We are globally inspired.