Workman Avenue Elementary School is located in the City of West Covina and is part of the Covina-Valley Unified School District, 
which includes nine elementary schools serving grades K-5, three middle schools serving grades 6-8, three comprehensive high schools, 
and one alternative school. Workman Avenue Elementary School opened in 1956 and currently serves 460 students in Transitional 
Kindergarten through fifth grade. Workman's student population is represented by 84% Hispanic, 6% Caucasian, 3% Asian, 2% African 
American, 4% Filipino students and 1% other sub groups. Approximately 22% of our students are English Language Learners. Workman 
is a Title I Schoolwide School that receives categorical funding from Title I based on its 75% socioeconomically disadvantaged 
population. The Special Education program consists of two SAI (Specialized Academic Instruction) classes serving students in 
Kindergarten through fifth grade. 
 
The District strives to maintain a class size of 24 in Kindergarten through third grade. The average class size in fourth and fifth grade is 
31 students. Workman's faculty includes regular education teachers, special education teachers, music teachers, physical education 
teachers, and a Title I Intervention Specialist. Teachers are credentialed and have assignments within their credential authorization. 
Advanced degrees are held by 42% of the certificated staff. In addition, highly qualified instructional aides assist student learning. 
 
California State Standards and California Common Core State Standards clearly define what students should know and be able to do 
in Language Arts, Mathematics, Science, and History/Social Science, as well as English Language Development. Workman teachers 
meet frequently in Professional Learning Communities to discuss best practices, student learning, assessment results, intervention 
programs, and other instructional topics. Teachers use research-based instructional strategies that promote active involvement of all 
students. Site priorities are based on ongoing data collection and analysis. Teachers will analyze a variety of data to drive their 
instruction and measure student progress including: District Interim Assessments, Common Formative Assessments, Accelerated 
Reader STAR reports, i-Ready Diagnostic Tools, and CAASPP Interim Block Assessments and Summative Assessments. 
 
All teachers have been trained by Code to the Future on Computer Science Immersion and have implemented computer science into 
the core curriculum. Students have one-to-one computers to utilize both in computer science instructional time and in cross-curricular 
areas. Parents and community members are invited onto our campus three times per year for showcases to see examples of learning 
that is taking place in the specific area of computer science. 
 
All teachers have had training on Effective Lesson Design and Delivery. Teachers will continue to receive ongoing staff development 
on lesson design and delivery throughout the year to expand their knowledge and increase their implementation of these strategies. 
Staff development will focus on strategies to help students access the Common Core, including augmenting rigor, text dependent 
questioning, close reading, Thinking Maps, and Write From the Beginning and Beyond. As a result, all students are expected to develop 
strong reading, oral, and written communication skills. District Interim Assessments are administered three times a year by the 
classroom teacher to measure progress toward CA CCSS in English Language Arts and Math. Students determined to be at-risk in 
reading are monitored monthly by the Principal and Learning Specialist. Assessment data is used to determine placement in 
intervention programs (RTI Pyramid). Daily thirty minute reading and math intervention blocks are scheduled for at-risk students in 
TK-5th grade. Placement and effectiveness of the intervention program are reviewed monthly by the Principal and Learning Specialist. 
 
All Workman teachers are certified to work with our English Language Learner population. SDAIE strategies are incorporated into 
lessons throughout the day to make the content comprehensible for our second language learners. EL students are provided daily 
leveled ELD, as well as daily reading interventions as needed. We continue to reach out to our EL parents through ELAC, parent 
workshops, and EL parents meetings. 
 

2017-18 School Accountability Report Card for Workman Avenue Elementary School 

Page 2 of 12 

 

Parents and community partners provide valuable support and funding to educational programs. Parent participation and involvement 
in school activities are essential to student success. We continue to expand the number and type of school events in order to encourage 
and promote parent involvement. School functions include informational meetings, parent trainings, parent/child reading events, and 
family nights. Events are scheduled during the school day, before and after school, and in the evenings in order to reach a variety of 
parents. For the past several years, Workman has established a partnership with Christ Church of the Valley/Kaleidoscope Project to 
provide a variety of after-school activities including tutoring, mentoring, dance, art, and sports. This volunteer-based program provides 
extra-curricular activities to Workman students at no cost. 
 
WORKMAN VISION STATEMENT 
We, the faculty and staff at Workman Elementary School, will be leaders in the educational community, providing a well-rounded and 
diverse education that promotes lifelong learning and inspires leadership among our students. We will ensure that a safe, nurturing, 
and challenging environment exists for everyone. Students will gain academic excellence, self-esteem, and pride through achievement 
and accomplishment. We will serve as the catalyst to strengthen community partnerships, maximizing opportunities for all students 
to reach their greatest potential. 
 
Workman’s top priority is maximizing student learning. Along with our school wide instructional focus on reading comprehension, we 
will align all programs, professional development opportunities, resources, and parental and community involvement to assist all 
students to meet or exceed standards on standards-based assessments in all academic subjects. Our curriculum and instruction 
provide an exemplary and balanced educational program to our students with an emphasis on computer science and coding embedded 
throughout our curriculum. All students have access to highly qualified teachers who employ a variety of instructional strategies to 
provide a rigorous standards-based curriculum. Students will benefit from the multitude of experiences they enjoy in computer 
science, the arts, drama, and athletics. 
 
Workman is resolute in its commitment to close the achievement gap. All students at Workman will flourish given strong instruction 
and appropriate interventions guided by expert leadership. 
 
Through strong connections, positive relationships and high expectations, a four-year college readiness culture provides a vision for 
excellence that permeates our school. Students will leave Workman Avenue Elementary School with a heightened awareness of how 
college is vital to their future success. Students will leave our school with the skills and experiences to compete with other students 
across the nation. 
 
WORKMAN MISSION STATEMENT 
The mission of Workman Avenue Elementary School is to provide a safe, challenging and positive learning environment where all 
students will acquire the knowledge and skills essential to achieve their full potential and become responsible, productive citizens. All 
faculty and staff will utilize best educational practices and research-based instructional strategies and hold high expectations of 
students to meet the needs of all students.