School Description 
Rincon Valley Charter School (RVCS) is located on two campuses, one on the Sequoia Elementary School campus, one on the 
Matanzas/Spring Creek Charter campus in Santa Rosa, California. Our charter school provides excellence in middle school education 
in a small, secure atmosphere, where students hold themselves to high academic and personal standards. Our Project-Based Learning 
environment bridges all areas of the curriculum and units of study are based on Common Core and California State standards. Our 
charter school is unique, as each of our 330 middle school students is equipped with a laptop computer that connects them to the 
global learning community. Our technology-driven curriculum gives students the tools they will need to become leaders and 
productive citizens in the 21st century. 
Our experienced and dedicated staff is accessible and approachable. We believe when home and school communicate effectively, 
student success rates rapidly rise. RVCS students are on a constant growth trajectory filled with innovation, challenge, and recognition. 
In addition to our vibrant core of subject content areas, RVCS students can participate in an award-winning band and technology 
electives. Fine arts, championship athletics, and elective courses provide students the opportunity to shine in multiple environments. 
This was formally highlighted with a 2007 California Distinguished School Award. 
 
Purpose Statement 
The purpose of Rincon Valley Charter School is to provide an excellent and challenging technology-empowered program in a small 
school setting. We provide a student-centered, project-based learning environment in which technology, fine arts, inquiry, and service 
learning are the foundational components. We differentiate our instruction to meet the academic needs of all students. We strive to 
develop a 21st century learner by building relationships, using critical thinking, and engaging curriculum to meet academic standards. 
To ensure the success of our purpose, we will measure and evaluate our efforts using state, district, and school standards. We are 
here to inspire students to be self-motivated, competent, lifelong learners and exemplary citizens with the help and guidance of the 
entire school community.