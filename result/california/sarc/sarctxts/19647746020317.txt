Greetings Students, Parents, and Community Stakeholders: 
 
As your Principal, I am extremely grateful to be part of the Wilson Elementary Wildcat family. I am thrilled to be the educational leader of 
such a student-centered school. Wilson Elementary School has dedicated and talented teachers who are motivated to help each and every 
student succeed. This is clearly evident when one looks at the Common Assessments scores that continue to improve. One of my goals is 
to support and promote the established foundation of collaboration between teachers, and further cultivate the highest degree of 
education for every student, ensuring continued growth. Our teachers will continue to work in their collaborative teams to develop the 
most effective teaching strategies to ensure students are truly grasping the critical concepts of the Common Core curriculum in every 
classroom. Rigorous, student-centered, hands-on instruction enhanced by Blended Learning technology and ALD / ELD streamlined 
strategies within the 50 / 50 instructional approach is also evident in every classroom in our school. 
 
We believe that our progressive instruction should help students develop intellectual abilities and interests, social awareness, personal 
competencies, as well as a code of ethics. Hence, students explore subject matters from a variety of platforms. Our high-quality instruction 
guided by the Common Assessments provides a balance between gaining a solid knowledge and learning foundation, exploring a wide 
range of inter-connected subjects, and obtaining needed skills for success in middle school, high school, college, and careers. Our 
instruction is designed to provide students with a wide range of opportunities and experiences, expanding their neurological capacity for 
life. 
 
Expanding beyond just knowledge and facts, we emphasize deeper understanding and connections, as well as learning how to learn. As 
students begin to take more responsibility for their own learning, the faculty members remain supportive and nurture personal growth. 
Teachers work to build explicit links between subjects, with a theme-based approach, in order to deepen students’ understanding of the 
world in which they live and to enable students to ask higher-level thinking questions. Our hope is to engender students’ sense of wonder 
and possibility and develop a belief in every student that they are a powerful learner. Wilson Elementary School is a vibrant and diverse 
community, promoting personal and intellectual growth. We provide challenging, engaging programs in a nurturing learning environment, 
devoting care and attention to every student. Embracing high standards of character, commitment, and achievement, Wilson Elementary 
School encourages students to think critically and creatively and to act ethically. 
 
We are fortunate to have a strong community at Wilson Elementary School. There are numerous ways for students and parents to stay 
involved. I am overwhelmed by the amount of support and pride I see every day from our amazing students, parents and the community. 
I am acutely aware of all the hard work the team has done over the past many years to establish and improve the school environment 
and am so proud to call myself a Wildcat. I firmly believe in open and meaningful communication between school and home. My door is 
always open so please take the time to stop by and say hello. 
 
Wilson Elementary School is, has been, and will continue to be a uniquely exceptional educational institution. I am extremely pleased to 
be a part of this truly special organization as your Principal and leader. I am honored and humbled to serve this community at Wilson 
Elementary School. We will continue to work hard to make the Wilson Elementary School experience a tremendous one. The heart of any 
school begins and ends with the people who enter it every day, from the staff, to the students, and their families. I will work to build a 
community where each and every stakeholder is filled with a sense of pride as a contributing member of Wilson Elementary School. 
 
Sincerely, 
 
Trionne Magee, MS Ed. 
 
Principal, Wilson Elementary School 
 

2017-18 School Accountability Report Card for Wilson Elementary School 

Page 2 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Wilson Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

32 

32 

25 

2 

0 

3 

0 

6 

0 

Lynwood Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Wilson Elementary School 

16-17 

17-18 

18-19