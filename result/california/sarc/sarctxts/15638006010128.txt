Principal’s Message 
Jefferson School serves 226 K-3 students in the unincorporated area of Ford City. The students are 
provided current, State-adopted materials. We have many positive interactions with our students. 
We strive to achieve high academic success focusing on reading and writing. Our staff receives 
many professional development opportunities that focus on language skills and best practices. 
 
Mission 
Taft City K-3 Schools as a learning environment will empower our students to be successful, 
responsible, and well-educated citizens. 
 
Vision 
Taft City K-3 Schools will set the standard for excellence in public education, now and in the future. 
 

 

 

-
-
-
-
-
-
-
- 

----

---- 

Taft City School District 

820 Sixth Street 
Taft, CA 93268 
(661) 763-1521 
www.taftcity.org 

 

District Governing Board 

Dee Goodwin, President 

Keith McElmurry, Vice President 

Les Clark, Clerk 

Greg Mudge, Member 

Mike McCormick, Member 

 

District Administration 

Julie Graves Ed.D. 
Superintendent 

Nancy Hickernell 

Assistant Superintendent 

 

2017-18 School Accountability Report Card for Jefferson Elementary 

Page 1 of 9