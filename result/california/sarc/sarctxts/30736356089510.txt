Esperanza Education Center serves the entire Saddleback Valley Unified School District (SVUSD) 
and provides a diverse educational program for students with developmental disabilities, ages 14-
22. Teachers and staff are dedicated to the quality of individualized instruction intended by the 
Individuals with Disabilities Education Act (IDEA), which guarantees a free and appropriate public 
education. Our center's focus is on functional academics, independent living, vocational, 
community and recreation/leisure skills. 
 
Vision Statement: Intentional Lesson Design, Student Engagement & Innovative Strategies focused 
on the 5 Domains: Functional Academics, Independent Living, Vocational, Community and 
Recreation/Leisure. 
 
Mission Statement: 
Esperanza Education Center is a dynamic campus that houses secondary and post-secondary 
programs. We are committed to the success of all students. We create a safe and supportive 
learning environment. We partner with the community to provide students with educational and 
work experience opportunities. We collaborate with students, parents, and community to meet 
our students' individual needs and prepare them for the future. 
 
Esperanza Education Center is a viable option for school placement within the range of special 
education programs offered by SVUSD. 
 
Rochelle Stewart, Coordinator/Site Administrator 
 

----

-

--- 

Saddleback Valley Unified School 

District 

25631 Peter A. Hartman Way 

Mission Viejo CA, 92691 

(949) 586-1234 
www.svusd.org 

 

District Governing Board 

Suzie Swartz, President 

Dr. Edward Wong, Vice President 

Amanda Morrell, Clerk 

Barbara Schulman, Member 

Greg Kunath, Member 

 

District Administration 

Crystal Turner, Ed D. 

Superintendent 

Dr. Terry Stanfill 

Assistant Superintendent, Human 

Resources 

Connie Cavanaugh 

Assistant Superintendent, Business 

Laura Ott 

Assistant Superintendent, 

Educational Services 

Mark Perez 

Director, Communications and 

Administrative Services 

Dr. Ron Pirayoff 

Director, Secondary Education 

Liza Zielasko 

Director, Elementary Education 

Dr. Diane Clark 

Director, Special Education 

Rae Lynn Nelson 
Director, SELPA 

Dr. Francis Dizon 

Director, Student Services 

 

2017-18 School Accountability Report Card for Esperanza Education Center 

Page 1 of 8