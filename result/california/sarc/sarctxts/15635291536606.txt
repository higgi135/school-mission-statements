West High School (WHS) was established in 1965 and is located at 1200 New Stine Road, 
Bakersfield, California. WHS is one of eighteen comprehensive high schools in the Kern High School 
District (KHSD) and is located in metropolitan Bakersfield in Kern County. The school is comprised 
of 77 permanent classrooms, a library, a 600-seat auditorium, 1 small conference room, food 
services area, lockers, a portable wrestling room, eating areas, and a gymnasium (1,500-seat 
capacity). The staff consists of a principal, two assistant principals, a dean of students, a 
coordinator for Title I and ELL programs, directors for athletics and activities, five counselors, 86 
certificated teachers and 61 classified employees. Our certificated staff is relatively young and 
energetic; we believe the staff is a positive, cohesive team focused on success. Currently, the ethnic 
make-up of the teaching and support staff is 62% White, 22% Hispanic, 14% African-American, and 
2% Asian. We are committed to powerful teaching and learning. Student success and learning is the 
driving force behind every decision that we make. Teachers, administrators, instructional 
assistants, campus security personnel, food service workers, custodians, clerical, coaches and all 
other staff support our school’s philosophy of creating the right opportunity for every student to 
succeed. 
 
Our population has changed dramatically, as the city has grown and district boundaries have been 
re-drawn. At one time, WHS was a neighborhood school that served the city's middle-to-upper class 
families. Now, the surrounding areas are comprised of a business district, a retirement residential 
area, apartment complexes, and several low-income and Section 8 housing developments. The 
current population reflects both the lower socioeconomic levels surrounding the school, as well as 
an at-risk population that is bussed from a significant distance. West High School serves two feeder 
school districts, Bakersfield City (Curran Junior High School and Sequoia Middle School) and Panama 
Buena Vista Union (Thompson and Actis Junior High Schools). We have a diverse student 
enrollment of about 2,000. Currently, the student body demographics are 68% Hispanic, 15% 
African-American, 11% White, 2% Asian, 2% Two or more Races, 1% Filipino and 1% other. The 
English Learner (EL) population represents 7.5% of our enrollment. Approximately 83% of our 
students are eligible for the Free or Reduced Lunch Program; this is an increase of over 53% in the 
last fifteen years. 
 
West High School has been effective in developing programs to meet the needs of our diverse 
student population. The school meets the wide array of needs by offering programs such as Honors 
and Advanced Placement (AP), Gifted And Talented Education (GATE), Advancement Via Individual 
Determination (AVID), Career Technical Education (CTE) , Project Lead the Way (PLTW), Dual 
Enrollment, Title I Support, English Learners, Migrant, 9th and 10th grade Intervention, Agriculture, 
Independent Study, Home Hospital, Kern Learn (the district's on-line learning school) and Special 
Education. 
 
Specifically, the adopted Mission and Visions statements are: 
 
West High Vision Statement: 
We believe that through the creation of a results oriented culture where staff members work 
collaboratively to focus on student learning, all students can be academically, socially, and 
emotionally prepared to thrive in the 21st century. 
 

2017-18 School Accountability Report Card for West High School 

Page 1 of 14 

 

 

West High Mission Statement: 
The mission of West High School is to provide the programs and services to make it possible for all students to graduate on time with a 
high school diploma and prepared to succeed in the workplace or in collegiate studies.