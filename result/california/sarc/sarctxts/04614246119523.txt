Vision: To be a model for successful education of the whole child. 
 
Mission: To nurture and deepen each child’s academic and creative capacities using methods inspired by Waldorf education in a public 
school setting. 
 
The Blue Oak School (“Blue Oak” or the “Charter School”) is a tuition-free Waldorf-methods public school that opened in September 
of 2000 with seventeen children and one teacher. Today the school supports over 380 children and 50 employees and represents 
over three hundred families all over Butte County. Our school has outgrown our site four times in ten years due to expanding 
enrollment with some parents commuting from remote rural areas to attend. 
 
Blue Oak is committed to nourishing and educating the whole child, based on the natural developmental model. The Waldorf 
curriculum is an inspirational and disciplined approach that infuses learning with enthusiasm, creativity and significance. Blue Oak 
brings Waldorf, a formerly private-sector developmental model of educational pedagogy into the public sphere with integrity, purpose, 
and vision. Engagement of the child is one of the tenants of this model of education and it is the job of the teacher, and the life-blood 
of the school, to awaken and engage the will of a child. Children at Blue Oak model citizenry, compassion, and excellence in academic 
and social endeavors while wholly embracing standards-based curriculum, instruction and assessment. The curriculum is 
developmentally appropriate, neurologically sound, integrative and imaginative and aligned with California state standards , with an 
emphasis on practical skills highly applicable to life in the 21st century and an intentional focus on art and music.