Located in the Pocket-Greenhaven area of South Sacramento, Martin Luther King, Jr. K-8 School 
offers parents and students the opportunity to complete middle school in an environment that 
maintains the connectedness established in grade school. 
 
In addition to a strong academic program stressing reading, writing, and mathematics, students 
participate in art, physical education, vocal music, leadership, media, legos, robotics and much 
more. Over 200 computers allow students and staff the opportunity to access technology in every 
classroom as well as in a laboratory environment. Teachers are equipped with latest technology to 
utilize all of the components of our math, science and social science adoptions, as well as 
implementation of Common Core State Standards. Each classroom is equipped with a document 
viewer, LCD projector, and laptop as essential teaching tools connected to the internet. English 
Learners receive support in learning English through pull out and ELD instruction. Martin Luther 
King, Jr. K-8 School offers four different special education models to meet the needs of our students 
with IEPs including autism classes as well as two specific SDC classes. Students from pre-school 
through 8th grade receive speech services five days a week. Our school psychologist is on site two 
days a week. Additionally, GATE students are enrolled in GATE Cluster classrooms with staff who 
are fully trained and/or certified GATE instructors. 
 
Parents may also select from a menu of optional extended day programs for their children. Courses 
in music, art, science, and athletics allow students to enrich their lives and expand their knowledge. 
Teachers also arrange opportunities for students to receive extra help in reading and math before 
or after school and during breaks. 
 
A fee-based, district-sponsored day care program is also available to parents whose children attend 
K-6 classes at MLK. In addition, several family nights are offered throughout the school year 
including Common Core State Standards Workshops, an Ice Cream Social, Chili Cook-off, Family 
Movie Night, Family Coding Night and a Spring Social as well as an evening for parents to view the 
annual Science Fair. 
 
Parents play a key role at MLK in addition to participation in an active PTA, the School Site Council 
(School Improvement Program), the English Learner Advisory Committee, and the School Advisory 
Committee, parents can be seen daily working in classrooms, in the workroom, or on the yard. The 
PTA also holds fundraisers, which help support classroom and school needs and provide special 
opportunities for students. Many parents volunteer hundreds of hours a year to help make school 
activities successful. 
 

 

 

----

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Mary Harding Young 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for Martin Luther King Jr. K-8 School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Martin Luther King Jr. K-8 School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

17 

18 

23 

1 

0 

0 

0 

0 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Martin Luther King Jr. K-8 School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.