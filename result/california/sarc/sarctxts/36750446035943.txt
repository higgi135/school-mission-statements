It is the vision of the Joshua Circle Staff that we remain committed to empowering parents to be educational partners to educate for 
the 21st Century and to collaborating in grade level teams, to share best teaching practices and to continually assess individual student 
progress in reading, writing, and math, ensuring student mastery of State standards. The school is located in central Hesperia and is 
surrounded by a residential neighborhood. Joshua Circle is a TK-6 school, with approximately 700 students. 
 
Mission: Promoting Higher Achievement through Critical Thinking, Collaboration, Communication, Creativity and Innovation. 
 
Vision: Joshua Circle Elementary School, along with our parent and community partners, encourages students to be responsible and 
caring members of our community through meaningful and innovative educational programs. Our programs emphasize technology, 
real world problem solving, and application that prepares students for the 21st Century. 
 
Focus: All students will read to understand in order to critically analyze information.