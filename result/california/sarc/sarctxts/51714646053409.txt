At Lincoln Elementary School, our mission is to promote a life-long love of learning that is reflected in students' performance, self-
esteem, and citizenship. To achieve this goal, the school staff, pupils, parents, and community must work together to create a positive 
school climate that encourages high expectations and excellence for all students.