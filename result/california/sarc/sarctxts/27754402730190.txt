Vision 
 The vision of Soledad High School is to be the highest rated secondary institution in Monterey County. 
 
Mission 
The mission of Soledad High School is to develop confident, competent, compassionate leaders. 
 
Student Learning Outcomes 
 
Strength and Academic Excellence in: 
Pursuing College and Career Goals 
-Develop and apply problem solving and critical thinking skills 
-Present information effectively 
Expressing creativity in a variety of areas 
Honor and Ethical Responsibilities in: 
Learning 
-Use technology appropriately and successfully 
-Be accountable and reflective learners 
Service through Global Citizenship 
-Contribute to school culture 
-Work collaboratively 
-Understand issues of local, state, and international importance 
 
 
Soledad High School is a multi-use facility. The Soledad branch of the Monterey County Free Library is part of the school complex 
and is available for use by both students and the public during regular school hours. Additionally, evening classes for Hartnell 
Community College are held on the Soledad High School campus, allowing both students and community members can take college 
classes without driving to Salinas. 
 
Soledad High School Facilities were built in three phases. The first phase which opened in August 1999 included four classroom 
buildings, a multi-purpose room attached to a large gym and athletic facilities, an administrative office building and a large library 
built for combined use as the high school library and community library for Soledad. In 2001, three other classroom buildings were 
completed. Each classroom building has four traditional classrooms and two labs. The labs have different purposes depending upon 
student need. 
 
We have one computer lab used for digital media and one for computer science. All students are provided one-to-one iPads so that 
technology can be integrated into the learning process. Four modular buildings were added in 2015 as a result of the growing 
student body. We currently have just under 1,500 students. 
 
 If funding is available, we look forward to a new science building in partnership with local a local community college. 
 

2017-18 School Accountability Report Card for Soledad High School 

Page 2 of 18