EastLake Elementary School is one of 49 schools in the Chula Vista Elementary School District, 
including charters. The school is located in the EastLake community of Chula Vista, and serves the 
neighborhoods of EastLake Hills and EastLake Shores. In addition, our site receives students from 
the surrounding EastLake and Otay Ranch neighborhoods. A large parking area and landscaped 
grounds greet visitors, staff and students. Each of the five buildings has four classrooms, a 
workroom, and office/study rooms. In addition, there are 11 portable classrooms. 
 
EastLake Elementary School has a strong sense of community. We pride ourselves on being visitor 
friendly, service-oriented, and child-centered. Our Eagle Family works together in making decisions 
while accepting responsibility for the success of our children. 
 
Our PTA has been very active with many sponsored events throughout the year. In addition, our 
site features Visual and Performing Arts (VAPA) teachers in the areas of Music, Theater Arts, and 
Visual Arts/Graphic Design, who also serve as cross-curricular Support Teachers who reinforce 
language arts, mathematics, and technology standards. 
 
Mission 
EastLake Elementary educates the whole child by nourishing a student's social, academic, and 
verbal growth. Through in-school and after school enrichment activities, our students develop into 
independent, kind, responsible citizens who are prepared academically and intra-personally for 
college and career. 
 
In support of our mission, we have goals in our site plan to support achievement in English / 
Language Arts, Reading, and Mathematics. Achievement and progress are monitored using a 
variety of data sources: CAASPP, CELDT, Triennial Progress Reports, Local Measures, and site 
formative assessments. 
 
Our school-wide focus is in the area of California State Standards, enhancing student speaking and 
listening skills with an emphasis on the development of English Language Learners. Students work 
to increase the ability to read quickly, and effortlessly with meaning and expression, while focusing 
on the meaning and vocabulary words. 
 

----

---- 

Chula Vista Elementary School 

District 

84 East J Street 

Chula Vista, CA 91910-6100 

(619) 425-9600 
www.cvesd.org 

 

District Governing Board 

Leslie Ray Bunker 

Armando Farias 

Laurie K. Humphrey 

Eduardo Reyes, Ed.D. 

Francisco Tamayo 

 

District Administration 

Francisco Escobedo, Ed.D. 

Superintendent 

Jeffrey Thiel, Ed.D. 

Assistant Superintendent, Human 
Resources Services and Support 

Oscar Esquivel 

Deputy Superintendent, Business 

Services and Support 

Matthew Tessier, Ed.D. 

Assistant Superintendent, 

Innovation and Instruction Services 

and Support 

 

2017-18 School Accountability Report Card for EastLake Elementary School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

EastLake Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

32 

1 

Teaching Outside Subject Area of Competence 

NA 

28 

28 

0 

 

0 

 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

EastLake Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.