Linda Vista Elementary School is a Visual and Performing Arts School that partners with Joseph George Middle School. We believe that 
incorporating fine arts into the curriculum has had significant impact on the academic and social success of our students. 
 
Linda Vista Vision Statement 
 
Linda Vista Elementary is a safe, friendly school where teachers, staff and parents respect each other and work together to provide a 
challenging educational experience that supports all students through its integration of the visual and performing arts. 
 
We need your continued support and encouragement to assist your child in attending school on a regular basis and arriving on time. 
Also, helping your child to be responsible for returning schoolwork will lead him/her in the right direction towards a rewarding school 
year. Your encouragement and praise will give your child the confidence to make wise decisions. 
 
We are confident in our professional abilities and commitment to create an enriching learning experience for your child. We have a 
wonderful support staff that includes an administrative assistant, a school office assistant, cafeteria staff, custodians, bus 
drivers,health aide and a librarian to contribute to your child’s successful school year. 
 
If concerns should arise, please contact your child’s teacher. Your school principal is also ready to assist if necessary. Your child’s 
successful educational experience is our business. We look forward to a wonderful school year with you!