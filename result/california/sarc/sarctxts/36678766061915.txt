Curtis Middle School was built in 1965. In 2008, a completely new facility was opened with 26 general classrooms, 3 special education 
classrooms, a library, a multi-purpose media center, and an administration office. The facility strongly supports teaching and learning 
through its ample classroom and athletic space. Curtis also houses a STEM Lab for engineering and a Medical pathway where students 
can earn their CPR certification. 
 
Mission 
We provide a rigorous learning experience to prepare students for college and career. Vision: To be a high reliability educational 
organization, which creates hope in a safe and positive learning environment.