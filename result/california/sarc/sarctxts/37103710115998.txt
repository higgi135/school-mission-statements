MISSION STATEMENT: As members of Juvenile Court and Community Schools (JCCS), we are committed to high expectations, social 
justice, and equality for all students. We value diversity and strive to eradicate institutionalized racism and discrimination in all forms. 
Our priority is to raise the achievement of all students while eliminating the achievement gap between students of color and white 
students. We accomplish this through the delivery of culturally and linguistically responsive standards-driven instruction, courageous 
and advocacy-oriented leadership, and relevant professional development. All JCCS community members stand personally committed 
and professionally accountable for the achievement of this mission. 
 
San Pasqual Academy (SPA) is located on 238 acres in beautiful San Pasqual Valley in Escondido. San Pasqual Academy is a first-in-the-
nation residential education campus designed specifically for youth in foster care. Four partners - San Diego County Health and Human 
Services agency, New Alternatives, Inc., San Diego County Office of Education, and San Diego Workforce Partnership - work 
collaboratively on campus to provide a seamless delivery of services to students. The partners provide dependency case management, 
residential, academic, and work-readiness programs and services that create a community where the students can learn, grow, and 
thrive. Currently there are 97 youth living on the campus, with 72 students enrolled in grades 9-12 attending the onsite high school. 
The students in foster care qualify for the federal lunch program; 24 of the students qualify for special education services, and 12 are 
English learners. 
 
Staffing Demographics as of December 2018: 
There are six general education teachers, 1.2 special education teachers, three part-time CTE teachers, one in-school counselor, one 
school administrative assistant, one student support specialist, and one principal. 
 
San Pasqual Academy is fully accredited by the Western Association of Schools and Colleges, thereby ensuring that all major colleges, 
universities, vocational training programs, and the military accept the diploma. With a required 220 credits for graduation, students 
complete UC a-g core requirements in English (40 credits), mathematics (20 credits), science (20 credits), social science (30 credits), 
fine/practical arts (10 credits) and physical education (20 credits). Additionally, they must complete 80 elective credits (16 completed 
courses). Spanish, art, CTE culinary arts, CTE digital media, CTE horticulture, leadership/Associated Student Body (ASB), critical 
numeracy, senior seminar, introduction to business, and kitchen chemistry are offered as electives this year. The school calendar is 
based on semesters with summer intersession for credit recovery. San Pasqual Academy recognizes that a major part of adolescent 
development includes extra- and co-curricular activities enjoyed by high school students. The academy offers co-curricular activities 
such as yearbook and ASB. ASB helps create a positive school culture by providing student voice, awards assemblies, lunchtime 
activities, pep rallies, spirit weeks, and dances. 
 
San Pasqual Academy is a member of the California Interscholastic Federation San Diego Section Southern Conference. One to two 
sports are offered each season. This year, SPA will participate in girls volleyball, eight-player football, boys and girls basketball, softball, 
and track and field. 
 
Although the school models itself after traditional comprehensive high schools, it is recognized that our students have gaps in their 
education and credits. Credit recovery is offered during the school year, as is AB216 when necessary. The school is able to offer small 
class sizes (average 10:1), which allows the teachers to offer more support. Math and literacy support are provided as an elective. The 
in-school counselor creates individual learning plans in order for the students to meet graduation requirements in a timely manner. 
After-school tutoring is provided by teachers via a SPSA goal and some students also receive tutoring via Tutor Connection. Due to 
the increase of students with IEP's, a Multi-level Study Skills class is offered in addition to the support students receive in their classes. 
 
The classrooms are equipped with projection systems and/or Chromecast-equipped televisions. Each teacher has a laptop cart and 
access to a computer lab. Each classroom has a class set of Chromebooks. The teachers post assignments and resource links for their 
classes to HAIKU. They also are using HAIKU for record keeping to share grades and assignments with invested adults. 
 

2017-18 School Accountability Report Card for San Pasqual Academy - Public View Document 

Page 2 of 12 

 

San Pasqual Academy is developing a Multi Tiered System of Support.The focus is on building school-wide positive behavior support 
so that it becomes the culture of the school. A school-wide behavior plan that focuses on restorative practices is identified as a need 
to build positive school culture. The staff is trained and continues ongoing professional learning in implementing restorative practices. 
The majority of the staff has participated in Crisis Prevention Institute training. We are also continually refreshing our training in 
trauma-informed care. Due to the level of trauma the students have experienced, the teachers experience a wide array of behaviors 
in the classroom. 
 
San Pasqual Academy, on average, graduates 23 students per academic year. Approximately 25 percent attend four-year colleges or 
universities and the remaining 75 percent transition into community colleges, vocational training schools, and work. 
 
San Diego County Office of Education's Local Control and Accountability Plan (LCAP) goals: 
Goal 1. Ensure excellence in teaching and learning so each student is prepared to succeed in college and career. 
Goal 2. Cultivate stakeholder engagement to support excellence in each student’s success. 
Goal 3. Develop coherent and transparent systems for operational excellence to support each student’s success. 
Goal 4. Support the integration and transition of students who are at risk, expelled, English learners, and foster youth to be prepared 
to succeed in college and career.