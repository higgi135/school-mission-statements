Message From Principal 
Dear Families and Community: 
Edna Hill Middle School has been selected by the California Department of Education as one of 
California’s best middle schools! We are considered a Schools To Watch “model” middle school for 
our efforts to meet the needs of adolescent students. 
 
Edna Hill School began its journey as a 6th – 8th grade middle school in 2003-2004. Our exceptional 
and committed staff wanted to design the best programs possible for students. Recent and 
emerging research confirmed that close relationships with teachers were critical to academic 
success for students in the middle grades. To that end, we set out to create a student-centered 
learning environment that responded to the intellectual, social, and psychological needs of young 
adolescents. 
 
Edna Hill Middle School has shown dramatic academic gains on state testing. Our efforts to align 
instructional programs with the rigors of state standards began to reveal its impact on student 
achievement. The staff’s pursuit of continuous improvement regarding their own professional 
practices intensified as well. In partnership with our district’s Curriculum and Instruction 
department, teachers learned from each other through peer observations, collegial dialogue, and 
research-based strategies that work for kids. We are continuing to work on our upward trajectory 
as we implement the California State Standards. 
 
As a result of our efforts to meet the needs of all students, we have been selected as a California 
and National Schools To Watch – Taking Center Stage model school. We recognize this honor as an 
opportunity to develop relationships with other schools and to continue championing student 
achievement in the middle grades. We have also achieved the status of being “Re-designated” as a 
Schools To Watch Model School in 2010, 2013 and 2016. We are exceptionally proud of our entire 
school community, and the school district that supports our efforts. 
 
Mission Statement 
Vision - Edna Hill Middle School 
E-Establish rigorous expectations for learning and utilize innovative approaches to reach all 
students. 
H-Healthy relationships between students, families, and staff are fostered through mutual respect 
and advocacy. 
M-Modeling respect creates a ‘Culture of Care” in our learning community. 
S-Solid organizational structures allow for collaboration and accountability ensuring our school 
continues on a trajectory toward excellence. 
E-Establish rigorous expectations for learning and utilize innovative approaches to reach all 
students. 
H-Healthy relationships between students, families, and staff are fostered through mutual respect 
and advocacy. 
M-Modeling respect creates a ‘Culture of Care” in our learning community. 
S-Solid organizational structures allow for collaboration and accountability ensuring our school 
continues on a trajectory toward excellence.

2017-18 School Accountability Report Card for Edna Hill Middle School 

Page 1 of 9 

 

 
District Mission Statement 
The Brentwood Union School District will provide an exemplary education to all children in a safe, student-centered environment designed 
to nurture the whole child and partner with families to prepare globally productive citizens.