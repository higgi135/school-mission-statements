Description of District 
The Visalia Unified School District is the oldest school district in Tulare County. The district is comprised of 25 elementary schools, 5 
middle schools, 4 comprehensive high schools, 1 continuation high school, 5 charter schools, 1 adult school, and a school that serves 
orthopedic handicapped students. Over 32,000 students Pre-K to adult are served through the Visalia Unified School District. 
 
Description of School 
Golden Oak Elementary School served approximately 625 students in grades K-6. Our teachers and staff are dedicated to ensuring the 
academic success of every student and providing a safe and productive learning experience. The school holds high expectations for 
the academic and social development of all students. Curriculum planning, staff development, and assessment activities are focused 
on assisting students in mastering the state academic content standards, as well as increasing the overall student achievement of all 
subgroups. 
 
School Mission Statement 
Golden Oak Staff is dedicated to developing a community of students who are academically, behaviorally, socially and emotionally 
prepared to persevere in life. 
 
We will achieve this by ensuring that students: 
will have access to state standards 
will encounter a challenging and interesting curriculum 
will grow one one level per year in English Language Arts and Mathematics 
will grow one level on the ELPAC, if they are English Language Learners 
will interact with a variety of instructional strategies to enhance the learning environment 
will develop a sense of significance and belonging through connections with teachers, staff members and the other students 
 
Additionally: 
We promote mutual respect between home and school 
We promote respect and appreciation of diverse groups and cultures 
We promote effective communication between parents and schools 
We implement outreach strategies to provide information, education and support for parents