Cerro Villa Middle School has a long standing tradition of excellence in the community. We are one of five middle schools in the Orange 
Unified School District located in the quiet residential community of Villa Park, in northeast Orange County. With six feeder elementary 
schools, our population is presently 1,000 seventh and eighth grade students for the 2018-2019 school year. The campus, our home 
away from home, is well-maintained and shows our sense of pride. Guest teachers, visitors and parents consistently comment about 
the warm, hospitable and professional staff as well as the cheerful and respectful nature of our students. 
 
Our mission statement, “Through collaborative efforts, the staff, students, parents, and surrounding community of Cerro Villa Middle 
School are dedicated to fostering diverse educational programs in a safe and caring environment where all students have the 
opportunity to experience academic success and develop the habits of strong character,” illustrates our commitment to student 
learning, achievement and ethical behavior. Now, we have added our PBIS (Positive Behavior Intervention and Support) goals and 
slogans: "CVMS...A Caring Community. Vikings always make a difference. At CVMS students and staff Choose Respect, Value Yourself 
and Others, Make it Safe, and Strive for Success. CVMS is a caring community of professional learners. We strive to do our personal 
best and celebrate each other's success." 
 
CV is a standards-driven school where passionate professionals and supportive community share a common vision and work 
cooperatively to provide our students with a superior education. CV’s talented teachers engage our learners with a variety of 
motivating instructional strategies in lessons that promote 21st century skills of collaboration, communication, creativity, and critical 
thinking. We scaffold, modify and intervene to support all student achievement. We work together to align instruction through analysis 
of achievement data, student work and by sharing best practices. Common expectations and consistent routines in all classes succeed 
in creating a powerful learning environment and confident middle school adolescents. 
 
With funding from an EETT-c Technology Grant in 2011, Cerro Villa was awarded a laptop classroom consisting of forty-four laptops 
along with tables and chairs in room 206. Research reports, class projects, document-based questioning and team projects are 
completed in this lab. Our second lab in room 101 was recently refurbished (Summer 2016) offering 40 desktops available for student 
use. In addition, 172 iPads, 150 Chromebooks, and 60 HP Stream laptops are currently maintained in carts that serve as labs on wheels 
to be accessed by all of our students. Our students and teachers are excited to utilize the increase in available technology devices. We 
continue to strive for all of our students to have technology available throughout the day whenever needed. Therefore in our fourth 
year, we are encouraging a BYOD (Bring Your Own Device) program. Through this program, students may bring their own device to 
use in all their classrooms whenever a device is needed for educational purposes. Our students understand what it means to be 
digitally responsible, how to become digitally literate and how to utilize these tools as 21st century learners who are college and career 
ready. We are very proud of our staff for their dedication to increased technology use on campus and in serving the needs of our 
students. With the production of CVTV, a video production class elective, all of the community may get their own look at our talented 
students on Success TV in the Main Office. This Success TV illustrates through pictures and video all of the things that make CV the 
best middle school in the district. 
 
CV maintains a positive and nurturing social climate conducive to peer support. Numerous extra-curricular programs, leadership 
opportunities and over 17 clubs such as after-school sports, Honor Society, Robotics Club, Anime, Math Club, French Club, etc. involve 
students in cultural and academic extensions as well as physical activities. Character education permeates every aspect of campus life 
from standards-based lessons with respect, integrity, and responsibility, to daily words of wisdom from the staff. Teachers stand 
outside during passing periods to greet students as they enter the classroom. Students frequently give back to the community by 
donating time and resources to philanthropic projects. 
 
CV’s paraprofessionals, counselors, classified and nutritional staff, psychologist and nurse add positive support to the academic and 
social/emotional development of our students with their caring attitudes and selfless dedication. Our program is further strengthened 
by enthusiastic parental support in virtually every activity on campus including PFSO (Parent, Faculty, Staff Organization), School Site 
Council, ELAC, Dad's Club, Parent Meetings, classroom assistance, dance chaperons, and fund-raising events. 
 

2017-18 School Accountability Report Card for Cerro Villa Middle School 

Page 2 of 12 

 

We are very proud of Cerro Villa Middle School; our students and staff want to be here which is evident by our energy and hard work. 
CV’s recent selection as a top 10 middle school in the nation with our STEM program, recognition as a California Distinguished School 
on May 21, 2013, and district and community awards demonstrate our distinction in this community. Our work will never be done as 
we persistently pursue continuous achievement for our students. In May 2015, Cerro Villa became the recipient of the prestigious 
Gold Ribbon School of California award. This award set CV apart as a state model program with special emphasis in working with 
struggling students. We are also very proud to announce that we are the first WASC (Western Association of Schools and Colleges) 
Accredited Comprehensive Public Middle School (Grades 7-8) in Orange County, receiving the award in June 2014 and again in June 
2017. We are accredited through June 2024! Cerro Villa continues to shine with our most recent national and state recognition as a 
2018 California Schools to Watch model middle school!