The mission of Gladstone High School is to educate, empower, and prepare all students to become 
confident, empathetic, global citizens who can succeed in an ever-changing world. We are 
committed to high expectations for individual academic success with meaningful support. Together 
with students and parents, we will create a community of respect and responsibility. 
 
Gladstone Gladiator graduates are: 
 
Globally Aware through academic experiences found through their college or career path. 
Lifelong Learners who are self-directed, effectively communicate and purposefully collaborate. 
Academic Achievers who strive for excellence in all disciplines of study. 
Driven 21st Century innovators who are systematic problem solvers. 
Socially responsible citizens who advocate for our community and beyond, value diversity and 
support one another. 
 
Gladstone High School is a comprehensive high school in the city of Covina but part of the Azusa 
Unified School District. Our school is staffed by high quality educators who value diversity and 
believe that all students can achieve in a friendly, family-oriented school climate. We are 
committed to excellence in all academic areas, and support that commitment with action to 
improve instruction through professional learning communities. Our program offers a number of 
options for all students to be college and career ready, including our Advanced Placement (AP) 
program, Early College Program, our Medical Academy, AVID, and other developing programs. 
Gladstone High School offers an array of extra-curricular and co-curricular activities aimed at 
helping students to develop positive relationships and to set personal as well as collective goals 
towards success. GHS is partnered with McKinley Children's Services to provide additional 
counseling and therapeutic services for students and families. 
 

2017-18 School Accountability Report Card for Gladstone High School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Gladstone High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

54 

50 

48 

0 

0 

1 

0 

1 

0 

Azusa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

441 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Gladstone High School 

16-17 

17-18 

18-19