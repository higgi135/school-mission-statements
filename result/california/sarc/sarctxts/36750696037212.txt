Cabrillo Elementary is family centered school located in the foothill community of Upland near the 
western border of San Bernardino County. Cabrillo currently has a student population of 625 
students making it the second largest elementary school in Upland Unified. Cabrillo celebrates its 
diverse population which provides our students with the opportunity to interact with many 
different cultures. Cabrillo represents a close knit community that includes homes and businesses. 
Approximately 86% of our students receive free or reduced lunch. Breakfast is provided at no cost 
to all students at Cabrillo. 
 
Cabrillo is committed to educating the whole child with equitable emphasis on academics, 
behavior, and social emotional well being. We strive to provide a safe environment where scholars 
will become responsible, respectful, and confident life long learners. We believe in a fully inclusive 
environment dedicated to the success of ALL students. We see our students as individuals and we 
provide interventions in all areas to meet our students where they are and help them to become 
college and career ready. We have a college going culture that emphasizes ALL students are able to 
go to college through hard work and perseverance. Through our school wide behavior expectations, 
our students are taught how to be prepared and responsible, to act respectfully, to work hard and 
to stay safe. We provide universal access to research based curriculum in all academic areas. In 
addition, we provide our students with opportunities to work with the most current strategies for 
social emotional well being. Students are provided with one-to-one access to technology giving 
them the opportunity to experience engaging and relevant instruction. Cabrillo Elementary creates 
a community of support that focuses on a strong first instruction and identifies and intervenes early 
to help all students succeed academically, behaviorally, and socio-emotionally. 
 
 
 
 
 

2017-18 School Accountability Report Card for Cabrillo Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cabrillo Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

29 

30 

29 

0 

0 

0 

0 

0 

0 

Upland Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

495 

10 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Cabrillo Elementary School 

16-17 

17-18 

18-19