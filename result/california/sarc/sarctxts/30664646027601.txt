Our Mission Is Learning 
Las Palmas is a community committed to bilingualism, biliteracy, and sociocultural competencies by supporting students in achieving 
their personal growth and academic potential. 
 
Las Palmas Elementary School is located in the beautiful coastal community of San Clemente. Las Palmas was the city’s first school. 
Once known as San Clemente Grammar School, it has been in use since 1927. Since then, Las Palmas Elementary has gone through 
many changes. In the beginning, the first teachers taught students of many different grade levels in one classroom. In the 1970s, Las 
Palmas welcomed young Vietnamese refugee children and helped them adjust to their new life in the United States. Today Las Palmas 
is 51% Hispanic and home of the unique Two-Way Language Immersion Program, which helps students to become fluent in both 
Spanish and English. Although the majority of our students come from the immediate neighborhood, approximately 40% come from 
other parts of San Clemente, Dana Point, and elsewhere to attend the Immersion Program. 
 
Las Palmas serves students in Grades K-5 and also runs a state-run preschool program that serves over 100 children. The total student 
enrollment is approximately 880 students. The Las Palmas staff consists of 34 certificated and 25 classified employees. 
 
The instructional programs at Las Palmas are firmly aligned with the Common Core Standards. The overall goal for all students is 
achievement at or above grade level. The instructional goals are literacy, math and technology. Teachers, students, and parents work 
towards these goals through intensive quality instruction offered by highly qualified and fully credentialed teachers. The widely 
acclaimed Two-Way Language Immersion Program is provided to all Las Palmas students and brings in students from throughout the 
district. Students complete the program in fifth grade as fully bilingual in both Spanish and English. Las Palmas also offers a STEAM 
lab, music and art, an organic garden, and many other additional programs. 
 
Las Palmas Elementary School continues to be one of the most sought after schools for parents wanting to bring their child to a future 
thinking school that provides a powerful bilingual program while it also focuses on the whole child. Each year there is a long wait list 
for the kindergarten program.