Kinney High School was established in 1966 as a continuation high school. KHS is located on the same land where a one-room school 
house once stood dating back to the late 1800s when it was called Kinney School. That same family friendly atmosphere still exists as 
teachers help students reach their goals of graduation and career preparation. 
 
Kinney’s mascot is Wiley Coyote because he never quits. We too believe “Never Give Up!” 
 
Kinney High school offers a strong academic program. Students follow the same common core state standards and are tested with the 
same district-wide tests. Kinney students are expected to perform the same quality of work in a smaller school setting. Students must 
complete 200 credits including Integrated Math 1 and Integrated Math 2 Foundations. Kinney has a class size of approximately 19 
students per teacher and an average of 145 students are enrolled per day. Students are enrolled in up to six classes. Classes are set 
up in three week sessions, each session is worth 1.0 credit. Students must attend each day and complete all the classwork. Homework 
projects for extra credit are then available. In addition to academic courses, students have access to electives including Metals, 
Culinary, Student Leadership, Sociology and Film as Visual Literature. Kinney High is proud to announce the expansion of our Metals 
Fabrication program, which now includes an Intro to Metals course. In this course students explore the basics of welding and 
machining. They then choose a Career Technical Education (CTE) pathway to enter, either Welding or Machining. 
 
Kinney High School also offers a number of after school clubs: Gardening Club, Art Club, Robotics Club and the Homework Club. 
 
Kinney offers more than just academic and vocational training. The Student Needs Assistance Program (SNAP) matches students in 
need with caring trained adults on campus to counsel, motivate and discuss with students their needs. Our staff works with the 
students so they find their own answers to student issues and improve their choices and opportunities. 
 
The District Workability Program assists students with placement in jobs and supports them while on the job. We also have 
partnerships with local military recruiters and Folsom Lake College, part of the Los Rios Community College District. These are very 
valuable resources for the students. 
 
The educational staff performs the additional tasks of Math Representative, CAC Representative, Positive Behavior Intervention 
Supports (PBIS) coach, Professional Learning Communities (PLC) coach, English coach and technology manager. Our students have 
access to a computer lab/multimedia center. All students have access to Chrome books to further support their technological 
classroom needs. 
 
This year, we have increased organized physical activities to promote growth and learning by offering physical education. In addition 
to our physical education course, We receive funding for field trips, and incentive rewards for students with excellent attendance and 
good behavior. All staff, with the assistance of our student leadership class, focuses on maintaining a positive school climate through 
our behavior interventions and support (PBIS) programs. 
 
Our staff has hosted a back-to-school night so that parents and staff can discuss program expectations. We feel that a strong 
partnership between parents and our staff can help our students progress toward graduation and/or get back to a comprehensive 
school. 
 
Vision Statement: The vision of Kinney High School is to offer an alternative place for learning while providing a safe, caring 
environment where students can improve their academic, personal and social skills to become positive, productive members of 
society. 
 
At Kinney High School, we believe that all students can HOWL: Honest, Open and Willing to Learn 
 

2017-18 School Accountability Report Card for Kinney High School 

Page 2 of 14 

 

Mission Statement: Kinney High School is dedicated to student learning. We believe that all students can learn and be successful in an 
environment that strives to meet various learning styles, academic abilities and personal needs. We provide this by offering smaller 
class sizes, by emphasizing personal responsibility and by establishing a safe and nurturing climate that enhances self-esteem. 
 
Kinney High School Expected School Wide Learning Results (ELSRs): 
 
Academically Proficient Learners who: 

• Demonstrate proficiency in math and English as demonstrated by the CAASPP 
• Meet or exceed the California State Standards in English, Math, Social Studies and Science 

Critical Thinkers who: 

• Use a variety of strategies to produce and complete academic projects 
• Read, comprehend and analyze academic text, informational materials and other media 
• Have the ability to research solutions 
• Apply creative solutions to personal and professional problems 

Effective Communicators who: 

• Write cohesive paragraphs and essays that cite evidence 
• Use technology to research, organize and communicate in an effective manner 

Responsible Citizens who: 

• Exhibit good attendance 
• Set attainable goals and follow through 
• Demonstrate the ability to work collaboratively 
• Respect diversity 

Our student enrollment, reported on the California Basic Educational Data System (CBEDS) in October 2017 was 122.