Our mission is to provide a well-balanced curriculum, complimented by high quality instruction and leadership; to graduate career-
oriented learners who are able to cope with the lifelong challenges of our global society. Calipatria High School (CHS) has a rich 
tradition of academic excellence and school pride. For 100 years, we have prepared our students for careers and the workforce and 
have sent them on to colleges and universities. This year we continued with Advanced Placement (AP) U.S. History, Language Arts, 
Spanish Language/Literature, Statistics, Government, and Biology in our repertoire of college preparatory offerings. We have also 
added numerous CTE courses in order to prepare our students for the future. We continuously work to increase student achievement 
schoolwide using strategies that address the needs of our diverse student population. These include extra-help programs in language 
arts and math for students struggling in these areas and tutorial programs created in collaboration with a variety of community 
partners. Calipatria’s curricular emphasis is on meeting grade-level standards in the core subject areas and expected schoolwide 
learning results. This year we also continued to increase parent and community involvement through a variety of parent workshops 
and school site committees and organizations. We welcome parents and community to participate in all school activities. Students 
enjoy having members of the community participate in their learning experiences. 
Joe Derma, III, PRINCIPAL