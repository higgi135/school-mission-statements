Northwood Elementary School is a public school serving Kindergarten thru 5th grade students. As part of the Berryessa Union School 
District located in San Jose, CA, we are located in the Silicon Valley, nestled against the northeast foothills of San Jose. Northwood has 
a diverse student population including a majority of Asian students, as well as Latino, White, African-American, and Pacific Islander 
making up our ADA of approximately 580 students. Our school is committed to rigorous instruction, standards-based curriculum, and 
a strong tradition of high pupil achievement. Our students master 21st century skills and consistently score above average on state 
and national tests and attend high school in the East Side Union High School District. In addition to our core instructional subjects we 
provide specialized programs in instrumental music and physical education. 
 
Age of School Buildings: Northwood was opened in 1965. This school has 26 regular classrooms, a multipurpose room, a library, and 
an administration building. In addition to the main buildings, there are five portable classrooms that have been added to accommodate 
class size reduction and special programs, after school community programs, early childhood center, and a daycare center. We have 
recently undergone modernization including, a new facade, flooring and paint, classroom furniture, and the addition of a flexible 
instructional space and the installation of solar panels. 
 
There is a pre-school/daycare on site - San Juan Batista Early Learning center. Pre-school parents are invited and encouraged to attend 
parent meetings (Coffee w/ the Principal, Kindergarten Readiness meetings) in the Spring when registration is open for kindergarten 
placement. Kindergarten registration materials are made available on-site and online for pre-school parents. 
 
All students at Northwood will be prepared for the future in a safe, nurturing environment that inspires academic and social learning 
in order to be critical thinkers and active citizens in a global community. 
 
Berryessa Union School District provides all students the skills to become lifelong learners and successful 21st century global citizens.