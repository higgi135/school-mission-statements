Pomona Alternative School serves students in the seventh through ninth grades. The campus is located at 1460 E. Holt Ave, st#100 in 
Pomona, California and shares a campus with Park West High School. The 7th-8th grade students attend four classes daily that include 
English Language Arts, English Support, Mathematics, Physical Ed.. The 9th grade students attend 4 classes per day. Course offerings 
include English Language Arts, English Language Support, Algebra, Algebra Support, Health, Physical Education, and other classes 
offered through APEX, its online educational program. Pomona Alternative School’s instructional program is grounded in the District 
and state standards, providing personalized instruction through small group instruction. Students who enroll in Pomona Alternative 
School receive a quality educational experience and the opportunity to grow emotionally, socially and academically. 
 
School Profile: Pomona Alternative School (PAS) opened in 1976 under the name Project Outreach as an Independent Study program 
serving students who exhibited attendance problems, low academic achievement and who demonstrated social/behavior problems. 
In 1985, the Pomona Unified School District’s Board of Education changed the school’s name to Pomona Alternative School. All 
students are taught by highly qualified fully credentialed teachers with 100% having CLAD/BCLAD/SB169 certification. The staff 
consists of classroom teachers, an administrative designee, a proctor, part-time kitchen and janitorial support and an office secretary. 
During the 2010-11 school year, Pomona Alternative School was reviewed by WASC and received a three-year accreditation. WASC 
returned in 2013 for a Mid term progress and was awarded 3 more years. 
 
Vision: 
Pomona Alternative School prepares students to succeed. 
 
Mission: 
Pomona Alternative School provides students with Academic Rigor, Social and Emotional, Awareness, and Successful Relationships. 
Pomona Alternative School creates 
 
SLO's 
Productive Learners who: 
Can identify and address areas of personal and academic growth 
Recognize and respect standards of behavior that are essential to achieving personal goals 
Plan for the future by setting career goals and post-secondary priorities 
 
Competent Thinkers who: 
Apply problem-solving and critical processes to real life situations 
Analyze, interpret, and evaluate information within various contexts 
Are able to demonstrate proficiency in basic academic skills 
Are resourceful and creative problem solvers 
 
Effective Communicators who: 
Convey clear messages to others, orally and written 
Receive and correctly interpret messages 
Give and receive personal support in relationships 
 
 
 
 
 

2017-18 School Accountability Report Card for Pomona Alternative School 

Page 2 of 12