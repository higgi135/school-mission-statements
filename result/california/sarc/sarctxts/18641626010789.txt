We believe all students have the potential to succeed and our responsibility is to help them achieve their potential. Our small, rural 
setting allows our students to achieve a personalized learning experience while maintaining home values. 
 
Juniper Ridge offers all of the required courses for a Kindergarten through eighth grade school. The curriculum is aligned with the state 
frameworks and content standards in the areas of reading, language, mathematics, science, history/social studies, health, physical 
education, visual and performing arts.