Sweetwater Community Day School serves students in grades 7, 8 and 9 who might benefit from a highly structured learning 
environment that provides behavioral support as well as the required educational program. Students are placed into Community Day 
School through an intake process conducted by the SUHSD office of Student Support Services. 
 
The educational environment consists of self-contained instructional classrooms with a teacher-student ratio of 18 to 1. The 
Community Day School instructional program adheres to the California Educational State Code as it relates to community day school. 
 
Students in the Sweetwater Union High School District are expected to master state and district standards which will prepare them to 
meet the challenges of the 21st century.