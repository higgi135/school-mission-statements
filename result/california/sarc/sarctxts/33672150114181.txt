Provide all learners a safe and engaging environment in which all learners will reach their full potential by cultivating positive character 
traits and meeting or exceeding academic standards.