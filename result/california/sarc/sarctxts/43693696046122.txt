In partnership with parents and the community, Ben Painter Elementary School’s mission is to empower students to reach their 
potential as whole and unique individuals through the following:focusing on communication, collaboration, creativity, critical thinking, 
the meaningful use of technology, rigorous learning opportunities, and support of English Language Learners. This will enable our 
students to become productive, responsible 21st century thinkers. Ben Painter inspires creativity and lifelong learning, which will 
prepare our students for successful careers. 
 
Painter provides School Linked Services (SLS) for all Painter families. SLS offers on-site school-based services to heal and strengthen 
individuals, families and systems by addressing needs and risks faced by children, youth, and families. Painter has continued its 
partnership with Think Together, which is comprised of 6 lead members and 1 supervisor who provide student support from 11:40am 
to 6:00pm each school day for students in Kindergarten through 5th grade. Students receive a healthy snack before participating in a 
rotation of structured activities. Think Together's standards-based curriculum intentionally aligns, engages and reinforces learning 
concepts taught during the core day instruction which in turn, positively impacts student achievement. Furthermore, we believe our 
programs can have an even broader impact beyond academic achievement in that we help to prepare students to be successful in 
college or careers after they graduate high school. 
 
In addition, Painter has partnered with our after school provider Think Together to provide us with a coach who offers recess, 
lunchtime and physical education activities for all students, and a before school recess for all students who arrive early. The Painter 
team strongly believes that parents are very important members of the school community. As a result, one of our continual goals is to 
make Painter Elementary School a significant part of your family’s life. To be the most effective we can be, we need to work together 
to create a positive learning environment for all our students. Therefore, we encourage you to become active and informed. This may 
be done by attending meetings of the School Site Council or the English Learners Advisory Committee, volunteering to assist in the 
classroom, attending as many school programs and events as possible, and by keeping close communication with your child’s teacher. 
Maintaining this type of parent participation and communication will play a vital role in your child’s success. We are also happy to be 
developing a parent participation program and will be working with all families to assist them in completing 30 hours of parent 
participation hours. 
 
Education is a partnership between home and school and thus, communication between parents and staff is essential. Please visit our 
website which is designed to provide you with helpful information about our school. 
 
Thank you in advance for your support. We welcome your suggestions, and we will solicit your help throughout the school year. If you 
need additional information or assistance, please do not hesitate to contact us at (408) 928-8400. 
 
 
 

2017-18 School Accountability Report Card for Ben Painter Elementary 

Page 2 of 11