The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters. As you read the 
Report Card, you will gain a better understanding of Sierra as a school with a record for improvement, a faculty that is professionally 
skilled and personally committed to meeting the learning needs of students and a student body which is enthusiastic and motivated 
to perform well. 
 
Sierra Middle School, located in the Hillcrest section of East Bakersfield, serves grades six, seven and eight. The 12.3-acre site is 63 
years old. 
 
Our site mission is "Together we achieve success; Together we are Spartan Strong." 
 
The vision of Sierra Middle School is "While doing what we love, we are creating a better tomorrow for our Sierra family."