North High School wants every student to leave our campus “Prepared to Succeed” in the 
workplace or at the post-secondary level. 
 
INSTRUCTIONAL PROGRAM 
What makes North High School unique is how we prepare our students to succeed through aligned, 
articulated pathways. Students who enter North as freshmen are required to complete a career-
inventory and personal interests course, Career Pathways, that leads to selection of one of the 
many pathways available in our instructional program. As seniors these same students will have 
the potential to leave us as graduates having earned up to 30 college credits in their chosen 
pathway. These credits are transferable to any post-secondary institution within California. 
 
PATHWAYS 
The following Industry Sector Pathways are available at North High School to ensure our students 
are “Prepared to Succeed" 
 
Agriculture and Natural Resources 

• Ag Business 
• Ag Animal Science 
• Ag Horticulture 

 
Arts, Media, and Entertainment 

Instrumental Performance/Education 

• Drama Performance/Education 
• 
• Vocal Performance/Education 
• Studio Art 
• Photography 
• Graphic Design 
• Graphic Production 
• Video Production 

 
Building and Construction 

• Residential Construction 

 
Business and Finance 

• Business Marketing 

 
Engineering and Architcture 

• Engineering Design - Robotics (after school club) 
• Engineering Design 
• Architecture Design - BIM/CAD 

2017-18 School Accountability Report Card for North High School 

Page 1 of 13 

 

Fashion and Interior Design 

• Fashion Design 

 
Health Science and Medical Technology 

• Kinesiology 

 
Marketing, Sales, and Service 

• Video Game/App Design 
• Media Design 

 
Transportation 

• Aviation Operations 
• Unmanned Aircraft Systems (UAS) - Drones 

 
North’s AVID program provides significant support for first-generation college students. Likewise, we are proud of the continuous increase 
in the number of students who leave North having successfully completed the A-G courses required by the University of California system. 
North's AVID program was recently named a Program of Distinction, putting it in the top 10% of programs worldwide 
 
SOFT SKILLS INSTRUCTION & CHARACTER DEVELOPMENT: STARS ON POINT 
 
In addition to academics, students need to graduate with soft skills that make them marketable in the job market of the 21st century. 
North High School is developing a positive 
 
approach to student life on campus that helps all students learn how to succeed. Students who are demonstrate these skills are “On 
Point”, and are recognized and celebrated. 
 
North High School has focused our campus climate around five pillars (S.T.A.R.S): 

• Success 
• Trust 
• Ambition 
• Respect 
• Spirit 

 
Students learn how to apply these skills in their classes. They define what it means to be a North High Star. 
 
OUTSTANDING INSTRUCTORS 
North’s teachers continue to be recognized at both the state and national levels for their tremendous professional accomplishments in 
educating our students. 

• National Association of Agriculture Educators Young Member of the Year - Natalie Ryan (one of only 6 nationwide) 
• Our Ag program was recognized as a Three Star Program - the only program in California to earn this ranking this year 
• NHS Graduate Aalexias Woolf is serving as a State Ag Officer (Secretary), one of only 6 in the state to serve in this prestigious 

capacity 
 

CO AND EXTRACURRICULAR ACTIVITIES 
North High has an outstanding co-curricular program. The choir, marching band, color guard, percussion, jazz band and cheerleading 
squad have won numerous awards and perform regularly. There are over 30 student clubs on campus including a newly established 
competitive Robotics Team! A Drone (UAS) club is starting in Spring 2019, and a Fencing club will be up and running in January of 2019! 
North is committed to offering innovative ways for our students to engage with the school in clubs and activities that are fun and help 
them prepare for post-high-school success. 
 
ATHLETICS 
Each year more than 300 athletes participate in North’s athletic program. Since 2010, North High School has won thirty-two league titles, 
and two Central Section CIF championships. 
 

2017-18 School Accountability Report Card for North High School 

Page 2 of 13 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

North High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Kern High School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

75 

87 

77 

0 

0 

5 

0 

8 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1700 

216 

12 

Teacher Misassignments and Vacant Teacher Positions at this School 

North High School 

16-17 

17-18 

18-19