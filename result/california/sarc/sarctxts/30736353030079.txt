Silverado High School embraces the motto "Everybody Is Somebody". For the past 40 years, we 
have supported students who come to us needing a chance to start over or catch up with their 
coursework. We take the time to recognize the individual needs of each student who enters our 
program. We are now extending our special brand of education to include 9th and 10th grade 
students as we expand our continuation high school program. In collaboration with high school 
credit recovery night school, Silverado also offers one of the most comprehensive arrays of 
technology training in our district. Students can earn graduation credits through a variety of diverse 
programs. During the 2011-2012 school year, we transitioned to a trimester grading calendar and 
we now have six 55-minute academic periods per day. High School Credit Recovery Nigh School 
opportunities provide another avenue for students to earn graduation credit. Regional 
Occupational Programs and community colleges offer many other opportunities for career training. 
This provides students with a better opportunity to master difficult subjects while still having the 
opportunity to earn a higher number of graduation credits per year than at a comprehensive high 
school. During the 2013-2014 school year, we worked hard to prepare for full implementation of 
the Common Core State Standards (CCSS) and focusing on academic vocabulary and non-fiction 
writing across the curriculum. The 2018/2019 school year finds Silverado High School implementing 
the first phase of Next Generation Science Standards, NGSS. Silverado High School continues to 
focus on bringing new opportunities for success to our students. The 2014-2015 school year saw 
the implementation of Edgenuity virtual learning. These online course offerings provide yet 
another opportunity for students to earn credits. During the 2016/2017 school year we focused on 
performance task and alternative assessments that allow students to demonstrate how they can 
utilize what they have learned. Project based learning allows students another way to show they 
have mastered material. 
 
David Gordon, PRINCIPAL 
 

----

-

--- 

Saddleback Valley Unified School 

District 

25631 Peter A. Hartman Way 

Mission Viejo CA, 92691 

(949) 586-1234 
www.svusd.org 

 

District Governing Board 

Suzie Swartz, President 

Dr. Edward Wong, Vice President 

Amanda Morrell, Clerk 

Barbara Schulman, Member 

Greg Kunath, Member 

 

District Administration 

Crystal Turner, Ed D. 

Superintendent 

Dr. Terry Stanfill 

Assistant Superintendent, Human 

Resources 

Connie Cavanaugh 

Assistant Superintendent, Business 

Laura Ott 

Assistant Superintendent, 

Educational Services 

Mark Perez 

Director, Communications and 

Administrative Services 

Dr. Ron Pirayoff 

Director, Secondary Education 

Liza Zielasko 

Director, Elementary Education 

Dr. Diane Clark 

Director, Special Education 

Rae Lynn Nelson 
Director, SELPA 

Dr. Francis Dizon 

Director, Student Services 

 

2017-18 School Accountability Report Card for Silverado High School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Silverado High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

15 

15 

21 

0 

0 

0 

0 

0 

0 

Saddleback Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1,157 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Silverado High School 

16-17 

17-18 

18-19