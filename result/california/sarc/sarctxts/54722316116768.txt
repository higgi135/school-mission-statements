Tulare Community Day School serves sixth through eighth grade students who have demonstrated 
the need for a controlled environment, individual attention, and distance from the atmosphere of 
a typical middle school. The goal of Tulare Community Day School is to build a community 
atmosphere that is highly academic in nature that places an emphasis on character development 
for students. Tulare Community Day School instills Character Counts with its students which means 
the six pillars of character 
(Respect, Citizenship, Caring, Responsibility, Fairness and 
Trustworthiness) are modeled, integrated into lessons and are followed each and every day. In 
addition to emphasizing academic progress, our program is designed to instill a sense of self-worth 
and accomplishment in our students, and to help them develop positive behaviors both in and out 
of school. Tulare Community Day School supports student achievement by being purposeful in 
planning the instructional day to maximize class time effectively and offering students individual 
instruction based on areas of need. Communication between the school and parents regarding the 
progress of their students is ongoing and an integral part of the program. Our program provides 
appropriate instruction in a setting that emphasizes: a highly structured environment, a place for 
students to continue to learn, and the continuation of academic pursuits in the core subject areas. 
We provide assistance to students so that they may make behavioral adjustments, stressing that 
students are held accountable for their accounts and the natural consequences of inappropriate 
behaviors. 
 

 

 

-
-
-
-
-
-
-
- 

----

---- 

Tulare City School District 

600 North Cherry Street 

Tulare, CA 93274 
(559) 685-7200 
www.tcsdk8.org 

 

District Governing Board 

Teresa Garcia 

Irene Henderson 

Melissa Janes 

Phil Plascencia 

Daniel Enriquez 

 

District Administration 

Dr. Clare Gist 

Superintendent 

Philip Pierschbacher 

Assistant Superintendent, 

Personnel 

Joyce Nunes 

Assistant Superintendent, 

Business/Psychological Services 

Paula Adair 

Assistant Superintendent, Student 

Services 

Brian Hollingshead 

Assistant Superintendent, 
Curriculum/Technology 

 

2017-18 School Accountability Report Card for Tulare City Community Day School 

Page 1 of 8