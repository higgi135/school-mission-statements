Irvine High School opened its doors in 1976. It is one of five comprehensive high schools in the Irvine Unified School District and 
supports a population of 1991 students in grades 9-12. Irvine High School is proud of its ethnic diversity. Numerous students represent 
a variety of ethnic backgrounds and speak many different languages. In 2007 Irvine High School was named a California Distinguished 
School by the State Board of Education. In 2015 Irvine High School was once again named a Grammy Signature School Semi- Finalist, 
recognizing the music program as one of the top in the nation .In spring 2018 Irvine High was awarded a six-year status of accreditation 
from the Accrediting Commission for the Western Association of Schools and Colleges. In 2012 Irvine High School was selected as a 
California Leadership Site for our exemplary program in Positive Behavior Intervention and Support. In 2017 Irvine High School was 
recognized as a Gold Ribbon School and an Arts Exemplary School by the CA Department of Education. 
 
At Irvine High School, our mission is to enable all students to become contributing members of society empowered with the skills, 
knowledge, and values necessary to meet the challenges of a changing world, by providing the highest quality educational experience 
we can envision. 
 
Irvine High School incorporates a Positive Behavioral Intervention and Support culture that utilizes the acronym of IHS to identify the 
values that are significant to the student climate and culture at Irvine High School. These values are: Integrity, Honoring Self and 
Others, and Social Responsibility. 
 
The Expected School wide Learning Results for Irvine High School students are listed below: 
 
Students will be able to: 
Demonstrate the ability to recognize and solve problems using critical thinking skills 
Demonstrate knowledge of emotional, mental, and physical wellness and will exhibit positive and appropriate interpersonal skills 
Demonstrate the ability to communicate effectively by listening, speaking, reading, writing and utilizing the technology of the 21st 
century 
Demonstrate an understanding of what it means to be a contributing member of their local, national, global, and digital communities 
Develop long and short term goals to prepare for a successful and informed transition to college and career