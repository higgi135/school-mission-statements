Welcome to William Orr Elementary School’s annual School Accountability Report Card. On behalf 
of the William Orr staff, it gives me pleasure to share information with our parents and community 
about our school’s instructional programs, academic achievement, materials, and facilities. 
 
William Orr Elementary is located in Norwalk and has approximately 400 students attending 
transitional kindergarten through fifth grade. Most of our students will continue their education 
at Lakeside Middle School, also a part of Little Lake City School District. We are a community school, 
relying on parent, community, and local business support to ensure our students receive a top 
notch education and support services. Parent involvement is a priority at Orr as evidenced in our 
numerous opportunities for parents, including our Parent Teacher Association (PTA), School Site 
Council (SSC), English Learners Advisory Committee (ELAC), and many other school events. Based 
on a 2017-2018 parent survey, 98% of our parents were satisfied with William Orr. 
 
William Orr Elementary is committed to the belief that every student will learn. The mission of 
William Orr Elementary School as a learning community, in partnership with families, is to develop 
all children into responsible, academically prepared citizens through the efforts of a skilled staff 
implementing a rigorous curriculum, rich in the diversity of a constantly changing world. We strive 
to provide a positive learning environment that supports high expectations for student 
achievement and behavior. A balanced educational program is implemented, which recognizes the 
need for growth in academic, social, physical, and technological skills. At William Orr, we use 
evidence based teaching strategies to improve the reading, writing, and math skills of all students. 
 
William Orr has been recognized numerous times both at the state and national level for academic 
excellence. In 2010, Orr was the first school within the Little Lake City School District to be 
recognized as a National Blue Ribbon School. Moreover, William Orr was also recognized in 2010 
as a California Distinguished School and a Title 1 Achieving School. We have also been recognized 
as a California Business for Education Excellence Star School multiple times (2009, 2010, 2012, 
2013, 2014, 2015, 2016, and 2017). In addition, William Orr was named a 2016 Gold Ribbon School 
and a Title I Academic Achieving School. Most recently, Orr was recognized by the California PBIS 
Coalition with a Silver Level Implementation Award for our fidelity in implementing Tier 1 of the 
PBIS framework. It is with great pride that Orr has accepted these recognitions and has continued 
to improve student achievement. 
 
In our longstanding tradition of excellence, we maintain our commitment to students, parents, 
community members, and staff to provide the best educational experience possible. We renew this 
commitment annually in hopes that together we will continue to improve our effectiveness in 
educating our students and preparing them to become productive citizens. 
 
Rebecca Casillas, Principal 
 

2017-18 School Accountability Report Card for William W. Orr Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

William W. Orr Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16 

17 

18 

1 

0 

0 

0 

0 

0 

Little Lake City School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

9 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.