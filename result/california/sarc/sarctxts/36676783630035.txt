Don Antonio Lugo High School was founded in 1972 as a ninth grade school to accommodate the 
impacted enrollment at Chino High School. Each year subsequent to 1972, a new class was added 
until Don Lugo High School became a comprehensive 9th – 12th grade high school. It is steeped in 
a tradition of excellence in the areas of academics, activities, arts, and athletics. 
 
Don Antonio Lugo High School exemplifies the definition of a comprehensive high school. Located 
in the City of Chino in the growing Chino Valley, our population is diverse and reflective of the 
community we serve. Don Antonio Lugo High School has a strong tie to the surrounding community 
and many longstanding traditions. Many of the present student body have parents who were 
graduates of Don Antonio Lugo High School. And, currently several staff members are alumni of 
Don Antonio Lugo High School. 
 
The Vision of Don Antonio Lugo High School is to prepare every student to meet the “A-G” four-
year college requirements, and to develop the 21st century skills necessary for post-secondary 
career opportunities. The Mission of Don Antonio Lugo High School it to provide students with a 
quality 21st century education that offers programs, experiences, and opportunities for college, 
career and life readiness. Our motto is “One School, One Family.” The staff of Don Antonio Lugo 
High School dedicates its efforts to encompass its commitment to the intellectual, emotional, 
social, athletic, academic, and aesthetic development of each student promoting 21st century skills 
that lead to overall success in life. 
 
The new student learning outcomes embrace the adoption of Positive Behavior Interventions and 
Supports that we have implemented this year. The following are the new student learning 
outcomes for Don Antonio Lugo High School: 

• Be Respectful 
• Be Responsible 
• Be Involved 

----

--

-- 

Chino Valley Unified School 

District 

5130 Riverside Drive 
Chino, CA 91710-4130 

(909) 628-1201 

www.chino.k12.ca.us 

 

District Governing Board 

James Na, President 

Irene Hernandez-Blair, Vice 

President 

Andrew Cruz, Clerk 

Christina Gagnier, Member 

Joe Schaffer, Member 

Alexi Magallanes, Student 

Representative 

 

District Administration 

 

Superintendent 

Sandra Chen 

Associate Superintendent, Business 

Services 

Grace Park, Ed.D. 

Associate Superintendent, 

Curriculum, Instruction, 
Innovation, and Support 

Lea Fellows 

Assistant Superintendent, 
Curriculum, Instruction, 
Innovation, and Support 

Richard Rideout 

Assistant Superintendent, Human 

Resources 

Gregory J. Stachura 

Assistant Superintendent, 

Facilities, Planning, and Operations 

 

2017-18 School Accountability Report Card for Don Antonio Lugo High School 

Page 1 of 14 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Don Antonio Lugo High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

82.9 

84.9 

76.25 

1 

1 

0 

0 

1 

1 

Chino Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1199.9 

22 

2 

Teacher Misassignments and Vacant Teacher Positions at this School 

Don Antonio Lugo High School 

16-17 

17-18 

18-19