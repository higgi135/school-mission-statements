As principal, I invite you to explore the Westminster High School Annual School Accountability Report Card, which provides valuable 
information about our school's instructional programs, academic achievement, classroom materials, school safety, facilities, and staff. 
Understanding our educational program, student achievement, and curriculum development can assist both our school and the 
community with on-going school improvement. 
 
Westminster High School is focused on providing all students with the opportunity to achieve. We strive to accommodate individual 
learning styles while maintaining high, obtainable expectations for all our students. We are extremely proud of our diverse academic 
programs, and we are dedicated to providing all of our students with an educational pathway that ensures both their academic and 
career success. Classes for college prep and fine and applied arts are an integral part of our comprehensive high school. A commitment 
to strong vocational education opportunities, outstanding athletic programs, and extensive extra-curricular activities complete the 
well-rounded educational experience Westminster High offers its students. 
 
Staff and parents work together to create a learning environment that promotes academic and social development, teaches 
responsibility and pride, and models learning as a lifelong adventure. We are excited about our school and welcome all to join in our 
efforts to create successful, responsible citizens. 
 
Our mission is to educate students to become creative, productive citizens by providing multiple avenues of learning, including 
innovative technology, a challenging curriculum, and a variety of co-curricular experiences. 
 
Westminster High School is one of six comprehensive high schools in the Huntington Beach Union High School District. Founded in 
1959 and situated on approximately 57.1 acres, Westminster High maintains a British theme that parallels the City of Westminster 
design. The school also has a five-acre working farm and a successful agricultural sciences program. 
 
Our school is proud of its diverse student population. Of our 2,820 students, approximately 48.5% are Hispanic/Latino, 42% are Asian, 
5% White, 1% American Indian, 0.8% Pacific Islander, 1.1% Filipino, and 1% African American. Twenty-two percent of our student 
body is English Learners, coming from homes that speak twelve languages. Additionally, 10.5% of our students receive Special 
Education services and 77% of our students qualify for free or reduced priced lunches.