California Virtual Academy @ Kings believes that, given a comprehensive and mastery-based curriculum, high expectations, access to 
technology, strong instructional support, a significant amount of off-line work, guidance from experienced teachers, and a strong 
commitment from parents or other caring adults, a well-conceived virtual education program will boost student achievement, serve 
the unique needs of students and families, and offer a new model for effective public education in the 21st century. 
 
The goals of our educational program include: 

Enabling all students to become self-motivated, competent and lifelong learners 
Empowering the parent as the teacher through a guided program and training 

• 
• 
• Offering flexibility and choices to increase student interest in learning 
• Using a variety of lessons, activities, and modalities to improve student achievement 
• 
• Developing reflective learners 
• Developing technology literate students 

Striving for mastery of all skills by every student 

Our Mission Statement 
California Virtual Academy at Kings provides individualized standards based education for students. The commitment of CAVA @ Kings 
is to provide a team of highly qualified staff who partner with students, parents and learning coaches to provide students the skills 
necessary for success through an innovative and challenging educational environment. We empower students to be life-long learners, 
critical thinkers and inspire a passion for learning as we teach them to persevere through life’s challenges and prepare them for an 
ever changing global culture. We thrive on our students demonstrating respect, honesty, integrity, creativity and empowering them 
to be team workers within the community and with one another. It is important for CAVA @ Kings staff to incorporate the whole child 
by molding them into strong citizens and positive contributors to society. 
 
 

2017-18 School Accountability Report Card for California Virtual Academy at Kings 

Page 2 of 17