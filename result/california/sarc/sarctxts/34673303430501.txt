The Mission of Walnutwood High School (WHS) is to provide each student an individualized opportunity, with parent and teacher 
support, to earn a high school diploma, acquire values, skills, and knowledge necessary to promote lifelong learning, enhance self-
esteem, and become productive, responsible citizens. 
 
The Vision of Walnutwood High School is to provide an alternative to the traditional classroom setting where the individual needs of 
each student are addressed. Emphasis is placed on self respect, self discipline, personal responsibility, and achievement. 
 
Our School 
WHS serves students in traditional Independent Study for grades 7-12, as well as two district programs; Adolescent Parent Program 
(APP) and Medical independent Study (MIS) for pre K-12 students. WHS is an open entry-open exit school. 
 
Independent study students typically meet with their teacher once each week for forty-five minutes. During that time teachers verify 
student homework, administer tests, give instruction, and provide new assignments. Students are primarily responsible for organizing 
their learning activities during the week; however, the teacher is available via e-mail or telephone to assist or advise students who are 
in need of help with their studies. Enrichment activities in subjects such as math, science and art are provided for students. Computers 
and supplemental computer programs plus internet access are available. 
 
Our Students 
For many of our students, WHS is their school of choice where they attend, thrive and make progress towards graduation. For others, 
WHS may not be their best option, but for a variety of reasons, it is their only option. Thus, we have a wide array of students from 
college bound, self-motivated, and goal oriented, to others who have not been successful anywhere else in their school career. We 
welcome them all and do our best to develop an individual education plan that best meets their needs. Our student enrollment, 
reported on the California Basic Educational Data System (CBEDS) in 2017 was 185.