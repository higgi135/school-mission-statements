Waterford High School (WHS) is well known throughout the Central Valley for consistent gains in student achievement. In fact, WHS 
has the highest four-year growth in academic performance of any comprehensive high school in California. In 2007, WHS was named 
a California Distinguished School. The students at WHS received the Title I Academic Achievement Award presented at the state Title 
I conference in May 2006 and 2007. WHS was the first high school in the region to reach and surpass the state target of 800 (2008). 
In the 2016-2017 school year, Waterford High School received the Gold Ribbon Award and Title I Academic Achievement Award. These 
awards are some of the most prestigious award school can earn in the state. WHS offers a diverse catalog of courses. Students may 
explore the Arts, Music and Drama. Students can also enroll in Advanced Placement and Career Technical courses.Our biggest 
challenge is sustaining academic growth over time. Our mission to engage, empower, and encourage students in a comprehensive 
education using creative instruction and challenging curriculum requires each person (teacher, student, parent, and administrator) to 
strive daily to do his best. Thank you for taking time to review our School Accountability Report Card. We hope the information in this 
report will give you a better understanding of our educational program and the level of student achievement at our school. The faculty 
and staff at Waterford High School believe that all of our students can succeed in school. Each year teachers receive training to improve 
their teaching techniques, and, as a result, we have seen improvements in the academic performance of our students.High school 
should be a time of academic skill development, social maturation, and preparation for adult life. Our goal is to provide a quality 
educational program for all students. A rich and rewarding four-year experience is available here at Waterford High School; we want 
every student to make the most of this opportunity.