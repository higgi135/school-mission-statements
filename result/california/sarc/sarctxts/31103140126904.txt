Mission Statement: The Student Services Department will increase student achievement through meaningful and rigorous instruction 
for all students. 
 
Situated in the Sierra Foothills, Placer County Office of Education (PCOE) serves over 68,000 students in multiple school districts across 
Placer County. In order to serve the students of Placer County, PCOE provides an array of programs and service to meet student needs. 
This task is accomplished by providing high quality specialized programs for students; recruiting, retaining, and developing highly 
qualified staff; and monitoring fiscal accountability and expenditures. 
 
The Placer County Pathways Charter School is located at multiple sites in Placer County. As a Placer County Office of Education school, 
Pathways serves students in grades transitional kindergarten through twelve (TK-12). 
 
The Pathways Charter School serves students in several distinct programs: 
 
iLearn Academy is an Independent Study program serving families with children in TK-8 who choose to educate their children at home 
or in a hybrid program. iLearn teachers regularly collaborate with parents and students to provide a rigorous, standards-based learning 
plan that addresses individual student needs and interests. The iLearn Academy program also provides students access to on-line 
learning, on-site classes, field trips, and school-wide events to promote community and connections with others. iLearn Academy 
typically serves 170-200 students at any one time. Demographic data: 11.86% Hispanic, .56% American Indian/Alaskan Native, 1.69% 
Asian, .56% Black/African American, 81.36% White, 2.26% Multiple, 1.69% Missing, 33.39% English Learners, 15.82% Special 
Education, 11.86% Socio-Economically Disadvantaged, 0% Foster Youth. 
 
CARE (Community Action for Responsive Education) is a County Community School program serving seventh through ninth grade 
students (7-9), offered in partnership with identified middle schools and high schools in Placer County. CARE classes provide a small, 
self-contained setting with individual student attention as part of the school's comprehensive intervention system. Parents of CARE 
students must agree to have their children participate in the program. CARE teachers provide core instruction aligned to the local 
school's curriculum, and students may attend other classes for electives at their local school, PE and other courses as they are able. 
Students enrolled in the CARE program also have access to local school extracurricular activities including athletics and social activities. 
Students will only be enrolled in a CARE class on a district campus with the permission of the host school district and the host school. 
The CARE program typically serves 20-24 students per classroom. Demographic data: 25.0% Hispanic, 8.33% American Indian/Alaskan 
Native, 16.67% Asian, 8.33% Black/African American, 41.67% White, 0% Multiple, 16.67% English Learners, 0% Special Education, 
16.67% Socio-Economically Disadvantaged, 0% Foster Youth. 
 
Intensive CARE (iCARE) is a County Community School program serving seventh through twelfth graders who are expelled, referred by 
probation or the School Attendance Review Board (SARB), or placed by parent. Intensive CARE classes provide instruction in a small 
and supportive setting. Students have access to Career Technical Education options as they make progress toward their learning goals. 
Academic and Career Counseling is provided to all students and a broad course of study is available using a blended model of in-class 
instruction and on-line coursework. iCARE typically serve students for less than one year. The school specific demographic data 
reported below was collected October 2018, but this data can be extremely variable due to the transient nature of the student 
population. iCARE Community School typically serves 30-60 students at any one time. However, a large percentage of these students 
transfer in and out of the program throughout the school year. iCARE may also provide a voluntary independent study option for 
students who are probation referred, expelled, or voluntarily placed by parent. Demographic data: 19.61% Hispanic, 1.96% Asian 
3.92% Black/African American, 1.96% Pacific Islander, 56.86% White, 15.69% Multiple, 11.76% English Learners, 17.65% Special 
Education, 41.18% Socio-Economically Disadvantaged, 1.96% Foster Youth. 
 

2017-18 School Accountability Report Card for Placer County Pathways Charter School 

Page 2 of 13 

 

The Come Back Program is an independent study program specifically designed to address the academic needs of adult students who 
have not completed high school and who wish to obtain a high school diploma. The Come Back Program reaches students who have 
"dropped out" or are not currently enrolled in any school or who face particular challenges that make daily attendance difficult. The 
Come Back Program educates students who have fallen behind in their studies, provides opportunities and resources to increase 
career/workforce readiness skills, provides individualized instruction, and assists students in earning a high school diploma. 
Demographic data: 29.41% Hispanic, 5.88% Asian, 47.06% White, 23.53 Multiple, 5.88% English Learners, 11.76% Special Education, 
70.59% Socio-Economically Disadvantaged, 0.00% Foster Youth 
 
During the 2017-2018 school year, 231 students were served at Pathways Charter School.