Butte Valley High School has been providing a quality high school education to the students of Butte Valley since 1916. The current 
building has been in existence since 1945. 
 
Currently, the facilities include two major buildings. The main building contains five standard classrooms, one science lab/classroom, 
one home economics room/classroom, one computer lab, a counseling office (with a small computer lab), a library, and a modernized 
gymnasium with a weight room and locker rooms. The auxiliary building contains an agricultural mechanics shop, a wood shop, pottery 
room and a cafeteria-scale kitchen. Six full-time and four part-time teachers are on staff along with a full-time secretary and custodian. 
 
The high school course offerings enable the students to meet the entrance requirements for the California UC/CSU university systems. 
In addition to the core subject areas (English, Mathematics, Science, and Social Science), students may choose from a variety of elective 
classes including Spanish I and II, yearbook, culinary arts (beginning and advanced) , ag mechanics, wood shop, digital media, chemistry 
or physics (in alternating years), trigonometry, and calculus. Classes are also available through Cyber High. A selection of college level 
classes are also available through the College of the Siskiyous and Klamath Community College on-line classes and distance learning 
networks. 
 
School Profile: 
Butte Valley High is the only comprehensive high school in the Butte Valley Unified School District. The school is fully accredited 
through the Western Association of Schools and Colleges (WASC). Curriculum is focused on the California Common Core Standards for 
each subject. The course selection is particularly selected to ensure students can meet the University of California A - G entrance 
requirements, however a solid selection of Career Technical Education courses are also available. The school supports cultural 
awareness on a daily basis through its diverse literature selections, the adoption of curricula in all subjects that include intrinsic cultural 
awareness, and a focus on personal relationships among the diverse students that make up the student body of Butte Valley High 
School. 
 
 
Mission Statement: 
Butte Valley High School will work in partnership to develop personal responsibility, a positive outlook, academic skills, and career 
goals for our students. 
 
Expected School-wide Learning Results: 
 
We expect that our students will: 
Become effective communicators 
Become self-directed lifelong learners 
Develop life skills 
Become responsible citizens 
 
District Mission: "All of us working together for the education of our students." 
 
 

2017-18 School Accountability Report Card for Butte Valley High School 

Page 2 of 11