Village Elementary School creates a student-centered learning environment that serves the whole child in grades ranging from 
preschool through 5th Grade. We enable students to prepare for their future by developing the skills, knowledge, and confidence 
necessary to seek a meaningful life, both individually and as a productive member of the community. 
 
Our mission statement: Through rigorous academic standards, high expectations, and a coordinated curriculum, Village Elementary, 
in partnership with our district and community, will graduate students with the knowledge and skills necessary to excel in higher 
education, careers, society, and life with the confidence not only to dream, but to determine their futures. 
 
Located in the heart of the village of Coronado, a small island resort community on the coast of San Diego, California, Coronado Village 
Elementary, a past California Distinguished School and a National Blue Ribbon School of Excellence, serves as the hub of this small city. 
Our beautiful brick building is one of three schools located within one square mile of the Coronado Unified School District. We share 
this block with the middle school and high school. Our partner elementary school, Silver Strand, is located three miles south and serves 
families living in Naval Enlisted Housing and a neighboring resort area. 
 
The Village Elementary main campus opened in 1992, and encompasses two campuses: the main campus at 600 6th Street which 
serves grades 1-5, and the Early Childhood Development Center (ECDC), which houses Crown Preschool, a fee-based preschool serving 
three and four year old students in general and special education, and Village Transitional Kindergarten and Kindergarten students. 
Between the two campuses, approx.850 students from pre-school to grade five are active, engaged learners becoming prepared for 
college and career. 
 
The community includes low, middle, and upper income homes, apartment buildings, small businesses, and hotels. Approximately 
40% of our families are military, serving on our local naval bases, ships, and submarines, making our school population highly mobile. 
Seven different ethnic groups and languages are represented at Village. The staff is committed to providing the best education possible 
for every student, by name, by need. All decisions are made based on what is in the best interest of our students. We believe in 
modeling life-long learning and are continuously attending professional development opportunities to improve our curriculum, 
instruction, and assessment practices. 
 
Village Elementary teachers meet regularly to collaborate and learn about best instructional practices focused on the needs of students 
and reflect on current student formative data to plan instruction. In addition to providing a rigorous curriculum based on high 
standards, the Village staff works in partnership with our families and community members to teach life skills through a district-wide 
program, Sanford Harmony, so students learn what it means to be a positive, respectful, and responsible citizen. The Military Family 
Life Counselor (MFLC) and the Clinical Counselor teach classroom lessons, provide group and individual counseling, and other levels 
of social-emotional supportive programs to ensure all students are achieving and succeeding. Everyone-A-Reader (EAR) volunteers 
provide one to one support in reading to students on a daily basis, and our learning labs proive additional support in reading, writing 
and math daily. In partnership with our MFLC, we host Anchored for Life for students to provide extra support to new military families. 
The Village team of dedicated professionals are committed to striving for excellence for all our students, for our programs, and for our 
learning environment. Our inclusive learning environments provide equity for all students, regardless of degree of learning or physical 
challenges. Students are included in the general education classrooms, with small group or individual assistance as needed. The 
Student Success Team (SST) program is offered for students who need assistance academically, socially, emotionally, or cognitively as 
the foundation of our Multi-Tiered Systems of Support (MTSS) framework. We actively use Restorative Practices to engage our learners 
in restoring relationships as a form of social and emotional reintegration and relationship management. Teachers and students use a 
variety of technology applications to extend thinking and to provide a challenging curriculum. The Village computer lab, library, and 
netbook computers available for each grade level, support project-based learning and a growing one-to-one program, grades K-5. Each 
classroom houses a multimedia computer workstation including an LCD projector/document camera, and many classrooms have 
Smart Boards. 
 

2017-18 School Accountability Report Card for Coronado Village Elementary 

Page 2 of 11 

 

Choir, Band, World Languages (French and Spanish), Robotics, Tennis, and many other enrichment opportunities are in place both 
before and after the instructional day to provide a wide variety of extracurricular experiences for all students on the Village campus. 
We partner with many businesses and organizations in the community throughout the school year to promote learning opportunities 
for students and provide volunteer opportunities for community members.