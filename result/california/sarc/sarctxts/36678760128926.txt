Dominguez Elementary School opened August 4, 2014. The school has 20 classrooms, a media center, library, multi-purpose room, 
and an administration office. The facility strongly supports teaching and learning through its ample classroom and playground 
space. The school also has professional office space for support staff (i.e., speech therapist, psychologist, etc.) 
 
H. Frank Dominguez Elementary School is planting the seeds of Hope, Inspiration, Knowledge, and Relationships by providing 21st 
Century skills and education that emphasizes Critical Thinking, Collaboration, Creativity, and Communication. We will encourage 
exploration in career pathways that are relevant and enable our students to be prepared to compete in the global economy.