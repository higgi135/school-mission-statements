Lewiston Elementary School is a single school district located in the beautiful and historic village of 
Lewiston in eastern Trinity County. It serves approximately 70 students in grades K-8. Lewiston 
Elementary School has adopted and implemented a curriculum based on the California State 
Standards. The school uses Common Core English Language Arts and mathematics curriculum. The 
facilities support three classrooms, a special education resource room, a music/fine arts room, 
library, cafeteria, school office, gymnasium and extensive playing areas. The school operates a 
ACES After-school Program from 2:45 - 6:00 PM that focuses on completing homework, music and 
art, physical exercise and sport teams. A state preschool operated by Human Response Network is 
located on our site. The district contracts with the Trinity County Office of Education for speech 
and language therapy, psychological services, co-operative services, and school nurse services. 
 
School Mission - To provide a safe environment which promotes academic excellence, responsible 
citizens and a life-long desire for learning. 
 
DataQuest 
DataQuest 
the CDE DataQuest web page at 
https://dq.cde.ca.gov/dataquest/ that contains additional information about this school and 
comparisons of the school to the district and the county. Specifically, DataQuest is a dynamic 
system that provides reports for accountability (e.g., test data, enrollment, high school graduates, 
dropouts, course enrollments, staffing, and data regarding English learners). 
 
Internet Access 
Internet access is available at public libraries and other locations that are publicly accessible (e.g., 
the California State Library). Access to the Internet at libraries and public locations is generally 
provided on a first-come, first-served basis. Other use restrictions may include the hours of 
operation, the length of time that a workstation may be used (depending on availability), the types 
of software programs available on a workstation, and the ability to print documents 

is an online data 

tool 

located on 

2017-18 School Accountability Report Card for Lewiston Elementary School 

Page 1 of 7 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Lewiston Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

3.5 

3.0 

3.0 

0 

0 

0 

0 

0 

0 

Lewiston Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

3 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Lewiston Elementary School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.