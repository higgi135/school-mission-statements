The Eureka Union Elementary School District encompasses 14.8 square miles in Granite Bay and 
sections of Roseville, California. The District is comprised of three Transitional Kindergarten through 
third grade schools, two fourth through sixth grade schools, and two junior high schools. Oakhills 
Elementary School has an enrollment of approximately 473 Transitional Kindergarten through third 
grade students. Oakhills School is located in Granite Bay and is nestled in a residential community. 
The Oakhills campus includes two Special Education Classrooms, a Transitional Kindergarten class, 
four Kindergarten classes, five first grade classes, five second grade classes, and five third grade 
classes. 
 
Oakhills Elementary School was recognized as a Distinguished School in 2014 and a Gold Ribbon 
School in 2016. Our mission is to nurture the intellectual, physical, and emotional capacities of each 
student to the fullest extent. Oakhills School is committed to the development of the whole child 
by providing a safe environment in which students can achieve high self esteem with respect for 
others, develop a love for learning, and reach personal potential as knowledgeable, caring, 
responsible, and contributing citizens. We believe this to be possible when the school is in 
partnership with the students, parents, and community. We have high expectations for all students 
academically and challenge every student’s learning. A rigorous academic program is embedded in 
student-centered learning opportunities that engage students in meaningful ways. Our school-wide 
Character Education program helps foster our positive school climate along with our Super Coyote 
positive behavior program. We encourage all students to develop mutual respect and tolerance 
for others, learn cooperation to achieve goals, and gain a love for learning that will last a lifetime. 
 
Principal, Sarah O'Brien, leads a dedicated staff of twenty classroom teachers, one SAI teacher, two 
SDC teacher, two school counselors, one school psychologists, two speech and language 
pathologists, one occupational therapist, and forty-four classified employees. The classified staff 
includes: a school secretary, an office clerk, two custodians, a computer tech, a library tech, a 
health assistant, kindergarten 
language para 
educators, noon duty aides, and crossing guards. As a staff, we are committed to providing our 
students a nurturing environment that fosters academic, social, emotional, and physical 
development of each individual child. 
 
 

instructional aides, paraprofessionals, world 

----

---- 

Eureka Union Elementary School 

District 

5455 Eureka Road 

Granite Bay 

(916) 791-4939 

www.eurekausd.org 

 

District Governing Board 

Andrew Sheehy - Board President 

Renee Nash - Board Clerk 

Jeffrey Conklin 

Ryan Jones 

Melissa MacDonald, Ph.D. 

 

District Administration 

Tom Janis 

Superintendent 

Melody Glaspey 

Chief Business Officer 

Kelli Hanson, Ed.D. 

Director of Human Resources 

Kristi Marinus 

Director of Student Services 

Ginna Guiang-Myers, Ph.D. 

Director of Curriculum, Instruction, 

Professional Development and 

Student Assessment 

 

2017-18 School Accountability Report Card for Oakhills Elementary School 

Page 1 of 8