We do all we can to prepare students to meet life’s opportunities and challenges with confidence and determination. 
 
Motto: A place where students find success and learn to love school again. 
 
Students do not fail into Pollard they mature into Pollard. 
 
Mission 
 
We seek to support students who work, who are behind in credits, who have challenging family situations, who have made mistakes, 
and who have made a real commitment to graduate from high school. 
 
The programs of the school are designed: 

 

• 
• 

• 

To empower students to pursue, achieve, and accept success 
To develop a partnership with parents and community that encourages their involvement in the students’ education, 
future endeavors and vocations 
To ensure an environment where students feel safe in order to succeed 

All participants in the 2016 WASC visit worked on revising and fine-tuning the school Vision and Mission statements. 
 
Community 
 
Lee V. Pollard High School is a Model Continuation School for the state of California within the Corona-Norco Unified School District 
in Corona, California, and is located the south-central portion of the city. Students are from Corona, Norco, Eastvale, a segment of 
Mira Loma and two un-incorporated areas called El Cerrito and Home Gardens. The school services students from the five 
comprehensive high schools of our district. The primary purpose of Lee V. Pollard High School is two-fold: (1) to provide an opportunity 
for credit recovery and (2) to assist students within the district who have exceptional educational needs reach their goal of graduating 
from high school and prepare students for their future. 
 
Enrollment 
 
Lee V. Pollard’s over 750 students may choose from three programs of study, Regular Day Program, Academic Learning Center, and 
Independent Study. In addition to our Special Education Resource level students, we have added to our options a Special Education 
Adult Community Transition (ACT) class for 18-22 year old students, which is in its second year. In the Regular Day Program and 
Independent Study student, ethnicities are (approximately) listed as: 76.8% Hispanic or Latino, 15.6% Caucasian, 3.5% African 
American, and 4.1% other. There approximately 41 % are females and 59% are males in our programs. Sixty nine percent of our student 
population qualify for free or reduced price lunch. 
 
Lee V. Pollard High School staff is also diverse and reflects the three largest ethnicities of students on the campus, however not in the 
same proportion. Pollard seeks to employ faculty and staff who best qualify for the positions available without regard for ethnicity. 
There are total of 60 persons on staff. If listed by ethnicity for our instructural staff the percentages are approximately as follows: 
Caucasian 77%, Hispanic 23%. The total number of credentialed instructional personnel at Lee V. Pollard High School is 35 along with 
2 Administrators, 1 Student Advisor and 2 Academic Counselors who are also credentialed. Lee V. Pollard has 100% highly credentialed 
teachers on site. Approximately 67% hold advanced degrees. 
 

2017-18 School Accountability Report Card for Lee V. Pollard High School 

Page 2 of 18 

 

Each year, approximately 400 students have had some type of transfer to, or from, this school for a variety of reasons. This high 
turnover rate affects assessment results and student achievement. Students may choose from three programs of study currently being 
offered. The objective is to place our students in an educational setting that best suits the needs of each individual. Each program has 
been designed to eliminate barriers to success. For many students the possibility of being successful in a four-year comprehensive 
high school environment is not realistic. Some students cannot focus in a classroom of 42+ students, some work 40 hours a week, and 
some have one or more children to care for. It is mandated by law that the district meet the needs of these students and Lee Pollard 
High School does just that by accepting and being responsible for the education of these exceptional students. 
 
Enrollment in most programs is determined through an application; criteria include attendance, discipline and lack of credits for 
graduation. The administrative staff review the applications, a placement is recommended based on available student data, student 
needs, parental concerns, and an appropriate program choice is made. Because student progress is closely monitored, a change in 
programs sometimes occurs after a student has begun at Lee Pollard High School to ensure his/her educational success. Once eligibility 
is determine, an orientation meeting with student, parent and administration is held to provide additional information and review 
academic expectations for student success. The three different programs serving student needs are: 

• Regular Program: This program is designed to accommodate students who do best in high school classes with teacher-
directed, standards-based instruction. Each day, a student receives 270 minutes of instruction, and attends classes on a 
rotating block schedule, which provides an excellent educational environment. A student is enrolled in a minimum of six 
classes plus an academic enrichment homeroom, all of which meet state standards. Success in completing credit 
deficiencies is dependent on student willingness to complete take-home and/or individualized instruction classes, as well 
as enrollment in Work Experience, Riverside Community College or the county’s Regional Occupational Program. 

• Academic Learning Center (ALC): This program allows a student to work at his/her own pace to complete classes by fulfilling 
course requirements set forth in portfolios. Two fully credentialed teachers are available to answer questions and assist 
students toward their goals of high school graduation. Students attend 180 minutes in their daily ALC classes, as well as a 
completing 20 hours of homework each week. This rigorous course of study, with portfolios that strictly address state 
standards, provides students the opportunity to return to their home school, matriculate into our regular program or 
graduate from Lee Pollard High School. This shorter day schedule is beneficial to many students who do not function well 
in a traditional classroom setting, have jobs or have personal and/or family concerns. Most students in this personally 
demanding program are successful, and those who are not are given a chance to succeed in another program on campus. 
Independent Study (IS): This program is offered to students encountering extenuating circumstances, which inhibit their 
ability to attend school daily. These students meet weekly with fully credentialed teachers who design a standards-based, 
individualized workload within the parameters of district graduation requirements. The intent of IS is not to service 
students over an extended period, but rather to provide a means for students to continue their education when personal 
difficulties arise. At the end of a school semester, students who have resolved their circumstances return to their home 
schools or are referred to another LPHS program. 

• 

Adult Community Transition class (ACT): is a community-based, functional instructional program that leads students from high school 
to post-secondary education, vocational training, integrated employment, adult services, independent living and community 
participation. ACT is designed for the student who has graduated from high school with a Certificate of Completion after participating 
in a functional academic and vocational program. ACT Programs provides students with a natural transition to adult life. The goal of 
the CNUSD ACT Program is to enable the student identified as an individual with exceptional needs to improve his/her ability in all 
areas so that he/she may function as independently as possible at home, in school, in the community, and in the vocational setting. 
 
In addition to the programs directly associated with the school, other programs are offered to assist students in meeting graduation 
requirements and future vocational plans. The County of Riverside offers the site-based Career Technological Education/Regional 
Occupational Programs, off-campus CTE/ROP, Work Experience and Night School classes, as well as Riverside Community College co-
enrollment which also assist the students in meeting their goal of graduation. 
 
Adult Education ensures students an opportunity for added educational possibilities such as a GED, Night School Classes, or an Adult 
Education High School diploma. Many concurrent Night School classes are offered to Pollard students through Night School to make 
up course deficiencies before their graduation date. Students must be a least 18 years of age and have completed their senior year 
before seeking regular Adult Education. The California High School Proficiency Exam is offered through the state twice a year to 
students wishing to leave high school with an equivalency certificate. 
 

2017-18 School Accountability Report Card for Lee V. Pollard High School 

Page 3 of 18 

 

In the regular program, students are enrolled in core curriculum classes for 11th and 12th grade with California State Standards 
embedded in the textbooks, pacing guides and teacher lesson plans. The standards are posted and are an integral part of lessons 
taught in core subjects, as well as integrated throughout the curriculum. Students use Standard Based Curriculum Guides (Portfolios) 
to access knowledge and complete subjects not mastered previously in freshman, sophomore, or junior classes. All core curriculum 
Credit Recovery Portfolios (CRP) have been realigned to the California State Standards and new CRPs are being written to incorporate 
Common Core standards and performance tasks. Teachers follow core curricular pacing guides developed by representative faculty 
from district schools. Some of our teachers have worked on their development. The pacing guides in turn govern the curriculum, which 
prepares students for the district and state assessments. Results of the End of Course Exams are used to evaluate and modify teaching 
methods. These results also define the modifications teachers use in teaching a course the second time. There are varieties of electives 
offered, as evidenced by the Course Offerings List. These electives include the following classes: Regular Program classes, CTE/ROP, 
Credit Recovery Portfolios, Work Experience, School Service, and Riverside Community College Dual-enrollment. 
 
The Academic Learning Center Program, Extended Day and Independent Study Program are independent learning programs where 
teachers assist students in mastering Standards Based Core Curriculum with CRPs. The California State Standards are embedded in all 
core curricular assignments. Independent Studies students, throughout high school, have California State Standards as the basic 
curriculum for all core subject areas. Portfolios from Language Arts, Algebra, Social Science and Science are included in the resource 
file for perusal.