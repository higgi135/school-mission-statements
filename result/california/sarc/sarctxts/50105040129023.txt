Come Back Kids offers students 18 and older a rigorous high school academic program that is flexible, customized, and focused on 
areas of need to help you successfully earn a diploma. Following a community college model, students are enrolled in classes according 
to identified individual areas of need. Choices of courses are available online and all courses offer support from teachers. Come Back 
Kids helps to reduce the drop-out rate, assists every student with an Individualized Learning Plan and Career Plan, culminating in 
achievement of a high school diploma. We envision a safe environment where communication and success is fostered and 
accomplishments are celebrated.