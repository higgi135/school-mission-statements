Bagby Elementary School is an award winning school that values academic excellence and enjoys parental and community support. 
Bagby School serves approximately 511 students in grades TK - 5. Our safe, warm learning environment is evident as one enters the 
beautifully landscaped campus. We enjoy being the home for numerous special programs, which are an integral part of our campus. 
We embrace the whole child by honoring diversity, instilling not only joy of learning, but also allowing the exploration of talents 
through art and music. Our students are encouraged to be Upstanders and are given opportunities to be responsible, respectful and 
safe citizens. 21st century skills drive our instructional practices to provide opportunities to grow and become critical thinkers, 
collaborative workers and creative problem solvers. Bagby School fosters a growth mindset to teach students to believe that mistakes 
are an opportunity to learn. It is our goal to prepare students for their academic future, and to be cooperative and caring members 
of society.