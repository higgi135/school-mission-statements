The School Accountability Report Card was established by Proposition 98; an initiative passed by California voters. As you read this 
Report Card, you will gain a better understanding of Williams as a school with a record for improvement, a faculty that is professionally 
skilled and personally committed to meeting the learning needs of students, and a student body which is enthusiastic and motivated 
to perform well. 
 
Williams Elementary School is a neighborhood school, located at 1201 Williams Street in East Bakersfield. Williams has a culturally 
diverse student population of students in Transitional Kindergarten through 5th grade. Our school plan is developed and implemented 
to reach academic goals. Our school wide goals are to Increase Academic Achievement for all students, close the Achievement Gap 
between sub-groups, and Ensure a Safe and Positive Learning Environment including strong Parental Involvement. As a school, we 
strive to communicate the importance of attendance to each of our families. As a staff, we agree that effective professional 
collaboration and on-going adult learning are keys to reaching these goals. 
 
Williams Elementary School’s Mission is to ensure all students achieve at their highest academic level and develop the social skills 
necessary to be productive citizens.