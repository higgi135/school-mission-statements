PRINCIPAL’S MESSAGE 
I would like to welcome you to Hesperia High’s Annual School Accountability Report Card(SARC). In accordance with Proposition 98, 
every school in California is required to issue an annual SARC that fulfills state and federal disclosure requirements. Parents will find 
valuable information about our academic achievement, professional staff, curricular programs, instructional materials, safety 
procedures, classroom environment, and condition of facilities. 
 
We believe that Hesperia High School is the best high school in the high desert! We would like you to know that we are committed 
to providing the support needed to ensure that every student has every opportunity for success, both in the classroom and beyond. 
As a school, we are committed to providing a 21st century education, with a focus on creativity, critical thinking, communication, and 
collaboration. We offer rigorous, high quality curriculum and instruction, relevant preparation for college, careers and successful 
futures, and relationship and leadership building opportunities through athletics and activities. 
 
In our partnership with families, we ask that you stay involved in your student's high school experience so we can work as a team 
toward individual student achievement. It is our goal that every student has a positive and successful experience at Hesperia High 
School. Please let us know how we can best serve our students and families. We welcome any suggestions or questions you may have 
about the information contained in this report or about our school. 
 
Mission: 
To best meet the needs of students in our community, Hesperia High School offers authentic learning experiences that provide 
opportunities for students to grow both academically and socially in a globally conscious manner in response to the evolving nature 
of the 2st century. 
 
Vision: 
Hesperia High School will provide all students powerful learning experiences in an open, safe, and supportive environment in 
preparation for college and careers. 
 
Hesperia High School Learner Outcomes: 
 
Striving for excellence through: critical thinking, creativity, collaboration, communication, and character. 
 
 
SCHOOL PROFILE 
The city of Hesperia is a suburban community located 35 miles north of San Bernardino in an area known as the High Desert. Hesperia 
is a commuter city of 92,000 residents with a median income of $48,000 and a median housing price of $183,000. As the largest 
employer in the High Desert, Hesperia Unified School District serves over 22,000 students in 25 schools from Pre-K to 12. Hesperia 
High School serves approximately 2000 students, grades 9-12, as the original of three comprehensive high schools in Hesperia. The 
school demographics are similar to the community, with 81% socio-economically disadvantaged as measured by free and reduced 
lunch recipients. HHS is a Title I school, has 19% English Learners, 70% Hispanic, 19% white, 8% African American, and 1% Asian. 
 
Hesperia High School prides itself in its extensive Advanced Placement offerings and flourishing AVID program in an effort to prepare 
students for college. There are over 200 AP students and over 200 students enrolled in the AVID elective, with some overlap between 
the two. However, the data on college going rates and parent education level indicated a need for an additional emphasis on making 
college a reality for HHS students. With this in mind, HHS has created a one of a kind Early College Academy. The ECA was developed 
to give students who may not have otherwise been college bound a head start on a college degree. Hesperia High is also home to 
DEMA, the Design, Engineering, and Manufacturing Academy. This academy provides college and career preparation through applied 
learning in the visual and vocational arts. 

2017-18 School Accountability Report Card for Hesperia High School 

Page 2 of 18 

 

 
HHS also has articulated agreements with the local community college for several Career and Technical Education courses and serves 
as a satellite campus for many college classes offered throughout the day and evening. In addition to our emphasis on college 
readiness, we have a strong focus on providing career opportunities. Through ROP and a career pathway partnership grant, we offer 
engineering, automotive, health, and construction programs. Our extensive arts programs are award winning both locally and 
nationally, with courses offered in art, sculpture, photography, orchestra, drama, band, and choir. Hesperia High School offers a wide 
variety of clubs and sports to meet the demands of our very diverse community. HHS boasts over 45 active clubs—providing many 
opportunities for students to serve the community. We are committed to providing the depth and breadth of opportunities necessary 
in preparing our students for their futures and creating an engaging learning environment.