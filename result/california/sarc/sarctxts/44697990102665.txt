Welcome to the new Radcliff Elementary School. Since opening in 1920, Radcliff has seen many changes in both the community and 
in education. Re-establishing an elementary school in the downtown Watsonville area was a dream of both the Pajaro Valley Unified 
School District (PVUSD) and the City of Watsonville. Major renovation has been completed on the original building. The school opens 
onto an inner courtyard around which are two Kindergarten classrooms and several individual offices. The majority of the classrooms 
were built in a two-story building housing 22 classrooms and a large multipurpose room, complete with a basketball court and stage. 
 
Our school's greatest assets are the students, the new and energetic staff, and the community we serve. Radcliff has approximately 
550 students in grades K-5: over 90 percent of whom are English Learners. Almost all students qualify for free or reduced-price 
lunches. We invite parents, community members, and other interested parties to visit our school and see why Radcliff is fondly referred 
to as the "heart of the community." 
 
One of our challenges is students with many academic and emotional needs, and little literacy support from home. English is not the 
primary language for most of our families.