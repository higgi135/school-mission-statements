OUR MISSION 
Godfrey G. Berry Elementary is an exceptional learning community where we seek to develop the whole child. We are building the 
foundation of this community through a focus on the advancement of reading proficiency of our students in all subject areas. Our goal 
is that by third grade all students are reading at grade level so that they can achieve their goals and have the skills they need to succeed 
in an ever changing world. We believe that continuous improvement and self-reflection are critical elements needed to succeed as we 
strive for excellence both academically and socially. Our mission is to empower all students to apply their reading skills and knowledge 
to lead productive lives and to become contributing members of the global community. 
 
Our students are respectful, responsible, successful, compassionate, and... THEY BELIEVE IN THEMSELVES! 
 
SCHOOL OVERVIEW 
Godfrey G. Berry Elementary is one of 11 schools in the South Bay Union School District. Berry currently serves approximately 471 
students in kindergarten through sixth grade. It is located on 10 acres of land and has 34,389 sq. ft. of permanent and 7,000 sq. ft. of 
temporary classrooms. 
 
Student support services include, but are not limited to: speech therapist; nurse; resource specialist; psychologist; library; Instructional 
Resource Media Technician and STEM/Visual and Performing Arts program where students engage in weekly lessons in the arts and 
STEM. Additionally, Berry has implemented the use of 1 to 1 Chromebook devices in third grade through sixth grade, and twelve 
Chromebooks per class in transitional kinder through second grade. Finally, every classroom is fitted with a Promethean interactive 
whiteboard system. Promethean systems are a world-class tool for learning. It involves and motivates the whole class engaging all 
students allowing them the opportunity to learn and grow as individuals.