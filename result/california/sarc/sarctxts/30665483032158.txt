Fountain Valley High School is part of the Huntington Beach Union High School District. HBUHSD has over 15,000 students who attend 
six comprehensive high schools and two alternative educational sites. Fountain Valley High School has served students in the city of 
Fountain Valley since 1966. The school has become a beacon of excellence within the community. Fountain Valley High School was 
recognized as a California Distinguished School in 2003 and 2007. 
 
The strength of the FVHS program is the diverse curriculum offered to the students by a committed and caring staff. The curriculum 
at FVHS offers challenging advanced placement and honors courses representing the most rigorous curriculum; diverse elective 
offerings including vocal and instrumental music programs known throughout the nation for their excellence; vocational and technical 
opportunities for students to explore career options; and a comprehensive special education program that meets the needs of 
students with various disabilities. 
 
Fountain Valley High School is committed to maximizing the learning, growth and development of every student through the 
mentoring efforts of a dedicated and caring staff in partnership with parents and community. Our students will become critical 
thinkers, effective communicators, self-directed learners, and responsible members of society.