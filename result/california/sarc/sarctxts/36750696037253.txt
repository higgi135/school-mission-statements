We reach, teach, and positively impact the lives of all students, preparing them for tomorrow. We 
expand their intellectual and physical abilities, develop high levels of proficiency with a diverse and 
rigorous curriculum that address the needs of the whole individual. 
 
Scholars at Pioneer are expected to demonstrate Pioneer PRIDE: Pioneer, Respect, Effort, Integrity, 
Demonstrate safety, Effort. 
 
Pioneer Junior High embraces "College and Career Readiness" by designating every Wednesday as 
College Day. Teachers, students and staff school-wide show their support by wearing college 
apparel and each classroom has adopted their own college to showcase. 
 

 

 

-------- 

Upland Unified School District 

390 North Euclid Ave. 

Upland, CA 91786 

(909) 985-1864 

www.uplandusd.org 

 

District Governing Board 

Jack Young, President 

Linda Angona, Vice-President 

Wes Fifield, Clerk 

Robert Bennett, Member 

Mary Locke, Member 

 

District Administration 

Nancy Kelly, Ed.D. 
Superintendent 

Shinay Bowman 

Assistant Superintendent, 

Elementary Education 

Scott Sypkens 

Assistant Superintendent, 

Secondary Education 

Harold Sullins 

Assistant Superintendent, Business 

Services 

 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for Pioneer Junior High School 

Page 1 of 8