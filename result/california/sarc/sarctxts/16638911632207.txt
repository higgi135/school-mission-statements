Our district’s mission is "We are relentless is creating an environment for all to improve mind, body and character." The vision of 
Corcoran Unified School District is to become a “Destination School District” where "people are drawn to Corcoran due to the quality, 
reputation and accomplishments of our schools." For our high school to achieve this vision, we have made our mission “ to provide 
rigorous academics with real-world skills in order to ready our students for both college and careers." The vision of Corcoran High 
School is "To partner with our students to own their learning, pursue their ambitions and achieve their dreams." Corcoran High School 
will provide rigorous literacy and numeracy with quality research-based strategies so students are effective critical thinkers, 
collaborators, communicators, and leaders who are prepared and productive members of society. 
 
A staff of 46 credentialed teachers offers an array of courses in core curriculum supplemented by Advanced Placement and Career 
Technical classes. 
 
Corcoran High School currently has in place six Student Learning Outcomes (SLOs), which are as follows: 1) As a Corcoran High School 
Panther who is an effective COLLABORATOR, I will work with my team as a leader and a listener. I can successfully fill the following 
collaborative roes: Informer, Discoverer, Interpreter, Analyzer, Problem Solver, Regulator, Reflector, Constructor of Arguments, and 
Creator. 2) As a Corcoran High School panther who is an effective CRITICAL THINKER, I will understand, pose and solve complex 
problems through depth of understanding, discovery, interpretation and analysis. I will demonstrate the ability to construct logical 
arguments all while self-regulating and reflecting. 3) As a Corcoran High School panther who is an effective COMMUNICATOR, I will 
engage in academic discourses using formal language in order to appropriately communicate in diverse environments. I will use 21st 
century tools and know when it is appropriate to listen. I will be confident in sharing thoughts, questions, ideas and solutions. 4) As a 
Corcoran High School panther who is an effective CREATOR, I will generate new ideas that I will have the courage to explore and 
develop, and I will be open to constructive criticism. I will refine my ideas that will innovate high quality products. 5) As a Corcoran 
High School panther who is an effective LEADER I will promote and abide by the five to thrive: Safety, Acceptance, Recognition, Justice 
and Fun. 6) As a Corcoran High School panther who is planning for success, I will be able to articulate my COLLEGE AND CAREER GOALS 
by building my path to college and/or my path to my career interest. 
 
Community: 
 
The Corcoran Unified School District has served the residents of this small Kings County community for over 100 years. This predates 
the incorporation of Corcoran, which took place in August of 1914. The area’s rich agricultural condition led to the establishment of 
the J.G. Boswell Corporation in 1925 and has remained the most prominent business in Corcoran. California State Prisons have opened 
two large facilities in Corcoran in 1989 and 1997. Corcoran is the epitome of what a “small town” is, with the community, local 
businesses, and schools often working together on many of the major projects in town. Despite the addition of other businesses in 
the community, Corcoran is still strongly influenced by its agricultural roots. The schools that are housed in Corcoran are each feeder 
schools to the next grades. There is one high school and one alternative education school. The population of Corcoran High School is 
primarily made up of 88% Hispanic, 6% Caucasian, 3% African-American and less than one percent Asian or Indian. 
 
The City of Corcoran partnered with Corcoran Joint Unified School District to open the Technology Learning Center (TLC) on the west 
side of the CHS campus. The TLC provides classrooms and conference areas for the school district and other entities, while also 
providing access to community college courses for CHS students and community members. Opening the TLC facility has allowed the 
community of Corcoran to expand their partnerships with two local community colleges, College of the Sequoias and West Hills 
Community College in Lemoore. Both community colleges offer courses that CHS students can take concurrently during the school 
day. Corcoran High School was granted a six year WASC accreditation in 2016-17 and will have a one day mid-cycle visit in 2020. CHS 
enjoys a great deal of community support through boosters clubs, advisory committees, and parental involvement. Our community 
has taken great pride in assisting us in expanding the “pursuit of excellence” through parent involvement in these clubs and committee, 
as well as, financial support through the Communities and Schools Together organization. 
 

2017-18 School Accountability Report Card for Corcoran High School 

Page 2 of 15 

 

About This School 
 
Corcoran High School, located in the heart of Central California’s San Joaquin Valley, has been the focal point for academic, social, and 
recreational activities for the City of Corcoran for over 100 years. The Class of 2017 will mark the 103rd graduating class of Corcoran 
High School (CHS). Because Corcoran is a small rural community, extra curricular activities and clubs provide a large share of the 
recreation opportunities for students. CHS is a comprehensive high school serving approximately 860 students. Feeder schools include 
three elementary schools and one middle school. The district also supports a continuation school, an independent study program, a 
charter school, a community day school and an adult education program, all located on the Kings Lake Education Center. 
 
Corcoran High School is a comprehensive four-year public high school which will have 880 students enrolled at the start of the 2018-
19 school year in grades 9-12. The school opened in the fall of 1920. Corcoran High School is accredited by the Western Association of 
Schools and Colleges (WASC). 
 
Curriculum 
 
The academic program is organized by eight periods Monday-Friday. Thirty-five credits per semester makes up an average course load; 
however, students have the option of taking an additional after school credit-recovery courses for a maximum course load of fifty 
credits per semester. 
 
AP courses are offered in Literature, Language, and, Spanish. Students are placed into AP courses by teacher recommendation and by 
having a signed contract on file with the counseling office. The number of AP courses is determined by student interest. 
 
Honors courses are offered for English 9, 10 and 11. 
 
Career Technical Education Courses include Health Education, Medical Terminology, Educating for Careers, Video Gaming, Web 
Design, Digital Design, as well as our agriculture and business classes. 
 
Corcoran High School partners with two local community colleges, College of the Sequoias and West Hills College, to provide students 
with the opportunity to take college courses while concurrently enrolled in high school. Students can access classes after school, online 
and in many cases, as one of their seven periods during their regular school day. 
 
Grading and Ranking 
 
A - Excellent = 90-100 4.0 
 
B - Above = 80-89 3.0 
 
C - Average = 70-79 2.0 
 
D - Below Average = 60-69 1.0 
 
F - Failure = 59 or below 0.0 
 
Rankings are located on all student transcripts and show where a student is ranked in comparison to his/her same grade classmates. 
There is also a 10-12 class rank as well as weighted and non-weighted GPA’s provided. The weighted grades include the calculation of 
the extra point for Honors and Advanced Placement courses. GPA calculations are computed using the above point system. Students 
are required to earn a minimum of 260 credits in order to graduate. 
 
 

 

2017-18 School Accountability Report Card for Corcoran High School 

Page 3 of 15 

 

Colleges Attended by Corcoran High School graduates over the last four years: 

Fresno City College 
Fresno Pacific University 
Fresno State University 

ITT Technical Institute 
Long Beach University 
Los Angeles, University of California 

• Bakersfield State University 
• Berkeley, University of California 
• Brandman University 
• College of the Sequoias 
• Davis, University of California 
• 
• 
• 
• Humboldt State University 
• 
• 
• 
• Merced, University of California 
• Monterey Bay University 
• Porterville City College 
• Reedley City College 
• Riverside, University of California 
• 
• 
• 
• 
• West Hills College 

San Joaquin Valley College 
San Diego State University 
San Diego, University of California 
Santa Cruz, University of California 

School Climate: 
 
Corcoran High School has a School Safety Plan and Crisis Intervention Plan on file and available for parents as well as the general public. 
The school plans are reviewed and updated on an annual basis. The staff has developed the plan with input from parents and 
community members to work to ensure a safe and non-violent environment. The Safe School Plan also includes information on 
communication with outside organizations, police, and fire protection. Meetings are held regularly with the Corcoran Police 
Department, Probation Office and the Sheriff’s Office to make sure that the lines of communication are open and all involved know 
their role. 
 
Additionally, practice drills are held each quarter to make sure that students and staff understand what to do and where to go in the 
event of different types of emergency situations. Using the bell system, the school has created different tone qualities to alert staff 
and students of the type of emergency situation that may be underway. Corcoran High School follows the district-adopted Crisis 
Management Plan for Schools © 2003, which covers the type of emergencies most frequently seen in schools. Teachers receive 
training about the safety plan at the beginning of each school year, and training updates are conducted as needed throughout the 
year. Regular fire/emergency drills are conducted to prepare students and teachers to respond to emergency situations. 
 
Suspensions and Expulsions 
 
An important element of the philosophy of education at Corcoran High School is that all students should be provided with every 
opportunity to experience a positive learning environment. Staff and students share in the creation, maintenance and refinement of 
this environment. Corcoran High School has adopted elements of the Restorative Justice program and is now implementing Positive 
Behavior Intervention and Supports (PBIS). The standard procedure for processing all violations will be one of intervention, conflict 
resolution, restorative justice, consistency, expedient consequences and proactive parent involvement. 
 
The implementation of PBIS has led to a substantial decrease in the number of suspensions beginning in the Spring Semester of 2013. 
Staff and students are being educated on the elements of PBIS and how to meet the new motto of the school, “It takes five to THRIVE” 
at CHS. For the 2016-17 and 2017-18 school years, Corcoran High School was awarded a Model Gold Level Banner school with the 
focus being on individual intervention. 
 
 

2017-18 School Accountability Report Card for Corcoran High School 

Page 4 of 15