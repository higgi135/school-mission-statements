Olivet Elementary School is nestled on the edge of Laguna de Santa Rosa on the western boundary of the City of Santa Rosa. It is 
known for it's quality education offered by experienced teachers and support staff. Olivet is a TK-6 school with approximately 320 
students and 13 teachers. Olivet has a highly qualified, professional staff, skilled at working with all students in a caring manner. The 
school has a family feeling and thrives on being a "Small School with a Big Heart". Olivet is fortunate to have a very active parent group 
called "Olivet Families" which sponsors over 15 fun school events throughout the school year. 
 
We also have Program Assistants (PAs) who are used in all classes on a daily basis. These dedicated individuals provide small group 
and individual instruction to students who may need additional help in math or language arts; they also serve to relieve the teacher 
by monitoring students doing independent work, so that the teacher may provide intensive instruction as needed. PAs also administer 
the progress monitoring assessments that provide valuable academic data to the teachers. What is special about these adults, is that 
they are also supervisors during recess. They get a chance to know the students in and out of the classroom setting allowing trusting 
relationships to develop. 
 
Technology is prevalent in all classrooms: students in grades 1-3 have iPad mini labs in their classrooms, and grades 4-6 each classroom 
have 1:1 Chromebooks to use while collaborating, researching, writing, typing programs, as well as ELA & Math tutorials, and for 
completion of on-line assessments. 
 
Olivet School is a well-balanced, elementary school where students enjoy multiple forms of learning activities and engage in 21st 
Century Learning experiences: 
 

• Weekly assemblies begin each week with the school pledge, the Pledge of Allegiance, and a new "Tool of the Week" from 
the ToolBox Project. This effort fosters a keen focus and sense of school community for students, staff, and families that stay 
to listen to the assembly. 
Field trips to museums, the Luther Burbank Center of the Arts, California Missions, Angel Island, the Exploratorium, Marin 
Headlands, local farms, pumpkin patches, fire stations, and more. 

• 

• Activities that include parades, Stone Age Days, photography, campus beautification days, carnivals, Hawaiian luaus, talent 

shows, skate nights, star-gazing, BBQ's, harvest and book fairs, and more; 

• Museum related exhibits and activities are held on campus from the Charles Schulz Museum, the Lawrence Hall of Science 

and special guest musical performers engage students in sing-a-long concerts. All ignite students' enthusiasm for school; 

• Art projects that include Asian calligraphy and painting, hand-painted crafts, window paintings, mosaics, masks, and more; 
• After school tutorial, art, music, running, and gardening programs are offered on campus; 
• 

Students provide service activities such as reading to special need preschoolers who meet on our campus and helping in our 
local community such as with the recycling program that helps fund our sixth grade science camp. 

 
The district and school mission statement is clearly reflected at Olivet: As a community, we engage in authentic, dynamic, and relevant 
learning that develops each student's academic, emotional, and social growth. 
 
Olivet's Vision: 
 
We provide 21st Century teaching and learning for ALL students. Our community of students, staff, and families works collaboratively 
as a team. Our families and community members are valued, respected, and included. We demonstrate our commitment to the whole 
child by nurturing their intellectual growth and social-emotional well-being. We appreciate one another and are recognized for our 
shared successes. 
 

2017-18 School Accountability Report Card for Olivet Elementary Charter School 

Page 2 of 11