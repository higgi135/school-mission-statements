The Mission of Pivot Charter Schools is to instruct students in grades TK-12 through a rigorous online educational platform supported 
by site based class offerings and individualized attention. The unique educational program coupled with flexible scheduling and a 
caring environment provides students the skills, confidence and motivation to lead a successful and productive life in the 21st century. 
 
Pivot Charter School offers a unique and customized tuition- free public education for students in grades TK-12. Our program allows 
students to access their courses from the comfort of their own home or from our safe and welcoming resource centers five days a 
week; Pivot is truly a Blended Learning Program. The 6th through 12th grade students at Pivot Charter Schools come from a wide 
range of backgrounds. They come for advanced placement courses, flexibility in scheduling, one-on-one attention, small group 
learning, to make up credits and to graduate early as well as to take college courses. The diversity of our student body is one of the 
things that makes the Pivot experience unique. Our TK-5 program offers hands on projects as well as tutoring and arts. The TK-5 
program utilizes the highly acclaimed, Compass Learning online curriculum with additional supplemental materials coupled with the 
support, guidance tutoring and site-based classes provided by a CA-credentialed teacher. In the Pivot elementary program, parents 
assume a key role in overseeing the education of their student. Using Pivot educational resources, and supported by CA-credentialed 
teachers, students in the elementary program will progress at a level that is commensurate with their abilities. 
 
Pivot Charter Schools recognize that students working online and in independent study must have a significant level of independence 
or support at home in order to be successful. And not all students have that self-motivation, support or independence. Therefore, 
Pivot has developed onsite programs to support those students in meeting their educational goals using the online curriculum. 
Students in grades 6-12 can participate in electives such as learning lab, music, journalism, science lab, Spanish, and PE at most sites. 
Every campus offers weekly Fun Fridays and field trips. 
 
PIVOT CHARTER SCHOOLS IS FOUNDED IN THE FOLLOWING CORE BELIEFS: 

• 
Successful schools are student centered, not adult centered. 
• When focus on changing students’ lives, one can’t go wrong. 
• A teacher’s role is to have frequent, supportive yet motivating communication with students. 
• 
• 
• 
• 
• 
• 

Students’ academic performance is greater when they have the influence of a positive adult in their lives. 
Schools must show how much they care about students. 
Educators should have warrior spirit, a servant’s heart, and a fun loving attitude in serving students. 
Education needs to provide more options, not less. 
The goal is that students learn; how we get there should be as unique as every student. 
Technology is our friend and should play a crucial role in educating students.