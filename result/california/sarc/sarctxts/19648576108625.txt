Vision: Palmdale School District and Mesquite Elementary School will be recognized regionally as the finest educational organization, 
offering innovative, academic choices and opportunities for success in the 21st century. 
 
Mission: Mesquite staff and students will celebrate the power of learning through academic and social excellence in school and life. 
 
Instructional Focus (Vision): All students will read and comprehend grade level text. 
 
Instructional Focus (Mission): Equip and empower all students with the skills they need to effectively read, analyze, evaluate, and 
utilize complex grade-level sources (close and analytical reading across all content areas). 
 
Mesquite Elementary School serves students in grades TK-5 and has a current enrollment of approximately 800 students. Mesquite is 
located in the high desert area of Palmdale, California. The school was established in 1989, built in 1992 and is situated on 10 acres of 
land on the east side of the Palmdale School District. Mesquite is a Title I school with approximately 90% of the students on free or 
reduced rate lunch. 
 
School Student Demographics: 
+ 617 (77%) = Latino/Hispanic 
+ 121 (15%) = African-American/Black 
+ 33 (4%) = Caucasian/White 
+ 14 (2%) = 2 or More Races/Other 
+ 11 (1.5%) = Asian 
+ 2 (.5%) = American Indian/Native Alaskan 
+ 243 (31%) = English Learners 
+ 24 (3%) = Foster Youth 
+ 2 (.5%) = Homeless 
+ 17 (2%) = GATE Students (36 students were just assessed for GATE.) 
+ 51 (6.5%) = RSP Students (9 Pending Initials) 
+ 28 (4%) = Speech Student (7 Pending Initials) 
+ 90% = Free & Reduced Price Meal Program 
 
School Staff Numbers: 
+ 2 Administrators 
+ 28 General Education Teachers 
+ 2 RSP Teachers 
+ 1 LST 
+ 1 PE Teacher 
+ 1 Psychologist (3 Days - Share with Millen Middle School) 
+ 1 Speech & Language Pathologist (2 Days - Share with Cactus Middle School) 
+ 5 Instructional Assistants (General Education) 
+ 1 Instructional Assistant (Special Education) 
+ 1 Computer Technologist 
+ 1 Day Custodian 
+ 2 Night Custodians 
+ 1 Cafeteria Manager 
+ 5 Cafeteria Workers 
+ 9 Campus Supervisors (6 Permanent Campus Supervisors, 3 Substitute Campus Supervisors) 

2017-18 School Accountability Report Card for Mesquite Elementary School 

Page 2 of 11 

 

 
Many special programs serve the diverse population at Mesquite Elementary School. Mesquite operates under a School Based 
Coordinated Plan and the programs include Special Education, RSP, Title I, ELD, and Speech. 
 
Technology is a priority at Mesquite. A wide variety of technological resources are available to the Mesquite staff and students. Every 
classroom contains a Promethean interactive, high definition, big screen television. Every teacher has an All-in-One computer 
workstation and black toner laser printer, except grade level lead teachers have a laser color printer which serves as a hub for their 
grade level. Students in grade 2-5 have 1:1 Chromebook devices. TK-1 grade students have at least 6 iPads in addition to access to 
laptops and computer desktop workstations. There are two fully equipped computer labs which contain 40 All-in-One computer 
workstation with headphones. Our library contains a Promethean and 5 iPads for student use. The administration and front office 
staff are also fully equipped with technology. Additionally, we have robotic technology (Speros and Cubelets) to assist students in 
learning robotics and coding. 
 
Software programs include Accelerated Reader (AR), Imagine Learning, Discovery, Mystery Science, and Starfall. The AR program 
monitors reading progress. Teachers use a the AR program and other reading progress monitoring tools to set reading goals where 
students receive incentives for reaching their reading goals. Our library book collection is continually update to provide students with 
current and high-interest reading books. Additionally, students can register for eBooks which they can download and read on their 
technology devices. AVID (Advancement Via Individual Determination) is a school-wide program that is dedicated to closing the 
achievement gap by preparing all students for college and other post secondary opportunities. Our AVID program features an 
organization continuum which provides students with method of organization from TK-5. Our 4th and 5th grade students utilize the 
AVID online organizational tool. 
 
Mesquite School strives to provide a welcoming environment which encourages community and parental involvement in its many and 
varied programs and services. Parents are encouraged to volunteer in Mesquite classrooms and to provide assistance and additional 
monitoring on field trips. Parents are also encouraged to participate in Parenting Partners, parent workshops, family nights, parent 
meetings, and parent-teacher conferences. Mesquite School has established an English Language Advisory Committee (ELAC), African-
American Parent Advisory Council (AAPAC), a Gifted and Talented Education (GATE) Advisory Committee, and a School Site Council 
(SSC) that work to provide added support and direction to Mesquite students, parents, and staff. The SSC is actively involved in the 
decision making processes that affect the instructional and learning program for Mesquite. At Mesquite, Spanish translators are 
provided on site for parents who are need those services. Our parents and community members also volunteer to teach after school 
enrichment programs such as: Folklorico dance, choir, art, music, and sports. Mesquite also provides students with Extended Learning 
Opportunities (ELO) in math and English language arts to provide remediation and enrichment. Most ELO classes are held after school. 
Targeted student groups for ELO classes are at-risk, English Learners, foster youth and homeless, and socioeconomically disadvantaged 
student 
 
Mesquite is committed to the Cycle of Inquiry to meet the academic needs of our students. This cycle consists of the analysis of 
student achievement scores from annual standardized testing, district assessments, district-adopted curriculum assessments, grade 
level common formative assessments, and teacher classroom assessments of student achievement. Parent input and feedback is also 
utilized. Mesquite teachers meet frequently in Professional Learning Communities (PLCs) within and across grade levels to plan, 
discuss, coordinate instruction, as well as analyze student assessment data and work samples/artifacts. Professional learning 
opportunities are provided by district and site staff. This school year, all of our teachers will have been trained in Capturing Kids' 
Hearts which focuses on building and foster student-staff relationships. 
 
At Mesquite, students participate in semester awards assemblies that recognize their academic achievement, citizenship, perfect 
attendance, and other areas. Each month, a Kiwanis Student of the Month is recognized for his/her exemplary citizenship which is 
presented to the student in a ceremony at the district office. Additionally every month, a student is selected from each classroom as 
the classroom Student of the Month and is acknowledge with a lunch with administrators. Students earn Best Behavior Gold Cards 
and winners are selected weekly to earn a prize from administration. Once a month, a Gold Card student is selected from each 
classroom to participate in a Best Behavior breakfast with administration. 
 
 

2017-18 School Accountability Report Card for Mesquite Elementary School 

Page 3 of 11