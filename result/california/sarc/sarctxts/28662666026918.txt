Description: Pueblo Vista Magnet School is a Dual Immersion/ Environmental Studies school that provides children in grades TK-5 with 
a rigorous and engaging program integrating environmental sciences with all areas of the curriculum in Spanish and in English. 
Assemblies, field trips, and hands-on projects provide experiences that connect students to their community and build a solid academic 
foundation. The school garden, culinary kitchen, science lab, library and computer lab provide unique opportunities for student 
learning. 
 
The students at Pueblo Vista Magnet School are served by 18 credentialed classroom teachers, science teacher, intervention teacher 
and a reading intervention teacher, and a part-time support staff that includes Special Education teachers (resource, speech/language, 
school psychologist), physical education K-3, instrumental teacher (4th-5th), credentialed nurse and nurse assistant, two instructional 
assistants, and a bilingual community liaison. We take pride in the partnerships we have formed with the Boys and Girls Club, Napa 
Library, Napa Valley Museum, Napa Waste and Recycling, Connolly Ranch, and other community organizations. 
 
Mission Statement: With a passion for dual immersion and environmental stewardship, Pueblo Vista Magnet promotes high academic 
achievement through rich language instruction and an integration of environmental sciences with a rigorous core curriculum. Students 
will leave Pueblo Vista bilingual, bicultural and biliterate in both Spanish and English. Carefully designed inquiry-based units provide 
learning experiences that allow students to communicate, collaborate, and think critically about local and global issues. Teachers 
partner with parents and local community resources to connect learning to the natural world. As a result, Pueblo Vista students will 
develop an understanding of the interrelationships among science, the environment, languages, diverse cultures, and their world.