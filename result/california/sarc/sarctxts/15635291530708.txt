Bakersfield High School has accepted learning as the fundamental purpose of our school and 
therefore is willing to examine all practices in light of their impact on learning. We are committed 
to working together to achieve our collective purpose (using high-trust/performing learning teams), 
and will assess our effectiveness on the basis of results, rather than only intentions. Individuals, 
teams, site council, and district/school officials and leaders, will seek relevant data and information 
to use that toward ongoing and continued improvement. 
 
MISSION STATEMENT 
Bakersfield High School is committed to a quality educational program, responsive to the needs of 
its students within a safe, nurturing environment. The staff, students, and community assume 
responsibility for each student’s academic and personal success. Recognizing the worth and dignity 
of each student, Bakersfield High School prepares all students to achieve their fullest potential. Our 
goal is to produce students who make informed decisions, as they become responsible citizens and 
productive members of society. 
 
VISION STATEMENT 
Bakersfield High School, as a developing professional learning community, is committed to 
providing relevant, rigorous curriculum and assessment that enables students to attain their 
individual goals in a clean, safe and secure environment which promotes responsible behavior and 
pride in our school. 
 
Bakersfield High School is one of the largest (approximately 3,000 students) and the oldest 
comprehensive high school (opened in 1893) in the Southern San Joaquin Valley. Bakersfield High 
School is one of eighteen comprehensive high schools in the Kern High School District, which is the 
largest high school district in the state of California (37,000 + students). Bakersfield High School has 
an alumnus that extends worldwide and permeates all levels of society. Notable graduates include 
California Governor and US Supreme Court Chief Justice Earl Warren, Football Hall of Famer Frank 
Gifford, Olympic Gold Medalist Jake Varner and current US Congressman Kevin McCarthy. 
 
Bakersfield High School is located in the city of Bakersfield which is the county seat of Kern County, 
one of the largest (geographically) counties in the state of California. The county population is 
approaching 700,000 and its land area covers 8,141 square miles. Approximately 375,000 people 
live within the city limits of Bakersfield in a land area of 113 square miles. An additional 100,000+ 
people live within the greater metropolitan area. The economic base of Bakersfield and Kern 
County is oil production, agriculture, and warehousing. Bakersfield is located in the San Joaquin 
Valley, approximately 100 miles north of Los Angeles and 280 miles south of San Francisco. 
 
Bakersfield High School is one of the most ethnically and socio-economically diverse high schools 
in the state of California. BHS has graduates who attend some of the most prestigious universities 
in the nation as well as students who struggle to graduate from high school. The student population 
draws from upper middle class to inner-city lower socio-economic neighborhoods. The school is 
proud of its heritage and the contributions that its graduates have made to the city, state, and to 
the nation. BHS is committed to excellence for staff and students, and we are especially proud of 
its hard earned reputation as a comprehensive high school offering the highest possible quality 
academic, athletic, and activity programs (nationally ranked football and wrestling teams in 2011, 
CEO Public Service Academy, Fashion Design and Agriculture as county featured CTE programs, 
state ranked forensics program, ELA department partnership with the CSU system, regionally noted 
Advanced Placement/Honors program, model transition program for Special Education and Foster 
Youth, AVID and student support, music and theater...the list goes on and on). 

2017-18 School Accountability Report Card for Bakersfield High School 

Page 1 of 18 

 

Once A Driller, Always A Driller 
 
2018-2019 Leadership 
Dr. Bryon Schaefer, District Superintendent 
David Reese, Principal 
Cheyenne Bell, Assistant Principal of Instruction/Curriculum 
Sydney Peterson, Assistant Principal of Administrative Services 
Don Ellsworth, School Site Council Chair 
 
Bakersfield High Anti-Discrimination Policy 
Bakersfield High does not allow discrimination based on actual race, color, ancestry, national origin, ethnic group identification, age, 
religion, marital or parental status, physical or mental disability, sex, sexual orientation, gender, gender identity or expression, or genetic 
information; the perception of one or more of such characteristics; or association with a person or group with one or more of these actual 
or perceived characteristics.