Abraham Lincoln Elementary School has 42 classrooms, a library, a multipurpose room, and an administration office. The campus was 
built in 1968 and was modernized in 2012. Lincoln has an increased capacity to service students more effectively through focusing on 
K-5 instructional technology, digital learning environments, supplementary online academic intervention supports, career pathway 
education, and a focus on mastery of Common Core State Standards, in a facility with ample space for instruction.