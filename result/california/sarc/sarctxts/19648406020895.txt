Welcome to D.D. Johnston Elementary School, where we focus on Building Great Character One 
Pillar At A Time. Currently we have just completed the second year of state testing using the new 
Smarter Balanced Assessment on the California Common Core Standards. The state of California 
has yet to release guidance in expected growth and status expectations. Our academic program is 
called ExCEL, which stands for Excellence: A Commitment to Every Learner. Students are assessed 
on a consistent basis and parents are provided progress reports on a regular basis. Students in 
Kindergarten through 5th grade take a diagnostic assessment at least twice a year (Kindergarten) 
and three times (1st through 5th grade). 
 
D.D. Johnston Elementary supports our districtwide literacy initiative and teachers collaborate to 
provide our students the best instruction possible. Our School Site Council (SSC) provides resources 
to support this initiative. We also provide students the opportunity to many online resources at no 
cost to our families. Students have free access to an online academic support computer program 
(iREADY) in ELA and Math both at school and at home. Students are awarded points and can earn 
an award each month for attaining the most points. 
 
Our school's motto is Building Great Character One Pillar At A Time. We enrich our students with 
education in the Six Pillar of Character, which include Trustworthiness, Respect, Responsibility, 
Caring, Fairness, and Citizenship. Each month, we focus on a pillar and discuss it in assemblies, 
classrooms, and Student of the Month awards. In addition, we are a Playworks school. We have a 
full-time coach that teaches all of our students the Power of Play. 
 
Here at Johnston, our community is very important to us. We keep our parents informed of their 
child's progress through our schoolwide report cards. We include important information on our 
website and send flyers and calendars/events through our Thursday Folders. Every Friday, parents 
are welcomed to have lunch with their child during their regular lunch period. 
 

 

 

----

---- 

----

--- 

Norwalk-La Mirada Unified 

-

School District 

12820 Pioneer Blvd 
Norwalk, CA 90650 

(562) 868-0431 
www.nlmusd.org 

 

District Governing Board 

Ana Valencia Board President 

Jude Cazares Board Vice President 

Darryl R. Adams Member 

Jesse Urquidi Member 

Karen Morrison Member 

Chris Pflanzer Member 

Jorge Tirado Member 

 

District Administration 

Dr. Hasmik Danielian 

Superintendent 

Dr. Patricio Vargas 

Assistant Superintendent - 

Educational Services 

Estuardo Santillan 

Assistant Superintendent, Business 

Services 

John Lopez 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for D. D. Johnston Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

D. D. Johnston Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

17 

14 

19 

0 

0 

1 

0 

0 

0 

Norwalk-La Mirada Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

898 

2 

4 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.