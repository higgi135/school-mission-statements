Bayside Community Day School offers an alternative education program not intended to be juvenile 
court school, although students on probation may be assigned. The targeted population of Bayside 
Community Day School are students in grades 10 through 12 who are identified as at-risk due to 
attendance affecting their behavior, or academic standing for the purposes of accepting expelled 
students, or those in the suspension/expulsion progress. Bayside Community Day School is similar 
to the MVCLC - Charter School. However, in the organization of our alternative educational 
program, currently there are few expelled students enrolled in Bayside Community Day School and 
those are on a suspended expulsion and placed on contract at the time they enroll. 
 
Bayside Community Day School represents the belief that the addition of this alternative school 
within the Moreno Valley Unified School District, provides a unique and additional opportunity to 
address the educational needs and desires of the students in Moreno Valley. 
 
The fundamental goals of Bayside Community Day School are to provide at-risk students in grades 
10 through 12 with an opportunity to maintain a connection to the academic process, to make-up 
their credits, to increase their basic academic skills and to prepare themselves to return to their 
regular comprehensive high school program or to pursue a vocational career. 
 
Bayside Community Day School's curriculum is committed to articulate with the curriculum of the 
high schools in the Moreno Valley Unified School District. Students' skills are increased and 
reinforced. Students are provided with opportunities to learn and apply problem solving and critical 
thinking techniques. In addition, lessons are presented emphasizing various student-learning 
modalities to maximize the potential for success. The counselor meets with the student and parent 
and an Individual Graduation Plan is prepared for each student, to ensure students are enrolled in 
the appropriate classes to complete their high school graduation requirements. 
 

2017-18 School Accountability Report Card for Bayside Community Day School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Bayside Community Day School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

10 

10 

10 

0 

0 

0 

0 

0 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Bayside Community Day School 

16-17 

17-18 

18-19