Julian Charter School (JCS) is an independent study TK-12 charter school sponsored by Julian Union Elementary School District. The 
school was established in November 1999 to meet the needs of students who were underserved by traditional delivery systems of 
education or for families who had a strong desire to home school. Beginning in the 2018-2019 school year JCS split into 6 smaller 
charter schools to serve students more locally. JCS serves students in Orange, Imperial, Riverside, and San Diego counties with the 
majority of students in San Diego County. As of 2000, JCS is a non-profit corporation and, as such, receives direct funding from the 
state. Administrative offices are housed on the Julian Junior High School campus in the town of Julian in the mountains of northeast 
San Diego County. 
 
The school offers a variety of resources to meet the needs of independent study learners including: a resource center; home study 
education units (EUs) for vendor course instruction or educational materials; field trips; labs; program options (online learning, 
portfolio, home study, intensive intervention); and support programs such as Safety Net and virtual tutoring. The school is accredited 
through the Western Association of Schools and Colleges (WASC). Our most recent WASC accreditation in 2016 resulted in 6 years 
with a midterm report. 
 
The mission of JCS is to empower learners with educational choice. Our vision is to provide an exemplary personalized learning 
program in a supportive, resource-rich learning environment. We are dedicated to excellence and committed to nurturing passionate 
lifelong learners. Core values include: 

• Creativity and Innovation: Envision and explore rich teaching and learning opportunities. 
• Commitment: Educate students to their full potential and uphold the greater good of the school. 
• Choice: Empower individual paths and goals through personalized learning. 
• 
• 

Excellence: Foster a climate of high expectations, quality, and accountability. 
Integrity and Compassion: Model honesty, dignity, fairness, and responsibility while demonstrating respect and 
understanding. 

 

2017-18 School Accountability Report Card for Julian Charter School 

Page 2 of 11