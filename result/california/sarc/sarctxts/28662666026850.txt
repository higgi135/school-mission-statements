Description: The students at McPherson Elementary School are served by twenty-two credentialed classroom teachers and a support 
staff that includes an academic specialist, intervention teacher, resource specialists, science, physical education, technology, 
instrumental music (4-5) , a speech therapist, a part-time school psychologist, a part-time nurse, three instructional assistants, and a 
part-time librarian. The campus is a community orientated facility - used after hours for the after school program sponsored by the 
Boys and Girls Club which serves over two hundred students (this program is funded by Prop 49 monies), Parent University classes, 
and little league baseball. McPherson Elementary School also has a full time Family Resource Center with a staff that includes a 
Director, a Parent Engagement Coordinator and a Case Manager. The Family Resource Center is open daily (including summer and 
other school vacations) Monday-Friday, 8:00 am - 4:00 pm. 
 
Mission Statement: Learn from my past, work hard in my present to succeed in my future.