Palmdale Discovery Center is dedicated to providing each of our students with the finest possible education supported by highest 
quality staffing programs within a healthy and safe learning environment. 
 
The mission of the Palmdale Discovery Center is to provide each student with an appropriate and relevant academic experience in a 
safe learning environment which will provide each student educational benefit and prepare each student to be responsible and 
respectful members of our community.