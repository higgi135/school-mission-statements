Boron Junior Senior High School is the centerpiece of a rural mining community. The school is a traditional 7-12 public school. BJSHS 
is part of the Muroc Joint Unified School District. Boron includes an on-site Alternative Education Program that utilizes on-line courses 
to server our students for credit recovery and earning college credits in conjunction with Cerro Coso Junior college. The population 
falls within the lower to middle socioeconomic group. 
 
VISION 
Boron Jr./Sr. High School’s vision is to educate its students in a safe environment with a well-rounded education that all citizens should 
possess. This will enable our students to participate and benefit in a higher quality of life and life-long learning. 
 
MISSION 
Boron Junior Senior High School's mission is to educate its students in that common core of knowledge possessed by educated citizens, 
to teach appreciation of their integral role in their school, community and nation, to promote their realization in their inherent value 
as individuals. Also, it is to prepare them to be responsible American citizens and positive contributors to the workforce. 
 
School-Wide Learner Outcomes 
CATS: Character + Ambition + Teamwork = Success 
C Character as a Strength, Students will: 
· Positively contribute to the community and society as a whole 
· Be critical thinkers and problem solvers 
· Demonstrate citizenship and ethical behaviors 
A Ambition, Students will: 
· Be self-disciplined 
· Be self-motivated 
· Be self-directed life-long learners 
T Teamwork, Students will: 
· Collaborate 
· Communicate 
· Cooperate 
· Contribute to the success of everyone 
S Success, Students will: 
· Share the goal to be college and/or career ready 
· Value one’s self and community 
· Be real-world ready 
 
 

2017-18 School Accountability Report Card for Boron Junior-Senior High 

Page 2 of 12