Northwood High School, a fully accredited secondary education institution, rests in the hills adjacent to the Northwood community of 
North Irvine. The vision for NHS embraces a collaborative model in which teachers dissolve the boundaries that traditionally separate 
classrooms and disciplines and work together to write and refine curriculum. Our school currently serves 2196 students ranging from 
grades nine through twelve. 8% of our students are identified as English Language Learners. Students are enrolled in advanced 
placement, honors level, college preparatory and non-college preparatory course work. We also provide different special education 
programs for our identified special education students. These programs include a full inclusion model that assists special education 
students within regular education classrooms, special day classes designed for students with disabilities requiring support beyond the 
regular education classroom, a separate program for developmentally disabled students, and a speech and language program. The 
physical structure of our school supports a spirit of collaboration. Every four classrooms are joined together by an adjacent student 
workroom. Our student workrooms or “pods” allow teachers and students from different disciplines to open the doors of their 
classrooms and interact. We also built the following features into our program to personalize the nature of student to teacher and 
teacher to teacher interactions. 
 
Advisement—We believe students receive greater individual attention and security within smaller learning communities; therefore, 
our teacher advisement program ensures that each Northwood High School student remains with one adviser, and one group of 
students, for the duration of his or her four years at Northwood High School. Students meet in their advisement four days a week, 
and during this time, students receive important information regarding campus events, discuss school-wide issues, and schedule their 
biannual adviser/parent conferences. These conferences provide an opportunity for students to work individually with their advisers 
and their parents to establish their academic and personal goals and to plan their course work. 
 
Tutorial— In order to address individual student needs further, we structured into our bell schedule forty minutes of tutorial time 
twice a week for students and teachers to meet outside the context of large group instruction. Tutorial provides many opportunities 
for students to work individually with their teachers, to meet with other students to work on group projects, as well as to engage in 
many other learning opportunities. 
 
Humanities Core Program—Our Humanities Core Program also facilitates collaboration by pairing 9th and 10th grade English and 
History teachers together. These teachers share the same students and meet to discuss both curriculum and student achievement. 
Although humanities teachers teach in separate classrooms, they work together to develop and score interdisciplinary projects that 
foreground the connections between their disciplines. 
 
Long-block schedule—Our ninety minute, alternating block schedule allows teachers the opportunity to structure lessons that move 
students more deeply into an activity so that they construct meaning without interruption. Students attend a maximum of four classes 
daily and the sustained time within each class allows students more time to internalize essential concepts and to balance their 
workload over the course of two nights. 
 
Our faculty and staff embrace the elements of Northwood High School that distinguish our school from others. However, we also 
collectively endorse the overarching mission statement and vision for the Irvine Unified School District: To enable all students to 
become contributing members of society empowered with the skills, knowledge and values necessary to meet the challenges of a 
changing world and to accomplish these things by providing the highest quality educational experiences we can envision. To foster 
the IUSD Vision Statement and Mission Goals, Northwood has adopted the following Northwood High School Guiding Principles: 
 
Pursuing growth is our school-wide responsibility. 
 
We believe all students can learn. 
We believe students learn differently. 
We believe learning is an active and ongoing process. 
We believe collaboration enhances learning. 

2017-18 School Accountability Report Card for Northwood High School 

Page 2 of 14 

 

We believe growth is achieved through reflection, support, and intellectual risk taking. 
We believe in fostering an environment that encourages the overall balance to support the whole person. 
We believe we all need to feel emotionally, physically, socially, and intellectually safe. 
We believe embracing diversity fosters understanding and strengthens our community. 
We believe making a connection to school is essential. 
We believe in compassion, mutual respect, and trustworthiness. 
 
Therefore, in our conversations, our thinking, our teaching, and our decision making, we do what is best for the growth and learning 
of all students.