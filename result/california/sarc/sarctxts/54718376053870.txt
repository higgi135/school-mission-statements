Burton Elementary School is located on the west side of Porterville (population 40,625) in rural Tulare County, in the heart of the San 
Joaquin Valley. BES serves approximately 590 students in Kindergarten through Sixth Grades, and is one of nine schools comprising 
the Burton School District. Committed to Our Students Burton Elementary School is a dynamic learning environment for students, 
teachers, staff, and parents. The learning community is united with goals of strong character, building a strong school community, 
ongoing professional development, and academic achievement for all. Students, staff, and families are committed to the vision of 
school success for every student. 
 
School Vision: Burton Elementary strives to be a neighborhood school where staff and students feel valued and appreciated; authentic 
learning takes place, and technology is used to educate the whole child. School Mission: Burton Elementary School seeks to establish 
itself as a present-day, neighborhood school by encouraging family gatherings, service projects, and parent volunteers; Burton 
Elementary School will meet the demands of the 21st century by expanding technology in each classroom and emphasizing creativity, 
critical thinking, and collaboration. Burton Elementary School will bring the 1:World vision to life by using technology to focus on 
community and social awareness issues. Burton Elementary centers our culture around Capturing Kids Hearts. We seek to make sure 
that each child and staff members feels that they are a valued member of our BES community. Technology will play a pivotal role in 
this process as students and adults explore significant and relevant ways in which they can impact their local or global community and 
enhance student learning.