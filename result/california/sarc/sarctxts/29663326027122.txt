Lyman Gilmore Middle School welcomes over 500 students each morning. With a dedicated staff that values the whole child, Gilmore 
hosts a wealth of academic and enrichment options. Electives include computer animation, Gilmore News Network, dance, band, art, 
musical theater, foods, computer programming, coding, robotics, STEAM, and more. Academic options include high school level math 
(Algebra and Geometry equivalent) available in 8th grade, honors courses, and a learning center with multiple resources. This school 
year, 2017-18, Lyman Gilmore Middle School added the Dual Immersion Program from Bell Hill Academy (a K-4 Dual Immersion School 
in the Grass Valley School District). The program starts in the 5th grade this year and will move into the 6th grade during the 2018-19 
school year. Full implementation is expected by the 2020-21 school year. Lyman Gilmore has been recognized as a California 
Distinguished School, a Title One Academic Achieving School, and has been ranked, through various measures, as in the top 1% to 15% 
of middle schools in California. We live up to our mission statement, which reads, “Lyman Gilmore Middle School is a safe, nurturing, 
stimulating, and adventurous learning community dedicated to ensuring that all learners are respected, encouraged, and supported 
in their academic, social, and personal growth. 
 
Lyman Gilmore prides itself on meeting the needs of the “whole child” and providing a well-rounded, balanced education. Every 
weekday morning, different clubs and activities (Gilmore News Network, Math Club, Interact and various sports teams to name a few) 
meet before school. Everyday after school, a host of other activities takes place, (including our free after-school program) providing 
students an opportunity to explore, learn and grow in a safe, stimulating environment. Equal pride is also taken in student academic 
growth. We take our state test scores very seriously (CAASPP) and for that reason have set goals that are specifically directed toward 
improving test scores and better preparing our students for high school academia. Teachers, 6-8 grades, give students the CAASPP 
Interim Benchmark Assessments to prepare their students for the CAASPP. 
 
Like our students, the Lyman Gilmore staff is continually challenged to learn and to grow, to be part of creating a future that is inspiring 
and hopeful. Lyman Gilmore, Jr. was part of creating a future of possibilities that few could even dream. The school that bears his 
name is pleased to carry on that legacy. 
 
Gilmore's Vision Statement is: "As lifelong learners, we are empowered to succeed, have confidence in our potential, and see the 
world as a place full of opportunities."