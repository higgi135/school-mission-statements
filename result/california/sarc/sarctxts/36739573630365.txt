Principal's Message 
Welcome to Chaparral High. I invite you to read this year's School Accountability Report Card containing valuable information about 
our school's staff, programs, facilities, and progress in providing a rigorous, standards-based comprehensive curriculum to our 
students. In accordance with Proposition 98, every school in California is required to issue an annual School Accountability Report 
Card. To keep the lines of communication open, we welcome any suggestions, comments, or questions you may have about this report. 
Our number one priority is our students. 
 
Mission Statement 
Snowline Non-Traditional Education staff strives to inspire our students to reach their full potential academically, socially, and 
behaviorally. Our goal is to promote confidence, instill success, and cultivate learning, so that students become effective 
communicators and problem solvers within their communities. 
 
School Profile 
Chaparral High School is located in the central region of Phelan and serves students in grades nine through twelve following a 
traditional calendar. Students who attend Chaparral High are provided a safe, flexible learning environment to complete graduation 
requirements and explore post-secondary options. Instruction follows a seven-period schedule. Chaparral participates every spring in 
the CAASPP state testing to monitor the progress of the students. Students must either be 16 years of age upon enrollment or must 
turn 16 years of age by the end of their first semester to attend Chaparral High. 
 
Effective at the beginning of the 2013-14 school year, enrolled students may participate in Chaparral High School's Edgenuity online 
learning program. Coursework may be completed during and after school on campus and at home with Internet access. A full range 
of core curriculum and electives are available. 
 
Chaparral High has a six-year WASC accreditation. In 2011, the California Department of Education recognized Chaparral High School 
as a Model Continuation High School. Only 27 high schools throughout the state received this honor for their outstanding efforts to 
create a culture of learning and educational continuity for challenged students. 
 
It is the goal of Chaparral High School to serve the needs of all students while educating them in accordance with the goals established 
in our Schoolwide Learner Outcomes and Academic Standards (SLOAS) and the Common Core State Standards. Our SLOAS are divided 
into three areas: Academics, Career Preparation, and Productive Citizenship. The administration and staff are composed of a core 
group of professionals who are dedicated to the progress of Chaparral programs. Teachers continually strive to update curriculum, 
technology, and teaching methods in order to prepare students for twenty-first century learning and career readiness. Our desire is 
to help all students develop to their fullest potential – mentally, physically, emotionally, and socially.