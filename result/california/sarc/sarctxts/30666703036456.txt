Santa Ana Valley High School (VHS) is a large comprehensive high school situated in the city of Santa 
Ana and is a part of the Santa Ana Unified School District. Valley High School opened its doors in 
1959 and will celebrate its 60th anniversary next year in 2019. We intend to serve as a launching 
pad for the future endeavors of our students, as we prepare them for careers and college. 
 
The school is comprised of over 100 classrooms located on 55 acres. Facilities include a beautifully 
refurbished cafeteria nicknamed Club 59, a 1,500 seat auditorium, a magnificent Aquatic Center, a 
Sports Complex that consists of a football stadium and practice field, two baseball diamonds, two 
softball diamonds, recently modified tennis courts, and a new all-weather track, and lastly a Higher 
Education Center that houses 5 Counselors and two Higher Education Coordinators. We enjoy 
established partnerships with Santa Ana College, California State University, Fullerton, and the 
University of California, Irvine. Additionally, our students and families benefit from many other 
collaborative relationships with entities such as the Orange County Department of Education, the 
Nicholas Academic Center, the Corbin Family Resource Center, Healthy Options and others. 
 
All departments have adapted their curricula to reflect the Common Core State Standards into our 
curriculum and instruction. VHS offers a variety of curricular programs to meet the needs of 
students, including the High School Inc Academies, AVID program and the Anteater Academy (for 
students committed to taking a minimum of 4 honors or AP courses in their yearly schedule). We 
offer a variety of Honors and Advanced Placement courses in English Language and English 
Literature, Spanish Language, US History, World History, US Government, Macro Economics, 
Calculus AB and BC, Statistics, Psychology, Physics, Biology and Environmental Science. 
 
Our school's focus is High School Inc., a partnership with the Santa Ana Chamber of Commerce and 
the High School Inc. Foundation, in which over 150 local businesses collaborate to support six 
academies at Valley High School: Culinary Arts and Hospitality; Health; Automotive; New Media and 
Performing Arts; Global Business; and Engineering. The aim of High School Inc. is to enable business 
partners to work closely with the career academies to create curriculum that aligns rigorous 
academic courses with relevant career and technical education and activities that will allow 
students to learn more about different careers. Our students in our CTE academies, High School 
Inc., leave Valley High School ready for college and career, prepared to enter post-secondary 
education programs, and enter the workforce with a strong skill set already mastered. 
 
Mission 
Valley High School is a supportive community which inspires and equips students with the skills to 
meet the career and academic challenges of the 21st century global society utilizing academies and 
industry partnerships. 
 
Vision 
Valley High School students graduate as strong, connected, lifelong learners with the necessary 
knowledge, skills, and character to enable their successful transition to post-secondary education 
and careers. 
 
 

 

----

---- 

Santa Ana Unified School District 

1601 East Chestnut Avenue 
Santa Ana, CA 92701-6322 

714-558-5501 
www.sausd.us 

 

District Governing Board 

Valerie Amezcua – Board President 

Rigo Rodriguez, Ph.D. Vice – 

President 

Alfonso Alvarez, Ed.D. – Clerk 

John Palacio – Member 

 

District Administration 

Stefanie P. Phillips, Ed.D. 

Superintendent 

Alfonso Jimenez, Ed.D. 
Deputy Superintendent, 

Educational Services 

Thomas A. Stekol, Ed.D. 
Deputy Superintendent, 
Administrative Services 

Mark A. McKinney 

Associate Superintendent, Human 

Resources 

Daniel Allen, Ed.D. 

Assistant Superintendent, K-12 

Teaching and Learning 

Mayra Helguera, Ed.D. 

Assistant Superintendent, Special 

Education/SELPA 

Sonia R. Llamas, Ed.D., L.C.S.W. 
Assistant Superintendent, K-12 
School Performance and Culture 

Manoj Roychowdhury 

Assistant Superintendent, Business 

Services 

Orin Williams 

Assistant Superintendent, Facilities 

& Governmental Relations 

 

2017-18 School Accountability Report Card for Santa Ana Valley High School 

Page 1 of 12 

 

Valley High School graduates will be 
 
Connected 
Listen, interpret and respond appropriately and critically to verbal and nonverbal communication. 
Articulate ideas using a variety of media responding to varying demands of audience, task and purpose. 
Demonstrate respect and tolerance for individual differences, cultures and beliefs. 
Learn from and work collaboratively with others. 
Take responsibility for individual actions and affect positive change in the community. 
 
Lifelong Learners 
Write in a coherent, focused manner to convey a well-defined and supported point of view. 
Use technology as a tool to research, organize, evaluate and communicate information. 
Read, analyze and comprehend complex material. 
Evaluate evidence, arguments, and claims and draw conclusions based on informed analysis. 
Apply learning to real life situations. 
 
Strong 
Reflect critically on learning experiences and processes. 
Creatively generate original ideas. 
Evaluate priorities, set goals, and create a plan for achievement in high school, post secondary education and a career. 
Exhibit productive study habits and behaviors that facilitate success. 
Demonstrate self-motivation and self-discipline. 
 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently serving nearly 49,300 students in grades K-
12, residing in the city of Santa Ana. As of 2017-18, SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 
alternative high schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or Reduced Price Meal 
program, 39% qualifying for English language learner support, and approximately 13% receiving special education services. Our district’s 
schools have received California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I Academic Achieving 
Schools, and Governor’s Higher Expectations awards in honor of their outstanding programs. In addition, 20 schools have received the 
Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have developed and maintained high 
expectations to ensure every student’s intellectual, creative, physical, emotional, and social development needs are met. The district’s 
commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high quality educational 
program. Consistent success in meeting student performance goals is directly attributed to the district’s energetic teaching staff and 
strong parent and community support.