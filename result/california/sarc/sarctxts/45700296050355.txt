The Igo-Ono-Platina Union Elementary School District is very proud of the community-centered, 
quality education that we provide to our students. The District comprises one kindergarten through 
eighth grade school that serves the rural communities of Igo, Ono, and Platina. 
 
Our staff believes that teachers and administrators are part of the learning community. We 
welcome parents, grandparents, family, and community members to volunteer at our school. 
Parent volunteers at school are very active in providing educational opportunities and materials for 
our students. We value the help and support we receive from our friends and neighbors. Although 
we live in rural communities, our intent is to give our students the skills, knowledge, and intellectual 
capacity they need to be successful in any location. 
 
The Igo-Ono-Platina Union School District, in cooperation with parents and community, is resolved 
to provide a quality education to our students within an enthusiastic and caring environment. We 
strive to instill a sense of responsibility and individual achievement for each student as he or she 
acquires the skills necessary for our complex world. 
 

-------- 

Igo-Ono-Platina Union 

Elementary School District 

6429 Placer Rd 
Igo, CA 96047 
530-396-2841 

igo.reddingschools.net 

 

District Governing Board 

Robert Hylton 

Diane Morris 

Gayle Martin 

 

District Administration 

Robert Adams 
Superintendent 

Robert Fellinger 

Chief Business Official 

Cindy Bishop 

Dir. Educational Services 

Cindy Trujillo 

Dir. Human Resources 

Tawny Cowell 

Dir. Facilities/Nutrition Services 

Kim Bryant 

Dir. Intervention Services 

Seth Hemken 

Dir. Technology 

 

2017-18 School Accountability Report Card for Igo-Ono Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Igo-Ono Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Igo-Ono-Platina Union Elementary School 
District 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

2 

1 

0 

2 

0 

0 

2 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Igo-Ono Elementary School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.