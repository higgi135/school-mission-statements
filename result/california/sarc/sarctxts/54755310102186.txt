A Message from the Principal: 
 
 
Ronald Reagan Academy provides students with an individualized educational program. Students 
meet an average of once per week with their instructor and are required to complete a minimum 
of 20 hours of academic work per week. Students meet with their instructors in office cubicles in 
one of two classrooms on the Ronald Reagan Academy campus. Students are provided with 
content specific work packets on a weekly basis that are to be completed at home and turned in to 
their teacher the subsequent week. Each packet is worth one credit. Students must complete an 
end of unit assessment before moving onto each subsequent packet. The hour that students spend 
with teachers is tentatively scheduled as 15 minutes for review of homework, 30 minutes of 
instruction and 15 minutes to review new homework assignments. 
 
 
 All math classes are held onsite via online instruction and are common core aligned. Computer 
Application, Personal Finance, English 9 and English 10 courses are also provided via online 
instruction and are common core aligned. A math homework lab is also provided during the week 
to provide students with additional math support. 
 Ronald Reagan Academy provides an alternative education placement to those students who 
function best in a highly individualized and minimal school setting. Because student work is 
primarily completed independently, Ronald Reagan Academy is reserved for students who have 
evidenced ability to work independently and with minimal academic support. 
 
 A Ronald Reagan Academy task-force is currently working on updating all course curriculum that 
aligns with common core standards. 
 
Focus for Improvement 
The focus of improvement for Ronald Reagan Academy is based on the following as recommended 
by our Mid-Cycle WASC review completed in the 2014-2015 school year: 
 
Recommendations 
1. The school site staff needs to rewrite their course materials to reflect on implementing the 
common core curriculum 
2. The school site and district administration need to evaluate allocation of human resources for 
special needs learners at Ronald Reagan Academy. 
3. The school site staff needs to continue the development of a Long-range Technology Plan that 
increases student access to computers, includes professional development for staff, and addresses 
the identified assessment needs in respect to California Common Core State Standards. 
4. The school site staff needs to create a plan that leads to a greater awareness, involvement, and 
responsibility of all stakeholders in the school’s on-going journey of continuous improvement 
 
 
 
 
 
 

2017-18 School Accountability Report Card for Ronald Reagan Academy 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ronald Reagan Academy 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Dinuba Unified School District 

With Full Credential 

Without Full Credential 

16-17 17-18 18-19 

14 

0 

0 

9 

0 

0 

2 

0 

0 

16-17 17-18 18-19 

♦ ♦ 

314 

♦ ♦ 

18 

11 

Teaching Outside Subject Area of Competence ♦ ♦ 
 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ronald Reagan Academy 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.