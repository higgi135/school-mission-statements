Cesar E. Chavez Middle School (CCMS), which opened in August 2005, is a neighborhood school that provides students with a safe 
place to learn, outstanding teachers, and a rigorous academic environment. Most importantly, graduating students leave CCMS ready 
to complete grade-level and higher-level academic work in high school and beyond. 
 
Here is our Mission/Vision Statement: 
 
Our school is a safe, secure and nurturing environment that provides a collaborative and rigorous academic setting. 
Our school provides opportunities for students and staff to innovate, take risks, and learn from both failure and success. 
Our school understands the importance of character and how the development of character can help shape students and staff to live 
a more fulfilling life. 
Our goal is for all our students to be prepared for high school. 
 
Academically, all students take a full year of math, language arts, PE, science, and social studies. We offer Band and Art to our 7th and 
8th grade students. We offer enrichment activities either in the regular classroom or in the afterschool program. Physical activity and 
athletics play an important and critical role in our program, and all students have the opportunity to participate in one or more of our 
many sports activities.