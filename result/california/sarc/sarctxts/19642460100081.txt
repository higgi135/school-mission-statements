William J. "Pete" Knight High School, home of the Hawks, opened its doors on September 2, 2003. 
 
The school was named for the late California Senator, William J. “Pete” Knight, who was a community leader and role model through 
his work over the years of distinguished service in the U. S. Air Force, as a test pilot at Edwards Air Force Base, as the first mayor in the 
City of Palmdale and as a California Senator. 
 
Based on the ACS WASC mid-cycle review, it has been determined that William J. Pete Knight High School (grades 9 -12) meets the 
ACS WASC criteria for accreditation. This accreditation status is based on all of the information provided by the school, including the 
school’s progress report, and the satisfactory completion of the on-site mid-cycle accreditation visit. 
William J. Pete Knight High School’s accreditation is now reaffirmed through the end of the six year cycle ending in June 2021 
 
All students select courses from a variety of University of California a-g approved classes that not only fulfill high school graduation 
requirements but university entrance as well. These rigorous courses, our support programs and qualified staff support students to 
complete requirements that prepare them for college and encourage critical thinking skills. 
 
William J. "Pete" Knight High School is an academically rigorous learning environment that promotes college and/or career goals for 
all students through standards driven curriculum and student centered instructions. 
In accordance with this statement our Vision statement reads, "Every student at Knight High School will have the option of attending 
a four-year college or university." 
In accordance with our vision statement, Pete Knight High School strives to continue and strengthen the College-going Culture. The 
master schedule, instructional minutes, a modified schedule and a highly qualified faculty support the vision that all students will be 
prepared for college or university entrance. 
 
The modified bell schedule includes a homeroom period and a weekly reduced, or “flex” Wednesday schedule. The homeroom period 
is designed to promote personalized learning through small learning communities with teacher advocates and peer study groups to 
provide support for each student. Reduced Wednesdays are designed for teachers to participate in a collaborative team staff 
development model to create lessons, common assessments, common vocabulary, and syllabi, as well as professional development 
opportunities. In addition, school-wide initiatives such as Classroom Walk Throughs, AVID and Marzano instructional strategies exist 
throughout all subject areas. 
 
Instructional Partners are in place to provide support to new and veteran teachers in instructional strategies and teaching skills that 
enhance teacher efficacy. The master schedule is built in accordance to student requests and needs. Numerous support classes are 
offered, as well as Advanced Placement and Honors courses. 
 
A climate of collaboration and communication drives Knight High School. Community meetings and collaboration among staff and with 
families enhances the learning community. By maintaining positive relationships and continuing to work together, we are confident 
that Knight High School will continue to be viewed as an exemplary learning institution. 
 
 
 
 
 
 
 

2017-18 School Accountability Report Card for William J. (Pete) Knight High 

Page 2 of 15