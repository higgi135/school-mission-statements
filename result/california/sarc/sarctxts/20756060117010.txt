Minarets High School 
Minarets High School is a comprehensive high school that is project-based, tech-centered (one-to-one laptop program), student-
oriented and career-focused. Minarets High School is an extension of the commitment of the current Chawanakee Unified School 
District policies that emphasize innovation and creativity along with rigorous academics. Examples include utilizing technology to learn 
21st century schools, smaller class sizes, high level of teacher access (all teachers publish their cell phones for student calls, texts, etc.), 
student involvement in all hiring of teachers (interview panels, teaching demonstrations, etc.), student surveys of all classes and 
instruction, high profile programs in Arts/Media/Entertainment, as well as Ag and Natural resources, high degrees of student input, 
students as professional presenters, student project menus, student project coordinators and more. 
 
Minarets High School places a central focus on job and professional skills, as well as the need for college preparatory programs. 
Additionally, the school has already instituted two Career Pathways outlined in the Educational Master Plan – one for Ag and Natural 
Resources, as well as one for Arts, Media and Entertainment. Staff, parents and community members engaged in the Minarets learning 
community know that people have different learning styles and that in education one size does not fit all. From its inception, Minarets 
High School was to be unique—a school based on individual learning and truly being student-centered in all aspects. 
 
Minarets High School, is a fully accredited public high school that serves students in the Chawanakee Unified School District. 
We believe strongly in the goal as a school to provide students with a positive learning environment. Our Project Based, rigorous and 
relevant, one to one laptop approach is at the core of our mission to provide students with rigor, relevance, and relationships that 
positively impact their future. 
 
Minarets students will engage in a challenging academic experience driven by professional level technology, project-based instruction, 
and college and career readiness. We are a community where professionalism, academic rigor and social success are the foundation 
for learning. 
 
Vision: 
While incorporating the guiding principles of Rigor, Relevance, Relationships and Engagement, Minarets High School / Minarets 
Charter High School will continually work towards being a model 21st century high school that integrates technology, digital learning 
models, student-centered learning and young professionalism. The SIX C’s of a 21st century education – Creativity, Critical Thinking, 
Communication, Collaboration, Competency, and Community – are essential and foundational to the learning environment and 
outcome of Minarets. 
 
Minarets must have a strong academic program, excellent staff and the appropriate facilities to be successful. Teachers at Minarets 
must be supported and trained to provide a rigorous, relevant education that begins with a focus on relationships. Teachers at 
Minarets High School must be leaders in the Educational Technology community and seek professional development to maintain a 
high level of success with students. Quality professional development founded in research and based on student needs is provided for 
staff members. Minarets High School staff will continue to participate in CUE (Computer Using Educators), as well as other professional 
development organizations and opportunities. All of Minarets High School’s instructional staff will regularly participate in a summer 
training institute every summer that focuses on technology integration, student success, program evaluation and 21st century 
standards. Minarets staff will continue to present at various professional and educational conferences as a way to hone their 
instructional skills. All instructional staff will participate in weekly staff collaborations. 
 

2017-18 School Accountability Report Card for Minarets High School 

Page 2 of 12