Hollywood Park Elementary School is a small, neighborhood school that is culturally diverse. We 
have a cohesive staff that work collaboratively to provide exciting and creative lessons in English 
Language Arts, Math, Social Studies, Science, Physical Education, and Art. We have a small, but 
active PTO that work hard to provide fundraisers, assemblies, campus beautification, and other 
school wide events. Our Leadership Club focuses on community service projects and school spirit. 
Our goal is that the students at Hollywood Park Elementary School will continue on to middle school 
with a skill set that includes a strong academic foundation, the ability to use and work with 
technology, the capacity to work collaboratively with their peers, and develop a love of learning. 
 

2017-18 School Accountability Report Card for Hollywood Park Elementary School 

Page 1 of 11 

 

Hollywood Park Elementary School offers a wide variety of experiences and opportunities for our students and families: 
GATE Certification of all Teachers 
CLARA Visual and Performing Arts 
Character Education 
Chess Club 
Parent-Teacher Organization (PTO) 
4th R 
New Hope 
Kinder-Plus (an extended Kindergarten day) 
Kindergarten-2nd Grade Music Program 
4th-6th Grade Leadership Club 
Field Trips- IMAX, Sunsplash, River Cats Triple A Day, Capitol, Crocker Art Museum, Junior League Play, Sacramento Zoo, Sacramento 
Ballet, California History Museum, Sky High, Land Park, Galena Street East, Pumpkin Patch, Bowling, Soil Born Farms, B Street Theater, 
Community Center 
4th-6th Grade Camp Trips- Coloma, Westminster Woods, Marin Headlands, Sly Park 
Yearbook 
Technology- MacBook Airs in every classroom 
Library 
Awards Assemblies 
School Spirit Dress-Up Days 
Community Service Projects- Pennies for Pets (SPCA), Loaves and Fishes Stockings, Veteran's Day Letters, Valentines for Shriners, Kids Can 
Food Drive 
6th Grade--Crocker Art Docents Program 
Sports Teams- Indoor Soccer, Basketball, Flag Football, Stride 
Runnin' for Rhett 
Partnership with CSUS 
After school tutoring