Lake Canyon Elementary School's mission is to personalize the learning of each and every one of our students through the provision 
of a quality and meaningful educational experience. As educators we know at the heart of creating sustainable academic achievement 
and ensuring the college and career readiness of our students, is the provision of a school culture where engagement is valued and 
maximized. Lake Canyon Elementary School’s administration, staff, and parents have joined together to make increased student 
engagement a reality. We provide varied opportunities, both indoors and outdoors, for our students to discover and explore their 
areas of interest and talent. Our vision is that through the ongoing implementation of this mission the students of Lake Canyon will 
be 100% prepared for next steps in their educational pursuits and will achieve civic, college, and career readiness.