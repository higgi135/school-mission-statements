Lampton Elementary School serves almost 700 students in kindergarten through fifth grade. Our 
school has received several honors for our exemplary instructional program, Our awards include 
the California Distinguished School award, Title 1 Academic Achievement Award, and the California 
Businesses for Education Star School Award. Lampton's campus is always well cared for, and our 
classrooms are positive and productive places to learn. The school is adorned with State 
achievement award emblems, three dolphin murals, a beautiful tiled entrance area, and a three-
dimensional wall sculpture done by a local artist. It is our mission to develop the personal skills and 
academic abilities of each individual student. 
 
Our students receive a well-rounded education at Lampton. Teachers engage students in 
interesting lessons, utilizing many effective teaching strategies. Literacy centers, flexible skill 
groupings, and active learning strategies abound. Instruction is designed around state and district 
standards and infused with experiences that integrate subjects and enhance understanding for 
students. The curricular program is enriched by a strong relationship with the Cerritos Performing 
Arts Center. They provide teacher training and free tickets for classes to attend live theater, dance 
and music productions. Most students have had the opportunity to attend at least one free 
performance each year. Lampton's school wide Positive Behavior Support system has also 
increased positive behavior on campus. In 2013-14 we began implementing Playworks on our 
playground. A Playworks "Team Up" Coach has organized and supervised positive play activities 
for students during recess and lunch. This program has greatly decreased conflict and bullying on 
our campus. 
 

 

 

----

--- 

Norwalk-La Mirada Unified 

-

School District 

12820 Pioneer Blvd 
Norwalk, CA 90650 

(5662) 868-0431 
www.nlmusd.org 

 

District Governing Board 

Ana Valencia Board President 

Jude Cazares Board Vice President 

Darryl R. Adams Board Member 

Jesse Urquidi Board Member 

Karen Morrison Member 

Chris Pflanzer Board Member 

Jorge Tirado Board Member 

 

District Administration 

Dr. Hasmik Danielian 

Superintendent 

Dr. Patricio Vargas 

Assistant Superintendent - 

Educational Services 

Estuardo Santillan 

Assistant Superintendent, Business 

Services 

John Lopez 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for Loretta Lampton Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Loretta Lampton Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

26 

25 

28 

0 

0 

0 

0 

0 

0 

Norwalk-La Mirada Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

898 

2 

4 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.