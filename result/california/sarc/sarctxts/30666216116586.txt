Running Springs Elementary School is one of 27 elementary schools in the Orange Unified School District. The school was established 
in July of 1999 and is located in a residential neighborhood in Anaheim Hills. Running Springs consists of thirty classrooms, six portable 
classrooms, a multi-purpose building, and an administration building. The administration building contains offices, the staff lounge 
and workroom, the STEAM lab, and library. Approximately 700 students were enrolled in grades transitional kindergarten through 
sixth including general education and special education during the 2017/2018 school year. Running Springs students are fortunate to 
have the opportunity to learn from the variety of ethnic and cultural backgrounds that make up our diverse population. 
 
Running Springs was moved from a year round calendar to a modified traditional calendar during the 2009/2010 school year. Running 
Springs provides the following number of instructional minutes with a modified Wednesday schedule: Kindergarten- 295 minutes, 
grades 1 to 3- 295 minutes, and grades 4 to 6- 320 minutes. Grades K-6 each have 245 minutes of instruction every Wednesday to 
allow for professional development and teacher collaboration to plan instruction that prepares students for the 21st century. 
 
The school has a significant technology infrastructure as part of its design. As a result teachers are able to use a variety of technology 
resources as instructional tools including; a STEAM Lab, classroom computers, and hand held devices. These devices allow each 
student with the opportunity to develop computer skills necessary as our students move to middle and high school. School-wide 
technology is also used to access diagnostic and periodic assessments that support each student's individual needs. 
 
During the 2017/2018 school year, teachers were provided support to develop their expertise in differentiated instruction, while 
incorporating 21st century skills that target student communication, collaboration, creativity, and critical thinking. As a result, 
students were provided with rigorous instruction that aligned with the Common Core State Standards. To meet the individual needs 
of students, benchmark assessments were used to reflect and determine the individual support each student required to be successful. 
Support was provided through small group instruction in the classroom and after school tutoring. 
 
Student leadership plays an important part in our school community. Students can exhibit leadership abilities through classroom jobs 
and the opportunity to lead projects. In addition, upper grade students can apply to be selected for Student Council. This group of 
students are role models for their peers and support spirit days and lunch activities. Another opportunity for students to participate 
in leadership is through our Peer Assistance and Leadership program (PAL). Students in the PAL program are dedicated to working as 
a group to solve issues on campus and design and implement community projects to better our school. In addition, we have 
implemented an student run technology support team named the Redhawk Technology Squad (RTS). 
 
Running Springs is committed to providing a success-oriented and safe learning environment for all students. The teaching staff 
encourage parents to become involved and familiar with their children's school. We welcome and depend on the strong parent 
involvement to provide a team approach to our children's education. 
 
Vision Statement: 
The staff and community of Running Springs Elementary School are dedicated to providing a quality education which will empower all 
students to achieve academic success, embrace life-long learning, and become productive citizens. 
 
Mission: 
It is our mission to fully utilize all available resources to help each and every student continue to grow academically and emotionally. 
As Running Springs Redhawks, we promote "Soaring for Excellence". 
Running Springs Redhawks S.O.A.R.! 
 
S- Safe 
O- Outstanding Effort 
A- Accept Responsibility 
R- Respectful 

2017-18 School Accountability Report Card for Running Springs Elementary School 

Page 2 of 15