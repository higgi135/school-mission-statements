Capital City School is one of the most innovative schools in the Sacramento City Unified School 
District (SCUSD). Our goal is to customize a quality education program for students whose needs 
are best met through study outside the traditional setting. Our independent study strategy allows 
teachers and students to confer one-on-one focusing on the student’s educational needs, interests, 
aptitudes and abilities. 
 
Capital City School serves students in grades K-12. Full accreditation (six years) from the Western 
Association of Schools and Colleges (WASC) was granted to Capital City School in the year 2000. In 
February 2013, we completed our WASC review and received six years accreditation with a renewal 
date of June 30, 2019. Course material, assignment criteria, textbooks and standards adhere to a 
continuity that allows students to return to the traditional school setting if they wish to do so. 
However, many students do make Capital City School their school of choice. 
 
Students and parents value the small school environment, one-on-one teaching strategy, 
individualized educational plans and high standards. Capital City School has proven to be a catalyst 
for building self-esteem and motivating students to get back on track. The staff’s hard work is 
evident in the changed attitudes and beliefs of students who were once underachievers. More 
than simply a means by which many students make up lost credits and complete their requirements 
for graduation, Capital City’s safe, caring environment fosters academic and personal growth. 
 

 

 

----

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Chad Sweitzer 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for Capital City Independent School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Capital City Independent School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

17 

17 

20 

1 

0 

1 

0 

1 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Capital City Independent School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.