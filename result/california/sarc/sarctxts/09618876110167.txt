OUR DISTRICT VISION 
The District vision is to create and maintain an environment where every child receives a high quality education and comes to school 
feeling safe, cared for, and optimistic about his or her ability to learn. 
 
OUR SCHOOL MISSION 
We are committed to providing academic programs that will lead to high achievement and assist our children to have well developed 
life skills for the world of the future. We are a community founded on the principle of treating others as we want to be treated and 
dedicated stewards of the funds entrusted to us to educate children. The motto of Gold Trail Union School District reflects our 
educational goal to provide all students, regardless of background or socioeconomic status, with the comprehensive education to 
prepare them for success in a literate world. To attain this goal, the Sutter’s Mill professional staff has adopted research and standards-
based curricula, which provides a sound educational foundation for all students, yet is flexible enough to accommodate their individual 
needs and interests. 
 
SCHOOL DESCRIPTION (FY 17/18) 
Sutter’s Mill School is located in a beautiful rural setting, nestled in the foothills, three miles from Coloma the California gold discovery 
site. The Sutter’s Mill School site was dedicated October 26, 1989 and opened with temporary facilities in September 1991. Permanent 
construction was completed in September 1992. Sutter’s Mill School currently serves 270 students in grades TK-3. 
 
PROGRAMS 
The school has a parent/school advisory council that annually writes a school based curricular plan and includes a transitional 
kindergarten program to optimize learning at an early age. The Gold Trail Board of Trustees approves the school plan annually. The 
plan allows the school to mix various instructional funds to support enriched and cooperative school goals for integrated learning 
based on state grade level standards, thematic teaching, and parent involvement.