Our Mission Statement 
"The educational community will assist all students in maximizing their academic achievement and 
personal responsibility." 
 
Our Vision Statement 
At Little Lake Elementary we are motivated by this belief: students and their families hold the power 
to reinvent themselves and their world. They must be given the opportunity to develop a strong 
sense of self, a commitment to social responsibility and the academic agility needed to improve 
themselves and their community. Education should not be the repository of the status quo. Rather, 
it should be an instrument for freedom, teaching students how to deal effectively with the world 
and to help transform it.We believe in the power of the individual and the power of community. 
We are dedicated to providing a nurturing, challenging and creative learning environment that 
celebrates diversity with compassion and acceptance. 
 
School Profile 
Little Lake Elementary was established in 1899. We are proud of our heritage and our long standing 
tradition of a commitment to excellence. This is appropriately reflected in our Mission Statement: 
“Little Lake Elementary is to assist all students in maximizing their academic achievement and 
personal responsibility.” We are committed to a child centered learning environment with the 
highest level of excellence in teaching, learning, and positive behavior. 
 
Currently, Little Lake has 35 active classrooms. Twenty seven classrooms are utilized for general 
education classes in grades TK thru 5th and eight classrooms are utilized for special education. Five 
of the special education classes are Severely Handicapped elementary and preschool programs 
which draw their attendance from both inside and outside our district boundaries. 
 
To effectively maintain our high teacher expectations as well as continually strive to exceed them, 
change is inevitable and our staff is learning that collaboration is essential. We have revised our 
Mission and Vision Statements (as displayed above in bold print) so we are able to refocus our 
efforts in an organized manner that will benefit all students. 
 
Little Lake’s teaching and support staff has grown to over 80 members with more than 800 years 
of successful teaching experience. To maintain a Tradition of Excellence, our Little Lake staff 
continues to hone skills through collaboration, staff development. This dedication drives our 
instruction. 
 
We provide rigorous academics that stress flexibility of thought as well as a social-emotional 
approach that emphasizes empathy amongst all members of our school community. We know that 
emotional intelligence is strongly correlated to success in life, both personally and professionally. 
We believe that the best students are complete humans. Children come first at Little Lake 
Elementary. 
 

2017-18 School Accountability Report Card for Little Lake Elementary School 

Page 1 of 10