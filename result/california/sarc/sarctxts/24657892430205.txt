Lance Morrow Principal - Welcome to Buhach Colony High School, “Home of the Thunder!” I am honored to join this incredible high 
school as its principal. Our goal is to provide a rigorous and relevant education to our students so they are college and career ready 
upon leaving our classrooms, athletic fields, and extra-curricular activities. Thunder PRIDE will always shine through. 
Buhach Colony Students, Staff, Faculty and Administration share a common posture and core set of values. We strive daily to embody 
our Thunder PRIDE. 
 
Productivity: We at Buhach Colony are Productive Individuals Who: 
Are punctual and prepared as a means to be constructive. 
Utilize our time efficiently to create quality work and meet established goals. 
Inquire and research to become knowledgeable across a broad range of disciplines. 
 
Respect: We at Buhach Colony are Respectful Citizens Who: 
Display and extend, through both language and appearance, kindness, dignity, and 
courtesy. 
Are positive interpersonal and digital communicators. 
Honor school rules and procedures 
Are caring and considerate of school and personal property. 
 
Integrity: We at Buhach Colony are People of Integrity Who: 
Are principled and act responsibly. 
Display ethical behavior including but not limited to trustworthiness, responsibility, 
fairness, sportsmanship, and citizenship. 
Do and stand for what is right, because it is right. 
 
Determination: We at Buhach Colony show determination through: 
Critically and creatively thinking, identifying, analyzing and finding solutions to 
challenges or problems. 
Displaying dedication, taking risks, and persevering despite difficulties. 
Reflective and purposeful preparation for lifelong learning and a balanced future. 
 
Equality: We at Buhach Colony value equality by: 
Treating others as we would wish to be treated. 
Acting fairly 
Fostering an open-minded environment that welcomes diverse cultures, beliefs, and attitudes that may be different from our own. 
 
BUHACH COLONY HIGH SCHOOL MISSION 
Our mission is to educate and support our students through quality school-wide curriculum. We will offer programs targeted to meet 
the interests and needs of our diverse population. We are committed to educating all students with the school community mindset 
by developing positive relationships through commitment from students, staff, parents and community members. 
 
BUHACH COLONY HIGH SCHOOL VISION 
Buhach Colony High School provides a critical thinking environment that creates the opportunity for all students to be successful. By 
providing a rigorous and relevant curriculum that includes building relationships, we will prepare students for their individual futures 
in a global economy. 
 

2017-18 School Accountability Report Card for Buhach Colony High School 

Page 2 of 24 

 

School Description 
Buhach Colony High School first opened its doors during the 2001-02 school year and is currently one of six comprehensive high schools 
in the Merced Union High School District. The student population for the 2018-2019 school year is 1905, consisting of 63% Hispanic 
or Latino, 21.3% White, 8.9% Asian and Pacific Islander and 3.1% African American. The school currently has one established academy: 
Engineering. The school has 81 classrooms, a library media center, theater, gymnasium, cafeteria, multipurpose room, dance room, 
an administrative wing and several athletic facilities and an aquatic center.