Welcome to Garrison Elementary School, a small school with a big heart! Our experienced, 
professional staff and community members are committed to making our school the center of our 
community, where parents, students, and neighbors can come to learn, socialize, participate in 
organized and safe activities, and enjoy a variety of activities. Our beautiful, parklike campus is 
superb for sports and family activities. We celebrate our rich diversity in ethnicities, languages, and 
socioeconomic status, which all contribute to the exciting and positive culture of our school. Every 
morning we welcome students and families with the Pledge Of Allegiance, a moment of silence and 
a positive quote. 
 
We have a strong focus on the Common Core Standards and the arts. Students are given the 
opportunity to thrive in an exciting educational environment. This fusion creates an exciting 
educational environment that supports the growth and development of the whole child. We are 
very proud of our friendly and positive school culture. 
 
Each year we put on a variety of high quality musical programs. Our production during the Spring 
displayed the many talents of the students and staff at Garrison Elementary School. Our 
intermediate grades produced and performed a patriotic musical honoring our country. 
 
Garrison is a fun place to learn and to grow. All are welcome here! 
 
 

----

---- 

Oceanside Unified School District 

2111 Mission Avenue 
Oceanside CA, 92058 

(760) 966-4000 
www.oside.us 

 

District Governing Board 

Eleanor Juanita Evans, President 

Mike Blessing, Vice President 

Eric Joyce, Clerk 

Raquel Alvarez, Member 

Stacy Begin, Member 

 

District Administration 

Julie Vitale, Ph. D. 
Superintendent 

Shannon Soto, Ed. D. 

Deputy Superintendent 

Todd McAteer 

Associate Superintendent Human 

Resources 

Mercedes Lovie, Ed. D 

Associate Superintendent Business 

Services 

 

2017-18 School Accountability Report Card for E. G. Garrison Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

E. G. Garrison Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

25.50 25.48 

0.0 

0.0 

0.0 

 

 

 

 

Oceanside Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

21 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.