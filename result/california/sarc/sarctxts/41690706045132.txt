Skyline Elementary School has hardworking and fun-loving students, dedicated teachers, and 
supportive parents. We view Skyline School as a place of work (but we also have fun) and each child 
as a unique person with individual gifts, interests, and talents. The curricula and instructional 
strategies are designed to foster a community of lifelong learners. The teaching staff is continually 
improving techniques to encourage optimum intellectual, social, emotional, and physical growth 
for every child. All students have equal access to the curriculum and equal opportunities for 
success. Every classroom has multiple computers and the school has 2 laptop carts that are 
continually used, as well other technologies, which are integrated into the curricular areas. The 
Skyline staff continually participates in staff development. Every child is encouraged to be 
competent and self-reliant. Students are provided a learning environment that enhances positive 
feelings of self-esteem. 
 
We provide additional structured activities like noon time sports tournaments and after-school 
programs like reading intervention, math club, book club, art, garden club and the bricks for kidz 
lego engineering club. There is an RTI teacher for additional support and a Reading Assistance 
Program. 
 
We are proud of the diversity of cultures and languages at Skyline School. Students, staff, parents, 
and community members all feel part of the Skyline School Family. Our school is a great place to 
be! We are very proud to have been awarded the California Distinguished award in May of 2010. 
Our school was awarded Distinguished School for 2014 as well. 
 
As part of our continual improvement process we are updating the school mission statement, and 
creating student learning expectations. Our motto is "At Skyline We Grow Readers and Leaders!" 
 
The mission of Skyline School is to promote the embodiment of our student learning expectations- 
Skyline Students are: Committed, Academic Learners; Active, responsible citizens; and Creative 
Thinkers/Problem Solvers. 
 
 

 

 

----

---- 

----
South San Francisco Unified 

---- 

School District 

398 B. Street 

South San Francisco, CA 94080 

650.877.8700 
www.ssfusd.org 

 

District Governing Board 

John C. Baker 

Patricia A. Murray 

Daina R. Lujan 

Eddie Flores 

Mina A. Richardson 

 

District Administration 

Shawnterra Moore, Ed.D. 

Superintendent 

Keith B. Irish 

Assistant Superintendent, Educational 

Services and Categorical Programs 

Jay Spaulding, Ed.D. 

Assistant Superintendent Human Resources 

and Student Services 

Ted O 

Assistant Superintendent, Business Services 

Leticia Bhatia, Ed.D. 

Director English Learner Programs, 

Categorical Programs and Special Projects 

Jason Brockmeyer 

Director of Innovation, Community Outreach 

and Special Projects 

Valerie Garrett, Ed.D. 

Director of Student Performance, Program 
Evaluation, and Instructional Interventions 

Ryan Sebers 

Director of Student Services 

Velma Veith, Ed,D. 

Director, Pupil Personnel Services and Special 

Education 

Bryant Wong 

Director of Technology 

Ronald Vose 

Director of Facilities and Safety 

Fran Debost, MS, RDN 

Director of Nutrition Services and 

Distribution 

 

2017-18 School Accountability Report Card for Skyline Elementary 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Skyline Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

19 

20 

20 

0 

0 

0 

0 

0 

0 

South San Francisco Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

412 

16 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Skyline Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.