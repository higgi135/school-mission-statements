The Circle of Independent Learning (COIL) Charter School is a personalized learning school that is 
WASC Accredited and authorized by the Fremont Unified School District. A credentialed teacher 
is assigned to each student as the students advisory teacher as well as each teacher provides direct 
instruction in a classroom, "hybrid" model. This advisory teacher facilitates a personalized learning 
plan that offers a wide variety of curriculum choices for students. Students then return at regularly 
scheduled intervals throughout the school year for their COIL teacher to evaluate and assess their 
learning. This model of learning provides for immediate intervention as well as acceleration 
depending upon the needs of each individual student. Although a non-classroom based charter 
school, COIL has four classrooms where a variety of classes are offered in a "hybrid" model by 
credentialed teachers offering a "hybrid" model of direct instruction and independent study. Our 
vision is to “reach the unique bent of genius in each of our students.” 
 
Our “circle of learning,” including student, parent, and advisory teacher, work together to develop 
a personalized plan that best enables the student to discover his or her unique learning style and 
mature into an independent, life-long learner. Within this approach, students are provided with 
opportunities to grow through a variety of classes, activities, and field trips. COIL’s School-wide 
Student Goals provide an accountability system where we monitor students’ progress towards 
becoming effective communicators, self-directed learners and productive, responsible contributors 
to society. 
 
COIL school meets the needs of all students by offering a complete University of 
California/California State University course list, on-line courses, a split-day opportunity with 
Fremont Unified School District (FUSD) for junior and senior high students, a partnership with the 
Mission Valley Regional Occupation Program and concurrent enrollment in local community 
colleges. COIL also partners with the Mission Valley SELPA in providing a Resource Specialist, 
Psychologist, and Speech & Language Services on campus. For the 2017-2018 school year, COIL 
offered the following classes/labs and support: Pre-Algebra, Algebra, Algebra 2, Geometry, pre-
Calculus, French 1, Literature & Language Arts, High School Science Labs in Biology, Chemistry & 
Physics, Psychology, Middle School Science Labs, Elementary Science Classes with the Math Science 
Nucleus, Elementary grade level enrichment labs, Robotics, 5-12 Math Tutoring, Art History, 
Writing, Public Speaking and Art Therapy for students with special needs. In addition, COIL provides 
an on-site therapist for student support and a full time school counselor. 
 
Please visit our web site, where we have more information about our school, classes and activities 
and how you can contact teachers, and more at www.coilk12.net. 
 

2017-18 School Accountability Report Card for CIRCLE OF INDEPENDENT LEARNING 

Page 1 of 13 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

19 

19 

20 

0 

0 

0 

0 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1713 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

0 

0 

0 

0 

0 

0 

1 

0 

Vacant Teacher Positions 
Notes: 
1) “Misassignments” refers to the number of positions filled by teachers who lack 
legal authorization to teach that grade level, subject area, student group, etc. 
 
2) Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners. 
 

0