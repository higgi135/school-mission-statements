COMMUNITY: Located in the West portion of Santa Clara County, Campbell Union School District 
serves more than 7,600 students from the communities of Campbell, San Jose, Saratoga, Santa 
Clara, Monte Sereno and Los Gatos. 
 
Lynhaven School, one of nine elementary schools in the Campbell Union School District, educates 
more than 550 students from grades Transitional Kindergarten through 5th. Our skilled teachers 
provide engaging instruction based on the California state standards. We partner with parents and 
the community to enrich the school experience, provide a caring, respectful school climate, and 
prepare students for the rigors of college and career in the 21st century. 
 
Mission Statement: 
Lynhaven, the home of the Lynx, is a caring community of students, families, and staff who share 
responsibility to ensure that all students achieve a solid academic foundation, communicate 
effectively, and apply critical thinking skills in order to make respectful choices in their academic 
and social life. 
 

 

 

----

---- 

----

--

-- 

Campbell Union School District 

155 N. Third Street 
Campbell CA, 95008 

(408) 364-4200 

www.campbellusd.org 

 

District Governing Board 

Pablo A. Beltran 

Danielle M.S. Cohen 

Chris Miller 

Richard H. Nguyen 

Michael L. Snyder 

 

District Administration 

Dr. Shelly Viramontez 

Superintendent 

Nelly Yang 

Assistant Superintendent, 
Administrative Services 

Lena Bundtzen 

Assistant Superintendent, Human 

Resources Services 

Whitney Holton 

Assistant Superintendent, 

Instructional Services 

 

2017-18 School Accountability Report Card for Lynhaven Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Lynhaven Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

24 

27 

27 

0 

0 

1 

0 

0 

0 

Campbell Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

305.60 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Lynhaven Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.