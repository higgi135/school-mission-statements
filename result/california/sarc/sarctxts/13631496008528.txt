Holtville Middle School is located 120 miles east of San Diego, 40 miles west of the 
California/Arizona border and 20 miles northeast of Mexicali, Baja California, Mexico. Holtville 
Middle School is in a rural, agricultural area and serves approximately 278 students. 
 
In 2017 Holtville Middle School was awarded the Gold Ribbon School Award and recognized as a 
top Title One School in academic achievement. Holtville Middle School is a Gate Honor School and 
a certified AVID School. Holtville Middle School offers not only core subjects, but also, sports, flags, 
and shields cheer, band, robotics, coding, and webpage design, just to mention a few, as part of 
their After-School Education & Safety Program, ASES, and their very successful MESA program. For 
the last two years, and again this year, students took part in a private theater production called the 
Missoula Theater that travels around the country offering our students a chance to experience the 
theater by acting, production support, and even directing in a live theater production. Holtville 
Middle School is moving towards a one - to - one, paperless technology school. The classrooms are 
all equipped with state-of-the-art equipment such as computers, projectors, wireless access points, 
Apple TV's, wall mounted 60"TV and other electronics. Google Chrome, Microsoft programs, and 
other software support some of their paperless goals. In November of 2018, Holtville Unified School 
District administered a parent survey and parents responded strongly that they feel their school is 
clean and well maintained. In addition, parents felt they had input, were able to communicate with 
staff, and were well informed. In August of this year, a Holtville Unified Administrative team 
performed a FIT, Facility Inspection Tool, where they walked through the campus and inspect it 
based on a number of points, and Holtville Middle School was rated as “good.” Holtville Middle 
School enjoys a fairly large volunteer group of people consisting of parents, grandparents, local 
community members, social groups, clubs and organizations, public and private organizations and 
businesses, as well as the fire and police departments. Holtville Middle School promotes an 
environment that fosters respectful, responsible students and ensures that every student reaches 
a high level of academic achievement. The mission of Holtville Middle School is to create a safe 
academic environment, which ensures learning to the highest standards, instills responsibility, self-
worth, respect, and a strong work ethic in all students. 
 
 
Holtville Middle School Students, Parents and Teachers are Committed to Excellence 
 
Vision Statement: 
Our vision is to have every student be a responsible citizen, an independent thinker, an effective 
communicator, and a life-long learner capable of success. 
 
Mission Statement: 
Our mission is to create a safe academic environment, which ensures learning to the highest 
standards, instills responsibility, self-worth, respect, and a strong work ethic in all students. 
 
 

2017-18 School Accountability Report Card for Holtville Middle School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Holtville Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

15 

17 

13 

2 

0 

2 

0 

4 

0 

Holtville Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

78 

11 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Holtville Middle School 

16-17 

17-18 

18-19