The Butteville Union Elementary School District is a rural, one-school K-8 district at the base of Mount Shasta in Siskiyou County. Our 
vision is to hold the highest expectations for students and staff. Our students will become effective leaders in the community by 
achieving academic success through clarity in writing, speaking, thinking, creating and performing. Butteville Elementary has has a 
enrollment of 202 students. We employ a superintendent/principal, twelve teachers and fourteen classified staff in various capacities. 
Students in Transitional Kindergarten through sixth grades are taught in self-contained classrooms, while the seventh and eighth 
grades are in a departmental setting. 
 
At Butteville, we are committed to providing the best educational setting and opportunities for our children. Our highly trained and 
dedicated teachers and support staff collaborate with each other daily. One hour every day is reserved for reading at every grade 
level. Students work in small, leveled reading groups with an adult to practice decoding, reading fluency, comprehension and analysis. 
Through frequent formative assessments and periodic summative assessments, students' reading growth is measured, and students 
are moved to increasingly challenging reading groups as they gain skills. 
 
Beyond reading, Kindergarten through sixth grade teachers adopted new state standards-aligned math curriculum in the 2014-15 
school year. Before the start of the year and during, teachers received on-site training on using the new textbook and related materials. 
Teachers received other on-site and off-site professional development training focused on integrating the new California State 
Common Core Standards into math, science, language arts and social sciences curriculum. 
 
Butteville Elementary School has emerged as one of Siskiyou County’s premier elementary schools. High test scores and a focus on 
fine and performing arts are part of the success that our school is experiencing. We have a very committed Parent Teacher 
Organization (PTO) which is the primary fundraiser to support the arts program. In addition to offering an academic and visual and 
performing arts-focused program, students enjoy a comprehensive sports program and a large garden. The school also provides a 
Spanish program as part of its enrichment offerings. Recently, the School Board of Trustees invested in security measures at the 
campus increasing student safety. A Prop 39 Clean Energy Solar Panel project was completed March 1, 2018 and have applied for 
Prop 51 New Construction and Renovation state funds. 
 
Butteville Elementary School District is dedicated to making the difference for each of our students. We believe that every student can 
achieve at high levels. We believe that every family matters. 
 
 

2017-18 School Accountability Report Card for Butteville Elementary School 

Page 2 of 10