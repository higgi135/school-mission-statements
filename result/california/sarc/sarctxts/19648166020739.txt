Voorhis Elementary School "Home of the Mustangs" serves TK through fifth grade students. It is 
one of ten elementary schools in the Mountain View School District located in the City of El Monte 
in the western San Gabriel Valley. It is our commitment as staff, parents and students, to do 
whatever it takes to be successful in school. Voorhis School provides all students with a child-
centered environment which focuses on providing a positive, supportive climate that enables 
students to develop social interaction skills and foster personal responsibility which helps them 
become successful, contributing members of a global society. 
 
The local community and Voorhis Elementary benefit greatly from their collaboration and 
commitment to each other. We strive to provide a variety of educational and extra-curricular 
events for our students and families. Some of our activities include: Red Ribbon Week, Meet the 
Masters (Art Curriculum), Conga Kids dance group, Read-a-thon, Reading is Fundamental book give-
a-ways, After School Math and Reading Enrichment Clubs, Chinese New Year Celebration, 
International Day, Extended Library Activities and hours, Movie Evenings, Parent Training and 
Workshops, Spirit Days, field trips and 5th grade promotional activities. 
 
The Voorhis School team of highly qualified teachers and staff join parents and community 
members in a collaborative effort to promote student development from acquisition of basic 
academic skills to the application of those skills through rigorous standards-based instruction. Most 
recently our school took the wellness policy to a new level, providing skills and resources to 
students and staff about making nutritious choices and becoming more physically fit. The Voorhis 
School's "Positive Behavior Intervention and Supports (PBIS)" program helps students successfully 
resolve conflicts and internalize focus values. Our Behavior Expectations are taught to all students 
and are posted throughout the school. Our motto is: Voorhis Mustangs Ride with PRIDE! We are 
Productive, Responsible, Independent and Determined EVERYDAY! Our school staff has been 
trained in these expectations and we incorporate them into our daily interactions with each other, 
our students, and our parents. 
 
Our goal in presenting you with the information within this report card is to keep our community 
well informed. If you have any questions or are interested in making an appointment to discuss 
this report, please call the school office at (626) 652-4450. 
 
We want you to feel part of our “family” so if you have any questions about this report card, feel 
free to call our school. 
 
 

2017-18 School Accountability Report Card for Voorhis Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Voorhis Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

20 

19 

17 

0 

0 

0 

0 

0 

0 

Mountain View School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

383.8 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Voorhis Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.