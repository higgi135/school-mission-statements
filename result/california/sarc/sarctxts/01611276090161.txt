Albany Middle School (AMS), a National Blue Ribbon School, is a school community dedicated to learning. The mission of providing a 
rich and rigorous standardsbased curriculum is guided by our vision statement: In a safe, engaging environment, each member of the 
AMS community strives toward excellence, acceptance of differences, exploration of ideas and responsibility to a larger world. At 
AMS, everyone teaches, everyone learns. 
 
We are proud of our recent academic achievement as represented by our local assessments as well as the Smarter Balanced 
Assessment Consortium (SBAC) results. We continue to work on strengthening our use of essential standards and formative 
assessments toward the purpose of all students achieving at high levels. We are also continuing our work of integrating restorative 
practices and restorative justice into our behavior matrix and practices in an effort to impact behavior, increase feelings of belonging, 
and reduce suspensions. 
 
Our school places a strong value on school climate, and we will continue to build on the strong school climate programs we already 
have, both setting foundational skills and responding to concerns as they arise. We also have been working at improving our school 
climate from the perspective of equity by adding opportunities for connectivity for populations who are smaller in numbers and/or 
who we are not serving as successfully. Additionally, we have been focusing on an integration of restorative practices into our practices 
and responses to behavior.