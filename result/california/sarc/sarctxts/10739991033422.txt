ALL ENTERPRISE HIGH STUDENTS WILL BE SUCCESSFULLY INSTRUCTED, ACQUIRE KNOWLEDGE, 
AND PRODUCE GRADE LEVEL WORK . 
 
A Message from the Principal: 
The Alternative Education department provides a variety of opportunities in school placement, full 
or partial-day scheduling, and focus on core subject areas. Often students come to Enterprise High 
School with predetermined attitudes toward education, attendance, and personal conduct. It is the 
mission of each staff member to assist in guiding and redirecting students to academic 
achievement, good attendance, accepting responsibility for personal choices and graduation. 
 
Enterprise High School presents the same curriculum as Kerman High School. The curriculum has 
been adapted to a direct standards skill instruction format. Students have the opportunity to focus 
on core subject areas and prepare for the required high school exams, along with making up needed 
credits through augmented studies. 
 
Enterprise High teachers diligently align assignments to the state curriculum standards and state 
standards. We view the students' skill levels and the state standards as the blueprint upon which 
we build our lessons and assignments. 
 
Major Achievements: 

In 2017-18 Enterprise High School graduated 32 students 

• 
• Student behavior and attendance continues to be a focus for improved student 

character skills and academic achievement. 

• Enterprise High School students have access to ROP courses at KHS 
• Teachers are collaborating on aligning the curriculum with state standards, common 

• 

• 

core and administering benchmark tests. 
EHS has a positive partnership with Kerman Police Department, Fresno County TIP 
Program and Eminence Health Care. 

In spring 2014 Enterprise High School was granted a six-year accreditation from WASC 
through June 30, 2020. The Mid-Cycle Visiting Committee visited Enterprise High School 
on October 7, 2016. The Committee found that the school had done due diligence in 
our follow-up process and made expected progress on each of the four areas. The 
Visiting Committee found no new concerns at Enterprise High School. 

 
Focus for Improvement: 
Teachers receive staff development in the following areas: effective instructional strategies for 
working with English Learners; the process for second language acquisition; and implementing core 
curriculum pacing guides and quarterly benchmark assessments. 
 
Teachers also attend state standards training: training to implement the new state standards, along 
with continued Professional Development training on-site and at Kerman High School with 
department peers. Training is also provided to improve English Learner skills in English/Language 
Arts and Math. 
 
The student assessment platform helps administer assessment programs and gives immediate 
performance data. 

2017-18 School Accountability Report Card for Enterprise High School 

Page 1 of 9