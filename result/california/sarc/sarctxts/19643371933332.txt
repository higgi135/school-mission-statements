Monterey High School is the continuation high school for Burbank Unified School District. It is an alternative education program for 
students over the age of 16 years who are typically behind in credits toward graduation or need a smaller more personal learning 
environment. It is the belief of the Monterey High School staff that all students have value and are entitled to a Second Chance for 
Success. The goals of Monterey High School are 1) to assist students in attaining the credits needed for graduation and 2) to develop 
academic, social, and life skills which students can take in to their future and assist them in becoming life long learners and productive 
citizens. Monterey High School offers both contract based and traditional instructional setting classes. Additionally, the program offers 
a parenting program for pregnant teens and parenting teens in which the students complete their requirements for high school 
graduation and has childcare provided on campus,allowing teen parents the opportunity to learn age appropriate parenting skills. The 
school offers students a variety of electives which include agriculture, nutrition, psychology, criminal justice, just to name a few. The 
average school population at any one time is 200 students. Class size averages 20 students per teacher, allowing students to receive 
one-on-one and small group instruction. Other academic counseling, discipline and guidance services are provided through the 
principal and assistant principal, a school psychologist, and community based counselors on campus. Academic support is provided by 
a tutor and at-risk interventionist. The goal of Monterey High School is for students to become positive productive young adults able 
to pursue their additional education or vocational skills, find a job and become a responsible citizen in our community.