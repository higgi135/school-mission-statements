The Oak Grove Union Elementary School District Vision 
 
The Oak Grove Union School District, in partnership with our community, creates a challenging, safe, and caring learning environment 
for each student. 
 
We are committed to: 

 

• 
• 
• 
• 
• 

¦ Academic Excellence 
¦ Engagement with the Arts 
¦ Development of Life Skills 
¦ Celebration of Diversity 
¦ Stewardship of the Environment 

 
We have a vision of the Oak Grove Union Graduate which is three-fold: students excel academically, are well-rounded and balanced 
individuals, and are engaged members of the community. 
 
In the Oak Grove Union Elementary School District, we strive to provide a safe, caring and challenging learning environment for every 
student. We are committed to developing outstanding children who are respectful, responsible and have a positive attitude. In 
partnership with our parents, community and educational foundation, we create schools that support each child and allow them to 
reach their full potential. 
 
Oak Grove Elementary School is a K-5 charter school located above the town of Graton. Willowside Middle School is a 6-8 grade charter 
school located on the western edge of Santa Rosa and together with Oak Grove Elementary they make up the Oak Grove 
Elementary/Willowside Middle Charter School. The school is renowned for its caring and nurturing environment as well as its award 
winning fine arts and environmental education program. Student academic achievement is a hallmark of the school with students 
consistently performing above the state and county averages in the Smarter Balanced Assessments in English Language Arts and 
Mathematics as well as the California Standards Test in Science, all of which are part of the California Assessment of Student 
Performance and Progress (CAASPP).