Principal’s Message: 
Silverado High School continues to be committed to providing students with the best possible educational experience. This is 
demonstrated through greatness in and out of the classroom. I encourage everyone in the Silverado community to get involved in and 
with their school. Every day there are amazing things going on at Silverado High School, and we encourage you to share them with us. 
Silverado continues to focus on good first instruction, positive behavior rewards and interventions, and instructional interventions to 
ensure academic success for all students. As a large, public comprehensive high school, we are fortunate to offer a wide variety of 
clubs, activities and athletics designed to meet the unique needs and interests of our student body. As a school, we continue to 
demonstrate excellence in all areas - in and out of the classroom. 
 
For more information about Silverado High School, visit the official Silverado Web site at www.vvuhsd.org. 
 
MISSION STATEMENT: 
Silverado High School is committed to providing a safe environment with staff and students working together to reach their individual 
potential. The dignity and heritage of each person will be affirmed and respected. Our school will promote the development of 
responsible and successful community members with the capacity for leadership in a democratic society. 
 
SCHOOLWIDE LEARNER OUTCOMES (SLOs): 
Students graduating from Silverado High School will . . . 
 
Have effective communication skills: 

• Write with clarity 
• Read with comprehension 
• 
• 

Listen actively and efficiently 
Speak articulately and coherently 

Access information efficiently: 

• Evaluate sources of information 
• Distinguish fact from opinion 
• 

Integrate appropriate technology 

Work independently and cooperatively: 

• Value and implement the strength of diversity 
• Pursue life-long personal and academic growth 
• Accept personal responsibility for actions 

Know how to solve problems: 

• Use creativity and imagination 
Integrate critical thinking skills 
• 
• Make proper connections 
• Apply knowledge and skills to life 

Support the community: 

• Practice good citizenship 
• Utilize effective employability skills 
• Promote health and safety awareness 

2017-18 School Accountability Report Card for Silverado High School 

Page 2 of 14 

 

MASCOT: 
HAWKS 
 
SCHOOL COLORS: 
NAVY BLUE & SILVER