Principal's Message 
Welcome to Liberty Elementary School, home of the Lions! Our Liberty staff continues to work effectively toward a community of 
excellence. In 2016-17, our staff worked diligently to create standards-based report cards that align with the California State Standards 
(formerly known as Common Core State Standards). These standards were mandated by the state in 2014-15. Our staff has continued 
to focus on increasing rigor in the classrooms to make sure our students are developing the 21st century skills needed to be college 
and career ready. Our goal in 2018-19 is to continue to increase our students’ depth of knowledge through collaboration, creativity, 
communication and critical thinking. We have continued with Project Based Learning in grades TK-8 to encourage students to take 
ownership of their learning and implemented a new writing program. We will continue our collaboration between the community, 
staff and student body and are committed to maximizing student achievement, improving communication, promoting Character 
Counts!, and ensuring a safe school environment. 
 
Parents, we need your help, too! Research shows that high parental expectations and parental involvement positively affect student 
achievement. Please become involved in your child’s education. We welcome parents 
in classrooms as volunteers, and we regularly hold school activities that need parent participation. We understand that your time is 
precious, so please remember that parent involvement can also be as simple as 
checking your child’s homework each night or reading a book with your child. 
 
We are dedicated to student success, and as a community, we will reach our goals. We look forward to working with parents and 
children to provide a rewarding and productive environment each school year. Please join us as we continue to build a community of 
excellence.