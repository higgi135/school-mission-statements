Superintendent’s messageLEUSD is well positioned for the 2018 school year! The collaboration 
between voters, parents, teachers and staff has resulted in student achievement growth, improved 
facilities and playing fields under Measure V, and new 
instructional technology for 
classrooms.Under our state accountability system, the Fall 2017 update to the California School 
Dashboard was recently released. The Dashboard provides teachers and principals with valuable 
performance data that is examined weekly during their PLC collaboration time, and used to guide 
instruction. The current Dashboard shows LEUSD schools are making progress.LEUSD improved in 
several areas, though English Language Arts and Math growth indicators are little changed from a 
year ago, a trend statewide. The Dashboard is powered by six state indicators and four local 
indicators, along with a variety of custom reports by which to compare schools, districts, and 
student subgroups. Dashboard color codes reflect status and change to depict achievement growth 
ranging from Red (lowest) to Blue (highest).LEUSD performance highlights:SUSPENSION RATES—by 
lowering suspensions, results for the 'Suspension' indicator have IMPROVED from orange to yellow, 
changing from a high suspension rating to a medium suspension rating.‘EL’ PROGRESS—English 
Learners IMPROVED from yellow to green, changing from 'Medium' to 'High' as a result of an 
additional 3.1% students making progress towards English proficiency.GRADUATION RATE—this 
indicator has IMPROVED from green to blue. The District continues to have a 'High' rating due to 
an increase in graduating students of 1.5%.COLLEGE/CAREER PREPAREDNESS—growing College & 
Career Preparedness is an area for increased attention. The CA School Dashboard shows 35.2% of 
LEUSD graduates as being 'Prepared.” The State will not have a color indicator for College & Career 
Preparedness until 2018, but notably, LEUSD 11th grade students' ELA and Mathematics scaled 
scores increased in both areas respectively by 0.3 points and 4.2 points, a positive college readiness 
indicator.CHRONIC ABSENTEEISM—for the first time, the CA Schools Dashboard includes District 
and school Chronic Absenteeism rates, though a Chronic Absenteeism color indicator does not 
appear on the Fall 2017 report. District wide, LEUSD’s Chronically Absent statistic is 12.8%.LEUSD 
met all local indicators for implementing state standards, providing safe school facilities, adequate 
books and instructional materials, as well as meeting indicators for school climate, and student and 
parent engagement. View how LEUSD is performing at www.caschooldashboard.org.These are 
positive indicators, so let’s be mindful of the many positive accomplishments of 2017 to help set 
the bar high for 2018.Sincerely,Dr. Doug Kimberly,Superintendent 
 
Principal's Message 
At Canyon Lake Middle School (CLMS) we feel it is extremely important to meet the diverse needs 
of all students. We are currently implementing the following programs: Gifted and Talented 
Education (GATE), English Language Development (ELD), Special Education for students with 
learning disabilities, and various Intervention Programs (IP) designed to assist students in meeting 
grade-level standards. In addition, we are proud to offer students the opportunity to participate in 
the Avenues Via Individual Determination (AVID) program, which is a high-quality college 
preparation program. All of our student programs are consistently evaluated to identify ways to 
further develop and improve them. 
 
In addition to our strong academic programs, we also offer band, choir, drama, dance, cheer, art, 
and computer technology that further enrich the academic experience of all students. Leadership 
opportunities also exist for students through ASB, PLUS, and club activities. Our PTSA is also very 
active at our site. We have dances, events, and field trips throughout the year. We welcome all to 
become a member of the Canyon Lake Family!

2017-18 School Accountability Report Card for Canyon Lake Middle School 

Page 1 of 11 

 

 
Our Belief Statement 
The Canyon Lake Middle School culture holds that all students are unique individuals and can learn. Our collaborative staff is committed 
to caring and valuing all members of our community while supporting student achievement. We will create and maintain a rich, rigorous, 
and evolving curriculum through continuous professional development. Our students will progress beyond comprehension of basic 
concepts, enabling them to synthesize and apply their learning to new situations. 
 
Vision Statement 
All Canyon Lake Middle School Community Members: 

• Work Collaboratively with a Respectful, Positive Attitude to Observe, Share, and Cooperate 
• All members of our community will participate in a collaborative group 
• 
• Meeting dates/times are scheduled on a school year calendar 

Teachers will participate in horizontal and vertical groups 

 
Engage in Active and Reciprocal Teaching/Learning Experiences to Continually Examine, Reflect, Refine, and Grow 

• 
• 
• 
• 

Plan, scope and sequence w/ common assessments and benchmark testing 
Share student results identifying strengths and weaknesses 
Continue to enhance instructional practices with the increased use of instructional technology. 
Schedule observations to share strategies with collaborative group members 

 
Develop Support Programs and Interventions to Enhance Learning 

Continuous efforts to improve intervention programs to provide support for students who struggle academically 

• 
• Administrative support to improve the learning of all community members 

 
Foster Responsibility and Contribution of All Members by Providing Leadership Roles to Instill Self-Worth and Value 

• 
• 

Increase the number of leadership positions held by members of our school community 
Increase the opportunity for parent engagement and involvement in the school community