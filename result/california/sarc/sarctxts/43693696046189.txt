Cureton School has been providing a quality education to our community for over fifty years. Our teachers are dedicated to providing 
each and every student with the best educational opportunities possible. Our staff recognizes the importance of developing, in each 
student, an attitude of self-respect, self-worth, self-confidence, and a desire to be lifelong learners. 
 
Consequently, the teachers and I do our very best to create an exciting and challenging learning environment for all of our students. 
 
We are a Visual and Performing Arts focused school, with students participating in choir, band, and art related instruction to enhance 
the core curricular areas. We have strengthened our two small learning communities at Cureton. Discovery small learning community 
is comprised of students in grades K-2, and Endeavor small learning community is comprised of students in grades 3-5. The small 
learning communities allow us to personalize education and strengthen the meaningful relationships we seek to develop with all 
students. 
 
Cureton has continued its partnership with City Year. City Year is comprised of 6-10 corps members who provide student support from 
8:00am-6:00pm each school day, including daily tutoring and mentoring for intensive students, leading academic clubs during recess 
and lunch periods, and supervising after-school activities for 120 students. We are also partnering with Reading Partners to provide 
academic tutoring for intensive and strategic students. 
 
In addition, Cureton has welcomed the Little Heroes program which continues to provide us with a coach who offers recess, lunchtime 
and physical education activities for all students, and a before school recess for all students who arrive early. 
 
Cureton staff strongly believes that parents are very important members of the school community. As a result, one of our continual 
goals is to make Cureton Elementary School a significant part of your family’s life. To be the most effective we can be, we need to 
work together to create a positive learning environment for all our students. Therefore, we encourage you to become active and 
informed. This may be done by attending meetings of the School Site Council or the English Learners Advisory Committee, joining the 
Parent Teacher Association, volunteering to assist in the classroom, attending as many school programs and events as possible, and 
by keeping close communication with your child’s teacher. Maintaining this type of parent participation and communication will play 
a vital role in your child’s success. We are also happy to be developing a parent participation program and will be working with all 
families to assist them in completing 30 hours of parent participation hours. 
 
Thank you in advance for your support. We welcome your suggestions, and we will solicit your help throughout the school year. If you 
need additional information or assistance, please do not hesitate to contact us at (408)928-7350. 
 
 
 

2017-18 School Accountability Report Card for Horace Cureton Elementary School 

Page 2 of 12