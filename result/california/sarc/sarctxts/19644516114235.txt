Welcome to Old River Elementary School, where we are educating today’s children to successfully 
navigate the future. This document will provide you with specific data and information about our 
school, including but not limited to academic standings, curriculum and instruction, school facilities 
and safety, budget, and facility enhancement. 
 
As you read our School Accountability Report Card, you will see a school that ensures every child 
has equal access to a rigorous core curriculum in language arts, mathematics, science, and social 
studies. The hard-working staff is both skilled and dedicated to the success of our students. The 
parents and the community are very supportive. 
 
At Old River School we believe that all students can learn. Every student will have access to a 
challenging core curriculum. We strive to give all students a positive self-image, respect for others, 
and enthusiasm for lifelong learning. Students will achieve success with the support of parents, 
staff, and the community. 
 
Our goal in presenting you with the information in this report card is to keep our community well 
informed. If you have any questions or are interested in making an appointment to discuss this 
report, please call our school. 
 
Caryn Jasich, PRINCIPAL 
 
 

 

 

----

--

-- 

Downey Unified School District 

11627 Brookshire Ave. 
Downey, CA 90241-7017 

(562) 469-6500 
www.dusd.net 

 

District Governing Board 

Tod M. Corrin 

Donald E. LaPlante 

D. Mark Morris 

Giovanna Perez-Saab 

Barbara R. Samperi 

Martha E. Sodetani 

Nancy A. Swenson 

 

District Administration 

John A. Garcia, Jr., Ph.D. 

Superintendent 

Christina Aragon 

Associate Superintendent, Business 

Services 

Roger Brossmer 

Assistant Superintendent, 

Educational Services - Secondary 

Wayne Shannon 

Assistant Superintendent, 

Educational Services - Elementary 

Rena Thompson, Ed.D. 

Assistant Superintendent, 

Certificated Human Resources 

Veronica Lizardi 

Director, Instructional Support 

Programs 

Marian Reynolds 

Administrator, Student Services 

Jennifer Robbins 

Director, Elementary Education 

Patricia Sandoval, Ed.D. 

Director, Special Education 

 

2017-18 School Accountability Report Card for Old River Elementary School 

Page 1 of 8