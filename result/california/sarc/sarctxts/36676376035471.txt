Welcome to Fallsvale School’s Annual School Accountability Report Card. The purpose of this document is to provide parents with 
information about our school’s instructional programs, academic achievement, textbooks, safety procedures, facilities, and 
professional staff. Understanding our educational program will help our families and community join our efforts in providing an 
outstanding learning experience for Fallsvale’s students. 
 
We have made a commitment to provide the best educational program possible for our students. The excellent quality of our program 
is a reflection of our highly committed staff. We are dedicated to ensuring that our school provides a warm yet stimulating 
environment where students are actively involved in learning academics as well as positive values. 
 
Our goal in presenting you with this information is to keep our community, and the public, well informed. We desire to keep the lines 
of communication open and welcome any suggestions, comments, or questions you may have. Thank you for helping Fallsvale School 
be the outstanding school that it is. 
 
Fallsvale School, located in Forest Falls, provides instruction for students in grades K-8. During the 2017-18 school year, a total of 83 students 
were enrolled. The school’s enrollment for 2017-18 was comprised of 73.4% students who qualified for free and reduced meals, and 4.81% 
students who qualified for special education services, and 0% English Learners. Currently, for the 2018-19 school year, a total of 74 
students are enrolled and is comprised of 55% students qualifying for free and reduced lunch, 11% students qualifying for special 
education services, and 0% English Learners.