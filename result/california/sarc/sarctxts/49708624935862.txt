San Antonio High School (SAHS) is a continuation high school for students who need a more individualized program to complete high 
school credits. Students receive daily instruction in classes with fewer than 20 students. Additionally, students have access to an 
academic counselor, a marriage and family therapy (MFT) counselor, and a very supportive staff, while they attend and complete their 
graduation goal. Students can also access the San Antonio satellite office of the Petaluma Health Center for medical concerns. San 
Antonio High School is accredited by the Western Association of Schools and Colleges (WASC), with our most current accreditation 
occurring in 2016. 
 
San Antonio High School provides a positive alternative education for students who have not found success in other settings. We 
believe that our students, regardless of past experiences, possess the potential for improved life skills, increased academic 
performance, and enhanced awareness and self-esteem. We believe every student can succeed when they begin to see themselves 
as valuable, productive members of society. We endeavor, as a school community, to be a place where students are inspired, and 
supported, to pursue their passions and creativity. 
 
Our special focus is a schoolwide approach that uses current standards-based instruction while supporting our students social and 
emotional growth with mentorships, school wide activities and team building exercises. We continue to work on close reading, 
analysis, and interpretation of a variety of media to help students develop effective writing and speaking skills. We foster collaboration 
through group work and presentations that illustrate their thinking and learning. In short, we strive to engage the whole student so 
that are positioned to take their place in the world as successful members of society.