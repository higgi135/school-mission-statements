Golden View’s dedicated staff and supportive community are committed to meeting students’ individual needs in a positive and 
nurturing environment while providing a high quality educational program that incorporates collaboration, communication, creativity, 
critical thinking, and technology to prepare our students to be successful in the 21st century. 
 
Students also recite the following statement with the morning announcements on a daily basis: “Golden View is a community of good 
citizens working together and achieving our goals. We are respectful, responsible and safe in all that we do and say.” 
 
In addition to an aviary, Golden View is home to the Student Environmental Learning Facility (SELF) or otherwise referred to as our 
farm. This two-acre fenced area of our campus has farm animals (sheep, goats, chickens, ducks, geese, and turtles). Golden View also 
has a garden that is shared by all classrooms with over 800 square feet of community garden space. This unique facility provides our 
students with exceptional opportunities for hands-on learning in taking care of animals, planting seeds according to seasons, 
observing, maintaining, and cooking with the harvest. Students and teachers utilize the SELF extensively to integrate environmental 
learning across all curricular areas while promoting social skills. Students have opportunities for after school nature programs, as well 
as participation in composting and recycling programs across the grade levels. The SELF is a beloved part of our students’ elementary 
school experience and families are encouraged to get involved. 
 
District & School Profile 
Golden View Elementary School is located in the western region of Huntington Beach and serves students in grades kindergarten 
through five following a traditional calendar. Golden View also has two Early Kindergarten classes that run from January to June. The 
Ocean View School District is located in western Orange County and serves over 8,000 students from Pre-kindergarten through eighth 
grade. Ocean View School District is dedicated to educational excellence and the continuous academic growth of all students, which 
supports its motto: “Encouraging a Deliberate and Global Education.”