Navelenica Middle School serves serves approximately 385 students in grades 6-8. The school is 
located at the foot of Mount Campbell in suburban Reedley, ten miles northeast of the Kings 
Canyon Unified School District Office. Navelencia enjoys a 13 acre park like setting and is nestled in 
an agricultural setting near Highway 180 and nearby Kings Canyon National Park in rural Fresno 
County. The beautiful campus is surrounded by large open grassy play fields, a natural 
amphitheater and is surrounded by locally owned orange groves, almond orchards, and vineyards. 
Each day, the school enjoys a view of the Sequoia/Kings Canyon National Park. More than half of 
the parents of the students are employed by agricultural companies as seasonal farm workers. 
 
Our staff is dedicated to ensuring the academic success of every student and providing a safe and 
productive learning environment for all. The school holds high expectations for students and staff 
in efforts to ensure the academic and social development of all students. Curriculum, instruction, 
assessment and professional development are focused on assisting students in mastering the 
common core standards, as well as increasing the overall student achievement of all student 
subgroup. It is one of three middle schools in Kings Canyon Unified School District. Other middle 
school students are served in four K-8 schools. 
 
Navelencia Middle School Mission Statement: 
Navelencia staff will provide rigorous, meaningful, comprehensive educational services, integrating 
technology throughout the curriculum, providing intervention as needed and challenging students 
to achieve at high levels. Our students will participate, by being fully engaged and working to or 
beyond their potential, in areas of academics, citizenship and co-curricular activities. In partnership 
with parents and community, Navelencia Middle School will provide various extra-curricular 
activities, foster value in education, and promote individual achievement thus creating well 
rounded individuals. 
 
Expectations 
Be Respectful! Be Responsible! Be Safe! 
 
Motto 
Today a Patriot. Tomorrow a Leader 
 

 

 

----

-

--- 

----

---- 

Kings Canyon Joint Unified 

School District 
1801 10th Street 
Reedley, CA 93654 

559.305.7010 

www.kcusd.com 

 

District Governing Board 

Craig Cooper 

Robin Tyler 

Manuel Ferreira 

Noel Remick 

Sarah Rola 

Clotilda Mora 

Jim Mulligan III 

 

District Administration 

John Campbell 
Superintendent 

Roberto Gutierrez 

Deputy Superintendent, Human 

Resources 

Monica Benner 

Assistant Superintendent, 
Curriculum and Instruction 

Mary Ann Carousso 

Administrator, Student Services 

Jose Guzman 

Administrator, Educational 

Programs 

Adele Nikkel 

Chief Financial Officer 

 

2017-18 School Accountability Report Card for Navelencia Middle School 

Page 1 of 9