School Profile 
Albert Michelson Elementary School is a TK-5 elementary campus located in the California Gold Rush town of Murphys, California. The 
school features a rigorous academic program with robust opportunities to explore interactive curriculum in language arts, math, 
science, technology, art, and music. Albert Michelson Elementary is unique in demographic characteristics bridging learning 
opportunities for students from a range of life experiences and backgrounds including English Language Learners and an included 
Special Education population. Last year, 105 of the 223 enrolled students qualified for Free/Reduced lunch. As a resuslt of a 2018 
Title 1 Funding audit, the school has reidentified itself under the Title 1 School-Wide designation and taken proceedural steps to 
register under the Title 1 school-wide criteria based on the percentage of low income students attending school. Also, as part of an 
organizational review, the school completed the arduous Western Association of Schools and Colleges (WASC) Initial Site visit 
application. The process included a comprehensive review of current programs and strategies with the report outcome identifiying 
areas of excellence and target areas for future growth. The WASC team will visit in Spring 2019. 
 
Michelson students are empowered and expected to take responsibility to master core curriculum and to perform to the best of their 
ability. Intrinsic motivation is fostered in all students to achieve. All learners work to demonstrate proficiency through the use of 
frequent multiple measures and school-wide initiatives to support student learning and success, including iReady online assessments, 
Star Reading Assessments, and progress-monitoring in English Language Arts and Math. Systematic Instruction in Phonological 
Awareness, Phonics, and Sight words (SIPPS) is used in early literacy learning. A hallmark of the school's instruction is the Universal 
Access Language Lab which rotates students throughout classroom settings providing differentiated, small group instruction four days 
each week. In addition, Fountas and Pinnell Guided Reading and the use of running records to monitor student progress is used to 
support developing literacy proficiency. Barton Reading is being introduced to select students in 2018/19 for those not making 
adequate progress with other reading interventions. 
 
Grades TK-4 have embraced the English Language Arts curriculum, "Center for Collaborative Classroom", with an emphasis on rigor, 
integration of cross-curricular subjects including social emotional learning, literacy, and writing. Small group work within the general 
education classroom for all students is an expected component of this program. In 2018, Grade 5 launched the "Journeys" Language 
Arts program, rich in rigor with flexible literacy instruction with digital tools to reach all learners. The school also embraces the Second 
Step program taught to all grades to develop a tool kit for appropriate social behaviors. 
 
Community collaboration between the school, Michelson Parent Club, businesses, and community organizations such as Rotary, 
Murphy’s Senior Center, and the retired Veterans, provide unique opportunities for the students at Michelson School. Programs 
include the Master Gardeners Club, 4th and 5th grade Ukulele seminar, ceramics, fine art painting, Homework Club (English and ELL), 
Reading Buddies, Student Leadership and an embrace of inclusion for all learners in general education settings. Our school community 
includes a diverse population that includes long-time local families, as well as families who have recently relocated from more 
suburban areas. The school enjoys the support of a robust English Language Advisory Council featuring regular attendance from the 
Spanish-speaking parent community. In 2018, Michelson began offering adult English Classes twice a week in connection with the 
Calaveras County Office of Education. Parents play very important roles through their active participation and involvement as small 
group reading volunteers, School Site Council, ski program, chaperoning for field trips, coaching, Parents Club, community garden, and 
tutoring. 
 
Michelson installed two new play structures in the summer of 2018 to promote play and fitness on safe structures and a complete 
window and energy management thermostat replacement will be completed in 2018. Measure E, a general obligation bond, is on the 
November 2018 ballot to fund additional infrastructure improvements. 
 
The staff at Albert Michelson embrace the mantra of "Innovate to Educate" and provide all learners an individual, standards-driven, 
rigorous education. 
 

2017-18 School Accountability Report Card for Albert Michelson Elementary School 

Page 2 of 11 

 

Vision: 
At Albert Michelson Elementary School we maintain the expectation that all learners will achieve essential standards, and become 
responsible, respectful, life-long learners. 
 
Mission: 
Albert Michelson Elementary is a community of learners that expects all learners to become proficient in Common Core State 
Standards (CCSS), actively engaged in learning by utilizing 21st century learning skills, and to be able to apply this knowledge to new 
situations through highly effective instruction and learning opportunities. 
 
Learners at Albert Michelson have the responsibility to master core curriculum and to perform at high levels. All learners will 
demonstrate proficiency in essential standards through the use of frequent multiple measures of assessments including Star Reading 
and Math Assessment, summative curriculum based measures, and the Smarter Balanced Assessment (SBAC). 
 
Michelson is a school that continually monitors data to inform instruction to improve student outcomes though a collaborative 
teaching process. Our learners are enthusiastic and participate in a wide-variety of learning activities. Learners will be offered 
extension activates and support designed to meet individual learners’ needs and to help all students fulfill their potential.