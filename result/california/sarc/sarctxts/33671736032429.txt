Bella Vista Elementary School is dedicated to creating a college and career-minded community of 
successful citizens that embrace academic excellence, individual strengths, and differences, 
personal growth, and respect for self and others in an ever-changing global system. 
 
The staff of Bella Vista Elementary School affirms that all students have the ability to learn and will 
demonstrate achievement. Our school's purpose is to educate all students and foster positive 
growth in social/emotional behaviors and attitudes through a well-rounded, learning-centered, and 
child-centered environment. We strive to motivate students to attain their maximum educational 
potential in all areas of learning by providing a learning environment that is supported by a rich and 
challenging curriculum. Students have numerous opportunities to achieve personal and academic 
success. Bella Vista Elementary is a school that strives to engage students in a college-going 
atmosphere. We encourage the Bella Vista learning community of parents, students, and staff to 
work as a team in order to pursue this mission. 
 
Bella Vista Elementary School is located in Desert Hot Springs, a community twelve miles north of 
Palm Springs. As one of 27 schools in the Palm Springs Unified School District, Bella Vista Elementary 
School serves approximately 750 students in Transitional Kindergarten through fifth grade. 
 
Bella Vista works cooperatively with Palm Springs Unified School District to develop regulations and 
procedures that align with the Educational Code and California Department of Education 
guidelines. 
 
 

----

---

- 

Palm Springs Unified School 

District 

150 District Center Way 
Palm Springs, CA 92264 

(760) 883-2700 
www.psusd.us 

 

District Governing Board 

Richard Clapp, President 

John Gerardi, Clerk 

Karen Cornett, Member 

Madonna Gerrell, Member 

Timothy S. Wood, Member 

 

District Administration 

Sandra Lyon, Ed.D 
Superintendent 

Michael Swize, Ed.D 

Assistant Superintendent, 

Educational Services 

 

Tony Signoret, Ed.D 

Assistant Superintendent, 

Human Resources 

 

Brian Murray, Ed.D 

Assistant Superintendent, 

Business Services 

 

 

2017-18 School Accountability Report Card for Bella Vista Elementary 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Bella Vista Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

35 

35 

33 

0 

0 

1 

0 

1 

0 

Palm Springs Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1038 

18 

34 

Teacher Misassignments and Vacant Teacher Positions at this School 

Bella Vista Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.