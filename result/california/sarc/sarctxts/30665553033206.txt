Laguna Beach High School is part of the Laguna Beach community. Sloping down the coastal hills to 
the edge of the Pacific Ocean, Laguna Beach is known for its dramatic contrast of mountains and 
beaches. A center for fine arts, Laguna Beach’s culture is as unique as its landscape. 
 
The Laguna Beach Unified School District serves students in grades kindergarten through 12 by 
providing comprehensive educational programs through its two elementary schools, a middle 
school, and a high school. The District is proud of the high level of community support it receives 
and the commitment the community demonstrates towards its students. The Laguna Beach 
Education Foundation (SchoolPower) was created by the community to provide additional funds to 
the District to support school programs and activities. Laguna Beach High School serves students in 
grades nine through twelve on a traditional school calendar. The mission of Laguna Beach High 
School is to maximize learning for every student in a supportive and caring environment to ensure 
that, upon graduation, all students are ready for college, career and global citizenship. Our school 
has been recognized at the state and federal levels as a California Distinguished School and a 
National Blue Ribbon School. In 2018-2019, 1068 students were enrolled at the school. The student 
population consisted of 10% socioeconomically disadvantaged, 2% English learners, and 12.4% 
students with disabilities. LBHS student population is 11% Hispanic, 4.9% Asian, 76.9% white, 5.2% 
two or more races, and 1% other races. The LBHS team of educators aims to provide relevant 
learning experiences to all students during their time here. 

 

 

----

---- 

--

---

--- 

Laguna Beach Unified School 

District 

550 Blumont Street 

Laguna Beach, CA 92651 

(949) 497-7700 
www.lbusd.org 

 

District Governing Board 

Jan Vickers 

Dee Perry 

Carol Normandin-Parker 

James Kelly 

Peggy Wolff 

 

District Administration 

Jason Viloria, Ed.D. 

Superintendent 

Alysia Odipo, Ed.D. 

Assistant Superintendent, 

Instructional Services 

Leisa Winston 

Assistant Superintendent, Human 

Resources and Public 

Communications 

Irene White 

Director of Special Education 

Mike Morrison 

Chief Technology Officer 

Jeff Dixon 

Assistant Superintendent, Business 

Services 

Chad Mabery, Ed.D. 

Director, Assessment and 

Accountability 

Michael Keller, Ed.D. 

Director, Social Emotional Support 

 

2017-18 School Accountability Report Card for Laguna Beach High School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Laguna Beach High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

54 

53 

53 

0 

0 

0 

0 

0 

0 

Laguna Beach Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

156 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Laguna Beach High School 

16-17 

17-18 

18-19