West Fresno Middle School is located in the heart of California's Central Valley in Fresno and serves about 370 students grades 6-8. 
West Fresno Middle School teachers and staff are dedicated to ensuring the academic success of every student and providing a safe 
and productive learning experience. 
 
Our mission is to provide a positive environment that utilizes assessment driven instruction to develop critical thinkers and ensure 
student engagement and mastery in grade level standards for a successful future. We will continue offering numerous opportunities 
for our students to advance academically. In addition, we are committed to providing our students the necessary social skills to become 
caring, respectful, and motivated members of our society. West Fresno Middle School also embraces the district goals of providing 
the highest level of student achievement, a safe learning environment, and effective and efficient operations. Our vision is to provide 
students with multiple opportunities to demonstrate learning and support their learning through collaborative teams. We are a 
Professional Learning Community that promotes effective teaching practices across all curriculum and implementation of school wide 
technology infused methods for checking student understanding. Our teachers meet weekly to review bi-weekly assessment data and 
the results are utilized to target deficiency areas. This process ensures consistent monitoring and change to meet the needs of our 
students. To further support these areas, we have implemented the most recent and effective state adopted curriculum.