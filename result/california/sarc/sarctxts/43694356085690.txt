LeyVa Middle School, a California Gold Ribbon School, is a school with a rich and diverse history 
with a focus on the future. Since the doors opened in 1973, LeyVa has stood for excellence in 
academics and athletics. 
 
Our mission is to have each students will be inspired to reach their full academic and social 
potential. We will use the strengths of each individual to build an inclusive and collaborative school 
campus for students, parents, and staff. 
 
No longer can we measure aptitude by the amount of facts we remember, but instead we do so by 
the students' ability to use information to communicate, collaborate, problem-solve, and 
contribute new ideas or creations. By teaching 21st Century Skills through a culture that empowers, 
technology that enables, and curriculum that engages, we will enhance learning. LeyVa Middle 
School proudly offers parents and students two instructional choices. Whether you chose Bulldog 
Tech's project-based integrated courses or the comprehensive traditional journey, we promise to 
give your child the most engaging and excellent education — an education to serve him or her in 
the 21st Century where we all must communicate effectively, think critically, collaborate, and be 
creative problem solvers. 
 
Clubs, sports, leadership and and extensive elective choices have something for everyone. This 
includes our championship athletic program consists of cross county, basketball, volleyball, soccer, 
and track. Our teams are well known throughout the county for their excellence. When you enter 
our gym, the banners and awards speak for themselves. 
 
Evergreen School District, located in the City of San Jose, is comprised of fifteen elementary schools 
and three middle schools. Once a small farming city, San Jose became a magnet for suburban 
newcomers between the 1960s and the 1990s and is now the third largest city in California. The 
city is located in Silicon Valley at the southern end of the San Francisco Bay Area and is home to 
nearly one million residents. 
 
LeyVa Middle School is centrally located within the district’s borders. In the 2017-18 school year, 
the school served 756 students in grades seven and eight on a traditional calendar schedule. The 
chart displays school enrollment broken down by ethnicity. 

--

-- 

----
---- 
Evergreen Elementary School 

District 

3188 Quimby Road 
San Jose CA, 95148 

(408) 270-6800 
www.eesd.org 

 

District Governing Board 

Bonnie Mace, President 

Leila Welch, President Pro-Tem 

Jim Zito, Clerk 

Christopher Corpus, Trustee 

Marisa Hanson, Trustee 

 

District Administration 

Bob Nuñez 

Superintendent 

Bob Nuñez 

Interim Superintendent 

Dan Deguara 

Assistant Superintendent 

Delores Perly 

Chief Business Officer 

Carole Schmitt 

Director of Human Resources 

Denise Williams 

Director of Instruction 

Carole MacLean 

Director of Pupil Services 

Rick Navarro 

Director of Operations 

 

2017-18 School Accountability Report Card for George V. LeyVa Middle School 

Page 1 of 10