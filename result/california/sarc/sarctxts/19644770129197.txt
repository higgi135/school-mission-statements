Enterprise Elementary opened on August 11, 2014. It is the fifth school in Eastside Union School 
District. 
 
Vision - 
The Enterprise Elementary School Vision is to provide for all students to engage in the passion of 
learning while realizing personal success. In achieving this goal we take the following core concerns 
into consideration: Responsibility – We are responsible for a large part of the success of the 
students we teach; Relationship – We can not ensure personal and academic success in our 
students until we realize and achieve the need for positive adult relationships; Confidence – We 
have to build positive self-perceptions in order to grow students who can, and want to learn; 
Autonomy- We facilitate and guide students, but we understand that we must allow them to realize 
their own visions and work towards their individual envisioned futures. 
 
Enterprise Staff Why/Vision: Inspiring, empowering, guiding, and motivating for a better future. 
 
Leadership Team Why/Vision: We care to make a difference, to motivate and inspire children to 
reach their potential, and to impact their family legacy. 
 

----

---- 

Eastside Union Elementary 

School District 

45006 30th St. East 
Lancaster, CA 93535 

(661) 952-1200 

www.eastsideusd.org 

 

District Governing Board 

Mrs. Peggy Foster 

Mr. Joseph "Joe" Pincetich 

Mrs. Donna Martinez 

Ms. Doretta N. Thompson 

Mrs. Deborah L. Sims 

 

District Administration 

Dr. Joshua Lightle 
Superintendent 

Mr. Daryl Bell 

Assistant Superintendent of 

Human Resources 

Dr. Donna Smith 

Assistant Superintendent of Ed. 

Services 

Mr. Scott Lathrop 

Chief Business Officer 

Ms. Margo Deal 

Director of Student Services & 

Special Education 

 

2017-18 School Accountability Report Card for Enterprise Elementary School 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Enterprise Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

35 

32 

2 

0 

2 

0 

 

 

 

Eastside Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Enterprise Elementary School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

2 

0 

0 

0 

0 

 

 

 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.