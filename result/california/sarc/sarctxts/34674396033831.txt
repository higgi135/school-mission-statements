Caroline Wenzel School is a caring community of students, parents, and staff dedicated to 
educational excellence. Staff, parents, and students have worked to make Wenzel a place where 
students feel safe, where their voice is heard (Student Council, Peer Mediators), where they are 
recognized for citizenship, attendance, life skills and academic efforts and most of all a place to 
have FUN while learning! The culture and climate of Wenzel are built upon a frame of ethical 
leadership focused on the standards of commitment, duty, equity, integrity, ethical responsibility, 
and respect. 
 
Support services for continuous improvement in student achievement are provided to students. 
Class size is 24:1 in Kindergarten; 24:1 in 1st, 2nd and 3rd grades and 33;1 in grades 4-6. In addition 
to the regular K-6 classrooms, Wenzel has four special day classes for communicatively disabled 
students, a class for severely developmentally disabled students and an autistic transition class 
designed to transition autistic students into general education classes, as well as 18 full inclusion 
students and over 80 speech students, including students from private schools. Our special day 
classes are fully included in all school-wide events and programs. English Language Learners (ELL) 
program is available for qualified students in both an integrated and designated approach and is 
provided a number of opportunities throughout the day to have their curricular needs met. Caroline 
Wenzel provides a strong PE prep program. Caroline Wenzel is proud to also have a staffed library 
where students can check out books on a rotating basis. To add to our expanding services is a 
family resource support center, providing a variety of support to our school and families, including 
but not limited to mentoring and counseling. For the past two years, including this school year, 
Caroline Wenzel holds Saturday Academy to provide enriching learning opportunities for students. 
For the 2018-19 school year, Caroline Wenzel staff will engage in improvement science work as a 
method to bring improvement to teaching and learning. 
 
Parents and community volunteers are a very strong resource and help to implement the overall 
school program in cooperation with an active School Site Council and the English Learner Advisory 
Council. Wenzel’s dynamic PTSO supports many important programs including a Peer Mediator 
Program for Conflict Management, and annual events such as an Intermediate Physical Fitness 
Meet, and a Primary and Intermediate Play Days. Parents have been instrumental in implementing 
family events such as our Spaghetti Dinner, Carnival, Family Science Night and Movie Nights as well 
as ancillary activities like primary and intermediate school dances. There is our new Caroline 
Wenzel Academy of Scholars (CWAS) licensed childcare program available for before and after 
school childcare on our campus in addition to the After School Education and Safety (ASES) after 
school program serving nearly 86 students in grades 1st - 6th. Finally, we have an active student 
council that meets once a week with the principal to discuss school-wide student issues as well as 
plan student activities. This year, leadership opportunities provided students with opportunities to 
serve as technology team, school tour teams, and mentoring team. 
 
Parents, students, staff, and members of our community are all working together to continue 
Wenzel’s path to continuous improvement in academics as well as having fun learning in an 
inclusive, caring, and small community environment! 
 

2017-18 School Accountability Report Card for Caroline Wenzel Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Caroline Wenzel Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

9 

1 

0 

11 

13 

1 

0 

1 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.