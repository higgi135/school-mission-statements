Tomas Rivera Elementary School, home of the Coyotes, is located in the heart of the Orangecrest area of Riverside, California. One of 
the newer schools in the Riverside Unified School District, Rivera opened its doors on September 7, 1995 as the 28th elementary 
school. Our school is named after Tomas Rivera who was born into a family of migrant farm workers. At a young age, Tomas was 
introduced to the world of books by a librarian in Texas. He grew up to be a poet/writer, an educational leader, and eventually became 
the first Hispanic Chancellor in the University of California, Riverside. approximately 776 students in grades TK/K-6 is comprised 43% 
white; 40% Hispanic; 7% African/American; 5% Asian; and 5 % other ethnicities. Eighty-nine percent of our students speak English only 
and 5% or our students are Academic English Learners. In addition to our regular education student population, Rivera has a 
kindergarten/first grade and a first/second grade special education non-severely handicapped class as well as two special education 
preschool special day classes. Other special education services include two Speech and Language Pathologist, a one half time School 
Counselor and a one and one fourth Resource Specialist. All classrooms are equipped with wireless internet access. All second through 
sixth grade classrooms are equipped with iTree tools that include a Promethean interactive board, LCD projector, Document Camera, 
a teacher tablet PC, and student computers. TK/Kindergarten and first grade classrooms have LCD projectors, a document camera, 
teacher laptop, and student computers. Students participate in Accelerated Reader, Brain Pop, Dreambox which are standards-based 
computer programs. Students can access these programs in their classrooms, the school library, and at the Orangecrest Library. Rivera 
prides itself on being awarded California Distinguished School status in 2006. We attribute our success to our outstanding parents and 
students, and our professional and dedicated staff. Everyone at Rivera works on behalf of our students to ensure that they are safe 
and are receiving instruction that matches the rigor of the Common Core State Standards and the Smarter Balanced Assessment 
Consortium State Assessments. Teachers know every student by name and by need and monitor learning closely to ensure that each 
students' learning needs are identified and interventions are designed to close all learning gaps. All staff at Tomas Rivera Elementary 
School are proud to work in the field of education and take seriously the impact that we have on the future of our students. With that 
in mind, we do whatever it takes to ensure that each student at Rivera makes academic progress from year to year and is well-prepared 
for middle school. We implement school-wide Positive Behavior Interventions and Support as our strategic tool in achieving and 
sustaining a positive school culture. The Rivera community accepts responsibility for ensuring that every child receives instruction that 
meets or exceeds the rigor of the Common Core State Standards at each grade level. We will monitor all students by name and by 
need on formative and summative assessments and will be relentless in our work of closing the gap between our subgroups, providing 
strategic interventions to ensure that each students' needs are identified, and teaching high-yield strategies to increase each students' 
proficiency level. By working collaboratively as a professional learning community that includes staff, parents, and students, we are 
confident that we will achieve our goals.