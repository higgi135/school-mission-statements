César E.Chávez High School is located at 2128 South Cypress Avenue in the city of Santa Ana. César 
E. Chávez High School provides both a learning environment that enhances our students' 
achievement through a challenging and standards-based curriculum, and the support necessary to 
promote their social and emotional development. In 2017 César E. Chávez High School received a 
six-year term of accreditation from the Western Association of Schools and Colleges (WASC). The 
College Board approved the rigor of our standards-based course offerings to meet four-year 
university admission requirements. Each year, over 60% of César E. Chávez High School's graduates 
enroll in institutions of higher learning. 
 
Students from throughout the district who are at least 16 years old enroll at César E. Chávez High 
School so that they can earn high school course credits at an accelerated pace. The small-school 
setting allows students the opportunity to participate in a comprehensive, yet individualized 
program. Currently in operation are many programs for students at risk of graduation, English 
learners, pregnant minors, teen parents, students with special needs, migrant education students 
and gifted and Talented students (GATE). Enrollment in the school is arranged through a referral 
process (RFA) initiated by site administrators at comprehensive high schools for students who are 
deficient in credits. Pupil Support Services may refer less than 5% of students for inter-district 
transfers, involuntary placements or readmission from expulsions. Students must complete 220 
credits in required courses of study to be able to graduate and receive a high school diploma. These 
are the same graduation requirements as the other high schools within the Santa Ana Unified 
School District. 
 
SCHOOL VISION: "Making Students WHOLE," which is an acronym for Well, Happy, Organized, 
Learned, and Excellent. 
 
The school matrix provides a daily paradigm for every action and interaction between teachers, 
staff and students. WHOLE is the acronym for Well, Happy, Organized, Learned, and Excellent. We 
work with students to insure that they are Well. We will help students discover their purpose in life 
so that they will be Happy. We teach them how to be Organized so that they can work on a plan of 
study. We insure that they are Learned to achieve academic success. And we teach them the joy of 
being Excellent which is a natural result of being "WHOLE". Every student is an integral part of the 
success at César E. Chávez High School; and at César E.Chávez High School, we prove that the 
WHOLE is greater than the sum of its parts. The greatness of César E. Chávez High School is making 
students WHOLE. After all, this is a place where eagles soar. 
 
SCHOOL MISSION: The mission of César E. Chávez High School is the holistic, continuous process 
of making students WHOLE. César E. Chávez High School asserts that an empowering education is 
possible only within a scholastic context that is safe and therapeutic. Within that context, teacher 
and students work together to ensure that all efforts are aligned to our two prime directives: To 
provide and participate in effective and engaging instruction, and to build and maintain positive 
teacher/student relationships. All of this happens in the context of a "Trauma Informed School 
Culture" of teachers and staff who understand the importance and impact of insuring that every 
interaction they have with students is a positive one. 
 

----

---- 

Santa Ana Unified School District 

1601 East Chestnut Avenue 
Santa Ana, CA 92701-6322 

714-558-5501 
www.sausd.us 

 

District Governing Board 

Valerie Amezcua – Board President 

Rigo Rodriguez, Ph.D. Vice – 

President 

Alfonso Alvarez, Ed.D. – Clerk 

John Palacio – Member 

 

District Administration 

Stefanie P. Phillips, Ed.D. 

Superintendent 

Alfonso Jimenez, Ed.D. 
Deputy Superintendent, 

Educational Services 

Thomas A. Stekol, Ed.D. 
Deputy Superintendent, 
Administrative Services 

Mark A. McKinney 

Associate Superintendent, Human 

Resources 

Daniel Allen, Ed.D. 

Assistant Superintendent, K-12 

Teaching and Learning 

Mayra Helguera, Ed.D. 

Assistant Superintendent, Special 

Education/SELPA 

Sonia R. Llamas, Ed.D., L.C.S.W. 
Assistant Superintendent, K-12 
School Performance and Culture 

Manoj Roychowdhury 

Assistant Superintendent, Business 

Services 

Orin Williams 

Assistant Superintendent, Facilities 

& Governmental Relations 

 

2017-18 School Accountability Report Card for César E. Chávez High School 

Page 1 of 11 

 

To that end, César E. Chávez High School is committed to: 
1. Providing all students a safe, clean and orderly environment with a calming and healing affect (Teachers commit to being an integral 

member of a "Therapeutic Web"). 

2. Delivering daily instruction that is effective and engaging because it is student-centered, and directly supports the academic efforts 

of students to achieve their A through G requirements. 

3. Developing programs that will enhance every student's talents and abilities, including those that have special needs. 
4. Supporting experiences that promote the multicultural, civic and environmental awareness of students. 
5. Facilitating and developing opportunities for parents to meaningfully engage in the family dynamics and actions necessary to assist 

and encourage the success of their students for achieving their college and/or career goals. 

 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently serving nearly 49,300 students in grades K-
12, residing in the city of Santa Ana. As of 2017-18, SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 
alternative high schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or Reduced Price Meal 
program, 39% qualifying for English language learner support, and approximately 13% receiving special education services. Our district’s 
schools have received California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I Academic Achieving 
Schools, and Governor’s Higher Expectations awards in honor of their outstanding programs. In addition, 20 schools have received the 
Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have developed and maintained high 
expectations to ensure every student’s intellectual, creative, physical, emotional, and social development needs are met. The district’s 
commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high quality educational 
program. Consistent success in meeting student performance goals is directly attributed to the district’s energetic teaching staff and 
strong parent and community support.