Colfax High School is located adjacent to the community of Colfax, just off Highway 80, approximately 16 miles from Auburn. The 
attendance area extends from just outside Auburn to Emigrant Gap and is bordered by the American and Bear Rivers. It is part of the 
Placer Union High School District and serves approximately 600 students in grades 9-12. The Principal is Paul Lundberg, and the 
Assistant Principal's are Danise Hitchcock and Annette Udall. 
 
Colfax High School operates on the fundamental assumption that all students are capable of experiencing success and achieving 
excellence in learning. We provide a climate which affirms the worth and dignity of all students while setting high standards for learning 
and behavior. The campus has a relaxed and friendly atmosphere where students and staff feel safe and enjoy good relationships. 
Staff, students, parents and community members are encouraged to work together to create a friendly and challenging environment, 
which encourages people to reach their full potential. We not only provide our students with a strong academic foundation, but many 
outstanding opportunities outside the classroom are afforded our students to round out their educational experience. These include 
strong programs in athletics, drama, music, fine arts, and career-technical education. Recently, Colfax high has become an official 
International Baccalaureate (IB) school and will begin offering IB courses in the Fall of 2019. 
 
Our Expected Schoolwide Learning Results (ESLR's) are for graduates to be: 

Effective Communicators 

 
 Resourceful Learners 
 
 
 Academically Responsible 
 

Life-long Learners 
Fit Individuals 

Exceptional Citizens 

 
Mission Statement: 
The mission of Colfax High School is to serve as an active part of a community based in tradition and strong values of supporting, 
serving, respecting and celebrating who we are. Colfax High School strives to meet the needs of all learners in a positive, supporting 
environment, believing strongly that all students can learn at high levels. We are dedicated to stimulating all students’ thinking, 
challenging their creative minds with inquiry, and reinforcing the value of taking risks in order to create principled, balanced, reflective 
thinkers and communicators. 
 
All programs at Colfax High School emphasize high expectations of scholarship and merit, as well as international mindedness so that 
students can be open-minded, responsive to diversity, and become positive, well-rounded citizens of this world.