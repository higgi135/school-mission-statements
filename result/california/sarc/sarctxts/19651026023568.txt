Del Sur School is located in the northwest portion of the Antelope Valley at the intersection of 90th Street West and Avenue H. Our 
enrollment as of October 2018 consisted of approximately 830 students in grades K-8. This school is operated as a K-8 school with a 
Principal and a Vice-Principal. Del Sur also has a full-time counselor. 
 
The educational program at Del Sur is framed by state guidelines and tailored by administrators, teachers, and parents working 
together to establish school priorities. The School Site Council helps coordinate school activities and generate the School Improvement 
Plan for students. Our English Language Acquisition Committee helps provide input and direction for our students. Students are 
rewarded for academic achievement, attendance, and for citizenship. Focusing on the positive helps to improve the total school 
climate at Del Sur School, we utilize John Wooden's Pyramid of Success to help motivate our students. Del Sur School continues to 
ensure that student learning is a priority through student engagement, the use of learning objectives, data, and professional 
development to enhance learning strategies including the AVID program, Project Lead The Way, and STEM . We also offer after school 
tutoring for grades 1-8, enrichment programs for all grades, and a Latino Literacy program for ELL parents. We are a true middle 
school and elementary school operating on the same campus. Many classrooms on campus utilize Google Classroom and technology 
is widely used by all grade levels. We have a school wide garden, that students, staff and the greater community have worked diligently 
together to create. Students have the opportunity to join student clubs such as Interact, CJSF, Big Brothers and Big Sisters and Dancing 
Feet. We offer a Robotics Program as well as a Model United Nations Program for 7th and 8th Graders. Students in 6th, 7th and 8th 
grade are encouraged to join our sports teams including: Cross Country, Basketball, Volleyball, Soccer and Track. We provide lunch 
time tutoring for grades 4-8, after-school tutoring for grades 1-8 and a Mindfulness Program, Science Program and parent lead Art 
Program for students K-6. Teachers meet in professional learning communities (PLC). In these communities, teachers create common 
formative, summative assessment, discuss student assessments, strategies to best assist all students. 
 
Mission Statement 
Our Mission Statement is, Del Sur School is a dynamic learning community committed to preparing students socially and academically, 
to be college and career ready. “Pointing the Way & Providing the Path.” 
 
DEL SUR SCHOOL’S VISION STATEMENT 
 
We are… 
Determined 
Relationship building 
AVID in and across all grade levels 
Goal setters 
Organized 
Nurturing 
Student focused 
GO DRAGONS! 
 
 
 
 

2017-18 School Accountability Report Card for Del Sur Senior Elementary 

Page 2 of 12