Pleasanton Middle School is a high achieving comprehensive middle school that houses several 
special programs such as Spanish Dual Immersion, a Counseling Enriched classroom, and several 
Special Education classes. As a staff, we are committed to helping all of our students experience 
academic success, and are working hard to identify and serve each individual student who is not 
making adequate yearly progress. To that end, we continue to offer intensive intervention classes 
in mathematics and language arts and academic support during the school day, and strategic 
interventions after school through the Study Hour Tutoring. All of these intervention efforts have 
required monetary and personnel support from the district and parent groups. Title I, LCAP, and 
categorical funding, from the district, was utilized to provide staffing for intervention classes and 
additional instructional materials, new software, and resources to assist our underserved students. 
Teachers and administrators alike are participating in numerous trainings and professional 
development in order to better support student achievement. We anticipate that our efforts will 
continue to result in great educational success for all of our students, and we continue to see 
improvements in student growth. 
 
Mission Statement 
The goal of the faculty and staff at Pleasanton Middle School is to prepare students to meet the 
challenges and demands of the twenty-first century. Our educational program is designed to meet 
the academic, social, emotional, and physical needs of the young adolescent. The comprehensive 
core curriculum is thoughtfully formulated to emphasize academic achievement, encourage 
student responsibility, and enhance self-esteem. We are committed to providing positive support 
and guidance for students during this transitional period. 
 
School Profile 
Pleasanton Middle School is located in the central region of Pleasanton and serves students in 
grades six through eight following a traditional calendar. The current 2018 - 2019 student 
enrollment is 1217, including 115 students in special education; 91 students qualifying for English 
Language Learner support; and 154 students qualifying for free or reduced price lunch. 

-------- 

Pleasanton Unified School 

District 

4665 Bernal Avenue 

Pleasanton, CA 94566-7498 

(925) 462-5500 

www.pleasantonusd.net 

 

District Governing Board 

Mark Miller, President 

Valerie Arkin, Vice President 

Joan Laursen, Member 

Jamie Hintzke, Member 

Steve Maher, Member 

 

District Administration 

David Haglund, Ed.D. 

Superintendent 

Micaela Ochoa Ed.D. 

Deputy Superintendent, 

Business Services 

 

Odie J. Douglas, Ed.D 

Assistant Superintendent, 

Educational Services 

 

Julio Hernandez 

Assistant Superintendent, 

Human Resources 

 

Edward Dialazo 

Assistant Superintendent, 

Student Services 

 

 

2017-18 School Accountability Report Card for Pleasanton Middle School 

Page 1 of 10