Good Hope Elementary School is one of eight elementary schools in the Perris Elementary School District. The school is located just 
outside the city of Perris and was built in 1962. The school serves Pre-School-6th-grade students from diverse cultural and socio-
economic backgrounds in a suburban/rural area. The current enrollment for the 2018 – 2019 school year is approximately 600 
students, comprised of 4% African American, 2% white, and 93% Hispanic population. Our school educates 56% English Learners and 
79% Low socio-economic families. Good Hope is on a modified-traditional schedule. 
 
The 2018-2019 school year continues to provide students with 1:1 iPads for grades TK – 6th. Funding has been allocated over the 
recent years for technology. Good Hope Elementary School is a third-year AVID Elementary School, implementing AVID strategies in 
the area of math and language arts. The 2018-2019 school year is the third year with a school-wide implementation of Project Lead 
the Way (PLTW). Students work on STEM-based modules guiding them through the design and engineering process. We provide a 
rigorous curriculum, guided by the California Standards, in the areas of language arts, math, PE, Art, and English Language 
Development. 
 
Good Hope Elementary works together with the School Site Council and ELAC to develop the School Plan for Student Achievement. 
Data, both CAASPP and district assessments, are used to drive the decisions made to develop goals. Goals have been created in the 
areas of English Language Arts, math, and increasing achievement of our English Learners. Another goal is to create a safe and 
welcoming environment for staff, students, and parents. Progress is monitored each year through parent surveys, district assessments, 
and state assessments. 
 
Good Hope Elementary School's mission statement is "Building Learners for Life."