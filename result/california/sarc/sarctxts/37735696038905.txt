San Luis Rey Elementary School holds high expectations for student achievement and behavior. We 
provide HOPE to our students and the community. H - We Honor and Respect everyone. O - Open 
Hearts and Minds. P - Perseverance, we never give up. E - Everyone is Family. We are committed to 
helping each student become academically successful. Our dedicated teachers meet with parents 
and discuss with them how they can better support their child. Grade-level teams collaborate to 
ensure that teachers cover the Common Core Standards during each school year. Our small 
enrollment of about 286 students allows us to pay close attention to individual students. 
 
We strive to have a balanced approach to teaching children literacy, and provide different programs 
to help students at their particular level. To support math instruction, most of our teachers have 
been trained with Cognitively Guided Instruction (CGI) strategies as well as the Go Math Program. 
We have two computer labs that allow students individual practice targeted to their particular 
needs and skills. Our 3rd, 4th, and 5th grade students also have access to one to one Chromebooks 
to enhance their learning experience and better prepare them for the 21st Century and real world 
skills. We provide a wonderful after-school program through the Boys and Girls Club of Oceanside, 
which gives students a great place to go after hours. 
 
Parents are integral to our success. We encourage you to attend School Site Council, PTA, GATE, 
and English Language Advisory Committee meetings. We appreciate parents who participate in 
school functions. Please volunteer in our classrooms or our school-wide events like Fall Festival, 
Field Day, and Math and Literacy Nights. 
 
 

 

 

----

---- 

----

---- 

Oceanside Unified School District 

2111 Mission Avenue 
Oceanside CA, 92058 

(760) 966-4000 
www.oside.us 

 

District Governing Board 

Eleanor Juanita Evans, President 

Mike Blessing, Vice President 

Eric Joyce, Clerk 

Raquel Alvarez, Member 

Stacy Begin, Member 

 

District Administration 

Julie Vitale, Ph. D. 
Superintendent 

Shannon Soto, Ed. D. 

Deputy Superintendent 

Todd McAteer 

Associate Superintendent Human 

Resources 

Mercedes Lovie, Ed. D 

Associate Superintendent Business 

Services 

 

2017-18 School Accountability Report Card for San Luis Rey Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

San Luis Rey Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16.80 14.80 

0.0 

0.0 

0.0 

 

 

 

 

Oceanside Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

14.24 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

San Luis Rey Elementary School 

16-17 

17-18 

18-19