Principal's Message 
I would like to welcome you to Community Day School's Annual School Accountability Report Card. 
In accordance with Proposition 98, every school in California is required to issue an annual School 
Accountability Report Card that fulfills state and federal disclosure requirements. Parents will find 
valuable information about our academic achievement, professional staff, curricular programs, 
instructional materials, safety procedures, classroom environment, and condition of facilities. 
 
Community Day School provides a warm, stimulating environment where students are actively 
involved in learning academics as well as positive values. Students receive a standards-based, 
challenging curriculum by dedicated professional staff and based on the individual needs of the 
students. Ongoing evaluation of student progress and achievement helps us refine the instructional 
program so students can achieve academic proficiency. 
 
We have made a commitment to provide the best educational program possible for Community 
Day School's students, and welcome any suggestions or questions you may have about the 
information contained in this report or about the school. Together, through our hard work, our 
students will be challenged to reach their maximum potential. 
 
Mission Statement 
It's all about the children. 
 
Education is not a service that can be delivered to students; it is something that happens within 
individuals as they engage with academic content, learn to do important things in the world, and 
make meaning of their own lives. It is best and most effectively supported by strong, mutually 
accountable relationships between teachers, families and the students themselves. All other 
activities should work to support these relationships and the focus on the student's success. 
 
School Profile 
Community Day School is located in the southern region of Claremont and serves students in grades 
seven through nine following a traditional calendar. At the beginning of the 2017-18 school year, 
9 students were enrolled, including 22.2% in special education, 11.1% qualifying for English 
Language Learner support and 77.8% qualifying for free or reduced price lunch. 
 
 

Claremont Unified School District 

 

170 West San Jose Avenue 
Claremont, CA 91711-5285 

(909) 398-0609 

www.cusd.claremont.edu 

 

District Governing Board 

Hilary LaConte, President 

Beth Bingham, D.Min., Vice 

President 

Nancy Treser Osgood, Clerk 

Steven Llanusa, Member 

Dave Nemer, Member 

 

District Administration 

James Elsasser, Ed.D. 

Superintendent 

Lisa Shoemaker 

Assistant Superintendent, Business 

Services 

Julie Olesniewicz, Ed.D. 

Assistant Superintendent, 

Educational Services 

Kevin Ward 

Assistant Superintendent, Human 

Resources 

Brad Cuff 

Assistant Superintendent, Student 

Services 

 

2017-18 School Accountability Report Card for Community Day School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Community Day School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

6 

0 

0 

6 

0 

0 

4 

0 

0 

Claremont Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

325 

5 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Community Day School 

16-17 

17-18 

18-19