John F. Kennedy Elementary School 
 
 
 
Motto: Envision Excellence . . . Exceed Expectations! 
 
 
 
Mission Statement 
 
 
 
 
At John F. Kennedy Elementary, we believe that every student is unique and capable of learning to be college and career ready. We 
exist to educate, facilitate, and engage students to be responsible, self-motivated citizens who are readily capable of solving problems 
in the ever-changing 21st century.