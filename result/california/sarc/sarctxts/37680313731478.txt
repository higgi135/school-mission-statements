Coronado High School is a comprehensive high school which offers a full slate of core academic courses and electives as well as 
programs such as Advanced Placement (AP courses), arts conservatories through the Coronado School of the Arts (CoSA), NJROTC, 
Career Technical Education Pathways (Arts, Media & Entertainment, Patient Care, Woodworking, Engineering, Computer Science), 
Adult Education, and CIF freshman, junior varsity, and varsity athletics. 
 
CHS: Our Vision 
“We inspire, innovate, and create limitless opportunities to thrive.” 
 
CHS: Our Mission 
Quality Education for Life 
 
Through rigorous academic standards, high expectations, and a coordinated curriculum, the Coronado Unified School District, in 
partnership with our small, involved community, will graduate students with the knowledge and skills necessary to excel in higher 
education, careers, society, and life with the confidence not only to dream, but to determine their futures.