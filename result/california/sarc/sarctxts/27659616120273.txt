At Dr. Martin Luther King Jr. Academy, we believe… 
 
it is a human right to receive a high quality education that helps marginalized communities break 
the cycle of poverty, that words have the power to nurture or destroy a child’s self-worth, and that 
we, as educators should serve as a guiding compass, that supports through positive relationships 
with our students, and the community as a whole. 
 
Dr. Martin Luther King, Jr. Academy (MLK) currently serves approximately 605 students in 
Transitional Kindergarten-6th grade, as well as approximately 80 pre-schoolers in the Monterey 
County Head Start Program. We are proud to provide educational services for all our students in a 
safe, and state of the art environment. 
 
Some highlights of our school includes the following: 
 
1. In the fall of 2017, our district opened up a Family Resource Center on our campus. This center 
provides families in our neighborhood access to numerous resources such as: 

• 
• 
• 
• 

counseling; 
adult English as a Second Language classes; 
technology classes; 
and referrals to a myriad of agencies/ organizations designed to support families in 
need. 

2. Our district has recently hired counselors to support students with their social emotional needs. 
Currently our counselor is shared with one of our sister schools, and is available to work with our 
students 2 1/2 days per week. 
 
 Our counselor works on Monday, Tuesday, and half a day on Wednesday. We have also partnered 
with Harmony at Home, an organization that provides counselors for students dealing with trauma, 
and are able to have an 
 
 additional counselor on site for another day; thus, counseling is available to our students a total of 
3 1/2 days per week. 
 
3. Classrooms are equipped with the latest technology, including updated wireless Internet, LCD 
projectors, document cameras (ELMOs); 
 
4. In addition, all students in Kindergarten-6th grades are provided 1:1 machines, either Dell Venue 
Tablet, Chromebooks, or an Apple Ipad to assist with learning 21st Century technology skills; 
 
5. Teachers also receive ongoing professional development on the latest strategies of technology 
integration during the school day. 
 
Software programs that our students have access to include, but are not limited to the following: 
• Google Classroom, Seesaw, Doceri, Accelerated Reader, Symphony Math, Type to Learn 
4, Lexia Core 5, Read 180, System 44, Education City, Scratch, IXL Math, IXL English 
Language Arts, and Code.org 

6. MLK has a state of the art Instructional Media Center/ Library with fully functioning computer 
lab, as well as two rolling Computer on Wheels Carts (COWS); 

2017-18 School Accountability Report Card for Dr. Martin Luther King Jr. Academy 

Page 1 of 9 

 

 
7. MLK has a Before School Program (BSP), as well as After School Program (ASP) that serves over 120 students on a daily basis. During 
our ASP, students are able to participate in: 

• Drumline; 
• Trumpets; 
• Mariachi Program (brass instruments); 
• Folkloric Dance 

8. MLK also has programs available to our students on Saturdays via a partnership with Hartnell Community College which are the NASA 
SEMA Program, and the nationally recognized Coder Dojo Academy; 
 
9. MLK is also the hub for fall sports in the Alisal Union School District as we house volleyball, soccer, and flag football games on our school 
grounds. 
 
10. MLK students have access to an outdoor garden for use during the school day, and during our After School Program. 
 
11. MLK teachers meet in a revamped Instructional Coach's office/ Workroom where they have access to resources such as guided reading 
books, and an area where they are able to collaborate during Grade Level Team meetings. 
 
12. MLK is currently in the 4th year implementation of the Positive Behavior Intervention and Supports (PBIS) Model. Students are 
recognized for their positive behavior, and understand the mantra of being Ready, Responsible, and 
 
 Respectful scholars. 
 
13. MLK teachers have identified a Problem of Practice (PoP), which is a lack of consistency in a school wide writing program, and as such 
have begun working with a Write Up a Storm coach/consultant to improve writing across all grade levels. 
 
At Dr. Martin Luther King Jr. Academy, our faculty works hard to promote our vision of success and excellence within each student. Each 
teacher continues to motivate our students to work hard and be self reliant; so that in turn, they can make a positive difference in their 
community. Students are recognized monthly for academic success and improvement in behavior via our student of the month assemblies, 
as well as end of trimester Accelerated Reader assemblies. 
 
Teachers and all staff members are continuously working to provide all of our students with the best educational experience possible. 
 

 

2017-18 School Accountability Report Card for Dr. Martin Luther King Jr. Academy 

Page 2 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Dr. Martin Luther King Jr. Academy 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

28 

25 

21 

2 

0 

1 

0 

3 

0 

Alisal Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

5 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.