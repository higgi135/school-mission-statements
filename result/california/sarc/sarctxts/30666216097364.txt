At Anaheim Hills Elementary the staff recognizes that the years from kindergarten through sixth grade are a time of uninhibited 
wonder, enthusiasm for learning and exceptionally rapid growth. 
 
During this time, children develop identities socially, emotionally, physically, and intellectually which often determine the future 
course of their lives. 
 
Anaheim Hills Elementary School provides a strong academic focus for our students along with an environment that supports the 
building of positive behavior, social relationships and individual responsibility. Students’ individual needs are a focus for teachers. 
Parents are always a welcome part of our instructional program and parent volunteers, at school and at home, help to support our 
program. Our very involved PTA are also partners in supporting our academic program by providing enrichment opportunities, support 
materials and supplementary programs. 
 
All administration and staff are implementing the new English Language Arts textbook adoption WONDERS, with a strong focus on 
Tier 1 of RtI delivering best first instruction. In grades kindergarten - third Daily 5 has been implemented, which allows students to 
practice reading and writing on a daily basis. In addition, the Daily 5 allows teachers the time to pull students for small group instruction 
to improve student achievement. A 45 minute daily writer's workshop model is being implemented this year using Write From The 
Beginning and Beyond. Grade 6 is implementing the Discovery Education Techbook. Other key focus areas this year include, but are 
limited to the integration of technology into our curriculum, and improvement in reading comprehension. Additionally, meeting the 
individual needs of all students in the classroom through leveling and small group instruction, school intervention, and enrichment 
programs are available. Continuing attention is made to use various sources of assessment data for instructional purposes to best 
support our students. Staff Development is encouraged and continuous. Teachers attend and participate in staff development each 
year and share what they have learned at the trainings during staff meetings. 
 
ANAHEIM HILLS MISSION STATEMENT 
It is the mission of AHES is to provide an education of the highest quality, blending the resources of the home, school, and community. 
We are committed to empowering students to reach their full potential with rigorous academic standards. We develop confident 
children who can collaborate, create, and communicate in a constantly evolving world. 
 
VISION FOR ANAHEIM HILLS ELEMENTARY 
Our main focus is encompassed in our motto: 
 
"PRIDE" 
 
"P"erserverance 
 
"R"espect/Responsible 
 
"I"ntegrity 
 
"D"edicated 
 
"E"xcellence in ALL things 
 
The staff of AHES strives to meet the needs of the children in a warm and supportive atmosphere and to provide opportunities for 
children to achieve their highest potential in the community. 
 

2017-18 School Accountability Report Card for Anaheim Hills Elementary School 

Page 2 of 12 

 

AHES provides an environment that encourages the building of positive behavior, social relationships and individual responsibility. In 
addition to focusing on the Common Core Standards, providing all our students with access to the Core Curriculum, and promoting 
positive values is included as a part of the student curriculum. 
 
Student diversity is valued at AHES and students' individual academic and social needs are a focus for teachers. 
 
In order to achieve this educational vision the students, staff, parents and community are committed to the following goals: 

• High academic standards and expectations for all students 
• 
• 

Students will acquire skills and attitudes which will promote a commitment to be lifelong learners 
Students are encouraged to be and will be taught to be critical thinkers, communicate, collaborate and be creative through 
cooperative learning experiences, technology resources, manipulative materials and science experiments 
Students are promoted from our school with a positive attitude, independence, self-motivation, responsibility and an 
acceptance of others which enables them to become productive members of society. 

•