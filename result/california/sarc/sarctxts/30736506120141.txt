Oak Creek Elementary School opened in September 2002 to serve the students within the Oak Creek community. Our attendance area 
includes all Oak Creek as well as the newer residential developments in the Spectrum community. Student enrollment typically 
fluctuates between 875-925 students in grades Kindergarten through Six. The Oak Creek Elementary campus also hosts a regional 
Autism Specific Program and on-campus child care facility (CDC) for students in all grades. Our beautiful campus is nestled within the 
residential area of Oak Creek which is composed of single dwelling homes, condominiums, and apartments. Our buildings and grounds 
are well-maintained, and boast desirable features such as ample natural light and square footage, shared collaborative spaces, and 
state of the art educational technologies. 
 
Our school and community at large are characterized by great pride, spirit, and diversity. This is evident at our many family events 
throughout the year, including Movie Night, International Food Faire, Jog-a-thon, Open House and Spring Fling. Throughout the year, 
we enjoy a high level of parent involvement, including daily volunteerism, special event planning, and participation in regular Parent 
Teacher Association, English Language Advisory Committee and School Site Council meetings. 
 
The Oak Creek staff is comprised of a diverse and enthusiastic staff who is committed to continuous improvement. As a staff and 
within grade level teams (Professional Learning Communities), our teachers regularly reflect on their practices and results to ensure 
that our students encounter an engaging and impactful standards-based curriculum. In addition to our academic focus, we remain 
attuned to and supportive of the development of the whole child, ensuring that our students prosper academically, physically, socially, 
and emotionally. 
 
At Oak Creek Elementary, our mission is to enable all students to become contributing members of society empowered with the skills, 
knowledge, and values necessary to meet the challenges of a changing world and to provide the highest quality educational experience 
we can envision. To provide such an experience, we believe in and are dedicated to: 

• Valuing each child’s uniqueness and celebrating diversity 
• Providing an instructional program that instills a joy of learning and empowers all children to reach their potential 
• Practicing ethics and values with an emphasis on respect, honesty, responsibility, cooperation, perseverance, compassion. 

We teach and reinforce these values through our school-wide positive behavior system. Since we are the Oak Creek Owls, our behavior 
expectations are: 

Integrity 

 Winning Attitude 
 
 Show Respect & Responsibility 
 Encourage Others 

They form the acronym W.I.S.E. and all students are reminded daily to be W.I.S.E. Owls. 
 

2017-18 School Accountability Report Card for Oak Creek Elementary School 

Page 2 of 11