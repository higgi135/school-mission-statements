VISION STATEMENT 
Inspiring excellence, believing all students can and will learn, Del Campo High School will prepare every student to meet the demands 
of higher education and careers, through innovative, challenging instruction, and comprehensive support in partnership with the 
Cougar Community. 
 
MISSION STATEMENT 
Del Campo's mission is to close the achievement gap by preparing all students for college readiness and success in a global society. 
 
SCHOOL PROFILE 
Del Campo High School, which opened in 1963, is located in the middle class suburban community of Fair Oaks, which is 12 miles 
northeast of the state capitol, Sacramento, California. Approximately half of the students attending Del Campo are from throughout 
the San Juan Unified School District and came to the school through open enrollment. 
 
Del Campo, a comprehensive 9-12 public high school places a strong emphasis on college preparatory courses. The school operates 
on a 4X4 Block System which allows students to take four classes that meet 82 minutes each day for eighteen weeks. Over the course 
of the school year students are able to take eight courses instead of the traditional six. Del Campo’s exemplary academic, athletic, 
and extra curricular programs are validated by accreditation from the Western Association of Schools and Colleges. Del Campo also 
earned recognition as an AVID National Demonstration School. The program, in its 17th year of operation, includes more than 200 
students, who are academically in the middle, and gives them the tools they need to succeed in a four-year university program. 
 
Del Campo’s student publications are exceptional. The Decamhian is rated as one of the top yearbooks in the country, earning all the 
major awards given in scholastic journalism, including 17 CSPA Gold and Silver Crowns, 15 NSPA Pacemakers, and has been inducted 
in the NSPA Scholastic Yearbook Hall of Fame. The Decamhian advisor was the 1996 National Yearbook Advisor of the Year. 
 
Del Campo’s Air Force Junior ROTC (JROTC) is ranked as one of the top programs in the nation, having achieved Distinguished Unit 31 
consecutive years. Only eight percent of the units nationwide were identified “with Merit,” indicating a ranking of “exceeds 
standards,” with zero deficiencies on the annual assessment by the Air Force Headquarters. Del Campo was awarded "Distinguished 
Unit with Merit" for 2016-2017 for the sixth consecutive year. Four Del Campo JROTC alumni are currently participating in senior ROTC 
in college with the aim of serving as an officer in the U.S. military. One cadet was California Cadet of the Year for 2010, as awarded by 
the Air Force Association; and our own Aerospace Science Instructor was honored as an Outstanding Instructor for 2010-2011. Several 
graduates are in worldwide military deployment, including Iraq and Afghanistan as they serve in a multitude of job assignments. A 
Del Campo cadet was once again selected to attend the Youth Conference at Valley Forge, PA -- one of 30 selected by the Veterans of 
Foreign Wars organization from 6000 JROTC cadets in the state. 
 
Approximately 550 students participate in athletic programs, with 9 earning all-city honors, 34 earning all-league honors, 25 individuals 
advancing to section level play as well as 4 teams advancing to section playoffs in the 20016-2017 year. 
 
Principal's Message: 
On behalf of the faculty and staff, I welcome you to Del Campo High School. Since its opening in 1963, Del Campo has served the 
community of Fair Oaks preparing every student to become contributing, responsible, and caring members of the community. 
 
Del Campo High School has established a set of School-wide Learning Outcomes that consist of the following: 
 
 

 

2017-18 School Accountability Report Card for Del Campo High School 

Page 2 of 17 

 

Engaged learners who… 
analyze and comprehend complex texts across all content areas 
demonstrate literacy through effective communication skills in both written and oral expression 
demonstrate digital literacy, including the ability to evaluate and cite sources 
justify understanding by citing specific evidence 
collaborate with other students to share information and solve problems 
 
Responsible community members who... 
demonstrate positive character traits 
participate in the community in a respectful manner 
are accountable for their individual actions and are respectful of others 
demonstrate persistence when encountering challenging academic content or skills 
 
Prepared graduates who... 
set short-term and long-term educational and personal goals and use strategies to achieve them 
make sound decisions about social, physical, emotional and mental health and demonstrate resiliency in the face of adversity 
evaluate their strengths and abilities in order to make informed decisions about their post-secondary plans 
 
In addition to a rigorous academic program, other programs are offered, including Mock Trial, Academic Decathlon, Drama, Air Force 
ROTC, Kids Helping Kids, Yearbook, Crime Science, AVID, Choir, Band, Science Olympiad, History Day, an assortment of clubs, and an 
athletic program that is rich in tradition earning numerous section titles. Del Campo High School has an active and supportive parent 
community. Currently, in addition to the many volunteer opportunities, Del Campo High School has an active Parent Teacher 
Association and Booster group that raises money for the entire school. 
 
Please join us as we continue to build upon previous successes as we move into the 21st Century. 
 
Sincerely, 
 
Greg Snyder, Principal