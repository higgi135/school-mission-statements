Antioch Middle School (AMS) is a comprehensive school for grades 6 through 8. AMS is dedicated to having all students successful 
both academically and socially. Students can choose from a rich arts program including instrumental music, choir, and drama. 
Additional enrichment comes from athletics, after school tutoring, STEM (Science, technology, engineering and math), clubs, and 
academic counseling. The Leadership class is involved in providing spirit days, after school dances and lunch activities. The WEB Crew 
Class builds supportive friendship with incoming 6th graders to help them with the transition to middle school. Antioch Middle School 
is a Restorative School Community dedicated to kindness, safety, and responsibility. We focus on Positive Behavior, Intervention, and 
Supports (PBIS), Toolbox (social emotional tools), Peer Advocates/Conflict Mediation, Daily Mindfulness Practice, and restorative 
community building circles. Our wellness room provides a safe and supportive space for students to receive social emotional support 
from a counselor. This inclusive philosophy is an integral part of our school climate. Antioch Middle School is committed to providing 
a safe and nurturing environment where students transition from elementary to high school. The entire staff has a goal of creating an 
environment that inspires greatness. Students are encouraged and challenged to reach their highest academic and social potential. 
We want our Eagles to soar to new heights. The mission of AMS: through partnerships with students, parents, faculty, and the 
community, Antioch Middle will prepare every student for a successful life. With a focus on core academics, enrichment, and 
community partnerships, students will have the opportunity to experience leadership, creativity, citizenship, and scholastic 
achievement. Celebrating success in and out of the classroom will create a climate of inclusivity and accomplishment.