Eleanor Murray Fallon Middle School is an exceptional school whose reputation and student 
achievement have grown steadily in recent years. With a strong strategic plan in place that 
emphasizes life-long learning, high levels of literacy, STEM subjects (science, technology, 
engineering, and math), the arts, and integration of technology in the classroom, we have fully 
embraced 21st-century learning and are well-positioned to ensure that all students make steady 
progress toward college and career readiness. In addition, we continue to support the social and 
emotional needs of early adolescents as they mature and grow into young adults, providing 
opportunities through an integrated Social and Emotional Learning Program for them to learn and 
practice skills such as managing emotions, building positive relationships, and making responsible 
decisions. 
 
At Fallon Middle School, students can expect to be challenged academically on a daily basis and 
take an active role in charting their course for learning. As our school and state have transitioned 
to the new California Common Core Standards with their emphasis on strong literacy skills and 
conceptual understanding of math, parents and students can expect teachers to provide students 
with timely feedback about their learning and provide opportunities for students to receive 
targeted support when necessary. Likewise, students who demonstrate exceptional skill or interest 
in a particular subject are given plenty of opportunities to explore and enrich their learning. In 
addition, students can expect to master the skills of critical thinking, creativity, collaboration and 
communication which are so important in the information age. 
 
It is an exciting time in public education, and all our children need and deserve to be fully prepared 
for the global marketplace of ideas they will encounter as they move forward into high school, 
college, and beyond. Fallon Middle School enjoys a strong working partnership between staff, 
students, parents, community organizations, and local businesses. Together, we have created a 
positive learning environment that supports and challenges all our children. 
 
School Mission Statement 
The Fallon School Community serves life-long student learners within a positive and nurturing 
environment, where diversity is embraced, and the individual is empowered to reach toward 
academic excellence, enriched by opportunities in sports, fine arts, and community service. 
 

2017-18 School Accountability Report Card for Eleanor Murray Fallon Middle School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Eleanor Murray Fallon Middle School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

66 

68 

60 

0 

0 

1 

0 

3 

1 

Dublin Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

553 

16 

1 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.