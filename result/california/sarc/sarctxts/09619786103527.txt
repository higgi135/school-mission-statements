“The Rescue Union School District, working cooperatively with parents and community, will educate all students to their highest 
potential, preparing them to understand and appreciate the past, adapt to the ever-changing present, and make responsible decisions 
for the future.” 
 
Marina Village School is located north of Highway 50 in El Dorado Hills. The school is 36 years old. It is a sixth/seventh/eighth grade 
school with an enrollment of 810 students. Marina Village is one of seven schools in the District. Marina Village has a trimester 
schedule which provides students with more enrichment choices than on a semester schedule. Progress reports are mailed home 
shortly after the middle of each trimester. Grades are mailed home at the end of each trimester. 
 
Marina Village has established a reputation for academic excellence. There are grade requirements for participating in extracurricular 
activities. The staff has high expectations for quality work from students. Each student can expect to be treated fairly, to work and 
play in a safe environment, to be challenged, and to be properly instructed and evaluated by competent, caring teachers. 
 
Students with special needs are provided special help through several support programs. The Resource Specialist Program provides 
help for students in the areas of mathematics, reading and language arts. This support is provided by direct instruction, collaboration 
with the classroom teacher, and team teaching. Class size is small to allow for individual attention. Additional support is provided by 
a school counselor, a district nurse (1 day per week), a district psychologist (4 days per week), and a county speech/language specialist 
(1 day per week). Services include academic counseling, crisis intervention, and referrals to outside agencies. Tutorial instruction is 
available before school, during lunch periods, and after school through the HIP program (Homework is a Priority) for those students 
in need of extra help. A mandatory after-school intervention program called ZAP (zeros are prohibited) is provided for students who 
are academically failing. Marina Village teachers work closely with the support staff.