Description of District 
The Visalia Unified School District is the oldest school district in Tulare County. The district is comprised of 25 elementary schools, 5 
middle schools, 4 comprehensive high schools, 1 continuation high school, 5 charter schools, 1 adult school, and a school that serves 
orthopedic handicapped students. Over 32,000 students Pre-K to adult are served through the Visalia Unified School District. 
 
Description of School 
Manuel F. Hernandez Elementary School served approximately 715 students in grades Pre-K - 6 in 2017-2018. Our teachers and staff 
are dedicated to ensuring the academic success of every student and providing a safe and productive learning environment. The school 
holds high expectations for the academic and social development of all students. Curriculum planning, staff development, and 
assessment activities are focused on assisting students in mastering the state academic content standards, as well as increasing the 
overall student achievement of all subgroups. Manuel Hernandez School was recognized as a California Distinguished School and a 
Title 1 Academic Achieving School for the 2011-2012 and the 2016-2017 school year. 
 
School Mission Statement 
Our fundamental purpose is to show academic growth for each student as demonstrated by ongoing student assessment data. 
Teachers are committed to knowing the standards and working collaboratively to design and deliver lessons to meet the individual 
needs of the students. 
 
We will achieve this by ensuring that students: 

• will encounter a challenging and interesting curriculum that requires content and instruction ensuring student 

achievement of agreed upon academic standards. 

• will experience a variety of instructional strategies including the use of technology to enhance learning opportunities. 
• will build high self-esteem through success in personal achievement based on a foundation valuing hard work, excellence 

in effort, perseverance, trustworthiness, caring, responsibility, respect, fairness and citizenship. 

Further: 

 We promote mutual respect between home and school. 
 We promote respect and appreciation of diverse groups and cultures. 
 We promote effective communication between parents and school. 
 We implement outreach strategies to provide information, education and support for parents.