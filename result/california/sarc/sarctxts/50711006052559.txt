Hickman School District joined the Charter world in 1994 when it sponsored a start-up school, Hickman Charter School. After 
witnessing the success of the Hickman Charter School students, along with the positive power of the parent/staff collaboration, both 
the districts’ Elementary (K-5) and Middle School (6-8) chose to embrace the possibilities charter status could afford. Charter status 
empowered us to better meet the challenge of supporting the diverse needs of all students. Thus, in 2000 both schools converted to 
charter status. Since there are just 3 schools in the district, once all were charter, we became the Hickman Community Charter District. 
The district's charter has been renewed in 2005, 2010, and 2015. 
 
There is power in being a district of choice. With this power, we choose the mindset of creative flexibility, which includes not only 
flexible scheduling, but also creative staffing with all three schools working interactively. We choose to create a culture where a 
collaborative spirit attracts students, parents, and community members who are all willing to “look outside the box” and work 
together. 
 
Our Mission: Inspiring students to learn and grow to their potential. 
 
Our Vision: Every student a responsible, productive citizen in a diverse and competitive world. 
 
Our Values and Beliefs: 
 
Success of All Students: All students can learn and they learn in different ways. They deserve the opportunity to have instruction 
delivered in a way that is meaningful, relevant, and accessible to them. The process of learning is as important as the product and 
requires a growth mindset. 
 
High Expectations: Expectations for students, parents, and staff are clearly defined, understood, and shared. Students, parents, and 
staff are empowered, supported, and trained to meet those expectations. 
 
Respect and Integrity: Every person is valuable and deserves respect. Communication and interaction is defined by mutual respect, 
trust, and support. 
 
Teamwork: The organization works collaboratively and creatively to ensure student success in a supportive environment. Successes 
are recognized and celebrated. Parental involvement is an essential element of a quality educational experience. 
 
Safety: Schools and work sites are safe and secure for students, parents, and staff. 
 
Effectiveness and Efficiency: Financial and human resources are managed effectively, and prioritized to meet the goals and 
expectations of the organization. 
 
Continuous Improvement: Staff, parents, and students collaboratively evaluate progress using multiple, reliable measures, and make 
changes when needed. 
 
 
 

2017-18 School Accountability Report Card for Hickman Elementary School 

Page 2 of 10