Built in 1960, Pomerado Elementary School has served generations of Poway families. Our school currently serves approximately 400 
students in transitional kindergarten through fifth grade, in addition to one regular education State-funded 6-hour preschool class 
that serves 3-4 year old students. We also have Level I and Level II Autism Spectrum Disorder (ASD) preschool classes that service 
approximately 20 special education preschool students and 10 typical peers. In addition, our site has two Autism Spectrum 
Disorder (ASD) classes that serve TK-2nd and 3rd - 5th grades. Pomerado Elementary is a whole-school Federally Funded Title 1 school. 
 
 Pomerado is proud to be a part of the nation-wide No Excuses University network. We believe in setting high expectations for each 
of our students and remain focused on creating a safe, supportive learning environment where all students can be successful in 
meeting their goals. Staff and students see their learning through the lens of a growth mindset and learn to persevere through 
challenges. All students have signed a pledge on display in our Multi-Purpose Room to Work Hard, Be Kind, and let No Excuse stand 
in their way. 
 
 Staff is highly dedicated and passionate about using the very best teaching strategies and resources. Staff collaborates on a regular 
basis within and between grade level teams, and as a whole staff. Staff attends a variety of district and site based professional 
development opportunities, and is eager to implement research-based programs that focus on specific student needs. Technology is 
used to support learning and student engagement, and staff is trained on how to best incorporate technology into their 
instruction. Pomerado is part of a district Voyager program, which provides weekly coaching support for teachers as they focus on 
personalizing learning for each student. The blended learning that is occurring allows our students to have voice and choice in how 
they learn, and how they show their learning. We guide students in discovering their strengths, values, and interests to help them 
find their own best pathway to college and/or career. 
 
 We provide a wide array of academic support programs for children who require special assistance in mastering academic standards. 
Our Resource Specialist Program (RSP) serves approximately 30 students, and two speech and language pathologists work with 
students with IEP speech and language goals. Our English Language Learner program assists students who need extra support with 
language acquisition and educational support. An ELL Instructional Aide is part of this program and works with small groups on 
targeted skills. We also employ a part-time Spanish-speaking Bilingual Parent Liaison who provides translation for school documents and on-
site translation services to aide in communication with teachers, staff, and parents. Our academic support program also includes part-
time trained impact teachers who work with groups of students on specific and targeted skills. These groups are flexible, programs 
used are research-based, and data is tracked on a regular basis. Our before and after school programs extend the learning day for 
students in need of academic interventions, homework help and/or time on computers. 
 
 Challenges and higher level thinking activities are also in place for our students who are excelling and moving beyond 
proficiency. These include our Math Olympiad Club and our Book Club; both focus on using critical thinking skills and more advanced 
problems and books. In addition, students can be identified for our Gifted and Talented Educational Program (GATE) starting in second 
grade through our district qualification process. 
 
 Pomerado staff not only cares about every child’s academic achievement, but also their social and emotional development. For this 
reason, we embrace the 6 Pillars of Character of the Josephson Institute of Ethics - CHARACTER COUNTS! Initiative. As coalition 
members, we integrate these concepts into the daily experience of each student and staff member. A new pillar is introduced and 
emphasized each month. Our school wide discipline plan is followed by all staff and is based on the practices of Restorative 
Justice. Students earn Good Character cards which are signed by parents, celebrated in the office when turned in, and then displayed 
on our Multi-Purpose Bulletin Board. For our students who need some extra support with social/emotional needs, we have a 
counselor on site 3 days a week and a Student Services Aide. Both see small groups, meet with students one on one, and teach 
classroom lessons on empathy, being a friend, and other social skills. During our monthly Kind Cats lessons each student hears and 
discusses the same book. Books focus on kindness and allow us to have a common language and strategies for solving problems with 
peers. 

2017-18 School Accountability Report Card for Pomerado Elementary School 

Page 2 of 11 

 

 
 We take pride in creating a safe environment where students are engaged in using high level thinking skills as they master California 
State Standards. Our Wildcats help each other, show kindness to others, persevere with school work, and are ready to take on any 
challenge as they prepare themselves for college and careers. 
 
 
 SITE NO EXCUSES UNIVERSITY (NEU) STATEMENT 
 Pomerado's staff believes that each of our Wildcats deserves a safe learning environment that will empower and motivate him/her 
to achieve or move beyond proficiency. Each staff member commits to providing this environment and will let No Excuse stand in our 
way. Each day we focus on creating a culture of universal achievement and building exceptional systems that form a firm foundation 
for student learning. We pledge to keep expectations high, hold each other accountable, and keep our students firmly on the path to 
college and career readiness. 
 Work Hard, Be Kind, No Excuses!