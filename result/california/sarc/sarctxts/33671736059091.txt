Vision: Provide a learning environment that supports students in reaching their academic, social 
and emotional potential. 
 
Mission: To promote the 21st Century Skills, ( 4 C’s Communication, Collaboration, Critical thinking 
and Creativity) and a 'growth mindset' to ready students to become college and career ready. To 
promote bilingualism within our learning community. 
 
Nellie N. Coffman Middle School serves a diverse population of 1098 6th, 7th and 8th grade 
students. Situated on the boundaries of Cathedral City and Rancho Mirage, the campus commands 
sweeping views of the surrounding mountains. NNC carries a proud tradition of excellence and of 
connection to the community. We focus our continuing efforts on the improvement of our 
instructional practices. We use district-approved curriculum to design instruction for our students 
in the California State Standards for each grade level. Our electives program includes opportunities 
in band, choir, digital media, journalism, art and student leadership. We ensure that all students 
have access to rigorous and engaging curriculum. At Nellie N. Coffman Middle School, each day is 
a new opportunity for success. 
 
 

----

---

- 

Palm Springs Unified School 

District 

150 District Center Drive 
Palm Springs, CA 92264 

(760) 883-2700 
www.psusd.us 

 

District Governing Board 

Richard Clapp, President 

John Gerardi, Clerk 

Karen Cornett, Member 

Madonna Gerrell, Member 

Timothy S. Wood, Member 

 

District Administration 

Sandra Lyon, Ed.D 
Superintendent 

Michael Swize, Ed.D 

Assistant Superintendent, 

Educational Services 

 

TonySignoret, Ed.D 

Assistant Superintendent, 

Human Resources 

 

Brian Murray, Ed.D. 

Assistant Superintendent, 

Business Services 

 

 

2017-18 School Accountability Report Card for Nellie N. Coffman Middle School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Nellie N. Coffman Middle School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

44 

46 

45 

1 

4 

0 

4 

2 

4 

Palm Springs Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1038 

18 

34 

Teacher Misassignments and Vacant Teacher Positions at this School 

Nellie N. Coffman Middle School 

16-17 

17-18 

18-19