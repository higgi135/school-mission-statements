Carlsbad High School is a nationally recognized school of excellence based on student performance. 
Our students regularly demonstrate the skills and knowledge required to be admitted to the most 
prestigious colleges and universities in the nation. Our last Academic Performance Index (API) 
reported by the State was 840. Carlsbad High School empowers students to become successful 
learners, creative thinkers, and contributing citizens in an environment dedicated to excellence, 
respect, diversity, and tradition. Our goals are to use the tenets of student-centered learning to 
improve student engagement and success in the learning process, to expand school wide 
systematic interventions in order to support student academic success for all subgroups, and to 
provide students with 21st century skills and assist them in creating a college or career pathway so 
that they successfully graduate Carlsbad High School with a post-secondary plan. Each of these 
areas will emphasize support for our students with special needs and our students who require 
English as a second language. The need to change, grow, and improve is at the core of our school 
site plan. The goal is not only to excel in comparison to nearby schools, but to be one of the top 
schools in the Nation. 
 
 
 
 

 

 

----

---- 

----

---

- 

Carlsbad Unified School District 

6225 El Camino Real 
Carlsbad CA, 92008 

760-331-5000 

www.carlsbadusd.k12.ca.us 

 

District Governing Board 

Mr. Ray Pearson, PRESIDENT 

Ms. Kathy Rallings, VICE PRESIDENT 

Mrs. Veronica Williams , CLERK 

Mrs. Claudine Jones, MEMBER 

Mrs. Elisa Williamson, MEMBER 

 

District Administration 

Benjamin Churchill, Ed. D. 

Superintendent 

Mr. Chris Wright 

DEPUTY SUPERINTENDENT, 
ADMINISTRATIVE SERVICES 

Mr. Rick Grove 

DEPUTY SUPERINTENDENT, 

PERSONNEL SERVICES 

Dr. Robert Nye 

ASSISTANT SUPERINTENDENT, 

INSTRUCTIONAL SERVICES 

 

2017-18 School Accountability Report Card for Carlsbad High School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Carlsbad High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

96 

100 

0 

5 

0 

0 

 

 

 

Carlsbad Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

574 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Carlsbad High School 

16-17 

17-18 

18-19