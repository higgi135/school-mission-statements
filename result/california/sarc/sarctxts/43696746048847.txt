The community immediately surrounding Haman School is comprised of small single family homes and numerous apartment 
complexes. Many of the homes were built in the 1940’s and 1950’s. Haman is adjacent to a small city park, which is frequently active 
with athletic activities for all ages. Numerous modest small businesses are mixed into the residential neighborhood lending to the 
small town atmosphere. Our students come from a variety of home situations including single parent families, children living with 
grandparents, foster homes, and extended families living together in the same home. 
 
Haman School is a place where students become lifelong learners and develop the skills and strategies necessary to become 
contributing citizens. High academic standards and a challenging curriculum are the foundations to student success. We work to 
develop a strong sense of community where students feel safe and appreciated for their individual differences. We value parents and 
the community as significant contributors to student success. We strive to enhance the self esteem of each child as we work to ensure 
each child’s continued success.