Sunrise School is a special education facility serving Mt. Diablo students. Sunrise educates and works with students who have not been 
successful while receiving special education services on larger, general education sites. Students receive a strong, structured behavior 
management program to promote academic and social-emotional growth. Sunrise serves a maximum of 47 special education students 
in five classes, grades K-8. All classrooms are designated as special day classes and staffed with a special education teacher, a special 
education assistant(s) and are supported by Behavior Health Specialist(s). The middle school special day classroom provides a more 
intensive mental health program and are staffed with two full-time Behavior Health Specialists to support students’ mental health and 
behavioral needs throughout the school day. The other four classrooms are each assigned a Behavior Health Specialist who provides 
milieu support during the day and pull out mental health services. The crisis team which is comprised of a Behavioral Health Specialist 
and a Special Education Assistant, support all students in the milieu as well. 
 
There is a high ratio of staff to students at Sunrise. Classrooms are staffed to maintain a minimum 3:1 ratio. Additionally, students 
have the benefit of a part-time school psychologist and other DIS services, as needed, such as speech/language services, occupational 
or physical therapy and adapted physical education. 
 
Sunrise School provides an academic program consistent with state and district standards, using modified instructional materials and 
settings, as needed, to meet individual student needs. Modifications and accommodations are used in accordance with each child’s 
I.E.P. 
 
Sunrise students are expected to do their personal best and to accept responsibility for their learning and their behavior. They are 
expected to accept individual differences, work together and to be respectful to peers and adults. 
 
Sunrise staff values parental involvement and works hard to build and maintain a close working relationship between school, staff and 
parents. 
 
MISSION 
The mission of Sunrise School is to educate, serve and support special education students with diverse social, emotional and academic 
needs who have not yet achieved success on a regular education campus. Sunrise supports students and their families by providing 
academic and therapeutic interventions in a safe individualized learning environment. Sunrise school strives to promote personal 
growth and academic success by teaching social skills that can be generalized to the school and the community.