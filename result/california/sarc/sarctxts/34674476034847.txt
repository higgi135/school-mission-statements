Principal's Message 
Pershing Elementary School provides a safe, nurturing, and child centered environment in which our children can grow academically, 
socially, and emotionally. We encourage our children to learn, celebrating their creativity, initiative and individuality, through daily 
interactions in the classroom and through participation in art, music and physical education. Our students are focused on always doing 
their "personal best." Staff members are dedicated and committed to the success of every child, and their professionalism and 
knowledge is greatly acknowledged. Our parents/guardians and dedicated volunteers help provide the support and interaction that 
contributes to a valuable partnership between home and school, benefiting the children at Pershing school. 
 
Mission Statement 
The mission of Pershing Elementary, created collaboratively with the staff and community, is as follows: 
 
An inspiring, nurturing, and innovative school, the mission of Pershing K-6 Elementary is to engage and challenge every child to be a 
compassionate citizen and an analytical problem solver through high-quality, dynamic instruction based on individual student needs 
within a cohesive learning community. 
 
School Profile 
Pershing K-6 Elementary is a neighborhood school with a strong academic program. Pershing K- 6 Elementary has the Rapid Learner 
program, beginning in grade 2 and continuing through grade 5. The Rapid Learner program is an accelerated program and students 
are working at a more accelerated pace with more time spent applying their knowledge with more depth and rigor in English Language 
Arts and math. The school has an increasing number of students that are identified as English Learners. Every student comes to school 
with different needs and the Pershing staff believe every student deserves support to access the same high-quality education. 
 
Pershing Elementary is one of 41 elementary schools in the San Juan Unified School District. Our demographics this year include 
approximately 66% white, 15% Hispanic/Latino, 10% Asian/Asian American, 2% African American (or Black), 7% are two ore more 
races, and less than 1% are other ethnicities or unknown. About 7% of our students are English Learners, with Spanish being the most 
common primary language, and about 34% of students are socioeconomically disadvantaged. Additionally, 22% of students are 
enrolled in the Rapid Learner program and approximately 7% are receiving special education services. 
 
Pershing offers a variety of classes and activities to enrich the learning environment. They include: 

Field trips and speakers brought into classrooms to enrich grade level instruction. 

• 
• Band in Grades 4-6, Choir Grades 4-6 
• Discovery Club, before and after school child care is available (subsidized and fee-based). 
• PTO (Parent Teacher Organization) sponsored assemblies focusing on fine arts, science and bullying prevention (topics 

vary by year). 

• A full-time media technician that runs our library. 
• After school baseball run through Orangevale Pony League (fee-based). 
• Chess Club offered at lunch on a weekly basis (fee-based). 
• Programs such as Mad Science, Early Engineers or Firefly Art are typically offered after school (fee-based). 
• 

Family activities, like the Meet and Greet Breakfast in the fall, Spring Carnival, Book Fairs, and Family Nights focused around 
topics such as math, science, literacy and writing. 

2017-18 School Accountability Report Card for Pershing Elementary School 

Page 2 of 11 

 

For 2018-2019, the Pershing staff, based on the current data analysis of student data, is focused on improving student ability to explain 
their thinking through writing across subject areas. Through analysis of our student data, it is evident that our students are developing 
in their ability to explain their thinking, especially in problem solving in math. Throughout 2018-2019, staff professional development 
and collaboration will include a strategic focus on furthering our students' skill development in this area. In addition, staff are focused 
on ensuring that students will feel safe at school and feel a sense of belonging and connectedness to our school through the use of 
restorative practices.