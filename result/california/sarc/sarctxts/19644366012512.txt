Grovecenter Elementary School is located in West Covina, California on the corner of Badillo Street and Lark Ellen Avenue. The school 
was built in 1957 and is one of nine elementary schools in the Covina-Valley Unified School District. Total enrollment for the current 
school year is approximately 600 students. Class size reduction is in place in all Kindergarten through third grade classes. In addition, 
three Autism Spectrum Disorder classes are located at Grovecenter Elementary School. 
 
Located on the Grovecenter campus is Kids Korner, which is a Covina-Valley Unified School District before and after school care 
program. As part of our instructional program, fourth and fifth grade students participate in weekly music classes. The campus also 
houses a Library Media Center and each classroom provides 1:1 devices for all students. The school library contains over 6,400 volumes 
and is staffed by a part-time school helper. The Destiny library system is used to maintain students' library books and textbooks. 
 
We are proud of our diverse culture at Grovecenter. 74% of our student population is Hispanic and 9% is Caucasian. Other ethnic 
groups include African American, Filipino, and Asian, which represents approximately 18% of the school population. 72% of the student 
population qualifies for Free and Reduced lunch. English is the predominant language, and Spanish speaking students make up 
approximately 24% of the population. 
 
Students have access to a variety of instructional materials. In addition to subject area specific textbooks, other supplemental 
instructional materials such as maps, textbook support materials, computer software, multimedia materials, and on-line learning 
resources are available to support the school’s instructional program. Grovecenter has a system of interventions in place to meet the 
needs of students performing below grade level. A response to intervention pyramid is used to guide school staff in providing 
appropriate intervention based on the level of intervention needed and the student's success in the given interventions. 
 
Grovecenter Elementary School is clean and the grounds are well maintained. Staff, students, and parents work together to achieve a 
safe and secure educational environment where students are expected to maintain appropriate behavior at all times. School rules and 
procedures are distributed and discussed on a regular basis. In addition, programs such as Award's Assemblies, Character Counts, and 
Principal's Lunch Bunch help promote positive behavior in and out of the classroom. 
 
Grovecenter continues to meet the educational needs of its students as demonstrated by continued academic achievement results. 
Staff evaluates its testing data yearly in order to effectively provide instructional strategies that are best for the students. Based upon 
student achievement data, instructional strategies are developed to provide the optimal educational setting for the students. In Spring 
of 2016, Grovecenter was recognized as a Gold Ribbon School and Title I Achieving School. In Spring of 2017, Grovecenter was 
recognized as a 2016 Honor Roll School by the CBEE. Grovecenter is also in its first year of coding with Code to the Future. All students 
in TK through 5 will participate in Epic Builds each trimester to showcase their coding projects. 
 
Grovecenter Elementary School has made a commitment to raising student achievement in reading and mathematics, including a 10% 
increase in the number of students proficient or above on the CAASPP, including all subgroups. The school wide instructional focus is 
that 100% of students show a minimum of one year's growth in their ability to read and comprehend text at grade level or above as 
measured by their performance on the STAR reading assessment. 
 
GROVECENTER VISION AND MISSION STATEMENT 
Grovecenter School’s staff, students, families, and community members make up our Professional Learning Community. We are 
dedicated to providing a strong, challenging, and rigorous educational program that supports the academic and social success of all 
students within a safe, positive, nurturing learning environment. Our goal is for students to be high achievers in academics and 
character, as well as to be prepared for life in college and beyond. 
 
 

2017-18 School Accountability Report Card for Grovecenter Elementary School 

Page 2 of 11