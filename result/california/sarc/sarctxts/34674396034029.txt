Isador Cohen is a Family Partnership School with emphasis on core curriculum and social 
responsibility. Cohen is a GATE Center in Sacramento City Unified, for grades 2 thru 6, where 
students experience advanced instruction with exceptional instructors. Cohen is a caring, nurturing 
school where the children can learn and grow each day with understanding teachers. We seek to 
enhance and expand our focus on teaching the Common Core State Standards with positive 
outcomes which are reflected in student test scores, and the overall academic interest and well 
being of our students. 

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Tu Moua 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for Isador Cohen Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Isador Cohen Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

12 

0 

0 

8 

3 

1 

12 

3 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Isador Cohen Elementary School 

16-17 

17-18 

18-19