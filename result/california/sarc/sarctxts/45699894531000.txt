Burney Junior- Senior High School serves students in seventh through twelfth grades. The school is located in eastern Shasta County 
on Highway 299 East, approximately 55 east of Redding. The town of Burney has a population of 3,300 and is the most urban of the 
rural communities served by the Fall River Joint Unified School District. 
 
The Board of Trustees of the Fall River Joint Unified School District believes that it is the responsibility of the school and home, working 
together, to provide a positive environment for students within which they may: 

• Realize potentials for learning 
• Develop and maintain basic skills and concepts 
• Develop an understanding of responsibilities to self, to family, and to community 
• Develop creativity 
• 
• Respect and appreciate different cultures 
• Respect and appreciate the American heritage 
• 
• 

Learn in an environment that fosters a feeling of mutual respect and tolerance 
Learn in a safe and positive environment 

Enjoy learning and become lifelong learners