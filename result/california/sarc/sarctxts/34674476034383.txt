Mission Statement 
Valuing diversity and full inclusion, the mission of Cameron Ranch School is to ensure that each student becomes an informed critical 
thinker, strong communicator, and self-directed learner through personalized, rigorous instruction, utilizing technology, and modeling 
positive character traits in a safe, nurturing, respectful environment, in partnership with parents and community. 
 
School Profile 
Cameron Ranch Elementary is one of 33 elementary schools in the San Juan Unified School District. The curriculum provided is aligned 
to the Common Core State Standards. The school supports cultural awareness on a daily basis through its diverse literature selections 
and other school activities. 
 
As a full inclusion site for children with developmental disabilities, Cameron Ranch is dedicated to developing the individual potential 
of all students. We believe that each student has the right to be successful academically and socially. We encourage in all students’ 
sensitivity to diversity and a sense of community responsibility. Staff and community work together to share knowledge and ideas to 
provide a rich learning environment for students. 
 
We base our school behavior and discipline on Positive Behavior Intervention System (PBIS). We implemented Responsive Classroom 
strategies and structures to provide a positive community of learners in every classroom. Students learn social skills and problem 
solving through weekly Second Step curriculum lesson in our general education classrooms. We provide a tiered system of behavior 
supports including social skills groups, check-in/check-out, individual behavior charts and positive rewards. 
 
The Cameron Ranch principal, teachers, and staff value diversity and excellence. Our mission is to educate and inspire each student to 
succeed and responsibly contribute to a radically evolving world by providing innovative, rigorous, student-focused instruction and 
programs in a safe, caring, and collaborative learning community. 
 
We are a Response to Intervention school, which addresses the individual needs of each student. We have implemented a scientific, 
research-based balanced literacy program with the belief that every student can learn to read and write. Within our balanced literacy 
program we differentiate instruction employing a number of strategies to meet the varied needs of individual students. 
 
Cameron Ranch is a data driven school. All staff members have been trained on using the Illuminate system and gathering formative 
assessment information during daily instructional to target learning for each student. Illuminate provides teachers with information 
needed to plan focused instruction and provides internal accountability. Teachers are able to measure and monitor student progress 
using this data so that instruction is precise and personalized. 
 
We ensure that all students have access to core curriculum, all teachers are highly qualified, and interventions are checked frequently 
for proper placement of students. Teachers provide rigorous instruction based on Common Core State Standards. 
 
We pay close attention to the culture and climate of our school. We provide varied opportunities for our children to learn in an 
environment that is safe and fosters personal and academic growth. 
 
Cameron Ranch staff hold high expectations for all stakeholders. We practice the following beliefs: that every person is unique and 
has equal worth, that every student can and will learn, and that diversity is a valuable asset that strengthens and enriches our 
community. Through these multiple beliefs we have been able to start closing the achievement gap. 
 

2017-18 School Accountability Report Card for Cameron Ranch Elementary School 

Page 2 of 12 

 

We believe that parent involvement is the key to building a sound foundation that encourages academic and personal achievement. 
Parents are invited to attend classroom visitations referred to as Family Day and themed activity mornings including Harvest Festival 
and Earth Day. During Family Day, the parent shadows their student thus developing an understanding of the classroom environment, 
how the instructional rigor is scaffolded for students throughout each day and how they can support their child's educational 
experience. Parents of students in 1st, 2nd and 3rd grade have the opportunity to participate in Academic Parent Teacher Teams to 
learn activities which can be done at home to improve early literacy skills and math fluency. During individual APTT conferences, 
parents set SMART goals for their child to achieve academic growth by practicing skills at home. 
 
Professional development occurs weekly at Cameron Ranch. We provide time for grade level collaboration and professional learning 
communities. We recognize the importance of ongoing teacher learning and that it is directly linked to instructional practices and 
strategies. In the past three years we have based our professional development around PBIS, lluminate, Universal Access, Guided 
Reading, Critical Literacy, Running Records, and Writer's Workshop. All of these trainings support classroom teaching strategies, school 
and class organization, balanced literacy instruction, intervention and assistance as well as home, school and community partnerships. 
 
Cameron Ranch offers an After School Bridges program. Bridges After-School/ASSETs Programs provide homework support, 
enrichment and pro-social recreation in a safe and positive environment. The Bridges After-School/ASSETs Programs currently serve 
over 3,000 students, grades K-12, at 32 sites. Student success is supported through the implementation of each of the four components 
of the program: Homework support, enrichment/disguised learning, pro-social skills/recreation, and parent support and 
empowerment. Strong relationships between program staff, school staff, parents and students are key! If you are interested in this 
program please contact the school office. 
 
Principal's Message 

• We will ensure all staff are implementing rigorous instructional strategies using targeted learning goals, integrating 

relevant technology and utilizing individualized assessment data to increase student proficiency. 

• We will actively engage families and community as partners in the educational process. 
• We will identify, model, and integrate positive character traits and social skills instruction, to ensure all students become 

contributing, responsible, and caring members of our diverse community.