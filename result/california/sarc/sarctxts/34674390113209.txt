The John Morse Therapeutic Center is a small special education school in the Sacramento City 
Unified School District that meets the unique needs of students in first through eighth grades who 
exhibit emotional and behavioral challenges. Our students show problems with self-regulation and 
impulse control resulting in the need for a more restrictive school environment than what is 
available on a regular comprehensive school campus.Our vision is to make JMTC a safe and caring 
community where students feel secure so that they can reach their full potential, positioning them 
to be productive members of society. 
 
At John Morse Therapeutic Center, we support students’ social and emotional development in a 
variety of ways. From our close-knit community of staff and students, come caring relationships. 
At JMTC, every student is known and seen. We also encourage student self-motivation and self-
monitoring with our school-wide token economy and level system. In addition, we offer as-needed 
access to clinical staff, close communication and collaboration with parents/guardians, outside 
therapists and agencies, as well as a mindfulness curriculum to help students learn self-regulation. 
Our P.E. curriculum includes cooperative play and yoga to further social and self-regulation skills. 
Best of all, we offer school-wide ‘fun days’ to promote positive school climate and cohesiveness, 
and to generally make JMTC a fun place to be! 
 
 
 

 

 

----

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Tu Moua 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for John Morse Therapeutic Center 

Page 1 of 10