Lillian J. Rice Elementary School is located in the southwest section of Chula Vista, approximately 
eight miles north of the United States/Mexico border. It is one of 49 schools in the Chula Vista 
Elementary School District, including charters. Rice School was built in 1938 and consists of three 
wings of self-contained classrooms, a media center, a multipurpose room, and administration 
offices. Rice School was one of 11 schools that participated in the Modernization Program during 
the 1996-1997 school year. Rice received an extensive modernization again during the summer of 
2014. Rice School has a student enrollment of 768 students. There are 33 classrooms, Pre-School 
through Sixth grade. We are in our second year of transitioning into a Dual Language Immersion 
Program. We currently have two Kindergarten classes and two First Grade classes. Two of our 
classrooms (2nd and 3rd grade) offer an Alternative Bilingual Program. There are two 
Moderate/Severe Special Ed. classrooms in Grades K-6 and two Special Ed Preschool classes. Rice 
also has two Head Start classes, as well as a therapeutic preschool (Mi Escuelita) on site in 
conjunction with South Bay Community Services. Rice is one of three schools in the district that 
hosts a Family Resource Center on campus to provide social services to the school and the 
surrounding community. In addition, we house the Rice Clinic which supports the medical needs of 
our community. 
 
Mission 
Rice Elementary, an innovative, nurturing, and diverse community, raises and empowers the 
leaders of tomorrow on the pillars of academic rigor, personal integrity, and civic responsibility. 
Our mission is to support these future leaders to love to learn and to respect others through a 
commitment between students, parents and staff. We value the uniqueness of each child and their 
individual learning styles. Learning is meaningful and relevant and touches all aspects of the child's 
life. Our diversity is embraced and enhanced across the curriculum and is linked with the world 
outside of the classroom. All members of the Rice community recognize and promote the joy and 
importance of learning as a lifelong process. The community supports an environment which 
facilitates students who are respectful of themselves and others, innovative problem solvers, self-
motivated and confident in their ability to achieve, responsible for themselves and others, and 
multilingual and multi-literate. Rice School derives strength from an active participation of 
community including alumni, parents, business, staff, and nearby Chula Vista High School. Everyone 
is encouraged and taught to be responsible for the success of our students. A variety of seminars 
are offered to parents/care givers to become more effective. The Rice School community is 
committed to creating a safe, clean physical environment that is aesthetically inviting and 
supportive of learning for all students. We plan to create a dynamic learning environment that 
supports and encourages excellent teaching and educational growth to our staff, students, and 
their families. The Rice School Community is dedicated to nurturing our students as our most 
precious resource and our future leaders. 
 
Vision 
Our vision is to provide a modern learning environment that allows students and staff to express 
their knowledge, talents, and skills through meaningful and relevant experiences. 
 

2017-18 School Accountability Report Card for Lilian J. Rice Elementary School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Lilian J. Rice Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

30 

1 

Teaching Outside Subject Area of Competence 

NA 

31 

28 

0 

 

0 

 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Lilian J. Rice Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.