We at John Swett High School envision self-motivated graduates who will be responsible, trustworthy citizens, respectful of cultural 
diversity, and able to communicate effectively, think critically and behave compassionately. Our teachers and staff work tirelessly to 
ensure that all our students have the ability to reach their highest potential. Students at John Swett have access to high quality 
instruction, many extra curricular activities and athletics, and a safe, student centered environment. All of our students have access 
to our award winning Careers Academy, which trains students in many professional pathways. John Swett High School is a great place 
for kids and families, and many of our students go on to great college and career opportunities!