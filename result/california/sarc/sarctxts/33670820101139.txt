Harmony’s Mission Statement 
It shall be the mission of Harmony Elementary School to engage and motivate all students to 
become invested in their own learning. It is our purpose to provide a variety of experiences, 
curricula, and activities that reach all learning modalities, backgrounds, and experiences. We will 
develop self-esteem, academic growth, and personal responsibility by providing a safe and 
nurturing learning environment for all students. 
 
Harmony’s Vision Statement 

• 

• 
• 

To foster high expectations for academic achievement and responsible citizenship for 
all students. 
To provide an enriched curriculum aligned with California Curriculum Standards. 
To develop a sense of community with emphasis on mutual respect and observance of 
personal rights balanced with responsibility. 

 
Principal’s Message 
Welcome to Harmony Elementary School! We take great pride in our school and believe that it is 
one of the finest elementary schools in the valley. Harmony has a very dedicated, highly qualified 
staff that is passionate about educating children. We are proud of our accomplishments. We have 
high expectations for our students and staff. Our staff operates as a Professional Learning 
Community and is always striving to improve and foster students that exceed academic standards. 
With our experienced staff and supportive parent/community, we believe that we will accomplish 
our goals. 
 
Educating elementary children is greater than just teaching the academic standards. Our goal is to 
develop students emotionally, physically, and morally, as well. We plan to develop lifelong learners 
who, in future years, will contribute to their community. We implement the Positive Behavior 
Intervention Support System at our school. 
 
School Profile 
We have established many traditions and we have achieved academic success. Our school has 
received the Title I Academic Achievement Award. In addition, we are a California Distinguished 
School. 
 
Harmony Elementary School is located on the western edge of the city of Hemet and is one of 
fifteen elementary schools within the Hemet Unified School District. Harmony serves a diverse 
community that primarily resides within walking distance of our school. 
 

2017-18 School Accountability Report Card for Harmony Elementary School 

Page 1 of 10