Principal's Message 
We are Leigh High School, a school community committed to providing a well-rounded and dynamic educational, social and emotional 
experience. We offer a variety of academic courses including Advanced Placement (AP) in English Literature and English Language, 
Calculus AB and Calculus BC, Computer Science, Statistics, Chemistry, Physics, Biology, Environmental Science, U.S. History, World 
History, American Government, Studio Art, Spanish Language, and French Language. Honors courses are offered in English 2 and 3, 
Drama, and Chemistry. Specialized programs in the AVID (Advancement Via Individual Determination) program have been in place for 
more than 10 years and prepare underrepresented students for four-year colleges. The Project Lead the Way (PLTW) pathway provides 
a four-year STEAM (science, technology, engineering, arts and mathematics) experience. 
 
Leigh is privileged to have a highly experienced staff. The average years of experience of the teaching staff is 16.5 years. We are a 
school community that retains our staff, and many Leigh alumni return as teachers. Our staff is a collaborative group that meets 
throughout the year to align curriculum, develop common assessments and share best practices. They actively participate in school 
events and support student interests by supervising many clubs. 
 
School spirit is the backbone to our school and the connections we make with our students, staff and parents. Our student council and 
leadership work with staff and students to plan and execute rallies, staff and student activities, homecoming week, dances, 
multicultural fairs, and many other events to educate and engage all students. 
 
School Mission Statement 
Leigh's mission is to provide a safe and caring learning environment to prepare students for success in their post-high school life by 
engaging them in meaningful experiences. This includes participating in activities, using appropriate tools, and emphasizing critical 
thinking, problem solving, and analytical skills. To promote high standards and expectations, students, faculty, staff, parents and the 
community share the responsibility for advancing the school's mission. We are a school of about 1800 students and we are located in 
a beautiful area surrounded by trees and mountains. We are a neighborhood school with many students whose parents also graduated 
from Leigh. This translates into a deep sense of pride and commitment to our school. We offer ourselves many opportunities for our 
students to connect to their high school experience through sports, electives, clubs, band, and theater. Our goal is to support our 
students in successfully completing high school with as many options for their post-secondary life. 
 
School Vision Statement 
 
Leigh High School’s vision is to foster a learning community where we all strive for continual growth.