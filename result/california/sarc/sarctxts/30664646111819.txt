Our Mission Is Learning 
“We will ensure that all our children will learn more today than yesterday, and more tomorrow than today.” 
 
TEAM LOBO 
Together Everyone Achieves More - Learning Offers the Best Opportunities 
 
Lobo is transitioning into an Academy of Innovation. We are focused on project-based learning and adding STEAM(Science, 
Technology, Engineering, Art, and Math) into all of our instruction. 
 
The Team Lobo vision calls for: 
A rigorous and challenging curriculum that addresses the needs of all learners. 
The building of school and community pride through a strong sense of cooperation, compassion, and mutual respect. 
A safe environment in which students grow academically and socially to achieve their personal best. 
The encouragement of responsibility, decision making, and problem solving. 
The promotion of curiosity, ingenuity, and integrity. 
The fostering of active parent involvement. 
 
For additional information about school and district programs, please visit www.capousd.org