Mission statement: The Watsonville High School community is committed to offering rigorous academic and vocational opportunities 
for multiple career and college pathways beyond graduation. 
 
Watsonville High School enjoys the support of the State of California via seven small learning grants which give students the 
opportunity to learn and experience different aspects of seven career sectors. Our academies -Business, Agriculture and Technology; 
Environmental Sciences and Natural Resources; Engineering and Technology; Health Careers; ECHO/Leadership; Mosaic for the Arts; 
and Video. These academies also make our large campus more personal and help to promote good relationships among students and 
between students and their teachers. Our ninth graders are also clustered into a small learning community which focuses on skill 
development and adjustment to high school for success using strategies from our AVID program. In addition to our small learning 
communities, WHS offers a full complement of Advanced Placement courses for all subject areas and a superior counseling department 
for students to achieve their post-high school dreams. Over 150 students from other attendance areas are choosing to attend 
Watsonville High School. 
 
Our School Site Council (SSC), English Language Advisory Committee (ELAC), and booster groups actively support our school. We 
welcome parents and community members to volunteer on our campus. Our Parent Center is located for easy access and is open 
Monday through Friday for quick questions, information on campus or college resources, and to provide computer access to families 
who wish to access School Loop to monitor their child's progress.