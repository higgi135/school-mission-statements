Arvin High School was established in 1949 and serves over 2,600 students in grades nine through 
twelve from the rural communities of Arvin, Lamont, and Weedpatch. Arvin High School is one of 
eighteen comprehensive, four-year high schools in the Kern High School District and was the 6th 
school to open in the district. Agriculture and agriculture-related industries comprise the majority 
of employment opportunities for the adults in the area. AHS receives both Title I and Migrant 
Education Program funding. To adequately meet the needs of the student population, AHS offers 
diverse programs for English Learners (ELL), Special Education programs (SPE), Gifted and Talented 
(GATE), Honors, Career Technical Education (CTE), Academies, Advanced Placement (AP), Early 
College, and Dual Enrollment. Approximately, 91% of seniors graduated and 32% of graduates 
complete the required coursework for acceptance to the University of California and California 
State University systems. 
 
While the poverty and mobility rates of the students in the area are high, Arvin High's focus remains 
on increasing student academic achievement and school wide improvement. Over the past seven 
years standardized test scores and academic performance have continued to increase as have the 
number of students meeting college entrance requirements and taking college entrance exams. 
 
Mission: 
The Mission of Arvin High School is for all students to graduate prepared for college and career. 
 
To this end, the Schoolwide Learner Outcomes (SLOs) supplement the overall mission of AHS in that 
staff are committed to helping students graduate prepared for college and career by providing 
instructionally rich activities to support each of the four SLOs. SLOs are represented by the acronym 
LAPS or Learn, Achieve, Plan, and Succeed. 
 
The subset of each of these areas is measured by Arvin High School's action plan and students' 
individual goals/needs as they relate to the school mission. SLOs are dynamic and must be revisited 
by the stakeholders based on the changing needs of students as they move toward graduation. 
 
LEARN - Core academic, fine-arts, and vocational skills 
 
ACHIEVE - Proficiency level needed to earn the diploma 
 
PLAN - Continually plan for the future with the goal of obtaining a high school diploma 
 
SUCCEED - Succeed in the present and in future college or career paths 
 
 
Vision: 
 
The Vision of Arvin High School is to develop a culture that encourages: 
 
1. Positive relationships between staff, students, parents, and the community. 
2. Ownership of and responsibility for individual actions. 
3. Academic rigor and challenge. 
 
 

2017-18 School Accountability Report Card for Arvin High School 

Page 1 of 14 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Arvin High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Kern High School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

108 

128 

102 

2 

2 

1 

0 

6 

1 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1700 

216 

12 

Teacher Misassignments and Vacant Teacher Positions at this School 

Arvin High School 

16-17 

17-18 

18-19