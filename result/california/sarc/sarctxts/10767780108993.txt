The Washington Unified School District consists of seven schools, including one comprehensive high school, Washington Union High 
School, and three alternative education options for high school students, including Easton High School, a continuation high school. 
 
The Mission of WUSD Alternative Education Schools is to provide a positive on campus and/or Independent Study experience for 
students that need an alternative setting to a comprehensive high school. 
 
The Vision of WUSD Alternative Education Schools is for our students to learn from and complete Credit Recovery classes and return 
to a comprehensive school or find success through graduating from our program.