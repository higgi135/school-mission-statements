The mission of Quail Valley is: 
Quail Valley; a safe, nurturing, and positive environment where teachers and parents empower students to be active learners and 
creative thinkers. 
 
Quail Valley is one of 26 schools in the Palmdale School District. It is a K-5 school located at Avenue S and 58th Street East. The school 
population is very diverse in its educational background, economic, cultural, and social characteristics. Our population is approximately 
634 students with 74% of the population Hispanic or Latino, 20% African American and 6% other. It is a walking school with bus 
services limited to Special Education students only. Quail Valley is a School-wide Title 1 school with 91% of students on free or reduced 
lunch. 28% of our student population has been identified as Limited English Proficient. The predominant language spoken by our 
bilingual students is Spanish. All of our teachers have their CLAD or SDAIE certification. Quail Valley is in Program Improvement, Year 
5. Our school implemented the Breakfast in the Classroom program in the 2014-15 school year. 
 
Our school has 1 Principal, 1 Assistant Principal, 20 regular classroom teachers, 3 Special Day Class teachers, 1 Learning Support 
Teacher, 1.5 Resource Specialists, 1 Speech Pathologist, 1 Psychologist, 1 Secretary, 1 Health Aide, 1 Parent Liaison 1 Media 
Technician,1 half-time Librarian, and 1 half -time Bilingual Clerk. 
 
Our campus houses a computer lab, library, teacher workrooms, teachers’ lounge, cafeteria with stage, media center, office area, 
health room, sports pavilion, and 12 manufactured buildings. 
 
Quail Valley prepares students for the "21st Century". We ensure that students acquire the skills for accessing, processing, and 
communicating information that enables and inspires them to become life-long learners with the ability to adjust to a rapidly changing 
world. Quail Valley is a technologically advanced school that integrates technology into the curriculum. Every classroom has student 
computers, a Promethean board, and technology to support Airplay for teacher's i pads. Technology is as common to the process of 
education as pencil and paper. Students , staff, parents, and the community contribute and utilize their combined talents and 
resources to implement this integration. Technology is a tool that supports and enhances curriculum and student achievement. 
Technology assists our students in going above and beyond the limitations of traditional teaching in the classroom. 
 
Our Vision is: Learn for today! Succeed for life!