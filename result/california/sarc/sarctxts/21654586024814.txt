Laurel Dell Elementary School is a small, multi-cultural school located in central San Rafael. It draws 
its attendance from the Bret Harte, Picnic Hill, and East Gerstle Park neighborhoods, and is 
considered a school of request by families throughout San Rafael. Laurel Dell prides itself on the 
power of its community and a true sense of family among the students, families and staff. 
 
Laurel Dell is committed to teaching basic learning skills, offering enrichment programs in Art, 
Music, P.E., Garden, and Dance. The school community is dedicated to providing equal access to 
educational opportunities for all students, and challenging students at their educational level. 
Involving families and providing educational and support opportunities is an important part of this 
work. Laurel Dell’s Vision Statement: 
 

• 

• 

• 

• 

• 

 

 

Students will develop a deep understanding of essential knowledge, benchmark state 
standards, and life skills. 
 
Students will develop the capacity to apply their learning, to produce quality work, to 
reason and solve problems, preparing them for the challenges of the future. 

Students will be enriched by recognizing the contributions of many cultures, values, 
and ideas. 
 
Each student’s unique physical, social, emotional, and intellectual contributions are 
valued, ensuing a safe and supportive learning environment. 

Effective collaboration and communication between school staff, district staff, families, 
and community members will facilitate the shared responsibility of advancing the 
school’s mission and goals. 

2017-18 School Accountability Report Card for Laurel Dell Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Laurel Dell Elementary School 

16-17 17-18 18-19 

Teacher Credentials 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

San Rafael City Schools 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

11 

0 

0 

9 

0 

0 

11 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

248 

0 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Laurel Dell Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.