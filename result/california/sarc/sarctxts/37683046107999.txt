It is a privilege to serve as the principal of Mt. Woodson Elementary School, recipient of the 
California Distinguished School award in 1995 and 2010, the National Blue Ribbon School award in 
1997, and the California School Boards Association Golden Bell in 1999. This is an exceptional school 
with a dedicated staff that strives to provide an outstanding education to each student, everyday. 
Our commitment to supporting each student academically and emotionally is the heart of our 
school. We also share a passion for developing good character and social awareness in our students. 
Our high quality instructional program is enhanced by the support of our PTA and parent 
volunteers. We work together to support students and make everyday a great day to learn at Mt. 
Woodson. 
 
Mt. Woodson School is committed to providing a nurturing and inspiring educational program for 
all of our students. It is our goal that scholars develop into self-regulating learners that can 
persevere through any challenge. We believe it is our responsibility to prepare all students for 
career and college readiness, without exception. Our program includes an emphasis on academics 
for all students, the development of a strong self-esteem, an appreciation of the arts, and an 
understanding of technology as an important tool. 
 
We ended the 2017- 2018 school year with approximately 475 students in 17 classes ranging from 
transitional kindergarten through sixth grade. We provide a full inclusion support services program 
that serves all students with special needs. 
 
We believe our mission is to educate all scholars to higher levels of academic performance in a safe, 
loving, and healthy environment that promotes respect for self and others. We believe that all 
students have a right to achieve the mastery of grade level and extended skills. Through developing 
Self-Regulated Learners, scholars will take interest and responsibility over his or her own learning, 
ask the questions and seek resources they need to build understanding of skills necessary to be a 
life-long learner, thinker, and problem-solver. 
 
Tamara Lewis, PRINCIPAL 
 

2017-18 School Accountability Report Card for Mt. Woodson Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Mt. Woodson Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

20 

20 

21 

1 

0 

1 

0 

0 

0 

Ramona Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

244 

6 

4 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.