Our mission is to provide a positive learning environment that supports and encourages each child to reach his or her full potential. 
We work collaboratively with parents and community members to create that learning environment, which includes high educational 
standards, the integration of technology to help reach those standards, and differentiated learning opportunities to ensure the success 
of all students.