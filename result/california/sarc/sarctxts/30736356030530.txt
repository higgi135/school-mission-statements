Mission: 
"Branching Towards the Future; Rooted in the Past" 
 
Vision and Values: 
Trabuco Elementary School is beginning its 139th year of service to the community. We are 
privileged to have both a historic heritage and a technological future. Research supports the 
importance of a positive partnership among parents, staff, and students in building academic 
success for all students. This partnership must reflect a strong commitment to student 
achievement, fostered by consistent communication and collaboration between school and home. 
 
At Trabuco we believe that a school family should focus on success for all in a safe school 
environment. We believe in a challenging curriculum based on the California Common Core State 
Standards and the Next Generation Science Standards that is enhanced by fine arts and technology. 
With a unique focus on the importance of outdoor education, we also maintain high expectations 
for behavior, responsibility, and citizenship. Our staff is dedicated to providing the highest quality 
instruction to ensure student mastery of state content and practice standards 
 
Lisa Paisley, PRINCIPAL 
 

----

-

--- 

Saddleback Valley Unified School 

District 

25631 Peter A. Hartman Way 

Mission Viejo CA, 92691 

(949) 586-1234 
www.svusd.org 

 

District Governing Board 

Suzie Swartz, President 

Dr. Edward Wong, Vice President 

Amanda Morrell, Clerk 

Barbara Schulman, Member 

Greg Kunath, Member 

 

District Administration 

Crystal Turner, Ed D. 

Superintendent 

Dr. Terry Stanfill 

Assistant Superintendent, Human 

Resources 

Connie Cavanaugh 

Assistant Superintendent, Business 

Laura Ott 

Assistant Superintendent, 

Educational Services 

Mark Perez 

Director, Communications and 

Administrative Services 

Dr. Ron Pirayoff 

Director, Secondary Education 

Liza Zielasko 

Director, Elementary Education 

Dr. Diane Clark 

Director, Special Education 

Rae Lynn Nelson 
Director, SELPA 

Dr. Francis Dizon 

Director, Student Services 

 

2017-18 School Accountability Report Card for Trabuco Elementary 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Trabuco Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

3 

0 

0 

3.49 

0 

0 

4 

0 

0 

Saddleback Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1,157 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Trabuco Elementary 

16-17 

17-18 

18-19