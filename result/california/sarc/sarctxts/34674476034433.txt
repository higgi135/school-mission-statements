MISSION STATEMENT 
Carriage Elementary, a visual and performing arts school, in partnership with families and community, will ensure all children build a 
strong academic foundation that empowers them to become responsible 21st century citizens through a balanced and engaging 
curriculum. 
 
SCHOOL PROFILE 
Carriage Elementary is one of 33 elementary schools in the San Juan Unified School District. Carriage Elementary is a kindergarten 
through 5th grade school located in Citrus Heights, California serving approximately 550 students. We at Carriage believe in an 
environment that promotes academic success and positive self-esteem while valuing individual differences. Students are expected to 
be safe, be respectful, be responsible, and be good communicators with others. Through the use of positive reinforcement and 
consistent modeling, we are shaping our students into becoming model citizens. 
 
We at Carriage believe that the administration, the teachers, our parents, and the community must work together in order to complete 
our mission of academic excellence and model citizenship for students. Teachers are professionals who strive to be cooperative, 
flexible, communicative, and supportive. Parents strive to create a supportive learning environment in the home. By working together 
with families and the community, we provide an atmosphere where students have an opportunity to develop to their fullest potential. 
 
Our school programs are used to provide academic growth, enrichment opportunities, and extension activities for our students and 
for their families. Art, music, and physical education are taught by specialists throughout the day. Each grade level organizes a musical 
production throughout the school year. This gives the students an opportunity to demonstrate their academic and content knowledge 
through alternate expressive means. Bridges After-School is a program that provides homework assistance, recreation, technology 
experience, and learning centers. Students who are designated as Gifted and Talented Education students (GATE) are given the benefit 
of an educational program designed to suit their capabilities and are encouraged to extend their learning beyond the grade level 
requirements. The district offers parenting courses so parents are able to refine their parenting skills. These courses are available on 
the district website. 
 
Carriage also provides programs to meet the needs of learners who require individualized educational plans. We have two Special 
Education Teachers with self-contained ASD classrooms, as well as two Resource Teachers, one full time and one half time, as well as 
two full time Resource Instructional Assistants. We have one Inclusion Teacher who supports students who are fully mainstreamed 
with shared instructional assistant support. The Special Education Department supplements the regular education program and 
supports the district’s mission by providing the best instruction and opportunities to improve academics and develop character for all 
students. 
 
Carriage has a population of English language learners speaking a variety of languages. Carriage has on staff a full time English Language 
Development Teacher who works with our English learners in small groups according to skill level. We place our English learners in 
general education classrooms where the teachers deliver differentiated instruction on a daily basis. 
 
Carriage Drive Elementary was named a California Distinguished School in 2010. 
 
 
 

 

2017-18 School Accountability Report Card for Carriage Drive Elementary School 

Page 2 of 11 

 

Principal's Message 
Carriage Elementary has a rich history of academic excellence, parent participation, and a visual and performing arts (VAPA) focus with 
an emphasis on STEAM related activities. STEAM (Science, Technology, Engineering, Arts and Math) education is embedded into many 
of the classroom lessons , all of which are characteristics of a dynamic and successful school. I am absolutely delighted to be leading 
a school team that is focused on teaching the whole child through a balanced and engaging curriculum, with an emphasis on student 
learning, collaboration, and commitment to high expectations for all students. 
 
As Principal, it is my intent to support the philosophy of Carriage School as outlined in our strategic plan. My goal is to sustain 
leadership that will help facilitate teaching and learning by providing opportunities for rigorous, rich learning, engagement in visual 
and performing arts activities, and integration of relevant technology in a vastly changing 21st century society. 
 
It is a pleasure to be part of the nurturing school environment at Carriage, where the staff and parents are dedicated to providing an 
outstanding and enriching education for all students. 
 
Sincerely, 
Brooke Thomas, Principal 
bthomas@sanjuan.edu