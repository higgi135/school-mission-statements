We will provide an environment where students learn about themselves and the world around 
them, develop their potential, and acquire the skills necessary to become life-long learners. 
 
We envision a school that provides an environment that focuses on high academic instruction and 
values responsibility! 
 
Dwyer Middle School provides a stimulating, quality instructional environment for approximately 
1,300 students in the northwest section of Huntington Beach, six blocks from the ocean. The Dwyer 
staff, over 90 members strong, is dedicated to the mindset that all students can learn and find 
success. We have created classes that help students achieve at this high level of performance. We 
are confident that with our help, students will master the challenging academic program we have 
created at Dwyer. We work collaboratively with colleagues, students, and parents to ensure that 
our academic programs continue to improve. This year we launched our PBIS (Positive Behavior 
Interventions and Supports) program titled GRIT (Gratitude, Responsibility, Integrity and Tenacity). 
The students were trained school-wide on the expectations of behavior and rewards system, as 
well as teachers being trained on implementation. 
 
In order to continue to raise the bar for all of our students and to close the gap for students with 
below grade-level skills, Dwyer teachers will continue a tiered approach to placing students in CORE 
language arts/social studies, math, and science. Students are placed in one of five possible literacy 
skill levels in language arts; one of six possible math skill levels, and one of two possible skill levels 
in science - general education science at each grade level and one general co-taught science for 
select special education and English learner students. Each student is placed according to a multiple 
measures approach that includes on-going teacher formative assessment, GPA, SBAC assessments, 
teacher recommendations, district Benchmark assessments, and collaboratively created teacher 
assessments. When appropriate, students move between levels as skills improve or intervention is 
deemed necessary. 
 
Dwyer has been fortunate to receive bond monies from Measure Q to increase technology on site. 
During the 2018-2019 school year, Dwyer has had a complete technology integration and 
modernization upgrade to the main building. All the classrooms in the main building have 
Chromebook carts, collaborative seating, interactive boards and have been completely renovated 
with new furniture, paint, carpeting and infrastructure. To address any safety concerns, camera 
systems have been upgraded and increased along with upcoming increased security fencing to be 
installed. The ground has also been broken on the upcoming STEM multi-purpose room and 
teaching classrooms. 
 
Dwyer is a targeted Title I school and therefore supports two major interventions aimed at 
increasing student academic performance. Dwyer has instituted an after-school tutoring service for 
all students who sign up to receive it. Students receive grade-level support from Huntington Beach 
High School National Honor Society members (through our library-media technician) who help 
Dwyer students who struggle in all subject areas. The second intervention is Extended Learning 
Academy, in which students are invited to a three-hour block to catch up on zeros and missing work 
with the assistance of a credentialed teacher. Dwyer is also using an academic incentive program 
named STAR (Students Taking Academic Responsibility), to reward students for their hard work. 
 
 

2017-18 School Accountability Report Card for Ethel Dwyer Middle School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ethel Dwyer Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

47.25 48.75 48.25 

0 

8 

0 

8 

0 

7 

Huntington Beach City School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

268.36 

0 

9 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ethel Dwyer Middle School 

16-17 

17-18 

18-19