Our Mission: The mission of the Francophone Charter School of Oakland (FCSO) is to provide a dual-immersion curriculum to a diverse 
community of students. Our goal is to develop bilingual and biliterate global citizens who are open-minded and value intellectual 
curiosity, personal integrity and creativity. 
 
Our Dual Immersion Model: Sometimes called Dual Language Immersion (DLI) or Two-Way Immersion (TWI), our model prepares 
students to achieve academic standards in both French and English. Teachers and staff speak to students in one language at a time to 
provide the desired balance between the two languages. Francophone uses the 90-10 model of dual immersion. In transitional 
kindergarten, kindergarten, first grade, and second grade, students are immersed in French 90% of the school day; the remaining 10% 
is taught in English. In grades 3-5, French is used 70% of the time and English language time increases to 30%. In grades 6-8, time is 
divided evenly between instruction in French and English. As they learn French, the international language of diplomacy, spoken in 
over 29 countries around the globe by over 275 million people, our students learn about the diverse cultures and traditions in the 
Francophone world and have a unique opportunity to reflect on their own backgrounds, beliefs, and values. 
 
Our Programs: Our students participate in a broad range of study, including reading, writing, speaking, and listening in French and 
English, history, mathematics, science, physical education, technology, and the arts. Thanks to the instructional innovation of our 
teachers and the support of our family community, Francophone students are prepared for the California Assessment of Student 
Performance and Progress (CAASPP), the Diplôme d’Etudes en Langue Française, Primary (DELF-Prim), and for a rigorous course of 
study in high school. Most importantly, our staff and programs gradually build students’ capacity to communicate with complexity, 
to think critically, to collaborate with diverse groups, and to navigate a changing world.