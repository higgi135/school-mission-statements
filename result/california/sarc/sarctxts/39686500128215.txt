Harvest High School is a continuation school established in July of 2013. Harvest High serves the rural community of Ripon, a town of 
over 15,260, located in California’s culturally diverse and agriculturally rich Central Valley. Harvest High is one of two high schools in 
the Ripon Unified School District and students transfer in from Ripon High School. Moderate growth in the area is expected to impact 
the school, increasing enrollment and diversity. 
 
Our Vision 
 
To provide students with a quality education, the opportunity to recover credits and earn a high school diploma. 
 
Our Mission 
 
To train young adults to be positively connected with their community and prepared to transition to technical careers and/or college. 
 
Belief Statements 
 
At Harvest High, we strongly believe that: 

Comfortable, safe, and and inclusive learning atmosphere dominate both teaching and learning process. 

• All students are capable to achieve. 
• We must know, understand, embrace, and celebrate cultural and linguistic diversity. 
• 
• Our students should assume personal responsibility to actively pursue their goals. 
• We need to set high standards for individual behavior that will match our school’s vision and mission. 
• 
• 

Parents and community members should be active participants in our school activities. 
It is our duty to provide the EL students with adequate instructional practices that will ensure their accelerated mastery 
of English. 
Common Core practices must be effectively used in every subject on a daily basis. 
Innovative methods of using Internet technologies should become an integral part of everyday learning. 

• 
• 
• Our students should reach graduation being fully prepared to enter the workforce and/or pursue further education in a 

college of their choice.