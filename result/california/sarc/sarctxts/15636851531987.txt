Desert Junior-Senior High School (“Desert” or “DJSHS”) is a traditional grades 7-12 public school located on Edwards Air Force Base in 
Kern County, California. DJSHS is part of the Muroc Joint Unified School District (MJUSD). Desert now includes an on-site Alternative 
Education Program that utilizes on-line courses to better serve students who may need to make up credits or are best served by non- 
traditional methods of instruction. Desert is fed by one on-base elementary school (Branch). In addition to these schools, MJUSD also 
includes a combined junior/senior high school (Boron Junior-Senior High School) and elementary school (West Boron) in the adjacent 
off-base community of Boron. Of the students attending Desert, 25% are military dependents living on Edwards Air Force Base. 75% 
of the students who attend Desert live off base and provide their own transportation to and from school. Most of these students have 
parents who are either in the military living off base or are civilian contractors working on the base. 
 
VISION 
Desert's Vision is two-fold: To create an encompassing, progressive learning environment by integrating a rigorous academic program, 
student engagement and teacher dedication with family and community involvement. To guide and support students in achieving 
personal excellence in academic, social and emotional growth. 
 
Mission 
All Desert Junior-Senior High School students will become articulate, and productive citizens: life-long learners who are creative, critical 
thinkers. 
 
School-wide Learner Outcomes 
Feel the STING! 
 
S Desert Scorpions will be Self-disciplined by 

• Being self-motivated to succeed 
• Keeping a mindset of continuous improvement 

T Desert Scorpions will be Critical Thinkers 

 Being problem solvers 
 By researching, interpreting, and evaluating information 

I Desert Scorpions will be Involved in school, community, and society by 

 Displaying leadership across campus 
 Making positive contributions to school and society 

N Desert Scorpions display Ingenuity by 

 Working independently and collaboratively to complete a task 
 Exhibiting creativity and originality 

G Desert Scorpions will be Goal Oriented by 
 Setting and achieving realistic goals 
 Preparing to enter the job market, trade school, college, or military service 

 

2017-18 School Accountability Report Card for Desert Junior-Senior High 

Page 2 of 11