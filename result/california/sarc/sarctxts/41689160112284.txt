California Virtual Academy @ San Mateo believes that, given a comprehensive and mastery-based curriculum, high expectations, 
access to technology, strong instructional support, a significant amount of off-line work, guidance from experienced teachers, and a 
strong commitment from parents or other caring adults, a well-conceived virtual education program will boost student achievement, 
serve the unique needs of students and families, and offer a new model for effective public education in the 21st century. 
 
The goals of our educational program include: 

Enabling all students to become self-motivated, competent and lifelong learners 
Empowering the parent as the teacher through a guided program and training 

• 
• 
• Offering flexibility and choices to increase student interest in learning 
• Using a variety of lessons, activities, and modalities to improve student achievement 
• 
• Developing reflective learners 
• Developing technology literate students 

Striving for mastery of all skills by every student 

Our Mission Statement 
Our mission at the California Virtual Academy at San Mateo is to provide our students with a high-quality, 21st century education. Our 
mission is grounded in high academic standards and integrity, which fosters personal growth. Through a dedicated teaching and 
support staff, our students are provided with rigorous individualized instruction and high academic standards in order to achieve 
career and college readiness. 
 
We believe the key to a successful education includes a partnership between students, teachers, involved parents, learning coaches, 
leadership and surrounding community supports.