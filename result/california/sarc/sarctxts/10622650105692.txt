School Description A.L. Conner Elementary School opened in 2004-05 and is located in Orange 
Cove. The school served 335 students in grades K-5 during the 2017-2018 school year and included 
a staff of 14 regular classroom teachers. A.L. Conner Elementary School teachers and staff are 
dedicated to ensuring the academic success of every student and providing a safe and productive 
learning experience. School Mission Statement At A. L. Conner Elementary School we will ensure 
high levels of learning for all students. 

---- 

Kings Canyon Joint Unified 

School District 
1801 10th Street 
Reedley, CA 93654 

559.305.7010 

www.kcusd.com 

 

District Governing Board 

Craig Cooper 

Robin Tyler 

Manuel Ferreira 

Noel Remick 

Sarah Rola 

Clotilda Mora 

Jim Mulligan III 

 

District Administration 

John Campbell 
Superintendent 

Roberto Gutierrez 

Deputy Superintendent, Human 

Resources 

Monica Benner 

Assistant Superintendent, 
Curriculum and Instruction 

Mary Ann Carousso 

Administrator, Student Services 

Jose Guzman 

Administrator, Educational 

Programs 

Adele Nikkel 

Chief Financial Officer 

 

2017-18 School Accountability Report Card for A.L. Conner Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

A.L. Conner Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

17 

13 

10 

0 

0 

2 

0 

6 

0 

Kings Canyon Joint Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

422 

38 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

A.L. Conner Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.