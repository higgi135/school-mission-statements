Ridgeview High School is the alternative high school serving the Paradise Unified School District. The school serves students in grades 
10-12 and has an enrollment of 125 students. Students are referred for reasons of academic deficiency and/or attendance and 
discipline issues. Students have the opportunity to make up missing courses and to transfer back to the comprehensive high school or 
remain at Ridgeview and receive a high school diploma. The student:teacher ratio is 22:1. Students can enroll in all required academic 
classes for high school graduation and selected elective classes. Ridgeview High School successfully completed the Self Study process 
and was awarded a six-year accreditation through 2020 by the Western Association of Schools and Colleges. 
 
The school offers various support programs for students, including a full-time resource teacher, mathematics and reading intervention 
classes, full-time school counselor, and psychological and nursing services.