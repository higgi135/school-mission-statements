Terra Linda is a comprehensive high school serving students in grades 9-12 in the San Rafael City 
Schools High School District. The campus occupies 29 acres in a residential area off Manual Freitas 
Parkway in northern San Rafael. Students who typically attend Terra Linda High School include 
graduates from Miller Creek Middle School (part of the neighboring Dixie Elementary District), 
Venetia Valley K-8, and Davidson Middle School. 
 
At Terra Linda High School, a well-trained and highly qualified group of professionals are committed 
to ensuring all students achieve at high levels. Terra Linda High School is currently participating in 
the accreditation process through the Western Association of Schools and Colleges (WASC) with a 
scheduled visit in February 2019. 
 
The Terra Linda community has a tradition of innovation and excellence, and it has continued this 
tradition through a strong focus on preparing all students for success in postsecondary 
opportunities in college, career and community directed by the school leadership teams that 
strongly supports this vision. All students have access to the core curriculum, which includes 
English, math, social sciences, and science. Students are encouraged to develop a six-year plan for 
their high school education when they are in the eighth grade in middle school. Students follow a 
traditional bell schedule on Mondays, with a block schedule the remainder of the week and take 
six or seven classes as a full load. Students wishing a greater challenge are encouraged to enroll in 
Honors and Advanced Placement courses. Many students at Terra Linda are concurrently enrolled 
in courses at the College of Marin. 
 
 

2017-18 School Accountability Report Card for Terra Linda High School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Terra Linda High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

San Rafael City Schools 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

62 

56 

59 

0 

0 

0 

0 

0 

1 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

126 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Terra Linda High School 

16-17 

17-18 

18-19