Welcome to Los Cerros Middle School. Los Cerros is a community of dedicated teachers and staff 
who do everything possible to encourage and challenge your student. We have a thriving learning 
environment with 160 Academic and Elective classes per day. Our teachers are highly experienced, 
caring professionals, and as a result our students are successful academically and personally. 
 
Los Cerros is a community centered middle school with projects building relationships linking our 
academic instruction and character education. In November we gather for our annual Basket 
Brigade where we assemble and distribute 300 baskets of Thanksgiving supplies and gifts in families 
in need. In the Winter we leap into San Francisco Bay to raise awareness for Special Olympics in the 
Polar Plunge and also host the Special Olympics Basketball Tournament for elementary and middle 
school students. Throughout the year our Leadership students keep Los Cerros involved with Coins 
for a Cure and Pennies for Patients. 
 
Our academic and elective programs stay current with elective additions including Forensics, 
Computer Programing, Media/Graphic Design , Video Production, Robotics, 21st Century Design, 
and a leveled Foods class. We participate annually in the national Hour of Code during which all 
students learn to computer code. We include our Special Education students in all aspects of our 
school life and our General Education students are fortunate to work with these amazing students 
during class. 
 
The Los Cerros Teaching staff has fully implemented Common Core Standards into the classroom 
and we maintain our focus on engaging students through projects, labs, presentations, technology, 
and more. Staff attend regular professional development on current practices through participation 
in district and off-site trainings as well as weekly collaborative work with their peers. All of this 
dedication and work results in increased rigor and relevance in the classroom. 
 
As a parent you may be involved in the school through participation in PTA, Academic Boosters, 
and School Site Council. Our PTA and Academic Boosters support Los Cerros through their many 
volunteers hours and fundraising events and because of them we are able to maintain smaller class 
sizes, to offer a variety of electives, and to operate viable school sports, music and volunteer 
programs. 
 
I hope you will use this SARC report as a valuable tool in learning more about Los Cerros. Everything 
you need to know about our schedules, procedures, activities, and guidelines for student behavior 
and responsibilities are included. Additional details may be found on our school, library and 
counseling websites www.lcms.srvusd.net 
 
Parents are encouraged to communicate their ideas, concerns, or questions to the school. As a staff 
we are happy to assist you in anyway to help your student become the outstanding individual they 
are capable of being. 
 
Thanks you for your continued support and I look forward to our year together. 
 
Evan Powell 
Principal 
epowell@srvusd.net 

2017-18 School Accountability Report Card for Los Cerros Middle School 

Page 1 of 9 

 

Mission Statement 
Los Cerros Middle School’s mission is to inspire and prepare all students to apply the intellectual knowledge learned, skills, and character 
traits necessary to become creative problem solvers and to achieve personal success as thoughtful citizens who responsibly contribute in 
our community and digital world. 
 
School Profile 
Los Cerros Middle School, established in 1966, is located in Danville and serves 6-8th grade students in the Northern San Ramon Valley. 
Los Cerros has been awarded the National Blue Ribbon, California Distinguished School, and California Gold Ribbon designations.