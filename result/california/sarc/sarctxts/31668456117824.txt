Mission: 
H. Clarke Powers is committed to working with our community and our students to create 
internationally-minded lifelong learners through rigorous academic standards. 
 
Vision: 
To support students in reaching their full potential, our staff embraces all students through a shared 
responsibility in an engaging, caring, and collaborative environment. 
 
Core Values: 
As a school community, we will exhibit compassion, respect, integrity, and responsibility in our 
everyday lives. We will strive to foster independence, commitment to learning, and global decision 
making for years to come. We will respect our differences and celebrate our strengths and 
achievements. 
 
District & School Profile 
The Loomis Union School District is located in the Town of Loomis a quaint, family-oriented 
community. Established in 1850 and incorporated in 1984, Loomis retains its rural character and 
charm with its large residential lots and custom homes, an old-fashioned downtown, and 
woodlands with natural streams and rolling hillsides. Loomis is located about 25 miles northeast of 
Sacramento and is only ten minutes from Folsom Lake and a little over an hour away from either 
Lake Tahoe or the San Francisco Bay. 
 
Loomis Union School District is comprised of six elementary schools and a charter school, serving 
students in transitional kindergarten through eighth grade. H. Clarke Powers Elementary School is 
located in a rural section of Loomis and is surrounded by farms, acreage, and natural trees native 
to the area. The staff is enthusiastic, gives of their time and is committed to the cause of every 
student being a successful learner. 
 

----

---- 

Loomis Union Elementary School 

District 

3290 Humphrey Road 

Loomis, CA 95650 

(916) 652-1800 

www.loomis-usd.k12.ca.us 

 

District Governing Board 

Jim Foster, Area 1: Citrus Colony 

Kurt Turner, Area 2: Loomis 

Jacob Hardey, Area 3: Rock Springs 

Todd Wilson, Area 4: Placer 

Ann Baker, Area 5: Franklin 

 

District Administration 

Gordon Medd 
Superintendent 

Jay Stewart 

Associate Superintendent - 

Business Services 

Brittaney Meyer 

Assistant Superintendent - 

Educational Services 

 

2017-18 School Accountability Report Card for H. Clarke Powers Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

H. Clarke Powers Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

22 

24 

23 

1 

0 

0 

0 

0 

4 

Loomis Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

133 

3 

20 

Teacher Misassignments and Vacant Teacher Positions at this School 

H. Clarke Powers Elementary 
School 
Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

16-17 

17-18 

18-19 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.