Edwin Markham Middle School, a California Gold Ribbon School in the Placerville Union School District, serves approximately 450 
students in grades 6 through 8. The school is situated in the community of Placerville in the Sierra foothills 40 miles east of Sacramento 
on Highway 50. Our school is named after Edwin Markham, poet, author and educator who lived in El Dorado County during the early 
1900s. 
 
Our mission statement is at the core of what drives our campus: Ensuring every student’s intellectual and emotional growth while 
promoting effective citizenship. Building relationships and academic foundations are the goals of the caring and hard-working 
Markham staff. From the bus driver to the classroom teacher, the custodial staff to the office staff, the Markham family knows all 
campus adults have a role to play in our students’ learning. Families are an important component in the success of Edwin Markham 
Middle School. The School Site Council and Parent Club are two teams composed of parents and community members who work side-
by-side with our staff to create a successful school. Parents and community members volunteer in many ways: in the classroom, for 
daily yard/lunch supervision, field trips, dances and other school activities. Back-to-School Night, Open House and twice-yearly 
Student-Parent-Teacher Conferences are the framework of parent support for student academics. 
 
The facilities at our school are a focal point for the Placerville community as well. Our facilities are in use seven days a week with 
community and recreation programs sharing the grounds on most days. Our gymnasium, multi-purpose room and athletic field are 
examples of Markham and outside organizations working in tandem for the benefit of all residents. Markham is truly a hub of 
community life for Placerville. 
 
Our students truly have “Panther Pride,” working diligently under the direction and leadership of our teaching and instructional staff. 
Since the 2012-13 school year, we’ve made tremendous strides in implementing the new Common Core State Standards. Rigorous 
classroom instruction and a strong commitment to ensuring that all students succeed are the cornerstones of our teachers’ practices. 
Our students are taught in an atmosphere in which they are valued and where in turn, our students value learning. In addition, our 
school offers an array of opportunities to engage students in their campus community outside of the classroom by participating in 
clubs, music, support programs, and athletics. Over one third of our students are involved in some extra-curricular activity. On a daily 
basis, students demonstrate evidence of “Panther Pride” and are recognized through our PAW Program (Panthers Always Win), 
becoming eligible for weekly drawings of rewards. 
 
Throughout our campus, from the classroom to the cafeteria, from classified to certificated staff, we provide a school environment 
where students are the focus.