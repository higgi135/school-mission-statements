La Mirada High School has a rich tradition of academic excellence. For over 50 years we have been 
sending our students to colleges and universities of their choice and preparing them for careers in 
the workforce. The Matador Scholar Academy (MSA) continues to provide a learning community 
that offers an honors/Advanced Placement (AP) path for those students who want to be challenged 
academically, well prepared for college, and who wish to earn college credit while still in high 
school. Currently the school offers 22 different AP Courses. Our three academies also provide 
enrichment activities and specific academic focus to help students thrive in the high school setting. 
The APPLE academy prepares students for careers with children, the ACE academy provides future 
business leaders/entrepreneurs focused coursework and experiences in a global, competitive 
society and the VAPA (Visual and Performing Arts) academy which provides opportunities for 
students to explore both visual and performing arts. La Mirada also has two PLTW (Project Lead the 
Way) pathways that encourage students to explore either Engineering or Bio-Medical courses. Both 
pathways are articulated with local universities and provide internship opportunities as well. In 
addition our welding program is articulated with Santa Ana College and prepares our students for 
full certification in one year post graduation. Our extracurricular activities, internships and clubs 
enrich our student lives and help them thrive in high school. We continue to provide a competitive 
and successful athletic program. Student athletes compete on twelve different teams, including 
football, baseball, wrestling, girls’ softball, girls and boys’ volleyball, soccer, basketball, track, 
tennis, and swimming at league, CIF, and state levels. 
 
Despite state-wide challenges in rapidly decreasing enrollment, La Mirada continues to experience 
an increase in enrollment. Parents, as well as the community at large, are vital to the success of 
all of our endeavors, and we invite you to join us for a new year of growth and accomplishment. 
 

 

 

----

--- 

Norwalk-La Mirada Unified 

-

School District 

12820 Pioneer Blvd 
Norwalk, CA 90650 

(562) 868-0431 
www.nlmusd.org 

 

District Governing Board 

Ana Valencia Board President 

Jude Cazares Board Vice President 

Darryl R. Adams Member 

Jorge Tirado Member 

Karen Morrison Member 

Jesse Urquidi Member 

Chris Pflanzer Member 

 

District Administration 

Dr. Hasmik Danielian 

Superintendent 

Dr. Patricio Vargas 

Assistant Superintendent - 

Educational Services 

Estuardo Santillan 

Assistant Superintendent, Business 

Services 

John Lopez 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for La Mirada High School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

La Mirada High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

102 

90 

94 

0 

0 

0 

0 

0 

2 

Norwalk-La Mirada Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

898 

2 

4 

Teacher Misassignments and Vacant Teacher Positions at this School 

La Mirada High School 

16-17 

17-18 

18-19