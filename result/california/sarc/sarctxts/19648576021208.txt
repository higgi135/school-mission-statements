Palmdale School District Mission Statement 
 
The mission of the Palmdale School District is to provide each child with a rigorous and relevant academic education, a safe learning 
environment and the knowledge, skills and attitudes necessary for success in the 21st Century. 
 
Yucca Elementary is a TK - 5 Title I school in the Palmdale School District, located on the east side of Palmdale. It was built is 1955 and 
is one of the oldest schools in the Palmdale School District and is located in the center of town in a low socio-economically 
disadvantaged community. 
 
Yucca serves a diverse and transient community of approximately 634 learners in grades Transitional Kindergarten through 5th, which 
includes 3 Special Day classes in Kindergarten through Second Grade. Approximately 97% are eligible for free and reduced lunch. 75% 
of the student population is Hispanic/Latino; the African American population comprises 22%; Caucasian is 2% and other comprises 
1%. Yucca's English Language Learner population consists of 181 students. In the 2017-2018 school year we reclassified 27 students. 
In the spring of 2019, there are possibly 40-50 English Learner students who may reclassify. We currently have 22 Resource Students 
(K-5). 
 
Yucca's staff is composed of a variety of professionals providing services that include: 1 Resource Specialist, 1 Speech Pathologist , 
Classroom Teachers, and 1 Psychologist. Yucca has 32 classified employees and 7 casual employees. Of the 32 classified positions, 7 of 
them are instructional aides that assist classroom teachers in grades Kindergarten through Fifth. 
 
The staff consists of a Principal, Assistant Principal,1 Learning Support Teacher, 1 part-time EL Coach, 23 General Education teachers, 
1 Physical Education teacher, 1 Resource teacher, 1 Speech Pathologist, 1 Secretary, 1 Health Aide, 1 part-time Bilingual Clerk, a full 
time Parent Liaison, a part-time library aide, a full-time media tech, 3 full-time custodians, a Student Behavior Interventionist and 7 
instructional assistants. 
 
Yucca Elementary is committed to students academical but also to their social-emotional needs. It is a Capturing Kids' Heart National 
Showcase School which means that great emphasis is put on building relationships with all of the students so that they can achieve 
academically, socially, and emotionally. The school provides character assemblies, luncheons with the school administration for those 
students chosen for the character trait awards, school resource deputy contests, anti-bullying lessons and activities, field trips and/or 
science camp which is free for 5th grade students. 
 
Yucca's School Plan offers the on going opportunity for teachers to plan, implement, monitor and evaluate a meaningful standards-
based curriculum for all students. The use of Illuminate and Google Drive is fully implemented, allowing teachers and other key staff 
members to have access to student academic records. Teachers utilize Professional Learning Community meetings to focus and 
collaborate on Common Core Units, Lesson planning and the RTi process which addresses the specific academic/behavioral areas of 
need. Teachers collaborate to create an action plan which is designed to improve student achievement and behaviors. 
 
The Learning support teacher works closely with the teachers to support the learning of the students. Her primary duties are to work 
closely with teachers and the at-risk students. She has designed a plan for the 7 instructional assistants to conduct intervention groups 
with the most at risk students. The data that was used to determine which students were most at risk was the District's Universal 
Screening and Diagnostic tests which includes the Daze, running records, the BPST and STAR 360. 
 
The school's goals, objectives and activities have been identified and written in part, based on needs expressed through teacher, 
student and parent surveys. Also used to guide instruction is the analysis of District LPM and the ELPAC assessments. These goals, 
objectives and activities are established to improve instruction at Yucca, and to provide students an opportunity to meet grade level, 
district and state standards. 

2017-18 School Accountability Report Card for Yucca Elementary School 

Page 2 of 10 

 

 
The Principal, Assistant Principal, Learning Support Teacher, in collaboration with the Leadership Team, School Site Council, and ELAC 
developed the plan and will continue to work towards the yearly implementation, evaluation and revision of this plan. 
 
The Yucca School community is committed to promoting a positive learning environment that fosters and nurtures a love of life-long 
learning for all students. Our mission is to help all students: 

• develop and maintain oral language proficiency 
• master basic skills 
• develop critical thinking skills 
• demonstrate respect for self and others 
• become productive citizens 
• maintain cognitive engagement in all academic areas 

We are a school that believes that all children will: 

reach their full potential 

• 
• develop respect for self and others 
• become productive citizens 

A successful school experience is the foundation to life-long learning. Recognizing this, we commit to providing a safe, healthy, and 
caring environment.