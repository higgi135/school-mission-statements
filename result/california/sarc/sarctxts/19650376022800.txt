Our mission is to ensure that all scholars have the academic skills and character traits that are 
necessary to be successful adults. We know that our scholars have several external factors (socio-
economic status, English as a second language, foster youth, etc.) that make learning complex. We 
are dedicated to unwrapping the complexities and providing scholars with the tools to cope with 
and overcome external factors. We promise to value the diversity of our scholars' population and 
work diligently to provide the best instructional practices. We will honor each scholar by providing 
a safe and positive environment where everyone can learn and develop intellectually, socially, and 
physically. We seek to instill an appreciation for cultural sensitivity, a positive attitude toward self 
and others, and the responsibility for lifelong learning skills. Together, the staff at Carmela, our 
parents, and our community stakeholders will empower students to prepare for an ever-changing, 
diverse, competitive, and complex world. 
 
In accordance with our mission, Carmela offers several programs to assist in the full development 
of a scholar. Academically, we provide whole group instruction with research-based student 
engagement strategies and GLAD strategies throughout the curricular day. We also provide small 
group instruction and an RTI intervention model for striving scholars. During this intervention, 
scholars are provided with extra opportunities to learn the core curriculum. Additionally, we 
provide lessons on the top 10 scholarly character traits. Scholars are expected to model these 
behaviors and are reminded/rewarded monthly at our Spirit Assemblies. Additionally, Carmela is a 
PBIS school with an anti-bully program. 
 

----

---- 

South Whittier School District 

11200 Telechron Avenue 

Whittier, CA 90605 

(562) 944-6231 

www.swhittier.net 

 

District Governing Board 

Elias Alvarado, President 

Sylvia Macias, Vice President 

Deborah Pacheco, Clerk 

Jan Baird, Member 

Natalia Barajas, Member 

 

District Administration 

Dr. Gary Gonzales 
Superintendent 

Martha Mestanza-Rojas 

Associate Superintendent, 

Educational Services 

Mark Keriakous 

Associate Superintendent, Business 

Services 

Dr. Marti Ayala 

Associate Superintendent, Human 

Resources 

Sandra Jashinsky 

Director, Special Education & 

Student Services 

Stacy Ayers-Escarcega 
Director, Assessment, 
Accountability & Parent 

Engagement 

 

2017-18 School Accountability Report Card for Carmela Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Carmela Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

20 

16 

16 

0 

0 

0 

0 

0 

0 

South Whittier School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

138 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Carmela Elementary School 

16-17 

17-18 

18-19