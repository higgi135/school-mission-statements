Description of District 
In 2017-18 Lindsay Unified School District (LUSD) was comprised of six elementary schools, one comprehensive high school, and three 
alternative education schools. During the 2017-18 school year, the district served approximately 4,111 students (source: CALPADS) in 
grades K-12. The LUSD Mission Statement is “Empowering and Motivating for Today and Tomorrow.” 
 
Description of School 
Loma Vista Charter School served approximately 26 students in grades 9-12 in 2017-18. Our learning facilitators and staff are dedicated 
to providing a positive learning environment and learner-centered learning experience for all learners. In addition to Loma Vista 
Charter School’s core academic program, a wide range of support services are available to students. All programs, practices, 
interventions, and supplemental activities are focused on ensuring learner academic achievement and personal success.