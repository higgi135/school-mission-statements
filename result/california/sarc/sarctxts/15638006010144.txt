Welcome to Parkview School. At Parkview we believe that a successful educational program must 
include respect as well as cooperation between home and school. To achieve this goal, we 
encourage parents to participate in the decision-making process. With the help of parents and the 
community, Parkview strives to achieve its goal of educating every child to the fullest possible 
extent. 
 
Parkview School is located in Taft and draws its student population from both incorporated and 
unincorporated areas of Taft . The school is within the city limits and has sidewalks, sewers, and 
public parks. Parkview School is one of four primary schools in the Taft City School District and 
serves up to 350 students in grades TK-3. Two Transitional Kindergarten classes are now in 
operation servicing 25 students in each from around the city of Taft. We are very proud of our TK 
classes and the amazing growth these students make to prepare for Kindergarten. Tremendous 
changes have been implemented in the areas of curriculum planning, staff development, student 
assessment, and intervention. We now have teacher leaders in each grade level who help to drive 
our curriculum development and implementation as well as our student achievement in test scores. 
Additionally, we have the Teacher Induction program and our District supports our interns, PIPS 
and STPS by providing mentors to these teachers. Parkview is proud that we received the 2016 and 
2017 CA Honor Roll Awards for our State test scores. 
 
Parkview Elementary School’s Vision and Mission Statements 
 
Mission 
Taft City K-3 Schools as a learning environment will empower our students to be successful, 
responsible, and well-educated citizens. 
 
Vision 
Taft City K-3 schools will set the standard for excellence in public education, now and in the future. 
 
 

2017-18 School Accountability Report Card for Parkview Elementary School 

Page 1 of 9