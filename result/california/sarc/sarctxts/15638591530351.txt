Wasco Independence High School (WIHS) is a continuation school in the cit y of Wasco. WIHS serves as the learning center for the 
Wasco community housing two comprehensive high school programs - Continuation and Independent Study Programs. The school 
strives to empower students to achieve their educational, vocational, and personal goals in a caring environment that produces lifelong 
learners with the education, skills and values to be productive, responsible citizens. 
 
Vision 
"Believing in Educational Excellence for All" 
 
Mission 
Meet the unique educational, social, and emotional needs of our student population in a equitable and rigorous learning environment 
and to prepare each student to pursue post high school opportunities.