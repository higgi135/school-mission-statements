During the 2018-19 school year, A.G. Currie Middle School has placed continued focus on taking academic risks, engaging in a rigorous 
curriculum and making/building relationships with our students through year 2 implementation of Restorative Practices. 
Instructionally, we have placed a strong emphasis on best first or Tier I instruction along with how we respond when students 
understand or don't understand the content in order to maximize student learning. The schools vision is to be a National Blue Ribbon 
School through developing relationships between and among students and teachers, in order to grow students who are socially and 
emotionally connected, passionate for learning, and academic risk-takers within a rigorous curriculum. In order to support that vision, 
Currie's mission is to provide a safe, rigorous learning community that prepares and inspires all students to be the best version of 
themselves with limitless options. 
 
To help accomplish our mission and vision, the administration, students, staff, and parents are encouraged to instill three core values 
that best represent A.G. Currie Middle School. Those three core values are illustrated in what it takes to be RAD. Respect Myself and 
Others, Accept Responsibility and Do the Right Thing. We believe in the importance of developing resilience within our school 
community of being determined during tough times to work through the difficulties our students face on a daily basis. We are 
accountable to ourselves, as staff, students, parent, and community members to build relationships with students, to prioritize, 
embrace challenges, set goals, practice, take risks, compete and finish strong in all we endeavor. We will work in unity toward common 
goals with consistency in learning and behavioral expectations, instructional strategies, and supports to aid all students in meeting 
high levels of achievement. Our students will develop the courage and personal integrity to take academic risks, to achieve high levels 
of achievement by setting goals, and stretching beyond their comfort levels. 
 
We believe a promising strategy for achieving the mission of A.G. Currie Middle School is to develop our capacity to function as a 
professional learning community. We continue to be a school in which all stakeholders have high expectations for all and we accept 
no excuses for below standard work. With the implementation and reinforcement of our core values, as well as the data that is 
collected to help drive our instruction, we are confident that all students will work at the highest of levels with limitless options. 
 
The school is working toward growth in all areas specifically in three areas. School wide goals include: By May 2019, Currie Middle 
School students will receive instruction based on a proactive response to student achievement through evaluation of student learning 
and adjustment of instruction so that all grade levels and departments will achieve at least 50% of their unit goals for student 
achievement as measured by department created end of unit summative and formative assessments. By May 2019, students will 
receive effective First or Tier I instruction through the implementation of collaborative structures, learning objectives and independent 
practice time with frequent feedback to improve learning. As a result, 70% of students will score proficient or higher as measured by 
two district wide writing prompts. By May 2019, 75% of Currie Middle School Students with Disabilities will meet at least 75% of their 
IEP goals through incorporation of the MTSS Strategies during our Tutorial and Directed Studies periods as well as the inclusion of 
alternative research based intervention programs.