Yuba City High School is one of two comprehensive high schools in the Yuba City Unified School District. It was founded in 1923 and 
for 83 years was the only 9-12 comprehensive high school in the district. In the late 1990's and early 2000's, the school's student 
enrollment was the largest of any high school north of Sacramento. In 2004, the school enrollment reached nearly 3,000 students. 
Today, Yuba City High School has over 1800 students, and continues to maintain all of the academic, social, athletic, and college and 
career readiness programs of a large comprehensive high school. 
 
Yuba City High School is proud of its course offerings, extensive elective and robust Career and Technical Education (CTE) career 
pathways which include: a) Agriculture & Natural Resources, b) Arts, Media & Entertainment, Education, c) Child Development and 
Family Services, d) Engineering & Architecture, e) Fashion & Interior Design, f) Health Science & Technology, g) Hospitality, Tourism & 
Recreation, h) Information & Communication Technologies, I) Manufacturing & Product Development, j) Public Services, and k) 
Transportation. As a result of offering an extensive selection of classes, Yuba City High School is a host school to over 150 co-enrolled 
students who are not included in the total enrollment. Co-enrolled students come to us from Marysville High School, Sutter High 
School, River Valley High School, Albert Powell Continuation High School, Yuba City Independence Academy, and the Sutter County 
Superintendent of School Special Education programs. Our co-enrolled students take academic and elective classes such as German, 
Physical Education, Music, Drama, Ceramics, Drawing, Beginning Art, as well as classes in the CTE career pathways. 
 
Yuba City High School offers numerous opportunities for students to be involved in a variety of on-campus clubs and organizations, 
one of which is our FFA program which competes all year in a multitude of agriculture competitions. Students also have the 
opportunity to be involved in interscholastic athletics and compete on one or more of the 38 CIF athletic teams in various sports in 
the Tri-County Conference. 
 
After graduation, our students represent YCHS at many of the major universities and colleges throughout California and the nation. 
Our community is particularly proud of the many achievements of our students in the classroom and on the athletic field. 
 
Mission Statement: 
Yuba City High School educates and supports students so that graduates will be literate, meet academic standards, establish goals for 
their future, and demonstrate involvement in school and/or community. 
 
Vision Statement: 
The Yuba City High School community will foster a rigorous, comprehensive, and supportive environment. YCHS students will 
demonstrate the knowledge and skills needed to achieve their academic, personal, and career goals as responsible, contributing 
citizens. 
 
Expected Schoolwide Learning Results: 
 
Demonstrate Literacy: 
Read, comprehend, and write at the 10th grade level 
 
Meet or exceed academic standards established at Yuba City High School: 
Achieve competency in the standards of any course 
Achieve College and Career Readiness status in ELA and Mathematics on the Smarter Balanced Assessments 
 
Establish Goals: 
Develop a plan to achieve individual short and long term goals, utilizing available resources 
 
Participate in a school activity/and or community organization: 

2017-18 School Accountability Report Card for Yuba City High School 

Page 2 of 14 

 

Be involved in school sports, clubs, and community volunteerism