Webster Elementary School, a 2004 California Distinguished School, serves approximately 620 
kindergarten through 6th-grade students. Webster Elementary is located in a residential area 10 
miles east of the city of Madera and 10 miles northwest of the city of Fresno. The Golden Valley 
Unified School District was established in 1998 and is led by a five-person school board. Webster is 
one of two elementary schools in the District. Webster's students feed into Ranchos Middle School 
and then into Liberty High School. 
 
Webster Elementary is more than academics, we instill in our students the values that make 
Madera Ranchos an ideal place to live and raise children. In 2014, The Bonner Center for Character 
Education and Citizenship based at Fresno State’s Kremen School of Education and Human 
Development recognized Webster Elementary for its character education program. The 
recognition reads Webster is being recognized “For Exemplary Commitment to the Character and 
Virtues Education of Its Students.” The Webster staff and community are committed to educating 
the whole child. We emphasize and reward students for displaying positive character traits in and 
outside of the classroom. We believe that victory without honor is no virtue. We aspire to achieve 
with character. 
 
Mission 
The Webster community works collaboratively to create a positive school climate. We strive to 
develop good character and focus on achieving personal and educational goals. We take pride in 
increasing student achievement through exemplary practices and resiliency. 
 
Vision 
Achievement with Character. 
 
Belief Statements 
We believe that: 
Learning is contagious in a respectful environment. 
Learning is relevant, authentic, and appropriate. 
Character development is essential to a well-rounded education. 
Competition creates perseverance, pride, and motivation. 
 
 

----

--

-- 

Golden Valley Unified School 

District 

37479 Avenue 12 

Madera, CA 93636-8726 

559-645-3570 

www.gvusd.k12.ca.us 

 

District Governing Board 

Brian Freeman 

Mona Diaz 

Maria Knobloch 

Andy Wheeler 

Steven Lewis 

 

District Administration 

Rodney Wallace 
Superintendent 

Kevin Hatch 

Assistant Superintendent of 

Educational Services 

Kuljeet Mann 

Director of Human Resources and 

Student Services 

Maureen Hester 

Director of Business Services 

 

2017-18 School Accountability Report Card for Webster Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Webster Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

23.5 

25.5 

27 

3 

0 

1 

0 

0 

0 

Golden Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

99 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Webster Elementary School 

16-17 

17-18 

18-19