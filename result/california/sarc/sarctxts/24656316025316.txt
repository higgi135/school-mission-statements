Aileen Colburn is a preschool to sixth grade school serving approximately 426 students. The school is a non-busing school and maintains 
a warm community climate. "Our mission is to inspire greatness through the consistent implementation of a rigorous academic 
program with collaboration among staff, students, and parents while building an outstanding community of learners. The school has 
made strides with technology to enhance and enrich instruction. All classrooms have data projectors and document readers. Chrome 
books are used in each grade level to enhance instruction. 
 
The student population is ethnically diverse with a great majority of students, currently 98%, qualifying for free and reduced lunch. 
Spanish is the language spoken by the majority of the families and 43% of the students are classified as English language learners. 
Character building is an important part of the school program. The staff uses the Character Counts program which targets good 
citizenship in all aspects of school life. A student leadership group of fifth and sixth graders, "Cougar Leaders", model community 
service for all students. Each month an awards assembly is held to honor students for character and academic achievements. 
 
Reading prevention and intervention programs are used to promote reading growth and fill gaps in reading for all students 
kindergarten through sixth grade. Data is used to diagnose reading strengths and weakness and to help form the best reading and 
language arts instruction for each student. 
 
The Aileen Colburn staff works diligently to provide a caring, collaborative atmosphere focused on maximizing each student's academic 
and social growth.