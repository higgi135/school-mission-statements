Olympic View School is one of 49 schools in the Chula Vista Elementary School District, including 
charters. Olympic View was built in 1995. Each of its five academic buildings has four classrooms 
and a workroom. Fourteen permanent portables have been added to accommodate Olympic View's 
growth. 
 
Mission 
The Olympic View community believes ''It takes an entire village to raise a child.'' Our children are 
at the center of all our decisions. Olympic View Elementary School provides a rigorous, well-
balanced education, preparing students for the 21st Century. 
 
We value the development of the whole child. At Olympic View, students reach their highest 
potential intellectually, physically, emotionally, and socially. Building on a strong foundation of 
academics, students acquire the skills to be literate, creative, constructive, and contributing 
citizens. 
 
Olympic View students are prepared to compete in a technologically advanced world. They value 
tolerance, respect and diversity. Upon leaving Olympic View, students are responsible, resilient, 
lifelong learners who ''make every minute count''. Students appreciate Olympic View as an 
essential building block in their personal and academic growth. 
 
Our entire community embodies the Olympic spirit by modeling positive human relations. Families, 
students, staff, community, and businesses work together to make decisions while accepting the 
responsibility for the success of our children. We ensure a safe environment in which all individuals 
have a sense of belonging and everyone is treated with dignity and respect. Olympic View serves 
as a center where activities and programs enrich the entire community in the areas of fine arts, 
academics, technology, and physical well being. The Olympic View community is dedicated to 
preparing our students for the ever-changing future. Olympic View's Shared Vision is the result of 
collaboration among students, staff, parents, EastLake High School, EastLake Middle School, the 
EastLake Educational Foundation, and community businesses. 
 
 

----

---- 

Chula Vista Elementary School 

District 

84 East J Street 

Chula Vista, CA 91910-6100 

(619) 425-9600 
www.cvesd.org 

 

District Governing Board 

Leslie Ray Bunker 

Armando Farias 

Laurie K. Humphrey 

Eduardo Reyes, Ed.D. 

Francisco Tamayo 

 

District Administration 

Francisco Escobedo, Ed.D. 

Superintendent 

Jeffrey Thiel, Ed.D. 

Assistant Superintendent, Human 
Resources Services and Support 

Oscar Esquivel 

Deputy Superintendent, Business 

Services and Support 

Matthew Tessier, Ed.D. 

Assistant Superintendent, 

Innovation and Instruction Services 

and Support 

 

2017-18 School Accountability Report Card for Olympic View Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Olympic View Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

31 

0 

Teaching Outside Subject Area of Competence 

NA 

32 

0 

NA 

31 

0 

 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.