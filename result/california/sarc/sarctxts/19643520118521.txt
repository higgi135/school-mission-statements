Principal’s Message 
Centinela Valley Independent Study School (CVISS) is an alternative school for students who have extenuating circumstances that do 
not allow the to attend a traditional school on a daily basis. Students are given the opportunity to work from home while still receiving 
a rigorous load of online coursework and having typical high school experiences. CVISS provides planning for life after high school 
with monthly academic counselor presentations, weekly visits from El Camino academic counselors, field trips to higher education 
options and speakers visiting campus. All students participate in one 1-hour class per week (content of their choice) to act as an 
instructional supplement to their computer-based coursework. Students and parents like the flexible schedule, web-based learning, 
intervention plan and easy access to teachers. 
 
District Profile 
Centinela Valley Union High School District is located next to the 405 freeway in the city of Lawndale, approximately 20 miles southwest 
of the city of Los Angeles. During the 2014-15 school year, the district’s three comprehensive high schools, one continuation school, 
one independent study school, and one community day school served a total of 6,661 students in grades 9-12 residing in the cities of 
Hawthorne, Lawndale, and Lennox, and unincorporated portions of Los Angeles County. The district serves four feeder districts, 
Hawthorne Elementary, Lawndale Elementary, Lennox Elementary, and Wiseburn Elementary. 
 
District Vision 
The Centinela Valley Union High School District is committed to providing an educational environment in which all students succeed. 
Students will be prepared to meet the challenges of higher education and the future demands of a modern technological society. 
 
School Profile 
During the 2017-2018 school year, Centinela Valley Independent Study School served 59 students in grades 9-12. Most students who 
experience difficulty in a traditional comprehensive high school setting often find success in an alternative program. The independent 
study structure enables students to progress through a standards-based curriculum at their own pace and learning level. Individualized 
instruction with certificated staff ensures students successfully acquire the necessary skills, knowledge, and concepts in all subject 
areas.