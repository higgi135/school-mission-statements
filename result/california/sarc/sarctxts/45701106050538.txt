This is Susanna Winstead's seventh year at Sycamore Elementary. She has been an administrator 
for thirteen years, and in education for 22 years. 
 
Sycamore Elementary school is a No Excuses University, with the vision of Creating Successful 
Learners and Stewards of the World. As a school community we believe that every student will 
achieve his or her potential social-emotionally, behaviorally, and academically. We are committed 
to explicitly teaching self-regulation, citizenship, and social emotional learning, as well as 
academics. 
 
As a Capturing Kids Hearts district, we build positive relationships and create an environment for 
students to learn how to build positive relationships with each other, as well as adults. We are one 
of two schools in our district to be honored by Capturing Kids' Hearts as a National Showcase 
School. 
 
We have a college going culture and have the belief that we have the responsibility to prepare our 
students for middle school, high school, and ultimately their choice to go to college, vocational 
school, or the military. 
 
We are also proud to offer the choice of enrolling students in a dual immersion program, with the 
target language being Spanish. It is a 90/10 model, where in TK-1 grades, 90% of the day is in 
Spanish and 10% in English, then 2nd grade 80% Spanish and 20% in English and so on until 5th 
grade where students would be fully bilingual and bi-literate with doing all subjects 50% Spanish & 
50% English. 
 
Sycamore Elementary school is committed, courageous, collaborative, creative, character 
centered, and college crazy! 
 

----

---- 

Redding Elementary School 

District 

5885 East Bonnyview Rd 

Redding CA, 96099 

(530) 225-0011 

www.reddingschools.net/home 

 

District Governing Board 

Bruce Ross - Board President 

Stephen Martinez - Vice President 

Beckie Luff - Clerk of the Board 

Peggy O'Lea 

Nathan Fairchild 

 

District Administration 

Robert Adams 
Superintendent 

Robert Fellinger 

Chief Business Official 

Cindy Bishop 

Dir. Educational Services 

Cindy Trujillo 

Dir. Human Resources 

Tawny Cowell 

Dir. Facilities/Nutrition Services 

Kim Bryant 

Dir. Intervention Services 

Seth Hemken 

Dir. Technology 

 

2017-18 School Accountability Report Card for Sycamore Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Sycamore Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

8 

0 

0 

10 

10 

0 

0 

1 

0 

Redding Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

143 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Sycamore Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.