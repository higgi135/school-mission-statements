At Timber Point we are "Creating a Better Tomorrow." We do this by nurturing the development of 
students as learners and human beings. The following are some of our tenets that help us realize 
the goal of Creating a Better Tomorrow for each student: We believe Timber Point is a safe 
environment where everyone feels welcomed, valued, and respected; we believe our staff and 
parents work together to provide a well-rounded education with high expectations for all; we 
believe our staff cares and takes responsibility for the education of all Timber Point students. As 
members of a growing global community, it is important to respect the rights and differences of 
each person, and to acknowledge that everyone has a contribution to make. Through a positive 
support system, realizing that caring and supportive relationships influence individuals in positive 
ways, and by recognizing we are each responsible for our actions, we help to maintain the well-
being of the whole person, and thus nurture an atmosphere where everyone can learn and achieve. 
School-wide. A code of conduct policy clearly states the responsibilities of maintaining a classroom 
environment that allows a teacher to effectively communicate with all students in the class; allows 
all students in the class to learn; has consequences that are fair and developmentally appropriate; 
considers the student and the circumstances pertaining to each situation; and is accordingly 
enforced. By learning from the past, living in the present, and keeping an eye on the future, our 
individuals and community yield unlimited potential. 

2017-18 School Accountability Report Card for Timber Point Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Timber Point Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

25 

25 

25 

0 

0 

1 

0 

1 

1 

BYRON UNION SCHOOL DISTRICT 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Timber Point Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.