Mission and Vision Statement 
The staff at Serra High School nurture students to envision and realize their full potential. Serra is a safe and caring environment where 
students experience a second chance to achieve academically and grow socially. At Serra, students who have faced many personal 
and academic obstacles discover their own resiliency by making good personal choices that will lead to graduating with a high school 
diploma, successful lifelong learning, and a fulfilling career path. 
 
We are the small school with a BIG heart! 
 
Schoolwide Learner Outcomes 
 
BE RESPECTFUL 
 
Students will: 

 

• Demonstrate honest, ethical, and courteous behavior 
• Understand and respect the diversity of individuals/cultures 
• Resolve conflict through effective communication 

BE RESPONSIBLE 
 
Students will: 

• Attain proficiency levels in all academic areas 
• Demonstrate proficiency in written and oral communication 
• Communicate effectively in reading, writing, and speaking 

BE READY 
 
Students will: 

• Acquire the ability to set achievable short term and long term goals 
• Exhibit the social skills needed to be productive members of their school/community 
• Explore careers and higher learning options to become successful lifelong learners beyond graduation 

For additional information about school and district programs, please visit www.capousd.org