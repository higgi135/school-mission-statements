Our mission at California Middle School is to provide students with a rigorous, relevant and 
engaging curriculum, thereby creating a community where students are empowered to think 
critically and acquire the skills they need to be college and career ready 
 
Our school strives to build mastery in all areas of the curriculum. We provide rigorous, standards-
driven instruction in language arts, history, physical education, science, and mathematics. Our 
programs are tailored to the individual needs of our students and each student is programmed 
individually based on multiple sources of data. California Middle School also offers a variety of 
elective courses such as Student Leadership, Art, Band/Orchestra, and Speech and Debate, Culinary 
Arts, Advanced Drama and Robotics. Our beautiful Library and technology carts are available for all 
of our students. After-school programs support the teaming environment at California Middle 
School. We offer tutoring, computer-assisted learning, clubs, and sports teams including basketball, 
volleyball, cheer leading, cross country, soccer, track, softball, and golf. Students may also 
participate in extra-curricular activities such as our annual Spring Play, Air Rock, annual Lip Dub, 
and a number of middle school academic competitions. 
 
At California Middle School, students, faculty, parents, and visitors work as a caring community. 
We value diversity and promote a “can do” spirit. We demonstrate these values as we maintain a 
welcoming, helpful, clean, and safe environment for all of our students. 
 
We also believe that it takes an entire community to raise and educate a child. Therefore, we 
encourage partnerships with the community and parents. We value all stakeholders as critical in 
the education of our students. Frequent expressions of such support will encourage our students 
to do well and will go a long way in promoting their achievement in school. Parents can help by 
assisting our children with the understanding that we all expect their best effort. Parents are also 
encouraged to participate in our PTSA, SSC and other decision-making councils. Through a culture 
of shared responsibility, we support the learning of all students here at California Middle School. 
 

 

 

----

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Chad Sweitzer 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for California Middle School 

Page 1 of 9