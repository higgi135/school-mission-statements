About Our School 
 
“Where Young Minds Meet Open Doors” 
SANTEE SCHOOL DISTRICT VISION, MISSION, BELIEF STATEMENTS, AND GOALS 
 
Adopted May 1, 2012 
 
MISSION STATEMENT 
 
Santee School District assures a quality education, empowering students to achieve academic excellence and to develop life skills 
needed for success in a diverse and changing society. 
 
VISION STATEMENT 
Santee School District will be an innovative leader in education, 
inspiring students to realize their unique potential. 
 
 
BELIEF STATEMENTS 
Children are our first priority. Therefore we believe…. 
All students can learn. 
Student growth, academic performance, and positive personal development are the highest measures of student and district success. 
Trust, integrity, respect, citizenship, honesty, responsibility, commitment, and pride are the foundations on which our district is built. 
Students should understand and respect the origin of the nation, the law of the land, and the principles of our democracy. 
Parent and community involvement in our schools is crucial to the academic success of our students. 
Knowledgeable, motivated, and inspired employees assure the success of our students. 
Everyone has the right to learn and work in a safe, healthy, orderly, and clean environment. 
The district operates efficiently and effectively through focused leadership, fiscal responsibility, and open communication, with a 
strong academic program as the top priority. 
 
BOARD GOALS 
Educational Achievement 
Assure the highest level of educational achievement for all students. 
 
Learning Environment 
Provide a safe, engaging environment that promotes creativity, innovation, and personalized learning 
 
Fiscal Accountability 
Financially support the vision, mission, and goals of the District by maximizing resources, controlling expenses, and managing assets 
to ensure fiscal solvency and flexibility. 
 
Staff Development 
Implement a staff development plan as the cornerstone of employee performance and growth. 
 
Student Well-Being 
Provide social, emotional, and health service programs, integrated with community resources, to foster student character and 
personal well-being. 

2017-18 School Accountability Report Card for Carlton Oaks Elementary School 

Page 2 of 11