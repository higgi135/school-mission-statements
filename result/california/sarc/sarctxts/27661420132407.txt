The Dual Immersion Academy Salinas- Boronda is dedicated to preparing all students linguistically, 
academically and socially to contribute to, and thrive in, an ever-changing world. 
 
Dual Immersion Academy Salinas - (DIAS Boronda) opened fall 2015 with one transitional 
kindergarten class and two kindergarten classes. The school is adding dual immersion classes every 
year through 2021-2022 to become a TK-6 program. Based on the 90:10 model in the first two 
grades (90% Spanish and 10% English), Dual Immersion Academy of Salinas students incrementally 
increase the amount of English to a 50:50 model in the upper grades. In our program dual language 
or two-way bilingual immersion program, all students are engaged in second language study for 5-
7 years in an effort to develop high levels of bilingualism (the ability to speak fluently in both 
Spanish and English) and biliteracy (the ability to read and write in both languages). In addition to 
immersing students in language and literacy, the educational program integrates and emphasizes 
STEAM: Science, Technology, Engineering, Arts, and Mathematics. 
 

 

 

----

---- 

-------- 

Salinas City Elementary School 

District 

840 S. Main Street 

Salinas 

(831) 753-5600 

www.salinascityesd.org 

 

District Governing Board 

Art Galimba 

Francisco Javier Estrada 

Kathryn Ramirez 

Amy Ish 

Stephen Kim 

 

District Administration 

Martha L. Martinez 

Superintendent 

Sara Perez 

Assistant Superintendent 

Business Services 

 

Lori Sanders 

Assistant Superintendent 

Education Services 

 

Beatriz S. Chaidez, Ed.D 
Assistant Superintendent 

Human Resources 

 

 

2017-18 School Accountability Report Card for Boronda Elementary School -Dual Immersion Academy Salinas (DIAS) 

Page 1 of 8