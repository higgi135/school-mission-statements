Newcastle Elementary School and District, located at the gateway to the foothills, first opened to 44 first through eighth-grade 
students in 1865. Although the building location has moved three times and enrollment for the elementary school has grown to over 
400 students, it is still a small and close-knit community school, serving transitional kindergarten through eighth-grade district and 
charter students on the same campus and in the same classes. 
 
Motto: 
"Newcastle Knights – Gems of the Foothills, Shining Above the Rest" 
 
Vision: 
Newcastle School is the cornerstone of a lifelong community of learners where students, staff, families, and community work together 
to ensure that students are successfully prepared for rigorous higher education coursework, career challenges, and a globally 
competitive workforce. Students are prepared to the highest level of social, moral, and academic development. 
 
Mission: 
The Newcastle Elementary School District is committed to work in partnership with the home and community. Our students will be 
provided with an academic program designed to develop the 21st Century skills necessary to become active and effective global 
citizens. Our students will be supported in a safe, positive learning environment that meets all students’ needs, and fosters healthy 
academic, social, emotional and physical development. 
 
Newcastle Elementary/Charter School enjoys the reputation of having a very strong academic and social program with capable, caring 
staff and administration. The school provides for academic achievement through a rigorous curricular program focused on the 
common core standards and rich in arts and technology. In the Newcastle school community, we believe that every child can be 
successful, but not necessarily in the same way or on the same day and we work together to create opportunities for success for all 
students. 
 
Students receive differentiated instruction daily through flex time in the four core areas. Each class has the benefit of an instructional 
assistant to support instruction and small groups during this time. All students in K-7 receive weekly instruction in Spanish and music. 
Sixth through eighth-grade students participate in the exploratory program where elective classes such as foreign language, choir, 
character development, and technology skills are offered. Students who qualify may participate in additional activities such as after-
school sports, STEM classes, leadership, yearbook, history club, science/adventure club, and among others, Odyssey of the Mind. 
Newcastle Elementary/Charter School recognizes the social and academic value of a positive environment. A school-wide student 
recognition program honors exemplary student behavior and citizenship through the Stellar Knight program, Student of the Month, 
monthly character traits, and academic excellence. 
 
 

2017-18 School Accountability Report Card for Newcastle Elementary School 

Page 2 of 10