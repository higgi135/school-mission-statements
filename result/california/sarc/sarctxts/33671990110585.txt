The School Accountability Report Card is issued annually for each school in the State of California and provides an overview of selected 
conditions related to the school, its resources, its accomplishments, and the areas in which improvements may be needed. 
 
As you review the report for Sky View Elementary School, I believe that a school dedicated to improvement and meeting the needs of 
students will be evident. Our school’s mission is to provide a safe learning environment with a respect for diversity and well-balanced, 
rigorous instruction, Sky View Elementary School in partnership with our community, will inspire students to reach their academic 
potential and become productive global citizens. Sky View teachers and staff believe in placing students first and continue to dedicate 
themselves to the profession by modeling expectations necessary to build scholars and character for tomorrow's challenges. Our 
students will continue to grow academically and socially because of the relevant, rigorous and standards-based instruction delivered 
by our instructors. With effective implementation of student-centered instruction, our highly qualified teachers will tie comprehensive 
learning to college and career readiness. 
 
Sky View school goals include: 
1. All students by the culmination of their fourth year at Sky View Elementary School will be reading for understanding. 
 
2. All children will show mutual respect while being educated to the best of our ability in a clean, safe and caring environment. 
 
3. Sky View staff will create an environment that promotes ethics and emphasizes responsible behavior with fidelity to PBIS and AVID 
strategies. 
 
When you visit our campus, you will see the latest technology to enhance the instruction delivered and students participating in an 
array of educational opportunities that support their elementary school experience. We encourage all parents to stay actively involved 
in their child/children’s education by becoming a volunteer, joining a Sky View parent partner group and attending school events. 
Together, we will make a positive difference in the life of Every Student, Every Day! 
 
Opportunities for parental involvement include: 

School Site Council (SSC) 
English Learner Advisory Council (ELAC) 

• 
• 
• Class/Office Volunteer 
• 
Supervision Volunteer 
• 
Family Involvement Action Team (FIAT) 
• WATCH D.O.G.S. (Father Involvement Program) 
• Attendance/Kindness Counts Campaign 
• Civic Learning Action Projects 
• 
• AVID Parent Workshops 

Joy of Writing (Gold Ribbon Program) 

The wealth of experience available in the community is a resource that Sky View Elementary uses to enrich the educational 
programming. To participate in any of the above opportunities, contact the school office and the volunteer policy can be found on the 
school's website. 
 

2017-18 School Accountability Report Card for Sky View Elementary School 

Page 2 of 12