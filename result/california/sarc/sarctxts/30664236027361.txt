Gauer Elementary School is one of 23 schools in the Anaheim Elementary School District. Located on the western border of the 
district, Gauer School has 649 TK-6 students that come from rich and diverse ethnic and socioeconomic backgrounds.Fifty-three 
percent (53%) of our children are English learners and sixty-six (66%) are on free or reduced meals.Gauer has a principal, vice 
principal, curriculum coach, and digital coach that work closely with the teachers to analyze data, set goals, and implement effective 
teaching strategies.Gauer has a Teacher on Special Assignment and an RSP Teacher that provide small group instruction in the areas 
of English-language arts and mathematics. Students that are struggling academically at a Tier 3 level or have an active IEP participate 
in one of our intervention programs during the instructional day. In addition to designated English Language Development (ELD) for 
30 minutes a day, English Learners are provided with integrated ELD support in all content areas. Students in our GATE (Gifted and 
Talented Education) Program participate in learning experiences with a focus on teaching/learning across the curriculum with 
greater depth and complexity. 
 
On a monthly basis, teachers meet together in Professional Learning Communities.There has been a strong emphasis in 
implementing a more standards-aligned curriculum. Staff members use pacing guides, assessments, and other pertinent information 
in the immediate and long-term planning of lessons. Gauer School reviews student assessment data on an ongoing basis and 
modifies instruction accordingly to effectively reduce the barriers to our students’ academic achievement. The staff, students, and 
community are dedicated to ensure that high expectations for academic excellence is maintained for all of our students. 
 
Gauer’s school mission statement: At Gauer Elementary School we nurture creativity and innovation through a STEAM infused 
education. Our school provides a safe and supportive environment where every child achieves educational excellence. We maintain 
strong partnerships with parents and community to prepare all students with the necessary academic and social skills to support 
lifelong learning. 
 
Gauer’s Vision: Gauer Elementary School prepares students to be successful innovators and problem solvers in order to meet the 
challenges of today and tomorrow.