San Gorgonio High School campus encompasses 50 acres and has 182 classrooms, a library, an auditorium, a multipurpose room, and 
an administration office. The campus was built in 1965 and modernized in 2012. Five new special education classrooms were built in 
the 2006-07 school year. The facility strongly supports teaching and learning through its ample classroom and athletic space, and a 
staff resource rooms. 
 
San Gorgonio High School is a comprehensive community-related school, which encourages academic challenges, development of 
individual talents and skills, and positive connections between students and the community.