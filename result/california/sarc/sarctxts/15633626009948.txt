Panama Elementary School, established in 1875, is the oldest of nineteen elementary schools in the Panama-Buena Vista Union School 
District. The original school was established in 1875 and the present school site in 1939. The original school was named after the 
swampy area where it was built due to its similarities to the Isthmus of Panama. Located at 9400 Stine Road in the growing southwest 
area of Bakersfield, it is rapidly changing from a rural to a suburban school. The students in grades K-6 are culturally, ethnically, and 
socioeconomically diverse. Our majority population is Hispanic at 65% with 16% White, 11% Asian, 6% African-American, and 2% other. 
About 75% of our students participate in the Free and Reduced school lunch program. To ensure the achievement of all students, and 
to meet the needs of an English learner population of 25% and our low income students, we provide a half time assistant principal, an 
academic coach, two full time reading intervention teachers, and a Sheltered English Immersion (SEI) kindergarten class. All students 
receive a rigorous, standards-aligned curriculum. Those students with special needs receive additional support through our 
intervention teachers, our MTSS, and Panama Accelerated Learning (PAL). A highly trained support staff, including the school nurse, 
psychologist, and speech pathologist help design interventions and supports for students with special needs. The music specialists 
and GATE program enhance and extend the curriculum. The School Accountability Report Card was established by Proposition 98, an 
initiative passed by California voters in November 1988. The Report Card, to be issued annually by local school boards for each 
elementary and secondary school in the state, provides for parents and other interested people a variety of information about the 
school, its resources, its successes, and the areas in which improvements are needed. 
 
Programs for all students at Panama Elementary are systematic and explicit. The culture of collaboration supports the goal of collective 
efficacy, so important to the continued success of our students. Staff, families, and community members work together to provide a 
learning environment with high expectations for all learners. Our combined efforts are focused on our students' academic, physical, 
social, and emotional growth. A dedicated staff rises to the challenge of helping all students reach proficiency through highly effective, 
research-based instructional practices and sound assessment strategies. Teacher collaboration and articulation ensure that the 
kindergarten through sixth grade experience prepares students for junior high, high school and beyond. 
 
Panama School’s motto, “A Learning Community Since 1875” provides the focus for our vision, which is two-fold: 1) to enable each 
student to acquire the academic, social, and emotional skills to compete successfully at higher academic levels, and 2) to enable each 
student to become a productive member of society, both professionally and personally, and to exercise the rights and responsibilities 
of citizenship. Specific goals guide all members of our school community who work to accomplish the following: a) all students will 
develop a strong foundation of academic and higher order thinking skills, b) staff, students and their families, and the broader 
community will be involved as partners to promote the social, emotional, and academic success of students, c) the school will provide 
a safe and nurturing learning environment in which students develop positive character traits, a sense of responsibility, and effective 
decision-making skills, and d) students will leave Panama with the knowledge and understanding to work collaboratively with others 
and respect individual differences, the ability to communicate effectively, and the necessary skills to be a contributing member of 
society. 
 
Panama Elementary School received funds from the state based on the state's funding formula which provides extra funding for English 
learners, students on the free/reduced lunch program, and foster students. In addition, Panama Elementary received federal Title I 
funds. 
 
 

2017-18 School Accountability Report Card for Panama Elementary School 

Page 2 of 11