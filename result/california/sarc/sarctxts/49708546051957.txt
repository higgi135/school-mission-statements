McDowell Elementary School is a TK-6 school located in Petaluma, 40 miles north of San Francisco. Our school is one of seven 
elementary schools in the Petaluma City Elementary School District. We are served by a board of education that also governs the 
Petaluma Joint High School District. McDowell serves approximately 270 students. 
 
McDowell’s campus is a place of learning for more than just elementary students. From parenting support by Petaluma People Services 
to full time guidance specialist, from a free and confidential Family Resource Center to adult classes offered through Petaluma Adult 
School, McDowell is a place of care, community, and learning for all! We also partner with organizations such as Mentor Me and 
welcome volunteers to our campus. 
 
Our school is dedicated to equipping students with 21st century skills of communication and critical thinking. Every student has access 
to an iPad for learning, where they are supported to learn various apps and programs that support the development of these skills. 
Experienced teachers integrate problem-solving into their daily instruction, and promote student engagement through Academic 
Conversations (Zwiers and Crawford 2011). 
 
McDowell students experience a rich curriculum aligned with the Common Core State Standards. With over a century of experience 
between them, our wonderful classroom teachers differentiate and scaffold learning for their learners, realizing that all students have 
different strengths and needs. Our English Learners benefit from integrated and designated language instruction, multiple bilingual 
staff, and social studies content embedded into weekly Spanish language classes. Our students with special needs and challenges are 
supported through intervention programs that provide push-in and small group support, as well as free after-school tutoring provided 
by classroom teachers. All students at McDowell are supported to achieve! 
 
McDowell families get involved in campus life in many ways. Parents receive information about their child’s educational program 
through principal newsletters, report cards, parent-teacher conferences, Parent Teacher Association (PTA) meetings, ELAC (English 
Learner Advisory Committee) meetings, and classroom newsletters. Some teachers communicate with parents via apps like Class Dojo 
and Remind. Our fully bilingual front office staff helps our families stay connected to all the wonderful things happening on campus. 
 
We are very united by our vision statement: All McDowell students are actively engaged learners and communicators. Our school 
community supports bi-literacy, compassion, critical thinking, academic achievement, and contribution to our global society. 
 
Our school community is dedicated to our four core values: respect, responsibility, compassion, and perseverance.