Desert View Community Day School opened to students the summer of 2008. The school educates 
students in grades fourth through eighth who have been placed there due to behavioral or 
attendance issues. 
 
The goal of Desert View Community School is to provide students a strong academic program while 
assisting students in learning life skills so that they may return back to their regular school program. 
The school offers a low student to adult ratio. Instruction is provided by a multiple subject teacher 
and support is offered from two part-time instructional aides. A half-time special education teacher 
also serves student needs. Students’ academic expectations align with district schools. Students 
participate in district curriculum, have access to computer-based learning, and are monitored 
through district assessments. 
 

 

 

-
-
-
-
-
-
-
- 

----

---- 

Keppel Union Elementary School 

District 

34004 128th Street East; P.O. Box 

186 

Pearblossom, CA 93553 

(661) 944-2155 

www.keppelunion.org 

 

District Governing Board 

Matthew Gaines, President 

Dominique Ballante, Vice President 

Georgia Halliman, Clerk 

Jannie Dutton, Member 

Theresa McCafferty, Member 

 

District Administration 

Dr. Ruben Zepeda II 

Superintendent 

Dr. Jesus Luna 

Assistant Superintendent of 

Instructional Services 

Christine Goulet 

Interim Director of Support 

Services 

Ward Lunneborg 

Interim Director of Business 

Services 

Christine Goulet 

Director of Special Education 

Lilian Arreguin 

Director of Child Nutrition Services 

Vacant 

Director of Construction, 

Maintenance & Operations 

Stacey Nestlerode 

Business Services Supervisor 

Karina Hoffman 

Executive Assistant and Secretary 

to the Superintendent 

 

2017-18 School Accountability Report Card for Desert View Community Day 

Page 1 of 6