Lincoln Elementary School is privileged to educate 370 preschool through fifth grade students in 
the Tulare City Elementary School District. We are dedicated to recognizing the unique value of 
each student, attaining the highest academic standards, and providing a safe and supportive 
environment for active learning. We realize that education is an integrated partnership between 
home, school, and community; a partnership that is critical in the formation of happy and 
productive citizens. Please join us in the adventure of elementary education, and don't hesitate to 
contact me with your questions or concerns. 
 
Annual Schoolwide Plan Review 
Members of the Lincoln Leadership Team, staff members, and parents review the Lincoln 
Schoolwide Plan throughout the year. Planning for updates and revisions takes place at the 
conclusion of the school year. Updated information is added to the official document at the 
beginning of the next school year. A special emphasis is placed in meeting all objectives in the plan 
according to the timeline established for their completion. 
 
In order to meet the requirements of the Common Core Standards, Lincoln Elementary School is 
continually in the process of aligning core curriculum as well as implementing targeted intervention 
programs for identified students. 
 

--

-- 

----

--- 

-

Tulare City School District 

600 North Cherry Street 

Tulare, CA 93274 
(559) 685-7200 
www.tcsdk8.org 

 

District Governing Board 

Teresa Garcia 

Irene Henderson 

Melissa Janes 

Phil Plascencia 

Daniel Enriquez 

 

District Administration 

Dr. Clare Gist 

Superintendent 

Philip Pierschbacher 

Assistant Superintendent, 

Personnel 

Joyce Nunes 

Assistant Superintendent, 

Business/Psychological Services 

Paula Adair 

Assistant Superintendent, Student 

Services 

Brian Hollingshead 

Assistant Superintendent, 
Curriculum/Technology 

 

2017-18 School Accountability Report Card for LIncoln Elementary Schoo 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

LIncoln Elementary Schoo 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

17 

16 

15 

0 

0 

0 

0 

2 

0 

Tulare City School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

450 

27 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

LIncoln Elementary Schoo 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.