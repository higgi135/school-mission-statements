Wright Charter School (WCS) is the original of three K-6 elementary schools in the Wright School 
District. WCS was founded in 1865. Beginning 2009-2010, Wright Charter School expanded its 
services to include a middle school that serves students in grades seven and eight. We are very 
proud to offer a small school environment for grades TK-8. 
 
Wright Charter School serves approximately 500 students who represent a diverse population. 
Approximately seventy-two percent of Wright School’s students participate in the Federal 
Government’s Free and Reduced Lunch Program. Fifty-two percent of our students are English 
Language Learners. The majority of our second language learners speak Spanish as their primary 
language. 
 
Wright Charter School proudly offers a range of services to meet the diverse learning needs of our 
students. Our classes include eighteen regular education classes, a middle school learning lab, a 
Resource Specialized Program, music, garden, and library classes. In addition, we offer students 
Speech and Language services, English Language Learner services, Title 1 Reading services, tutoring 
services and counseling. We have a brand-new multipurpose room, which is used for assemblies, 
school events, sports, music, and as a cafeteria for breakfast and lunch. Our students are fortunate 
to have meals prepared for them on site in our school kitchen by experienced school food service 
employees. We fondly refer to our cafeteria as the “Cougar Café.” 
 
Through a partnership with CalServes, our school also offers students an after-school program in 
which about one-hundred-fifty students participate in enrichment activities and receive help with 
homework and reading practice. The Extended Child Care (ECC) Coalition provides before and after 
school care for students. 
 
Mission and Vision Statement 
 
The Wright Charter School is founded on the principles of Ecological literacy: the understanding 
that every living system is related to and affected by every other living system. 
 
Ecoliteracy promotes the development of social, emotional and ecological intelligence through an 
ever-evolving understanding of the relationship between and among all living systems. 
 
Our mission is to foster in our students the desire and capacity to live and contribute responsibly 
in society and to envision and achieve their goals, both in their own lives and as stewards of the 
planet. Our students will be educated to meet or exceed grade level expectations and mastery of 
core academic standards. Our students will develop an understanding of their responsibilities as 
global citizens and be educated to be contributing members of a larger society. We strive to 
identify, nurture and support the unique capabilities of every student. 
 
Our vision is to create an environment that encourages every child to work to his or her potential, 
that builds basic skills, that kindles and nourishes curiosity, that teaches problem solving, that 
encourages children to love learning, and that inspires both teachers and children to pursue 
academic challenges. 
 

2017-18 School Accountability Report Card for Wright Charter School 

Page 1 of 9 

 

Wright Charter School provides opportunities for children to develop personal and civic responsibility, self-discipline, necessary life skills 
for continuous education and economic independence, and a positive code of ethics supported by the community at large. We respect 
each individual, honor differences, and we strive to strengthen the community by educating its children and engaging families in the 
educational process.