Vision Statement: Students will prepare for tomorrow's world through today's meaningful 
experiences while 
implementing the 6 C's: Collaboration, Creativity, Critical Thinking, 
Communicating, Character Education, and Citizenship which will prepare them to be productive 
members of society. 
 
Mission Statement: All members of Selma Herndon will Believe in themselves, Excel in academics, 
Achieve high academic standards, display Respect for themselves and the community, and overall 
“WE” will Succeed… “WE” are the Selma Herndon BEARS!!! 
 
Selma Herndon Elementary School (SHE) is located in Livingston, California serving TK through 5th 
grade students. The current total student enrollment is 633. The ethnic makeup is: 82.46% 
Hispanic, 13.72% Asian, 2.39% White, .32% American Indian/Alaskan Native, 0.32% Black/African 
American, 0.32% Hawaiian/Pacific Islander, and 0.47% other. In addition, 83.88% of students are 
Socio-Economically Disadvantaged, 61.40% are English Learners (ELs), and 4.31% are in Special 
Education. 
 
The school is composed of a diverse group of staff members that consists of instructional aides, 
literacy tutors, academic clinicians, and an academic coach who provide instructional support in all 
classrooms. Each member brings a caring and talented energy for student learning. It's because of 
such diverse and culturally accepting individuals that a love for learning is fostered for all students. 
Staff members bring forth ideas and positive challenges that motivate all student learners. The 
school provides support to the teaching staff, SHE also provides student support services through 
a school site counselor, health aide, district nurse, district speech pathologist, and district 
psychologist. The school offers a well-articulated Dual Language Academy Program in grades 
Kindergarten through 5th grade, and a Structured English Immersion Program for TK-5th grade. 
 

 

 

----

---- 

----

---- 

Livingston Union School District 

922 B Street 
Livingston, CA 
(209) 394-5400 

www.livingstonusd.org 

 

District Governing Board 

Mr. Vernon Boyd 

Mr. Rigo Espinoza 

Mrs. Anne Land 

Mrs. Yolanda Correia 

Mrs. Kanwaldeep Bains 

 

District Administration 

Mr. Andrés Zamora 

Superintendent 

Mrs. Sara Crawley 

Director of 

Fiscal and Business Services 

 

Mrs. Kuljinder Sekhon 

Assistant Superintendent of 

Instruction and Student Services 

Mrs. Maria Torres-Perez 

Director of 

Categorical Programs and 

Special Projects 

 

Mr. Nick Jones 

Director of 

MOT and Facilities 

 

Mrs. Tiffany Pickle 

Director of Instructional 

Technology 

 

2017-18 School Accountability Report Card for Selma Herndon Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Selma Herndon Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

31 

28 

31 

0 

0 

2 

0 

0 

0 

Livingston Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

115.5 

0 

1 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.