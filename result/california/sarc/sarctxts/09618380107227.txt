Principal’s Message 
Valley View Charter Montessori is a transitional kindergarten through seventh grade elementary 
school located in El Dorado Hills, 18 miles east of Sacramento in the Sierra foothills. Our beautiful 
campus creates a positive learning environment for children with its buildings situated among open 
green spaces and many stately oak trees. Built to completion in 2013, Valley View Charter 
Montessori opened its doors to student in 2017-18. In 2017-18, Valley View had an enrollment of 
520+ students and had a waiting list in most grade levels. This current year, 2018-19 we have added 
seventh grade and have 630+ students with 14 new staff members. 
“The Buckeye Union School District values, encourages, and provides opportunities for the 
involvement of staff, parents, and the community in school and District decision-making activities.” 
As such, the District moved forward in preparing the Montessori Charter petition to respond to 
identified, expressed needs from parents and students within the District. The program began in 
the 2004-2005 school year by serving Kindergarten students on the Blue Oak Campus. Grade levels 
were added the following 10 years and in 2017 the school moved to Valley View. Now, we currently 
serve students through 6th grade. Next year (2019-20) we will add 8th. The Charter Montessori 
School follows the instructional methodologies, standards, and educational direction established 
within the Buckeye Union elementary District respecting the Montessori philosophy and utilizing 
Montessori materials and learning strategies. In addition, the Charter Montessori School/Program, 
in utilizing the Montessori methodology, will incorporate core sections in Practical Life, Sensorial 
Materials, Language, Mathematics and Cultural Subjects. Valley View Charter Montessori also 
hosts a Montessori pre-school operated by the El Dorado Country Office of Education 
 
Inclusion of all students is a high priority for us. Special Education students are mainstreamed to 
receive a balanced curriculum in the least restrictive environment. Both staff and parents pay close 
attention to subtle changes in our students that may indicate a need for additional support and 
assistance. Our community is generous and caring in an atmosphere where cooperation and 
respect is highly valued. We have taken specific action to implement the Common Core State 
Standards along with the Learning Center model for our SPED. All of our grade level through the 
development of a comprehensive standards based report card that holds both students and 
teachers accountable for specific academic achievements. 
 
At Valley View Charter Montessori we have high expectations for student achievement. We are 
committed to providing students with a solid academic background in a safe and supportive 
environment. We challenge students with academic curriculum that requires them to apply skills 
and knowledge to meet district standards. Our intent is to build an academic foundation that will 
allow students to be successful in middle school, high school, and beyond. 
The newly renovated and updated campus added a new Band Room, Middle School Girls and Boys 
Locker Rooms, 3 additional classrooms that are used for Middle School. We added a fully functional 
STEAM Lab that is next to our library. Our library has one of the finest literature collections in El 
Dorado County. Students in all classes meet with the library assistant weekly to choose age-
appropriate books and hear quality literature read out-loud. Along with 3rd-5th grade 1:1 
Chromebooks, Kindergarten through 2nd grade has 14 Chromebooks and at least two iPads in each 
room. . This year, our PTO helped our school district in purchasing Chromebooks for more 
classrooms to continue piloting and utilizing this new technology. Teachers are provided monthly 
training to use the technology in their rooms with the most effective and best practices. 
 

2017-18 School Accountability Report Card for Valley View Charter Montessori 

Page 1 of 8 

 

The staff of Valley View Charter Montessori strives to work together in a collaborative manner to provide an education that will nurture 
and build the academic strengths of each child. Our dedicated and innovative teaching staff shares in leadership responsibilities, knowing 
that solid, research-based planning is the key to a quality academic program. The staff continues to work to develop a program that is 
aligned with the state content and Common Core standards in all subject areas. Included in the program is a comprehensive assessment 
and accountability component. The results of all assessments are used to identify areas of need and to refine instruction. We have 
implemented reading intervention programs during the school day to support under performing students, extra support for our English 
learners, and a targeted, needs-based math and reading intervention for students in grades first through fifth, along with differentiated 
instruction within the classroom. Valley View Charter Montessori is fortunate to have a Literacy, Math and EL Certificated Teacher to 
assist students and teachers in their areas of need. Lastly, our Montessori Mentors offer cross-age tutors from upper grade to help us 
strive toward our vision, “Valley View Charter Montessori is a community of learners where children can reach their full potential and 
contribute positively to society.”