Welcome to Lindo Park! We are passionate about teaching and learning. The mission of Lindo Park 
Elementary School is to work collaboratively to ensure students will engage in dynamic, rigorous, 
and relevant curriculum. Students will develop abilities to enable them to contribute to our global 
society. Students will achieve goals in a safe, nurturing, and respectful environment. Our vision is 
for all students to attain their maximum academic and social potential. 
 
Lindo Park is a neighborhood school serving the Lakeside Community for more than sixty years. We 
are located in a semi-rural community, 25 miles east of San Diego. A 54-acre county park and lake 
are directly across the street from our school. We serve a variety of residential areas. Our 
community is culturally diverse and provides us with a rich cultural heritage. Our keys to successful 
teaching and learning are a highly trained staff, engaging instruction, digital opportunities, 
dedicated parents, supportive community partnerships, and most importantly energetic, resilient 
students who come to school each day wanting to learn and excel. 
 
We have evening activities for parents and students including Family Science Night, Bingo Night, 
PTA School Smarts Parent Academy, and Family Reading Night, in addition to Primary and 
Intermediate Science Club, Spanish Club, After School Athletics, Dance-Choir and Dance Clubs. All 
school enrichment programs are funded by our school site so that all children can enjoy after school 
learning opportunities. We have a dedicated Science Lab that is utilized by all classrooms to present 
hands-on standards-based science experiments and instruction. An expert music teacher offers 
weekly instruction to 5th grade students. 
 
Lindo Park wants students to excel in our global society. Our curriculum is research-based and we 
actively integrate 21st century skills with research-based curriculum. Teachers utilize adaptive 
instructional programs, expert teaching, and student progress is carefully monitored, and shared 
with parents. 
 
All classrooms are equipped with internet access, iMac computers, SMART Boards, remote devices, 
and doc-u-cams. K-5 Teachers and students have access to iPads Laptops, and iMacs. K-5 grade 
classrooms have a 1:1 ratio of iPad Tablets for instruction. Fourth and fifth grade students take 
iPads home to enrich and extend the school day. On-line instructional resources can be accessed 
by teachers and students twenty-four hours a day. Students regularly access digital media, books, 
and related materials. In addition to classrooms, as a part of our STEM Program, we feature a 
MakerSpace for Engineering Design, a school garden, and a Science Laboratory. 
 
Instructional time develops a positive school climate through a social skills curriculum and positive 
behavior supports. All classrooms have common expectations and skills woven into the curriculum. 
Student leadership and community involvement are encouraged through After School Enrichment, 
Student Council, Safety Patrol, Playground Leaders, TechNinjas, and a partnership with Lindo Lake 
County Park. School spirit is promoted by creating a safe and orderly environment focusing on 
rigorous academic achievement and joyful enrichment opportunities in arts, athletics, language, 
and STEM (Science, Technology, Engineering, and Mathematics). 
Lindo Park Staff and PTA focus on parent engagement and community service. We know that when 
families and community are positively involved in students’ schools, academic excellence rises! 
 

2017-18 School Accountability Report Card for Lindo Park Elementary School 

Page 1 of 11 

 

We specialize in: 
Science, Technology, Engineering, Arts, and Mathematics 
21st Century Innovative Skills: Critical thinking, Communication, Collaboration, and Creativity 
Differentiated, dynamic, digital learning for all students to reach their full potential 
Targeted Intervention for all English Learners, Title I students, and At-Risk students 
 
After School Enrichment includes: Spanish Club, After-School Athletics, Dance-Choir, Dance Club, and Science Clubs 
Character Education programming to promote proactive, peaceful behavior 
Before and after school childcare 
 
MISSION 
We will work collaboratively to ensure students become productive, responsible citizens. Students will engage in dynamic, rigorous, 
relevant curriculum. Students will develop abilities, which enable them to contribute to society. Students will achieve their goals in a safe, 
nurturing, and respectful school environment. 
 
VISION 
All students will attain their maximum academic and social potential.