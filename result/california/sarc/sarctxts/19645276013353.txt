Principal’s Message 
Mission Statement: The mission of the staff, parents, and community of North Ranchito is to 
prepare our students to participate, appreciate, and flourish in the rich cultural heritage of their 
community and to successfully navigate through an increasingly globalized world. To prepare our 
students to be global citizens, we will foster a nurturing climate that promotes humanity and a 
passion for lifelong learning. Driven by research-based principles, we are committed to providing 
our students with the tools to promote creativity, critical-thinking, communication and 
collaboration. In meeting these 21st century goals we, as teachers and leaders, employ established 
educational theory, innovative strategies, Standards-based instruction and assessment, data 
analysis, and reflection to promote the academic excellence of all students. 
 
At North Ranchito, every staff member at the school is focused on improving student achievement 
in all areas. The staff, students, and community believe the goal of education is to produce citizens 
who will make a positive contribution to our society. We believe that all students can learn and 
succeed. We have high expectations and standards for our students in the area of academics. A 
knowledgeable, creative, and dedicated staff works effectively each day to provide a Standards-
based instructional program to 450 students. All students from Transitional Kindergarten to fifth 
grade are the focus of all school programs. 
 
At our school this year, our areas of focus are reading comprehension, vocabulary development, 
math application, writing strategies, and increased use of technology for all students. As a team we 
continuously analyze data, and collaborate to ensure success for all our students. Standardized 
assessments assist us to make informed decisions about instruction through our State, District, and 
local measures. Our school is driven by a Standards-based curriculum that is set by guidelines 
created by the State of California. In addition, we utilize research-based strategies and professional 
development to achieve our goals. Students of all abilities, from a variety of linguistic and 
socioeconomic levels, come together as a learning community at North Ranchito. The school 
community expects all students to achieve high standards as we have established in our School Plan 
for Student Achievement (SPSA). 
 
Students approach their education with dedication and enthusiasm. Grades TK-5 students receive 
Trimester Awards where the top performing students are recognized in the areas of reading, math, 
and citizenship. They are eager to receive these awards and challenge each other on the dynamic 
education derived from our Standards-based curriculum. 
 
North Ranchito teachers are well qualified to handle the challenge of delivering a Standards based 
education to a bright and diverse student body. Many teachers live in the community and are 
bilingual and bicultural. Emotionally invested in the school, they arrive early, work late, and serve 
in leadership roles. This is truly a testament to the tireless work and efforts of our staff on the behalf 
of the students and families that they serve. 
 
The School Site Council (SSC) and Parent-Teacher Organization (PTO) are very active on campus and 
meet regularly to discuss how to bolster student achievement and school climate. As part of our 
technology goals and in an effort to promote parent participation, our parents will be offered the 
opportunity to participate in a series of workshops that focus on technology education and use of 
the Internet to support student academic goals. 
 

2017-18 School Accountability Report Card for North Ranchito Elementary School 

Page 1 of 8 

 

The community of North Ranchito is vibrant and enthusiastic. We will continue to reach for the stars and raise expectations for our school 
in the pursuit of excellence. 
 
School Passion Statement 
We at North Ranchito Elementary School will not rest until each student achieves academic success. To accomplish this goal we all commit 
to working together collaboratively, respectfully, with focus and flexibility. 
 
District Mission Statement 
The El Rancho Unified School District will actively partner with the community as its leading educational institution that provides a 
technology rich and innovative learning environment for all students. Students will be challenged to become college and career ready 
and life-long Learners.