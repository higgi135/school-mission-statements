Located in Campbell Union School District, Campbell School of Innovation (CSI) is a new TK-8th 
grade public school option that opened in August 2018 for grades TK through 4th. The school's 
focus is to change the landscape of learning by using Design Thinking and creating a campus that is 
student-centered and fosters collaboration, exploration and discovery. Nearly 300 CSI students 
engage with Design Thinking principles every day, across the curriculum. Furnishings are bright and 
learning spaces are open with flexible seating and common spaces for hands-on, project-based 
learning. The newly built campus will grow to serve students from preschool through 8th grade 
over several years. 
 
MISSION: We are a community that seeks to understand, learns through design, and leads with 
innovation to create a better world. 
 

 

 

----

---- 

----

---

- 

Campbell Union School District 

155 N. Third Street 
Campbell CA, 95008 

(408) 364-4200 

www.campbellusd.org 

 

District Governing Board 

Pablo A. Beltran 

Danielle M.S. Cohen 

Chris Miller 

Richard H. Nguyen 

Michael L. Snyder 

 

District Administration 

Dr. Shelly Viramontez 

Superintendent 

Nelly Yang 

Assistant Superintendent, 
Administrative Services 

Lena Bundtzen 

Assistant Superintendent, Human 

Resources Services 

Whitney Holton 

Assistant Superintendent, 

Instructional Services 

 

2017-18 School Accountability Report Card for Campbell School of Innovation 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Campbell School of Innovation 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

N/A 

N/A 

Teaching Outside Subject Area of Competence N/A 

N/A 

N/A 

N/A 

13 

0 

0 

Campbell Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

305.6 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Campbell School of Innovation 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

N/A 

N/A 

N/A 

N/A 

N/A 

N/A 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.