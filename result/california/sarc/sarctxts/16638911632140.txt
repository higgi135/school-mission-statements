Kings Lake Education Center is an alternative education site that allows students the opportunity to complete their high school 
education and graduate with a diploma in an innovative setting. The campus houses the district’s Continuation High School; district 
Independent Study Program, Corcoran Academy and Corcoran Adult School, and Mission Community Day School. Kings Lake 
Continuation High School serves students in grades ten through twelve, and the District Independent Study Program and Corcoran 
Academy serve students from grades K-12, although most students in those programs are in middle school or high school. The Corcoran 
Adult School is open to adults 18 years of age or older that are looking for ways to expand their knowledge and gain a competitive 
edge. Whether students decide to earn a High School Diploma, sharpen their computer skills, acquire citizenship or learn English, we 
are here to help them pursue their special interests and reach their goals. The Adult Education program employs Three part time 
teachers, one that teaches adult students on independent study, one teaches English as a Second Language (ESL), and one teaches 
our seat-based class for acquiring their diploma. All programs at Kings Lake Education Center are academic in nature with classes’ 
varying in enrollment each semester with individual instruction available as the most common mode of instructional delivery. 
Although additional information is included above, this accountability report is for the continuation high school. 
 
The Mission of Kings Lake Education Center is to increase student achievement, provide safe schools and promote a positive climate 
for all its students.