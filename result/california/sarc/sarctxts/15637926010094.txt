Principal’s Message 
 
Standard Middle School is dedicated to providing an excellent learning experience for our students. Our team strives to lead students 
to greater and sustained academic achievement. We are constantly in the process of mixing the ingredients of highly effective schools 
together in search of maximum student success: Professional Learning Communities, Positive Behavioral Interventions and Supports 
(PBIS), 1:1 Google Chromebooks for all students all day, Restorative Practices in lieu of suspensions (ATS), Inclusion model for special 
needs students, Circle of Friends (COF) leadership special-needs student partnerships, Safe School Ambassadors, Multiple Tiered 
Systems of Support, and regular Data Inquiry. Our skilled faculty and students are eager to succeed. We value respect, honesty, 
integrity, achievement, and the joy of learning. 
 
We provide sixth, seventh, and eighth-grade students with a strong, CA Standards-aligned curriculum in all subjects. Our enrollment 
has been steadily increasing. Our sixth-grade students, who are in a modified self-contained classroom setting, go to P.E. five days a 
week or have P.E. and music for one period on alternate days. Teachers work in teams in sixth grade to share each other’s students 
by focusing instruction on either mathematics or reading/language arts. Our academic focus is on improving student learning in the 
core subject areas of language arts, social studies, math, and science. We have purchased state-adopted, standards-based textbooks 
for all of these subjects. Our teachers are continually working to further align their curricula and refine the instruction to specific 
grade-level standards. 6th Grade also added Warrior Studies as an elective each day for students to receive enrichment in the sciences, 
social sciences and arts in alignment with the CA Common Core Standards for literacy. 
 
Standard Middle School provides intense intervention for the students who are not making growth on the Smarter Balanced CASSPP 
assessments or not showing sufficient success in their language arts or math classes. Standard Middle School added an intensive 
Reading and Language Arts Intervention class that use Core curriculum to continue to improve student achievement. Intervention 
classes use core materials and Renaissance Learning for Math and Language Arts. With the new Chromebook initiative, we have added 
Digital Citizenship curriculum as a requirement for all students in care and behavior for online activity. Staff helps students pause, 
reflect, and chart individual progress on state test scores, GPA, and CFAs to set personal goals for future growth while instilling 
increased ownership of progress and performance. 
 
Warrior Pride, We Have It! 
 
Susan Denton 
Principal