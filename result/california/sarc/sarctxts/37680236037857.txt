Vision: Students of Greg Rogers Elementary School will be prepared, productive, literate, critical 
thinkers who contribute to a global society in the 21st Century. 
Focus: Instruction will combine the elements of listening, speaking, reading and writing across the 
curriculum. 
 
Greg Rogers School is a unique campus that serves both general education students and students 
with disabilities. There are approximately 509 preschool through 6th-grade students currently 
enrolled at this site. The special education population consists of approximately 200 students 
ranging from students with learning disabilities to students with severe disabilities. Services are 
also provided for a state-funded preschool program. Housed at Greg Rogers is California Children 
Services for occupational and physical therapy, Parent Intervention Program(PIP), Support Team 
For Autism Spectrum And At-Risk Students(STAARS), the Occupational Therapy program, and the 
district's Special Education Fair. All students at Rogers Elementary are held to the highest academic 
standards and all teachers have high expectations for student achievement. At Rogers, we believe 
Once a Pirate Always a Pirate and we practice Pirate P.R.I.D.E in many ways. Pride stands for 
Practicing Procedures, being Respectful, being Inclusive, being Dependable and Aiming for 
Excellence. 
 

----

---- 

Chula Vista Elementary School 

District 

84 East J Street 

Chula Vista, CA 91910-6100 

(619) 425-9600 
www.cvesd.org 

 

District Governing Board 

Leslie Ray Bunker 

Armando Farias 

Laurie K. Humphrey 

Eduardo Reyes, Ed.D. 

Francisco Tamayo 

 

District Administration 

Francisco Escobedo, Ed.D. 

Superintendent 

Jeffrey Thiel, Ed.D. 

Assistant Superintendent, Human 
Resources Services and Support 

Oscar Esquivel 

Deputy Superintendent, Business 

Services and Support 

Matthew Tessier, Ed.D. 

Assistant Superintendent, 

Innovation and Instruction Services 

and Support 

 

2017-18 School Accountability Report Card for Greg Rogers Elementary School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Greg Rogers Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

29 

0 

Teaching Outside Subject Area of Competence 

NA 

25 

24 

1 

0 

0 

0 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Greg Rogers Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.