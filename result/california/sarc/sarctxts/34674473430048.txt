The Mesa Verde Mission is: Our community provides diverse opportunities for all students to develop skills necessary to achieve their 
full potential through equitable access to rigorous curriculum, strategic support, and extended learning. 
 
Mesa Verde High School is in the incorporated City of Citrus Heights, a suburb of the City of Sacramento. With an attendance area of 
approximately 1100 students, our current enrollment is 897 students. Within our student population, we serve general education 
students as well as RSP, SDC, ED, and ILS. Our goal continues to be to mainstream a large portion of our Special Education students 
to create an inclusive school that promotes learning for all. Adding to our diverse learning environment, Mesa Verde’s racial 
composition is: 50.6%-White, 34.8%-Hispanic/Latino, 5.5%-African American, 3.1%-Two races or more, 2.3%-Asian, 1.8%-Filipino, 
1.3%-American Indian, 0.7%-Pacific Islander. While our school continues to become more ethnically diverse, we also continue to serve 
a growing number of English Learners. As of the 2017-2018 school year we served approximately 36 EL students (does not include 
students that we serve that have been Reclassified English Learners). Immigrants not yet fluent in English are mainstreamed at Mesa 
Verde, however the San Juan Unified School District, through the District LCAP funding, has provided the necessary FTE for school sites 
to support their English Learners. As a result, Mesa Verde High School now has .67 FTE to provide direct English Learner supports for 
students that meet this designation. 
 
The socioeconomic status of the Mesa Verde community indicates that the City of Citrus Heights is primarily a working-class 
community. The free or reduced lunch program has remained consistent over the past 4-years, averaging 63%, up significantly from 
2010-11 school year. Currently, however, our percentage of families that are identified as low income is 69.8%. 
 
One of Mesa Verde’s signature programs is Advancement Via Individual Determination (AVID). Over the years, AVID has produced 
college-ready students who pride themselves on academic success through their development in study skills, organization, and 
academic rigor. Fifteen percent of our school population (136 students) are enrolled in AVID. Our strength with AVID is due to three 
variables: 1) improved recruiting efforts that ensure the right students are accepted into the program, 2) year-round AVID that 
provides support for the students throughout the entire year and 3) committed team of AVID teachers. One hundred percent of our 
AVID graduates from 2017-18 exceeded A-G college entrance requirements and one hundred percent of the AVID graduates were 
accepted into at least one four-year college or university. In all, the AVID class of 2018 received 102 university acceptance letters. 
 
Mesa Verde High School is also the proud home of a California Business Academy Partnership. Our Business Academy continues to 
provide students with an effective learning environment that is rooted in fundamental business concepts integrated within the 
necessary core high school curriculum. Additionally, the Business Academy provides students (over 50% of which are qualified “at-
risk”) with opportunities to build relationships with peers, teachers, and community members through its school-within-a-school 
approach. Throughout the 3-year program, students travel together from class to class where they develop academically and socially 
through the lens of small business owners. One hundred percent of our senior cohort serve in a business community internship 
through the Regional Occupational Program (ROP). The Mesa Verde Business Academy has developed many business and community 
partnerships. As a result of our partnership with the SAFE Credit Union, we now have a fully operated credit union on our campus, 
which includes a banking counter and a cash dispenser. Additionally, the Business Academy program provides all 10th grade students 
with a two-week job readiness certification program through the Citrus Heights Chamber of Commerce and local business owners. All 
students within the program also test for their ServeSafe food handling safety certification and continue to build their resume with 
hands-on experience running a student store. 
 
Mesa Verde High School now offers three California Technical Education (CTE) pathways to students. Building our community 
connections, Mesa Verde has partnered with the Citrus Heights Police Department to offer a Public Safety pathway that prepares 
students who are interested in a career in law enforcement or a different branch of the legal system. We also offer our arts pathways 
in both Dance and Theater. These programs provide students with an opportunity to complete three continuous classes in each of 
the programs and provide students with real-world application in each of the selected pathways. 
 

2017-18 School Accountability Report Card for Mesa Verde High School 

Page 2 of 16 

 

Our Independent Living Skills (ILS) program focuses on the importance of inclusion for our students as we attempt to mainstream as 
many students as possible, providing them with an enriched academic experience. The Mesa Verde staff is incredibly accepting and 
willing to work with our ILS students to ensure their continued success in and out of the classroom. Our general education students 
also welcome ILS students by working closely with the program as TA’s as well as always working on including ILS students in events 
such as Cheer, Dance, and school activities like rallies and school-wide dances. The ILS students lead the Mesa Recycling Program and 
collect cardboard, paper, aluminum cans and bottles across campus and nearby Carriage Elementary School, which is then delivered 
and sorted at our school’s Recycling Center. The Work-ability Program is also an integral part of preparing our ILS students for 
employment when they graduate high school. Students are bused to local businesses to participate in job readiness tasks and 
programs. ILS students also participate in the “ILS Bowl” which involves approximately 200 and 300 staff and students. ILS students 
district-wide are invited to participate and teamwork and comradery are significant outcomes for our students. Many local charitable 
institutions actively support this annual event. 
 
Our school enjoys strong support from the Mesa Verde Booster Club, who raise funds from the operations at a local bingo hall to 
support numerous school activities, programs, and scholarships. The Booster Club makes it possible for our teams to be outfitted with 
the finest uniforms and safest equipment possible. During the 2014/15 school year, Mesa Verde High School benefited from a $55,000 
donation for two new digital marquees that are placed in the Quad and Big Gym. Additionally, in 2017/18, the Mesa Verde Booster 
Club graciously donated $25,000 towards a weight room re-model to provide all PE students and athletes with a state-of-the-art weight 
room. Additionally, the Mesa Verde Booster club continues their tradition of giving back by providing all graduating Seniors with an 
opportunity to be awarded a scholarship. Over the past 3-years, the MVHS Booster Club has averaged $35,517 in scholarships during 
Senior Awards night. 
 
The School Site Council involves parents, students, classified and certified staff members in the allocation of school funding (LCFF and 
Cell Tower). The intent of School Site Council is to provide an environment for the school to receive valuable input from all 
stakeholders to ensure dollars are being spent in a way that best serves our students. LCFF and Cell Tower funding is generally spent 
on supporting and enhancing school programs, teacher training, professional development, and classroom support and technology. 
 
As Mesa Verde works towards the achievement of our District Strategic Plan and LCAP goals, we will continue to focus on closing the 
achievement gap through instructional improvement and school climate.