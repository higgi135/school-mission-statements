School Mission Statement: 
At Katella High School, our mission is to prepare students for life after high school. 
 
School Vision Statement: 
This vision of Katella High School is to be the best school in America. 
 
In addition to this mission statement, the school is committed to being a reflective learning community as part of a plan to improve 
student learning and achievement. The staff, by departments, has written curriculum that is rigorous, relevant, and that will be 
implemented with fidelity. Katella is committed to improving instruction by using research-based strategies, data-driven decision-
making, and implementing a shared decision-making process for strategic planning. 
 
Educational Highlights: 
Katella High School is very proud of the increase in Advanced Placement Courses offered in the past few years. During the 2017-18 
school year, 400 students took 644 exams. The school currently offers the following Advanced Placement courses: English 
Language/Composition, English Literature, European History, Psychology, American Government, US History, Calculus, Biology, 
Chemistry, Environmental Science, Physics, Computer Science and Spanish Language. 
 
Additionally, Katella High School offers a variety of on-campus Career Technical Education (CTE) pathways and Regional Occupational 
Program (ROP) courses, whereby students may graduate with career distinction. The industry sectors include: Hospitality, Tourism, 
and Recreation; Residential and Commercial Construction; Education, Child Development and Family Services; Public Safety; Design, 
Visual and Media Arts; System Diagnostics and Services; Media Production Arts; Food Service and Hospitality; and Patient Care and 
Emergency Response. 
 
Katella offers a full-time College and Career Center that provides direct support to students applying to college and completing their 
FAFSA forms. Currently, over 90% of our seniors complete a college application and apply for financial aid. 
 
The Band, Choir, and Drama programs are thriving at Katella under two new directors. Our athletic program recently won multiple 
championships. 
 
Demographic Information: 
Katella High School, located in Anaheim, California, currently serves approximately 2,750 students, of which 82.2% are socio-
economically disadvantaged, 21.4% are English Learners, and 11.6% are Students with Disabilities. The demographic profile also 
indicates the following regarding student subgroups: 88.1% Hispanic, 2.6% White, 3.9% Asian, 1.3% African-American, 1.1% Filipino, 
and approximately 2% other ethnicities. These students are served by a staff of 105 teachers, 4 administrators, 5 counselors, and 72 
classified support staff. 
 

2017-18 School Accountability Report Card for Katella High School 

Page 2 of 13