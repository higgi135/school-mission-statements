Principal’s Message 
Gerber Jr./Sr. High School is a community school dedicated to helping at-risk youth reach their full potential academically, behaviorally, 
and socially. While the focus at Gerber Jr./Sr. High School is academic success, teachers and staff understand that before learning can 
take place we must first address the deeper issues that prevented success. Once emotional needs are addressed, students can increase 
their literacy and mathematics competencies, earn credit, gain job skills, graduate from high school and transition into a post-
secondary environment. Besides providing strong behavioral supports, the staff works with students to develop an individual success 
plan created to help set realistic goals they can accomplish while at our school. Our students are exposed to data driven instruction 
designed to remediate and accelerate the curriculum based on the student’s individual strengths and weaknesses. We provide a small 
campus setting with a modified schedule that mirrors a comprehensive program while allowing staff and students to forge close, 
trusting relationships. Each student is encouraged to participate in mentoring, athletics, and Career Technical Education (CTE). Guest 
speakers, assemblies, field trips, vocational goal setting, career exploration/readiness and academic tutoring are just a few 
opportunities for our students. 
 
School Description and Mission 
Gerber Jr./Sr. High School has been in operation since 2006. Since 2008, it has functioned as a community school of the Sacramento 
County Office of Education. The site serves public school students in grades 7-12 referred by a School District, the Probation 
Department, or the School Attendance Review Board (SARB). Students typically transition back to their home district after one or two 
successful semesters and completion of district return requirements; some students complete their education at Gerber with a high 
school diploma or GED.