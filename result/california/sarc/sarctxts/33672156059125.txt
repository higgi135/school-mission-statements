Central is a Magnet Middle School for the Arts, and Innovation in Riverside Unified School District. We are a middle school serving 7th 
and 8th grade students in the Riverside Unified School District. Our Central school mission is to increase our students' rate of academic 
growth while providing them experiences in the performing arts, technology and innovation for future career and college readiness. 
Our intent is to increase the language development of all students in reading, writing and presenting while increasing their 
technological skills and capacity. We work to foster in our students an understanding of our local and global communities providing 
the essential groundwork needed for our students to become socially contributing citizens of our community and have satisfying, 
happy and healthy lives. 
 
Increased academic growth at Central is supported by our high academic expectations of students. Our rigorous curricular content is 
aligned with state standards emphasizing student and family engagement in learning. We analyze student academic growth in reading 
and math through content benchmark assessments such as the Interim Assessment Benchmarks (IABs). Teachers engage in the cycle 
of inquiry to respond to student needs. The cycle of inquiry provides information that allows our teachers to respond to data with an 
actionable plan. Technology provides our students the opportunity to demonstrate learning in new ways. However, we continue to 
employ research based instructional practices as our foundation in addressing the learning needs of our under-represented and most 
at risk students. The focus of our teacher professional development centers on our closing the achievement gap in learning for our 
Hispanic, Socio-Economically Disadvantaged, homeless, special education and English language learners subgroups. We provide 
Central students a positive school climate to support their social and emotional growth. We nurture the unique skills or gifts that every 
student brings to Central. We first identify our students' strengths, interests and social needs. Our staff works hard to provide 
classroom and extracurricular activities that spur student intellectual, artistic, technological and/or mechanical abilities to higher levels 
of expertise. We focus on connecting our students to school, providing them both opportunities for leadership and learning in all 
things. 
 
All students are expected to be involved in at least two or more activities on campus during the year. We champion student 
connections to school through our range of electives (band, piano, guitar, ASB, AVID, AVID EXCEL, art, AP Spanish, digital media art, 
and technology) and our various after school clubs. Over 95% of eligible students were able to take their first choice of elective the 
last three school years. Additionally, we have been striving to provide elective access to our special education and at risk students 
through a zero period theater class and two elective classes (Art and ASB) during 8th period -held after the regular school day. ASB 
8th period has allowed our special education students the ability to be a part of our associated student body and build leadership skills 
(inclusion). It has also provided an opportunity for our other school student leaders to take rigorous classes such as AP Spanish and 
Advanced band during the school day.