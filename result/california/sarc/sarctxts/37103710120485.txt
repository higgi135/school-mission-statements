Davila Day School is specifically designed for children with hearing loss that significantly impacts their speech and/or language 
development and academic skills. Davila’s specialized academic instruction empowers students to reach their full potential. The 
mission of Davila Day School is to provide students who are Deaf or Hard of Hearing (D/HH) a comprehensive, fully accessible and 
language-rich learning environment, with equal access to the general education curriculum and standards. 
 
Davila Day School staff members believe that D/HH students have the right to maximize their personal, educational, social, and 
communicative potential through participating in rigorous educational experiences that develop their ability to think independently, 
critically, and creatively. These experiences, coupled with early access to spoken English and/or American Sign Language, enable the 
students to develop positive self-identities and the skills to be contributing and productive members of society. 
 
Davila Day School serves an average of 45 students in preschool through 6th grade (average student-to-staff ratio is 4:1) who are Deaf 
or Hard of Hearing. Our small student population means personal attention for students and families. Students attending Davila Day 
School reside with their families or foster families primarily within the southern and eastern parts of San Diego County. 
 
Davila Day School is located on the Vista Square Elementary campus in the Chula Vista Elementary School District. All Davila students 
(K-6th) are integrated with their hearing peers for recess and/or lunch daily, as well as music, physical education, visual and performing 
arts, and ASL instruction through collaboration rotations. Students may also join their hearing peers in general education classrooms 
for additional academic instruction. ASL interpreters or classroom assistants accompany students in the general education 
environment and teachers are provided with FM systems as appropriate.