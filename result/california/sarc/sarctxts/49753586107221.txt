At Brooks Elementary School, all students will have the opportunity to develop their talents and skills regardless of their ethnicity, 
gender, economic status, or religion. We look to grow students academically using a variety of instructional strategies and through the 
use of qualitative and quantitative data. We are intentional, purposeful, and passionate about our students, their learning and the 
whole BES community. 
 
It is the mission of the Windsor Unified School District to provide a supportive and nurturing environment for all students. Students 
shall acquire the basic skills of know ledge, along with the thinking skills needed for problem-solving and decision-making relevant to 
a changing world. Windsor students shall exhibit personal and social maturity through responsible behavior developed from 
understanding and respect for the diversity of all life and a genuine caring for others. 
 
Windsor Unified School District is committed to the following: 

Teach to varied learning styles 

Employ technology to enhance learning 

• Model positive, open communication 
• Collaborate with other public agencies 
• Promote inclusiveness in decision making 
• Pattern appreciation for diversity 
• 
• Offer ways to achieve academic progress 
• Provide a system of accountability 
• Create activities to work collaboratively 
• Model and teach critical-thinking skills 
• 
• Offer specialized programs in academics, the arts, athletics, student wellness, and technology 
• Promote student health and nutrition 
• 
• Maintain a safe, nurturing environment 
• Value human resources of the District 
• Cultivate trust and high standards 
• Plan professional staff development 
• Develop financial resources 
• 
• Maintain appropriate facilities 

Foster respect 

Employ sound organizational practices 

Windsor School District is at its best when: 

• Education is a shared, community partnership 
• Students are engaged in learning 
• Schools and facilities are state of the art 
• Specialized programs foster student success 
• A supportive social network is in place 

 

2017-18 School Accountability Report Card for Brooks Elementary School 

Page 2 of 10