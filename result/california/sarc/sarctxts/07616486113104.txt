The Black Diamond community is small and connected. That connectedness has created a peaceful campus with few discipline issues 
and great opportunities for students to both learn and develop socially in a supportive environment. 
 
Mission Statement: 
The staff at Black Diamond Middle School will foster a nurturing environment that promotes and values diversity. We will create a 
family of confident lifelong learners. All students will leave Black Diamond Middle school, collaborative, technologically savvy, and 
prepared for the challenge in high school.