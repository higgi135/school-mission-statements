Every student will engage in rigorous, relevant, standards-based instruction in every classroom 
every day to ensure student learning. Every student at Teague will show PRIDE by; being Proud, 
showing Respect, having Integrity, being Determined, and showing Empathy. 
 
Teague strives to build a healthy, positive, and engaging learning environment for all students. 
Through the Guiding Principles, Teague makes sure every student will learn in every classroom, 
every day. The school is the pulse and the social center of the community. Many families walk their 
children to and from the campus and stay to catch up on school and community news. One of 15 
elementary schools in the Central Unified School District, we are on the western border of the city 
of Fresno and serves the neighborhood of Highway City. Teague Elementary is considered by the 
city of Fresno to be urban fringe area. Teague Elementary School serves 713 Transitional 
Kindergarten through sixth grade students on a traditional schedule. Teague Elementary School 
also houses a state preschool program and shares space with a county special education program. 
Teague celebrates and honors our ethnically and economically diverse community. 
 
Teague staff is comprised of dedicated, dynamic educators who collaborate and utilize best 
practices for student success, with a major emphasis on core academic skills. Teachers are carefully 
selected and spend countless hours honing their professional skills and collaborating and planning 
in grade level teams, curriculum teams, Professional Learning Communities (PLCs) and with 
administration. The staff attends a wide variety of professional development offerings provided at 
the school as well as district wide trainings at the district office. Many teachers also pursue 
advanced level university degrees. Teague Elementary School's goals are based on the Elementary 
and Secondary Education Act, common core standards and Central Unified's Guiding Principles. 
 
Teague is dedicated to increasing and accelerating the learning of our English Learners (EL) with 
quality instruction and Intervention programs. With highly effective and researched based teaching 
strategies, Teague EL students continue to get the instruction needed to become proficient in the 
English language. Teague will plan to use the ELPAC test to determine language fluency levels and 
progress for our significant EL population. This year, teachers will be utilizing dedicated classroom 
time to work with identified EL students utilizing appropriate curriculum to address the specific 
needs of EL students. Teague will also have the support of the English Learner's Group to support 
and coach our school site in EL instruction. 
 
Teague Elementary has a Reading Intervention Program that has been rebuilt to accommodate a 
three-tier program. Tier one of the program provides the opportunity for certificated teachers to 
support the reading instruction for students in the classroom by providing core instruction to all 
students. Tier two is based on F&P scores where students are identified for reading intervention. 
Students are grouped for further reading intervention support. Students two years below grade 
level receive instruction in a small group setting based on their needs. Support is provided through 
our site literacy team that provides three reading teachers. Each team will push in to a classrooms 
and support the tier two students. Students identified more than two years below reading levels 
will then be in tier three where students are pulled out into intensive groups of no more than 5 
students at a time. Students will receive specialized instruction in reading and literacy skill 
development. In addition, tutorials (before and after school) as well as Saturday school enrichment 
time will also be provided to students to give additional time for students to grow academically. 
 

2017-18 School Accountability Report Card for Teague Elementary School 

Page 1 of 9 

 

 
This year Teague teachers are implementing more and more technology based learning tools to students in classrooms every day to close 
the experience gap many of our students face, as well as enhance learning for all.