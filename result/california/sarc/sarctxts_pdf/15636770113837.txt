Hacienda Elementary School opened its doors to students August 2007. The building of the school had been approved by the citizens 
of California City five years earlier when a bond was passed funding the building of a new elementary and high school. The Hacienda 
Elementary student body was formed by joining together students from two other schools. The fourth and fifth grade students from 
Robert P. Ulrich Elementary School and the sixth grade students from California City Middle School were removed from their respective 
school sites to form the student body of Hacienda Elementary School. Along with the students, the fourth and fifth grade teachers 
from Robert P. Ulrich and the sixth grade teachers from California City Middle School were also joined to become the teaching staff of 
Hacienda Elementary School. In the 2015-16 school year the district realigned the California City schools due to CCSS and enrollment. 
Hacienda began serving students 3-5 and the 6th graders were moved to California City Middle School. 
 
The peak enrollment at Hacienda Elementary school in 2017-18 school year was 537 students and we averaged about 520 students 
for the course of the year. The site consists of 7-third grade classrooms, 5- fourth grade classrooms, 4- fifth grade classrooms, a 4/5 
combination and a 4/5 Gifted and Talented classroom, 2 Intervention classrooms, and two Special Day Classes. In addition Hacienda 
has a resource specialist. 
 
School Mission 
 
The mission of Hacienda Elementary School is to provide students with the highest quality of education so they may obtain academic 
excellence in preparation for high school graduation as well as post high school opportunities. We strive to create an individual that 
possesses the skills to become successful in their future endeavors by being awesome J.E.T.S : Joining together to Encourage taking 
responsibility and Showing respect and safe actions. 
 
"We will work together with families and the community to encourage students to realize their potential and give them the skills 
needed to be successful beyond their years at Hacienda. We will create individuals who show respect for themselves and others, and 
who take responsibility for their learning and the decisions they make; and resolve differences by communicating and working to 
understand each other’s motivations while ensuring fairness." 
 
It is our vision to "provide an environment where students are motivated to reach both their academic and personal goals while 
engaging in rigorous and creative lessons. Acceptance of others and mutual respect will be displayed at all times. As a staff we will 
model good character and strive to positively impact the lives of our students. We are dedicated to the belief that all students can and 
will learn." 
 
 
To meet the goals set forth by the school, numerous supports have been put into place for the students and staff of Hacienda 
Elementary School. All students have been provided with curriculum that is aligned with the California Common Core State Standards. 
The curriculum used is consistent grade level to grade level so that students are provided with information that is presented in a 
comprehensive, systematic manner. The majority of teachers at the school site have had intensive training in the use of the common 
core and EDI. Students in grades 3-5 are provided with 2 hours daily of English/language arts instruction and 1 hour daily of 
mathematic instruction. In addition students receive a minimum of 30 minutes of Response to Intervention (RTI) time in 
English/language arts and 30 minutes of RTI time in mathematics four times per week. Response to Intervention is used to re-teach 
students who are struggling in specific skill areas, provide additional practice to those who need it, and enrich students with higher 
level learning needs. Identified English language learners received an additional 30 minutes of EL support using the State approved 
Treasures series. Students who qualify for intensive intervention in reading are assigned to the Read 180/System 44 classrooms. 
 
 

2017-18 School Accountability Report Card for Hacienda Elementary School 

Page 2 of 16 

 

Students who have been identified as Gifted and Talented are offered GATE placement in fourth grade. This placement consists of a 
self-contained, 4/5 combination class. The GATE teacher uses the California Common Core Standards to guide her instruction while 
providing the GATE students with the curriculum and instruction strategies to meet their unique learning needs. These students are 
challenged in a unique learning environment with project based learning. 
Respect, responsibility, and community service are also an important part of the Hacienda students’ educational process. All students 
are expected to be responsible for themselves and their school site. Students are given planners in which they can plan out their 
weeks and track their assignments. Community service is stressed to students so that they realize they have a responsibility to others 
which stretches beyond the school site. The Peer Helper Program has been developed for students who exemplify what being a JET 
is all about. In order to raise awareness and stress the importance of being kind to others an annual "Walk of Kindness" was added to 
the Hacienda events calendar.