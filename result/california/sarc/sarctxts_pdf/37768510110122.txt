Mission Statement: 
Bonsall West staff is committed to fostering the social and emotional needs of each and every child while embracing high academic 
expectations. The school focuses on developing students who demonstrate the six pillars of character and are 'up-standers' , not 
bystanders. Students are taught to be kind to others and to always give 100%. Our theme is Dream Big Bonsall West and Ride with 
Pride! 
 
Message from the Principal 
Bonsall West is a high performing school in which every student is inspired to do their best, be respectful, work hard, think of others, 
and dream big. Our school history began in 2005. We have a vibrant school community where parent participation and involvement 
is the norm, not the exception. Our PTA supports all endeavors, both financially and with volunteers. We have wonderful students 
who truly care about others. We are in our third year focusing on a year long kindness challenge based on feedback from our staff 
and students. We have thanked our local responders ( military, police, fire) during the month of September in remembrance of 9-11, 
collected candy for troops for Halloween in October, and collected hundreds of cans of food for November. Toys for Tots/ making 
holiday cards for our military will be in December, the week long Great Kindness Challenge is in January and Blankets for the Homeless 
in February. Each month, the students/staff determine where our kindness focus will be. We will be collecting pennies for patients 
as well in February. 
 
Our staff and principal promote high academic standards, respect, cooperation, and personal responsibility within a caring and safe 
learning community so that all students will effectively learn. The Bonsall West staff is committed to working together with parents 
and members of the community to continue offering the highest academic challenges to our students. This is evident as our students 
in grades 3-6 outperformed their county/state peers on the CAASPP . 
 
Bonsall West Elementary School opened its doors to students in 2005. We have a short history that is packed with great 
accomplishments. Each classroom has chrome book carts, one:one devices and i pad carts for use in the classroom for class projects. 
The entire school is on a wireless network and all classrooms are equipped with LCD projectors, flex-cams and Smartboards; all of 
these improvements help enhance the teaching and learning environment. Our students consistently outperform the district, county, 
and state on statewide assessments. Most recently, our students outperformed the state and county averages in both ELA and math 
on the CAASPP. On Great Schools.com, we scored a 9/10 to similar schools. We are located near Camp Pendleton Marine Base. We 
are proud to work closely with our school base liaison to offer resources/support to our military families. This year, we began offering 
Operation Hero sessions for our students after school to develop leadership/study skills. We also began the FOCUS program with the 
support of Camp Pendleton to enable students to learn more about military life and its' impact on military connected students. For 
the past four years, our school has hosted a special night to honor our military: Tell me a Story night and brought the Single Marine 
Fitness challenge to our students. We honor our military weekly with a patriotic assembly that includes the pledge and patriotic song. 
Last year, we were granted a $250,000 DoDea grant for military dependents. Project AIM( Academic Achievement, Innovation, Mental 
Health) High helps us address students' needs in literacy, STEM, and social emotional health. Our school boasts a diverse 
socioeconomic student body and an intensely dedicated group of educators and parents that will do whatever is necessary to help all 
students master the skills they need. 
 
** Note** All the data reflects the school data pertaining to the prior CDS code but does reflect the school data for 2013-14 school 
year for this school. 
 

2017-18 School Accountability Report Card for Bonsall West Elementary School 

Page 2 of 10