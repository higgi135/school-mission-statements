Mission Statement: 
Magnolia High School’s mission, in partnership with the home and its richly diverse community, is to educate students and to assist 
them in realizing their full potential as responsible, productive, contributing members of society, by providing an educational 
environment in which students are challenged, excellence is expected, and differences are valued. 
 
Highlights: 
Magnolia offers Advanced Placement courses in English Language and Composition, English Literature, European History, U.S. History, 
U.S. Government, Psychology, Calculus, Biology, Chemistry, Environmental Science, Spanish Language, Spanish Literature, French, and 
Studio Art. 
 
Magnolia High School features Career and Technical Education courses in child development and early childhood education, building 
industry technology, law and legal occupations, health care occupations, and digital photography. The school also features a strong 
Junior ROTC program. 
 
Magnolia High has a strong PUENTE program. PUENTE is a partnership between our school and the University of California that is 
designed to improve the college-going success of under-served Latino youth. Our school also operates an Advancement Via Individual 
Determination (AVID) program. 
 
General Information: 
Magnolia High School offers courses that meet University of California A-G requirements, as well as honors courses and Advanced 
Placement (AP) courses in English, Mathematics, Science, Social Studies, Spanish, and Studio Art. Fifty-five percent of Magnolia High 
School students are enrolled in Visual and Performing Arts classes and/or interscholastic sports. Magnolia High School has the 
following Career Technical Education (CTE) pathways: building industry technology, photo design, medical careers, protective services, 
teaching careers, and information services. Magnolia High School has a very active Junior Reserve Officers' Training Corps (JROTC) 
program, which has been recognized every year for its excellence. Magnolia High School partners with the local elementary school to 
allow students in the child development field a chance to work with elementary age students as teacher aides. Magnolia High School 
offers the following support programs to help close the achievement gap: English language arts (ELA) support classes, monthly Parent 
Education events, and after-school tutoring services with peer tutors through Anaheim Achieves community partners. Beginning in 
the fall of 2017, Magnolia High School will become the home of Southern California’s first Cyber-Security Career Pathway in partnership 
with University of California, Irvine, Cypress College, Tesla Foundation and North Orange County Regional Occupational Program. 
Magnolia High School is the first public high school in the nation to partner with the Tesla Foundation to provide a Drone Pilot Training 
course for our Saturday Academy program. This course features Federal Aviation Administration approved curriculum, equipment, 
and Drone Flight Simulation Kits. The Tesla Foundation’s goal is to identify and develop a “farm system” of young talent that can be 
future innovators and entrepreneurs in the unmanned systems industry. 
 
 
 

2017-18 School Accountability Report Card for Magnolia High School 

Page 2 of 12