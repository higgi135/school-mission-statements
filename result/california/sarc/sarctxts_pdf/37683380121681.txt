San Diego Global Vision Academy (SDGVA) is a TK-8th grade writing-based charter school in the Normal Heights neighborhood of San 
Diego. SDGVA is known for academic excellence, writing expertise, service-learning and social responsibility. The school began 
operations in the 2010–11 school year. 
 
The primary objective of San Diego Global Vision Academy is to create an exemplary learning environment that advances the 
development, growth, and success of each student. Toward that end, the school’s Executive Director and Director of Academic 
Achievement play an active role in re-enforcing school-wide expected learning results and academic standards. It is common for the 
Executive Director to meet with parents regarding such issues as frequent tardies, absences, and the implementation of extra 
academic or behavior support. 
 
SDGVA derives its purpose from the following vision: our students will possess academic knowledge to develop leadership skills to 
participate and make a positive contribution in the global community. San Diego Global Vision Academy develops civic-minded leaders, 
accomplished writers, and resilient lifelong learners. 
 
We teach our students to follow the SDGVA Way: I will be a student of integrity. I will think and act rationally, not based on my 
emotions. I will play safely and have high regard for the safety of others. I will be resilient and prepared to overcome any obstacles in 
my way. I will accompany all my decisions with action and finish what I start. I will honor my community through service learning. I 
will build a legacy based on knowledge, responsibility, and respect. I will write the future!