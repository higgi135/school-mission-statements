COMMUNITY: Located in the west portion of Santa Clara County, the Campbell Union School 
District serves more than 7,600 students in Transitional Kindergarten through eighth grade from 
the communities of Campbell, San Jose, Saratoga, and Los Gatos. 
 
SCHOOL: Marshall Lane Elementary School, located in Saratoga, educates more than 540 students 
from Saratoga, Los Gatos, Campbell, and San Jose in preschool through fifth grade. We emphasize 
academic achievement within a nurturing environment. Our success is attributed to a dedicated 
and hard working staff, a well-rounded challenging curriculum, active parental involvement, and a 
community devoted to supporting education. 
 
Our school has earned several awards over the past few years. Both the National Blue Ribbon 
Award and the California Distinguished School Award have been earned. In addition, Marshall Lane 
was awarded the prestigious Caring School Climate Award for the 2014-2015 school year from 
Project Cornerstone. 
 
Marshall Lane’s Vision Statement 
 
Marshall Lane will be a school where educators and parents collaborate to provide a rigorous and 
meaningful educational experience for all students in a supportive school environment. 
 
Marshall Lane’s Mission Statement 
 
Marshall Lane is a community that is committed to inspiring and empowering life-long learners who 
possess the mindset and skills necessary to thrive in a changing world. 
 
Marshall Lane’s Core Values 

Effective Communication 
Safe Learning Environment 

• 
• 
• Collaboration, Collegiality and Relationships 
• 

Educating the Whole Child 

2017-18 School Accountability Report Card for Marshall Lane Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Marshall Lane Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

23 

23 

22 

0 

0 

0 

0 

0 

0 

Campbell Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

305.60 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.