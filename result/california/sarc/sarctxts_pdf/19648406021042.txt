Los Alisos Middle School serves as a learning community where students are challenged and are 
engaged in rigorous curriculum and relevant instruction that is standards-based. Our agreement 
with Project Lead The Way (PLTW) and our partnership with USC have been key components in the 
development of our Math, Science, and Technology Magnet program that offers robotics and 
engineering courses and infuses STEM (Science, Technology, Engineering, Math) in the core 
curriculum. Los Alisos also offers an AVID (Advancement via Individual Determination) program. 
AVID classes include an exploratory wheel in 6th grade and elective classes in 7th and 8th grades. 
Our goal is to expand AVID strategies schoolwide. The AVID elective class and the magnet program 
both require an application and interview process to participate in this great yearlong course. 
 
We have a strong team of dedicated individuals who provide a welcoming environment where the 
top priorities are academic achievement and a safe and nurturing school. Our school program 
includes WEB (Where Everyone Belongs) a transition program designed to assist adolescents in 
making successful transitions from elementary to middle school and one that prepares them for 
the challenges of high school and beyond. 
 
Los Alisos has created a positive learning environment that expects and promotes respect and 
safety for all. Project Wisdom is the character education program we use to promote positive 
choices. We believe that “Character Education” is an intentional effort to help students 
understand, care about, and act upon core ethical values. Discipline at Los Alisos is based on 
developing positive relationships among students, teachers, parents, and administrators. We have 
developed the Six Ps (Polite, Proud, Prompt, Positive, Prepared, Productive) to guide our students 
as they learn to be more self-directed and self-disciplined. We hope to help students achieve these 
goals by encouraging them to become capable, connected, contributing members of our school 
community. Students with good character are caring, just, and responsible. Good character 
education improves social and emotional competencies as well as academic performance. This 
positive school environment along with our uniform dress code has significantly impacted our 
community’s perception and school pride. 
 
At Los Alisos everyone is held accountable for student learning. We expect students to come 
prepared and actively participate in class. To ensure that learning occurs, our school environment 
promotes academic rigor in each class through technology and project-based learning. We take 
pride in the cleanliness of our campus, the organizational structures in place, and the quality of 
instruction. Our goal is to develop productive citizens who will follow this code of conduct in school 
and their community. 
 

2017-18 School Accountability Report Card for Los Alisos Middle School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Los Alisos Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

65 

49 

51 

0 

0 

0 

0 

0 

0 

Norwalk-La Mirada Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

898 

2 

4 

Teacher Misassignments and Vacant Teacher Positions at this School 

Los Alisos Middle School 

16-17 

17-18 

18-19