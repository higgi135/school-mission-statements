Tustin High School is a comprehensive high school serving approximately 2,270 students from central and southern regions of the City 
of Tustin. The student population demographics are 78% Hispanic, 11% White, 3% African American, 3% Other Asian, and 5% Asian 
which closely reflects the City’s demographics. Tustin High School's academic programs are highlighted by the T-Tech engineering 
pathway, a Model United Nations program, and a robust AVID program for which the school serves as a national demonstration site. 
A wide variety of extracurricular activities are offered as evidenced by 44% of the student population reporting they participate in 
interscholastic athletic programs, and an additional 55% who report they participate in one of the many clubs and other extra-
curricular sports available at Tustin High School. 
 
Our Mission 
The mission of Tustin High school is to serve our students with the understanding that diversity of gender, physical and mental ability, 
culture, and background is a strength to be respected. By providing a diversified and rigorous curriculum, students will achieve 21st 
century core competencies and develop skills necessary to ensure college and career success. 
 
Our Vision 
Tustin High School will prepare confident graduates for the demands and opportunities of the future by providing differentiated, 
relevant, and rigorous curricula utilizing twenty-first century strategies and technology. The Tiller Team will continue to foster a school 
community that embraces diversity. 
 
Our simplified version of both our mission and vision is the following: 
The Tiller Family . . . 
Values Diversity and Community 
Engages in Rigorous Learning 
Develops 21st Century Competencies 
Prepares for Future and Current Success