Cypress Elementary School is located twenty-five miles east of Los Angeles in the East San Gabriel Valley and has an enrollment of 700 
students in grades Transitional Kindergarten through fifth. The school is part of the Covina-Valley Unified School District which is 
composed of nine elementary schools serving K-5, three middle schools serving grades 6-8, three comprehensive high schools, and 
one alternative high school. The campus hosts a Kids Korner Daycare. 
 
On the 2017-2018 CAASPP, 56% of students at Cypress met or exceeded the standard in English Language Arts. In Mathematics, 39% 
of students met or exceeded the standard. We continue to focus on our ELL population and our SED populations along with significant 
targeted interventions for all of our Kindergarten through fifth grade students who do not meet the standard. 
 
Cypress School is one of nine computer science magnet schools in the Covina-Valley Unified School District. Cypress is a one-to-one 
school where all students have their own Chromebook or Lenovo and are learning CODE as their second language. This year Cypress 
is again partnering with Code to the Future. Students grades TK-5th use their 21st Century skills of collaboration, critical thinking, 
creativity, and communication in an environment where computer science is taught as a normal discipline in the classroom. 
 
As part of our ongoing plan to sustain growth, our leadership team conducts a yearly needs assessment and evaluates our systematic 
intervention program to ensure that it continues to meet our students' needs. We continue to offer an intervention/extension rotation 
designed to provide dedicated leveled instruction for 45 minutes daily. Students are tested to determine which intervention they will 
receive and are retested at each trimester using standards-based STAR and i-Ready reading and math assessments to determine 
response to intervention. These findings are discussed within the grade level PLC meetings to make adjustments to the students' 
schedules and are documented in the yearly planning meetings with the principal. 
 
Daily leveled ELD is integrated into daily intervention for Kindergarten through 5th grade students. Cypress teachers are trained in 
GLAD strategies to help students develop rich vocabulary and concept knowledge. 
 
Currently, 15% of the students are categorized as English Language Learners (ELL) with the majority speaking Spanish. Cypress is a 
Schoolwide Title I school with 79% of the students on Free or Reduced lunch. Other demographics include: 3% African-American, 3% 
Asian, 3% Filipino, 83% Hispanic/Latino, 7% White, 3% African American, and 1% two or more races. 
 
All of the 34 teachers are fully credentialed, meet the ESSA State Standards for highly qualified, and are CLAD certified. Our staff 
includes four Special Education teachers in our Specialized Academic Instruction Program. In addition, we have 2 Speech and Language 
Pathologists and a part time Adaptive PE teacher. Cypress also has a Title 1 Intervention Specialist who works with our 
socioeconomically disadvantaged population, coordinates, and teaches our intervention program. 
 
Cypress Elementary School was constructed in 1956 and had some upgrades in 1994 through a modernization process. The school’s 
playground has been completely renovated and the 4,000 square foot Library Media Center was completed in the 2003-2004 school 
year. 
 
Cypress is proud to be a Gold Ribbon School and a Title I High Achieving School. The cultures of Cypress students are celebrated and 
respected through a variety of events throughout the school year including our Read on the Green celebration in March. Cypress has 
a very active PTA that works in conjunction with the staff to enhance learning opportunities for students through field trips and 
assemblies. Our Parent University has an active population that helps educate and link parents to school activities and volunteer 
opportunities. Through PTA, Cypress has developed many community partners that offer services to the Cypress School family. 
 
CYPRESS VISION AND MISSION STATEMENT 

2017-18 School Accountability Report Card for Cypress Elementary School 

Page 2 of 11 

 

At Cypress, we believe that our students will one day change the world. Cypress Elementary School is a caring and collaborative 
community where students are empowered to own their educational experience. We are committed to providing a safe and nurturing 
environment and developing self-motivated thinkers who are able to succeed in an ever-changing world. We aim to see learning 
through the eyes of our students and want students to actively be their own teachers. We want students to be visible learners. To do 
so, they must continually ask themselves: "Where am I going?", "How am I doing?", and "Where do I go next?". We are committed to 
a growth mindset and the belief that everyone can get a little bit better every day.