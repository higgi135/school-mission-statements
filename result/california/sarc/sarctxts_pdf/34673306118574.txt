Empire Oaks Elementary School, has achieved California Distinguished School status and has received the Blue Ribbon Award. Nestled 
against the foothills of El Dorado, the homes of the Empire Ranch community surrounds the campus. Hazel McFarland Park is adjacent 
to the beautiful campus and serves as a wonderful area for families to meet and play. Our student enrollment, reported on the 
California Basic Educational Data System (CBEDS) in October 2017 was 516 students in grades Kindergarten through five. The 
surrounding neighborhood is populated with families who are eager to participate in the education of their children. The school reflects 
a culture of caring and a strong home-school connection. Empire Oaks operates on a traditional calendar that begins in August and 
provides three trimesters of instruction for our students. The staff is made up of educators who hold California Teaching Credentials 
and paraprofessionals both who are committed to educational excellence for all students. 
 
The campus is made up of several well designed buildings which are situated on immaculately groomed grounds. The students are 
happy and engaged as you enter each classroom. Everyone on campus has a sense of pride in our learning community. Parents are an 
important part of the daily classroom routine. Whether it is working with a small group, one-on-one, or assisting with procedures, 
volunteers are visible on a daily basis. Parent volunteers are invaluable docents for our Fine Arts program: Meet the Masters. Parents 
volunteer over 400 hours a month on our campus. They are an integral part of the successful academic program at Empire Oaks. Many 
students attend the before and after school Student Care Center located on campus. 
 
Our PTA and School Site Council are actively involved in our school program. Our energetic PTA Board plans numerous activities and 
events which enhance the positive bond between parents, teachers, and students. The PTA provides significant financial support 
through fundraising that supports school wide curriculum. PTA funds the Meet The Masters art integration program in which trained 
parent docents integrate fine arts and music into lessons taught throughout the school year. Teachers are continually working with 
the students to ensure the development of our technology program. Grade level chrome books are available for classroom projects, 
research, and training. Each classroom is equipped with an Elmo and LCD projector to enhance learning. PTA funding has developed a 
commitment to our technology and STEM programs that are implemented at all levels K-5. 
 
Empire Oaks Elementary School students have opportunities to be involved in Student Council, Physical Education, GATE, Music and 
Special Education. Students can participate in after school activities such as, Chorus, Drama, Young Rembrandt's, Early Engineers and 
Acorn Adventure After School Clubs. 
 
Our school is structured around high academic and behavioral expectations, which help maintain a positive school-wide atmosphere. 
The implementation of Positive Behavioral Interventions and Support (PBIS) across campus has supported our community in striving 
for student support and empathy development. Students are praised through verbal and written feedback for appropriate behavior. 
The entire staff believes that "all students are our students" and share in the responsibility of providing an atmosphere where every 
student can meet their educational goals. Our staff believes that all students have the ability to learn, and are committed to providing 
conditions that promote student success. This success is accomplished through the use of clear expectations that are directly taught 
throughout the school year. Our staff works diligently to address and support the academic and social emotional needs of our students 
on a regular basis. 
 
At Empire Oaks we believe that all students can learn and that we, as a team with their families, can facilitate learning to ensure that 
our students will achieve to their greatest potential. This will be accomplished by teaching a rigorous curriculum and setting high 
standards. Our faculty of talented, highly trained teachers and staff work each day with all students to meet this goal. 
 
 
 

2017-18 School Accountability Report Card for Empire Oaks Elementary 

Page 2 of 11