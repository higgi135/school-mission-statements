Tehama eLearning Academy offers a blended model including a full range of online academic 
courses as well as on-site core and elective courses for grades seven through twelve. Our mission 
is to provide an individualized standards-based education for students in Tehama and adjacent 
counties through innovative electronic methods, state of the art mastery-based curriculum, 
traditional instructional models and parental involvement resulting in skill mastery. We target any 
student who can benefit most from a self-paced, individualized instruction that is delivered on site 
or in the home via technology. We believe that given a comprehensive and mastery-based 
curriculum, high expectations, access to technology (computer and internet), strong instructional 
support, guidance from experienced teachers, a strong commitment from parents (or other caring 
adults), and a well-conceived virtual education program can help boost student achievement, serve 
the unique needs of students and families, and offer a new model for effective public education in 
the 21st century. 
 
Mission Statement 
To provide the highest quality education through the use of innovative curriculum, quality 
academic service, and reliable technical support. 
 
School Profile 
Tehama eLearning Academy is a charter school that originally operated under the Mineral School 
District. Since 2013-14 it has operated as a charter school under Tehama County Department of 
Education. 
 
Our school has about one hundred students. We provide students with an engaging and secure 
online neighborhood to complete coursework, improve academic skills, and master the Common 
Core State Standards. Support and tutoring are available electronically and onsite every school day. 
The courses are taught under the guidance of California certificated teachers who are experts in 
their academic fields. All of the courses are “open entry” / “open exit” and students may access 
them twenty-four hours a day seven days a week. 
 
 

2017-18 School Accountability Report Card for Tehama eLearning Academy 

Page 1 of 9