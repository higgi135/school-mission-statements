School Description and Mission Statement 
 
The Shackelford Mission Statement is “ Shackelford scholars and staff are committed to being lifelong and self-driven learners. 
Learning is required!” 
 
Shackelford Elementary is located at 100 School Avenue in Modesto, California. The school was constructed in 1945 with additions 
made in 1984 through 1985. Portables were added to Shackelford from 1984 through 2001 adding 9 additional classrooms. Shackelford 
has a Head Start, Preschool and Transitional Kindergarten through Sixth Grade. Shackelford’s population in Head Start and Preschool 
is 86 students total. Transitional kindergarten through Sixth grade has a population of 537 with 91% Hispanic, 6% White and 3% other. 
Currently 71% of our students are English Learners. There are 3 sixth grade classrooms, 2 fifth grade classrooms, 2 fourth grade 
classrooms, 3 third grade classrooms, 3 second grade classrooms, 3 first grade classrooms, 3 kindergarten classrooms, and 1 
transitional kindergarten class. There are 3 Special Education classrooms, a second/third combination, a fourth/fifth combination, and 
a fifth/sixth combination with a population of 33 students. There is also a computer teacher and a computer lab with 34 computers. 
The teacher helps students become proficient in computer skills and teaching them how to use important programs. Additional 
support staff includes 4 office staff, 3 full-time custodians, and 4 mental health clinicians and a nurse that helps students with health 
issues two days a week. 
 
Shackelford has many programs helping students become successful. This school year Shackelford Scholars will have the opportunity 
to participate in the following extracurricular activities and programs: 10K with a COP and Mighty Miler running Program, Yearbook, 
Traffic Patrol, Associated Student Body, Chorus and Recycling Club. Shackelford has an after school program with approximately 120 
students being served. Students are helped with homework, grade level standards and character education. There is also a mentor 
program for the fourth through sixth grade students helping with reading instruction. Shackelford participates in the Stanislaus county 
Mentorship Program offered to fourth through sixth grade students. Also, beginning this school year, Gallo employees will be 
partnering with Shackelford to provide mentorships for students. This program provides students with enrichment opportunities 45 
minutes each week. 
 
Last updated on 11/17/2015