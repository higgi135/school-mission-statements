Tenaya Middle School was named in honor of Chief Tenaya who was the last of the Yosemite Indian Chiefs. Tenaya Middle School is a 
traditional calendar school serving grades seven through eight and lies in the southern part of the cit y Merced, west of the county 
fairgrounds. As part of the districts voluntary integration plan, Tenaya Middle School caters to students living in the southwest section 
of Merced. As it s top priority, the entire staff of Tenaya Middle School strives to meet the social, emotional, and physical needs of 
students. Tenaya Middle School is dedicated to ensuring that it s students come away with a well rounded educational experience 
that will serve them well in the future. Tenaya’s staff, in collaboration with all segments of the school community, is committed to 
providing an effective instructional program based on current research and practice. Our vision is to provide opportunities and 
experiences that will enhance all students’ academic, social, physical, and emotional development.