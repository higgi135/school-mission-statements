Pinnacle Academy Charter School is dependent charter in the South Monterey County Joint Union High School District. Pinnacle 
Academy is an independent Study school where the students have the flexibility when to attend their required 1-hour of instruction. 
 
Vision: 
Pinnacle Academy Charter School will provide a positive learning environment in order to meet student needs with high expectations 
for high school graduation. 
 
Mission: 
The mission of Pinnacle Academy Charter School is to provide a safe, challenging environment where students become a community 
of learners who develop knowledge, confidence, and independence to realize their academic and creative potential, develop respect 
and tolerance for others, and become involved and responsible citizens.