It is Arroyo Vista K8’s mission to develop and inspire ALL STUDENTS to become lifelong learners who will make a positive impact in the 
world. #We Care.We Share.We Prepare. 
 
Arroyo Vista K8’s Values: 
 
We Care - 
Systems for Support - Arroyo Vista is committed to systems and structures that are proactive and support student learning 
 
Relational Trust - Arroyo Vista is committed to a transparent relational trust that fosters safety in communication, risk-taking in action, 
confidence in sharing student results, and continuous improvement 
 
We Share - 
Common Goals - Arroyo Vista is committed to creating common SMART goals 
 
Responsibility - Arroyo Vista is committed to collectively working toward student centered goals to improve learning for all students 
 
We Prepare - 
Focus on Learning - Arroyo Vista is committed to the fundamental purpose of supporting all students 
 
Data Driven - Arroyo Vista is committed to the collection and examination of data to evaluate student learning 
 
Child and family focused, Arroyo Vista K-8 School is serenely nestled in the premier master planned community of Rancho Santa 
Margarita with its historic influence of local indigenous Spanish and Indian influences. Arroyo Vista is honored to be a California 
Distinguished School! Parents, students and teachers work together to ensure student learning. The school’s consistently high API is 
earned through this successful collaboration. 
 
Arroyo Vista’s partnership with the community stands as a key foundation for our success. The PTA partners with the school to provide 
additional programs such as the Meet the Masters Art program, additional music instruction for primary grades, field trips and 
educational assemblies for all grade levels. A highlight over the years has been the funding of our state of the art fitness center in 
order to enhance our Physical Education Program. Meanwhile, a volunteer parent group, the Arroyo Vista Children’s Theater, casts 
and produces an amazing play each year, featuring approximately 100 elementary students. 
 
Character education is also an important aspect of education and children are recognized for their good character and academic 
achievements. We do things the “Wildcat Way” here which is a series of expectations agreed upon by staff, students, and parents. 
 
Students are encouraged to help others through such programs as the PTA Care/Share program, a monthly food donation for a local 
food bank, the middle school ASB-sponsored Angel Tree, and “Caring Boxes”, toy-filled boxes donated by families for global 
distribution. Meanwhile, we offer peer tutoring between middle and elementary students as well as peer tutoring between middle 
school students. All emphasizing the “T” in Wildcat Way, we "Take Care of Each Other”. Students who participate above and beyond 
in their community service are awarded the Voluntary Service Award in their 7th and 8th grade years. 
 
As a Professional Learning Community, Arroyo Vista recognizes that quality instruction, parent support, and community collaboration 
are key to student progress. Our vision is clear, our care is sincere, and our standards are high. We are proud of our students at Arroyo 
Vista and continue to promote the Wildcat Way. 
 
 

2017-18 School Accountability Report Card for Arroyo Vista Elementary School 

Page 2 of 12