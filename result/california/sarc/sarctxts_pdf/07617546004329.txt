Sun Terrace is a school of about 480 students. W e are a very diverse community. We have both general education classes and special 
education classes. 
 
Sun Terrace Mission Statement is 
 
Sun Terrace Community will: 
 
STRIVE to be our best in all we do 
 
RESPECT self, others and the environment 
 
CONNECT with our class, our school and our community. 
 
Sun Terrace is also a STEM (Science Technology Engineering Math) magnet school in Mt Diablo Unified School District as of August 
2017.