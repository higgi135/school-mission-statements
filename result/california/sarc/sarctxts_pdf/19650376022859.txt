Our goal is to provide students with a supportive environment, which includes parents and the 
community, as they make the transition from elementary school to high school. We want to 
prepare students academically and socially for success in high school and to prepare students for 
independent and lifelong learning. We encourage our kids to set individual goals and make positive 
choices. 
 
We also have a drama club, a STEM (Science, Technology, Engineering, and Math) class, AVID and 
AVID Excel classes, a Student Government class, band class, art, Study Skills, Mentors, Culinary Arts 
class, Coding class, a painting class and a concurrent enrollment STEM class with East Los Angeles 
College so our students have the opportunity to enjoy electives and receive the full middle school 
experience. 
 
Richard L. Graves Middle School (GMS) is a two-year middle school serving grades seven and eight. 
GMS is the only middle school in the South Whittier Elementary School District serving this 
unincorporated area of Los Angeles County. The school is located on one of the community’s main 
thoroughfares, near two commercial shopping centers in the city of Santa Fe Springs. The 
neighborhood’s main housing consists of single-family homes and some apartment complexes. The 
school operates on a traditional year calendar. 
 
Dr. Matthew C. Fraijo, Principal 
 

----
---- 
South Whittier School District 

11200 Telechron Ave 
Whittier, CA 90605 

(562) 944-6231 

www.swhittier.k12.ca.us 

 

District Governing Board 

Elias Alvarado, President 

Sylvia Macias, Vice President 

Deborah Pacheco, Clerk 

Jan Baird, Member 

Natalia Barajas, Member 

 

District Administration 

Dr. Gary Gonzales 
Superintendent 

Martha Mestanza-Rojas 

Associate Superintendent, 

Educational Services 

Mark Keriakous 

Associate Superintendent, Business 

Services 

Dr. Marti Ayala 

Associate Superintendent, Human 

Resources 

Sandra Jashinsky 

Director, Special Education & 

Student Services 

Stacy Ayers-Escarcega 
Director, Assessment, 
Accountability & Parent 

Engagement 

 

2017-18 School Accountability Report Card for Richard L. Graves Middle School 

Page 1 of 8