As principal of Walnut Grove School, it gives me great pleasure to extend a warm welcome to our entire school community. Walnut 
Grove School is Patterson's newest school located at 775 North Hartley Street. Walnut Grove School is a K-8 school with less than 100 
students per grade-level, giving the school a small community atmosphere where students feel very connected. In addition, Walnut 
Grove has a 50/50 dual immersion model in Spanish and English in all grade levels. The Walnut Grove School Dual Language Academy 
is providing an enrichment program dedicated to building a student body that is bilingual, biliterate and multicultural. This is being 
achieved by providing a comprehensive core curriculum program paralleled with instruction in Spanish. Walnut Grove School is a 
“high-tech” school and serves as a model for what technology-rich schools in Stanislaus County will look like in the future. Each student 
in grades 6th-8th grade was issued a district chromebook to use for educational purposes through our 1 to Web program; in addition, 
every elementary classroom has a chromebook cart for student's use. Walnut Grove is a No Excuses School and we are preparing our 
students to be college and career ready. Staff is working diligently, continually monitoring student learning and refining their 
instruction in order to give Walnut Grove School students the support they need each year to grow academically. We are college 
bound! 
 
Vision Statement 
 
Walnut Grove will earn the reputation for academic excellence and for attaining significant and measurable academic growth each 
and every year. Walnut Grove will be a place where the entire school community strives to learn and grow in order to achieve 
extraordinary teaching and learning across all grade levels. The academic success students experience at Walnut Grove School will 
become a legacy, positively impacting their entire life. 
 
Mission Statement 
 
The purpose of Walnut Grove School is to address the needs of individual students and prescribe the enrichment and/or intervention 
they need. The entire Walnut Grove School community will strive to improve and grow, learning from the success and skills of others, 
in order to achieve academic excellence across all grade-levels. 
 
Board of Education Commitments: 

life-long learning 
a safe learning environment 

• 
• 
• developing responsible and accountable students 
• 
• healthy behaviors 
• motivating students to maximize their potential 

communication 

 

2017-18 School Accountability Report Card for Walnut Grove K-8 School 

Page 2 of 11