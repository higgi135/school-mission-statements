The Vision of Pathway Elementary Community Day School is to provide all students with a positive, 
structured environment focused on behavioral and academic improvement, where students can 
learn and practice skills to promote their success in secondary school, college, career and the 
community at large. The Mission of the school is to serve the At-Risk Youth of Central Unified School 
District. 
 
Pathway Elementary Community Day School is located in Southwest Fresno. Students’ grade levels 
range from K-6th grade. A Pathway student arrives through suspended expulsion, full expulsion, 
SARB placement, a district level referral and/or court/probation placement. Pathway's goal is to 
have 100% of students be able to transition back to their school of origination or a mainstream 
school to continue their educational and behavioral goals. Calls are made home daily for absent 
students and parents stay informed on academic work completed via the online grade system 
Parent Portal and regular contact from school staff. Students and/or parents may contact teachers 
or staff at any time to check on their child’s development. Pathway elementary has an RSP teacher 
that is shared with the secondary campus. Central Unified provides a school psychologist and an 
intervention counselor 5 days per week as well. Pathway Elementary has one full time teacher and 
2 full time aides. 
 

 

 

----

---- 

----
---- 
Central Unified School District 

4605 North Polk Ave. 

Fresno, CA 93722 
(559) 274-4700 

www.centralunified.org 

 

District Governing Board 

Mr.Jason R. Paul, Area 1 

Ms. Yesenia Z. Carrillo, Area 2 

Mr. Phillip Cervantes, Area 3 

Mr. Richard Atkins, Area 4 

Mr. Richard A. Solis, Area 5 

Mrs. Terry Cox, Area 6 

Mr. Naindeep Singh Chann, Area 7 

 

District Administration 

Andrew G. Alvarado 

Superintendent 

Mr. Kelly Porterfield 

Assistant Superintendent, Chief 

Business Officer 

Mrs. Ketti Davis 

Assistant Superintendent, 

Educational Services 

Mr. Jack Kelejian 

Assistant Superintendent, Human 

Resources 

Mrs. Andrea Valadez 

Administrator, Special Education & 

Support Service 

Mr. Paul Birrell 

Director, 7-12 & Adult Education 

Dr. Tami Boatright 

Director, K-8 Education 

 

2017-18 School Accountability Report Card for Pathway Elementary Community Day School 

Page 1 of 8