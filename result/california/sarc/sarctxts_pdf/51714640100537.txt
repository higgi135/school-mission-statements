Principal's Statement: 
The 2018-2019 school year is going to be a great year at Andros Karperos School (AKS). First and foremost, we want to "know our 
students." It is important that the staff believes EVERY student is "their" student. We have included a drama class and a multi-support 
class into our middle school schedule. Positive Behavior Intervention and Supports (PBIS) is in it's second year of implementation and 
we are off to a great start recognizing our students efforts for being scholars at AKS. 
AKS implements Advancement Via Individual Determination(AVID) concepts and strategies in grades 3-8 in order to promote a college 
and career focus. We continue to integrate technology into the classrooms with the use of Chromebooks. In grades 6-8, we have 
Chromebooks in all but a few classrooms. The staff is constantly learning and involved in research-based best practices with the end 
goal being student success. We will continue to look closely at our programs and look for ways to increase our students' experiences 
at AKS. 
 
Our plan is to focus on the following areas: 
 
The study and implementation of research-based instructional strategies 
Using data from curricular-embedded common assessments to guide instruction 
Increasing levels of proficiency in Math and English Language Arts 
Increasing levels of proficiency in both Special Education and English Learners subgroups 
 
We ensure student learning by: 
Knowing our students 
Choosing to be positive 
Finding solutions 
Committing to quality service 
 
Benchmarks along the way indicate that we continue to make progress in these areas and students are benefiting from our efforts. 
The 2018-2019 school year is an exciting time to be on our campus. Maintaining a positive school culture while pursuing excellence in 
the classroom will be a great experience for both our student body and staff.