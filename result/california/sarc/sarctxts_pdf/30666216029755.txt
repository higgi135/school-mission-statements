The purpose of the School Accountability Report Card is to provide parents with information about the school’s instructional programs, 
academic achievement, materials and facilities and the staff. Information about Orange Unified School District is also provided. 
 
It is my pleasure to share the School Accountability Report Card for Cambridge Elementary School. Cambridge Elementary School is 
one of 27 elementary schools in the Orange Unified School District. Approximately 500 students are enrolled in grades (transitional) 
kindergarten through six during the 2018/19 school year. 
 
The school doors were opened in 1951, located in the residential area of Old Town Orange. Our school is proud of its history of 
educating our neighborhood students and playing an important part in developing the future citizens of Orange. 
 
Cambridge Elementary School provides a strong academic focus for our students along with an environment that supports the building 
of positive behavior and social relationships and individual responsibility. Our focus is to enable our students to be prepared for 
twenty-first century learning and demonstrate college and career readiness. Students’ individual needs are a focus at Cambridge 
Elementary School. To support our Multi-tiered System of Supports, teachers in grades TK - 6th co-teach the PATHs Social Emotional 
Learning Curriculum with the Mental Health Counselor. Students in grades K-6 use the Wonders reading program for Language Arts 
instruction and English Language Development and the Envision Program for Math, 
 
Cambridge is also home to a large technological base including a STEAM lab, an Innovation Lab that houses all of our robots and coding 
devices, a computer lab, over 250 iPads, and one to one HP Stream notebook computers and Chromebooks in grades first through 
sixth. 
 
Parents are always a welcome part of our instructional program and parent volunteers, at school and at home, help to support our 
program. Our overarching focus in mathematics is on math facts and their application to problem solving. 
 
Mission: 
The mission of Cambridge Elementary School is to provide all students with an education that promotes high levels of academic 
achievement, self-esteem, mutual respect, and individual responsibility. In partnership with our students, families, staff, and 
community, we create a learning environment that emphasizes communication, collaboration, critical thinking, and creativity, and one 
that empowers our students to become positive contributors to the 21st century global society.” In order to support this mission, we 
are committed to maintaining a safe, structured, and inclusive environment that encourages students to develop a growth mindset, 
gives students the confidence needed to become creative problem-solvers, and promotes positive relationships and responsible 
behaviors. 
 
Vision: 
The vision of Cambridge Elementary School is to develop our students’ skills, knowledge, abilities and character necessary to reach 
their personal potential. By providing opportunities for communication, collaboration, critical thinking, creativity, and character 
building experiences, we will enable our students to function effectively as productive and contributing members of society within the 
context of a rapidly changing world and be college and career ready for the 21st century. 
 

2017-18 School Accountability Report Card for Cambridge Elementary School 

Page 2 of 11