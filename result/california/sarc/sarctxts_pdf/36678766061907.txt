Arrowview Middle School has 66 classrooms, a library, an auditorium, and an administration office. The campus was built in 1937, was 
modernized in 1990, and was modernized again between 2007 and 2009. One portable classroom was added in the 2004-05 school 
year, and five special education classrooms were built in 2007. The facility strongly supports teaching and learning through its ample 
classroom space and a staff resource room. 
 
The mission of Arrowview Middle School is to ensure that all students achieve proficiency in all subject areas and that all students will 
be equipped at the next level of education.