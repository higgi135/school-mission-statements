Mildred B. Janson School was founded in 1945. It opened as Marshall School and later named 
Mildred B. Janson in 1968. Mildred B. Janson was recognized by the California Department of 
Education as a Title 1 Achieving School in 2003, 2004, 2005 and 2006. In 2013 our Academic 
Performance Index (API) score was 872. The entire student body, staff, and community are very 
proud of our school's academic accomplishments and achievements. Due to the new Common Core 
Standards, there was no new API for this past year. Janson will remain frozen at 872. In May of 2017 
Janson School participated in the CAASPP and the results of the test are indicated in this report. 
 
The school consists of students and staff with rich multicultural and ethnically diverse backgrounds. 
Everyone works together to ensure quality education and success for all students. Janson school is 
an official "The Leader In Me, Lighthouse" school, first to be named a Lighthouse school in the state 
of California. Each student and staff member have an understanding of the 7 Habits and is the 
common language here on our campus. We all believe that every student is a "Leader" and our job 
as educators is to find the leader in them and to celebrate it. The hard-working staff is well trained 
in the Leadership model along with the 7 Habits and is dedicated to making a positive difference to 
all of our students. 
 
Each student has access to a rigorous Common Core Standards-based core curriculum in language 
arts, math, science, and social science. Students also have access to our many resources in our 
computer lab. The Janson computer lab is fully equipped with 34 iMac Book Desktops, 4 laptop 
carts of 30, 2 laptop carts of 20, 2 iPad carts of 30, 2 iPod carts of 20 and ten carts of Google Chome 
Books with 30-36 each. The Janson staff has and is utilizing technology in the classrooms daily and 
are preparing our students with the 21st-century skills they need. We as a Janson staff are very 
proud of our rich traditions and accomplishments. We always strive to provide our students with 
the best educational experience possible. 

• Gabriel Cardenas, Principal 

 
Mission Statement 
The mission of Janson Elementary School Community is simply "Learning, Growing, Leading." 
 
Community & School Profile 
Located in Southern California’s San Gabriel Valley, ten miles east of downtown Los Angeles, the 
Rosemead School District educates over 2,668 pre-kindergarten through eighth-grade students in 
the diverse community of Rosemead. Founded in 1859, the district is proud of its long tradition of 
academic excellence. There are currently four elementary schools (pre-K-6) and one middle school 
(7-8) in the district; students from Rosemead School District attend Rosemead High School which 
is part of the El Monte Union High School District. 
 
Rosemead School District believes in providing a challenging academic environment with high 
expectations and placing student needs as its number one priority. 
 
Due to the outstanding efforts of students and staff in a focus on raising student achievement, 
Janson School was recognized by the California Department of Education as a Title I Achieving 
School in 2003, 2004, 2005, and 2006. Congratulations to our school community for this 
outstanding achievement. 
 

2017-18 School Accountability Report Card for Mildred B. Janson Elementary School 

Page 1 of 11 

 

Through the hard work and dedication by the Janson community, in 2011 Janson school became the first Franklin/Covey Lighthouse school 
in California. Janson continues to implement the Leader in Me and provides Janson students leadership opportunities throughout the 
year. 
 
Janson School, which operates on a traditional school calendar, served over 650 students in Pre-Kindergarten through sixth grades in 
2017--18 school year. Student demographics are shown below. 
 
Asian=66% 
 
Black=.3% 
 
Hispanic=31.3% 
 
White=2.4% 
 
Homelessness=6.9% 
 
Free/Reduce lunch=75.89% 
 
Socio-economic=77% 
 
A Message from the Superintendent 
 
The purpose of the School Accountability Report Card is to provide parents with information about our schools and their instructional 
programs, academic achievements, materials, facilities, and staff. Information about the district is also provided. For more information 
about our school district, please visit our website at http://www.rosemead.k12.ca.us 
 
Parents and the community play a very important role in our schools. Understanding our schools’ educational programs, student 
achievement, and curriculum development can assist both the schools and community in ongoing program improvement. 
 
We have made a commitment to provide the best educational program possible for our students. The excellent quality of our program is 
a reflection of our highly committed staff. We are dedicated to ensuring that the Rosemead Schools offer a stimulating environment 
where students are actively involved in learning academics as well as positive values. Through our hard work together, our students will 
be challenged to reach their maximum potential. 

• 

-Mr. Alejandro Ruvalcaba, Superintendent