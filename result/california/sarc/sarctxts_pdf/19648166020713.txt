Cogswell Elementary School is located in the City of El Monte, in the western San Gabriel Valley. 
The community is highly supportive of the educational climate and is growing at a rapid rate. El 
Monte has much more to offer than just ‘the mount,’ as its name suggests. Local community 
morale is soaring due to the rapid spread of industry and businesses. Cogswell Elementary School 
serves grades kindergarten through six, as one of the ten elementary schools in the Mountain View 
Elementary School District. Cogswell Elementary School maintains a commitment to providing a 
strong instructional program. Teachers, staff, and administrators adhere to the principle of putting 
students first and tailor the educational programs and climate to meet the needs of an ever-
changing school population. 
 
Cogswell's commitment to academic excellence is in progress. With input from staff, parents, 
school community, School Site Council Cogswell tailors its' academic program to meet the needs of 
all students including our English Language Learner and Special Education populations. The parent 
community maintains a close relationship with the school through volunteerism and the active 
parent participation, which provides support for our teachers in class and activities that take place 
at Cogswell throughout the school year. Parents are regulars and welcome visitors to our 
classrooms and campus throughout the year. 
 
Further: 
~ 100% of Cogswell teachers and instructional assistants are considered "highly qualified" under 
NCLB. 
~ Cogswell is considered a Title I school-wide project and coordinates all State and federal programs 
under the school plan. 
~ Cogswell Elementary is committed to providing educational equity for all and eliminates 
discrimination, isolation and segregation on the basis of sex, sexual orientation, gender, ethnic 
group identification, race, ancestry, national origin, religion, color, and mental or physical disability. 
~ Through State ASES funding, Cogswell students have access to "Think Together." "Think 
Together" is an after school program that allows students access to homework assistance, healthy 
lifestyle education, physical activity, citizenship building, technology, and enrichment. Currently 
110 students in grades K-6 participate in this program. The Site Coordinator has daily direct contact 
with the school to ensure collaboration and communication resulting in success for our students. 
 
Mission Statement: 
At Cogswell Elementary School, we believe that all children can and will learn. To ensure that all 
children will reach their potential.... 
We will maintain high expectations and promote academic excellence for all students. 
We will create and support a school environment in which all children and adults feel welcomed, 
respected, trusted, and an important part of the school. 
We will create an environment where we can learn together and support each other. 
We will foster a positive school climate of a caring community which respects and values diversity 
and nurtures everyone's self-esteem. 
 

2017-18 School Accountability Report Card for Cogswell Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cogswell Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

22 

22 

21 

0 

0 

0 

0 

0 

0 

Mountain View School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

383.8 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Cogswell Elementary School 

16-17 

17-18 

18-19