Redondo Union High School has been successfully educating students for over a century. Founded in 1905, RUHS is the only 
comprehensive high school in the Redondo Beach Unified School District. Located in South Redondo Beach, the scenic 56-acre campus 
is the "Home of Scholars and Athletes". The entire school community is proud of the Sea Hawk traditions and is dedicated to achieving 
academic excellence. 
 
Our Mission Statement: The students, families, staff and community of Redondo Union High School provide a safe educational 
environment that supports the needs and co-curricular interests of a diverse population of students in meeting high academic 
expectations and post-secondary goals. 
 
The enrollment of 2,979 includes students in grades 9-12. The school operates on a three-period block schedule of 110 minutes four 
days per week. The Monday schedule follows a traditional six-period day, each period meeting for 55 minutes. The entire school is 
committed to providing excellent programs to meet each student's individual needs. The staff at RUHS vigorously challenges students 
to pursue the highest academic, extra-curricular and athletic standards. All students are encouraged to complete in academically 
challenging courses of study.