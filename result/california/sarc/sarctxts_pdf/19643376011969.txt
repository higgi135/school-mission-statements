The Joaquin Miller Elementary School staff, in partnership with parents, families, students and community, is committed to building a 
safe and caring learning environment where students' gifts are celebrated and extended, where each student recognizes they are a 
valuable, responsible citizen in a diverse global society. 
 
Each and every day we are living our school motto - STRIVING FOR EXCELLENCE!