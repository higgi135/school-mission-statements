It is our pleasure to introduce this annual School Accountability Report Card. The data and 
information contained within these pages will prove useful in informing you about our school, 
including, but not limited to academic standings, curriculum and instruction, school facilities and 
safety, budget, and facility enhancement. Our goal in presenting you with this information is to 
keep our community well-informed. If you have any questions or are interested in making an 
appointment to discuss this report, please call our school. 
 
Brent Shubin, PRINCIPAL 
 
 

 

 

----

--

-- 

Downey Unified School District 

11627 Brookshire Ave. 
Downey, CA 90241-7017 

(562) 469-6500 
www.dusd.net 

 

District Governing Board 

Tod M. Corrin 

Donald E. LaPlante 

D. Mark Morris 

Giovanna Perez-Saab 

Barbara R. Samperi 

Martha E. Sodetani 

Nancy A. Swenson 

 

District Administration 

John A. Garcia, Jr., Ph.D. 

Superintendent 

Christina Aragon 

Associate Superintendent, Business 

Services 

Roger Brossmer 

Assistant Superintendent, 

Educational Services - Secondary 

Wayne Shannon 

Assistant Superintendent, 

Educational Services - Elementary 

Rena Thompson, Ed.D. 

Assistant Superintendent, 

Certificated Human Resources 

John Harris 

Director, College and Career Ready 

Veronica Lizardi 

Director, Instructional Support 

Programs 

Alyda Mir 

Director, Secondary Education 

Marian Reynolds 

Administrator, Student Services 

Patricia Sandoval, Ed.D. 

Director, Special Education 

 

2017-18 School Accountability Report Card for Doty Middle School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Doty Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

50 

52 

55 

5 

1 

1 

0 

1 

0 

Downey Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

861 

13 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Doty Middle School 

16-17 

17-18 

18-19