School Vision and Mission 
John C. Martínez Elementary School seeks to develop the whole child academically, socially, and emotionally, helping prepare him/her 
to succeed in the world. We recognize that students bring a rich diversity of cultures, experiences, languages and learning styles that 
can be developed and shared in our school setting. Students should feel connected to the school and its objectives, value what they 
learn, and trust those who are teaching. 
 
Teachers/Students will model valuable life skills such as cooperation, conflict resolution, critical thinking, effective communication, 
problem solving, responsibility and accountability. Students will develop self-esteem and feel free to question and explore a larger 
world. They will discover and strengthen their talents, learn and practice social and leadership skills, and successfully meet both high 
academic and behavior expectations. 
 
School Profile 
Martínez Elementary School is a TK-6 school serving the community of Parlier, California, nestled in the heart of the great San Joaquin 
Valley in Fresno County, the largest agricultural county in the United States. 
 
The city of Parlier is a small, rural community of approximately 12,500 residents and was incorporated in 1921. It is a place rich with 
cultural heritage and ablaze with lively memories of Latino-rights icons like César E. Chávez and Dolores Huerta. The per capita income 
in Parlier is the third lowest in California, and the town ranks number one in the state in the number of families living below the poverty 
level. Unemployment figures for Parlier are continually among the highest in the state. The city, however, is beginning to grow and 
there are new homes under construction in the Martinez area. The City of Parlier has a Latino Chief of Police and Latino Mayor and 
City Council. The town feels a special respect for its roots and celebrates its primary culture and language as it embraces its new 
cultures and languages as well. 
 
The Parlier Migrant Housing Project houses migrant farm worker families who traditionally arrive in April and remain until October, 
harvesting the fruit and vegetables of Fresno County. Most of these students are from Mexico and the southern tip of Texas.