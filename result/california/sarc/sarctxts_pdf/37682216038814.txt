Welcome to another fantastic school year! We have so much to celebrate at Palmer Way School, 
and it is because of the support from our families, our highly qualified teachers, and the committed 
and engaged students that we are so successful. We are all partners in the educational journey of 
children. As a team, we strive for excellence in academics and in the social – emotional 
development of children. We have all known what a great school we have in our community and 
it is even better to know that the Educational Results Partnership (ERP) and the Campaign for 
Business and Education Excellence (CBEE), has recognized Palmer Way School as an Honor Roll 
school for the 2017 - 2018 California Honor Roll. 
 
As principal, I’m honored to serve as the instructional leader and I am committed to ensuring our 
school maintains high academic standards by providing a rigorous academic program that 
addresses the individual needs of each student. Our school’s dedicated staff is committed to the 
success of our students and works collaboratively to provide the most effective instructional 
learning experiences. Palmer Way is a great place to learn and grow, offering a relevant and 
rigorous curriculum based on the Common Core Standards and project-based learning. Technology 
is also used extensively by staff and students. The skills of creativity, communication, collaboration, 
and critical thinking are a focus in each classroom preparing the students for the challenges of the 
21st century. 
 
I firmly believe that parent and community involvement, as well as communication and 
collaboration, are essential to the overall success of our school. Palmer Way has a new and growing 
Parent Teacher Association (PTA). The PTA is the heart of the school and a vital link between 
parents, teachers and community members. Through our programs and activities, the Palmer Way 
Elementary PTA helps to meet the needs of children in our school. Volunteers are greatly 
appreciated and we would love for you to join our efforts. Please visit the school and inquire how 
you can GET INVOLVED. 
 
Palmer Way is in our third year of Positive Behavior Interventions and Supports (PBIS) 
implementation. PBIS is a framework or approach for assisting school personnel in adopting and 
organizing evidence-based behavioral interventions into an integrated continuum that enhances 
academic and social behavior outcomes for all students. Our PBIS team meets monthly to identify 
challenge areas and come up with strategies to improve student behavior. We are very excited to 
continue enhancing this approach at Palmer Way School. In addition Palmer Way has been 
investing many hours of professional development to Project Based Learning (PBL). At Palmer Way 
we believe that with PBL approach, students are encouraged to become independent workers, 
critical thinkers, and lifelong learners. Teachers are encouraged to exchange ideas with other 
teachers while also communicating with parents, in order to bring real-life context and technology 
to the curriculum. PBL is not just a way of learning; it's a way of working together. If students learn 
to take responsibility for their own learning, they will form the basis for the way they will work with 
others in their adult lives. 
 
I feel so fortunate to be a part of the Palmer Way Family! Working together as a team, Palmer Way 
teachers, staff, and parents do whatever it takes to ensure the academic and personal success of 
all our students. High student achievement, outstanding character, along with school 
connectedness and safety continue to be our top priorities. 
 
 

2017-18 School Accountability Report Card for Palmer Way Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Palmer Way Elementary School 

16-17 17-18 18-19 

Teacher Credentials 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

National School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

27 

23 

23 

0 

0 

0 

0 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

224 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Palmer Way Elementary School 

16-17 

17-18 

18-19