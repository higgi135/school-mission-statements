Principal's Message 
It is my privilege to introduce you to Quail Valley Middle School's annual School Accountability Report Card (SARC). I am tremendously 
blessed to be the principal of this outstanding Snowline school. Whether you are a student, parent, staff person or community 
member, the data contained within these pages will provide useful information for you about our school including our demographics, 
achievements, progress evaluation, ongoing goal realization, discipline, budget, and facility information, but like anything else, it only 
touches on the wonderful things our school and staff provide. Although our focus areas are a little more specific in the areas of 
Attendance and School Climate, we continue to find best practices in all our Academics in “ensuring endless possibilities for all students 
through high levels of learning.” We have added Cadet Corps and an increase in STEAM electives to broaden our students minds in 
finding their interests. We continue to strive to set rigorous goals in each of our academic areas of English, Math, Science and Social 
Studies, but we believe that opening the vision of our young minds through digital art, Spanish, robotic coding, Philosophy Thinking, 
Leadership and much more adds to the creativity of our options. 
 
As part of the new accountability system of LCFF, every school in California are required to issue an annual School Accountability 
Report Card. However, at Quail Valley we view this not only as a means of compliance, but as an opportunity to share in-depth 
information with you about our school. As we continue to define the Common Core Standards and become more innovative in 
analyzing the data, we will make every means possible to identify, instruct, intervene and collaborate with our school community. We 
will continue to provide great customer care to our Parents, Students and Staff. 
 
Thank you for allowing us to care about kids and make Middle School a place of positive intentions. 
 
Mission Statement 
Quail Valley Middle School is a community of academic achievers that promote a safe and healthy place to learn. We will continue to 
focus our attention on Instruction, Collaboration and Assessment with effective feedback to evaluate what students are learning 
academically and socially. 
 
CORE VALUES: 
The Cougar path to success: 
Q = Quality work 
V = Value learning 
M = Make responsible choices 
S = Show respect 
 
School Profile 
Quail Valley Middle School is located Phelan and serves students in grades 6-8 following a traditional calendar. Quail Valley Middle 
School has an incredible culture that allows kids to be themselves within the guidelines necessary to keep kids accountable. Quail 
Valley teachers have been working hard to create and implement SMART goals to keep them on track with what they want to students 
to know. Quail Valley Middle School is focused on providing a rigorous, CCSS standards-based program specially designed for the 
transitional period between elementary and high school programs. Instructional periods are structured in 80 to 85-minute time blocks 
supplemented with a daily 20-minute Cougar Connections during 1st period, a way to connect with our world around us. Sixth grade 
students are grouped into "teams"; each team of students shares the same teachers for math, social science, science, language arts, 
and elective. 7th and 8th grades has a mixed selection of teachers with some having multiple subjects and others in a specific subject 
matter, but all provide collaborative programs within each grade. This structure allows teachers to collaborate on student progress 
across all subject areas. Staff and students are challenged to meet high expectations and exceed state standards. We believe that all 
students can learn and we work collaboratively as a professional learning community to get every student to learn and be prepared 
for success in high school and beyond. As a member of the Snowline Joint Unified School District, we work to ensure endless 
possibilities for ALL students through high levels of learning. 
 

2017-18 School Accountability Report Card for Quail Valley Middle School 

Page 2 of 12