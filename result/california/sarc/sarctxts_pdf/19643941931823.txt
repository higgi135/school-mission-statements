Principal's Message 
I would like to welcome you to Claremont High School's Annual School Accountability Report Card. 
In accordance with Proposition 98, every school in California is required to issue an annual School 
Accountability Report Card that fulfills state and federal disclosure requirements. Parents will find 
valuable information about our academic achievement, professional staff, curricular programs, 
instructional materials, safety procedures, classroom environment, and condition of facilities. 
 
Claremont High School has a long history of excellence as an institution of teaching and learning, in 
an environment where all students have the opportunity to develop and excel academically, 
athletically, emotionally and socially. There are many wonderful programs at CHS. We will continue 
to offer our Honors classes for 9th and 10th graders. We continue to offer AVID, a college readiness 
program. For our 11th and 12th graders we continue to offer 18 Advanced Placement courses, and 
this is our ninth year as an International Baccalaureate World (IB) school. Our IB program offers 
rigorous courses with an 
inquiring, 
knowledgeable students who will help to create a better and more peaceful world through 
intercultural understanding and respect. In the area of Career Technical education some of the 
classes we offer on site are Game Design, Virtual Enterprise (in-school entrepreneurship program 
and global business simulation that empowers students to discover their potential through creating 
and managing business ventures and trading with other virtual firms nationally), Stage Design and 
Technology, and an after school Marketing Class. 
 
Mission Statement 
Claremont High School commits itself to nurturing the academic, social, physical, and emotional 
development of all students to prepare them to take their place as productive and invested 
members of the larger world community. 
 
School Vision 
Build a community of empowered and creative thinkers. 
 
School Profile 
Claremont High School is located in the central region of Claremont and serves students in grades 
nine through twelve following a traditional calendar. At the beginning of the 2017-18 school year, 
2414 students were enrolled, including 9.3% in special education, 2.2% qualifying for English 
Language Learner support, and 33.7% qualifying for free or reduced price lunch. 
 

Claremont Unified School District 

 

170 West San Jose Avenue 
Claremont, CA 91711-5285 

(909) 398-0609 

www.cusd.claremont.edu 

 

District Governing Board 

Hilary LaConte, President 

Beth Bingham, D.Min., Vice 

President 

Nancy Treser Osgood, Clerk 

Steven Llanusa, Member 

Dave Nemer, Member 

 

District Administration 

James Elsasser, Ed.D. 

Superintendent 

Lisa Shoemaker 

Assistant Superintendent, Business 

Services 

Julie Olesniewicz, Ed.D. 

Assistant Superintendent, 

Educational Services 

Kevin Ward 

Assistant Superintendent, Human 

Resources 

Brad Cuff 

Assistant Superintendent, Student 

Services 

 

2017-18 School Accountability Report Card for Claremont High School 

Page 1 of 13 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Claremont High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

93 

89 

91 

0 

0 

0 

0 

0 

0 

Claremont Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

325 

5 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Claremont High School 

16-17 

17-18 

18-19