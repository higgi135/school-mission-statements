The School for Integrated Academics and Technologies (SIATech) Academy South is a network of 
charter high school campuses in California focused on helping students get back in school and back 
on-track to graduation. The mission of SIATech Academy South is to provide a premier high school 
dropout recovery program engaging students through relationship-focused, high-tech, and 
rigorous learning experiences resulting in “Real Learning for Real Life.” The school was founded in 
2004 and operates in partnership with the Workforce Innovation and Opportunity Act (WIOA) 
locations. SIATech Academy South school sites are located in the Boyle Heights, and Pico-Union 
neighborhoods of central Los Angeles, as well as a recently opened site in Culver City. 
 
Students at SIATech Academy South are offered “Real Learning for Real Life,” with an opportunity 
to complete their high school education in a motivational, academically challenging environment. 
Standards-based academic learning is integrated in real-world, high-tech applications as the 
program focuses on literacy, numeracy, technology, and workplace readiness skills. Academic 
knowledge integrated into state-of-the-art technologies and real-world software applications are 
used within a professional, work-like setting to prepare students for success in the workforce 
and/or further academic training. The goal is to improve learning opportunities for the severely at-
risk students enabling them to both earn a high school diploma and succeed in entering vocational 
careers, military service, and postsecondary education. 
 
SIATech Academy South recently received a California Career Pathways Trust grant. The schools’ 
career pathways enable students to focus on coursework that will prepare them for jobs after 
graduation. SIATech Academy South schools offer a curriculum that includes UC approved a-g 
courses available to all students. UC utilizes “a-g” subject requirements to ensure that students 
have attained a body of general knowledge that will provide breadth and perspective to new, more 
advanced study. SIATech Academy South students graduate ready for college and career. 
 
 
 

2017-18 School Accountability Report Card for SIATech Academy South 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

SIATech Academy South 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

4 

0 

0 

6 

0 

0 

5 

0 

0 

Acton-Agua Dulce Unified 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

SIATech Academy South 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.