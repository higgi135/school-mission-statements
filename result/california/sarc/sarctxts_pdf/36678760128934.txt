The San Bernardino City Community Day School shares a site in conjunction with STAR at Anderson Community Day School (for grades 
K-6). The CDS program strongly supports behavioral change through a highly individualized and intensive behavioral and academic 
program. 
 
Program Overview: San Bernardino City Community Day School is designed to assist secondary students with behavior skill 
deficits. Students learn new social skills through a system of acknowledgement and correction with the goal of being successful in the 
regular classroom setting. Individualized Instruction: A certified teacher and instructional assistant work together to individualize 
academic and social skill instruction for this smaller group of students. In addition, the school counselor works with the Opportunity 
classroom on a regular basis. The goal of SBCCDS is to assist students in developing their skills as self-managers and improving 
academic performance to be successful in school and community settings.