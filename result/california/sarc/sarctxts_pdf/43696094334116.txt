Los Altos High School is a comprehensive public school situated in the heart of Silicon Valley and currently serves approximately 2200 
students. Our attendance boundaries include residential, light industry, high technology and retail districts of the cities of Mountain 
View, Los Altos, Los Altos Hills and a small section of Palo Alto. We are fortunate to be a Basic Aid District, and our local property tax 
base still exceeds the State revenue limit. 
 
Our students are socioeconomically, ethnically, linguistically and culturally diverse. Los Altos High School provides rigorous academics 
through an excellent college preparatory program for the large majority of our students, a wide range of honors and AP courses taken 
by over a third of our students, and a strong set of support classes and programs for students who are not meeting proficiency 
standards. Students take a broad range of courses in the visual and performing arts and participate in award-winning programs 
throughout the arts curriculum. We offer CTE courses in Engineering (partnering with PLTW), Computer Science (partnering with 
TEALS), Culinary Arts, New Media Literacy and our STEAM Academy. Our athletics program serves more than half of the student body 
and has been both highly successful in League and Section Competition. Numerous student clubs, an active Associated Student Body 
(ASB) and School Community Leaders class (SCL) along with a strong yearbook team and an award-winning student newspaper provide 
students many pathways to personal growth and enjoyment outside the classroom. 
 
We at Los Altos High School value a learning environment in which students and staff support one another in a spirit of unity and 
mutual respect. We are committed to continuous learning and the application of knowledge in the classroom and beyond. We value 
our diverse pathways and empower students with the skills they need to achieve their goals after graduation.