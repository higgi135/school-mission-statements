SCHOOL MISSION 
Summit Intermediate School is dedicated to creating, practicing, and sustaining a positive school culture where students are supported 
to develop a personal responsibility for developing excellence. Students are empowered to achieve their academic, social, and 
emotional potential by all members of our school community. 
 
DISTRICT & SCHOOL PROFILE 
Etiwanda School District serves over 14,000 TK-8 students residing in the cities of Rancho Cucamonga, Fontana, Alta Loma, and 
Etiwanda. The district currently operates thirteen TK-5 elementary schools, four intermediate schools (grades 6-8), and a Community 
Day School. Etiwanda’s graduating eighth-grade students are served by Chaffey Joint Union High School District for grades 9-12. 
Homeschooling program, preschool program, and childcare are provided at some schools within the district. More information is 
available on the district website or by contacting the district office at (909) 899-2451. 
 
The district’s commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high-quality 
educational program. Etiwanda School District appreciates the outstanding reputation it has achieved in local and neighboring 
communities. Consistent success in meeting student performance goals is directly attributed to the district’s energetic teaching staff 
and strong parent and community support. 
 
Summit Intermediate is a small neighborhood school in a planned community located in the central region of the district boundaries 
next to the district office. During the 2018-2019 school year, 1,111 students in sixth, seventh, and eighth grade were enrolled.