Alamos Elementary School, Home of the Owls, is an academic institution that prides itself on being academically rigorous, while 
developing in every student, a lifelong love of learning. The staff, students, and parents of this community are committed to ensuring 
educational excellence for all. AES is proud to offer a variety of different learning experiences for all children including a physical 
education program, visual and performing arts classes, visits to our technology learning lounge, and our school library. Parent 
involvement is just another ingredient to our recipe for success. The Alamos PTO, is over 600 members strong. This wonderful group 
helps to ensure fun school activities, family events and educational assemblies. 
Alamos is one of eighteen elementary schools in the Temecula Valley Unified School District. Our campus consists of our main building, 
a multipurpose room, and 4 instructional buildings. We have a preschool program with 128 students from the northern end of 
Temecula and our TK-5 program with 887 students. We serve a total of 1015 students. With the exception of our preschool 
program, our attendance area includes the neighborhood homes that surround our school. Out of our 887 student population 
in TK-5, we have a special education population of 63 students. We are recognized as a California Distinguished School. 
 
 
OUR VISION: 
 
 Alamos Elementary School will ensure that all children meet rigorous academic standards through powerful learning experiences in a 
positive, caring and motivating environment. 
 
OUR SCHOOL MISSION: 
 
 Think and Wonder. Wonder and Think. Oh, the Thinks we can Think Together! As a school community, we must prepare our children 
for the changing and challenging world around them. Children grow into the intellectual life that surrounds them and if we do not 
teach our children HOW to THINK, the world will most certainly teach them WHAT to THINK. our vision of student learning is one that 
is born of curiosity and wonder and brings with it endless questions, dreams and possibilities. It is our intent to encourage children to 
be thinkers and seekers of knowledge and for their teachers to instill within them, a hunger to learn.