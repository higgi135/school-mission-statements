Warwick is a safe and supportive place where students are empowered to reach their full potential, 
build critical thinking skills, and work collaboratively and independently to solve academic and 
social problems. We have a flexible resource program with many services for our special education 
students, a K-6 special day class program, MTSS, intervention classes for those who are struggling 
academically, and the Project Heart, Head, Hands (H3) character-education program. We have 
active parent groups including PTA, Magic Program, Gifted and Talented Education (GATE) 
Committee, English Language Advisory Committee (ELAC), and parents are also involved in School 
Site Council and the School Safety Committee. In 2016, Warwick was proud to be the recognized 
as a California Gold Ribbon School and in 2017, as a California Distinguish School. 
 
Each classroom is provided with technology resources to enhance student learning, this provide 
teachers and students with more access to California State Standards and our reading intervention 
program, Lexia, as well as our math programs,TenMarks and Moby Max. Our library/media center 
is well stocked with rich literature of different genres that include non-fiction/ fiction, audio books, 
and high interest titles from popular past and present authors, thus providing students the 
opportunity for reading. 
 
Our school provides students a wide range of enrichment programs such as Chess Club, Lego 
Robotics, Stem Science, Choir, Band, Debate, Basketball, Soccer, and Math Olympiad, providing 
them many opportunity for extra curricular activities. Part of our school success is due to our 
partnerships with PTA . Parents are not only welcomed but are actively recruited to take part in 
developing their students’ educational experiences. 
 
We are proud of all our staff members who make Warwick a very special place where learning 
comes first and our exceptional office staff will make all guest feel welcomed the moment you 
come through the office. Warwick pledge is to show respect, make good decisions, and solve 
problems. 
 

2017-18 School Accountability Report Card for WARWICK ELEMENTARY SCHOOL 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

42 

42 

42 

2 

0 

0 

0 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1713 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

0 

0 

0 

0 

0 

0 

1 

1 

Vacant Teacher Positions 
Notes: 
1) “Misassignments” refers to the number of positions filled by teachers who lack 
legal authorization to teach that grade level, subject area, student group, etc. 
 
2) Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners. 
 

0