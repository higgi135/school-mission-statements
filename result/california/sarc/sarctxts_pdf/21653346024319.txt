Principal's Message 
Welcome to Anthony G. Bacich Elementary School and thank you for taking the time to explore our magnificent school and school 
district. As Bacich elementary school principal, I am proud to represent this community and tell you why it is such a special place 
through our annual school accountability report card (SARC). I want to begin with the Kentfield School District's beliefs and the very 
young children who begin their educational journey at Bacich. Each of our students bring unique personalities, varied interests, 
individual experiences and abilities with them as they share their first years of school with us. We recognize that learning opportunities 
for them happen everywhere – in the classroom, on the playground and at home. We appreciate the partnerships we have with our 
Kentfield families to educate our children. While we stress academic success, we also believe in fostering the values found in our Six 
Pillars of Character: Responsibility, Caring, Respect, Citizenship, Trustworthiness and Fairness. These core values are critical in helping 
our young children develop the well rounded skills necessary to maximize their potential as contributing members of our society. Our 
teachers are passionate about providing a learning environment in which our students are challenged and thrive. Working in 
partnership with our parents, we provide a supportive and safe environment where our young students can take risks, develop a 
strong foundation of skills and begin to experience a genuine love of learning. In addition to the core subject areas, we offer our 
students the opportunity to explore their interests and passions through service learning, outdoor experiences, art, music, and 
technology. Our website is a link to our vibrant school community. I extend a warm invitation to you to learn more about us and to 
get involved if you are currently a parent in our school. Learn first hand the spirit of Bacich School and the Kentfield School District. 
 
Mission Statement 
The Kentfield School District's mission is to inspire and challenge all students to live, learn, and lead to their fullest potential. 
 
Vision Statement 
Kentfield School District will deliver a quality education that empowers our students to reach high, work hard, and be kind. 
 
School Profile 
Anthony G. Bacich Elementary School is located in Kentfield and serves students in grades transitional kindergarten (TK) through grade 
four (4) following a traditional calendar. At the beginning of the 2018-19 school year, 640 students were enrolled, including 11.3% in 
special education, 9.4% qualifying for English Language Learner support, and 9.2% qualifying for free or reduced price lunch.