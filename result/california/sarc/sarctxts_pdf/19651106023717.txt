Established in 1950, Orange Grove Elementary School is located in the western portion of the Whittier City School District. Currently, 
the school's enrollment is approximately 415 students, with 96% of the population being Hispanic or Latino. Approximately 20% of 
our students are English Language Learners; 18% of our school population are students with disabilities. Orange Grove is a school-
wide Title 1 school with approximately 74.88% of our pupils qualifying for free and reduced- priced lunch. All teachers and support 
staff at Orange Grove are highly qualified. All students have access to standards aligned instructional materials and there is a process 
in place for teachers to order materials when needed. 
 
Orange Grove School is a learning community of highly-qualified, dedicated staff, students, and parents who work in a team effort to 
increase student achievement. Our staff is currently working on increasing reading and math achievement throughout the school day. 
Teachers offer a Balanced Literacy environment where students provided instruction in English Language Arts (Reader's and Writer's 
Workshop,Word Work, and Shared Reading). Our school recognizes the importance of literacy. This year's instructional focus is 
Reading, implementing the mini lesson and enhancing teacher time with students when they confer while students are reading 
independently. Students who require additional support in Reading are grouped according to needs in the Response to Intervention 
(RtI) time. Teachers are trained to provide support to all students in this area and meet every 6 weeks to discuss progress with the 
goal of moving students according to their reading growth. We currently participate in Data Reflection Meetings where the Principal 
and Instructional Coach meet with teachers twice a month to monitor student progress by discussing assessment results and 
identifying what are the next steps for instruction for those who are achieving and are not achieving grade level standards. Eureka 
Math is implemented for Mathematics instruction daily in grades Kindergarten through fifth grade. STEM opportunities are provided 
to students throughout the school day. Positive Behavior Intervention and Support is a focus on campus at Orange Grove. Students 
are taught behavior skills to maintain a safe and health learning environment for all. Orange Grove serves as an effective and exciting 
place where all children can learn the academic and social skills needed to be productive and successful scholars. 
 
The mission of Orange Grove Elementary School is to cultivate a positive learning environment by collaboratively teaching a rigorous, 
enriched curriculum that educates and prepares all students to become life-long learners who are critical thinkers, problem solvers 
and socially responsible citizens. 
 
Orange Grove’s vision is that every student will achieve proficient or advanced levels in Language Arts, mathematics and Science. Our 
expertly trained staff will utilize best practices in order to provide an innovative, differentiated, and enriched curriculum, which will 
incorporate state-of-the-art technology, the arts, and innovative STEM curriculum. We will continue to foster an effective partnership 
among our staff, students and parents through collaboration, timely communication, and parent workshops so that all students will 
become life-long learners, critical thinkers, and socially responsible citizens.