SCHOOL VISION 
Grapeland Elementary School will be every child's favorite place to be. 
 
SCHOOL MISSION 
Grapeland Elementary School is committed to providing a quality education with an emphasis on academics. We believe students are 
our highest priority. All students are capable of learning and are worthy of our best efforts. All students should have access to a 
balanced, rigorous, standards-based curriculum derived through a quality instructional program. All students will flourish in an 
atmosphere of love and respect while maintaining a safe and orderly environment. 
 
DISTRICT & SCHOOL PROFILE 
Etiwanda School District serves over 14,000 TK-8 students residing in the cities of Rancho Cucamonga, Fontana, Alta Loma, and 
Etiwanda. The district currently operates thirteen TK-5 elementary schools and four intermediate schools (grades 6-8) and a 
Community Day School. Etiwanda’s graduating eighth-grade students are served by Chaffey Joint Union High School District for grades 
9-12. Homeschooling program, preschool program, and childcare are provided at some schools within the district. More information 
is available on the district website or by contacting the district office at (909) 899-2451. 
 
Grapeland Elementary is a small neighborhood school located in the central region of the district boundaries. Grapeland serves 
approximately 670 preschool through grade 5 students residing in the city of Etiwanda. 
 
District-sponsored, fee-based childcare is available Monday through Friday for Grapeland Elementary school-age students. The 
childcare center is open from 6:30 a.m. to 6:00 p.m. Participating students are provided enrichment activities in art, music, literature, 
and physical education. More information about the childcare program may be obtained from the district website or school office 
staff.