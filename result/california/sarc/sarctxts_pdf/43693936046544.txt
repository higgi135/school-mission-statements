COMMUNITY: Located in the West portion of Santa Clara County, Campbell Union School District 
educates more than 7,600 students from the communities of Campbell, San Jose, Saratoga, Santa 
Clara, Monte Sereno and Los Gatos. 
 
A school of nearly 620 preschool through 5th grade students, Castlemont serves students from the 
communities of San Jose and Campbell. Our partnerships with those communities and their families 
are among our greatest strengths. We are a school community with high expectations as students, 
educators, parents, and the community work together towards a common goal to actively support 
all students in achieving his or her personal best. Parents are part of the decision-making process 
through involvement in a very active Castlemont Home and School Club, our bilingual Advisory 
Committee, the School Site Council and our program review process. 
 
Mission Statement: 
Castlemont is a diverse and caring school community that empowers all students to be 
collaborative and innovative lifelong learners. 
 
Vision Statement: 
Castlemont Elementary School will be a nurturing, safe, and professional community that supports 
the social, emotional, and physical development of all students. Curriculum will be academic, 
engaging, and standards-based, with a focus on the learner. All school staff will be highly qualified 
and caring educators who are attentive to the needs of our diverse population. Castlemont will 
encourage positive parent involvement, which will support our school and community. Students 
will be respectful, productive citizens who think critically, make informed decisions, and act 
responsibly. 
 
 

2017-18 School Accountability Report Card for Castlemont Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Castlemont Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

27 

28 

25 

1 

0 

1 

0 

0 

0 

Campbell Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

305.60 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Castlemont Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.