Columbia East Valley 3-8 Community Day School is located at 675 Shasta View Drive in Redding, CA. 
Our school is made up of 1 classroom. We share facilities with Mountain View Middle School such 
as the library, computer lab, cafeteria and playground. 
 
The mission of our school district is to ensure learning for all children – no limits, no excuses. 

---- 

Columbia Elementary School 

District 

10140 Old Oregon Trail 

Redding, CA 96003 

530-223-1915 

www.columbiasd.com 

 

District Governing Board 

Matthew Riley 

Charles Van Hoosen 

Devon Hastings 

Melissa Reyes 

James Luna 

 

District Administration 

Clay Ross 

Superintendent 

Michelle Glover 

Director of Business Services 

 

2017-18 School Accountability Report Card for Columbia - East Valley 3-8 Community Day School 

Page 1 of 7 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Columbia - East Valley 3-8 Community Day 
School 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

1 

0 

0 

1 

0 

0 

1 

0 

0 

Columbia Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

44 

2 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.