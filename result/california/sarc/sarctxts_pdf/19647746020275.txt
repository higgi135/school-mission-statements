As Principal of Mark Twain Elementary School, I would like to introduce you to the annual 
Accountability Report Card. In accordance with Proposition 98, you will find a wealth of information 
about our school, our community, and our accomplishments. We welcome the opportunity to tell 
you more about us. 
 
Mark Twain Elementary has an ongoing commitment of academic excellence. Our school 
population consists of students and staff with rich multicultural and ethnically diverse backgrounds. 
Our students, parents, and staff, and community have joined together to assure success for our 
students. 
 
All students have special talents and are given the opportunity to develop these talents at Mark 
Twain Elementary School. The staff is dedicated to providing an atmosphere in which a child’s 
social, emotional, and intellectual needs are recognized, nurtured and supported. We care about 
each and every one of our students. 
 
Since its incorporation in 1921, Lynwood continues to move forward on a path of progress. The 
city’s many accomplishments can be attributed to its aggressive redevelopment program that has 
attracted new businesses and industry alike. Today, Lynwood is a vibrant city with a population of 
over 70,000. 
 
Lynwood Unified School district serves more than 19,000 students in grades Pre-kindergarten 
through twelve. Beginning in 2005-6 the district opened additional elementary, middle, and high 
school sites to better accommodate its expanding student population. 
 
Mark Twain Elementary offers comprehensive instructional programs which underscore the value 
of living in a multicultural society. Mark Twain is special because it is dedicated to the overall 
success of the school, its culture, and meeting all barriers for the school community. Staff 
continually strives to implement recommendations for improving academic achievement. As a 
community of learners, we have embarked on a path of continuous improvement. 
 
Edward Espino, M.Ed., M.S 
 
Principal 
 
District Mission 

• 

The mission of Lynwood Unified School District, the cultivator of innovative thinkers, is 
to ensure each student fearlessly achieves his or her highest academic and personal 
aspirations while contributing to the greater society through a unique system 
distinguished by: people of great character who inspire and lead by example the 
instilling of courage to be creative the transformative uses of technology safe and clean 
environments the honoring of all voices of our community. 

2017-18 School Accountability Report Card for Mark Twain Elementary School 

Page 1 of 11 

 

School Mission: 
The mission of Mark Twain Elementary School is aligned to the mission statement of Lynwood Unified School District. In 2011, LUSD 
formed a strategic team composed of parents, teachers, alumnae, administrators and city representatives. The mission of Mark Twain 
Elementary School is that every student matriculating will have all the prerequisite/foundational skills needed to successfully meet all the 
academic expectations set for them in middle school, high school, and beyond. 
 
Supporting this mission are the following actions: 

 Provide a rigorous, standards based curriculum to every student enrolled at Mark Twain 
 Provide families with educational opportunities that ensure their ability to assist students toward advanced achievement on 

standards based testing 

 Use our resources in a fiscally and environmentally responsible manner which promotes, encourages and supports student 

achievement 

 Continue improving our teaching skills through professional development and collegial sharing and planning. 

 
Major Achievements 

 

In 2012, Mark Twain Elementary School met 16 out of 17 criteria for Adequate Yearly Progress (AYP) required by the federal 
No Child Left Behind legislation. The school gained 21 Academic Performance Index (API) points. 

 Our teachers meet weekly in professional learning communities to establish goals, develop interim assessments for our unit 
standards, and discuss ways in which to support students not meeting goals. They share research-based strategies, best 
practices, look at student work, and plan instruction for future learning. 

 During our Data Reflection meetings, teachers and administration analyze district data to meet district proficiency target goals 
in English language arts and mathematics. The data is separated into individual student performance and subgroups to identify 
struggling students and create interventions. For those students meeting the goals, teachers discuss enrichment opportunities 
and differentiation to support student learning. 
In 2013-2014, the Mark Twain PTO was established to support extracurricular and enrichment opportunities. The PTO enabled 
our students to participate in a Music and Art Enrichment program. Students learned basic keyboarding, drawing and drum 
line. We were excited to have our first Visual Performance Arts after school enrichment program. 

 

 
In 2014-2015, in collaboration with Monik's Dance Studio, we offered an after school dance class focusing on a variety of multicultural 
dances. 
 
in 2015-2016, with the support of PTO, we will be having our first guitar lessons class. 

 

 

In our continued focus on students safety, we have implemented our first Student Safety Patrol. In addition to our campus 
monitors and teachers, during recess, our students have the opportunity to help monitor the playground and areas of needed 
supervision. 
In promoting our Districts' mission of preparing our students to become college and career ready, we had various activities 
such as college t-shirt day, "Dress for Success", and "Open your door to college", door decorating contest. 

 To prepare our students for the SBAC( state test) and college-career, in addition to our existing desktop computer lab, we are 
proud to have opened a grade 2-3 and 4-6 Chromebook computer lab. Students have the opportunity to conduct research, 
type reports, practice keyboarding skills, utilize supplemental educational applications/programs and practice state test item 
questions. 

 We were proud to announce that Mark Twain Elementary has been validated and certified as a California Gold Ribbon School 
2016 & Title I Achievement School. Mark Twain Elementary School successfully completed the California Gold Ribbon Schools 
Program application process and was recognized as one of the 772 California Gold Ribbon Schools for 2016. The award reflects 
our school’s success in creating a positive learning atmosphere for our students. Our success is a direct result of our dedicated, 
creative, and talented staff, students, parents, and our supportive school community. 
In October 2017, to ensure student safety, we opened our morning drop-off zone to help alleviate the traffic congestion. Our 
school safety patrol, led by students, provides assistance in guiding traffic and welcoming students and parents. 
In December 2018, we are excited to have our new Air Conditioning/Heater system in the cafeteria so that our students are 
comfortable during hot and cold school days. 
In November 2017, to strengthen the safety of the campus, our school has installed new fencing throughout the perimeter of 
the campus. 

 

 

 

 Each school year, the school is investing in technology. In addition to the chrome book carts provided by the District, for the 
past two school years, we continue to purchase additional two chromebook carts. We are proud to share that Mark Twain 
Elementary School has the most chromebooks of all the LUSD elementary schools. 

 Each class has a chromebook cart. All students have access to a chromebook during the instructional day. 

 

2017-18 School Accountability Report Card for Mark Twain Elementary School 

Page 2 of 11 

 

Focus for Improvement 

 We will continue with school-wide focus of implementing LUSD “Platinum Ticket”, a standards aligned monitoring tool used to 

guide instruction. 

 As a vehicle to collaboration, our teachers will continue to meet during grade level planning and Guided planning so they can 

share ideas, resources, and best practices. 

 We will continue to implement English Language Development (ELD)/Academic Language Development instruction for our 
English Learners and non-English learner populations. Our goal is aligned with the District English Learner master plan to re-
designate our English Learner students by the fifth grade. 

 School-wide we will focus on writing. Students will have the opportunity to write in every subject area in preparation for the 
Common Core standards. Common Core is a set of standards developed by the federal government in the hopes of unifying 
education standards nationwide. 
In Language arts, we will focus on reading comprehension. Our goal is to have every student being able to read by 3rd grade. 
In mathematics, we will focus on problem-solving. School-wide, we will focus on basic arithmetic such as addition, subtraction, 
multiplication, and division facts. We want students to develop foundational basic mathematical skills as they progress 
through the grade levels. 

 

 To promote good character and promote positive-decision making, we will continue our focus on a character education. The 
Six Pillars of Character program helps students understand the importance of character as it relates to a successful life and 
career. Students earn “Comet Tickets” and they are rewarded during our weekly Friday morning assembly. Students have the 
opportunity to earn Comet Tickets where they can purchase prizes in the PBIS store. In alignment with the District LCAP goals 
and school character focus, we will be continuing of implementing Positive Behavior Intervention Systems (P.B.I.S.). 
In the coming year, Mark Twain is committed to increasing parental involvement in school-related activities. Every month, the 
Principal holds a parent meeting, "Coffee with the Principal" to provide parents an update of school activities and give parents 
the opportunity to ask questions and voice their concerns. 

 

 To support the home school connection, the Principal has facilitated communication via Class Dojo and Remind apps. 
 We will reach out to local businesses to create partnerships with local businesses. 
 

In January 2018, we are piloting a Response to Intervention program (RTI), "Read Naturally Encore" to strategically target those 
students in grades 3-6th who are below grade level in reading fluency and comprehension. Our RTI pilot program is being 
used as support to the core curriculum 
 

 

 

2017-18 School Accountability Report Card for Mark Twain Elementary School 

Page 3 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Mark Twain Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

25 

27 

27 

0 

0 

0 

0 

0 

0 

Lynwood Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Mark Twain Elementary School 

16-17 

17-18 

18-19