SUNKIST MISSION STATEMENT…to provide a safe and supportive learning environment that promotes student achievement grounded 
in the belief that all students can learn. We believe all students are unique, valued individuals who will problem solve, reason and 
produce quality work to achieve high standards. Sunkist Staff is committed to collaboration and communication with parents and the 
community in order to ensure all students achieve. 
 
Our Vision: Sunkist Elementary is committed to the success of all students and staff. We respect individuality and celebrate learning 
in a collaborative community focused on student achievement. 
 
In addition to a standards based instructional program, students are supported throughout the curriculum with small group instruction 
in ELA/Math, A2i (K-1), ST Math and ongoing training in research based lesson design. Grade level teams meet on a bimonthly basis to 
review student progress and adjust instruction as needed. There is an emphasis on support for English learners in all areas of the 
curriculum. Students are serviced through an intervention process that includes support during the instructional day. Students who 
qualify may participate in GATE and various levels of Special Education support. Students in grades TK-4 receive weekly instruction in 
music by a full time general music teacher. All students in grades 5th-6th grade participate in our instrumental music program. In 
collaboration with the OC YMCA, we offer an after-school program for students, which includes physical activity, enrichment, and 
homework assistance.