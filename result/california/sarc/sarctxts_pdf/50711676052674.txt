Mission (Why do we exist?) 
 
We ensure high levels of learning for ALL students. 
 
Mission Mantra 
"At Everett, I WIN (get What I Need), I SOAR (am Safe Organized, Accountable & Respectful), and I WILL Succeed." 
 
Vision (What kind of school do we want to become?) 
ALL students are important and can reach their full potential for academic success. Everett will foster a positive, nurturing, safe school 
climate for ALL. We are committed to building positive relationships with our families and community to ensure our students will 
become productive members of society. 
 
Administrative commitments in order to fulfill our vision. 
 
We support teachers with specific professional learning opportunities that support student achievement. 
We seek evidence in order to support student and teacher needs. 
We continuously improve our systems and programs to accommodate ALL students. Staff commitments in order to fulfill our vision. 
We are continuously seeking new strategies that work. 
We demonstrate courage through vulnerability in order to seek out best practices. 
We have identified the essential standards and created short term SMART goals. 
We model courtesy, respect, and compassion, while being positive/supportive to our students, parents, and families. 
We operate as a Professional Learning Community by sharing data and focusing on the four critical questions. 
Student commitments in order to fulfill our vision. 
Students will demonstrate Positive Behavior on campus and follow the school's PBIS plan. The school promotes postive behavior 
through EAGLES SOAR ourmonthly character traits that are taught by all staff. 
 
Students are recognized daily, weekly, and monthly for demonstrating positive behavior on campus through incentives and assemblies. 
 
E- Everday (August) 
 
A - Achievement (September) G - Good Manners (October) L - Leadership (November) 
 
E - Excellence (December) S- Success (January) 
 
S - Safety (February) O - Organized (March) A- Accountable (April) R - Respectful (May) 
 
 

2017-18 School Accountability Report Card for Catherine Everett Elementary School 

Page 2 of 13