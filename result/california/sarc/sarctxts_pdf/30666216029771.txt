Esplanade Elementary School is one of 27 elementary schools in the Orange Unified School District. Esplanade is a Title I school located 
in well-established neighborhood on the east side of the city of Orange. There are approximately 370 students attending kindergarten 
through sixth grade on a traditional schedule. A large number of students walk to school. Esplanade Elementary School has a strong 
sense of community that is rooted in the generations of families that have attended Esplanade since 1964. 
 
The staff at Esplanade Elementary School thrives on excellence and commits to providing every child with a quality education. 
Esplanade staff includes 14 classroom teachers (including three Special Day Class teachers) and one Resource Specialist Program 
teacher. Additionally, Esplanade has one full time resource teacher who supports English Language Development instruction and 
academic intervention for students achieving below grade level. 
 
Esplanade Elementary School is committed to providing a success-oriented and safe learning environment for all its students. The 
teaching staff works with parents to become familiar with and involved in their children's school. Parents support the school's goals 
and their children's individual academic goals. Esplanade provides home-school communication via newsletters, phone calls, weekly 
communication folders, and parent meetings to support student achievement and family support systems. 
 
MISSION STATEMENT: 
Esplanade Elementary School, in partnership with parents and community, is committed to providing a quality educational program 
that enables all students to develop to their fullest potential in a safe, educational environment.