Mission Community Day School is located on the Kings Lake Education Center campus along with Kings Lake Continuation High School, 
Corcoran Adult School and the Corcoran District’s Independent Study Program. We also offer Evening English as a Second Language 
(ESL) classes and Citizenship classes. The Assistant Principal of Alternative Education, Mr. John Arriola, supervises the schools and 
courses listed above. 
 
The Mission of Mission Community Day School is to increase student achievement, provide safe schools and promote a positive 
climate. Taking the California State Standards Test, the California Assessment of Student Performance and Progress along with our 
own district and school site assessments guides the instruction of our Mission Community Day School students. These Periodic 
assessments show the progress and challenges of each student in a timely manner, allowing the teacher the opportunity to focus on 
each student’s specific areas of strength and need. Our teachers use both formative and summative assessments to monitor and 
guide student growth and success. The Mission School uses a variety of academic resources to assist student success.