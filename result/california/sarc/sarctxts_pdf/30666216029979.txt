The purpose of the School Accountability Report Card is to provide parents with information about the school’s instructional programs, 
academic achievement, materials and facilities along with staff information. Information about the Orange Unified School District is 
also provided. Unless otherwise specified, the information provided in this report is from the 2018-2019 school year. 
 
Villa Park Elementary School, a Gold Ribbon School, is one of (27) elementary schools in the Orange Unified School District. The school, 
which was built in 1951, is located on Center Drive, south of Villa Park Road in the city of Villa Park. Approximately 620 students are 
enrolled in grades kindergarten through sixth grade. The school draws from the local community as well as attracting many families 
from the broader OUSD community who are seeking academic excellence for their students. Villa Park Elementary has a tradition of 
outstanding instruction, producing high academic achievement, with over 75% of all students meeting or exceeding state academic 
targets. Further we work in partnership with our parents and community including our Home and School League, Dad's Club, Villa Park 
Women's League, and Rotary Club to enrich our student experience with such programs as K-4 music, a dedicated PE coach, art and 
science assemblies, Camp Bobcat, hands on STEM challenges, and a Makerspace including 21st century tools such as robotics and 3D 
printing. 
 
Our staff strives to provide rigorous instruction with appropriate challenges and supports to facilitate growth for all learners each year. 
Core instructional strategies include close reading of nonfiction text, use of Thinking Maps, Write from the Beginning, mathematical 
problem solving and practices, integration of technology, and school wide STEM Challenges. Our Positive Behavioral Intervention 
System, as well a social-emotional learning curriculum at all grade levels, foster the development of the whole child and a positive 
campus climate. Our instructional program includes multi-tiered systems of support for all students including English Language 
Learners, Students with Disabilities, Hispanic and Socially Disadvantaged Students and Foster Youth. Our vision at Villa Park Elementary 
School is to provide all our students with access to 21st Century Learning in the Core curriculum and beyond and teach them the 
essential standards that are necessary for their successful next step in the educational continuum, middle school. In addition to these 
academic goals, we also envision sending our students forward with a sense of who they are and how their actions determine what 
others think of them. 
 
The mission of VPE is to: 

• Promote growth in all academic areas 
• Provide a safe and nurturing environment 
• 
• 
• Promote tolerance and acceptance 

Foster school as a place of community 
Encourage responsibility, fairness, trustworthiness, integrity and respect 

 

2017-18 School Accountability Report Card for Villa Park Elementary School 

Page 2 of 12