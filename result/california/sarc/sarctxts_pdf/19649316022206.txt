: 
 Savannah's focus 
in a safe, conducive and supportive 
environment. We commit our team to prepare our culturally diverse student population to achieve 
academically and socially in an ever-changing and demanding world. Our goal is to produce Leaders 
who will take charge of their lives and proactively seek to better themselves in all areas, at all times. 
 
Savannah's motto: "Creating Tomorrow's Leaders Today." 
Savannah Elementary School stands for excellence in all endeavors. Our mission is to ensure all 
students achieve a solid academic foundation and become self-motivated learners with good moral 
character by providing exceptional instruction, engaging activities, and meaningful opportunities in 
partnership with our families and community. Our rigorous and relevant educational program is 
designed to meet the diverse needs of all students and involves a collaborative partnership among 
the students, staff, parents, and community. Together we cultivate GREATNESS in our students. 
With teamwork and a belief that we are all responsible for the academic success of every student; 
we commit to pursue and implement highly structured, rigorous, and relevant academic 
opportunities for every Savannah student. 
 
Our Mission is: "To Learn, To Lead, To Leave a Legacy" 
 The "Leader In Me" enhances our school’s student-centered program, fostering a Leadership 
mentality, positive self-image, high scholastic standards, and school pride. 
 
Savannah Staff: 
Provide a quality education for all students within a secure and supportive environment. 
Promote in all students academic excellence, social growth, and responsible decision making. 
Prepare all students to lead productive lives in a diverse, global community. 
 
Community & School Profile 
 Located in Southern California’s San Gabriel Valley, ten miles east of downtown Los Angeles, the 
Rosemead School District educates over 2,500 pre-kindergarten through eighth grade students in 
the diverse community of Rosemead. Founded in 1859, the district is proud of its long tradition of 
academic excellence. There are currently four elementary schools (pre-K-6) and one middle school 
(7-8) in the district; students from Rosemead School District attend Rosemead High School which 
is part of the El Monte Union High School District. Rosemead School District believes in providing a 
challenging academic environment with high expectations and placing student needs as its number 
one priority. 
 
 Savannah School, which operates on a traditional school calendar, serves approximately 475 
students in pre-kindergarten through sixth grades from the communities of Rosemead and El 
Monte. 
 
 
A Message from the Superintendent 
The purpose of the School Accountability Report Card is to provide parents with information about our 
schools and their 
instructional programs, academic achievements, materials, facilities, and 
staff. Information about the district is also provided. For more information about our school district, 
please visit our website at http://www.rosemead.k12.ca.us 
 

2017-18 School Accountability Report Card for SAVANNAH SCHOOL 

Page 1 of 10 

 

Parents and the community play a very important role in our schools. Understanding our schools’ educational programs, student achievement, 
and curriculum development can assist both the schools and community in ongoing program improvement. 
 
We have made a commitment to provide the best educational program possible for our students. The excellent quality of our program is a 
reflection of our highly committed staff. We are dedicated to ensuring that the Rosemead Schools offer a stimulating environment where 
students are actively involved in learning academics as well as positive values. Through our hard work together, our students will be challenged 
to reach their maximum potential. 
 
--Mr. Alejandro Ruvalcaba, Superintendent