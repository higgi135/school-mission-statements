The mission of the Bishop Union Elementary Community Day School is to provide a safe and 
supportive atmosphere where staff, students, family and community members work together to 
create a learning environment where all participants develop a desire for personal growth and life-
long learning. 
 
Bishop is located in Inyo County along Highway 395, at the gateway to the Inyo National Forest, 
with a population of nearly 4,000. Bishop Unified School District strives to provide a quality 
education for all its students, with a wide range of programs and a talented staff dedicated to the 
needs of its population. The District serves approximately 2,000 students in grades K-12. 
 
Students served at the CDS are those who qualify and who are not experiencing success in a regular 
school setting. It is hoped that placement in a setting with a small student-teacher ratio, more 
individualized instruction and closer guidance/monitoring will help the student achieve a more 
positive outcome both in school and in their lives. Students may be probation referred, SARBed, 
oppositional/defiant, expelled, academically unsuccessful, credit deficit, pregnant or parenting. 
 

 

 

-------- 

Bishop Unified School District 

301 No. Fowler St. 
Bishop CA 93514 

760-872-3680 

www.bishopschools.org 

 

District Governing Board 

Steve Elia 

Kathy Zack 

Trina Orrill 

Taylor Ludwick 

Joshua Nicholson 

 

District Administration 

Barry Simpson 
Superintendent 

Dr. Gretchen Skrotzki 

Principal - Elm, Pine and 
Community Day School 2 

Garrett Carr 

Asst. Principal - Elm, Pine and 

Community Day School 2 

Pat Twomey 

Principal - Home Street and 

Community Day School 

Derek Moisant 

Asst. Principal - Home Street and 

Community Day School 

Randy Cook 

Principal - Bishop High and 
Community Day School 3 

Dave Kalk 

Asst. Principal - Bishop High and 

Community Day School 3 

Katie Kolker 

Principal - Palisade Glacier High 

School, Bishop Independent Study 

and Keith Bright High School 

 

2017-18 School Accountability Report Card for Community Day School 

Page 1 of 7