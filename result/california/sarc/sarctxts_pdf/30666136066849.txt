The Ocean View School District is located in western Orange County and serves over 8,000 students from pre-kindergarten through 
eighth grade. Ocean View School District is dedicated to educational excellence and the continuous academic growth of all students, 
which supports its motto: “Encouraging a Deliberate and Global Education.” 
 
Mesa View Middle School is one of four middle schools in the Ocean View School District in Huntington Beach and has had a proud 
tradition of providing outstanding learning and growth opportunities for all students. Mesa View is located in a community of single-
family homes adjacent to the city’s sprawling Central Park and near two local high schools and business centers. It has nearby access 
to beaches, museums, art centers, amusement parks and two local community colleges. 
 
Several diverse groups come together at Mesa View to form a single body of learners. The staff of 34 teachers and 22 support 
personnel work with a diverse student population. In addition to the regular program, the school supports three separate special 
education classes (5% including Special Day and Emotionally Disturbed), resource and speech services (5%), and the District Gifted And 
Talented Education (GATE) magnet program (35%). 
 
Mesa View has earned a reputation of providing learning environments that are safe, inclusive, and supportive of learning. Our 
teaming model at the sixth grade level allows us to assist and support our students during the transition to middle school. Teachers 
share two groups of students in a 108-minute block of time. Students have one teacher for English/language Arts and Social Science 
and another for math and science. Students also have the opportunity to experience a trimester exploratory wheel or participate in a 
beginning band class. In seventh and eighth grade, each class period is 54 minutes. 
 
At Mesa View, we are proud of our students’ achievements. We believe that these successes are a direct reflection and compliment 
to the talents and support of our students, parents, and community, as well as the professional competency and dedication of our 
teachers and staff members. We are committed to providing a strong, standards-based curriculum and innovative instructional 
strategies to all our students. Additionally, a continuous review of student outcomes ensures that all students receive rigorous 
instruction that actively engages them in their learning. In an effort to develop 21st century skills, instruction is supported with 
Chromebooks in many of our classrooms, and SMART Boards in every classroom. Elective classes foster a variety of skills and interests 
in our students, including instrumental music, performing arts, Spanish language, and exploratory wheels (art, computers, STEM, 
drama, gardening, and creative writing). Mesa View also added a peer mentoring program for the 2018-19 school year called WEB 
(Where Everyone Belongs). The WEB program is designed to help with the transition to middle school and promote a positive school 
climate. This year, every 6th grade student has a peer mentor to support and check in on them on a regular basis. 
 
MISSION STATEMENT: Through innovation and collaboration, Mesa View scholars will be lifelong learners who take pride in their 
relationships, community, and success at school.