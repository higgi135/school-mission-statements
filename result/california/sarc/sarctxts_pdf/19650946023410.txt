Welcome to California Elementary School - home of the Cheetahs! Our mission statement: We exist to ensure all our students achieve 
at high levels of learning. Dedicated teachers and support staff work together and are committed to developing each child, socially, 
emotionally, physically, and intellectually. In December 2018 our school was selected as a Capturing Kids' Hearts National Showcase 
School nominee and is working towards receiving this distinct honor. In addition to welcoming students to school each day, our families 
are encouraged to be involved in our school community. The academic success of every student and providing a safe and productive 
learning experience are our top priorities. 
 
It is the belief of our staff, at California Elementary School, that students can and will excel in an academic environment that is tailored 
to their unique needs. Our comprehensive educational programs focus on our mission to provide all students with meaningful and 
rigorous instruction within a nurturing, child-centered learning environment. The school year began on August 16, 2018, and continues 
through June 7, 2019. Our school day begins at 7:40 am and we dismiss at 1:48 pm Monday, Tuesday, Thursday, and Friday. 
Wednesday dismissal time is at 12:48 pm, allowing time for weekly professional development benefiting student achievement and 
educational practices. Our educational programs include a standards-aligned core curriculum, targeted reading groups for fluency and 
comprehension, iReady Reading for individualized reading instruction, Achieve3000 for targeted nonfiction comprehension, iReady 
Math which focuses on targeted math instruction, and Reflex Math for math fluency. In addition, our school has support services for 
special education, foster and homeless youth, and English Learners. We also have a program targeting health and fitness with our 100 
Mile Running Club. 
 
Our School Goals for 2018-2019: 
 
1. Maintain a supportive, student-centered learning environment with the implementation of Capturing Kids' Hearts and work 
towards achieving Capturing Kids' Hearts National Showcase School Honor 
2. Implement focused, strategic learning rotations targeted in reading fluency and reading comprehension for intervention and/or 
enrichment based on instructional data within the school day 
3. Ensure high levels of professional collaboration and communication using the principles of Professional Learning Communities 
(PLCs) with dedicated release time to align standards and curriculum, develop common assessments, analyze data results, and 
determine next steps for mastery of skills and concepts 
4. Plan and organize focused professional development to support rigorous lesson design, targeted instructional strategies, and best 
practices with school and district initiatives 
5. Provide all students with 1:1 technology and learning opportunities to support educational programs and digital literacy skills 
6. Continue to increase the number of students meeting or exceeding standards in English Language Arts and mathematics as 
measured by multiple measures including the California Assessment of Student Performance and Progress (CAASPP) 
7. Maintain high levels of parent and community involvement using a variety of activities and methods of communication in order to 
strengthen the home/school connection 
 
 
 
 

2017-18 School Accountability Report Card for California Elementary School 

Page 2 of 12