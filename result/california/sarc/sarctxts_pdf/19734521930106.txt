As the District’s continuation high school, Santana High School is dedicated to achieving excellence. The staff at Santana strives to 
provide an effective, challenging, and realistic educational environment characterized by flexibility, creativity, and concern for all 
students. We identify and create instructional strategies that respond to students’ needs and individualized styles of learning. We 
provide all students with an accessible comprehensive curriculum and quality instructional program. We recognize the cultural 
diversity of the communities we serve and as such provide a learning environment that embraces the various cultures and abilities of 
our students. The major principles of our shared vision include a commitment to the following: 

• 

• 
• 

• 

To promote resiliency and success by individualizing the rigorous standards-based curriculum of instruction for each 
student academically, vocationally, and personally 
To present and cultivate traditional values which include responsibility, trust, respect, family and caring 
To foster a safe and secure learning environment through intercultural understanding, appreciation, high expectations, 
and cooperation 
To focus on school-to-college/career preparation 

Students, teachers and staff are committed to the principle's of SOAR as means to positively reflect our common shared commitments: 

• S to show you care about yourself, others and our community 
• O openly trust others and yourself. 
• A to act responsibly and to take responsibility for our actions 
• R respect others and self