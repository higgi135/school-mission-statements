Highgrove School is located in the county of Riverside and is in the north east corner of Riverside Unified School District. We serve 
approximately 800 students, from preschool through sixth grade. Our student population is diverse and we strive to meet the needs 
of all learners. Highgrove Elementary School serves a population of about 800 students including a Head Start preschool program, 
Special Day Preschool program , Special Day Transitional Kindergarten, , grades transitional kindergarten through sixth, and two 
Mild/Moderate (grades 3/4 and 5/6) special education classrooms. Our teaching staff includes 31 certificated teachers, eleven 
Instructional Assistants, two Speech and Language Pathologists, one Resource Specialist, one School Psychologist two days a week , 
and one full-time SAP counselor. Supporting our teaching staff is a Principal, Assistant Principal, office support, custodian, library media 
assistant, and nutrition services staff. 
 
Our teachers are able to meet the needs of all students, including students whose first language is not English, GATE and advanced 
learners, as well as those students who require additional support and/or interventions. At Highgrove every student, teacher, and 
staff member is focused on and supports our core mission of delivering the highest level of instruction and ensuring that every 
Highgrove student is prepared to go to college. Our mission is to support every student's academic success with no limits.