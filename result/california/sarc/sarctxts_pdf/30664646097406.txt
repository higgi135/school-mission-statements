School Mission: Educate and inspire ALL learners to become innovative, collaborative, and independent thinkers. 
 
School Vision: Shorecliffs EDUCATES with great instruction through collaboration, meeting student needs, exploration, and 
engagement. Shorecliffs INSPIRES school culture through respect, inclusion, perseverance, and school spirit. 
 
SMS serves neighborhoods in the City of San Clemente, the Capistrano Beach area of Dana Point, and the Meredith Canyon section of 
San Juan Capistrano. Our school is located on the bluffs of northern San Clemente overlooking the Pacific Ocean. Currently, we serve 
approximately 950 students in Grades 6 through 8. Shorecliffs is located on 20 acres of land and consists of 51 classrooms, 400 seat 
multi-purpose room, athletic track and fields, recreational blacktops, and the Westview Learning Garden. 
 
Teachers promote literacy throughout all content areas and across all grade levels. The standards-based curriculum is taught in a 
meaningful context, based upon the learning objectives of each subject area. Sixth graders are placed in interdisciplinary teams of two 
or three core teachers for instruction in language arts, social science, math, and science. In seventh and eighth grade, students typically 
have four core subject teachers. All students participate in Silent Sustained Reading after lunch. Students work hard and teachers do, 
too. The outcome of their efforts has been an improving API over the last seven years and an increase in the number of students who 
are meeting their growth targets. Over the last few years, the Seahawks have also done exceptionally well outside the classroom. Each 
year our students and teachers participate in the Orange County History Day competition, essay contests, spelling contests, and other 
academic competitions. The same has been true in the area of fine arts, which include Reflections, the statewide PTA-sponsored event; 
Color it Orange, and much more. Our performing arts program has also earned a reputation for excellence among its peers. Providing 
student opportunities such as these have been a hallmark of this school. 
 
Shorecliffs Middle School is unique in several more ways. Students are accessing the outside world through the Westview Learning 
Garden, an amazing outdoor classroom, constructed by teachers, students, parents, and community members. The garden is designed 
to provide real-life biology curriculum and it has captured the heart of our school community. Teachers are currently receiving training 
in SIOP (Sheltered Instruction Observation Protocol) to improve instructional practice to improve learning for all students to address 
the growing needs of this culturally diverse community. Activate – an after school program for struggling learners, funded through 
Federal grants, is meeting the needs of at-risk students for the seventh straight year. The widely recognized “Survivor Book Club” has 
excelled for nine years as an extra-curricular program promoting reading throughout our school community. For students struggling 
in math, remediation using technology and specially designed software is utilized. Shorecliffs has pioneered the use of Internet-based 
School Loop, an online strategy for families to access assignments, tests, projects, and grades. Communication with our families is 
strongly supported through the SchoolMessenger phone messaging system, School Loop, PTA "E-News". Internet access is available 
in every classroom and office, and technology is evident in classrooms school wide. 
 
For additional information about school and district programs, please visit www.capousd.org