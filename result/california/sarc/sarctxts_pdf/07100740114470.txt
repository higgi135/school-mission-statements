Making Waves Academy (MWA) is a public charter school authorized by the Contra Costa County Office of Education (CCCOE) and is 
listed as the “District Contact”. MWA is located in Richmond, CA, an area served by the West Contra Costa County Unified School 
District (WCCUSD). Therefore, WCCUSD data is used throughout the report in charts that compare MWA to the “District” and the 
State.

In September 2007, MWA opened its middle school to its first class of 5th grade students. In September 2011, MWA opened its high 
school (Upper School). With roots firmly planted in the Richmond community since the inception of the Making Waves Education 
Program (MWEP) in 1989, MWA fulfills the promise of public schools with a rigorous, college-preparatory curriculum, small
classes, transportation, psychological services, and academic support. The vision 
five Core Values: 
Community, Resilience, Respect, Responsibility, and Scholarship. The mission 
is committed to rigorously and 
holistically preparing students to gain acceptance to and graduate from college to ultimately become valuable contributors to 
the workforce and their communities."

is: "MWA 

is grounded 

in our 

MWA is located in Richmond, California. Richmond is a diverse community with a population consisting of 25.9% African American, 
39.5% Hispanic or Latino, 17.1% White, 13.3% Asian, 0.2% Native American, 0.4% Native Hawaiian and Other Pacific Islander, and 
0.6% other (1). The demographics of Making Waves Academy are comparable to West Contra Costa Unified School District (WCCUSD) 
which are both different than the overall demographics of Richmond. At MWA 8.6% of the population is African American and 89% is 
Hispanic or Latino. 83% of students qualify for free and reduced lunch. English Language Learners make up 19.5% of the school’s 
population. 

MWA recognizes that schools with higher proportions of students from economically distressed communities require additional 
resources to compensate for the challenges they face. MWA invests in the future by providing resources to support students’ social-
emotional well-being and academic development. To support the structure of our core day and enrichment programs, the typical 
school day is from 8:05am – 6:00pm. The master schedule accommodates student participation in interscholastic athletics, clubs, field 
lessons, and academic intervention, as well as teacher planning and collaboration time. At MWA, instructional activities are aligned to 
California Common Core Standards. The MWA curriculum includes language arts, math, science, history/social science, health and 
wellness, second languages, music, fine and performing arts and social emotional education. MWA Upper School courses in grades 
nine through twelve follow the California State A-G requirements which emphasize preparing a college focused community of 
learners. The Upper School also offers Advanced Placement (AP) courses in math, history, and science. For two years in a row, the 
Upper School has been recognized by Innovate Public Schools as one of the top performing high schools in the Bay Area in the 
subjects of English and Math. 

As part of our comprehensive Response to Intervention (RTI) Program, students receive additional targeted support in core subject 
areas by attending Saturday Academy. Students are identified to participate in Saturday Academy based on their Tier status, which is 
determined by their standardized tests and benchmarks scores. Saturday Academy courses compliment and reinforce Core Day 
coursework concepts. 

MWA is a community of learners that value multiple forms of discourse and cultural knowledge and teaches students about the rich 
educational tradition of our diverse communities. MWA students are taught to think critically about culture and identity and develop 
the skills necessary to navigate various cultural settings. 

At MWA, it is critical that students learn how to access various forms of media and use tools to communicate and enhance learning. 
To prepare students for a technological world, technology is integrated into several aspects of teaching and learning, including 
research, classroom presentations, assessment and grading, and professional data analysis. 

1. "2010 Census Interactive Population Search: CA - Richmond city". U.S. Census Bureau.

2017-18 School Accountability Report Card for Making Waves Academy

Page 2 of 14