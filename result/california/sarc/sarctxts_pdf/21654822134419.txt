San Andreas Mission Statement: San Andreas High School students challenge themselves, discover their passions, apply knowledge, 
and demonstrate a commitment to personal and intellectual growth as they pursue their goals and aspirations. Students meet or 
exceed rigorous expectations. 
 
San Andreas High School is the Tamalpais Union High School District’s continuation school, and is one of a number of alternative 
programs in the district. San Andreas is an accredited high school from the Western Association of Schools and Colleges. 
 
San Andreas High School has a maximum enrollment of 110 students. Students come to San Andreas from Tamalpais, Sir Francis Drake, 
Redwood, and Tamiscal High Schools. These students are referred to San Andreas because their needs may be better addressed in an 
alternative continuation school setting. 
 
The instructional staff consists of a full time principal and 6.8 FTE credentialed teachers, one of which is a special education resource 
teacher. The resource teacher is supported by one instructional assistant. Other support staff include a one-day per week school 
psychologist, a full-time counselor, a part-time college & career specialist, a part-time health counselor, a one day a week "School to 
Career" counselor, staff assistant, attendance clerk and a full-time secretary. 
 
San Andreas is developing a rich curriculum that is aligned with the district and state graduation requirements. There are classes in 
English, science, math, social studies, fine art, and computer education. San Andreas has an excellent art studio with photo lab, 
computer lab, science lab, and a multi-purpose room that is the site for food services and our assembly program. Class sizes are 
relatively small (average 13 to 1) and instruction is differentiated to meet individual student needs. Students attending San Andreas 
are required to complete all Tam District and State outcomes for graduation. 
 
The majority of students at San Andreas have encountered some difficulties in their previous schools. Our staff is very experienced, 
and prides itself on its ability to help students re-establish themselves in a school setting. There is an emphasis amongst the staff to 
meet the social emotional needs of our students by using trauma informed practices and culturally responsive teaching practices.