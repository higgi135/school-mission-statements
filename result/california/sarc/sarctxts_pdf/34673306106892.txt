Carl H. Sundahl Elementary School has served students residing on the northwest side of the American River in Folsom since September 
1987. Sundahl is a K-6 learning environment which in 2016 began a total modernization. As of today we have 10 brand new classrooms 
with an additional 8 being built this year. We operate on a traditional year calendar. Our student enrollment, reported on the 
California Basic Educational Data System (CBEDS) in October 2017 was 383. Our campus currently houses 14 classrooms. One of these 
14 classrooms are based on Montessori education for students from preschool through kindergarten. Additionally, we have a Learning 
Center and a Student Care Center. Our multipurpose room/cafeteria complex includes a stage/music room as well as a serving kitchen 
and indoor basketball court. The classrooms and office complex are linked through a computer network system that supports the 
instructional program technically and allows access to the Internet. All classrooms are equipped with Internet access for curriculum 
and research support. Technology access for staff and students is a focus at Carl Sundahl which supports both teacher and students. 
We currently have one Chrome Book per student in every classroom. At Carl Sundahl, we are committed to preparing our children to 
achieve what we know they are capable of achieving. Our primary goal is to prepare our students to become responsible citizens and 
productive, caring members of our society. Our strong educational programs work to support each student in successfully meeting 
the challenges of learning. Opportunities for differentiated learning help to provide appropriate challenges for all students, including 
those identified for GATE. Instructional intervention programs are offered to insure all students are supported in meeting grade level 
standards in reading and math. These programs include Lexia interventions, an individualized "Read Live" program, small group pull-
out math and reading tutorial programs as well as Response to Instruction (RtI) interventions through our school wide Tier Programs 
in our Learning Center. A wide variety of curriculum and enrichment activities are offered by staff and parent volunteers, including 
PTA Movie Night, Used Book Store, Red Ribbon Week, Winter Wonderland, Primary and Intermediate Read-Ins, Starstruck Showcase 
Dance Performance, Author's Day, Accelerated Reader Celebration, District Track Meet, Glee Chorus, Jog-A-Thon, and physical fitness. 
 
Our school culture and mission is to provide an academically challenging curriculum in a caring and positive school environment. Our 
highly skilled staff is comprised of veteran teachers, a BTSA support provider, a district Common Core Instructional Coach, Math 
Coach, Technology Coach and two National Board Certified instructors. A school wide program utilizing The Dolphin 5 (Be Respectful, 
Be Responsible, Be Safe, Be Friendly, Be Productive) standards recognizes and celebrates students' progress with such areas as Dazzling 
Dolphins and weekly Dolphin 5 Achievement Awards. Our active Student Council provides awareness of the importance of community 
service. Carl Sundahl is well known for its strong, supportive parent community. In addition to classroom volunteers, parents also 
serve on our Site Council, which assists with important curriculum, budget, and facility decisions. Our PTA and Foundation parent 
groups plan numerous activities that serve as a positive bond between home and school and provide financial support for our many 
programs.