The Tulare County Office of Education continues to operate the Court School, the Juvenile Detention Facility. The Court School works 
in tandem with the Probation Department and Health and Human Services to provide incarcerated youth with an educational setting 
conducive to developing academic, social, and life skills needed to successfully transition to their district of residence to complete 
their education. 
 
The Court School has adopted the mission to facilitate high-risk students in becoming proficient in both their academic and life skills 
to rejoin their communities as responsible citizens. Ongoing development of effective communication, ethical decision-making, and 
strong social skills will enable this transition. 
 
Common Core Standards-based curriculum is provided to approximately 100 minors. The academic program is geared toward 
individual needs as addressed in the student’s Individual Learning Plan (ILP). In small classes of a maximum of 18 students, the 
certificated teaching staff presents lessons that accommodate diversity of academic and linguistic skills and learning styles while 
addressing state curricular standards. Embedded in the instruction is character education which is also reinforced by literacy studies. 
The English and Math consultants have enabled the instructors to provide all learners access to the curriculum and facilitate their 
participation. There is a strong emphasis on technological applications in the classrooms to promote improved computer literacy, 
investigate career opportunities, and enhance presentation skills.