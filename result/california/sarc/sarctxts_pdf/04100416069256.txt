The Butte County Office of Education provides a full range of services designed to meet the needs of students with moderate to severe, 
low incidence and mild disabilities. Eligible students range in age from birth to 22 years old. Currently over 700 pupils receive services. 
 
Special Education Teachers and Designated Instruction Specialists provide the following: 

FOCUS (Program for Students with Moderate to Severe Disabilities) 
STRIVE (Program for Students with Autism) 
Speech and Language Services 

• ACCESS (Program for Students with Emotional Disturbances) 
• 
• 
• 
• Deaf/Hard of Hearing (DHH) Program 
• Visually Impaired (VI) I Program 
• Orthopedical Impairment Program (OI) 
• 
• Occupational Therapy Services (OT) 
• Resource Specialist Services 
• Adult Transition Programs for Students with Moderate to Severe Disabilities 
• Adapted Physical Education Services 

Infant/Toddler Early Start Intervention Services 

Vision: Enriching lives through intentional, individualized education 
 
Mission: Continuously refining and implementing exemplary educational practices