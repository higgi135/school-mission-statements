Price Middle School embraces the whole child by celebrating diversity, instilling in them the joy of learning, and advancing critical 
thinking skills to succeed in the global community. Price Middle School promises and works towards the understanding that education 
is a shared responsibility of public schools, parents, community and the individual learners to create the educated person in the 21st 
century. Towards this goal, Price has put in extensive hours in structuring its academic program to ensure that the needs of all students 
are being met. Our collaborative efforts have resulted in the implementation of Common Core State Standards which encompasses 
ASD/RTI classes, ELD classes, tutorial classes, accelerated math classes, and extensive elective choices. Student needs are further met 
with an emphasis on writing across the content areas and extended day opportunities available after school. The implementation of 
teacher teams assists in interdisciplinary instruction and connectivity. Educational excellence is celebrated and rewarded at all levels 
throughout the year, as evidenced by solid academic scores. 
 
PBIS, the positive incentive program at Price, helps guide students to be responsible citizens who are able to meet future challenges 
and make sound choices behaviorally and academically. Teachers are continually developing as educators thought PD and learning 
opportunities resulting in increased student engagement, effective cooperative learning and the reinforcement of best practices. 
Brain-compatible research, literacy strategies, differentiated teaching practices, and 21-century skills combine to give strength and 
provide depth to our child-centered philosophy. Price Middle School offers students the benefits of these programs and sees student 
success in high school and post-secondary options of fulfilling employment or admission to institutions of higher learning. Shared 
responsibility for student success ensures the development of our students into productive members of our community.