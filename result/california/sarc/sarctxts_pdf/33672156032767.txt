Sunshine Early Childhood Center provides educational support and services to over 360 children ages 3-5 receiving special education 
services. Programs are designed to provide the specific interventions, supports and services each child needs to progress 
developmentally and educationally. We have high expectations for all our students here at Sunshine and work as a collaborative team. 
Our mission is to prepare our students to enter a kindergarten program in the least restrictive setting.We have a developmentally 
appropriate thematic curriculum and are implementing the Frog Street curriculum which is a research based program and provides 
the foundation for our students to enter the public K-6 program. All students participate in the Desired Results Developmental Profile. 
Our staff works closely with families and Parents Actively Learning (PAL) has been a wonderful opportunity for families and staff to 
collaborate and learn together. The research supports preschool education as a vital opportunity to increase student success. Sunshine 
has a very dedicated staff that is committed to early intervention services. We all share in the belief that together we can make a 
difference in the lives of our students and their future educational and post school success. Ms. Carrie Antrim, Principal