Fremont Primary School staff members take great pride in the accomplishments of their students and the relationships they develop 
over the school year. Academic rigor is an integral part of our students’ success, along with an environment designed to support the 
unique growth of each individual student. As a TK–4 school, we emphasize reading, writing, language arts, and math. Our first through 
fourth grade students have the opportunity to participate in our After School Education and Safety (ASES) extended day program, 
which provides them with many means of academic improvement. We offer afterschool support programs in reading, language, and 
math, as well as homework tutoring. Certificated and classified staff members support our extended-day programs. Kindergarten 
students after school program focuses on letter identification, and letter sounds. Parents are an essential part of all our endeavors, 
and we encourage you to join our School Site Council or English Language Advisory Committee.We look forward to working with you 
and our students on another year of academic success! 
Leo Monroy, PRINCIPAL 
 
Fremont Primary School’s Vision and Mission Statements 
 
School Vision 
 
Fremont Primary School is committed in providing all students with an opportunity to reach high academic standards, by promoting 
high student achievement, in a safe school environment. 
School Mission 
 
The staff of Fremont Primary School is dedicated to the principle that ALL students can learn. We believe all students must be given 
the opportunity to acquire the knowledge and self-confidence necessary for their next educational step. We believe our goals can best 
be attained when there is a partnership between school and parents.