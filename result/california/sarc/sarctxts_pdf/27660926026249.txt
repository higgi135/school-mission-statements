Olson Elementary is a Transitional Kindergarten – 5th grade school that has approximately 360 
students. Olson has one principal, one academic coach, and 18 certificated teachers. Olson 
Elementary School is an ethnically diverse school in the City of Marina. Of the 360 students at Olson, 
approximately 46% are Latino, 19% are white, with the remaining student body comprised of 
students that are of African American, Filipino, Asian, and Pacific Island decent. Approximately 25 
students are enrolled in our LEAP program (Moderate/Severe K-5) which provides language 
enrichment, academic performance and social support for students with autism and/or 
communication disorders who are unable to access the general education curriculum at, near, or 
above grade level. In addition, Olson offers a Resource Specialist Program to serve the unique needs 
of all students. 
 
The Olson Mission is that Olson students and staff are committed to learning with perseverance 
and purpose each day. We strive to do our best, both in academics and behavior. We embrace all 
individuals and cultures because we are better together. The Olson Vision is that All Olson students 
will become productive citizens who can think critically, collaborate effectively, communicate their 
thought processes, and use technology with competence. 
 
Olson is especially proud of the family feeling shared among students, staff, and parents. A 
wonderful, supportive community of families and staff work together to create an excellent 
learning environment for our students. Olson proudly maintained and increased our strong 
academic programs. Olson School is a special place with diverse and motivated students, a 
dedicated staff, and involved parents and community—the recipe for success and excellence. We 
are proud of our accomplishments and look forward to further improvements. 
 
 

 

 

----

---- 

----

---- 

Monterey Peninsula Unified 

School District 
700 Pacific St. 

Monterey, CA 93942-1031 

(831) 645-1200 
www.mpusd.net 

 

District Governing Board 

Mr. Tom Jennings, President 

Dr. Jon Hill, Clerk Vice President 

Ms. Wendy Root Askew 

Ms. Debra Gramespacher 

Dr. Bettye Lusk 

Ms. Alana Myles 

Dr. Amanda Whitmire 

 

District Administration 

Dr. PK Diffenbaugh 

Superintendent 

Cresta McIntosh 

Associate Superintendent 

Educational Services 

Beth Wodecki 

Assistant Superintendent 

Secondary 

Bijou S. Beltran 

Assistant Superintendent Human 

Resources 

Ryan Altemeyer 

Associate Superintendent Business 

Services 

Marci McFadden 

Chief of Communications and 

Engagement 

 

2017-18 School Accountability Report Card for Ione Olson Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ione Olson Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

21 

20 

15 

0 

9 

0 

0 

2 

0 

Monterey Peninsula Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

441 

40 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ione Olson Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.