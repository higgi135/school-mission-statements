Sunny Brae Middle School (SBMS) is a 2009 National Blue Ribbon School for 6th through 8th 
students in the Arcata School District. There is one elementary school and one middle school in the 
district with a total population of 525 students. Arcata is a small university town located 265 miles 
north of San Francisco on the Northern Coast of California. Arcata is home to Humboldt State 
University and is best known for its redwood trees and university town atmosphere. 
 
Sunny Brae has an enrollment of 220 students. The school population draws from a wide range of 
socioeconomic groups, including university faculty, staff, and students; professionals; blue collar 
workers; and artists. The school has approximately 55% of its students on free and/or reduced 
lunch. 
 
Sunny Brae has a full time principal, a 4 day a week psychologist/counselor, a half-time 5 day a 
week counselor, 9 regular education teachers, 1 part time teachers, 2 special education teachers, 
a half-time music teacher, and 2 half-time physical education teachers. We have a comprehensive 
library/technology center open to students and staff. Title 1 support labs at each grade level are 
staffed with highly trained paraprofessionals. The office staff is composed of two full time 
employees, an attendance clerk and school secretary. Sunny Brae Middle School has been 
recognized for excellence over the last eight year period. In 2009, SBMS was one of only 320 schools 
nationwide to receive a National Blue Ribbon Award. Sunny Brae has been recognized four times 
as a California Honor School and four times as a Title 1 High Achieving school. Many of our students 
go on to serve as student leaders and academic scholars in high school. 
 
SBMS students have a core academic program that consists of Mathematics, Language Arts/English, 
Social Studies/History, and Science. This is complimented by electives, physical education, and core 
support classes. 
 
SBMS offers a rich elective schedule each trimester for 6th, 7th and 8th grade students. Our strong 
elective program offers students the opportunity to expand their learning and have an element of 
choice in doing so. Electives include but are not limited to Art, Spanish, Integrated 1/2 math, Glee, 
Instrumental Music, School Musical, MathCounts, Computer Lab, Literature enrichment, Science 
Exporation, Zumba and Video technology. 
 
Mission Statement 
Sunny Brae Middle School is committed to providing a comprehensive program in a safe 
environment that serves the academic, physical, social and emotional needs of our students as they 
move from childhood to adolescence. Working as a team with parents and the community, we will 
assist students in developing the necessary skills to become responsible, independent, and self-
sufficient learners in a global community. 
 

2017-18 School Accountability Report Card for Sunny Brae Middle School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Sunny Brae Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

13 

12 

14 

0 

0 

0 

0 

0 

0 

Arcata Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

31 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Sunny Brae Middle School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.