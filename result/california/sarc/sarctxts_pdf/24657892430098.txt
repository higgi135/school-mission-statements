School Description 
Golden Valley High School is one of six comprehensive high schools in the Merced Union High School District. The school was 
established in 1994 as a comprehensive high school, the second in the city of Merced. Merced County is the seventh most ethnically 
diverse community in the United States. US Census Bureau data provides the following demographic figures: 30.2% Non-Hispanic 
white, 56.8% Hispanic, 4.2% black, 8.1% Asian/Pacific Islander and 3.0% other races. The Hispanic population in the county has shown 
a continued increase in numbers, thereby decreasing the percentage of all other groups. 52.0% of the county residents are non-native 
English speakers who speak Hmong, Mien, Lao, Spanish and Punjabi. 82% of the non-native English speakers use Spanish as their 
primary languageThere are four feeder school districts; one private, three public. The largest feeder district is Merced City Schools, 
followed by Weaver Elementary School District, El Nido Elementary School District and Our Lady of Mercy Catholic School. 
 
Our goal is to provide the Golden Valley High School students with the best educational programs in the area. Student and parent 
involvement are crucial components for the success of our students and we offer a wide array of excellent programs. 
 
Golden Valley Cougar P.R.I.D.E. Posture 
 
P Positivity: 
Be positive. Assume the best about people and yourself. 
 
R Respect: 
Practice good manners. Be kind and accepting of difference. 
 
I Integrity: 
Be fair. Be trustworthy. Do the right thing even when no one else is looking. 
 
D Determination: 
Do not give up. Instead pick yourself up and try again. 
 
E Excellence: 
Be responsible. Do not settle for anything but your best. 
 
PRIDE Posture was a collaborative effort that included input from staff, students, and the community.