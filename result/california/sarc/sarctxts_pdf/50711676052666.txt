Enslen School was built in 1929 and recently celebrated its 89th year. It is located on a shady, tree-lined campus in an established 
neighborhood near the center of Modesto. The staff, parents, students, and community have a long-standing sense of "family" and 
pride for their school. Enslen has focused on developing and implementing educational programs that are responsive to the needs of 
students in an ever- changing world. Teachers, parents and support staff collaborate to provide a challenging and supportive 
environment for all students. The small school atmosphere lends itself well to fostering a rich, challenging learning climate and 
providing opportunities for children to develop emotionally, socially, and physically. 
 
Enslen has earned the reputation of maintaining high academic expectations and providing a strong educational program. Many of 
our parents and some grandparents are former Enslen students, and they have deliberately located in the Enslen area so that their 
descendants could continue the tradition of being Enslen graduates. Parents are actively involved and serve in leadership capacities 
through the Parent Involvement and Engagement Committee, English Language Parent Parnership, and the Parent Teacher Club (PTC). 
Parent volunteers are visible daily, providing support and assistance throughout the school. Our educational environment and teaching 
strategies meet the goals and objectives of a strong curriculum. The Enslen faculty utilizes every opportunity for grade level 
collaboration centered on analyzing student assessment data and utilizing effective instructional strategies to meet the needs of our 
students. 
 
Although Enslen is a small school with somewhat limited resources, the emphasis has been on utilizing funds to provide programs, 
services and technology that maximize student performance. Enslen’s warm and friendly atmosphere, positive learning environment, 
and tradition of excellence are the products of partnerships between staff, students, and the community.