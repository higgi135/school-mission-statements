Alta Loma Middle School is located in the city of South San Francisco, a growing biotechnology 
center and home of the historical sign “South San Francisco: The Industrial City” which can be seen 
from both air and land. There are currently nine elementary schools, three middle schools, two high 
schools, one continuation high school, an adult program and a pre-school program in South San 
Francisco Unified School District. Alta Loma Middle School was established in 1957, and during the 
2018-19 school year 702 students were enrolled in grades six through eight on a A/B Block 
schedule. Alta Loma Middle School has a strong curriculum that teaches the Common Core State 
Standards and reflects the interest and needs of our students. Alta Loma Middle School is a 
Professional Learning Community as teachers collaborate often to deliver quality lessons and 
evaluate student assessment results. Teachers are designing lessons that teach our students 21st 
Century Learning Skills: collaboration, communication, critical thinking, and creativity. Once a 
semester the staff will implement a school wide cross curricular based project, by grade level, 
involving all teaching disciplines. Computer technology, physical education, art, along with a library 
program that gives access to a rich variety of literature are some of the amenities available to 
students. Student technology use is important in instruction and learning. The ratio of students to 
computers is 2.5 to 1. Language Arts, Mathematics, Social Studies and Science departments each 
hold a Family Night to bring the Alta Loma Community together to participate in subject-related 
games to give parents an idea of the curriculum and standards students are learning in middle 
school. In addition, there is a student mentorship, anti-bullying program, recognition program and 
positive incentive program that contributes to a safe and positive learning environment. Individual 
student needs are taken into consideration at Alta Loma Middle School. Academic Instruction and 
Academic Support Classes, Speech and Language Program, English Language Learner programs, 
school counseling program, and an outreach counseling service: Youth Services Bureau are 
important parts of the educational program. In addition, Alta Loma Middle School has an 
outstanding instrumental music program where 20% of our students participate in one or more of 
the following bands: Beginning Band, Intermediate Band, Advanced Band, and Jazz Band. Alta Loma 
Middle School has a Response to Intervention Academic and Behavior Plan that is designed to 
improve student learning and behavior. Alta Loma's sports program, which involves a large 
percentage of the student population, is supported by the South San Francisco Parks and 
Recreation Department, parents, and staff. At the end of 2015-16 school year we began 
transforming our outdated library into a 21st Century Learning Commons that will include an 
Internet Cafe, New fiction and non-fiction books, e-subscriptions, learning carrels and a classroom 
where teachers can use with their students. 
 
In 2018-19 the Math After Dark is the first of a series in place to offer parent nights to educate 
parents on how to cope with middle school children and their academics. In addition, a Multi-
Cultural Night is being planned to augment our celebration of the different cultures that populate 
our school. 
 
Mission Statement: 
The mission of Alta Loma Middle School is to provide a safe learning environment where students 
can develop a positive self-image and acquire the necessary skills to further their intellectual, 
physical, and social development. 
 
 

2017-18 School Accountability Report Card for Alta Loma Middle 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Alta Loma Middle 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

32 

34 

32 

0 

0 

0 

0 

3 

0 

South San Francisco Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

412 

12 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Alta Loma Middle 

16-17 

17-18 

18-19