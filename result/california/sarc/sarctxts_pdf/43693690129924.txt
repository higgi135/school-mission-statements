Autumn Zangrilli returned to her home state of California after being nine years away. After graduating from the University of California 
at Berkeley with degrees in English and Theater, Autumn joined Teach for America and moved from California to Brooklyn to teach 
7th and 8th grade reading and writing. In that role, Autumn received extensive training from the Teachers College Reading and Writing 
project, joined up with Teach for America as a curriculum writer and content specialist, served as a student activities coordinator, a 
literacy coach for a staff of 25 teachers, a FellowAdvisor for the NewTeacher project, and an adjunct professor at Pace University 
where she received a Master of Science in teaching. Following these experiences, Autumn and her husband moved to Rome, Italy to 
teach at an American International school and broaden their experience. Autumn joined the KIPP Team and Family in 2010 as a Miles 
Fellow at KIPP AMP Academy in Brooklyn, NewYork as a Dean of Teaching and Learning. In her spare time Autumn and her husband 
enjoy their newest challenge- being the parents of a toddler! 
 
Description 
KIPP Prize Preparatory Academy is a free, open-enrollment, public charter school open to all students in San Jose. The middle school 
opened in the Alum Rock district in July of 2014 with 100 fifth grade students. Each year, the school has added a grade level and now 
the school is fifth-through-eighth grade middle school. 
 
Mission 
At KIPP Prize Preparatory Academy we know that the prize will not be sent to us. We have to win it. We know that some of the most 
competitive and robust opportunities in life and career exist in our own backyard- and we relentlessly prepare our students to be able 
to access those opportunities. 
 
We provide a rigorous literacy-based, college preparatory curriculum combined with an emphasis on personalized learning, character, 
and a diverse range of cultural and enrichment experiences to enable our students to access the opportunities in the Silicon Valley 
and beyond. We believe that an excellent school is built when a thriving student and staff culture work in tandem, to climb the 
mountain to college. 
 
We know that on our journey to college-readiness, there will be challenges. In partnership with our engaged and active families, we 
persist in our mission by approaching each second of each day of each year with joy and love- drawing on our most joyful and creative 
moments and keeping our eyes on the prize of our choice.