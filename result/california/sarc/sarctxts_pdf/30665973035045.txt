Newport Harbor High School (NHHS), founded in 1930, is proud of its rich tradition as an 
outstanding school, confirmed by its designations as a California Distinguished School, a National 
Blue Ribbon School, and currently as an International Baccalaureate School. Many NHHS graduates 
continue to reside in the area and watch as their children, grandchildren, and even great-
grandchildren graduate. 
 
Newport Harbor High School educators and administrators are committed to providing an 
outstanding instructional program to all students. Through a variety of innovative and specialized 
programs, faculty and staff ensure that each student is provided opportunities to reach his or her 
full potential. 
 
Through a process of shared decision-making, the faculty and staff at Newport Harbor High work 
closely to create a dynamic and challenging school environment that engages the entire Newport 
Harbor school community. 
 
Our Mission and Vision and Beliefs statements have been developed collaboratively, as have our 
ESLRs. All reflect the Newport Harbor High School shareholder’s view of our educational priorities 
and goals. They are posted throughout our school as a reminder to all of our purpose and priorities. 
 
Vision Statement (Long-term goal of Newport Harbor High School): 
Founded in 1930, fostered through a process of shared decision-making, the faculty and staff at 
Newport Harbor High School work closely to create a dynamic and challenging school environment. 
 
Mission Statement (Short-term, daily goals at Newport Harbor High School): 
We believe all students will become productive and successful members of our global society by 
challenging each student to his or her academic and personal potential. 
 
WE ARE: 
S-Scholarly 
A-Artistic 
I-Interconnected 
L-Leaders 
O-Outstanding 
R-Responsible 
 
P-Persistent 
R-Respectful 
I-Innovative 
D-Diligent 
E-Enlightened 

----

---- 

Newport-Mesa Unified School 

District 

2985 Bear St. 

Costa Mesa, CA 92626 

(714) 424-5033 
www.nmusd.us 

 

District Governing Board 

Charlene Metoyer, President 

Martha Fluor, Vice President 

Dana Black, Clerk 

Ashley Anderson, Member 

Michelle Barto, Member 

Vicki Snell, Member 

Karen Yelsey, Member 

 

District Administration 

Dr. Frederick Navarro 

Superintendent 

Mr. Russell Lee-Sung 

Deputy Superintendent, 
Chief Academic Officer 

Ms. Leona Olson 

Assistant Superintendent, 

Human Resources 

Mr. Tim Holcomb 

Assistant Superintendent, 

Chief Operating Officer 

Dr. Sara Jocham 

Assistant Superintendent, 

Student Support Services/SELPA 

Mr. Jeff Trader 

Executive Director, 

Chief Business Official 

Dr. Kirk Bauermeister 

Executive Director, 
Secondary Education 

Dr. Kurt Suhr 

Executive Director, 

Elementary Education 

 

 

2017-18 School Accountability Report Card for Newport Harbor High School (Home of the Sailors) 

Page 1 of 14 

 

 
School and Community 
Established in 1930, Newport Harbor High School (NHHS) is the largest of the four public high schools located within the Newport-Mesa 
Unified School District that serves a total of 21,884 students from the suburban communities of Newport Beach and Costa Mesa 
(combined total population of 170,000). The campus is located 45 miles south of Los Angeles and 100 miles north of San Diego. The 
student body at NHHS is quite diverse. Of the 2397 students enrolled for the 2018-2019 school year, nearly 40% represent different ethnic 
minorities (66% Caucasian, 30% Latino, 1.2% African American, 2% Asian). 
 
Parents established twenty-five private non-profit (501c3) educational foundations to support and promote academic excellence and 
enhance academic opportunities for all NHHS students through additional financial support-the Newport Harbor Education Foundation 
and Newport Harbor Athletic Foundation are just two of the twenty-five. 
 
School Characteristics 
Newport Harbor is a four-year high school, serving grades 9-12. Students take semester and year-long classes. 
 
Accreditation and Recent Distinctions 
*California Distinguished School, 1999 and 2005 
*National Blue Ribbon School, 2000 
*Six Year Western Association of Schools and Colleges (WASC) Accreditation through June, 2020 
 
Programs and Pathways 
NHHS realizes that one size does not fit all in high school. With that in mind we offer a variety of programs and pathways for our students 
to enhance their education. 
 
AP-Advanced Placement Courses: These are rigorous college level courses that culminate in a required AP Exam administered by the 
College Board. AP classes begin at the 10th grade and depending on their exam score and college policies, students may earn advanced 
placement and/or college credit for successful completion of these courses. AP courses include: Art History, Art Portfolio, Biology, 
Calculus, AB, BC, Chemistry, Computer Science, English Language and Composition, English Literature and Composition, Environmental 
Science, European History, Government and Politics: US, Physics, Psychology, Spanish Language, World History and US History. 
 
International Baccalaureate Program: This college level, fully integrated course of study requires examinations in the five basic academic 
areas, with a sixth examination selected from one of several options. This program begins at the 11th grade and continues through the 
12th grade. 
 
The International Baccalaureate organization (IBO) was started by a group of dedicated teachers, including many Americans, at an 
international school in Geneva in 1968. Since international schools are not tied to a particular country or state, they wanted to develop 
standards and a curriculum that would meet university requirements around the world. They also wanted a curriculum which centered 
on critical thinking, open-mindedness and “big picture” thinking. Students earning the IB Diploma receive advanced college credit at 
prestigious universities around the world. Many highly competitive colleges and universities recognize the IB Diploma for admissions 
and/or advanced standing. University of California schools award a full year’s college credit to a diploma student who scores 30 out of 45 
on the IB assessments. IB is a two-year program in the junior and senior years. Students take courses in six academic areas. 
 
Group 1: Studies in Language and Literature 
Group 2: Language Acquisition 
Group 3: Individuals and Societies 
Group 4: Experimental Sciences 
Group 5: Mathematics 
Group 6: Arts & Electives 
 

2017-18 School Accountability Report Card for Newport Harbor High School (Home of the Sailors) 

Page 2 of 14 

 

The courses are classified as Higher Level (HL) or Standard Level (SL). Higher Level courses require two years of study in advanced courses 
and Standard Level courses require one or two years of study in advanced courses. At least three and not more than four of the six subjects 
are taken at the Higher Level (HL). The balance of courses are taken at the Standard Level (SL). This allows students to explore some 
subjects in depth and some more broadly. Students will complete three additional requirements known as the core of the IB program. 
One requirement is a unique course known as Theory of Knowledge in which they will explore how we acquire knowledge and the 
connections between various disciplines. The second requirement is that over the two year period they will participate in and reflect upon 
activities of their own choice involving creativity, action and service (CAS). Finally, they will write an extended essay (research paper of 
4,000 words) during the 11th and 12th grade years. IB courses are also open to students who do not choose to take the full two year IB 
Diploma program, but are interested in taking some of the IB classes offered. These students must complete all class requirements and 
pass the IB exam in order to achieve the recognition of universities for participating in an IB course. Achievement of the International 
Baccalaureate Diploma is assessed using international standards. Each examined subject is graded on a scale of 1 to 7 with 7 being the 
highest score possible. Additional points may be earned for completing the required Extended Essay and Theory of Knowledge essay. The 
award of the Diploma requires a minimum total of 24 points. IB test scores have no effect on the final grade in the class, only on the 
awarding of the diploma. 
 
AVID (Advancement Via Individual Determination) 
A special program designed to prepare students, who have not sought out the opportunity, to succeed in a college preparatory path, for 
admission to four-year universities and colleges. The objectives of the program are to provide students with college level entry skills and 
improve their coping skills towards academic success. 
 
Four Career Technical Education (CTE) pathways-Business, Computer Science, Culinary, and TV/Film 
 
CTE PATHWAYS AT NHHS 
Business Management Pathway 
11th Grade- IB Business Management HL1 
12th Grade- IB Business Management HL2 
 
Culinary Arts Pathway 
9th Grade - Foods 
10th Grade - ROP Baking and Pastry 
11th Grade - ROP Culinary Arts 
12th Grade - ROP Advanced Culinary Arts 
 
Computer Science Pathway 
9th Grade - Exploring Computer Science 
10th Grade - Introduction to Computer Science with Python 
11th Grade - AP Computer Science Principles 
12th Grade - AP Computer Science 
 
Digital Media Pathway 
10th Grade - ROP Film Digital Arts 
11th Grade - ROP TV Video 
12th Grade - ROP Broadcast News TARtv 
 
Construction Technology Pre-Apprenticeship 
12th Grade - ROP Construction Technology Pre-Apprenticeship 
*Construction Tech is offered to all NMUSD seniors during 1st period at Estancia High School 
 

2017-18 School Accountability Report Card for Newport Harbor High School (Home of the Sailors) 

Page 3 of 14 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Newport Harbor High School (Home of the 
Sailors) 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

92 

89 

87 

0 

2 

1 

1 

1 

1 

Newport-Mesa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1105 

5 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.