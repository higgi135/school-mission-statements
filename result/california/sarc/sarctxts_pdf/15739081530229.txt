Vision Statement: McFarland Independent School's vision is to encourage, guide, and support each student to rise to their highest 
potential in 21st Century skills so that they may be effective communicators, innovators, and participants in the global economy. 
 
Mission Statement: McFarland Independent School's mission is to ensure each student graduates with a high school diploma and is 
ready to enter college and/or a career technical school. 
 
McFarland Independent School (MIS) is part of the McFarland Unified School District (MUSD), a K-12 district that serves an enrollment 
of approximately 3,600 students. McFarland Independent School is a small alternative independent study school where students 
meet with a certificated teacher weekly. Attendance is based on completed assigned work. Earned credits are determined by 
assignments completed and unit assessments given by the teacher. MIS is located in the small, rural and predominantly agricultural 
community of McFarland. Despite the agricultural basis for most employment, the mobility rate remains low which creates a relatively 
stable community. However, with a high percentage of non-English speaking families, with many experiencing extreme poverty, and 
in light of its geographic isolation as well as few resources and services, the community faces many challenges. 
 
There are a variety of reasons for choosing MIS which may include: personal hardships, pregnancy/parenting, special needs for work 
scheduling, and as a positive alternative to the larger high school setting for high-risk students. The goal of each McFarland 
Independent School student is to earn enough credits to transfer, back on track for graduation, to a continuation or traditional high 
school or to acquire a high school diploma. The student population is generally considered “transitional”, usually short term and limited 
to one or two semesters. This enables the student to either make-up missing credits in order to graduate with his/her original class, 
overcome personal crisis, finish up a work season, or to obtain one-on-one assistance. However it should be noted that a considerable 
number of students have graduated from this school. MIS is a Dashboard Alternative School Status (DASS) school.