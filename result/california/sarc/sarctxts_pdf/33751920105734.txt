Great Oak High School 
is the newest high school in the Temecula Valley Unified School District (TVUSD), opening its doors in 2004. The school lies at the base 
of Wolf Valley and is surrounded by foothills and suburban homes. 
 
 GOHS's student enrollment in 2017/18 was approximately 3,300. GOHS earned California Distinguished School recognition in 2009 
along with the Gold Ribbon and Green Ribbon Awards in 2017. Great Oak High School offers many programs to assist college-bound 
students including our distinguished IB and AP programs. We also have numerous CTE Pathways and newly developed capstones that 
will provide students with the 21st century skills they will need to be successful in the workforce and many college level programs. 
These programs help us work towards our goal of ensuring that all students are college and career ready and A-G compliant. 
 
 
 The staff is comprised of a group of highly qualified, idealistic educators who are taking advantage of the opportunity to try some new 
methods of assessment and education. Philosophically, GOHS is built upon a set of “Core Beliefs” known as the Branches of Excellence, 
also referred to as the SPIRIT of Great Oak. Scholarship, Passion, Integrity, Reflection, Involvement, and Teamwork comprise these 
core values. 
 
 
 GOHS staff and administration believe that students connected to their school are more successful. Our goal is to involve at least 95% 
of our students in a significant capacity through activities, athletics, clubs, and/or the performing arts. We are able to measure student 
involvement on campus using the 5 Start Students software program; student ID's are scanned at various events both during and after 
school hours. This allows us to have data that we can use to drive next steps towards getting students involved on campus. We also 
recognize academic excellence on a regular and consistent basis in order to help promote student success and involvement. 
 
 
 The GOHS structure is designed to provide a variety of opportunities for our students. GOHS houses 134 classrooms for instruction 
including department rooms that allow for teachers to collaborate. Each classroom contains a computer workstation, an LCD projector 
or Smart Board, and voice amplifiers for presentation purposes. GOHS also houses many specialized rooms in order to meet the 
requirements of the curriculum. The specialized classrooms include: 1 open-use computer lab; 1 computer science lab; 14 Chromebook 
carts; 14 science labs; a library; a culinary arts kitchen; fashion design room; ROTC with office and wardrobe rooms; 3 art rooms (1 
ceramics); TV Video production studio; an engineering/architectural design room; student store; training room; support den for 
exceptional students; college and career center; gymnasium (includes dance room, wrestling room, and weight room); and the 
performing arts complex (includes band room, choir room, activities room, and drama). 
 
 
 The campus also includes athletic fields for baseball, softball, and soccer, outdoor basketball and volleyball courts, sand volleyball 
courts and a stadium with a synthetic turf football/soccer/lacrosse/field hockey field and rubberized track. A new Aquatics Center 
opened in Fall 2017 that also serves our adjacent tennis courts. 
 
 
 The most significant aspect of GOHS is the staff. A highly qualified, enthusiastic, and energetic staff is the true leader of the pack. They 
have worked collaboratively to unpack and identify “priority standards.” Benchmark assessments have been developed to provide 
data that informs teachers of strengths or gaps in instruction. A number of departments have critically looked at grading and 
homework practices and have led the charge for a standards-based grading system that focuses on academic performance rather than 
effort and citizenship. We also have a unique, formal intervention program that is organized and monitored by our Intervention 
Specialist with the support of our site Math coach. 
 

2017-18 School Accountability Report Card for Great Oak High School 

Page 2 of 15 

 

 
 Our mission statement is as follows: 
 
Within a dynamic, relevant and nurturing educational culture, the Great Oak community encourages all students to climb the branches 
of excellence to realize their potential. 
The branches of excellence are also referred to as SPIRIT: 
SCHOLARSHIP: Students realize academic, vocational, and personal growth is the goal of education through the use of a rigorous, 
relevant curriculum that provides high standards and expectations. 
PASSION: Students are active participants in their own education and show intellectual curiosity and enthusiasm for learning through 
a commitment to improving themselves, their school, their community, and their world. 
INTEGRITY: Students respect themselves, others, and the environment through academic honesty, forthrightness, and service to their 
community. 
REFLECTION: Students think introspectively about the way learning occurs and understand that the risk of failure leads to an 
opportunity for continued intellectual growth. 
INVOLVEMENT: Students engage in a myriad of activities to develop and utilize their own unique abilities and talents in order to 
improve their school, their community, and their world. 
TEAMWORK: Students work collaboratively with other students, staff, and community members in an effective, constructive, and 
compassionate manner.