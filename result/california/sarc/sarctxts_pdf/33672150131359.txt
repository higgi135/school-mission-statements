Riverside STEM Academy (RSA) in Riverside Unified School District offers an educational option for students who would like to pursue 
their interest and aptitude in the areas of science, technology, engineering and mathematics. The academy provides students with 
accelerated and concentrated experiences and content in an environment that is conducive to individual exploration, innovation, and 
problem solving. RSA integrates STEM content through all curricular areas and provides opportunities for students to interact and 
partner with university faculty and graduate students as well as STEM related community organizations, giving students real-world 
applications and experiences. Students will leave the 5th through 12th grade program prepared to successfully enter, participate, and 
complete secondary and higher education STEM pathways. 
 
Vision, Mission, and the Five Pillars of STEM Instruction 
 
RSA Vision Statement: 
 
We prepare students to excel in STEM fields of study and succeed in 21st century careers as leaders and innovators. 
 
RSA Mission: 
 
Riverside STEM Academy's mission is to provide students a rigorous, interdisciplinary learning environment focused on science, 
technology, engineering and mathematics, to foster the joy of discovery, and to promote a collaborative culture of ethical and 
innovative problem-solving. 
 
 
Five Pillars of Instruction at the Riverside STEM Academy 
 
Science 
 
Mathematics 
 
Research and Design 
 
Communication 
 
Computer Programming 
 
 
 

2017-18 School Accountability Report Card for Riverside STEM Academy 

Page 2 of 18