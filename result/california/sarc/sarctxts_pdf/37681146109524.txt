Live Oak Elementary School has an enrollment of over 600 unique and culturally diverse students 
in Transitional Kindergarten through sixth grade. Our school is situated on a picturesque 12-acre 
site located in the southeastern part of Fallbrook, near historic Live Oak Park. Live Oak Elementary 
has an impressive focus on strong academic achievement, student leadership, and a staff 
committed to meeting the needs of all students. Live Oak teachers are reflective, research driven 
practitioners that construct and use an array of assessments, data results, and instructional 
resources to structure engaging learning opportunities for its students in a compassionate, student-
centered environment. As a Franklin Covey Leader in Me Lighthouse school, Live Oak students are 
engaged, take responsibility for their learning, and respect themselves and others in a safe learning 
environment where high standards, leadership goals, and a growth mindset are of greatest priority. 
We believe in building strong relationships with students and families, and parent involvement is 
of great importance and directly connected to our success at LOE. 
 
At Live Oak, we practice Positive Behavior Intervention and Support practices (PBIS), and equip our 
students with problem solving skills to facilitate social and emotional success. We strive to ensure 
that all of our students will become high school graduates who are self-confident and skilled 
citizens, ready to embrace college and career goals through hard work and perseverance in the 21st 
century through collaboration, communication, critical thinking, and creativity. Our strong focus on 
STEM and hands-on learning sets us apart from other school districts, and gives our students the 
advantage they need to be contributing members of a global society. As a professional learning 
community, we have implemented the California State Standards using a common instructional 
framework, while maintaining a needed focus on our English language learners. The Live Oak staff 
remains committed to providing differentiation and individualized learning through small group 
instruction, a focus on best practices, relevant and engaging instructional strategies, and a strong 
connection to families and community. 

----
---- 
Fallbrook Union Elementary 

School District 

321 North Iowa Street 

Fallbrook, CA 92028-2108 

(760) 731-5400 
www.fuesd.org 

 

District Governing Board 

Siegrid Stillman, President 

Patty de Jong, Vice President 

Lisa Masten, Clerk 

Caron Lieber 

Susan Liebes 

 

District Administration 

Candace Singh, Ed.D. 

Superintendent 

 

2017-18 School Accountability Report Card for Live Oak Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Live Oak Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

29 

29 

30 

0 

0 

0 

0 

1 

0 

Fallbrook Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

245 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Live Oak Elementary School 

16-17 

17-18 

18-19