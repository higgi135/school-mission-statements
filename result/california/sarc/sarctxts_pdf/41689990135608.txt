School Leader Kate Belden started her journey with KIPP in 2007 as KIPP King Collegiate High School’s Founding 9th grade English 
Teacher. Over the past nine years, she has served in many different roles at KIPP King, including Grade Level Chair, Department Chair, 
Dean of Curriculum, Student Government Founder, Assistant Principal of Student Support, and for the past four years, Principal. Before 
working with KIPP, Kate taught 6th to 8th grade English as a Teach For America Corps member at a large public middle school in the 
Washington Heights neighborhood of NewYork City (’04). Kate earned a B.A. from the University of Notre Dame in English and 
Anthropology. In 2006, she received her Masters of Science in Teaching from Fordham University, in 2009 she participated in the KIPP 
School Leadership Program, and in 2010 she earned her Masters of Education in Administration and Supervision from National-Louis 
University. 
 
Outside of school, Kate enjoys cooking, reading, traveling the world, and exploring the beautiful Bay Area. After spending many years 
in high school, Kate has become deeply aware of the importance of an excellent pre K-12 education in order to best prepare our 
students for success in college. She is inspired to return to her middle school roots and to learn about elementary education and she 
is honored to create more educational opportunities for students in the Bay Area! 
 
Description 
KIPP Valiant Community Prep is KIPP’s newest school located in the heart of East Palo Alto. The school was founded in 2017 through 
the hard work of parent leaders in the community fighting for better educational options for their children. Parent leaders chose the 
name for the school because it describes East Palo Alto’s courage and it also has a powerful, recognizable cognate in Spanish—valiente. 
The school opened in 2017 with grades TK, K, 1, and 6, and will continue to grow until it becomes a full K–8 school. KIPP Valiant 
students will apply their knowledge and skills through collaborative, real-world problem-solving using the design-thinking process to 
cultivate empathy, curiosity, and leadership. KIPP Valiant Community Prep is one of 12 KIPP schools in the Bay Area and 209 KIPP 
schools nationally educating 87,000 students on their path to and through college. 
 
Vision 
At KIPP Valiant Community Prep we support our students academically, socially, and emotionally so that they can be successful and 
fulfilled in college and in life. We empower one another to continuously grow into passionate, innovative agents of change who raise 
our unique voices to make the world a better and more equitable place.