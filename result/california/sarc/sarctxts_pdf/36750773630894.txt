Granite Hills High School (GHHS), located on 40 acres in the eastern part of Apple Valley, opened 
its doors to students in the fall of 1999. Now in its nineteenth year of operation, GHHS has an 
enrollment of approximately 1,545 students in grades nine through twelve. 
 
Academic/Activity Programs 
GHHS, a large comprehensive high school, offers a variety of academic programs and support 
systems focused on student achievement. The following list is a sampling of the many student 
programs offered at GHHS. 

-

• Advancement via Individual Determination (AVID) has been expanded and continues to 
serve historically underrepresented college-going groups and to facilitate completion 
of college entrance requirements. 

• Honors classes and Advanced Placement (AP) classes have consistently grown, as have 
the number of students who receive college credits for taking the AP exams. We are 
also the only school in our area which offers the AP Capstone courses. 
Through STEM partnerships with local industry, GHHS has created its first academy, 
System Control and Design Academy (SCADA). The academy is in its sixth year serving 
at-risk students and preparing them for college and career. 

• 

• GHHS has implemented interventions in both math, English Language Acquisition (ELA) 

• 

• 

and Biology. 
English Language Development (ELD) classes are provided for students new to the 
language and offer support for all curricular areas. 
Special education classes are provided to serve those students on Individualized 
Educational Plans (IEPs). 

• California Technical Education (CTE) Pathways are offered in, Culinary Arts - including 
catering and bakery skills, and the medical fields to include Medical Assistant, Med Core 
I, Medical Terminology, EKG Technician, Phlebotomy Technician, EMT and Sports 
Therapy. 

 
This is the first year for our GREAT academy, a CTE Arts Media Entertainment pathway which is our 
Visual and Performing Arts Academy (VAPA). 
 
Mission Statement 
Our mission is to provide a safe, challenging, and supportive environment in which all students 
learn the accepted standards of each discipline and can acquire the knowledge and critical thinking 
skills necessary for leading successful and productive lives as responsible members of society. 
 
Vision Statement 
The administration, faculty, and staff of Granite Hills High School strive to provide a rigorous and 
creative environment by implementing diverse programs and curriculum to meet the unique needs 
of the 21st century student. In order to achieve this goal, Granite Hills High School will utilize 
engaging, research-based lessons supporting the common core standards. 
 

2017-18 School Accountability Report Card for Granite Hills High School 

Page 1 of 12 

 

Student Learning Goals 
The staff is expected to ensure that all graduates will be able to: 

 
 

 
 

communicate effectively in various forms. 
acquire and apply knowledge and processes based upon accepted state standards as necessary for developing critical thinking, 
problem solving, and career planning skills. 
use technology effectively. 
understand the importance of being responsible and productive members of local, national, and global communities. 
 

Behavior Expectations through Positive Behavioral Intervention and Supports (The three "B's") 

 Be Respectful 
 Be Accountable 
 Be Safe 

 
All Students will be expected to: 

respect staff, students and school rules. 
illustrate integrity of character. 

 participate in activities which enrich our community. 
 
 
 demonstrate tolerance for differences in culture, beliefs, language, appearance, and lifestyle. 
 exhibit academic excellence.