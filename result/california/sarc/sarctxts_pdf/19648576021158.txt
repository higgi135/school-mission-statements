Palmdale School District Mission Statement 
 
The mission of the Palmdale School District is to provide each child with a rigorous and relevant academic education, a safe learning 
environment and the knowledge, skills and attitudes necessary for success in the 21st Century. 
 
At Manzanita School, administrators, teachers, support staff, parents and students are all necessary partners in enabling every child 
to reach his/her educational and personal goals and to realize his/her full potential. 
 
The Manzanita School Mission Statement reads as follows: 
 
Manzanita School believes that all students can learn, achieve, and be successful at school and in life. We pledge to maintain a safe, 
supportive environment, and to set and model high academic and citizenship standards. We believe in our children and that they can 
grow into respectful life long learners who will make positive contributions to society. 
 
Additionally, the Manzanita School staff embrace the District Mission Statement, which is: 
 
The mission of the Palmdale School District is to implement our vision with actions and services targeted to students, parents, and 
staff so our students can live their lives to their full potential. 
 
Manzanita School sponsors educational activities and projects that involve and call on the entire community, in an effort to inspire 
learning beyond the classroom. The staff models a commitment to the lifelong pursuit of learning as they participate in staff and 
personal development activities and as they share their gleanings with their students. 
 
Manzanita envisions a community of learners working together, sharing their expertise and interests, and contributing to an improved 
society. 
 
Manzanita has implemented a Kindergarten, First & Second Grade Dual Immersion program. Students are receiving 50% academic 
instruction in both Spanish and English throughout the day.