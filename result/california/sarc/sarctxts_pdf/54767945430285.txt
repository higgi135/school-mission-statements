Woodlake Community Day School (WCDS) is a school primarily established for students of Woodlake Unified School District that have 
been expelled or have been referred for disciplinary or attendance issues. The school is for students 7-12 grades. The goal for the 
school is to provide daily standards based education for those students who have been referred to WCDS. The location of the school 
is 3 miles from the district office in a country environment. The school sits on the grounds of the Woodlake High School agricultural 
farm. The students are in school from 8 a.m. through 2:45 p.m. Students are transported to the site by district bus services. The 
school environment in that it is a single classroom with 1 lead teacher and 1 aide. The maximum capacity for the class has been 
established at 15. 
 
The WCDS Vision statement is to provide a structured learning environment that meets the educational needs of each student while 
developing self-esteem, social skills and civic responsibility. 
 
The Mission statement of WCDS is to assess the educational needs of each student; provide an individualized course of instruction for 
completion, to utilize all available resources to provide opportunities for academic success, and to teach our students to accept 
personal responsibility for their present and future actions.