La Entrada School is located in the city of Menlo Park in San Mateo County, California. It is one of two schools in the Las Lomitas 
Elementary School District and includes grades four through eight. Graduates of La Entrada School attend Menlo-Atherton High School 
or Woodside High School in the Sequoia Union High School District, as well as private preparatory schools throughout the San 
Francisco Peninsula. 
 
The Las Lomitas Elementary School District includes the western-most part of Atherton, an unincorporated section of San Mateo 
County between Atherton and Menlo Park, the western section of Menlo Park (Sharon Heights), a portion of Woodside, and the 
community of Ladera (unincorporated San Mateo County). La Entrada participates in the Voluntary Transfer Program and receives 
approximately five percent of its population from East Palo Alto and East Menlo Park. The 2017-18 enrollment at La Entrada was 794 
students. La Entrada parents attach an extremely high value to rigorous academic programs and positive social and emotional growth 
without sacrificing art, music, and physical education. The parent community is well established and provides strong support to La 
Entrada School by contributing time and other resources to help achieve the school's educational goals. There is little transience. Most 
of the students entering La Entrada School in the fourth grade stay through 8th grade. More school information can be found at the 
La Entrada website at http://le-llesd-ca.schoolloop.com/ 
 
La Entrada Operational Vision Statement: 
La Entrada is an inclusive community of dedicated staff, students, and parents engaged in maximizing the academic, social, ethical and 
physical growth of every student. 
 
Our collaborative community embraces the ideals of critical thinking, life-long learning, and global responsibility in a safe and 
respectful environment.