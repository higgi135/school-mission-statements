School Mission Statement: The Loma Vista Mission is to continuously create a Dual Immersion environment of "Teaching and 
Learning," where students work to become bilingual, biliterate and bicultural. Where students build social, emotional, and academic 
capacity to function in a global environment in which we treat each other as neighbors in a world with a multiplicity of languages and 
cultures. 
School Description: In May 2009 the Old Adobe Union School District approved the opening of a Dual Immersion Program with two 
Kindergartens and one First Grade class comprising a total of 55 students. By August of 2013, the program had grown to over 200 
students and moved to the campus back to where it started - Bernard Eldredge Elementary School, now the Loma Vista Immersion 
Academy. In June 2015, the first Sixth Grade class was promoted to Middle school. In August 2017, Loma Vista Immersion Academy 
opened with 400 students. Through the District's continued vision, Loma Vista provides the community a choice that meets the needs 
of students for 21st Century Learning. The Dual Immersion Program at Loma Vista emphasizes a bilingual, bi-literate and bi-cultural 
community with positive cross-cultural attitudes and skills in an enriched experience to function in a global environment with social, 
cultural and economic diversity. Among the curriculum programs Loma Vista includes a language development emphasis through 
GLAD Curriculum (Guided Language Acquisition Design). All staff are bilingual and bi-literate and provide instruction through the 90/10 
Two-Way Dual Immersion Model. In this model, students receive instruction in Spanish beginning in Transitional Kindergarten for 100 
% of the day. This continues through first grade, and English instruction increases as students progress through the grades. In 4th 
through 6th grade a 50-50 Spanish/ English instructional model is implemented to meet the academic needs of students moving to 
middle and high school. In addition to the dual immersion emphasis, and with a staff of 19 teachers and 20 additional support staff, 
Loma Vista continues to provide traditional yet innovative educational strands found at other Old Adobe Union School District schools. 
The New Generation Science Standards, a fully equipped Science Lab, state of the art Apple Technology, rigorous art and music 
programs, a fully operational library staffed with a bilingual librarian, and a trained PE Tech. Classroom instruction is enhanced through 
rigorous implementation of Bridges Math, Project Based GLAD Units that focus on literature, social studies and language development. 
The Loma Vista Community is enhanced by a strong parent community focused through the Loma Vista PTA. The parent community 
contributes to a rich student experience through classroom volunteers and coordinated activities with classroom teachers.