J. P. Kelley Elementary School is committed to teaching and learning the essential academic and 
social skills by modeling kindness, showing effort, and being safe. We will optimize student learning 
through positive relationships, rigorous intentional instruction, relevant assessments, and 
accountability for all. Houghton Mifflin remains our core Reading/language arts program for 
kindergarten through fifth grades. Our new text book adoption in Mathematics, which includes the 
new Common Core Standards, is the Houghton Mifflin Go Math Series. Accelerated Reader 
supplements our core programs in first through fifth grades. Accelerated Reader allows students 
to be responsible for their learning through goal-setting, practicing, and responding to immediate 
feedback. We are proud of our school and students. We welcome you to visit and be an active 
participant in your child’s education as we work towards another year of academic success. 

 

 

-------- 

Rialto Unified School District 

182 East Walnut Ave. 

Rialto, CA 92376 
(909) 820-7700 

www.rialto.k12.ca.us 

 

District Governing Board 

Edgar Montes, President 

Nancy G. O'Kelley, Vice President 

Dina Walker, Clerk 

Joseph Ayala, Member 

Joseph W. Martinez, Member 

Jazmin Hernandez, Student 

Member 

 

District Administration 

Dr. Cuauhtémoc Avila 

Superintendent 

Dr. Daren McDuffie 

Lead Strategic Agent: Strategics, 

Congruence & Social Justice 

Kelly Bruce 

Lead Innovation Agent, Educational 

Services Elementary Instruction 

Jasmin Valenzuela 

Lead Academic Agent, Liberal Arts 

and Literacy/Intervention 

Dr. Edward D'Souza 

Lead Academic Agent, 

Math/Science and College/Career 

Pathways 

Rhonda Kramer 

Lead Academic Technology Agent 

Rhea McIver Gibbs 

Lead Personnel Agent 

Mohammad Z. Islam 

Associate Superintendent 

Syeda Jafri 

Agent: Communications/Media 

Services 

 

2017-18 School Accountability Report Card for J.P. Kelley Elementary School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

J.P. Kelley Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

28 

0 

0 

0 

0 

0 

0 

28 

0 

Rialto Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

0 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

J.P. Kelley Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.