Welcome to Alila Elementary School Accountability Report Card. With a strong focus on creating grade level readers through engaging, 
rigorous reading instruction, language-rich learning environments, and Professional Learning Communities focused on student 
learning and data, the Earlimart Elementary School District and Alila Elementary are committed to Creating Outstanding Readers in 
Earlimart (C.O.R.E.). 
 
As you read this report, you will find that what emerges is a picture of a school with a commitment to excellence, a faculty that is 
professionally skilled and personally committed to meeting the learning needs of all students, and a student body that as a whole, is 
well disciplined and motivated. Our school community has a great amount of pride and works as a family. The school staff welcomes 
parent and community participation and strives to create a partnership between parents, students, and the school. Parents are 
welcome to visit or work in the classrooms and are highly encouraged to take part in their children's educational process. 
 
With a large number (72.9%) of English Language Learners (ELL), Alila is proud to provide a strong reading foundational skills program, 
as well as an ELD program that lays the foundation for language and literacy success. We have been able to progress as a school and 
have shown progress on the SBAC test, with our year growth in ELA and Math exceeding the state average growth. 
 
We will continue to focus on ELL students, using the strategies for which the staff received training, building on ways to help these 
students who are having difficulty in reading. We will continue to offer research-based interventions during the school day as well as 
target first best instruction in the classrooms to meet their diverse needs. We will use data to target students at risk and will fit them 
into intervention programs that will specifically service their needs. Our students have one-to-one access to technology in their 
classrooms.