Live Oak High School (LOHS) is a comprehensive high school in the Morgan Hill Unified School 
District that serves students in grades nine through twelve. Live Oak is located in a fast-growing 
suburban area that has managed to retain the aura of a small town. Students come from the cities 
of Morgan Hill and San Jose as well as the unincorporated areas of San Martin and Coyote Valley. 
The student population reflects the rich ethnic and socioeconomic diversity of the community. 
 
Live Oak High School is committed to providing a learning environment that enables all students to 
meet or exceed state standards. The staff examines a variety of data in cycles of continuous 
improvement. Live Oak is proud of its excellent academic programs, including 17 Advanced 
Placement courses in the curricular areas of Physics, Chemistry, Biology, Environmental Science, 
AB/BC Calculus, Computer Science, Statistics, English Language and Literature, U.S and World 
History, Government and Politics, Music Theory, French, Spanish, and Studio Art. In addition, we 
offer 15 Career Technical Education courses within several career pathways, providing many 
opportunities for students to explore options and develop skills and certification.We are also proud 
of our strong athletic and extracurricular programs, and the wide variety of support services we 
provide for our students. These educational opportunities have led to Live Oak High School's overall 
graduation rate rising to 98%, with critical subgroups exceeding 96%, among the very highest in our 
county. Live Oak garnered two prestigious awards in 2015. A California Gold Ribbon School Award 
was received in honor of counseling and support services provided to our students. Project 
Cornerstone awarded Live Oak the Caring High School Climate Award honoring our excellent school 
climate of acceptance and tolerance. 
 
LOHS continues to provide academic support and tutoring program in partnership with California 
Student Opportunity and Access Program (Cal-SOAP). Tutoring services include all academic 
subjects, with extra support available in math and science. We provide support for students who 
need to remediate courses in which they were unsuccessful through individualized instruction and 
the online Cyber High/PASS program. A dedicated blended learning intervention center, along with 
our library computer center are provided as locations for these services. Both facilities are also 
open within and outside the school day for general student use. We have expanded our AVID 
program as an additional means of support for students who will be the first in their families to 
attend college. LOHS seniors have been commended by the National Merit Scholarship Program 
and recognized as students of the year by our local Rotary and the Morgan Hill Chamber of 
Commerce. 
 

2017-18 School Accountability Report Card for Live Oak High School 

Page 1 of 12 

 

We align curriculum, assessments and instructional practices to the Common Core State Standards and recently aligned our Science 
courses to Next Generation Science Standards. We focus staff development on instructional practices with an equity lens to support 
learning for all of our students. LOHS will continue and refine and enhance school-wide strategies to support English learners, and to 
support the academic vocabulary development of all students by implementing Constructing Meaning strategies across the curriculum. 
By the end of 2018-2019 school year, all teachers will have been training in Constructing Meaning strategies to support the academic 
vocabulary of all students. These efforts build upon Literacy and Academic Language instructional skills garnered from the 2013/14 and 
2014/15 professional development provided by Adams Educational Consulting. That training also provided skill sets for our teaching staff 
on proven best instructional practices, school wide instructional norms, and aligning instruction and content to the Common Core State 
Standards. LOHS staff collaborate every Wednesday to ensure that teachers are using and implementing effective instructional strategies 
in every classroom. Dedicated weekly time allows for the powerful process of teacher collaboration to become routine, further enhancing 
the education we offer our students. Peer observations and teacher "walkthroughs" also continue. 
 
Many groups contribute to our decision-making process. Our School Site Council (SSC), composed of the principal, parents, students, and 
faculty, makes decisions about our curriculum, school policies, and budget. Our English Language Advisory Committee (ELAC) includes 
many parents of English learners, our Bilingual Community Liaison, and our English Language Development Facilitator. The ELAC helps to 
shape our program for English learners. Parents in our Home and School Club (HSC) support our teachers and instructional program and 
provide valuable parent feedback. Our Instructional Leadership Team (ILT) also meets semi-weekly with the principal to analyze student 
achievement data and discuss program improvements. In addition, Student Voices, a group of students from a cross-section of our 
population continues to be a major contributor to our school climate, providing valuable student input and leading initiatives that focus 
on inclusion and mentoring new students. 
 
To serve our students with disabilities, we have Resource Specialist Program (RSP) teachers, Special Day Class (SDC) teachers, and 
paraprofessionals who work with our special education students. In addition, LOHS serves as the host school for Special Day Classes (SDC) 
serving students in the Moderate/Severe and Emotional Disturbance/Therapeutic Programs. Students with disabilities who require more 
structured and intensive support enroll in our SDC class and take most of their academic subjects with an SDC teacher. Classroom 
paraprofessionals also work with these students. Special education staff also work with students and their families to devise a plan for 
post-secondary placement through a comprehensive 9-12 Transition Planning curriculum and our Workability Program. We also have 
incorporated intensive reading support programs, Read 180 and System 44, into our English Skills classes to support literacy development 
for struggling readers. 
 
English learners receive intensive instruction focused on fluency and comprehension skills in English Language Development (ELD) classes. 
English learners also participate in grade-level courses alongside their peers that are co-taught by English and ELD teachers adept at 
differentiating instruction and materials for English learners. This English learners access to rigorous academic content while acquiring 
and enhancing English language skills. Our teachers are either Cross-cultural Language and Academic Development (CLAD) certified or 
Specially Designed Academic Instruction in English (SDAIE) trained. The percentage of ELL students gaining proficiency in English and 
attaining an RFEP status, has steadily climbed in recent years. We encourage the parents of English learners to join our ELAC. 
 
LOHS provides comprehensive guidance and academic counselling services to all students. Counselors hold grade-level parent meetings 
and college nights to inform parents of college entrance requirements and financial aid opportunities. They communicate with families 
about testing dates, upcoming visits from college representatives, financial aid seminars, and application deadlines. The career center 
provides students with information about colleges, universities, and trade schools. The co-location of Cal-SOAP hub in our College & 
Career Center allows for additional counselling staff and expanded services for traditionally underserved subgroups. LOHS offers several 
opportunities for our students whose demographic subgroups are underrepresented in college ranks to participate in field trips visiting a 
variety of colleges, including San Jose State University, to Stanford and UC Berkeley. LOHS introduced the Naviance program in 2016/17 
and is using this comprehensive tool more robustly each year to engage students in research about college choices and other post-
secondary career opportunities (based on an included skills and interest survey component), prep for the PSAT, SAT, ASVAB, Advanced 
Placement Exams. Naviance also tracks the post-secondary endeavors of LOHS students in order to give us accurate data about our 
graduates. 
 
By offering a wide range of courses and support systems, we provide Multi-Tiered Systems of Success (MTSS) approach for all students. 
Our counselors meet with students individually and in groups to help them navigate both academic and personal issues. Student Study 
Teams (SSTs) are held when students appear to need additional supports or encounter challenges to their success. When necessary, SST 
teams identify and connect students with further resources and counseling and/or community agencies. If a student exhibits poor 
attendance, we engage both student and parent in a School Attendance Review Team (SART) process, which seeks to identify underlying 
causes for attendance concerns, and supports that might help to mitigate those problems and improve attendance. In most cases, this 
results in a plan that will keep the student in school. 
 
We continue to seek opportunities to improve communication with parents, students, and the community through the redesigned school 
Website, weekly newsletter, and periodic phone calls in English and Spanish. As a 1:1 technology school, an ongoing area of focus is 
ensuring that students and teachers are using technology as an effective instructional and communications tool. 

2017-18 School Accountability Report Card for Live Oak High School 

Page 2 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Live Oak High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

45 

48 

52 

5 

3 

6 

1 

2 

0 

Morgan Hill Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

388 

7 

10 

Teacher Misassignments and Vacant Teacher Positions at this School 

Live Oak High School 

16-17 

17-18 

18-19