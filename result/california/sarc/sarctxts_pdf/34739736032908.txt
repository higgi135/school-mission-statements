At Arthur S. Dudley Elementary we are dedicated to partnering with families to create a safe and respectful environment that supports 
student learning and development. Our mission is to guide and encourage students to meet or exceed challenging academic 
standards, to establish a connection to school, to be responsible and productive citizens and to be life-long learners with a goal for the 
future of being college and career ready.