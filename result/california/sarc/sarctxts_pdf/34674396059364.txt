Will C. Wood – Our Vision Statement: Students are actively preparing to become academically and 
socially capable of meeting the challenges of high school and the 21st century. Our Mission: High 
School Readiness for Every Student. 
 
At Wood Middle School, students are grouped into classrooms based on current placement, 
teacher recommendations, and common assessment test results, and data from the district's SQII. 
This allows instruction and planning to occur at both a broader range and depth of the academic 
content. Academic discourse and high quality tasks are also academic goals for instruction. 
 
Reading continues to be a primary focus of our school. Every Wood classroom has an array of 
reading books for their classroom libraries. In addition, we provide a “little library” that remains 
open to students throughout the day. We use the Reading Counts Computer program to test 
students’ comprehension on the books they’ve read and to keep track on the number of books and 
words they have read during the school year. Our goal is to have every Wood student read over 
250,000 words and as a school to read over 15,000 books. Students that read 1,000,000 words or 
more during the school year are rewarded with an end of the year pool party and they and their 
families are invited to the Wood Literacy Night Celebration held in May. 
 
Our students are provided an intensive academic program by Highly Qualified Teachers (HQT) staff 
as determined by the State of California. They are trained in providing Direct Instruction following 
the High Quality First Instruction teaching model provided by SCOE. We have an active student 
council that focuses on building leadership skills, school pride, SEL, PBIS, and Restorative Practices. 
Will C Wood also has the GEAR UP program that provides our students with additional interventions 
and support. 
 
The use of technology is emphasized throughout our curriculum. We have two fully equipped 
computer labs that allow students to have easy access to computers. We also have six laptop carts, 
five IPad carts, and four IPod carts that are available for teachers to use to compliment their 
instruction. The new student information system, Infinite Campus, is also a valuable tool to engage 
students and parents. 
 
Our students and teachers continue to address the instructional shifts that are crucial for learning 
and teaching the new common core standards. 
 
We encourage you to come to our school for a visit and be active participants in your child’s 
education. 
 

 

 

----

-- 

----

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Chad Sweitzer 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for Will C. Wood Middle School 

Page 1 of 9