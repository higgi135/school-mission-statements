Two Bunch Palms Elementary School is located in Desert Hot Springs, a community located within 
Riverside County, California. Two Bunch Palms Elementary is part of the Palm Springs Unified School 
District, which is comprised of sixteen elementary schools, five middle schools, one alternative 
education academy, four comprehensive high schools, and a continuation high school. Two Bunch 
Palms opened in August of 1998. The enrollment is approximately 820 students spanning 
transitional kindergarten through grade five. 
 
It is the mission of Two Bunch Palms Elementary School that every member of the community 
shows respect for self, others, and the learning environment. It is the shared commitment of the 
staff, parents and students of Two Bunch Palms Elementary to provide an atmosphere of loving 
acceptance and mutual respect which meets the diverse needs of our school population. We will 
support each other in maintaining the climate of high expectations necessary for student 
achievement. Our entire school community strives for all children to realize their maximum 
academic and social potential by providing activities that promote not only critical thinking and 
decision-making skills, but also a life-long love of learning. Efforts to increase student achievement 
include implementing an effective Response to Intervention plan including research based 
programs. Within this plan student assessment data is frequently analyzed. Planning and pacing 
support student learning. Additional teacher professional development opportunities are provided. 
Two Bunch Palms Elementary School evaluates the effectiveness of our School Plan for Student 
Achievement (SPSA) following the release of annual state and federal assessment scores each year. 
Our School Site Council (SSC), English Learner Advisory Committee (ELAC) and other leadership 
groups have the opportunity to review all student achievement data. Revisions to our SPSA, and 
subsequent School Board approval, will occur if there are substantial budget and/or material 
changes during the school year. 
 
 

2017-18 School Accountability Report Card for Two Bunch Palms Elementary 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Two Bunch Palms Elementary 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

40 

38 

38 

0 

0 

1 

0 

0 

0 

Palm Springs Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1038 

18 

34 

Teacher Misassignments and Vacant Teacher Positions at this School 

Two Bunch Palms Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.