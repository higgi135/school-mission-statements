Fremont School is one of the oldest elementary schools in the Delano Union School District with the current campus open since 1951. 
The campus is located just west of Highway 99 in Delano, CA. Fremont School serves approximately 528 TK-5 students, as well as two 
preschool severely handicapped classes and a full day general education preschool class. Our population is 53% English Language 
Learners, 3% Gifted and Talented, 6% special education, 8% migrant, and 91% Hispanic. 
 
Vision Statement: 
Fremont School children will successfully learn grade-level standards daily. 
 
Mission Statement: 
The professional learning community at Fremont School will work diligently to maximize the learning and academic achievement of 
every child. 
 
ELA Goal: 
By the end of the 2018-2019 school year, we will increase the percentage of Fremont pupils who read at grade level in grades K-2 by 
5%, as measured by the Emergent Literacy Battery (ELB) for Kindergarten and Analytical Reading Inventory for grades 1 & 2. 
 
Grades 3-5 will decrease the percentage of students who are below standard in Reading on the Smarter Balanced Assessment 
Consortium (SBAC) standardized assessment by 5%. 
 
Math Goal: 
By the end of the 2018-2019 school year, 60% of our Fremont pupils in grades K-2 will score an average of 70% or better on the grade 
level specific math summative assessments, benchmarks 
 
Grades 3-5 will decrease the percentage of students who are below standard on Math Claim 1, concepts and procedures, on the 
Smarter Balanced Assessment Consortium (SBAC) standardized assessment by 5%. 
 
ELD Goal: 
Our goal for the 2018-2019 school year is for all ELLs to increase an average of one level as measured by the English Language 
Proficiency Assessment for California (ELPAC). 
 
Fremont School provides opportunities for Tier I and Tier II intervention for all students during the regular school day. Struggling 
students are offered after school intervention as well. 
 
 
We serve 84 students in the after-school ASES program where students receive 60 minutes of targeted intervention in ELA and math, 
30 minutes of additional physical education, and 45 minutes of STEAM-focused enrichment. 
 

2017-18 School Accountability Report Card for Fremont Elementary School 

Page 2 of 11