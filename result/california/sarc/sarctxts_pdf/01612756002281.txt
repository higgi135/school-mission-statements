Beach School first opened its doors to students in 1912 in Piedmont, a city of about 11,000 
residents, nestled in the hills above the San Francisco Bay Area. The educational programs at the 
school are tailored to be both relevant and challenging. Beach School is proud of its comprehensive 
supplemental instruction, including both vocal and instrumental music, computer science, art and 
science enrichment, and a full physical education program. Beach School collectively ensures the 
learning and well-being of every student. 
 

Piedmont City Unified School 

 

District 

760 Magnolia Ave. 
Piedmont, CA 94611 

(510) 594-2600 

http://www.piedmont.k12.ca.us/ 

 

District Governing Board 

Amal Smith 

Cory Smegal 

Sarah Pearson 

Megan Pillsbury 

Andrea Swenson 

 

District Administration 

Randall Booker 
Superintendent 

Cheryl Wozniak 

Asst Superintendent, Ed Services 

Ruth Alahydoian 

Chief Financial Officer 

Stephanie Griffin 

Director of Instructional 

Technology 

Hillary Crissinger 

Director of Special Education 

Pete Palmer 

Director of Facilities 

 

2017-18 School Accountability Report Card for Beach Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Beach Elementary School 

With Full Credential 

Without Full Credential 

16-17 17-18 18-19 

29 

0 

29 

0 

22 

1 

0 

Teaching Outside Subject Area of Competence 

2.55 

2.88 

Piedmont City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

172 

6 

1 

Teacher Misassignments and Vacant Teacher Positions at this School 

Beach Elementary School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.