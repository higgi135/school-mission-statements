MISSION STATEMENT 
 
The mission of Lang Ranch Elementary School is to maximize student potential by working collectively to educate the whole child. The 
expected achievement of our “life-long learners” includes the ability to read, comprehend, compute, problem solve, organize, 
communicate, inquire, create, design, and research. In addition, the student's moral character and cultural/social needs are addressed 
in a safe and nurturing environment that promotes self-reliance and a growth mindset. 
 
SCHOOL DESCRIPTION 
Lang Ranch Elementary is a 60,000 square foot, single story, indoor hallway structure that opened in 1998. Lang Ranch Elementary is 
located in the heart of our neighborhood and has a current enrollment of 667 students in grades K-5. 
 
Lang Ranch prides itself on meeting the needs of every learner. The school has specialized support programs and models to meet 
diverse student needs. The GATE (Gifted and Talented Education) program at Lang Ranch has robust staff and parent support that 
allows for many activities. These include special programs as well as in-class differentiation through the cluster-grouping model. The 
school strengthens exceptional learners with an MTSS (Multi-Tiered System of Support) model that allows teachers to meet the 
individual needs of students by building increasing support as the level of need increases. The school also provides a number of 
intervention and extended learning opportunities for students in need of assistance. In addition, the school’s Learning Center assists 
students qualifying for special education services. 
 
Lang Ranch prides itself in educating the whole child. The school does an outstanding job of providing a solid curricular foundation in 
all the content areas, with a distinct focus upon Science and Social Science. Additionally, the school maintains a character education 
program, a visual and performing arts program, a physical education program, a character education program and a music program 
(band, strings and chorus) that provide a solid foundation to our students in a well-rounded manner. 
 
Lang Ranch is moving into the future with a focus upon the 21st Century Skills known as “The 4 C’s” – communication, collaboration, 
critical thinking, and creativity. Students are prepared for the world of the future with an emphasis on these skills via digital and non-
digital tools. All primary classrooms have six computers, while upper grade classrooms have nine workstations. The school has two 
computer labs with workstations for up to 36 students. All classrooms have an LCD projector and document camera for presentation 
purposes, as well as interactive whiteboards in more than 75% of the school. In 2012, the school began the use of iPads as a teaching 
and learning tool with the purchase of a bank of iPads for student use in each classroom and laptops for students in grade 5. We have 
also purchased 5-10 iPads for each classroom to serve as an additional creation and research tool. Currently, there are three “pilot” 
classrooms implementing a BYOD (Bring Your Own Device) environment where students are able to bring their own mobile devices to 
school to use as a learning tool. All the media content from the school library (audio and video) is digitized and available for teachers 
on demand in the classroom. The entire campus has new wireless access. 
 
Lang Ranch is also pleased to be the home of SHINE, the district’s elementary Home and Independent Study Program. Students and 
families access SHINE on the Lang Ranch campus weekly and participate in an individually designed program for students and families 
to be educated at home based on state and district guidelines and standards. 
 
With all of this in place, Lang Ranch takes great pride in supplying a program that allows every child to reach their personal best 
through differentiation, support, and quality instruction. 
 
 

2017-18 School Accountability Report Card for Lang Ranch Elementary School 

Page 2 of 12