Healdsburg Junior High is the only junior high in the Healdsburg School District. Enrollment for the 
2018-19 school year is approximately 350 students in grades six, seven and eight. Students from 
HJH normally matriculate to Healdsburg High School. Students from four elementary schools 
representing three districts promote into HJH (Healdsburg Elementary, Healdsburg Charter, West 
Side School, and Alexander Valley School). 
 
Our school staff includes 20 credentialed teachers, one library technician, one principal, one Dean 
of Students, one bilingual office assistant, one credentialed counselor, one part-time speech and 
language therapist, one part-time nurse and 8 other classified support personnel. Healdsburg 
Junior High is supported by an active Parent Teacher Organization (PTO), Governance Council, and 
the English Language Advisory Committee (ELAC). Our school has a seven period block schedule 
with two days of extended block instructional periods. Students take a core program of seven 
classes including English, History, Math, Science, Physical Education and two periods of intervention 
or enrichment/elective classes. We offer intervention classes in all grade levels of math and within 
a grade level combined reading course. ELL students participating in our English Language 
Development Program take one or two sections of ELD instruction depending on their EL level. 
 
HJH students can access a safe, enriching after school experience through the City of Healdsburg's 
"Club H" located on campus and sponsored by the California Department of Education After School 
Education and Safety (ASES) program. HJH students also attend Boys and Girls Club after school. 
 
Learn more about the school at our website at http://hjhs-healdsburgusd-ca.schoolloop.com 
 
Mission Statement 
 
HJH is committed to create and maintain a safe and supportive school environment where all 
members of the school community show respect for each other. We are committed to maintaining 
high levels of academic achievement for all students based on and measured by state standards. 
Through multiple measures, a comprehensive system of support will be provided to meet the needs 
of all students. 
 
Current Focus 
 
HJH is focusing on student behavior expectations and supports.. Towards that end, the staff is 
committed to building a "team" atmosphere throughout the school. The first two days of the school 
year and then subsequent time allocation, are dedicated to building school spirit through 
collaborative activities involving all HJHS students and staff. Our goal is to reinforce school-wide 
expectations, team building, and collaboration. We continue to focus upon intervention for our ELL 
students and for all students in the area of Math. Increasing the academic rigor of our program and 
preparing our students for college and career in the 21st Century, remains a daily focus. 
 

2017-18 School Accountability Report Card for Healdsburg Junior High School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Healdsburg Junior High School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

25 

23 

19 

0 

0 

0 

0 

1 

0 

Healdsburg Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

92 

2 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Healdsburg Junior High School 

16-17 

17-18 

18-19