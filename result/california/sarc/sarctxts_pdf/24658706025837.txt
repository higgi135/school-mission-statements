Crookham Elementary School is 50 years old and in good condition. Our campus facilities, such as bathrooms, cafeteria, library, and 
the computer lab, are all used continuously. We have 16 permanent classrooms and 13 portables. Part of our ongoing maintenance is 
replacement of carpet, whiteboards, and vertical blinds in our classrooms. We are proud of the students and staff at Crookham 
Elementary School and believe that they deserve a clean, safe, and attractive surroundings. A safety inspection is done monthly by 
the principal and maintenance supervisor. The cleanliness of the grounds and restrooms is outstanding and we have a person who 
checks and performs any needed cleanup during the time students are in school. The maintenance staff performs daily preventive 
maintenance. There are custodians on site during the afternoons and evenings. The library has an excess of 46 books per student. The 
computer lab has 36 stations with Study Island and Front Row software for reading and math as well as Eggs for the younger students. 
These workstations are available for every student to work at his or her success level and at his or her rate. The students use the 
computer lab in the after school program during expanded learning. In addition, we have a 1 to 1 chromebook to student ratio at our 
school. There is Internet access available in the library, computer lab, and every classroom. Classrooms have two to four student 
computers in the grade level. 
 
A preschool was added to our campus in 2004-05. It is a welcome addition to our educational program. The facilities were upgraded 
to meet State preschool standards. 
 
At Crookham our teachers will equip our students with the necessary 21st Century skills in order to compete in a global economy. 
Students need to become proficient Communicators, Creators, Critical thinkers, and Collaborators. Our literacy and math programs 
effectively provide our students with the necessary rigor to access the Common Core standards and acquire 21st Century skills.