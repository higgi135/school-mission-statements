Welcome to Ardenwood Elementary School. We are proud to be home of the Dragons. Ardenwood 
Elementary is the proud home of the Dragons. This School Accountability Report Card shares with 
the community our efforts to meet our district's mission of ensuring that all students are college 
and career ready and develop essential skills to succeed in life. Ardenwood Elementary is an 
excellent school and was honored to be acknowledged as a California Distinguished School in 2010, 
2014 and again in 2018. Our school cultural norms are to have every child feel valued, connected 
and recognized even though our large student body is composed of 966 transitional kindergarten 
through sixth grade pupils. 
 
We are an exceptionally high-achieving school with a diverse student body. Ardenwood 
Elementary's results for the 2018 California Assessment of Student Performance and Progress 
(CAASPP) were excellent, especially in comparison to all California districts. Approximately eighty-
five percent of our students met or exceeded grade level standards on both English Language Arts 
and Mathematics tests. This performance mirrors the success that our students have demonstrated 
with continued high scores since Common Core State Standards aligned assessments became 
available to California public schools through the Smarter Balanced Assessment Consortium (SBAC). 
 
Our student body includes pupils from around the world; with over 35 languages spoken by the 
families in our learning community. We believe that every student has the right to learn in a warm, 
caring, safe, and productive environment. All students receive specific character education lessons 
through Project Heart, Head, Hands (H3) and our school-wide focus on being Safe, Responsible, and 
Respectful This program reduces negative interactions between students and increases class time 
available for instruction. All students, teachers, and staff sign a school-wide pledge to follow the 
H3 Eight Great Traits of caring, honesty, respect, integrity, planning and decision making, 
responsibility, citizenship, and problem solving. Ardenwood Elementary School views discipline as 
explicit instruction that corrects or perfects behavior, and it is our belief that clearly stated 
expectations by adults sustains a positive school climate. In response to the our community's 
growing enrollment, in 2009 a two-story building with 6 modern classrooms was constructed on 
our campus along with an accompanying single-story building with additional classrooms and office 
space. The district installed new playground equipment on both the kindergarten and big 
playgrounds in 2016, and our school community and students continue to be delighted with its use. 
 
VISION STATEMENT: 
To promote a school climate where all students are provided the maximum opportunity to learn 
and thrive while achieving academic success and building respect for diversity and community. 
 
MISSION STATEMENT: 
To implement the goals of our district and meet the needs of our learning community by providing: 

• A safe, nurturing, and supportive learning environment, 
• 
• 
• 

Effective communication to build a partnership with parents, 
Each student with strategies to be a responsible, productive and well-adjusted citizen, 
Instructional strategies to prepare our students for college and/or career readiness 

2017-18 School Accountability Report Card for ARDENWOOD ELEMENTARY SCHOOL 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

43 

42 

43 

0 

0 

0 

0 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1713 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

0 

0 

0 

0 

0 

0 

0 

0 

Vacant Teacher Positions 
Notes: 
1) “Misassignments” refers to the number of positions filled by teachers who lack 
legal authorization to teach that grade level, subject area, student group, etc. 
 
2) Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners. 
 

0