We at Excelsior Middle School (EMS) are continually working to improve students' academic 
achievement and social experience at our school. To accomplish this, we need the help and 
participation of all our stakeholders. We have instituted new programs, procedures, and 
opportunities at our school to improve how we do business, resulting in higher levels of learning 
and a safer campus for all. We encourage our families to become active participants in educating 
students, offering input as to how EMS and BUSD can better meet the needs of our students. 
 
At Excelsior Middle School our students practice PRIDE: 
 
Patience 
Respect 
Integrity 
Determination 
Excellence 
 
We value the talents, backgrounds, and unique qualities of all members in our learning community. 
We recognize the importance of social justice and equity. We inspire academic excellence and the 
development of strong character. We build supportive relationships that encourage individual 
growth and personal responsibility. We engage all students to become responsible and contributing 
members of society. 
 

2017-18 School Accountability Report Card for Excelsior Middle School 

Page 1 of 8