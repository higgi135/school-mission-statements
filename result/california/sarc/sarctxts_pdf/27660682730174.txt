The city of Greenfield covers an area of 1.7 square miles. It is located in the heart of California’s Salinas Valley, approximately halfway 
between Salinas and Paso Robles. The City of Greenfield is located between the Gabilan mountain range to the east and the Santa 
Lucia mountain range to the west. The city of Greenfield lies within one of the most productive agricultural areas in the world. The 
area is known as the “Salad Bowl of the World.” Over $2 billion worth of fruit and vegetables are produced here annually and shipped 
across the United States and abroad. The area is also known as a premier wine grape growing region due to the rich soil and desirable 
climate. Greenfield is also the fastest growing community in Monterey County; the town is expecting over 350 new homes in the next 
three to five years. Students at Greenfield High School are provided with educational and personal experiences that enable them to 
become life-long learners and responsible, productive citizens. 
 
Greenfield High School Mission Statement Greenfield High School provides all students with a standards-based curriculum, educational 
programs, resources and opportunties which empower all students to achieve academic success and reach their fullest potential. 
Greenfield High School-wide Learner Outcomes(SLO's) 
 
Bruin Pride 
 
P Be Prepared 
 
R Be Responsible 
 
I Have Integrity 
 
D Be Determined 
 
E Be Engaged