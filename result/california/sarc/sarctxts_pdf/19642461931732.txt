Desert Winds High School is a continuation high school in the Antelope Valley that is designed to serve the needs of a diverse student 
population in an alternative education environment. It is located in the northern portion of Los Angeles County and was established 
in 1975 by the Antelope Valley Union High School District in order to accommodate the needs of students attending three 
comprehensive high schools. Since Desert Winds was opened in 1975 the district has expanded from its original three comprehensive 
sites to eight and Desert Winds has expanded from one session to two sessions in order to meet the needs of those students who can 
benefit from the smaller class sizes and accelerated credit accrual program offered at an alternative education school such as Desert 
Winds. 
 
Desert Winds High School provides an alternative program for young people to continue their high school education. An emphasis is 
placed on the positive aspect of each student's ability as they are assisted to develop skills, knowledge, and attributes that will enable 
them to be successful in their future endeavors, be it in college or their chosen career field. The academic program at Desert Winds 
features a multifaceted curriculum that is focused on providing instruction in academic course work that is based upon California State 
Common Core Standards.The curriculum offered at Desert Winds is presented to students through a variety of instructional strategies. 
Students are regularly assessed at Desert Winds in order to monitor their progress toward their academic goals. While there are a 
variety of reasons under which students attend Desert Winds High School the primary goal is the same for every student: provide each 
of our students with the requisite skill set to become responsible and productive citizens within a diverse society. The staff at Desert 
Winds High School constantly strives to develop, in each of its students, a positive self-image and a keen awareness of their unique 
attributes as human beings and how those individual attributes will allow for our students to be life-long learners. 
 
Additional information may be obtained through the web site www.dwhs.org.