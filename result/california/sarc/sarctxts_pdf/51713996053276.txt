In the 2017-18 school year Live Oak Middle School remains committed to to academic achievement 
and the development of good citizenship in students. We continue to implement a data based 
approach in making key decisions to address the needs of our students. Academics combined with 
character development serves as the foundation of our program. We are pleased to offer a well-
rounded program primarily focusing in on reading, writing, and math. LOMS has been selected an 
AVID site of Distinction for its level of AVID implementation school wide, and also been recognized 
as a School to Watch at the state and national level. At Live Oak Middle School, students are able 
to focus on the core academics and receive additional support throughout the day in intensive 
intervention classes. In addition to core subject matter and academic support, our school offers 
character building opportunities: after school sports; after school Drama Club; WEB; ASB; AVID 
Elective 6-8; School wide AVID strategies; music; Robotics; Computer Literacy, Study Skills, Current 
Events; VAPA electives. Our goal is to provide students with the necessary academics and life skills 
to become productive members of our society. 
 
Mission Statement 
The mission of Live Oak Middle School, Connecting with kids, creating a respectful, engaging 
environment, educating and empowering students to succeed today and tomorrow. 
 
Parm Virk, PRINCIPAL 
 

 

 

----

---- 

----

---

- 

Live Oak Unified School District 

2201 Pennington Road 

Live Oak, CA 95953 

(530) 695-5400 

https://www.lousd.k12.ca.us/Domain

/8 
 

District Governing Board 

Scott Davis 

Kathy L. Walker 

Talwinder Chetra 

Ernest J. Rodriguez 

Roger D. Christianson 

 

District Administration 

Mathew Gulbrandsen 

Superintendent 

Satjit Dhami 

Curriculum, Instruction & 
Assessments Coordinator 

Glenn Houston 

Special Education Coordinator 

Christopher Peters 

Chief Financial Officer 

 

2017-18 School Accountability Report Card for Live Oak Middle School 

Page 1 of 9