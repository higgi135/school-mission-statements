Vision: 
Blandford Elementary School expects and promotes the growth of all members of our school community. Our vision is to is to inspire 
and nurture the journey of life long learning for all. We work at building community and learning together in order to create the best 
school for all students, families, and staff.