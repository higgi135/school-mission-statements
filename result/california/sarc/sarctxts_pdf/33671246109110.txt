North Ridge Elementary School is a TK-5 magnet elementary school that was recognized in 2010 as 
a California Distinguished School for our outstanding efforts in increasing student achievement and 
in closing the achievement gap. North Ridge was also awarded the distinction of being an Honor 
Roll School by the California Business for Education Coalition. 
 
High expectations, a commitment to faithfully teaching the Common Core State Standards and 
exemplary parent support provide a strong base for our instructional program. Our parent groups; 
Booster Club, AAPAC, and ELAC assist in providing assemblies, field trips, and family activities that 
help connect students to their learning that goes beyond the classroom. 
 
Our Vision for North Ridge Elementary School is to work cooperatively as staff, students, parents, 
and community to provide a safe and positive environment; recognizing the uniqueness of all. Our 
commitment is to provide a challenging STEAM curriculum while encouraging continuous learning 
and developing responsible citizens for the 21st Century through career and college readiness. 
 
 
 
 

 

 

----

---- 

----
---- 
Moreno Valley Unified School 

District 

25634 Alessandro Blvd 

Moreno Valley, CA 92553 

(951) 571-7500 
www.mvusd.net 

 

District Governing Board 

Susan Smith, President 

Jesus M. Holguin, Vice-President 

Cleveland Johnson, Clerk 

Gary E. Baugh. Ed.S., Member 

 

District Administration 

Martinrex Kedziora, Ed.D. 

Superintendent 

Maribel Mattox 

Chief Academic Officer, 

Educational Services 

Tina Daigneault 

Chief Business Official, Business 

Services 

Robert J. Verdi, Ed.D. 

Chief Human Resources Officer, 

Human Resources 

 
 

 

2017-18 School Accountability Report Card for North Ridge Elementary School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

North Ridge Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

27 

27 

31 

0 

0 

0 

0 

0 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

North Ridge Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.