Central Park Elementary is Santa Clara Unified's newest neighborhood school, opening in August of 2016. During the 2016-2017 school 
year, Central Park Elementary will serve 288 students in grades Kindergarten through Third Grade, with the goal of opening 4th and 
5th-grade classrooms in the coming years. Class size in grades K-3 is 24:1. Central Park is a public school, however since the school is 
new a number of open enrollment spots were available to Santa Clara Unified students. Therefore, approximately one-third of the 
students are neighborhood students and two-thirds are attending on open-enrollment status. 
 
Central Park Elementary is committed to educating the Whole Child and providing a state-of-the-art STEAM education for students of 
all races, ethnicities, gender and socioeconomic status. We teach the state standards using the following philosophies and structures: 
1. STEAM 
2. Project Based Learning 
3. Personalized Learning 
4. Blended Learning 
5. Active, Flexible Learning Environments 
6. Maker Space 
7. Social Emotional Development 
8. Developing a Growth Mindset and qualities of compassion, critical thinking, collaboration, creativity and communication 
 
FACULTY & STAFF: 
Central Park's staff consists of: 12 classroom teachers, a library media assistant, an on-site counselor two days per week, a psychologist 
one day per week, a Wellness Coordinator 2 days per week, a speech and language therapist one day per week a resource specialist 2 
days per week, a part-time ELSAT (English Language Technician) and a STEAM/Project Based Learning Coach (2)and 1/2 days per week. 
 
MISSION 
Our Mission is to serve a racially and ethnically diverse community of students, parents and staff members, dedicated to creating a 
learning environment of joy in which each individual is empowered to reach his or her academic, emotional and physical potential. 
Our purpose is to nurture the development of the whole child who is then inspired to continually expand all aspects of his/her being 
(mind, body, heart) and is empowered to improve the world for others. We build habits of mind and ways of being that are caring, 
curious, reflective and visionary. 
 
VISION 
Our vision is that Central Park Elementary School is a place in which each individual feels deeply cared about, known, and honored as 
a unique human being within the larger community. It is a place where all of us are inspired to grow every day, excited to tackle new 
challenges together and validated by having one’s individual talents and passions nurtured. 
 
 

2017-18 School Accountability Report Card for Central Park Elementary School 

Page 2 of 11