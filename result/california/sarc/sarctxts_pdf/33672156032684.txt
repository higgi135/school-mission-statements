Liberty exists to nurture and facilitate innovators' individual passion for learning. Through voice, choice, pace and path, learners will 
contribute to mankind's digitally connected world.