Carmen Dragon Elementary School is a beautiful facility located in southeast Antioch between Hillcrest and Vista Grande Drive. Our 
school is currently home to approximately 500 students servicing TK-6th graders. We are extremely proud of the teaching and support 
staff at Carmen Dragon and are committed to providing them with as many tools as possible to prepare our children for the future. 
 
The mission of Carmen Dragon Elementary School, in partnership with our parents and community members, is to empower students, 
teachers, and parents to be lifelong learners and achievers in an ever changing diverse world.