MISSION AND VISION STATEMENT 
 
With a focus on the individual student, we aim to challenge and enrich every child's education by facilitating both academic and 
affective learning opportunities to build a foundation for critical thinking, achievement, innovation, and success. Through the three 
E's of ENJOYMENT, ENTHUSIASM, and ENGAGEMENT, all students can learn at high levels. 
 
At Acacia Magnet School for Enriched Learning, we believe in the strengths, passions, talents, interests, and learning styles of our 
students. We are a Schoolwide Enrichment Model (SEM) school, which means that we offer a strength-based approach to high-level, 
student-driven, authentic learning that students experience through enriched learning opportunities in the classroom curriculum as 
well as through our Genius Hour Enrichment Clusters. We strive to provide a positive learning community in a safe and supportive 
school where all of us can be enthusiastic and successful 21st Century learners who embrace the four C’s of Communication, 
Collaboration, Creativity, and Critical Thinking. Our approach encourages students to discover their strengths and interests, pursue 
their passions, and work toward personal learning goals both at school and at home. Students, parents, staff and community at Acacia 
share this vision that serves as a basis for the development of our school goals, curriculum and educational program. 
 
SCHOOL DESCRIPTION 
 
Acacia Magnet School is a Schoolwide Enrichment Model (SEM) school with approximately 370 students. We are a diverse campus 
with several subgroups that include about 30% English Learners as well as students with special needs. We house three CVUSD 
Specialized Academic Instruction (SAI) Program classes for students with both cognitive and speech/language delays. Acacia mirrors 
real-world diversity that gives students opportunities to celebrate individual strengths, to collaborate with peers, and to learn from 
one another. 
 
At Acacia, we create joyful learning experiences that enhance overall achievement and self-confidence for all students. Best practices 
from Gifted and Talent Education are the foundation for SEM, a researched-based approach to high-level learning and talent 
development for ALL learners. The students’ SEM experience includes grade level curriculum infused with three types of enrichment, 
Enrichment Clusters, and designated grade level differentiation time called Excel Time. All of our classroom teachers have attended a 
summer weeklong conference at the University of Connecticut as a means to professional development and implementation of this 
personalized approach to education. The purposeful selection of this model supports the needs of every Acacia learner through a 
unique path that stems from our district’s Local Control Accountability Plan (LCAP) goals and our own School Plan for Student 
Achievement (SPSA) goals.