Sheppard Middle School is a College and Career Preparation Middle School. While maintaining its long standing traditions, Sheppard 
has transformed to meet the needs of community and proudly offers two smaller Academies: AVID and Atlas with-in Sheppard. Both 
Academies have distinct approaches to teaching and learning. The AVID approach focuses on the acceleration of student performance 
through practices of consistent application of learning tools and strategies that are research based and proven. The ATLAS approach 
focuses on student learning through thematic units and project based service learning. Both academies participate in content based 
field trips and host a variety of evening community events. 
 
VISION: Sheppard Middle School (Atlas Academy, AVID Academy, Citizen Schools and Community) works collaboratively to ensure that 
all students aspire and achieve at optimal levels:empowering them for success in High School. 
 
MISSION: 1. To engage students in 21st century, innovative learning experience, and meaningful work, through diverse 
approaches:Atlas and AVID Academies 2. To engage the entire community in collaboration to assure academic achievement of all 
students. 3. To empower, value, and respect all stakeholders in a professional learning environment that builds pride, self efficacy, 
and determination. 
 
Please visit our school, explore our website, or call us at (408) 928-8800 to learn more about our school and the spirit and pride of the 
Sheppard Trojans!