Attached is the School Accountability Report Card that was established by Proposition 98, an initiative passed by California voters. As 
you read the Report Card, you will gain a better understanding of College Heights; a school with a record for improvement, a faculty 
that is professionally skilled and personally committed to meeting the learning needs of students and a student body that is 
enthusiastic and motivated to perform well. 
 
Our students, staff, parents, and community work tirelessly to uphold our College Heights mission, vision, motto, and goals. 
 
Our Motto: "PAWS up! With Positive Attitudes We Succeed." 
 
College Heights Mission: College Heights Elementary School is a secure learning community where students are equipped to 
strengthen their individual talents as they persevere through challenges. Collaboration, rigorous instruction, intervention and 
enrichment collectively contribute to prepare college and career ready Cougars! 
 
College Heights Vision: College Heights Elementary School empowers students to become responsible, gracious and mindful leaders 
who are successful 21st century global community citizens. 
 
College Heights Goals: 1) Academic Achievement 2) Safe and Positive Learning Environment 3) Parent and Community Involvement