Oak Knoll is a big school of over 650 students that feels small, personal and focused on its mission: 
Every student an exemplary scholar, a valued friend, and a courageous citizen. As you walk onto 
our campus, you will notice the focus is on the whole child. Learning is not limited to our classrooms 
but is accessible everywhere on campus. You will see children of various ages working together and 
learning from each other. You will see students who love to read, as evidenced by the large number 
of students who stroll around book in hand. In the halls, you will hear children as young as five 
playing violin, or classes of kids engaged in hands-on science, or small groups of students discussing 
their reading. If you glance into the open doors of our classrooms, you will observe students using 
technology as a learning tool, students engaged in dynamic lessons, and individual students sitting 
with their teachers receiving targeted instruction. You might also notice signs on our classroom 
doors welcoming teachers to observe teachers! You will notice students taking the initiative in 
tending our school garden, planting seeds, and caring for the chickens. You will see students 
running the broadcast studio that produces the KNOL morning news and will watch the student 
tech crew doing sound checks behind the scenes at school-wide assemblies and performances. At 
recess, you will find children self-directed and participating in various games, sports and other 
activities, such as visiting our bustling school library. During this time, you might also notice groups 
of kids in the Gaga (Israeli dodge ball) pit, which students advocated for, designed according to 
geometric principles, and then built during math classes. If you venture further into our playground 
you will begin to hear the outdoor instruments, the large "big kid" sandbox (because big kids like 
to dig in the sand too), and the far off place called the Nature Zone where students work together 
using nature to build structures. While all of this is going on, over on our kindergarten playground 
our youngest students are riding trikes and building with our Imagination Playground (oversized 
foam blocks). 
 
At Oak Knoll, our mission is our blueprint and keeps us focused on what we value. Exemplary 
scholars have a growth mindset so we make sure to teach Dr. Carol Dweck’s “Mindset” research to 
our students and parents. They understand that intelligence is not an asset determined at birth and 
that the brain is pliable and capable of amazing accomplishments with practice and perseverance. 
Smart is not something you are; smart is something you get through hard work and practice. 
Developing exemplary scholars may be impossible without focusing on the whole child. We also 
emphasize students becoming valued friends and courageous citizens as part of our school mission. 
All three parts of our mission are equally important to us. We empower our students by providing 
many leadership opportunities in and out of the classroom. We prioritize and hold a high 
expectation for compassion. Our fervent belief in inclusion for students with specific needs has 
helped create an appreciation and celebration of unique differences. We value diversity and 
celebrate race, language, and culture on our campus. Oak Knoll has many students from around 
the globe. Valued Friends are compassionate students so we make sure to teach our students to 
empathize with others and embrace diversity. Courageous Citizens provide service to others and at 
Oak Knoll service is not a special project, but instead, something Oak Knoll students regularly 
provide. 
 
Oak Knoll believes in the power of relationships and connection. Teachers provide targeted 
instruction to students in the classroom who have not yet reached proficiency. This instruction is 
designed around academic goals and is also used as a critical time to deepen relationships. Speaking 
of relationships, our school staff also has strong working relationships which result in a lot of 
collaboration and school spirit. At Oak Knoll, we expect a lot of our teachers and put them on a 
pedestal because we appreciate and value the work they do to make Oak Knoll exceptional. Like 
our students, our teachers, are amazing! 

2017-18 School Accountability Report Card for Oak Knoll Elementary 

Page 1 of 11 

 

Oak Knoll is fortunate to have a close partnership between the staff and parents. Parent volunteers spend countless hours supporting 
classroom teachers, programs, and events. Oak Knoll is a neighborhood school like no other! As we take our students on this educational 
journey, we emphasize strategies that help them develop a deep sense of self-awareness, self-advocacy, and self-confidence. 
 
The Whole Teacher Framework: 
Continuous improvement is a core value at Oak Knoll. In order to improve, our teachers and staff must understand the expectations, 
engage in self-reflection and set individual goals. We have created a framework that focuses on the most important characteristics for a 
teacher. This framework is called The Whole Teacher Framework. Just as we focus on all the elements of the "whole child" at our school 
to ensure student connectedness and success, we also want to have a clear vision on what elements we believe contribute to teacher 
success at our school. The Whole Teacher Framework is made up of four equal components: teaching, connection, collaboration, and 
communication. All teachers at Oak Knoll are aware of these components and have spent time discussing, self-reflecting and goal setting 
around each component. Throughout the 2017-18 school year, Oak Knoll principals will meet with teachers one-on-one to check in and 
continue the conversation. Resources will be allocated based on teacher goals to ensure progress and development across our staff. The 
Whole Teacher framework is another import aspect of our overall vision. 
 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Oak Knoll Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

42 

45 

40 

0 

0 

0 

0 

0 

0 

Menlo Park City Elementary 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

204 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Oak Knoll Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.