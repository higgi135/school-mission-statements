Greenfield Middle School is one of three middle schools in Greenfield Union School District. It is 
located near the southern outskirts of the city limits and receives students from three feeder 
elementary schools, Planz, Palla, and Plantation schools. Enrollment for 2018-2019 consists of 944 
students made up of grades 6 - 8. Greenfield Middle School is a Title 1 School-wide Program. Our 
mission is to create a learning environment of high expectations and academic excellence for all 
students within a safe environment. Additionally, it is our mission to help students develop an 
attitude of personal responsibility, capability, self-worth, and self-discipline that will instill in them 
the character to become effective mature citizens. Students will acquire the knowledge and 
develop the skills necessary to assume their roles as effective family, school, and community 
members in a culturally diverse and technological society. Our vision is through collaboration, 
planning, accountability, and quality instruction, Greenfield Middle School as a professional 
learning community will ensure all students achieve academic excellence, exemplify character and 
become life-long learners. 
 
Our school motto is: “No Excuses, All Students Can Succeed.” 
 

---

- 

----

-

--- 

Greenfield Union School District 

1624 Fairview Rd. 

Bakersfield, CA 93307 

(661) 837-6000 
www.gfusd.net 

 

District Governing Board 

Mike Shaw 

Richard Saldana 

Kyle Wylie 

Dr. Ricardo Herrera 

Melinda Long 

 

District Administration 

Ramon Hendrix 
Superintendent 

Sarah Dawson 

Assistant Superintendent 

Curriculum 

 

Lucas Hogue 

Assistant Superintendent 

Personnel 

 

Rebecca Thomas/TBD 

Assistant Superintendent 

Business 

 

 

2017-18 School Accountability Report Card for Greenfield Middle School 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Greenfield Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

39 

4 

0 

31 

10 

3 

31 

14 

0 

Greenfield Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

399 

52 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Greenfield Middle School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.