Palo Verde Elementary School has approximately 400 students, kindergarten through fifth grade. The school neighborhood is bounded 
by Highway 101 on the east, Middlefield Road on the west, Oregon Expressway on the north, and Charleston Road on the south. 
Approximately 4% of our students come from other areas in Palo Alto and 5% from outside the district, including voluntary transfers 
from Ravenswood School District. 
 
The area encompasses a social, economic, cultural, linguistic, and ethnic diversity that is valued by the school community. Twenty-
four different languages are spoken in students' homes. (The most common languages other than English are Mandarin, Spanish, 
Korean and Hebrew.) 
 
The majority of Palo Verde parents are engaged in professional, technical or managerial employment, and less than half work in trades 
and services. Although the majority of families are homeowners, a sizable number of students live in rental properties, including 
apartments. 
 
Palo Verde has a positive and supportive learning environment with clear and consistent standards for behavior. Through the 
cooperative learning environment, the school helps students develop leadership responsibilities, problem solving, and teamwork skills 
necessary to be successful and productive members of the larger community. 
 
Class size for kindergarten is 19, first, second and third grades averages 23 students per room. Fourth and fifth grades average 24 
students per classroom. Average daily attendance exceeds 95%. 
 
Mission Statement: We believe that mastering academic skills, engaging in cooperative problem-solving, enjoying and participating in 
the arts, applying human relations skills, and increasing physical skills, strength, and endurance in a safe, supportive environment are 
vital to the full development of each child. Palo Verde staff, parents, and students work together to develop minds, build skills, enrich 
experiences, increase strength, solve problems, expand horizons, and encourage caring and responsible relationships.