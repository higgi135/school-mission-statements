Reedley Middle College High School (RMCHS) was established in 2012, with one classroom 
consisting of twenty 9th grade students. 
 
Currently, the school serves 204 students in grades ninth through twelve during the 2018-19 school 
year and included a staff of 4 full time and 6 part time teachers. Reedley Middle College High School 
teachers and staff are dedicated to ensuring the academic success of every student and providing 
a safe and productive learning experience. 
 
The mission of Reedley Middle College High School is to provide early access to college and the 
opportunity to assimilate to college early so that students can persist and complete their education 
in a post-secondary institution of their choice or begin their career. The purpose of our dual-
enrollment program is to assist all students in the successful completion of high school while 
allowing them to concurrently complete coursework that earns college credit towards the 
completion of an Associate’s Degree or to complete General Education requirements. 
 
Students attending RMCHS will be offered an individually designed, standards-based educational 
program providing the necessary foundation for success in college and/or career. The RMCHS 
educational plan consists of a broad range of goals and objectives to meet the unique educational 
needs of all students, specifically in the areas of Agricultural Business, Business Administration with 
an Entrepreneurship option, General Science, and General Education course of study. These goals 
and objectives are specified in the School’s adopted academic standards, which reflect the 
standards approved by the California State Board of Education. Use of the latest technology and 
Internet access will be made available to all students and staff to enhance the student’s educational 
experience. In addition to individually tailored courses of study at RMCHS, students can extend 
learning experiences and interests through access to appropriate community college courses, 
internships and service learning projects. 
 
The goal of RMCHS is to provide students with the foundation, to prepare RMCHS students for 
admission to, success in, and graduation from the college of their choice. Students will explore their 
capabilities and interests to fully develop their potential for success. The goal is to inspire and 
prepare students to be successful, self-motivated learners, workers, and citizens. 
 

 

 

----

---- 

----

---- 

Kings Canyon Unified School 

District, Reedley Middle College 

Charter High School 

1801 10th St 

Reedley, CA 93654 

559-305-7010 

www.kcusd.com 

 

District Governing Board 

Craig Cooper 

Robin Tyler 

Manuel Ferreira 

Noel Remick 

Sarah Rola 

Clotilda Mora 

Jim Mulligan III 

 

District Administration 

John Campbell 
Superintendent 

Roberto Gutierrez 

Deputy Superintendent, Human 

Resources 

Monica Benner 

Assistant Superintendent, 
Curriculum and Instruction 

Mary Ann Carousso 

Administrator, Student Services 

Jose Guzman 

Administrator, Educational 

Programs 

Adele Nikkel 

Chief Financial Officer 

 

2017-18 School Accountability Report Card for Reedley Middle College High 

Page 1 of 10 

 

RMCHS will provide students with: 

individualized, learner-centered instruction 
a standards-based rigorous and challenging curriculum 
college preparatory A-G approved coursework 

• 
• 
• 
• opportunities to be dually enrolled at Reedley College and earn college and high school credit 
• 
a comprehensive student education plan created by Reedley College counselors to meet student educational goal 
• 
supplemental instructional activities including field trips, service learning, and projects 
• 
access to college-level courses in addition to the core academic program 
• 
instructional practices based on current research 
• 
technology supported instruction 
• 
extra-curricular and co-curricular opportunities 
• 
an environment that cultivates individual responsibility 
• 
a safe and tolerant small school environment 
• parent and community program involvement 

It is the belief of RMCHS that everyone is a lifelong learner and that learning can occur in a variety of ways including teacher to student, 
student to teacher, student to student, and community member to student. Also, that all people, regardless of social or economic 
condition, must be provided the capacity to take charge of their lives. Through new models of teaching and learning, they will be enabled 
and empowered to make critical decisions for their futures.