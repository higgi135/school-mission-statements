Welcome to Walnut STEAM Academy. We continue to strive and develop students’ 21st Century 
skills along with their intellectual, emotional, physical, and social well-being. Walnut STEAM 
Academy’s focus is on engaging students with high academic rigor with an emphasis on STEAM. 
STEAM integrates the study of Science, Technology, Engineering, Arts, and Mathematics. Our 
students are thinking like engineers, mathematicians, artists, innovators, and technologists. They 
are involved in creating new materials or re-purposing current materials in the Makerspace Lab. 
They are learning about coding, programming, and videography in our Computer Lab. Schoolwide, 
we model and encourage excellent behavior utilizing Positive Behavior Interventions and Supports 
(PBIS) for all of our students. We are proud of our dedicated staff and the relationships we’ve 
cultivated with students and parents and will ensure that we work with our families and 
stakeholders to create a powerful STEAM community. 

----
La Habra City Elementary School 

- 

District 

500 North Walnut St. 
La Habra, CA 90631 

(562) 690-2305 

www.lahabraschools.org 

 

District Governing Board 

Cynthia Aguirre, President 

Sandi Baltes, Clerk/Vice-President 

John Dobson, Member 

Ida MacMurray, Member 

Adam Rogers, Member 

 

District Administration 

Dr. Joanne Culverhouse 

Superintendent 

Dr. Teresa Egan 

Associate Superintendent of 

Human Resources 

Dr. Sheryl Tecker 

Assistant Superintendent of 

Educational Services 

Dr. Cammie Nguyen 

Administrative Director, Special 
Education and Student Services 

Dr. Mario A. Carlos 

Director of Communications and 

Special Programs 

 

2017-18 School Accountability Report Card for Walnut Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Walnut Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

23 

21 

27 

0 

0 

0 

0 

0 

0 

La Habra City Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

0 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Walnut Elementary School 

16-17 

17-18 

18-19