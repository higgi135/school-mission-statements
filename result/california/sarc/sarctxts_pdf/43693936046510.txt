COMMUNITY: Located in the West portion of Santa Clara County, Campbell Union School District 
serves more than 7,600 students from the communities of Campbell, San Jose, Saratoga, Santa 
Clara, Monte Sereno and Los Gatos. 
 
School: Serving students from Campbell and San Jose, Blackford is home to 500 students from 
diverse backgrounds and various cultures. Our school hosts students from preschool to fifth grade, 
including our County OI (orthopedically impaired) program. We encourage parents to share in the 
decision-making process through involvement in our Blackford PTA, School Site Council, English 
Language Advisory Committee, and Project Cornerstone. 
At Blackford we work closely together to create a caring school climate that will benefit all of our 
children. Our expectations for anyone on campus is to be Respectful, Achieve to their fullest 
potential, and be Responsible. 
 
School Vision Statement 
Blackford creates lifelong learners who perform above grade level and contribute to a global 
society. 
 
School Mission Statement 
We uphold high expectations and empower all students to achieve academic success. 
 
 

 

 

----

---- 

----

--

-- 

Campbell Union School District 

155 N. Third Street 
Campbell CA, 95008 

(408) 364-4200 

www.campbellusd.org 

 

District Governing Board 

Pablo A. Beltran 

Danielle M.S. Cohen 

Chris Miller 

Richard H. Nguyen 

Michael L. Snyder 

 

District Administration 

Dr. Shelly Viramontez 

Superintendent 

Nelly Yang 

Assistant Superintendent, 
Administrative Services 

Lena Bundtzen 

Assistant Superintendent, Human 

Resources Services 

Whitney Holton 

Assistant Superintendent, 

Instructional Services 

 

2017-18 School Accountability Report Card for Blackford Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Blackford Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

23 

23 

21 

0 

0 

0 

0 

0 

0 

Campbell Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

305.6 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Blackford Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.