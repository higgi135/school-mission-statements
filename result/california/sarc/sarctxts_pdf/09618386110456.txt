Principal’s Message 
Silva Valley Elementary School is a true community school. Our dedicated staff, well-rounded 
students, and supportive parents personify our mission which is “to provide a safe, nurturing and 
challenging educational environment in which students are eager learners, realize their full 
potential, exhibit positive self-esteem, and are successful, productive citizens now and in the 
future.” It is our belief that students, staff, and the community are proud to be a part of the Silva 
Valley School team where diversity is celebrated and students learn together in a well supported, 
cohesive environment. At Silva Valley, it is also our belief that our commitment to develop the 
whole child through positive administrative leadership, Core Values, PBIS (Positive Behavioral and 
Supports System), family support, and community involvement, could not be met without a strong 
personal relationship with the community that has grown up around us. We are proud to be a 2014 
California Distinguished School. 
 
School Profile 
Silva Valley Elementary is one of six elementary schools, including a TK - 7 Charter Montessori 
school, and two middle schools in the Buckeye Union School District. The district was established 
in 1858 and currently serves the communities of Shingle Springs, Cameron Park and El Dorado Hills 
in El Dorado County. 
 
During the 2018-19 school year, 522 Transitional Kindergarten through fifth grade students were 
enrolled at the school, with classes arranged on a traditional schedule. 
 

2017-18 School Accountability Report Card for Silva Valley Elementary School 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Silva Valley Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

24 

23 

23 

0 

0 

0 

0 

1 

0 

Buckeye Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

219 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Silva Valley Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.