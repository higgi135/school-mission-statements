Blaker-Kinser Junior High School is located in the community of Ceres in the central San Joaquin Valley. The school has an enrollment 
of 718 7th and 8th-grade students. The ethnic composition of the student population is 86.35% Hispanic, 7.94% White, not of Hispanic 
origin, 3.34% Asian, 1.25 % African American, .56% American Indian, .27% Pacific Islander, .27% Other. English learners comprise 
49.16% of our student population and 85.62% of the students qualify for the National School Lunch Program. 
 
The original school campus was built in 1994 (Phase I) and the buildings have been well maintained. Phase II was completed in 1998 
with the addition of six classrooms, a music building, art building, technology building, home economics building, and gymnasium. 
Construction on six additional classrooms was completed in the late fall of 2006. The school is nicely landscaped and encompasses 23 
acres. The grounds include pickleball courts, ¼ mile track, football field, soccer fields, basketball courts, and two softball diamonds. 
 
The Blaker-Kinser staff includes 29 certificated teachers, a principal, an assistant principal, two learning directors, and an 
administrative assistant. All the teachers met the rigorous NCLB criteria to be highly qualified in their subject area. Specialized 
programs include music, art, technology, as well as four special education teachers. Support staff include one part-time nurse, one 
full time health clerk, one part-time school psychologist, one full time mental health clinician, one part-time behaviorist, an office 
manager, three full time secretaries, one part-time attendance clerk, one full time ASB clerk, one library media clerk, one full time 
campus supervisor, three campus supervision assistants, seven paraprofessionals, four custodians, one part-time SRO and six cafeteria 
employees. 
 
Parent involvement is highly encouraged by all staff members. We have an active School Site Council, English Learner Advisory 
Committee, and Family Engagement Committee, and Family Dinner Events. Parents are invited to attend all school events, including 
Back to School Night, parent/teacher conferences, sporting events, performances, and evening technology sessions. Blaker-Kinser has 
also enjoyed an increasing amount of community support as demonstrated by local businesses donating time and money to our 
programs. 
 
The school staff encourages parent participation on campus in an ongoing effort to make parents an integral part of the education of 
their children. Parents are welcome to visit classrooms and often volunteer to supervise school-sponsored events such as school 
dances and chaperoning field trips. Information regarding upcoming events and special activities is sent home regularly through the 
ParentSquare phone messaging system, Assistant Principal monthly Coffee Connect Meetings, personal phone calls, school website, 
parent dinners, parent portal, google calendar, and monthly newsletters. Regular communication between home and school is an 
important factor in student success. 
 
Blaker-Kinser Junior High School uses site-based teams, with the goal of increasing communication and collaboration among all 
stakeholders. Teams comprised of teachers, administrators, classified staff, parents and students address specific areas of concern to 
the school community. Feedback is then given to the Department Leader Committee, the School Site Council, and the English Learner 
Advisory Committee (ELAC), which provide input and report back to the school staff. 
 
Over the past few years, Blaker-Kinser teachers and administrators attended Professional Learning Communities conferences to learn 
more about creating professional learning communities and leading staff development. New teachers and staff will continue to attend 
PLC and RTI conferences to ensure consistent team practices. During the school year, planning time will continue to be used to 
implement Professional Learning Communities at Blaker-Kinser. 
 
As a Program Improvement school, we are working with our district to plan and implement an alternative governance plan to improve 
student achievement. 
 
 
 

2017-18 School Accountability Report Card for Blaker-Kinser Junior High School 

Page 2 of 11