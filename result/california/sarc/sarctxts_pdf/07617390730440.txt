Martinez, a small city of 38,000 residents, serves as the county seat for Contra Costa County. The Martinez community is a middle-
income suburb of the greater San Francisco Bay area with a rich local history. Many of the names associated with the community echo 
the city's proud past: Ygnacio Martinez, first settler of the area; Joe Di Maggio, Hall of Fame baseball player; John Muir, father of the 
National Park System, and, John Swett, father of California's public education system.Martinez has a rich history of community support 
as evidenced by several bond measures passed in the last 10 years. A well-established education foundation provides tens of 
thousands of dollars to teachers and programs designed to improve instruction. Local businesses and industries also provide fiscal and 
volunteer support for schools. 
 
Martinez Unified School District (MUSD) serves just over 4,100 students at four elementary schools, one junior high school, Alhambra 
High School (comprehensive high school), Vicente Martinez High School (continuation high school), Briones School (independent study 
school) and Martinez Adult Education (adult school). Some residences in the city limits are located in the Mt. Diablo Unified School 
District boundaries and some residents of the school district live in the surrounding unincorporated areas or live in other school district 
boundaries and are attending MUSD on inter-district transfers. The ethnic breakdown of the school district is as follows: African 
American 2%, American Indian/Alaska Native <1%, Asian 3%, Filipino 3%, Hispanic 31%, Pacific Islander <1%, White 48%, Two or more 
races 10%, No Response 1%. The district percentage of economically disadvantaged students is 24%. The percent of English Learners 
is 0.8%. 
 
Both Vicente Martinez High School (Vicente or VMHS) and Briones School (Briones) are located at 925 Susana Street in a newly built 
campus situated next to the Martinez Unified School District Office. Vicente consists of seven classrooms, a large multi use room 
(referred to as the Commons) and a food service area. Briones consists of a small multi use room (referred to as the Briones Commons) 
and three offices. Shared space between the two schools consists of a front office, staff lounge with kitchenette, student restrooms, 
staff restroom, records room, garden and basketball court. The campus is also directly across the street from the district’s preschool 
and Martinez Junior High School. The campus is centrally located in Martinez’ downtown where there is close access to county services 
for families in need, the county court house and local businesses for internship and employment opportunities for our students. 
 
Vicente was established in 1968. The school’s purpose is to support students who are at risk of not being able to graduate due to being 
credit deficient. There are many perceptions in the community that students who attend Vicente are the community’s “bad” kids. This 
is entirely not true and our staff strives to get positive messaging out into the community. Positive messaging comes from a one-on-
one orientation that the Principal conducts with each family prior to enrollment, word of mouth from Vicente parents and students 
and the support of Martinez’ local community newspaper, The Martinez Gazette. Some of the common reasons that students become 
credit deficient are: chronic absenteeism, not turning in homework, disengagement with school, getting lost in the shuffle at the 
comprehensive high school and depression and anxiety. Most of the students who attend Vicente have experienced some level of 
trauma in their lives. The Vicente staff understands that students must have their basic needs met in order to engage (or re-engage) 
in school and that is why we allocate funding to provide mental health counseling support as well as ongoing professional development 
for teachers and staff to be able to more effectively work with our students. All staff understands that trust must be built with students 
and we place a strong focus on building relationships with students. It is a difficult population of students and our staff prides itself in 
that we do not hold grudges with students. Every day is a new day and a fresh start. A kind, but firm approach is taken with students 
and expectations are provided. There is a high level of accountability along with a high level of support. Typically students rise to those 
expectations and those who do not are provided greater support. If expectations are not met then fair and consist consequences are 
applied. The school is currently working on implementing more restorative practices and approaches to further the support of all 
students. Vicente has the maximum FTE capacity to serve 92 students since classes sizes cap at 23. The popularity of the program is 
such that students and families are requesting to attend Vicente regardless of if the student is credit deficient or not. Vicente also now 
has a waiting list of students wanting to enroll. This is the first time that this has happened. While Vicente does not offer A-G 
coursework, it does offer a fully accredited high school diploma according to the Western Association of Schools and Colleges. Vicente 
is a Model Continuation High School as awarded by the California Department of Education. The Mental Health Services Act considers 
Vicente's mental health services program to be a model for the State. 

2017-18 School Accountability Report Card for Vicente Martinez High School & Briones School 

Page 2 of 14 

 

 
Vicente is a Title I school. Neither Vicente nor Briones are Program Improvement schools. 
 
The Martinez Education Foundation (MEF) provides grant funding for various school projects and enhancements. They raise money to 
support all Martinez Unified schools mainly through their MEF Run for Education that is held in October annually. Vicente and Briones 
has been granted iPads, a large glass enclosed display case and SMART boards in the past. 
 
Vicente and Briones have many relationships with local businesses. Each year our schools receive monetary and in-kind donations. 
The Saint’s Motorcycle Club does a school supply drive for us and the Kiwanis Club donates funds for library books. Our graduating 
seniors receive over $11,000 in scholarships from Rotary, Lioness Club, Kiwanis Club, Saints Motorcycle Club, Soroptimist and Martinez 
Education Association. Private families also donate scholarship funds, including the Campos Family, the Hobert Family and the Zichichi 
Family. The Nunez Family provides funding for a student to attend the Fire Academy. The Campos Family also sponsors a Thanksgiving 
Feast for all of our students and staff. They donate funds to purchase a small holiday gift for each of our students. Many local 
businesses support our students in internships and employment. Loaves and Fishes, a community food pantry, provides a culinary 
academy for our students. Les Schwab Tires, Jack in the Box, the Copper Skillet Restaurant and many local business hire our students 
regularly. The Martinez Gazette, a community newspaper, publishes information about our schools regularly, including a monthly 
column featuring our Student of the Month. 
 
At Vicente, the current teaching staff consists of 1.0 FTE English Teacher, 1.0 FTE Math Teacher, 1.0 FTE Social Science Teacher, 1.0 
FTE Science/Social Science Teacher, 0.2 FTE Art Teacher (grant funded), 0.4 FTE Special Education Teacher, 0.2 FTE Sports Medicine 
Teacher. During the last five years FTE has decreased from 7.0 FTE to 4.8 FTE. At Briones, the teaching staff consists of three teachers 
totalling 2.1 FTE. 
 
The staff that is shared between Vicente and Briones consists of a 1.0 FTE Office Manager, 0.6 FTE Typist Clerk, 0.2 FTE Academic 
Counselor, 1.0 FTE Mental Health Counselor (0.6 FTE is grant funded), 1.0 FTE Internship Coordinator (fully grant funded), 0.8 FTE 
Custodian, 1.0 FTE Campus Supervisor. There is also a 1.0 FTE Principal who is responsible for both Vicente and Briones. She also serves 
as the district’s Child Welfare and Attendance Liaison and oversees the district’s Student Attendance Review Board (SARB), truancy 
court process and district expulsion hearings. 
 
Vision: The vision of Vicente Martinez High School and Briones School is to provide an alternative school setting where students feel 
supported and can be successful academically while having their social emotional needs met. 
 
Mission: The mission or purpose of the schools is to provide engaging curriculum that relates to real life, to provide experiences that 
prepare students to be college and career ready and equip students with strategies to increase emotional capacity. As a staff, we 
believe that all students can increase their capacity in both areas. 
 
Values: Our values are a sense of connectedness, trust, respect, dedication, caring, positive outlook, motivation, optimism for the 
future, responsibility to give back to the community and globally, collaboration, patience, persistence, self-control, empathy, integrity, 
compassion and personal responsibility. 
 
Schoolwide Learner Outcomes 
 
Inventive Thinkers who: 
Apply learned skills to new situation 
Critically analyze and synthesize information from multiple sources 
Are creative, adaptable problem solvers who are lifelong learners 
 
Effective Communicators who: 
Interact and collaborate with others 
Comprehend, synthesize and report out information from multiple genres in written, oral and digital formats 
Focus, listen and comprehend information and demonstrate learning through note taking and reporting accurately of what was 
presented 
Demonstrate writing skills through reports, letters and essays 
 
Globally Aware Citizens who: 
Understand how choice, decisions and actions affect oneself, others and the community 
Demonstrate knowledge and appreciation of diversity. 
 

2017-18 School Accountability Report Card for Vicente Martinez High School & Briones School 

Page 3 of 14 

 

College and Career Readiness: Students at both Vicente and Briones are provided career and college readiness skills in the form of 
resume writing, cover letter writing, soft skills, interview skills, support with job applications and presentation skills. Local community 
colleges provide support for our students by facilitating application and financial aid workshops for our students at our site. They also 
host us at their campuses several times throughout the school year. Our students are invited to a senior day as well as summer 
experiences that pay and give high school and college credit. Our students are encouraged to concurrently enroll in the local colleges 
and are supported through the student services office. Seniors are required to complete a senior portfolio project that highlights all 
of their career and college readiness skills. Vicente is piloting a Career Pathways course this school year. The Get Focused, Stay Focused 
curriculum is being used. This class is articulated with Diablo Valley College and in the Spring 2018 semester students who successfully 
complete the class will receive both high school and college credit. Students have access to internships during the school day, after 
school and during the summer. 
 
Graduation Requirements: Both Vicente and Briones require 210 credits to graduate. Alhambra High School requires 240 credits to 
graduate. 
 
Additional Funding Source - Mental Health Services Act: Vicente Martinez High School receives two major allocations. The Prevention 
and Early Intervention (PEI) contract is facilitated through the Mental Health Services Act (MHSA). For twelve years, Vicente has been 
awarded this contract that is currently $185,383 to support mental health initiatives. For the first ten years of this contract, services 
were only provided to approximately 23 students per year. Now the services provide support to over 150 students per year across 
Vicente and Briones. Leadership and staff have been instrumental in determining the needs of the students in order for the funding 
to be allocated appropriately. Prior to the current principal being hired, MUSD began contracting with New Leaf Collaborative, a 
501(c)(3) to provide staffing for several positions. The reallocation of the PEI funds in the last two years has allowed for all students to 
be offered and receive services. This has had a positive impact on the school community since student mental health needs are being 
addressed. 
 
Additional Funding Source - Career Pathways Trust Grant II: Vicente also receives the Career Pathways Trust Grant II through the state 
that is facilitated by Alameda County Office of Education. Vicente is in year 4 of this 4 year grant of approximately $650,000. The 
purpose of this grant is to develop, implement and sustain a career pathways program. All students receive access to guest speakers, 
career exploration and internship opportunities even if they are not in the Career Pathways class. 
 
Online Programs: Vicente hosts all credit recovery for the district’s high school students through the use of Edgenuity credit recovery 
program. ALEKS math is used daily in the Vicente and Briones programs. 
 
Intervention for English Learners: During the 2017-18 school year there were seven students who were English Learners at Vicente (6 
students) and Briones (1 student). The supports provided are naturally embedded in the school program. Small class sizes, technology 
use, individualized support are all a part of our English Learners’ school day. CELDT testing is coordinated and administered by district 
student services staff. 
 
Intervention for Low Income/Socio-Economically Disadvantaged Students: 38% of Vicente students and 33% of Briones students are 
socio-economically disadvantaged. We ensure that families understand the free and reduced lunch program and confidentially assist 
them with filing the appropriate paperwork for this program. Students are also provided all school supplies and materials needed, 
including backpacks and binders. Community resource information is provided to families who need assistance. All students have 
access to mental health counseling and some of our disadvantaged students have never accessed services until now. 
 
Intervention for Foster Youth and Homeless Students: We ensure that families understand the free and reduced lunch program and 
assist them with filing the appropriate paperwork for this program as needed. Students are also provided all school supplies and 
materials needed, including backpacks and binders. Community resource information is also provided to families who need assistance. 
All students have access to mental health counseling and some of our disadvantaged students have never accessed services until now. 
Outside organization provide new clothing to our homeless families. 
 
Services for Special Education Students: Vicente has a 0.4 FTE Special Education Teacher who teaches two learning center classes per 
day. Students receive extensive transitional services that focus on career pathways and college readiness. Each student presents 
his/her transition plan at the IEP meeting. 
 
 
 

2017-18 School Accountability Report Card for Vicente Martinez High School & Briones School 

Page 4 of 14