John C. Fremont School resides in Corcoran, California. Corcoran is a small agricultural area in the Central San Joaquin Valley. John C. 
Fremont is one of three elementary schools in Corcoran and serves students in grades two and three. The school provides for the 
educational needs of the second and third-grade students in the community, a Special Day Class for children with special needs for 
children in first through third grade. We also house a County Severely Handicapped Class on the campus. John C. Fremont is a Title I 
school with a student enrollment of approximately 530 students. 
 
John C. Fremont Elementary is a place where all students are encouraged to strive for excellence academically, socially, and 
emotionally in a safe and supportive atmosphere. We set high expectations for our students because our entire school community 
shares the belief that all children can and will learn. 
 
We are committed to the following goals: 

• 
• 
• 
• 
• 
• 

Students will learn to read at grade level or above while developing a love for reading. 
Students will learn to write fluently for a variety of purposes. 
Students will gain an understanding of mathematical concepts and the role that math plays in all areas of life. 
Technology will be used as a tool to enhance all areas of the curriculum. 
Staff will provide instruction and support to meet the needs of diverse learners in our school community 
Staff and students will create an environment that is orderly, safe, inviting and stimulating.