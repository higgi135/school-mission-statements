Allan Peterson Elementary was named in honor of a man who provided leadership for the Merced community. Mr. Peterson was a 
member of the Merced City School District Board of Education for nine years and served as President of the Board. Peterson 
Elementary School serves grades preschool through sixth and lies in the north-central part of the city of Merced. The staff, parents 
and students strive to work as a collaborative to ensure success for all students. The educational program at Peterson Elementary 
School is Common Core Standards-based. Our number one goal is to ensure mastery and proficiency of all students, Preschool through 
6th, in all curricular areas. Peterson School offers a variety of programs unique to the site. The Youth Enrichment Program (YEP) 
provides before and after school child care for Peterson students. The site also mainstreams communicatively handicapped students, 
deaf or hard of hearing students from the Merced County Office of Education Program. We have a 3rd - 6th grade SDC class. Beginning 
in the 2006/2007 school year, Peterson School started a Preschool Program and one is located on the Peterson School campus. 
Beginning in the 2012-2013 school year, Merced City School District joined other districts throughout the state in offering the 
Transitional Kindergarten program to all students born between September 1 and December 2. The mission of Peterson Elementary 
is to provide a safe, nurturing environment of mutual respect while inspiring children to achieve their academic potential as life-long 
learners through challenging curriculum and behavioral guidance supported by dedicated staff and family.