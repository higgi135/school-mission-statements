The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters. As you read this 
Report Card, you will gain a better understanding of Downtown as a school with a record of high achievement, a faculty that is 
professionally skilled and personally committed to meeting the learning needs of students, and a student body which is enthusiastic 
and motivated to perform well. Learning activities include problem based challenges, working cooperatively, and engaged in 
technology and engineering principles. 
 
We, the faculty and staff of Downtown School, are dedicated to maximizing learning for all. Our mission statement says: Downtown 
School is committed to promoting accountable and responsible learners who are empowered in a safe and supportive learning 
environment. We use the acronym C.A.R.E. to remind us that: We are....Committed, Accountable, Responsible, and Empowered. We 
envision a school where all students know they are valued for their individuality and are supported to grow in a manner that best 
reflects their individual strengths and talents. It is our goal to assist each child in becoming a contributing member of a multicultural 
society by creating an environment that nurtures each child’s love of learning, promotes academic excellence, cultivates social 
consciousness, develops physical strength, and ensures emotional well-being. 
Our four main school goals are to: 
1. Ensure that all students are learning successfully at high levels. 
2. Ensure that all students are behaving successfully. 
3. Ensure a Safe School Environment. 
4. Ensure a high level of welcoming parent and community involvement. 
All our school programs work in a coordinated way to support achievement of the goals stated above.