Lakeview Middle School provides approximately 701 sixth through eighth grade students with a rigorous curriculum based on the 
Common Core Standards. We are a one to one school and all our students are provided a Chrome book throughout their three years 
at Lakeivew. Our students benefit from a school program that supports their academic and emotional growth. While we offer a 
comprehensive academic program in all major content areas, we also provide our students a wealth of opportunities to engage in 
elective coursework that includes but is not limited to strong visual and performing arts programs, journalism, and leadership. There 
are a variety of athletic opportunities throughout the year and clubs that strive to cater to our students diverse interests. Some of the 
clubs student's enjoy at Lakeview, are M.E.S.A (Mathematics, Engineering, Science, Achievement), Pen Pal, Club Live, Gaming, QSA 
(Queer Straight Alliance) Club. We strive to meet the needs of all students, including English Learners, Students in Transition, Gifted, 
and Special Education Students. 
 
Lakeview Middle School is a community of parents, students, educators and support staff that are committed to creating and 
maintaining a safe, supportive school environment that provides challenging learning opportunities and inspires family participation, 
giving all our students the skills and abilities necessary to make positive choices and succeed as independent learners and 
conscientious, caring adults. 
 
All students and staff are looking closely at their practices and working to improve in all major content areas. They are using strategic 
data analysis, focused training for teachers, and goal setting at the school, classroom, and student levels. Teachers work in content-
area grade level teams to analyze student data and implement researched-based teaching practices such as the formative assessment 
process. These efforts have helped us make some increases in the number of students in nearly every grade level in both 
English/language arts and mathematics. At Lakeview we are proud of our visual and performing arts program, athletics, technology 
across the curriculum, and on our focus on literacy and mathematics in content area classes. Our strong partnerships with community 
based agencies and organizations like that of Pajaro Valley Prevention and Student Assistance (PVPSA) and University of California 
Santa Cruz (UCSC), allow us to offer unique programs that support our students socio-emotional wellbeing as well as their academic 
achievement. We have initiated the development of implementing PBIS practices as well as those that support us in becoming a 
Trauma Informed campus. In addition, our school is committed to developing our students knowledge of post-secondary career and 
college options. The Lakeview staff celebrates diversity and respect throughout the school year and the staff enjoys having fun with 
students and one another!