Ladera Elementary is a public K-5 school with 336 students located in Tustin, California. The student enrollment includes 31% Asian, 
21% Hispanic, 27% White, Multi-Ethnic 17%, and 4% other. The student population includes subgroups of 24% Socioeconomically 
Disadvantaged and 10% English Language Learners. 
 
At Ladera Elementary School, student success is built upon a commitment by parents, staff, and community to support each student 
as a life-long learner. We expect children to learn and succeed in reaching their maximum potential. Teachers receive ongoing training 
to increase their own professional knowledge and work collaboratively to plan and implement strategies which increase student 
performance. We feel strongly that students require a solid foundation in the basic skills. Children can then apply their knowledge 
through an inquiry-based approach to learning in order to create meaning in their lives. 
 
Academic success at Ladera Elementary is a top priority, but there is also a strong emphasis on the development of the whole child. 
We feel assessment is the key to student achievement, which then drives goal-setting conferences outlining high expectations. We 
also believe that children need enriched curriculum to grow as well-rounded individuals. Our school-wide climate promotes a safe, 
nurturing environment, where children feel a palpable sense of belonging. Welcoming adults and peers care about each others' 
feelings and aspirations and support each others' social, intellectual, and emotional growth. 
 
Our main goals at Ladera are to provide a strong academic program utilizing district curriculum including Units of Study for Reader's 
and Writer's Workshop, Fountas and Pinnell Phonics Lessons, Wonders, Math Expressions, and STEM Scopes. Our goal for each 
student is for them to focus on their own personal growth as they reach and exceed grade level standards. We monitor our students 
growth each trimester through Running Record assessments and math progress checks. The data gathered from this progress 
monitoring is used to inform next steps in first instruction as well as to form plans for intervention and support, including the use of 
Leveled Literacy Intervention (LLI).