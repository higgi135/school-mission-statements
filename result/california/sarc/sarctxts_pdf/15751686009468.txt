School Vision Statement 
The mission of El Tejon School is to provide a program that meets the individual educational needs of each student in a safe and 
positive environment with the goal of preparing each student to be a confident and productive citizen. 
 
Description 
Historically El Tejon School has experienced decreasing enrollment and it appears most recently it has stabilized. El Tejon School serves 
approximately 228 students in grades 5th –8th. It is one of three schools in the El Tejon Unified School District. The other two schools 
are Frazier Park School, which serves students in Transitional kindergarten through fourth grades and Frazier Mountain High School, 
which serves ninth through twelfth grades. 
 
El Tejon School is located along Interstate 5 in the town of Lebec. Its attendance area encompasses a group of rural mountain 
communities in Kern, Los Angeles, and Ventura counties. These communities include Frazier Park, Lake of the Woods, Pinon Pines, 
Pine Mountain Club, and Lockwood Valley. Frazier Park is located approximately forty miles south of Bakersfield and forty miles north 
of Valencia. 
 
Many of the parents in the community commute to Bakersfield or the Los Angeles area for employment. Employers within the 
community are oriented toward the construction, fast food, recreation and service industries. El Tejon Unified School District is one 
of the largest employers in the community. The socioeconomic make-up of the community ranges from low to middle income. 
 
Mission Statement 
El Tejon School’s mission is to fully implement the following: 
 
Curriculum and Instruction: 
 
 
 
 
 

To continue to align the site curriculum, instruction, and assessments with district and state standards. 
To meet the needs of all students in 5th-8th grades so that they achieve proficiency in grade level standards. 
To maintain an ongoing process of evaluation, planning, implementation, and assessment of the academic program. 
To continue to improve the quality of instruction by providing all staff with effective staff development. 
To increase the number of students performing at Meeting Standards or Exceeding Standards levels on (Smarter Balanced 
Summative Assessments) in Mathematics and Language Arts. 
To exit Program Improvement Status. 

 

 
Climate: 

 
 
 
 
 
 
 
 

To foster ownership and stewardship among all staff members, students, and parents. 
To foster an open, friendly, supportive, and professional environment for all. 
To build a stimulating and productive environment for learning and teamwork. 
To maintain a safe and secure campus. 
Partnerships: 
To create and maintain productive partnerships between El Tejon School and the community. 
To maintain a positive relationship with the media. 
To provide meaningful and regular opportunities for parents and community members to become partners in education. 

 

2017-18 School Accountability Report Card for El Tejon Middle School 

Page 2 of 11 

 

Principal’s Message 
As parents and community members read this report, what will emerge is a picture of a school community that is committed to 
providing a student-centered educational environment with the expectation that all students focus on reaching their learning 
potential. We are an effective school with a strong academic focus and a commitment to continuous improvement; a positive and safe 
learning environment, with a staff that is professionally skilled and personally committed to meeting the learning and emotional needs 
of students; and a student body that works to meet expectations and perform at its highest level of academic competence. 
The El Tejon staff strives to instill in ALL students the desire to become lifelong learners, while demonstrating that education is essential 
to their lives. It is our goal that all students reach a level of skill and knowledge that will enable them to participate productively in an 
ever changing world. 
 
Sincerely, 
Leonard Lopez