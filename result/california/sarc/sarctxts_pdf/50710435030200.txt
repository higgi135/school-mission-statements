DESCRIPTION: 
 
Argus High School is an alternative education campus located in the central San Joaquin Valley, in the city of Ceres. Ceres is home to 
48,697 people, making it the third largest city in Stanislaus county. Ceres has almost doubled in size since 1990, resulting in increased 
enrollment at Argus. 
 
Argus High School has been recognized as a Model Continuation High School by the California Continuation Education Association 
(C.C.E.A.). For over forty years Argus has proudly served the students and families of Ceres as the safety net for those students whose 
learning experiences have not been met at the traditional high schools. Argus High School maintains a student enrollment near 200 
students, split between two instructional sessions. The current CBEDS/CALPADS report indicated that the student demographics 
consisted of 73.8% Hispanic, 18.6% White, 2.4% Asian, 0% Filipino, 1.4% African-American, and 1% American Indian. English Learners 
comprise 13.3% of the student population and 84.8% of the students qualify for the National School Lunch Program. 
 
Argus High School maintains a staff of seven regular education teachers, one full-time special education teacher, one office manager, 
one secretary IV, one attendance clerk, one community liaison, one campus supervisor, one full-time paraprofessional, one 
administrative assistant, two learning directors and a principal. In addition, Ceres Unified hired a third School Resource Officer, through 
the Ceres Police Department, who is dedicated full-time to the Argus/Endeavor site. 
 
The staff at Argus High School is committed to providing ongoing communication with all students and families. Argus utilizes the 
Parent Square communication system, quarterly newsletters, an active website and several types of social media to keep home-school 
communication open. Parent participation is encouraged by all staff members and remains a focus of the Family Engagement Team. 
Argus students participate in a variety of community events, including Breast Cancer Awareness (October), the Great American 
Smokeout (November), a local canned food and toy drive (December), and Pennies for Patients through the Leukemia Society (January-
February). 
 
Professional learning and staff development is focused on implementing the California State, English Language Development and 
Literacy Standards. 
 
MISSION STATEMENT: The mission of Argus High School is to provide each student with new opportunities and alternative paths to 
fulfill his/her unique potential. 
 
EXPECTED STUDENT LEARNING RESULTS (ESLR’S) 

• 

Technologically Literate Individuals 

Use and have knowledge of technology for school, work and home. 
 
Access information and solve problems through the use of the latest technology. 

• Academically Proficient Individuals 

Will pass the California High School Exit Exam. 
 
Show progress toward meeting state standards in core academic areas. 

• Effective Communicators 

2017-18 School Accountability Report Card for Argus Continuation High School 

Page 2 of 14 

 

Read and understand a variety of materials and/or genre. 
 
Write effectively using both technological and traditional methods. 
 
Listen, speak, and work effectively as individuals and in groups. 

• Responsible Citizens 

Understand and observe rules and laws. 
 
Monitor and correct their own performance and behavior. 
 
Pursue a personal career path.