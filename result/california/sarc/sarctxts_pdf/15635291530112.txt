Vista West is a continuation high school campus within the Kern High School District and is located 
three miles from central Bakersfield on State Highway 58 in a suburban area called Rosedale. The 
school was built in 1935 as a W.P.A. project and was originally intended to provide a school for the 
children of families migrating to the San Joaquin Valley from Texas and Oklahoma. The school is 
considered a historical site. Vista West opened in 1986 and a state-financed remodel was 
completed in 1993. Classrooms were carpeted and airconditioned; new chalkboards, tack boards, 
Smart Boards, and white boards were added. Ceilings were dropped and new wiring and lighting 
added. The result was a small school with a new feel. Yet, where possible, the 1935 architecture 
has been preserved to keep the traditional “flavor” of the school. 
 
The school has a total of thirty indoor instructional areas, an auditorium which can seat up to 300, 
a library, an administration building with the principal’s office, a dean’s office, two counselor 
offices, student support and intervention facilities, and clerical support areas. These areas were 
remodeled to put all personal counseling, intervention, and 9th-, 10th- and 11th- grade counseling 
in one area. There is another program called AIM (Alternative Instructional Method) which shares 
a campus with VWHS. This program is designed for special education students. Although it is on 
the same campus, AIM is a completely separate program and is not associated with Vista West High 
School. 
 
School Mission and Vision 
The Mission of Vista West High School is to provide students the education, skills and behaviors 
needed to graduate from high school prepared to succeed in the workplace or at the post-
secondary level, and to become productive, responsible citizens. 
 
The Vision of Vista West High School is that students will be prepared to become lifelong learners 
who will contribute to the community. Students will become responsible learners through 
challenging “standards-based” course work who can communicate effectively, think critically, and 
solve problems. The collaboration of school, home, and community will prepare every graduate for 
the challenges of the future. 
 

----

---- 

Kern High School District 

5801 Sundale Ave. 

Bakersfield, CA 93309-2924 

(661) 827-3100 

www.kernhigh.org 

 

District Governing Board 

J. Bryan Batey, President 

Joey O' Connell, Vice President 

Jeff Flores, Clerk 

Cynthia Brakeman, Clerk Pro Tem 

Janice Graves, Member 

 

District Administration 

Bryon Schaefer, Ed.D. 

Superintendent 

Scott Cole, Ed.D. 

Deputy Superintendent, Business 

Michael Zulfa, Ed.D. 

Associate Superintendent, Human 

Resources 

Brenda Lewis, Ed.D. 

Associate Superintendent, 

Instruction 

Dean McGee, Ed.D. 

Associate Superintendent, 
Educational Services and 

Innovative Programs 

 

2017-18 School Accountability Report Card for Vista West Continuation High School 

Page 1 of 11