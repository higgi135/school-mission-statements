The Envision Academy of Arts and Technology is a small, public charter high school located in downtown Oakland. The Envision 
Academy program includes 4 R's that set our school apart from other high schools. 
 
Envision Academy's mission is to inspire and prepare all students to enter, graduate from, and succeed in a 4-year college and in life. 
 
Rigor: The mission of our school is to prepare all of our students to enter and be successful in college. All of our students take rigorous, 
college-prep courses that make them eligible for the UC and CSU systems. These courses focus on helping students master essential 
content standards in each course and the Envision 21st Century Leadership Skills: Critical Thinking, Effective Project Management, 
Productive Collaboration, and Powerful Communication. 
 
Relevance: Teaching and learning at Envision Academy focuses on project-based learning. Our students never ask “When are we ever 
going to need to know this?” because most of what they learn is in a real-world, engaging context. Students work on projects regularly 
in order to prepare for success in college and 21st Century careers. Students share their projects throughout the year with their parents 
and the school community in public Exhibition events. Envision Academy integrates art and technology into the curriculum. Students 
take Digital Media Literacy & Expression and Performing Arts while in high school. Students learn 21st Century leadership skills through 
regular use of computer technology – the school maintains a 2:1 ratio of students to laptop computers. 
 
Relationships: Envision Academy maintains an intentionally small school community. Envision Academy serves approximately 330 
students. Class sizes are generally lower than other public schools so that students can receive personalized attention from teachers. 
The small school structure promotes the development of strong, supportive relationships among students and staff. Our students and 
their learning needs become well known in this family-like environment. Each student has an Advisor, who takes special responsibility 
for advocating for his or her students, making sure they do not fall through the cracks, and serving as a liaison with their families. 
 
Results: Envision Academy students are prepared to do well on standardized assessments such as the STAR and college entrance 
exams. All students, in order to earn their diploma, are required to defend their College Success Portfolio in front of a panel, their 
family, and fellow classmates. This Portfolio contains evidence of the student’s mastery of both content skills and the Envision 21st 
Century Leadership Skills.