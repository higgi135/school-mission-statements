James M. Burchfield is the only primary school in the Colusa Unified School District. It serves children in Pre-Kindergarten through 
third grades. The students, staff and parents of Burchfield Primary School are committed to creating the best possible learning 
environment for our children. 
 
Our Vision 
Building relationships to strengthen community that cross lines of culture and economics and provide opportunities for students to 
reach their full potential in body, mind, and spirit. 
 
Our Mission 
In conjunction with our district mission statement of, “Provide a safe, student-centered, high quality education for ALL students," we 
at Burchfield Primary School are continually striving to improve our programs and meet the challenging needs of our diverse 
population. Parents, teachers, and students working as a team offer the best opportunity for success. We are dedicated to helping 
our students achieve academic excellence, learn respect for themselves and others, and develop a sense of integrity.