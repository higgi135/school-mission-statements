Description of District 
The Visalia Unified School District is the oldest school district in Tulare County. The district is comprised of 25 elementary schools, 5 
middle schools, 4 comprehensive high schools, 1 continuation high school, 5 charter schools, 1 adult school, and a school that serves 
orthopedic handicapped students. Over 32,000 students Pre-K to adult are served through the Visalia Unified School District. 
 
Description of School 
Pinkham Elementary School served approximately 550 students in grades K-6 in 2017-2018. Our teachers and staff are dedicated to 
ensuring the academic success of every student and providing a safe and productive learning experience. The school holds high 
expectations for the academic and social development of all students. Curriculum planning, staff development, and assessment 
activities are focused on assisting students in mastering the state academic content standards, as well as increasing the overall student 
achievement of all subgroups. 
 
School Mission Statement 
It is the belief of Pinkham Elementary School that all students can and will excel in an environment that is tailored to their evolving 
needs and conducive to all facets of the learning process. It is due to this belief that we have been able to successfully develop a 
comprehensive educational system that meets the needs of all students. We are engaged in continuous improvement of our learning 
community by focusing on the intellectual, emotional, and social growth of individuals. We celebrate and promote ethnic and cultural 
diversity. We strive to provide students with not only the stimulation and encouragement to accomplish such feats but the means to 
do so as well.