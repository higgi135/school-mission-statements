Badger Springs Middle School is located on Delphinium Avenue between Indian Street and Perris 
Boulevard in the city of Moreno Valley. The school is considered a neighborhood school with a 
majority of the students walking to school and the remaining students riding the bus. Badger 
Springs is a Gold Ribbon School, a Title I Achieving School and a Project Read Exemplar Site. Badger 
Springs has also received multiple highly competitive i3 grants. These grants provide additional 
funding to provide curriculum that is aligned with Common Core State Standards and to provide 
teachers with high quality professional development in research proven instructional strategies and 
techniques so students can meet the rigor of the new standards. Each academic department is 
collaborating together in the Professional Learning Community model to identify key academic 
standards for each grade level, share best practices, and implement research proven instructional 
strategies. Students needing remediation or additional support are referred to after school tutorial 
programs and are assigned additional support classes during the school day in Mathematics and 
English Language Arts. Badger Springs also offers many electives that support Visual and Performing 
Arts and in becoming college and career ready such as: AVID, Spanish I, Band, Choir, Guitar, Art and 
Videography. 
 
Our Mission: The mission of Badger Springs Middle School is to provide high quality instruction for 
all students that prepares them to be academically and socially successful high school graduates 
that are college and career ready. 
 
Our Vision: Better Students...Stronger Families...Brighter Futures...through high quality instruction. 
 
We Value: Equity, Integrity, Respect, Relationships, Learning and Collaboration. 
 

----
---- 
Moreno Valley Unified School 

District 

25634 Alessandro Blvd 

Moreno Valley, CA 92553 

(951) 571-7500 
www.mvusd.net 

 

District Governing Board 

Susan Smith, President 

Jesus M. Holguin, Vice-President 

Cleveland Johnson, Clerk 

Gary E. Baugh. Ed.S. 

 

District Administration 

Martinrex Kedziora, Ed.D. 

Superintendent 

Maribel Mattox 

Chief Academic Officer, 

Educational Services 

Tina Daigneault 

Chief Business Official, Business 

Services 

Robert J. Verdi, Ed.D. 

Chief Human Resources Officer, 

Human Resources 

 
 

 

2017-18 School Accountability Report Card for Badger Springs Middle School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Badger Springs Middle School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

55 

43 

55 

0 

0 

0 

0 

1 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Badger Springs Middle School 

16-17 

17-18 

18-19