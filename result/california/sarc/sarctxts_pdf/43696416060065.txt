Frank S. Greene Middle School, previously named David Starr Jordan Middle School, opened in 1937. The school was closed for six 
years from 1985 to 1991 due to a decline in district enrollment. The school is currently home to over 1,000 students, and enrollment 
is continuing to grow modestly. In the Spring of 2018, the name of the school was changed to honor Dr. Frank S. Greene, Jr., local 
scientist, educator, entrepreneur and role model. 
 
Most Greene students come from the following elementary schools: Addison, Duveneck, Escondido, Walter Hays, Hoover, and Ohlone. 
 
Greene Middle School provides a personalized, engaging, and academically rigorous education for our students. We have a rotating 
7-period schedule, with each period meeting 4 times a week. Students also meet weekly with a faculty advisor. 
 
Sixth grade students are grouped in core teams of roughly 50 students, sharing two teachers for their 4 core subjects— Math, Science, 
English, and Social Studies—across 5 periods. In addition, sixth grade students have one period that is shared between music and 
physical education and one period of our exploratory wheel—a rotation of seven elective classes. 
 
Students in 7th and 8th grade are also grouped together in interdisciplinary teams, but students in these grades have a single teacher 
for each subject area. Seventh and 8th grade students take 4 core classes, PE, and choose among a rich array of 34 elective courses 
for their remaining two periods. Greene's elective course offerings include multi-media art, band, jazz, orchestra, foods/nutrition, 
leadership, drama, web page design, video production, broadcast media, biotechnology, Spanish, French, and Japanese. Greene also 
boasts a diversity of extracurricular sports, performing arts, and student clubs. 
 
Mission Statement: 
 
Greene Middle School is a community of students, staff, and families dedicated to a supportive and safe learning environment which 
fosters academic growth, resilience, independence, and personal enrichment. We build and facilitate independence in learning 
through collaboration among staff, students and families by implementing, reflecting and supporting best practices that benefit our 
school community. 
 
 
A recent expansion of our mission is our commitment to students beyond the classroom. In the Fall of 2009, the School Climate 
Committee was established to lead our continuing interest in promoting a positive school climate, which can have a significant impact 
on student learning and social emotional health. Comprised of a working group of students, teachers, and parents, we are focused on 
the school's core values of respect, responsibility, and safety. Our goals in developing student programs are to empower students, 
help them feel connected, and foster responsible decision-making. Our hope is to create a learning environment that instills a sense 
of belonging to all students, so they feel safe, valued, and respected as both individuals and contributing members of the school 
community. 
 
 
 

2017-18 School Accountability Report Card for Frank S. Greene Jr. Middle School 

Page 2 of 11