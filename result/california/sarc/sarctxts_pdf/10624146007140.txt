Jefferson Instructional Vision: 
Tigers will collaborate, communicate, think critically and be creative in an engaging rigorous environment supported by universal 
access. 
 
Jefferson is one of fourteen schools in Sanger Unified School District. Jefferson is K-5 school with a population of 410 kindergarten 
through 5th grade. The school has a gender mix of 53.7% male and 46.3% female. Special populations include: Special Education [RSP] 
5%, Migrant Education & English Learners 50%. Jefferson’s student populations include 203 English Learners and 31 students with 
disabilities. All students receive a free lunch. All subgroups including Hispanic/Socioeconomically disadvantaged and English 
Language Learners met the AYP goals in previous CST Testing, and have shown significant growth under the new CAASPP Testing. 
 
 School Demographics Profile: 
Sanger Unified School District is made up of twenty-one schools. There are fourteen elementary sites, a middle school, and one 
comprehensive high school. There are two K-8 and one K-12 charter schools. In addition, there is an alternative education 
continuation school, an alternative education independent study school, a community day school, and an adult school. The district 
covers 180 square miles and serves a population of 31,000. Within the boundaries of Sanger Unified is the city of Sanger and the 
communities of Centerville, Del Rey, Fairmont, Lone Star, Tivy Valley and portions of the Sunnyside area of metropolitan Fresno. 
Attendance in the District’s schools currently numbers 11,360 students. The district is rich in agriculture to the east and south, while 
the north and west are a combination of agriculture and suburban areas. 
 
The students at Jefferson Elementary represent a large population of minority and low socio-economic diversity. One hundred (100%) 
percent of the students receive free or reduced-price meals. The ethnic composition of the school currently includes 95% Hispanic, 
3% White, 0.2 % Asian, 0.4% African American and 1.4% multiple. 
 
 Jefferson has a pre-school program that prepares students for K-5 education at Jefferson. Jefferson has access to a Library and 
multimedia resources. 
 
 There is a staff of 16 full time credentialed teachers. All staff maintains an Elementary/Self-contained/multiple subject credential and 
CLAD/SDAIE English Learner Credential. 
 
 Regular parent communication is ongoing and encouraged throughout the year. Jefferson offers a weekly calendar which updates 
parents on activities for the week as well as upcoming events. Communication is also offered through phone messages, SSC, ELAC, 
Parent University classes, Facebook page, Schoolway app, website, parent-teacher conferences and PTO. All notices and phone calls 
go out in English and Spanish. 
 
 
 

2017-18 School Accountability Report Card for Jefferson Elementary School 

Page 2 of 12