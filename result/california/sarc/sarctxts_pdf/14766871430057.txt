PRINCIPAL'S MESSAGE 
As principal, it is my pleasure to be serving Palisade Glacier High School and to welcome you to this 
edition of our accountability report card. This document provides valuable information regarding 
the status and performance of our school. The entire staff at PGHS and all of the members of the 
Bishop Unified School District are completely dedicated to providing an exemplary and successful 
educational experience, individually and collectively, for all students. I encourage readers to 
examine this document closely and to pursue desired additional information through any of the 
contacts at the district. 
Katie Kolker, Principal 
 
MISSION STATEMENT 
Palisade Glacier High School (PGHS) is dedicated to providing an opportunity for all students, 
including credit deficient students and those who desire an accelerated graduation track, to learn 
in a safe, rigorous, yet caring 
incorporates and values 
differentiated/individualized instruction to address the learning styles and needs of every student. 
In addition, PGHS is committed to providing stellar support services and strategies to address and 
negate at-risk behaviors and support our goal of sending our students equipped to compete and 
succeed in the global society of the 21st Century. 
 
SCHOOL PHILOSOPHY 
Palisade Glacier High School provides an alternative program by which students may earn an 
accredited high school diploma. The program primarily addresses the students’ academic needs, 
but it also provides a setting where individual vocational, emotional, and personal needs can be 
more readily addressed. By developing an interactive relationship between staff and each young 
adult, we help each student establish attainable goals and construct their own unique post high 
school academic or vocational plan. 
 
Accountability for and ownership of success lies within each individual student. The staff 
encourages responsibility through positive reinforcement of academic achievement, positive social 
interaction, and personal growth and accomplishment. The PGHS environment supports students 
to acquire the attitudes and abilities necessary to become positive and productive members of the 
community and the larger society. 
 
SCHOOL PROFILE 
Palisade Glacier High School is located west of the Eastern Sierra town of Big Pine and serves 
students in grades nine through twelve following a traditional calendar. The majority of our 
students are 11th and 12th graders with a spectrum of credit deficiencies. In addition, we also have 
students who are on track to graduate on time or sometimes even up to a year early. A focus on 
individual progress and achievement and excellence in learning is a hallmark of our school. We do 
not accept work for credit below C quality. In addition to core academic classes, we offer vocational 
and fine arts courses such as Digital Media, AutoCAD/Entrepreneurship, Performing Arts, and 
Photography. 
 

2017-18 School Accountability Report Card for Palisade Glacier High School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Palisade Glacier High School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

3 

0 

0 

3 

0 

0 

3 

0 

0 

Bishop Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

95 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Palisade Glacier High School 

16-17 

17-18 

18-19