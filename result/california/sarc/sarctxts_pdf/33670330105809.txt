Harada Elementary School is a place where: 

 

• High expectations for all are upheld 
• Diversity is respected and celebrated 
• 
• Parents are valued as active and vital partners in the education of their children 
• 

Students and Staff are Lifelong Learners 

Students take responsibility for their own actions and are held accountable for their academic performance and their social 
interactions with peers. 
Students are challenged to reach their fullest potential. 

• 

Our Mission: 
 
At Harada Elementary all students are enthusiastic scholars, productive citizens, and are preparing to excel in college. 
 
Harada is part of the Corona-Norco Unified School District, which has an enrollment of over 54,000 students. There are thirty two 
elementary schools in the district. The surrounding area is a largely suburban and ethnically diverse community with working and 
middle class families. 
 
Harada Elementary School opened on July 6, 2004. It is located on the northern edge of the Corona-Norco Unified School District near 
the corner of Hamner and Limonite, in the city of Eastvale. Harada presently has approximately 1350 students. The school is on a four 
track, year-round schedule. 
 
Harada has 248 English Language Learners and students that speak over 14 different languages. Harada is also home to a Dual 
Language Immersion program for Spanish and English. This program spans Kindergarten through Sixth grade on A-track. We also have 
two Transitional Kindergarten (TK) classes. 
 
Harada also has 2 Special Day Classes and 2 Resource Specialist Program classrooms to support the academic needs of students that 
require specialized academic instruction. 
 
Harada is very fortunate to have a facility with two computer labs with over 100 computers, a library that would rival the size of many 
high school libraries, and 39 classrooms. We have 48 certificated teachers, 2 full time RSP teachers, one psychologist, one full time 
speech therapist and one half time. A school counselor and a band teacher are assigned to our school a few days each week. Other 
staff members include a library clerk, three custodians, one secretary, two full time and two part-time clerk-typists, three cafeteria 
workers, nine instructional aides, two full time and one half time bilingual aides, and nine noon duty supervisors. 
 
 

2017-18 School Accountability Report Card for Harada Elementary School 

Page 2 of 13