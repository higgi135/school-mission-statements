Plumas County Community School (PCCS) provided students with an alternative path to high school graduation – one that allowed 
students with atypical learning styles and life situations to make academic progress in their own ways. The PCCS School Plan for 
student achievement required the development of an individual learning plan for each student. The plans were intended to culminate 
in the acquisition of a Plumas Unified School District high school diploma. PCCS students studied independently, with individual 
instruction from a full-time credentialed teacher, and assistance from full-time, highly qualified alternative education aide. PCCS 
Students monitored progress in academic credits completed toward graduation. As a County Community School, PCCS allocated 
academic credit incrementally, so students can track their progress in smaller units of time. Each week each student received a 
statement of credits earned that week, and of credits remaining toward graduation.