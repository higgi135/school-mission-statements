Monte Vista Elementary is located in the West Covina Unified School District in the city of West Covina. Monte Vista is a TK-6th grade, 
school-wide Title I school. Monte Vista is proud to be a No Excuses University, Spanish Dual Immersion school. Currently, our Dual 
Immersion program is available to Transitional Kindergarten to third-grade students with plans to continue growing at each 
subsequent year of implementation. Monte Vista serves approximately 540 students following a traditional school calendar. Our 
school mission statement is as follows; Through mutual responsibility and collaboration, Monte Vista will provide every student a safe, 
inclusive environment that promotes academics, college and career readiness, and life long learning skills. All students will be critical 
thinkers, productive citizens and prepared to contribute to a global community. Monte Vista has been a No Excuses University partner 
school since 2015 and believes the six exceptional systems guide our work and goals. Our Monte Vista NEU exceptional systems are a 
culture of universal achievement, collaboration, standards alignment, assessment, data management, and intervention. Through the 
implementation of these exceptional systems, we support and monitoring student achievement and progress. Our school motto is, 
"After High School Comes College!" Our school motto is promoted at all school rallies, assemblies, and daily classroom meetings. Our 
school culture of universal achievement for all students and all sub-groups (foster youth, homeless, English Language Learners, Special 
Education) defines who we are as a staff and our commitment to learning. Our K-4 Reading Initiative, Monthly NEU rallies, Capturing 
Kids Hearts, Professional Learning Communities, NEU Pledge, and STEP (Success Through Educational Participation) our school-wide 
behavior management plan are examples of our outstanding school culture and support to students. 
 
Monte Vista prides itself in being an inclusive school community. Students in need of specialized academic instruction are 
mainstreamed as much as possible into the general education setting per their IEP goals. Our Special Education teachers and staff 
work collaboratively with the General Education teachers and staff to plan instructional opportunities aligned to the core curriculum 
and grade level standards. All staff members at Monte Vista are committed to professional learning communities and learning. Our 
school utilizes PLC to collaborate, share and discuss academic progress, assessment results and school culture. Monte Vista staff has 
great respect for planning time so that we may focus time to what truly matters, student achievement. Our strong leadership team, 
teachers, a teacher on special assignment (TOSA) and principal promote a positive learning environment for all and welcome your 
suggestions and inquiries. Our site TOSA as welcome meetings with foster youth to ensure they have all the necessary resources to be 
successful at Monte Vista. Moreover, she monitors their academic progress and recommends interventions as needed to help address 
any achievement gaps. 
 
Our vision is to provide our students with a safe and supportive environment where they can achieve academic excellence and personal 
development. We have made a commitment to provide the best educational program possible for Monte Vista Elementary School's 
students and welcome new members to our school community. Additionally, we offer a highly attended after-school enrichment and 
intervention program called the YASES. If you have any questions may have about the information contained in this report or about 
our school please contact our school office and/or administration. 
 
 

2017-18 School Accountability Report Card for Monte Vista Elementary School 

Page 2 of 12