The Central School staff is working in partnership with our students’ parents and the community members to provide the best 
education possible for our Central students. We believe in our school motto, “Student, Parent, School: Together We Achieve More.” 
 
The Central School staff is dedicated to providing each student an educational experience that is built upon exemplary programs, 
student services, and activities that foster lifelong learning and prepare our students to be productive citizens in our democratic 
society. We are dedicated to teaching the whole child. 
 
We understand that parents and the community expect us to provide a safe learning environment, as well as a quality education. The 
Central staff believes that concerned and involved parents are the most important factor in creating academic success for our students. 
We will keep parents informed as to their child’s school progress throughout the year. 
At Central we believe that students achieve their full potential when there is a partnership between the home and school. If you would 
like further information on how to get involved in your child’s classroom or volunteer at Central, please call the school office at (559) 
924-7797. 
 
Our goal is to make each student’s educational experience at Central Union Elementary School positive, challenging, and rewarding. 
 
Mission Statement 
Our mission at Central Union Elementary School is to provide a safe school environment, build positive character, celebrate diversity, 
and support academic excellence. 
Central School staff is committed to building and strengthening family and community partnerships for the purpose of student success 
and enabling students to function as productive and successful citizens in a changing society. 
 
School Profile 
Central School, located in Lemoore, takes pride in its diverse student population comprised of grades kindergarten through eight. 
Students are encouraged to meet the challenges of the present in order to succeed in the future. The cooperative efforts that are 
exemplified by the staff, students, parents, and community help to ensure the best possible learning environment for the students at 
Central School. Students are encouraged to reach their highest potential and present themselves in a positive manner. 
 
 
 
 
 

2017-18 School Accountability Report Card for Central Elementary School 

Page 2 of 12