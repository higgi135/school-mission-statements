Principal’s Message 
 The purpose of the School Accountability Report Card is to provide information to parents and community stakeholders on Pioneer 
Middle School’s instructional programs, academic achievement, materials and facilities, and the staff. Parents and community play a 
vital role in our schools, understanding our educational program, student achievement, and curriculum development can assist both 
our schools and the community in ongoing program improvement. 
 
 Pioneer MIddle School has made a commitment to provide the best educational program possible for our students. The excellent 
quality of our program is a reflection of our highly committed staff. We are dedicated to ensuring that our school provides a welcoming, 
stimulating environment where students are actively involved in learning academics as well as positive values. 
 
 District & School Profile 
 Pioneer Union Elementary School District serves about 1600 students and is comprised of two elementary schools and one middle 
school. The district and its schools are located in the town of Hanford, situated in the San Joaquin Valley midway between San Francisco 
and Los Angeles. As one of eight charter districts in the state, Pioneer Union Elementary School District offers the local community an 
exceptional educational program emphasizing student achievement academically as well as socially. Using research-based, innovative 
instructional methods and taking advantage of generous parent volunteers, students experience a rigorous standards-based 
curriculum administered by highly qualified teachers in a safe, nurturing environment. 
 
Pioneer Middle School enrolled for 2017-2018 was, 539 students in grades six through eight; the student body included 9% in special 
education, 3.9% qualifying for English Learner support, and 44% identified as Socio-Economically Disadvantaged. All staff members support 
the school’s mission to provide a challenging curriculum designed to develop life-long learners while providing a curriculum in a nurturing 
environment where social and personal needs are met. Pioneer Middle School takes great pride in providing our students with a well-rounded 
educational experience that includes opportunities to grow in athletics, academics, and via extra-curricular opportunities. There are various 
clubs on campus to support the personal growth of our students as well as to help develop positive connections for students. 
 
 District Vision 
 The Pioneer Union Elementary School District, in partnership with parents and the community, will build the foundation for student 
academic and social success by ensuring that all students receive rigorous instruction, support and intervention in an enriching 
environment. 
 
 District Mission 
 In order to challenge all students to learn, achieve and act with purpose and compassion, the Pioneer Union Elementary School District 
will develop and produce motivated, confident students who will: 
 

1. Meet or exceed grade-level academic standards; 
2. Become a life-long learner; 
3. Effectively communicate; 
4. Become contributing citizens of the community; 
5. Be prepared for a successful future. 

 
 

2017-18 School Accountability Report Card for Pioneer Middle School 

Page 2 of 12