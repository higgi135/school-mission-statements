Eastvale Elementary will be known for their students’ ability to demonstrate high academic performance while exhibiting compassion 
and integrity. 
 
Eastvale Elementary Mission Statement 
“To promote all students with high levels of academic and personal achievement, ready for college and career excellence, through 
researched based instruction, healthy school climate and collaborative system of support.” 
 
We are committed to work collaboratively to provide: 
A comprehensive and challenging standards-based curriculum with inquiry based instruction that develops critical thinking, problem 
solving, and reflects current research based best practices. 
A school climate and culture that celebrates different cultures and encourages self-esteem, self-discipline, and self-expression. 
An education based on collaboration and provides our students the tools to be ready for 21st century college and careers, and develop 
future leaders, critical thinkers, and productive citizens. 
 
 
In July of 2006, Eastvale opened its doors as the 29th elementary school in the Corona-Norco School District. Our state-of-the-art 
school is home to approximately 1300 students who reside in the newly-developed, middle class Eastvale area. Eastvale’s 
neighborhood is made up of students from a diverse range of ethnicity. Currently we are 51.4 points distance from Level 3 in ELA 
and 15.6 points distance for Level 3 in Math. We currently have met the state proficiency requirement in both ELA and Math with our 
current 2017-18 CAASP scores. 
 
When Eastvale Elementary was designed and built, a vision of student and teacher excellence was at the forefront of thought. The 
culture being developed at Eastvale among students, faculty, parents and community is a strong one. Together, we have set high 
standards for achievement and expect to reach our goals. As our vision clearly states, ““To promote all students with high levels of 
academic and personal achievement, ready for college and career excellence, through researched based instruction, healthy school 
climate and collaborative system of support.” 
 
Since Eastvale’s inception, we’ve implemented a myriad of exemplary programs to assist our teachers in designing well-organized, 
standards-based instruction. We currently employ 49 teachers who have strengths in many different disciplines including reading, 
mathematics, administration, special education, business administration, cross-cultural teaching and educational technology. Team 
building, grade-level meetings, cooperative lesson planning through team collaboration that take place during our PCT meetings 
provide our educators with the ability to analyze test data and administer quality instruction throughout the curriculum. By using the 
multitude of resources that each teacher brings to this school, the formula is in place for all students to progress and succeed. 
 
Our teachers deliver multi-modality instruction to students of all levels and abilities. Eastvale currently has over 180 students who are 
English Language Learners including our re-designated students, 152 students who qualify for special education services, 45 students 
identified as GATE, and 200 students receiving either reading or math assistance before or after school. Two Resource Specialists, 
Psychologist, Counselor, Adaptive Physical Education Instructor, Occupational Therapist, Physical Therapist, Applied Behavior Analysis 
Specialist, and two Speech and Language Pathologists are the educational specialists who are available at our site. In addition, the 
office, custodial and food services staffs are well trained and provide optimal service to the students, parents, teachers and site. 
 
We, at Eastvale, have set high expectations, not only for ourselves as a staff, but for our students as well. We believe that all students 
can succeed, and it is our job and duty to see to it this goal is met. Students are our priority, and the educational opportunities are 
offered to them at Eastvale Elementary are of the highest standards possible. Our focus at Eastvale is and will remain, “Every Child, 
Every Day, Every Minute, by Everyone.” 
 

2017-18 School Accountability Report Card for Eastvale Elementary School 

Page 2 of 14