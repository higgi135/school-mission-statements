Roberts Ferry Charter School has distinguished itself for its nurturing and supportive environment 
and academic achievement. 
This small, rural school has a tradition of pride in serving the “total” student and their families. 
 
Challenges in the coming year include maintaining and improving upon this tradition of student 
learning in a supportive environment. Our charter school was established in the district in an 
attempt to expand your student’s educational options. A facilities cafeteria modernization was 
completed. 
 

-------- 

Roberts Ferry Union Elementary 

School District 

101 Roberts Ferry Rd 

Waterford 

209-874-2331 

www.robertsferry.k12.ca.us 

 

District Governing Board 

Mrs Jaime Burroughs 

Mr Brent Stout 

Mr Paul Ichord 

Mrs Marlene Erickson 

Mrs Pat Rodgers 

 

District Administration 

Mr. Bob Loretelli 
Superintendent 

 

2017-18 School Accountability Report Card for Roberts Ferry Charter School Academy 

Page 1 of 7