Mission 
The Pearblossom School mission is to provide a safe and positive learning environment that 
respects and values diversity, empowers children to reach their greatest potential, and prepares 
them with the 21st century skills necessary for life-long learning. 
 
Vision 
Pearblossom School, where the: 
 
All students are equipped for success, 
 
All staff is empowered to get them there, and 
 
All school and community are partnered for the duration of the process. 
 
School Description 
Pearblossom Elementary School is located in southern California in the Antelope Valley. It is one of 
six schools in the Keppel Union School District . Pearblossom Elementary School’s attendance area 
covers 178 square miles. Approximately 70% of students live outside a one mile radius of the 
school. Pearblossom currently has a population of 336 students. Of that group, 55% of the students 
are Hispanic, 36% Caucasian, and 7% African American. All students, including English Learners, 
are provided standards aligned textbooks and/or instructional materials. Curriculum is driven by 
common core standards and materials and resources are identified to meet the rigor of the 
standards. The following are descriptions of the curricular resources used to meet common core 
standards: 
 
Pearblossom Elementary School currently employs a Principal and 14 general education certificated 
teachers, two Specialized Academic Instruction teachers, and 1 intervention teacher. All teachers 
are highly qualified teachers. The school also employs five classified personnel including 
paraprofessionals, a bilingual instructional assistant, a librarian, and clerical staff. There are four 
recreation leaders who provide daily cafeteria and playground supervision. Pearblossom 
Elementary School has a fully operational kitchen with five staff members. In addition, Pearblossom 
has two full-time custodians, one day, and one night custodian, and a groundskeeper. 
 
As part of a district -wide initiative, Pearblossom School has implemented the Positive Behavior 
Intervention and Support System in which students and staff build a culture of positive 
reinforcement for positive behaviors exhibited by students. In addition, Standards Based Report 
Cards are being used school and district -wide this year in English Language Arts, Math, Science, 
Social Studies, and PE. The change to this method of grade reporting allow s for more accurate 
representation of what students are learning with regard to state standards. 
 
The instructional program supports all student learners in that all teachers have been trained on 
the current adopted curriculum, and are able to use data to drive the instruction.

2017-18 School Accountability Report Card for Pearblossom School 

Page 1 of 10 

 

 
Pearblossom Elementary School has developed a focus on Visual and Performing Arts (VAPA). A five year VAPA plan has been developed, 
with one area of VAPA being added each year. Pearblossom Elementary School currently offers choir for grades 4-8, band for Grades 5-8, 
Cheetah Band for advanced students, classroom music for grades K-3, and Cheer for students in grades 3-8. Through the after school 
program, RISE, students are given the opportunity to participate in sports and a Folklorico dance group. 
 
The sports program is coordinated through the district and gives students the ability to participate in volleyball, basketball, soccer, and a 
track meet. The Folklorico dance group meets weekly to learn traditional Latin dance routines and give performances for the community 
periodically throughout the year. 
 
Special education services are provided through a Full Inclusion model with support of two Specialize Academic Instruction Teachers. 
Additional support is provided by the District through Speech and Language Therapists, and Adaptive Physical Education teacher, and 
School Psychologists. Services to Gifted and Talented Education (GATE) students are provided in the regular classroom through 
differentiated instruction and the development of Independent Learning Plans (ILP). 
 
English Language Learners receive English Language Development (ELD) instruction in the general education program. All grades receive 
45 minutes of ELD instruction daily. Instruction is based upon the student’s California English Language Development Test (CELDT) English 
proficiency level. Teachers incorporate differentiated questions, scaffolding and Specially Designed Academic Instruction in English 
(SDAIE) strategies throughout the day. A bilingual instructional assistant provided ELD support to English Learners. 
 
Pearblossom Elementary School focuses on Keppel School District ’s Big-Three research-based models: Effective First Instruction (EFI), 
Professional Learning Communities (PLC), and Response to Intervention (RTI). EFI strengthens the core academic program of the school. 
It includes consistent lesson planning and quality instruction based on high priority standards to ensure that the needs of all students are 
met including culturally and linguistically diverse students, historically underserved populations, English Learners, GATE, and Special 
Education students. Teachers collaborate within and across grade levels at regularly scheduled PLC meeting at least three times per month 
plus an embedded PLC one a trimester. Teachers develop common assessments, collect and analyze data, update information about the 
progress of each student in Language Arts and Math, establish SMART goals, and plan collaboratively for appropriate instruction. 
 
Pearblossom Elementary School also implements the Pyramid Response to Intervention multi-tier model for educational resource 
delivery. RTI is based on high quality classroom instruction (EFI), research-based curriculum prevention strategies, early intervention, 
universal screening, and progress monitoring. An RTI committee meets to monitor the implantation of the RTI Program and provide 
feedback to the teachers.