Lincoln Middle School is located 1501 California Avenue, one block north of Wilshire Boulevard, in 
the beautiful beach community of Santa Monica, California. We serve a diverse community of 
students enrolled in grades 6-8. The vast majority of these students arrive from our three local 
feeder elementary schools. Lincoln is recognized as one of the leading middle schools in both 
California and the nation and has received many awards, such as, California Gold Ribbon Schools, 
California Distinguished Schools, National Blue Ribbon, and Disney Spotlight School of the Year. In 
addition to a rigorous academic program, we integrate Mindfulness, Olweus and Character Counts! 
to help promote self- awareness, integrity and compassion in our student community. Lincoln has 
an award-winning music program and has three full-time music teachers: Director of Bands, 
Director of Orchestras, and Director of Choral Music. Students compete in local, state, and national 
competitions winning excellent and superior ratings. Students perform two school concerts each 
year in addition to local festivals. 
 
Our staff includes 49 credentialed teachers—including six who have received National Board 
Certification (NBC), three full-time administrators - one who was a NBC teacher, three full-time 
counselors, a full-time librarian, a full-time nurse, a full-time school psychologist, a speech 
pathologist, a bilingual community liaison, 17 paraeducators, two instructional assistants, three 
part-time music instructional assistants, three security guards, six custodians, and five office staff 
personnel. 
 
Lincoln Middle School Mission Statement 
 
Lincoln Middle School is a safe learning environment where students are supported intellectually 
and emotionally to meet their highest potential. Adults work together as role models to provide 
clear expectations for student achievement and student behavior ensuring that every student feels 
supported and successful. 
 
Principal Culpepper’s Vision 
Lincoln Middle School staff members work together to create a learning environment that is safe 
for students to explore the world and their place in it. Students are challenged to be active 
participants in their own learning; to be curious, ask questions, to admit what they don’t know and 
to accept nurturing support when needed. Our students are guided to be their best self in all 
settings. Who are you when you think no one is looking? 
 

2017-18 School Accountability Report Card for Lincoln Middle School 

Page 1 of 17