I'd like to welcome you to Perris Lake High School's Annual School Accountability Report Card. In 
accordance with Proposition 98, every school in California is required to issue an annual School 
Accountability Report Card that fulfills state and federal disclosure requirements. Parents will find 
valuable information about our academic achievement, professional staff, curricular programs, 
instructional materials, safety procedures, classroom environment, and condition of facilities. 
 
Perris Lake High School provides a warm, stimulating environment where students are actively 
involved in learning academics as well as positive values. Students receive a standards-based, 
challenging curriculum by dedicated professional staff based on the individual needs of the 
students. Ongoing evaluation of student progress and achievement helps us refine the instructional 
program so students can achieve academic proficiency. 
 
We are proud of our students, our school and the communities we serve. Here at The Lake, we 
have made a commitment to provide the best educational program possible for Perris Lake High 
School's students, and welcome any suggestions or questions you may have about the information 
contained in this report or about the school. Together, through our hard work, our students will be 
challenged to reach their maximum potential. 
 
Mr. Dean P. Hauser, Principal 
 
Mission Statement 
The mission of the entire staff at The Lake is to successfully educate all students and to help them 
develop academic and social skills while nurturing self-confidence in an atmosphere of mutual 
respect and high expectations. Our most important goal is to create the best conditions possible to 
assist your son or daughter to graduate either at The Lake or their original high school. 
 
 

----

---- 

Perris Union High School District 

155 East Fourth St. 
Perris, CA 92570 
(951) 943-6369 
www.puhsd.org 

 

District Governing Board 

Edward G. Garcia, Jr. 

Anthony T. Stafford, Sr. 

Dr. Randall Freeman 

Dr. Jose Luis Araux 

David G. Nelissen 

 

District Administration 

Grant Bennett 
Superintendent 

Candace Reines 

Deputy Superintendent of Business 

Services 

Dr. Charles Newman, Ed.D 
Assistant Superintendent 

Educational Services 

 

Kirk Skorpanich 

Assistant Superintendent 

Human Resources 

 

Joseph Williams 

Executive Director of Technology 

 

2017-18 School Accountability Report Card for Perris Lake Continuation High School 

Page 1 of 11