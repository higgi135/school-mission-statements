Maie Ellis Elementary is a TK-6th grade Dual Language School of Choice Program located in the 
Fallbrook Union Elementary School District. Dual Immersion education is an innovative, research-
based approach in which children develop the ability to speak, read, and write in English and a 
second language. Dual language programs use two languages for literacy and content instruction 
for all students. Each day all students will receive 50% of instruction in English and 50% in Spanish. 
All teachers are highly qualified and all self-contained classrooms have BCLAD credentialed 
teachers. Maie Ellis Elementary has advisory committees to help support our vision and mission 
statements through School Site Council, English Learner Advisory Committee and Parent Teacher 
Student Association. 
 
OUR GOALS: 
Bilingualism and Biliteracy: Students develop proficient levels of listening, oral, reading, and writing 
proficiency in both Spanish and English for the purpose of communicating effectively both inside 
the classroom and in the wider global community. 
Academic Excellence: Students achieve academic excellence in all subject areas as they learn 
educational content in both languages. 
Multicultural competence: 
 Students develop positive cross-cultural understanding and 
demonstrate the ability to appreciate the traditions and values of various cultures in our society 
and around the world. 
 
MISSION: 
To effectively develop a pathway for high achieving bilingual, biliterate students with positive 
cultural attitudes. 
 
VISION: 
Maie Ellis Elementary's vision is to educate our students in a multicultural learning environment. 
We will provide an effective academic approach with an additive bilingual environment in which 
our students will have greater opportunities, as a result of the acquisition of two languages. Our 
students will be well prepared, and have the pride and self-confidence to be successful in the 
competitive international community. 
 

2017-18 School Accountability Report Card for Maie Ellis Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Maie Ellis Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

26 

25 

25 

0 

0 

0 

0 

0 

0 

Fallbrook Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

245 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Maie Ellis Elementary School 

16-17 

17-18 

18-19