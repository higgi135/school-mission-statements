Mendota Junior High School is in its tenth year of existence. Previously known as McCabe Junior 
High, Mendota Junior High will continue the tradition of hard work, high expectations, and rigor. 
 
City of Mendota: 
Mendota Junior High is located in the city of Mendota, which is located about 42 miles west of 
Fresno. Mendota is a rural community with a population of roughly 10,000. The city of Mendota is 
approximately 90% Hispanic with a great majority of those people working in the farmland that 
surrounds Mendota. Mendota has seen an unemployment rate of as high as 50% which in turn 
makes many of our students socioeconomically disadvantaged. The majority of our parents are 
Spanish speaking which equates to roughly 70% of our students English Learners from year to year. 
 
Enrollment and Services: 
Mendota Junior High has an enrollment of 512 students of which 97.4% are Hispanic and 43% of 
them are English Learners. We have a total of 25 teachers, one+ Counselor, one Vice-Principal., one 
Academic Coach and 5 instructional aides. We are a school wide Title I school and also count on 
State and Federal Title III Funds. Our school has a large number of socioeconomically disadvantaged 
students which equates to 96.6% of our students qualifying for the Free/Reduced Lunch Program. 
We provide after school services through our FRESH after school program that is funded through 
the Fresno County Office of Education. This program provides a recreational, nutritional, and 
academic component. Some of the after school programs include cooking classes, gamers, and a 
mountain biking club. Our after school programs services about 250 students per day. Title III 
services are provided through a part-time teacher with students being registered a Title III class 
during the day to help support their Math and English classes. The Title III teacher uses System 44, 
Math 180, and Read 180 language acquisition computer programs to support language 
development. We received The Project Read Exemplar School during the 2016-17 school year in 
which we know must host site visitations to share how we have incorporated a school wide literacy 
initiative into all subject areas. After school academic tutoring is also provided through our Title III 
program. The Title III tutoring program is aimed at helping our newcomer students to transition 
into their core classes. Additionally, there are after school tutorial services provided by our teachers 
in all subjects. 
 
During the school day students participate in seven classes; five core classes and two electives. 
Some students who are identified as students of need based on the previous years standardized 
tests are provided intervention classes for both Math and English in lieu of elective classes. These 
intervention classes supplement the core Math and English classroom and serve as pre-teaching 
and/or re-teaching services to students in need. The math intervention classes are structured back 
to back with their regular core math class. This enables teachers to provide more one on one time 
with individual students during the guided practice portion of the lessons. All EL students are also 
scheduled with an ELD classes. These ELD classes are scheduled depending on the student's ELPAC 
level. Currently we have classes at levels 1,2, and 3. The ELD teacher coordinates with the core ELA 
teachers to develop lessons which reinforce skills learned in students core ELA class. As stated 
above, Title III classes are also scheduled for students who demonstrated a need in the are of Math 
and English.

-------- 

Mendota Unified School District 

115 McCabe Avenue 

Mendota, CA 93640-2000 

(559) 655-4942 

www.musdaztecs.com 

 

District Governing Board 

Jose Zavala 

Lupe Flores 

Alma Durazo 

Sergio Valdez 

Angelic Salinas 

Isabel Maldonado 

Adrian Perez 

 

District Administration 

Dr. Paul Lopez 
Superintendent 

Manuel Bautista 

Director of Instructional Services 

Jose Alcaide 

Chief Financial Officer 

Jose M. Ochoa 

Director 

State and Federal Programs 

 

Glen Wall 
Director 

Human Resources 

 

Dr. Jose Reyes 

Director of Special Education 

 

2017-18 School Accountability Report Card for Mendota Junior High School 

Page 1 of 8 

 

 
Students are also serviced by a full time counselor that works with our students in their academics, social, and personal life. The focus of 
our counselor is to act as a guide and mentor to assist students meet the demands of a secondary school. The counselor is available to 
all the students and routinely makes home visits. They are charged with tracking the academic success of all our students as well as being 
a safe supportive adult for our students. 
 
Goals: 
The goal of Mendota Junior High is to create successful, lifelong learners, who will realize their full potential. We will implement classes 
with a focus on rigor, and higher level student tasks. We strive to teach and leading with enthusiasm, transmitting our passion for the 
subject we teach to our students. We will create a school environment which fosters positive academic achievement. We will provide 
opportunities for students to succeed academically, adapt socially, and mature. We will rejoice in their successes and teach them to learn 
from their failures, building confidence over time. Our teachers are fully committed to the vision of our school and the responsibility that 
we carry. We will hold each other to the highest standards and accept no excuses as we strive to meet these goals.