Binkley Elementary School, a California Distinguished School, has stood for excellence in education for over 40 years. 
 
At Binkley we strive to be an academically rigorous place that students and teachers find vibrant and stimulating. We are actively 
committed to the full development of our students as human beings. We maintain a collaborative environment where staff is 
dedicated to helping all students learn, and where staff supports and respects each other professionally. We aim to be a place that all 
find supportive and where acceptance is guaranteed. 
 
The state frameworks, standards, and curriculum are the solid foundation of Binkley’s program. Teachers are trained in effective 
research-based instructional practices that are implemented in all grade levels. Daily instruction is differentiated to meet the academic 
and social emotional needs of all students. Binkley offers during and after school interventions and supports, including and English 
Learner Homework Club. 
 
In addition to a strong core program, Binkley provides an impressive music and arts program, media center (computer lab and library), 
science lab, a physical education program, and many extracurricular activities. A lively spirit day, “Camp Binkley,” kicks off each school 
year with students, staff, and many parents coming together to work in the garden, clean up the campus, participate in team-building 
games, and create classroom posters relating to our yearly theme that decorate our multi-purpose all year. Binkley also offers Fine 
Arts Day, Walk-a-Thon, Multicultural Night, Literacy, Math and Science Family Nights and many other events hosted by our Binkley 
Boosters parents club. 
 
Binkley prides itself on creating an outstanding school community where the faculty and parents work together to ensure student 
engagement and success. The open structure of the physical building caters to teachers working, planning, and sharing together as a 
professional learning community. Tucked against the foothills of Rincon Valley in a quiet residential neighborhood, Binkley is a 
wonderful place to work, play, and learn. 
 
Mission Statement 
 
The Rincon Valley Union School District pursues excellence in education through a differentiated curriculum focusing on rigor, 
relevance, and relationships. The district provides a solid foundation for every student through the development of critical thinking, 
communication, collaboration, and creativity. 
 
 
Vision Statement 
 
Rincon Valley Union School District provides all students an exemplary academic education and the foundation to become productive 
citizens of their community. 
 
 
 
 

2017-18 School Accountability Report Card for Binkley Elementary Charter School 

Page 2 of 16