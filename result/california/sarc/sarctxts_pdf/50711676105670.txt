Kirschen Elementary School is located in the southwestern section of the city of Modesto. Built in 1987 on approximately five acres, 
the school was developed to accommodate the growing population on the Westside of Modesto. The school originally was built for 
grades K-1 and had less than 400 enrolled with 12 classrooms. Kirschen changed to become a year round multitrack school in 1991-92 
with additional relocatable buildings added to accommodate over 800 K-6 students. The school is located within a low income 
neighborhood where all students are able to walk to school. It is unique in that the students represent a wide variety of ethnic and 
cultural groups. Kirschen is also adjacent to Robertson Road Elementary School. 
 
Kirschen Mission Statement: 
At Kirschen, we will create a safe intellectual environment that is conducive to optimal learning where ALL children can grow and 
achieve their personal best. Our scholars will become independent thinkers, lifelong learners, and contributing citizens.