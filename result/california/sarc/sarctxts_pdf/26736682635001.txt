Principal's Message 
 
Lee Vining High School's staff is committed to implementing an intensive, balanced, standards-based curriculum to ensure each child's 
success. This annual report card to the community will share valuable information about our instructional programs, academic 
achievements, school facilities and safety, highly qualified staff, textbooks, and finances compared to other schools and districts in the 
state. 
 
A special emphasis is placed on providing an instructional program designed to boost student confidence and increase subject matter 
proficiency. Staff and parents work together to create a learning environment that has high expectations, promotes students' 
academic and social development, teaches responsibility and pride, and models learning as a lifelong adventure. We strongly believe 
in creating a quality school culture. The schools motto is "Pride, Respect, and Hustle". We want everyone on our campus from staff to 
students to exhibit these qualities. We want students to take pride in what they do and in their school, whether it be in what goes on 
at the school or the physical plant itself. We want students to show Respect to each other, to staff, and to visitors on our campus. 
Finally, we want the students to do their best at all times whether in the classroom, in academic competitions, or on the athletic field. 
Some of our school philosophy is evident with our classroom setup. Students are given quality office desks and chairs that they can 
have some ownership of, and come to the middle of the classroom for instruction. The more ownership a student has in the school 
the more pride and respect they will show. We are excited about our school and its programs and welcome all to support our efforts. 
More than school setup, we make sure students get the help they need academically before or after school. Staff is also instrumental 
in planning extracurricular activities that are either academic or intended to bond students and staff outside the classroom. Students 
have had the opportunity to attend professional baseball and football games, attend plays and movies as a school that dealt with 
curriculum, and staff has put on overnight activities and camping trips to bring student s and staff together. This is all done to create 
a culture where students and staff work together to create a rigorous academic environment and prepare each student for life after 
graduation. 
 
Mission Statement 
 
The mission of ESUSD, a community ascending from varied pasts toward future promise, is to ensure that 
 
each student achieves academic and personal excellence, through a unique educational system distinguished 
 
by: 

student centered learning environments with no boundaries 

• 
• devoted and passionate staff 
• 
• 
• 

engaged partnerships within our communities 
technology that bridges the gaps with all communities and the world 
innovative risk in a secure environment 

School Profile 
 
Lee Vining High School is located in the northern region of Lee Vining and serves students in grades nine through twelve following a 
traditional calendar. At the beginning of the 2018-19 school year, 46 students were enrolled, including 22% in special education, 11% 
qualifying for English Language Learner support, and 55% qualifying for free or reduced price lunch. Lee Vining High School achieved 
a 2013 Academic Performance Index (API) score of 836 and met all 2013 Adequate Yearly Progress criteria. This was the last academic 
performance score given by the state. 
 
 

2017-18 School Accountability Report Card for Lee Vining High School 

Page 2 of 12