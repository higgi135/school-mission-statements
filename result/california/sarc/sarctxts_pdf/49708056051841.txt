Mark West Elementary School is located in the unincorporated area of Larkfield, just north of the city of Santa Rosa. The school serves 
approximately 440 students in grades Transitional Kindergarten through six. Students matriculate to the Santa Rosa High School 
District at Santa Rosa Middle School for grades seven and eight, and continue on to Santa Rosa High School for grades nine through 
twelve. Students also have the opportunity to matriculate to the Mark West Charter Middle School located on the Mark West campus. 
Mark West offers a comprehensive elementary school program including rigorous academic instruction in all the core subject areas. 
We also offer academic intervention for struggling readers within the school day, English Language Development, music K-3, 
instrumental and choral music as electives for 4th-6th graders, computer class for 3rd-6th graders, and weekly library for all grades. 
Mark West was proud to receive recognition as a California Distinguished School in 2010. 
 
Vision Statement 
Our students will receive an extraordinary education that will empower them to achieve their personal best, be prepared for the 
challenges of the future and become socially responsible contributing members of our community 
 
Mission Statement 
Mark West Union School District, supported by an involved community, will personalize, engage, and challenge students to reach their 
highest academic potential. We are committed to providing a rigorous and inspiring educational program enhanced with music and 
enrichment opportunities.