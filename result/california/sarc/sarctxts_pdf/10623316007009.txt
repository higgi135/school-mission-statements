Principal Message 
Our goal at Orange Center Elementary School is to provide a safe and intellectually challenging environment that will empower 
students to become innovative thinkers, creative problem solvers and inspired learners. 
 
Orange Center students and staff strive to be respectful, responsible and build positive relationships with others. High academic 
standards and responsible citizenship are the foundation of our school. It is with pride that we continue to hold these high standards. 
We are very excited to have earned the Positive Behavioral Interventions and Supports bronze medal at the culmination of the 2014-
15 school year, in addition to the silver medal in 2015-2016 and a gold medal at the end of the 2016-2017 school year. We continue 
to maintain a strong districtwide behavior intervention support plan and provide appropriate services to our students. 
 
Orange Center School District is determined to increase the academic achievement of all students, maintain highly qualified teachers, 
ensure that all students have access to standards-aligned curriculum and exposure to 21st-century skills through the use of technology, 
and to increase parent and community participation in school activities. 
 
It is our belief that all students have the right to a rigorous education in a safe and inviting school environment. I believe that it is our 
responsibility to provide students with the tools to be successful in the future. The Orange Center School District will continue to 
increase the use of technology and the implementation of the Common Core State Standards throughout the academic school year. 
It is our goal to provide meaningful opportunities to students that will assist them in developing their leadership abilities and their 
creativity. 
 
Together, the faculty has established core values that we are committed to uphold and model. We will be leaders: We will be effective 
communicators and visionaries, and we will be flexible. We will be educators: We will be knowledgeable in our craft; we will be 
professional, resourceful and caring. We will be lifelong learners: We will be attentive, responsible, engaged and enthusiastic. We will 
be community members: We will be invested, concerned and involved. 
 
We are very proud to provide excellent educational opportunities. Rigorous educational programs such as the Spanish Dual Immersion 
Program was started in kindergarten in the 2017-2018 school year and has expanded to first grade in the 2018-2019 school year. We 
look forward to expanding the program into 2nd grade in the fall of 2019. Orange Center offers an expansive after school program for 
students in grades K-8th grade, for over 180 students daily. This program provides opportunities for academic assistance, enrichment 
& physical fitness activities, competitive sports programs and the opportunity for students to eat a nutritious supper before they go 
home. In addition, Orange Center prides itself on providing intensive academic intervention for students in need of support in reading 
and mathematics. Orange Center Folklorico began in the 2016-2017 school year and continues to thrive with approximately 30 
members in 2019. Orange Center has established a mariachi elective where students have began learning to play instruments and 
perform mariachi themed music. The class has 19 students enrolled. Students play: the violin, guitar, vihuela, guitarron, or trumpet 
and learn to sing traditional mariachi songs. Other electives include: Clarinet, Leadership, Intro to Business, Three D Printing, 
Photovoice, Tinker CADS Coding, Lego/Robotics, Art, Multicultural Education, and Dance, throughout the school year. Orange Center 
is also very proud to offer 1:1 technological devices to students from TK-8th grade. 
 
We look forward to continuing to provide extensive learning opportunities and support for all Orange Center students! 
 
Together we can create a brighter tomorrow!! Go Tigers!! 
 
Respectfully, 
Terry M. Hirschfield 
Superintendent/Principal 
 

2017-18 School Accountability Report Card for Orange Center School 

Page 2 of 11 

 

Mission Statement 
Orange Center Elementary School strives to develop productive citizens who have in-depth knowledge and appreciation of the world 
around them. Our goals are to encourage students to accept responsibilities as members of society, to promote and create a lifelong 
enjoyment of learning, to meet the needs of the individual students, rather than just the individual class, and to produce students 
capable of creative and imaginative thought.