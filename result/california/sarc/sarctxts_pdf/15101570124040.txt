Located in the rural community of Arvin in California's Central Valley, Grimmway Academy is a K-8 charter school that is redefining the 
achievement gap through its innovative blended learning model of instruction. Combining best practices in education with wellness 
and healthy lifestyle principles, students learn life skills that will support them on their path toward lifelong achievement and success 
- in whatever they choose to do. Grimmway Academy is a public school: we accept all students, tuition is free, students take the 
same tests, and our teachers have the same accreditations. Our advantage lies in our flexibility and autonomy to build an academic 
model that addresses the distinctive needs of our community. 
 
Our Mission is to close the achievement gap for students in the rural areas of Kern County by creating an environment for student 
excellence and well-being. Our Vision is for Grimmway Academy to transform the education landscape for students in rural areas by 
providing a model of excellence and innovation leading to college readiness and lifelong success. 
 
The GA model is based is based on a blended learning approach, where concepts from the core curriculum are reinforced across a 
variety of different learning experiences. From fractions to history, to the life cycle of insects and plants, students learn concepts from 
their teachers in the classroom, experience them first-hand in the edible schoolyard and kitchen classroom, and work independently, 
at their own pace, using the latest technology in the school's Learning Labs. As part of our edible schoolyard program, students learn 
to grow and prepare healthy foods, while also developing life-enhancing skills, such as cooperation, collaboration, personal expression, 
and environmental stewardship. We expect great things for our students, parents, teachers, and staff, and strive to ensure a 
supportive learning environment for everyone. Small class sizes help to ensure that our highly skilled teachers can focus on the 
individual needs of each Grimmway Academy student. 
 
Students receive two fresh and seasonal meals each day, prepared by our Chefs and nutrition experts in the Grimmway Cafe, where 
they also learn healthy eating habits and develop positive attitudes to trying new foods. Grimmway Academy also offers an afterschool 
enrichment program where art, music, homework assistance, crafts, and soccer clinics are available to all students.