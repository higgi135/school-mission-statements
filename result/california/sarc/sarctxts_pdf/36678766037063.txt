Highland-Pacific Elementary School has 23 classrooms, a multipurpose room, and an administration office. The campus was built in 
1957 and modernized in 2012, providing sufficient space for instruction. The facility strongly supports quality first teaching in the 
classrooms and language arts support for struggling students through the learning center. 
 
Highland-Pacific Elementary School is dedicated to nurturing the academic, social, and cultural potential of each child, making possible 
the development of socially responsible citizens.