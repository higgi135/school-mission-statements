When Hanford High School opened its doors in August of 1892, it did so with the promise of new and innovative approaches to 
education that would empower all students to reach their highest potential. We continue that today with our goal to build a learning 
community dedicated to the development of students who are critical thinkers, effective communicators and responsible citizens. We 
strive to ensure that all students are actively engaged in the pursuit of knowledge and are respectful, capable problem solvers who 
demonstrate integrity, enabling them to become productive members of our school, community and world. This mission statement 
was designed and agreed upon by the high school community by reevaluating our core beliefs and desired outcomes for student 
success. The high school's curricular emphasis has both a solid foundation in the depth and breadth of California State Standards, 
while maintaining particular attention to the various learning styles and needs represented by all students. 
 
We also provide students with opportunities to excel in the areas of special interests. In addition to the array of course offerings that 
meet the A-G University of California/California State University requirements and State of California requirements, the school offers 
specialized course offerings. Examples include Career Technical Educational courses, local community college courses, English 
Language Development courses, Literacy support classes, Partnership Program, American Sign Language, music, drama, fine arts, 
agriculture, and technology classes. Enrollment at HHS has shown a slow but steady growth pattern. 
 
The administrative team consists of the Principal, Assistant Principal, and two Learning Directors, each having designated areas of 
particular focus with Student Services, Counseling, Attendance, Athletics, and Discipline. We have a strong administrative support 
team. Currently, 5 counselors each have an emphasis on monitoring students deemed at risk, and one counselor is responsible for 
monitoring our English Learner (EL) and Migrant student populations. The school has the support of a School Psychologist, five 
interpreters, a librarian, a library technician, a registrar and a site testing secretary. Both classified and certificated staff members are 
involved in ongoing in-service opportunities aimed at professional growth. Hanford High utilizes Wednesdays (designated as School 
Improvement Planning, or SIP, days) as days for in-service training, with students arriving for classes at 8:55 a.m., allowing for 90 
minutes of time for teacher collaboration. In addition, we have instituted our BullpUP period which is designated as an intervention 
period for all students to access help in needed areas. 
 
Our Mission Statement is:The Hanford High community is committed to Integrity, Knowledge, and Respect for every person every day.