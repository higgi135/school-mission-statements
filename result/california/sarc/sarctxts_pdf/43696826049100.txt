Welcome to Saratoga Elementary School! Having been a center of public education for the Saratoga 
community since 1854, Saratoga Elementary School is the oldest school site in California. While we 
are proud of our tradition of educating multiple generations of children, we also strive to be a 
school that nurtures 21st Century learning in a warm and caring environment that meets the needs 
of the whole child. 
 
We do this by providing rigorous instruction aligned with California Common Core State Standards 
and curriculum that fosters critical thinking, creativity, communication, and collaborative skills in 
our students. The Saratoga Educational Foundation, PTA, School Site Council, and Saratoga Alumni 
Association are critical parent and community partners providing support for enrichment 
curriculum that includes hands-on science labs, fine arts and physical education instruction, library, 
centers for small group instruction, reading intervention programs, character education, and 
student support services. We work with Project Cornerstone to provide students with social-
emotional learning opportunities that support the development of community responsibility and 
conflict resolution skills. Through service learning and project-based learning activities students 
connect classroom instruction with activities that deepen our connection to the Saratoga 
community and build their identity as global citizens. 
 
This year we are converting our computer lab into a Makerspace. By providing students with 
Chromebook carts, we've been able to move most computer learning into the classroom. Our 
Makerspace provides students the opportunity to apply concepts taught in the classroom in a 
hands-on setting. We are also growing workshop model-based reading instruction through Daily 
5/CAFE and are continuing to implement Lucy Calkins writing curriculum across all grades. We are 
also continuing implementing EngageNY math curriculum to support students in becoming 
mathematicians with fluency in multiple approaches to problem solving, in addition to developing 
fundamental skills in operational mathematics. We are implementing Next Generation Science 
Standards (NGSS) across K-5 grades. Students in 4th and 5th grades are working with STEMScopes 
to access NGSS. 
 
Finally, we are grateful for our enthusiastic and involved parent community. Whether its 
participating in the PTA, volunteering for a field trip or school event, supporting students in our 
community garden, or simply being an advocate for your student, I encourage you to get involved 
in whatever way works best for you and your family. We see our parents as essential partners, 
collaborators, and a valued presence at our school. 
 
Please know I am here to support you and your child’s education. Feel free to contact me with any 
questions, concerns, or ideas to share. 
 
SUSD Mission and Vision Statement 
The mission of SUSD is to create an innovative public school system that stimulates intellectual 
curiosity, providing academic rigor for each and every learner, and instills leadership, responsibility, 
and global citizenship in a safe and nurturing environment where learners THRIVE. We accomplish 
this with a highly professional and differentiated system of education, which engages the 
community as educational partners, embraces diversity, inspires creativity, and fosters students 
well-being. We measure success in student outcomes and achievement, professional growth, and 
a commitment to continuous improvement. 

2017-18 School Accountability Report Card for Saratoga Elementary School 

Page 1 of 12 

 

We define: 
 
INNOVATION as a new way of doing things that is transformation, original, and creative so it inspires others to learn. 
 
STUDENT WELL-BEING as fostering a positive physical, social, and emotional learning environment to allow students to thrive, flourish, 
and learn. 
 
PROFESSIONAL DEVELOPMENT as engaging in learning opportunities to grow professionally so that it affects continuous improvement 
and refinement of learning, teaching, and processes. 
 
COMMUNITY as engaging the community to build ongoing, permanent relationships so that a common vision is shared and implemented. 
 
ACADEMICS as supporting differentiated instruction where students need it; teachers inspire change in curriculum and methods of 
delivery. 
 
Saratoga Elementary Mission Statement 
 
The mission of Saratoga Elementary School is to create an environment which inspires and supports all children in becoming global citizens 
with a passion for lifelong learning. To educate students to their fullest potential, our District Strategic Plan includes three components; 
academic success for all students, providing a balanced curriculum, and the building of caring, responsible students. The goals of Saratoga 
Union School District are to create an innovative public school system that stimulates intellectual curiosity, providing academic rigor for 
each and every learner, and instills leadership, responsibility, and global citizenship in a safe and nurturing environment where learners 
thrive. We accomplish this with a highly professional and differentiated system of education, which engages the community as educational 
partners, embraces diversity, promotes civic responsibility, inspires creativity, and fosters student well-being. We measure success in 
student outcomes and achievement, professional growth, and a commitment to continuous improvement. Our vision recognizes the 
development of the whole child in the three areas of academic success, balanced curriculum, and caring, responsible citizens. 
 
Our vision at Saratoga Elementary School supports our firm belief that our role is to support all students to achieve their highest potential. 
Our first goal is Academic Success. Our teachers work on district and site teams to collaborate on goals, curriculum planning and 
development, and address the educational needs of all children. The second dimension, a Balanced Curriculum, is achieved through the 
working partnership of our school district and parent community. Partner organizations like the Saratoga Education Foundation, Parent-
Teacher Association, School Site Council, and the Saratoga Alumni Association provide support for our District enrichment curriculum that 
includes hands-on science labs, visual and performing arts, physical education instruction, library, character education, and student 
support services. The third dimension is Building Caring, Responsible Citizens. Our work with Project Cornerstone asset-building 
curriculum and philosophies is deepening through the campus. Project Cornerstone combines with our school’s Service Learning 
Committee to provide numerous enriching activities for our students that seek to deepen our connections with each other, with our 
Saratoga community, and our world. 
 
School Profile 
 
At Saratoga Elementary School, students, parents, staff, and our community work together to ensure each student receives a challenging, 
balanced, and integrated educational program in a safe and caring environment. Instruction across content areas build in our students 
critical thinking, creativity, problem solving, and communication skills. Professional development and collaboration provide staff 
opportunities to grow and reflect upon teaching practices as we strive to move forward in meeting the needs of our students in an ever-
changing world. Teachers use student data to assess instructional practice, student progress, and guide instruction. As a learning 
community, we share ideas and concerns honestly, communicate regularly with parents and community, and model a lifelong joy of 
learning through our own personal and professional growth. 
 
 

 

2017-18 School Accountability Report Card for Saratoga Elementary School 

Page 2 of 12