The Yuba County Office of Education provides a full range of educational services to meet the needs of students in the Juvenile Hall 
facilities. H.P.B. Carden School provides an alternative learning program for incarcerated students. T he primary goal of the school is 
to help students succeed academically and successfully return to public school upon their release. We focus on creating a stimulating 
learning atmosphere while remediating credit deficiencies and mitigating existing learning issues. Teacher directed learning, group 
and individual assignments, oral recitation, Health classes, computer classes, and arts are infused in to thematic lessons. Teachers use 
a variety of instructional strategies to meet the needs of all students. Special Education services are provided by classroom teachers 
under the guidance of a Resource Teacher w ho also works individually with students. One key aspect that we have addressed is the 
use of our Intervention Specialist to help bring extra support to the students that are several grade levels behind in Math, English or 
any subject. They also bring curriculum to students that may be on a modified program and unable to attend class. All students are 
tested w hen enrolled and placed in the appropriate level of work. Subsequent testing is administered every six weeks and recorded 
to help direct teachers and services in our LCAP. Students are offered the opportunity to make up credits. During our intervention 
time, students can either use textbooks or our online Odysseyware system to regain credits. Care is taken to immediately obtain 
transcripts from other schools and ensure that completed transcripts are forwarded immediately upon a student’s release. Transcript 
evaluations are completed within one w eek for all students in grades 10-12 to ensure placement in appropriate subjects. 
 
Mission Statement: 
“To inspire our students to achieve educationally, socially, and technologically.” 
 
Vision Statement: 
“To accept our students as they come and build on strengths, strengthen our weakness, holding everyone to school wide high 
standards. Learning is not an option but a way to achieve present and future successes."