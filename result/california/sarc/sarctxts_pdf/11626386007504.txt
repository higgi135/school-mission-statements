Plaza Elementary is a small rural K-8 school in Glenn County near the city of Orland. It is a single 
school district staffed by nine credentialed teachers and a full time superintendent/principal. 
Glenn County Office of Education (GCOE) provides direct support to the district in special eduction, 
speech, psychologist and health services. GCOE also maintains a severe services program in a 
separate facility on campus. 
 
There were 213 students in grades TK through eighth grade, all of which are single grade classes. 
In addition to the credentialed staff there are four paraprofessionals, a business manager, office 
clerk, cafeteria manager, cafeteria aide, bus driver/custodian, and two after school activity 
assistants. 
 
Mission Statement- To provide a lifelong love of learning through a positive and supportive school 
climate that provides the opportunity for all students to achieve their full educational and social 
potential. School staff, parents, and community members provide support that encourages high 
expectations of all students. 
 

2017-18 School Accountability Report Card for Plaza Elementary 

Page 1 of 7 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Plaza Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

9 

0 

0 

11 

10 

0 

0 

 

0 

Plaza Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

10 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Plaza Elementary 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.