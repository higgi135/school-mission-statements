Canyon Springs High School, located east of Riverside in the city of Moreno Valley, is one of four 
comprehensive grade 9-12 high schools in the Moreno Valley Unified School District. Canyon 
Springs High School offers a rigorous academic environment focused on achieving college and 
career readiness for all students. With three California Partnership Academies (Health Careers 
Academy, Business Academy and Academy of Creative Technologies), sixteen Advanced Placement 
courses, a thriving Advancement Via Individual Determination (AVID) program, emphasis on 
academics is always at the forefront. This year marks the first year of implementation for the 
International Baccalaureate program, furthering the goal of academic excellence. Nearly all 
courses offered at Canyon Springs High School are a-g approved through the University of California 
system. These courses are academically challenging and involve substantial reading and writing in 
various courses and subject matter, must include problems and laboratory work as appropriate and 
must show serious attention to analytical thinking as well as factual content. 
 
Canyon Springs High School has received several awards and honors in recent years. The Business 
Academy received a Lighthouse designation in 2014, naming it one of the top 15 model programs 
in the state of California. The Health Careers Academy earned a Golden Bell Award in 2013. A 
related program, the Charter Hospice club, earned a Golden Bell Award in 2014 for it's exemplary 
volunteer work, supporting hospice patients in the end stages of life. The Girls' Basketball team 
won the Division I state championship in 2014. Freshman Seminar, a program for at-risk 9th 
graders, won the Model of Academic Excellence and Innovation award in 2016. In 2017, the Health 
Careers Academy was named by the California Department of Education as a Distinguished 
Academy. Adding to the accolades, the CyberPatriot program received the Model of Academic 
Excellence and Innovation award in 2018. The school received a Bronze designation in US News and 
World Report's list of 2018 Best High Schools, and received a Silver award from the California PBIS 
Coalition for evidence-based interventions. 
 
Vision Statement: 
The Canyon Springs High School Community is dedicated to ensuring that our students display 
integrity, act responsibly, collaborate effectively, think critically, and demonstrate strength of 
character. Our Cougar values develop high school graduates who are thoroughly prepared to 
become civic-minded, responsible global citizens. 
 
Mission Statement: 
The Mission of Canyon Springs High School is to cultivate lifelong learners in supportive and 
challenging educational environments, which promote the development of the skills necessary to 
empower students in post-secondary education and/or viable career paths. 
 
School Wide Learning Outcomes: 
Canyon Springs will develop high school graduates who: 
Employ critical thinking skills 
Collaborate effectively with peers and staff 
Demonstrate the Cougar values of responsibility, integrity, and grit 
Become global and civic-minded citizens 
Utilize technology effectively as responsible digital citizens 
 
Cougar Values: 
Responsibility, Integrity, Grit 

2017-18 School Accountability Report Card for Canyon Springs High School 

Page 1 of 16 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Canyon Springs High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

99 

79 

101 

0 

0 

0 

0 

2 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Canyon Springs High School 

16-17 

17-18 

18-19