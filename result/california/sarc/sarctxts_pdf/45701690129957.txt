Northern Summit Academy is a unique charter school that relies on parent participation in conjunction with independent study 
opportunities for students. There are approximately 185 students enrolled in this charter school. Northern Summit Academy envisions 
enabling students to move through compulsory K-12 education into post high school opportunities with the confidence that their 
education, personal well being and contributions as members of a global society are valuable and personally controllable. Students 
need to be nurtured into taking responsibility for creating a fulfilling, healthy life. In order to do this, young people need support in 
the form of personalized instruction, up to date resources, and healthy adult and peer relationships and activities. These supports will 
help develop healthy personal habits, collaborative work and social skills, and career goals and skills. Northern Summit Academy is 
committed to equipping the students to meet the complex challenges of the 21st century. It is imperative that these habits and skills 
be seen as having value and relevancy on a personal and community level. 
 
The collaborative support system at Northern Summit Academy enables each student, parent/guardian, staff member and involved 
community member to contribute to developing each student as a valuable individual and a valuable, contributing community 
member. In addition, this collaboration provides opportunities for parent/guardians, staff members, and community members to 
realize their own value as individuals and contributing members of our educational system. 
 
Vision 
Northern Summit Academy supports students to complete their K-12 education and enter into post high school pursuits with the 
confidence that their education, personal wellbeing and contributions as members of a global society are valuable and personally 
controllable. 
 
Mission 
The mission of Northern Summit Academy is to “provide a personalized approach to providing a standards-based education and career 
awareness and preparation to students who need the flexibility of independent study combined with the support of classroom 
teachers, classes, and an academic environment.” 
 
 
 

2017-18 School Accountability Report Card for Northern Summit Academy 

Page 2 of 12