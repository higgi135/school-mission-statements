The Charger family is an inclusive and diverse community that provides a rigorous, innovative, and 
accessible curriculum in a supportive environment. El Toro's mission is to prepare all students to 
succeed in college and career and to contribute positively to our changing global society. 
 
Student Learning Outcomes 
Wellness: ET students will set goals and take appropriate steps toward a healthier, happier, more 
balanced life. 
 
Resiliency: ET students will develop growth mindset and take ownership over learning. They will 
exhibit competence, a sense of belonging, and usefulness as they persevere confidently. 
 
Creative and Critical Thinking: ET students will apply critical thinking skills to resolve challenges in 
creative ways. They will question and apply critical reasoning to real world situations, as well as 
employ higher level thinking skills such as analysis, synthesis, imagination, application, and 
evaluation to become effective and innovative problem finders and solvers in a diverse society. 
 
Collaboration: ET students will demonstrate the ability to be productive members of diverse teams 
through strong interpersonal communication, a commitment to shared success, leadership, and 
initiative. 
 
Civic Responsibility and Connectivity : ET students will contribute their time and talents to improve 
the quality of life for others, value diversity and remain culturally sensitive. As open-minded and 
empathetic citizens, they will be proactive and seek to be positive agents of change. 
 
Communication: ET students will demonstrate competence in using multiple methods to convey 
information, apply active listening, speaking, reading and writing strategies across disciplines, and 
be technologically literate, accessing and applying information to real-world situations. 
 
 
 
 
 
 

----

-

--- 

Saddleback Valley Unified School 

District 

25631 Peter A. Hartman Way 

Mission Viejo CA, 92691 

(949) 586-1234 
www.svusd.org 

 

District Governing Board 

Suzie Swartz, President 

Dr. Edward Wong, Vice President 

Amanda Morrell, Clerk 

Barbara Schulman, Member 

Greg Kunath, Member 

 

District Administration 

Crystal Turner, Ed D. 

Superintendent 

Dr. Terry Stanfill 

Assistant Superintendent, Human 

Resources 

Connie Cavanaugh 

Assistant Superintendent, Business 

Laura Ott 

Assistant Superintendent, 

Educational Services 

Mark Perez 

Director, Communications and 

Administrative Services 

Dr. Ron Pirayoff 

Director, Secondary Education 

Liza Zielasko 

Director, Elementary Education 

Dr. Diane Clark 

Director, Special Education 

Rae Lynn Nelson 
Director, SELPA 

Dr. Francis Dizon 

Director, Student Services 

 

2017-18 School Accountability Report Card for El Toro High School 

Page 1 of 14 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

El Toro High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

100 

99 

103 

0 

0 

0 

0 

0 

0 

Saddleback Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1,157 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

El Toro High School 

16-17 

17-18 

18-19