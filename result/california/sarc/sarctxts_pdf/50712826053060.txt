WELCOME TO "THE HOME OF THE CHAMPION UNIVERSITY-BOUND SCHOLARS!" 
Josephine Chrysler Elementary School is considered our community's second home and together we form the Chrysler 
Family. Chrysler is the home of six hundred eighty "Champion University-Bound Scholars" (C.U.B.S.) in grades Transitional 
Kindergarten (TK) through sixth grade. Our family consists of beautifully diverse parents, eager scholars, highly qualified teachers 
and Para-Professionals, dedicated office managers, hardworking custodial team members, progressive administrative leaders at the 
school site and at the district level who, hold the utmost respect for all of our Chrysler family members, have the highest level of 
expectations for every single one of our scholars, and provide the highest quality of service to our community. 
 
JOSEPHINE CHRYSLER ELEMENTARY SCHOOL'S MISSION STATEMENT 
"The mission of Team Chrysler is to educate, support, and motivate OUR community by providing equitable access for ALL scholars 
through effective, on-going communication, quality instruction, and providing a safe learning environment." 
 
STANISLAUS UNION ELEMENTARY SCHOOL DISTRICT'S (SUSD) MISSION STATEMENT 
The mission of the Stanislaus Union Elementary School District as the premier district of choice in partnership with the community is 
to ensure the ultimate educational experience for all children. 
 
JOSEPHINE CHRYSLER ELEMENTARY SCHOOL'S 2017-2017 SCHOOL PLAN FOR STUDENT ACHIEVEMENT (SPSA) AND STANISLAUS 
UNION ELEMENTARY SCHOOL DISTRICT'S 2017-2020 LOCAL CONTROL ACCOUNTABILITY PLAN (LCAP) GOALS 
Goal #1: Provide a guaranteed and viable curriculum using daily effective instruction and monitoring, including opportunities for a 
broad course of study, interventions, and enrichment by highly qualified teachers to ensure each student, and all subgroups, 
demonstrate success with the full implementation of Common Core academic content standards. 
 
Goal #2: Expand opportunities to increase parental involvement, collaboration, and partnerships with families and the larger 
community to support the district initiatives. 
 
Goal #3: Provide education and supports to promote character traits and a healthy lifestyle necessary for students' academic and 
social success by maintaining proactive measures that include safe and welcoming campuses that promote a positive and productive 
learning environment, strong attendance, meaningful relationships, and shared accountability. 
 All three goals for SUSD and Josephine Chrysler are available to access and review at each respective website address. 
 
ACCESS TO REVIEW AND PROVIDE FEEDBACK ON THREE GOALS 
 Josephine Chrysler Elementary School's 2018-2019 and previous school year's detailed SPSAs can be found on our school website at 
https://www.stanunion.k12.ca.us/CE under the "SARC/SPSA" tab and on the California Department of Education website. The SPSA 
goals are monitored and adjusted to meet the needs of our scholars. If you have any feedback or would like to take part in this 
process please attend a SSC and/or ELAC meeting. You are also welcome to call or visit our office to make an appointment to meet 
with the Principal at any time. 
 The Josephine Chrysler SPSA was reviewed by all stakeholders and was approved by Josephine Chrysler's School Site Council (SSC) 
on October 23, 2018. 
 
 Stanislaus Union Elementary School District's 2017-2020 detailed LCAP Plan and how it will reach each goal is located at 
https://www.stanunion.k12.ca.us/ by clicking on the "Local Control and Accountability Plan (LCAP) 2017-2020 Plan Summary" 
link. There is also a link for families to provide feedback by clicking on the "LCAP Public Feedback Form" link. 
 

2017-18 School Accountability Report Card for Josephine Chrysler Elementary School 

Page 2 of 14