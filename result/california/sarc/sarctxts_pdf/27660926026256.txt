Welcome to JC Crumpton Elementary School. JC Crumpton Elementary School is a transitional 
kindergarten through fifth grade school with 415 students. Starting in the 2017-18 school year, 
Crumpton is a pilot site for the STEM (Science, Technology, Engineering and Math) program. 
Students are receiving high quality instruction with integrated STEM components throughout the 
school day. It is located in the diverse community of Marina, California. The school has a wonderful 
family tradition with many of our current students’ parents having attended JC Crumpton as 
children. Our highly qualified staff provides each student with a quality education aligned to 
rigorous standards and behavior expectations. 
 
JC Crumpton is a multicultural and diverse learning community that prepares each student for 
academic, social, and personal success by providing a safe, supportive, challenging, and meaningful 
environment. We are committed to providing the very best that education has to offer to all 
students at Crumpton! Community is paramount at Crumpton, and our active PTA supports many 
school-wide events for our families to involve all our students and families in fun community 
activities. For example, we have a family-oriented Terrifying Taco fall festival prior to Halloween 
which is always well attended. We greet new families with a kindergarten BBQ and welcome 
orientation prior to the start of each new school year and last year we had a Lap-A Thon which 
proved to be a successful fundraiser for the school. 
 
In addition to our regular education students, we house a Language Enrichment Academic 
Performance (LEAP) program and a Special Day Class (SDC) program. These programs, overseen by 
our Special Services Department, supports students of special needs— particularly those who have 
a communication disorder frequently identified on the autism spectrum. 
 
We include all our students in school-wide activities and assemblies. Autism Spectrum Disorder 
students also participate in our general education program as determined by an Individual 
Education Plan (IEP). 
 
JC Crumpton has a hard-working PTA which sponsors many activities, including class field trips 
throughout the year. 
 
At Crumpton Elementary School, our mission is to provide the highest quality education in a secure, 
positive, and challenging environment for all students fostered by a cooperative effort between 
school and community. We aim for an atmosphere of cooperation with respect for individual 
differences and community values. 
 
Our vision is to create a challenging learning environment that encourages high expectations for 
success through the development of appropriate differentiated instruction that allows for 
individual differences and learning styles. All learners are encouraged to be self-sufficient, 
responsible, cooperative and caring members of our community. 
 
 
Sarah Hudson 
Principal 
 
 

2017-18 School Accountability Report Card for J.C. Crumpton Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

J.C. Crumpton Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

24 

21 

19 

0 

0 

1 

0 

2 

0 

Monterey Peninsula Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

441 

40 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.