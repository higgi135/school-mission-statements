MISSION: To develop a well-rounded child who is proficient in all academic areas, including the English language and who has an 
appreciation for diversity, a positive self-image, and an ability to incorporate respect, perseverance, and resiliency into their everyday 
lives. 
 
VISION: Central School, a Professional Learning Community, is dedicated to providing a standards-based curriculum that guarantees 
in-depth experiences and develops each child's social and academic potential; as they prepare for their future. 
 
MISSION STATEMENT: Central Elementary provides a safe and orderly environment, academic programs based on high standards for 
students' learning, and expectation for our students to become contributing, successful citizens of the twenty-first century. 
 
SCHOOL OVERVIEW 
Central Elementary School is one of 11 schools in the South Bay Union School District. Central currently serves approximately 510 
students in grades kindergarten through six. Central Elementary currently operates as a traditional elementary program that includes 
literacy, math, science and social studies as elements of daily instruction. Central also has a full-time VAPA/STEM instructor and all 
students receive one hour of instruction a week in either the arts or STEM. In addition to its general education staff, Central provides 
for a FT speech; four FT education specialists; a part time literacy interventionist, one psychologist; a part time nurse and FT IMRT 
(Instructional Media Resource Technician). 
 
Additionally, students in grades 3-6 have 1:1 access to instructional technology, and students in K-2 have access to two mobile 
computer labs and classroom chromebook workstations. All instructional classrooms at Central have Promethean interactive 
whiteboard systems. 
 
Central offers before and after school intervention and tutoring as well as multiple opportunities for arts and academic enrichment 
including choir, visual arts, gardening and STEAM clubs.