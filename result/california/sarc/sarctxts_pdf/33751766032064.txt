Superintendent’s message 
LEUSD is well positioned for the 2018 school year! The collaboration between voters, parents, 
teachers and staff has resulted in student achievement growth, improved facilities and playing 
fields under Measure V, and new instructional technology for classrooms. 
 
Under our state accountability system, the Fall 2017 update to the California School Dashboard was 
recently released. The Dashboard provides teachers and principals with valuable performance data 
that is examined weekly during their PLC collaboration time, and used to guide instruction. The 
current Dashboard shows LEUSD schools are making progress. 
 
LEUSD improved in several areas, though English Language Arts and Math growth indicators are 
little changed from a year ago, a trend statewide. The Dashboard is powered by six state indicators 
and four local indicators, along with a variety of custom reports by which to compare schools, 
districts, and student subgroups. Dashboard color codes reflect status and change to depict 
achievement growth ranging from Red (lowest) to Blue (highest). 
 
LEUSD performance highlights: 
SUSPENSION RATES—by lowering suspensions, results for the 'Suspension' indicator have 
IMPROVED from orange to yellow, changing from a high suspension rating to a medium suspension 
rating. 
‘EL’ PROGRESS—English Learners IMPROVED from yellow to green, changing from 'Medium' to 
'High' as a result of an additional 3.1% students making progress towards English proficiency. 
GRADUATION RATE—this indicator has IMPROVED from green to blue. The District continues to 
have a 'High' rating due to an increase in graduating students of 1.5%. 
COLLEGE/CAREER PREPAREDNESS—growing College & Career Preparedness is an area for 
increased attention. The CA School Dashboard shows 35.2% of LEUSD graduates as being 
'Prepared.” The State will not have a color indicator for College & Career Preparedness until 2018, 
but notably, LEUSD 11th grade students' ELA and Mathematics scaled scores increased in both 
areas respectively by 0.3 points and 4.2 points, a positive college readiness indicator. 
CHRONIC ABSENTEEISM—for the first time, the CA Schools Dashboard includes District and school 
Chronic Absenteeism rates, though a Chronic Absenteeism color indicator does not appear on the 
Fall 2017 report. District wide, LEUSD’s Chronically Absent statistic is 12.8%. 
LEUSD met all local indicators for implementing state standards, providing safe school facilities, 
adequate books and instructional materials, as well as meeting indicators for school climate, and 
student and parent engagement. View how LEUSD is performing at www.caschooldashboard.org. 
These are positive indicators, so let’s be mindful of the many positive accomplishments of 2017 to 
help set the bar high for 2018. 
 
Sincerely, 
Dr. Doug Kimberly, 
Superintendent 
 

----

--

-- 

Lake Elsinore Unified School 

District 

545 Chaney St. 

Lake Elsinore, CA 92530 

(951) 253-7000 

www.leusd.k12.ca.us 

 

District Governing Board 

Stan Crippen, Trustee Area 1 

Susan E. Scott, Trustee Area 2 

Heidi Matthies Dodd, Trustee Area 

3 

Juan I. Saucedo, Trustee Area 4 

Christopher J. McDonald, Trustee 

Area 5 

 

District Administration 

Dr. Doug Kimberly 
Superintendent 

Dr. Gregory J. Bowers 

Assistant Superintendent 

Dr. Alain Guevara 

Assistant Superintendent 

Dr. Kip Meyer 

Assistant Superintendent 

Arleen Sanchez 

Chief Business Officer 

Tracy Sepulveda 

Assistant Superintendent 

Sam Wensel 

Executive Director 

 

2017-18 School Accountability Report Card for Wildomar Elementary School 

Page 1 of 11 

 

• 

• 

 

 
Principal’s Message 
Wildomar Elementary School was originally constructed in 1886 and is located in the southern portion of the Lake Elsinore Unified School 
District in the incorporated city of Wildomar. It is one of 24 schools in the District. The Wildomar staff works diligently to make learning 
engaging while meeting the needs of the diverse student body. The current student population is approximately 6 8 0 students enrolled 
in grades TK-5. 
 
For the 2018-2019 school year, we continue to progress in Integrating new teaching practices into successful, foundational practices that 
a part of the culture of our school. We have 4 main focuses that drive our development as a staff: 1) Maintaining and building upon a 
school-wide social skills program to ensure that all students grow to be motivated to succeed while fostering empathy and empowerment, 
2) Developing sound educational practices in conjunction with a Multi-Tiered Student Support structure that will meet the diverse 
academic and emotional needs of our students, 3) Continuing to grow our collective capacity as educators through book studies, 
professional development and collaboration in our established professional learning communities, and 4) Leveraging our resources and 
relationships to increase our partnership with the surrounding community. 
 
The Wildomar staff is dedicated to providing a safe, nurturing, and challenging environment that promotes high academic achievement 
for our diverse student population. Wildomar Elementary has been recognized as a Distinguished School. 
 
School Mission Statement 
The Wildomar Staff’s mission is to foster each child's full academic potential, build each child's self-esteem and provide opportunities for 
each child to grow culturally with recognition of individual differences. It is our hope and expectation that every child who enters the 
gates will be impacted in a positive way. Wildomar Elementary staff believes in empowering each child to become responsible, respectful, 
and safe on their journey to becoming contributing citizens. This is consistent with our school theme for the 2018-2019 school year, ‘Be 
the HERO of your own story.” 
 
School Vision Statement 
The Wildomar School vision is to encourage every child to pursue academic excellence with honor and integrity. It is also to support the 
District's vision and strategic plan for the coming year.