Arizona Middle School is located in the eastern region of Riverside and serves students in grades 
six through eight following a traditional year. 
 
Arizona Middle School (AMS) is committed to implementing an equitable and rigorous curriculum 
to ensure that "All students realize their unlimited potential." AMS is distinguished by a legacy of 
greatness. Throughout the school's existence we have been recognized as a California Distinguished 
School by the California Department of Education; an award earned based on the academic 
achievement of our students and effective instructional practices that support student learning. In 
addition, AMS has received certification as an AVID (Advancement Via Individual Determination) 
National Demonstration School, for five consecutive years and an AVID Schoolwide Site of 
Distinction. 
 
We are committed to assuring that every student is prepared with a world-class education that will 
support lifelong success. Beyond our core academic program of English, Math, Science, History, and 
Physical Education we support lifelong success through elective courses in Art and Robotics/3D 
Graphic Design. Similarly, our co-curricular programs (ASB, band, clubs, and after school athletic 
teams) provide students with challenging activities where each child can reach her/his fullest 
potential. 
 
At Arizona Middle School we are a caring community that emphasizes support for all of our 
students. Students have access to 2.5 counselors and support programs to assist them in meeting 
their academic, social, and emotional needs. Our Apache community works together to create a 
learning environment that has high expectations, promotes students' academic and social 
development, teaches responsibility and pride, and supports our students in developing their 
abilities to be lifelong learners. We are excited about our school community and welcome your 
support! 
 
Mission Statement 
Alvord Unified School District, a dynamic learning community that embraces innovation, exists to 
ensure all students attain lifelong success through a system distinguished by: 

• Active and inclusive partnerships 
• Relationships that foster a culture of trust and integrity 
• High expectations and equitable learning opportunities for all 
• A mindset that promotes continuous improvement 
• Multiple opportunities for exploration and creativity 
• Professional development that promotes quality teaching and learning 
• Access to learning experiences that promote a high quality of life 

 
In addition, all schools strive to attain the Alvord vision that all students will realize their unlimited 
potential. 
 

2017-18 School Accountability Report Card for Arizona Middle School 

Page 1 of 10