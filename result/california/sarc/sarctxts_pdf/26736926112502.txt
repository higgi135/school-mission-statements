About the School Accountability Report Card (SARC) 
Since November 1988, state law has required all public schools receiving state funding to prepare and distribute a SARC. A similar 
requirement is also contained in the federal Elementary and Secondary Education Act (ESEA). The purpose of the report card is to 
provide parents and the community with important information about each public school. A SARC can be an effective way for a school 
to report on its progress in achieving goals. The public may also use a SARC to evaluate and compare schools on a variety of indicators. 
 
Mission Statement 
The mission of Mammoth Middle School is to inspire, educate, and empower our community’s future leaders by providing a healthy, 
safe, and respectful environment which fosters the students’ academic, social, and emotional growth.Our school has a tradition of 
academic excellence produced in a caring and safe environment. 
 
Principal's Message 
At Mammoth Middle School, we are committed to supporting your children in their intellectual and social development during their 
early adolescent years. The content of our classes focuses on the Common Core State Standards and provides opportunities for 
individual, small groups, and whole group instruction, as well as academic intervention. The culture of our school reflects a 
commitment to working together with parents, students, and our community to ensure all of our students meet their full academic 
and personal potential. In addition to a rigorous academic program, our school makes available for students a broad array of school-
sponsored clubs, sports, and activities. We offer a structured learning environment that encourages safe and respectful interactions 
among students and between students and adults. Taking advantage of these opportunities can help your children develop confidence, 
independence, and interdependence within a community. At Mammoth Middle School, we strive to make our motto, "Show your Mt. 
Lion P.R.I.D.E." come to life every day (Positive, Respectful, Integrity, Disciplined and Dependable, Excellence). Our instructional focus 
is to Read Actively, Think Critically, and Strive to Grow. Parents and the community play a crucial role in the school. Understanding the 
school’s educational programs, student achievement, and curricular offerings can assist both school and the community in making 
needed improvements. We hope this Report Card gives all who read it insights into what our school is all about. 
 
 
District and School Profiles 
Mammoth Unified School District, located in Mono County and nestled in the Eastern Sierra mountains, educates approximately 1,200 
students in grades kindergarten through twelve on a traditional calendar system. There are currently one comprehensive high school, 
one continuation high school, one middle school, and one elementary school in the district. 
 
The school opened its new facility in 2003 to grades six through eight. Teachers, staff, and administrators continue to act on the 
principle that students come first. The educational programs at the school are tailored to meet the needs of a changing school 
population. Mammoth Middle School is committed to providing a strong instructional program for all students to ensure excellence 
in education. This excellence can be seen in the staff who play such a vital role in providing a quality educational program. In the 2018-
19 school year, the school is serving 300 students. 
 
 

2017-18 School Accountability Report Card for Mammoth Unified School District 

Page 2 of 11