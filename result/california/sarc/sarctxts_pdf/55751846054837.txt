Tenaya Elementary School is home to the Warriors. Our mission echoes that of our district, “to provide each student a quality 
education in a safe and healthful learning environment.” Tenaya Elementary is the only elementary school in the Big Oak Flat-
Groveland Unified School District (BOFG). Its configuration is Transitional Kindergarten through eighth grade. The BOFG district is a 
small rural district that also operates two necessary small high schools and a community day school. Approximately 200 students 
attended Tenaya Elementary School during the 2017-18 school years. The district follows the Tuolumne River from the Northern 
entrance to Yosemite National Park down to the Lake Don Pedro area, a distance of over 60 miles and encompassing 678 square miles. 
Tenaya School is located in the southern part of Tuolumne County. The school serves the communities of Groveland, Big Oak Flat, 
Moccasin, and part of the Don Pedro area. 
 
Tenaya has faced declining enrollment over the last 10 years. This area depends heavily on the tourist trade as there is no substantial 
industry here. Approximately 54% of the total student body participates in the Free or Reduced Breakfast and Lunch Programs. Special 
Education services include combined Resource and Special Day Classes. The services of a speech pathologist and a school counselor 
are also available on a limited basis. Tenaya Elementary Staff is composed of 9 full time certificated classroom teachers, 2 full time 
certificated special education teachers, 1 part time P.E. teacher, 1 part time art teacher, a Principal, a full time school secretary, a full 
time principal’s secretary, 4 part time classroom/campus aides, 3 full time special education aides, 2 part time Title 1 aides, 1 full time 
and 1 part time custodians, 2 full time and 1 part time cafeteria workers. The school follows a traditional school calendar, and either 
meets or exceeds requirements for instructional minutes. 
 
About Our School 
 
Our staff and community are committed to the academic, social and emotional success of our students. All families have the 
opportunity to formally conference with their child’s teacher regarding their academic performance after the start of the school year. 
Tenaya teachers are always available for parent conferences if needed or requested. The students identified as low performing during 
the school year are targeted to receive intervention services. Some of these services include Title I funded remediation for phonics 
skills, reading fluency and comprehension, along with Front Row and Study Island computer lab assistance. SIPPS is available for 1st-
3rd grade students who need assistance in reading. Parents and community volunteers, as well as employees, work during school 
hours with these students. In addition, students who qualify receive special education services both in the classroom and in a pullout 
setting depending on their needs. The school crisis counselor provides individual counseling to students. The Tuolumne County SARB 
panel also assists students and their families throughout the school year. Free and reduced breakfast and lunch are available to those 
that qualify. Food for Kids is another resource for our families. Brainy Groveland provides assistance in reading and math, offering 
monetary motivation for students. 
 
Our teachers and staff use Positive Behavior Supports to limit discipline issues. Tenaya Elementary School utilizes Character Education 
to encourage respectful behaviors that decrease incidents of bullying, teasing, and other disruptive behaviors and encouraging acts of 
kindness. W e start each day with “Words of Wisdom” from Project Wisdom to encourage social-emotional learning such as self-
awareness, self-management and relationship skills. 
 
 
 

2017-18 School Accountability Report Card for Tenaya Elementary 

Page 2 of 10