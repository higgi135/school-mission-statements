Message from the Principal 
The voters of California established the School Accountability Report Card (SARC) in 1988. The Report Card, issued annually by school 
boards for each public school in the state, requires schools to provide our communities with information regarding the status of our 
schools. More importantly, however, the SARC has become a means for initiating conversations that engage our parents and 
communities in the life and improvement of our schools. As you read through the Diamond Ranch SARC, you will see that DR is deeply 
committed to the success of each of our students. 
 
Pomona Unified School District instructional initiatives, such as Direct Instruction, Thinking Maps, and Write for the Future; coupled 
with our site programs including a school wide reading program, the Visual and Performing Arts Pathway, Bright Prospect, AVID, and 
embedded literacy intervention/enrichment periods, provide us with the tools to meet the educational needs of all our students. 
 
We support student success through a comprehensive tutoring program. Peer tutoring is facilitated through our NHS and CSF chapters, 
as well as Math Club. The National College Resources Foundation provides college tutors, who are embedded into the Math, 1, Math 
2, and 3 classes. . 
 
We offer a full slate of honors and AP classes and college dual enrollment courses through a partnership with Mt. San Antonio College. 
Our efforts to challenge students to take rigorous courses and to excel academically was acknowledged again this year, when Diamond 
Ranch was named to the U.S. News & World Report Best High Schools, the Educational Results Partnership Honor Roll School Program, 
and Best Public High Schools in America by NICHE. 
 
Our teachers collaborate in cohorts organized around academic courses. Led by teacher-leaders, these cohorts work together creating 
common formative assessments, engaging in data dialogues, and sharing best 
practices. 
 
As we prepare to meet the future needs of our students, we continue to reshape and to rethink our practices to align with 21st century 
educational expectations. Teachers participate in both site-based and district-led professional development centered on the California 
State Standards. 
 
Diamond Ranch is a nurturing community with high expectations for all students. This is made possible because of the active 
involvement of our students, staff, parents and local community. I welcome any comments you have that will help us improve. My 
office phone number is (909) 397- 4715, ext 32050. 
 
Suzanne Steinseifer-Ripley 
Principal 
 
School Description 
Diamond Ranch High School graduates are empowered to pursue the futures of their dreams. They are successful in academics, 
athletics, and the arts. They are effective communicators, problems solvers, collaborators and goal setters. They exhibit self-discipline, 
honesty, integrity, and personal accountability. 
 
Diamond Ranch teachers participate in Cohorts organized around the courses of English Language Arts, mathematics, science, and 
social science, meeting at least three times a month, to analyze student performance data, to share instructional strategies, to create 
common assessments and grading rubrics, and to plan intervention/enrichment. We have adopted a rotating reading 
intervention/enrichment period, which enables students and teachers to identify individual student reading levels, and target reading 
materials that will maximize growth for the specific student. 

2017-18 School Accountability Report Card for Diamond Ranch High School 

Page 2 of 15 

 

 
VISION STATEMENT: Preparing our students to become first class citizens with a world class education 
 
MISSION STATEMENT: Our mission is to empower students to achieve lifelong academic, career, and personal success in an 
atmosphere that nurtures mutual respect, ethical behavior, responsibility, and hard work.