Space and Aeronautics Gateway to Exploration, S.A.G.E., Magnet Academy Middle School is located in Palmdale, one of many cities 
that are members of the Antelope Valley's rich history in space and aeronautics. S.A.G.E. Magnet Academy opened its institutional 
doors on August 9, 2017 for 6th, 7th, and 8th grade students with a focus on Science,Technology, Engineering, Art, and Math or STEAM 
Magnet Academy. An additional educational benefit that S.A.G.E. students share is the Planetarium that is located on the campus. We 
are a federally recognized magnet school. Our goal is to prepare students for success in high school and beyond by ensuring they are 
taught by highly qualified teachers and have access to the latest technology and curriculum. Research-based instruction will include 
hands-on activities, student collaboration, and project-based learning. To ensure our students have a well-rounded middle school 
experience, students will also have courses in the Arts such as music and art classes along with thematic units. 
 
The mission of S.A.G.E. Magnet Academy Middle School is to utilize our local aerospace industry and professionals, planetarium, flight 
lab, AVID and other aerospace resources to implement rigorous learning opportunities founded in the practice of Project Based 
Learning in order to achieve our vision. We see students as what they can become. We empower students to contemplate aeronautics 
and space as possible future career choices. We prepare students to practice resilience and develop critical thinking skills through 
creativity and hands-on, real-life projects. Finally, we teach students to take ownership of their learning process and become life long 
learners. 
 
Our vision at Space and Aeronautics Gateway to Exploration, S.A.G.E., Magnet Academy Middle School is to promote a growth mindset 
in all of our students, staff, and parents as we encourage academic achievement, global citizenship, and real world knowledge in space 
and aeronautics. We strive to provide authentic aviation and space themed instruction to develop critical thinkers prepared for high 
school, college, and careers.