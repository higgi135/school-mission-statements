Nelson Elementary is a PBIS, public school located in Tustin that provides rigorous and engaging learning experiences for our 480, TK 
through fifth grade, 21st century learners. We are a schoolwide Title I school committed to implementing best teaching practices to 
meet the needs of our diverse students. The Nelson student subgroups include 73% socioeconomically disadvantaged, 37% English 
Learners, 11% Students with Disabilities, 78.7% Hispanic, 7.2% Asian, and 6.6% White students. Our dedicated teaching staff and 
parents work in relentless pursuit of our mission: We at W.R. Nelson will not rest until all our students’ needs are met through high 
expectations, communication, and collaboration. 
 
Nelson’s most significant need for improvement is to support English Learners. This subgroup underperforms in meeting and exceeding 
standard on local and state assessments. Our focus is providing best tier I instruction by dedicating our resources to improve the 
effectiveness of integrated and designated ELD instruction and increase student engagement. Nelson will increase in overall reading 
and writing instruction with a focus on Writer’s Workshop training this year. Research shows that effective writing instruction impacts 
overall literacy success. We have established a strong Professional Development model to support teachers in ensuring high levels of 
instruction and learning. This includes 3 coaching opportunities for all teachers provided by Momentum in Teaching. Professional 
development will be differentiated to meet the needs of each teacher and team. The site will fund three grade level collaboration days 
to enable each grade level team opportunities to observe, plan, and study data to maximize instruction in response to student needs. 
Additionally, we will focus on developing mathematical practices. All Wednesday professional development will be aligned with a staff-
wide book study of Tracy Zager's "Becoming the Math Teacher You Wish You'd Had." Staff will participate in 3 lab days to facilitate 
learning new mathematical practices with our own students. 
 
During the 2018-19 school year, Nelson is committed to the following improvement goals: Nelson students will demonstrate an 
increase in math proficiency as measured by a 5% increase in scoring standard met on CAASPP Math assessment; The percentage of 
Nelson EL students scoring well-developed overall will increase 5% according to the ELPAC assessment. They will also show 5% growth 
in ELA achievement as demonstrated on the CAASPP ELA assessment.; Nelson staff will provide more strategic Tier I and Tier II 
instruction to improve first instruction as measured by SWD demonstrating a 3% increase in achievement according to CAASPP in ELA 
and Math; Nelson students will demonstrate improved engagement as measured by a 25% decrease in the number of students earning 
an "N" (Needs Improvement) or "U" (Unsatisfactory) in effort grades, between the first trimester reporting period and the third 
trimester reporting period.