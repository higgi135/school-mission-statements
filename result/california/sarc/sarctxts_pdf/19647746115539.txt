Let me begin by saying how privileged I feel to be serving the community of Lynwood as the 
Principal of Hosler Middle School, home of the lions! I am eager to begin the awesome work of 
building strong relationships with our students, parents, and staff to ensure that we provide a safe, 
conducive learning environment for all students. 
 
For over 20 years, I have dedicated my life to the field of education because I believe that a quality 
education can change the trajectory of a student’s life. I firmly believe that if provided the 
opportunity and the appropriate support, all students can learn and reach their full potential! My 
passion is fueled by my own personal experiences as a first generation college student, and that of 
many students who demonstrated to me through the years that all they needed was an opportunity 
and someone to believe in them. 
 
Hosler Middle School is committed to providing our students with instruction, programs, and 
activities that will help prepare them for the rigor of high school and beyond. We also understand 
that in order for our students to be academically successful, their social-emotional needs must be 
meet as well. 
 
Hosler will focus on developing innovative thinkers who can effectively communicate, collaborate 
and problem solve. Our dedicated staff meets regularly throughout the year to collaborate on 
instruction and programming to better meet the needs of all students. 
 
Using a holistic approach, Hosler Middle School is committed to helping develop and guide all our 
students to make good decisions and choices that will have a positive impact on their educational 
careers as well as their future. We will encourage our students to practice our school’s core values: 
Be Respectful, Be Responsible, Be Safe as a way to develop people of good character: who can 
inspire and lead. 
 
The middle school years are a critical and transitional time for students. Today’s students are often 
faced with a multitude of challenges that often extend beyond the reach of school, but can have 
immediate impact on our student’s educational achievement. This is why communication and 
collaboration between our parents, staff and students is key! If there should ever be a concern 
please do not hesitate to reach out to our teachers, staff or administration. Together we can begin 
to find solutions that can ensure our students social-emotional needs are meet and they stay on 
track towards meeting their educational goals. 
 
Please refer to the Hosler Middle School website to learn more about how our parents can be a 
part of our school committees and activities. Your voice matters! 
 
Sincerely, 
 
Celinna Pinelo 
 
Principal, Hosler Middle School 
 

2017-18 School Accountability Report Card for Hosler Middle School 

Page 1 of 11 

 

Hosler Middle School is one of three middle schools in the City of Lynwood. It is located next to the Lynwood Park, situated behind the 
Lynwood Unified School District’s Office of Education. We have 573 students enrolled at Hosler Middle School. The community consists 
largely of lower and middle income families. 
 
Hosler’s Mission Statement 
The mission of Fred W. Hosler Middle School is to provide a safe environment that is conducive to learning. All students will be motivated 
to participate in the Standard Based learning process, which will prepare them to work collaboratively in a safe environment. This will 
allow for growth in a culturally diverse and technologically advanced global market. It is our ultimate goal to provide ‘A learning community 
preparing students for the University and Workplace.’ 
 
Hosler’s Vision 
HOSLER MIDDLE SCHOOL is committed to providing an environment that will enhance the minds, lives and values of our students through 
an established innovative holistic program that enables students to achieve at higher levels of academic, social and leadership skills. 
 
HOSLER MIDDLE SCHOOL staff is committed to ensuring a physically and psychologically safe environment for its learning community, by 
working closely with students, parents, community members and law enforcement agencies. 
 
HOSLER MIDDLE SCHOOL courses, activities and programs are created to meet the academic and social needs of students. Instruction 
focuses on providing skills that will teach students to access and use information. Teachers recognize that serving the diversity of individual 
needs requires student-centered innovative teaching that enhances students’ learning experiences, collaboration among staff, flexibility, 
and cooperative learning experiences for student and teachers. 
 
HOSLER MIDDLE SCHOOL believes that for a student to grow in their personal and academic lives the HMS staff must be committed to 
serving in a variety of ways, including teaching, counseling, club advisers, coaching and other extra/co-curricular activities. 
 
HOSLER MIDDLE SCHOOL teachers and administrators work closely with parents and students to evaluate a student’s quality of work. 
Staff members at HMS are committed to creating partnerships with parents as well as maintaining open channels of communication with 
community members, community agencies and businesses. At HMS we believe that parent involvement is important when defining, 
implementing and evaluating curriculum and student learning. 
 
The leadership team has also worked together to develop and implement departmental Mission and Vision Statements that are aligned 
to standards and expectations. 
 
Hosler’s current schoolwide learning outcomes or expectations are as follows: 
 
Students are expected to: 

• Demonstrate their best efforts on all assignments whether in the classroom or homework; 
• Be active participants in classroom instructional activities and other extracurricular activities; 
• Arrive to class ready to learn with the necessary materials (pencils, pen, paper etc.); 
• Be prepared with appropriate homework assignment; 
• 
• Respect school properties (textbooks, computers, printers, desk etc.) and adhere to dress and discipline policies. 

Seek assistance for skills or concepts not fully understood; and 

Hosler is a data driven decision making school. Working with support staff, teachers examine data, analyze that data, and learn how to 
apply the information gained by forming a true picture of their own classroom successes, challenges, needs and gaps. Support staff and 
teachers use data for articulating goals and obtaining higher growth and performance for all students. 
 
Data-driven decision-making requires an important paradigm shift for teachers – a shift from day-to-day instruction that emphasizes 
process and delivery in the classroom to pedagogy that is dedicated to the achievement of results. 
 
Data-driven educators are able to articulate the essential elements of effective data-driven education outlined in the diagram below. The 
five major elements of data-driven instruction are: 

 Good baseline data; 
 Measurable instructional goals; 
 Frequent formative assessment; 
 Professional learning communities; and 
 Focused instructional interventions. 

Hosler utilizes these elements to enhance student learning and to inform teacher practice. 
 

2017-18 School Accountability Report Card for Hosler Middle School 

Page 2 of 11 

 

Hosler believes that in school as in life, consistent support from parents is crucial to sustaining a student’s confidence and sense of 
achievement. 
 
Here at Hosler parents are encouraged to participate in the education of their children through individual discussions with the appropriate 
teacher(s) and/or counselors, through participation as volunteers, and/or through participation in the parent council. 
 
Hosler invites all parents to be a part of the academic success of their children. 
 
Major Achievements 

 Parents involvement is visible at Hosler Middle School in the School Site Council (SSC), English Language Advisory Council 
(ELAC), School Advisory Council (SAC), and other parent activities. Hosler has allocated a Parent Center room for parents to 
meet regarding their students’ academic and social progress. 

 Parents attended a number of trainings throughout the year, including sessions on Bullying, Homework support, mental health, 

and building relationships with their teens. 

 Anti-bullying awareness and assemblies have been held at Hosler to better educate our students on the issue. Educational 

Theatre by Kaiser Permanente has provided anti-bullying assembly/performance for our students. 

 The My Buddy Club organized a Peanut Butter Drive for Feeding America's Food Drives to support those affected by Hurricanes 

Harvey and Irma. They collected of 800 jars. They also hosted an Autism Walk in honor of Autism Speaks and raised $2,000. 

 Our Advancing Via Individual Determination (AVID) Program has organized several college field trips. These trips are wonderful 
opportunity for students to learn about what college they would like to attend and what options await them. Often these 
days are the most remembered and valued for AVID students and their teachers. 

 This is the third year we have Project Lead The Way (PLTW) and our students are very involved and pleased in the class. 

Currently, we have three classes of Project Lead The Way. 
 

Focus for Improvement 

 Hosler will continue improving academics for all of our students. Special attention will be given to our student subgroups to 
close the achievement gap. Hosler has benchmark exams throughout the year; their results will help facilitate planning among 
teachers and administration. Students’ progress will be closely monitored by quarter and semester grades, number of referrals 
sent to the office, percentage of students attending on a daily basis, and benchmark exam scores. 

 Hosler continues to foster college awareness to our campus. During Back to School Night, all parents and students received a 
copy, in English and Spanish, of California State University’s “How to get to College: 6-12th grade A-G Requirements.” 
Furthermore, all teachers have posted The California State University’s “Think College” brochure to inform students what they 
need to do and explore as they get ready to enter high school. 

PBIS is also highly effective on campus; HMS has received The California PBIS (Positive Behavioral Interventions & Supports) Coalition 
Bronze Award for successful implementation of Positive Behaviors on campus. 
 
 

 

2017-18 School Accountability Report Card for Hosler Middle School 

Page 3 of 11