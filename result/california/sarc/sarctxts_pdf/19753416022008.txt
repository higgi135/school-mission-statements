Alta Vista Elementary School is located in Redondo Beach, CA and services students in grades Preschool through fifth grades. One of 
eight elementary schools in RBUSD, students receive a high quality, standards-based education. High student achievement is 
accomplished through the support of teachers, paraprofessionals, administration, parents, and the implementation of best-
researched strategies and practices. Our curricular instruction is built on critical thinking skills, differentiated instructional strategies, 
high rigor, and encourages perseverance and risk taking. Working together with our community, the staff provides a well-rounded 
curriculum and a safe, supportive environment conducive to student learning. 
 
Alta Vista's mission is to provide a safe, supportive, and enriched learning environment wherein all children can learn and grow 
academically, physically, socially, and emotionally. Alta Vista strives to mold the whole child by providing standard-based instruction 
guided by on-going assessment of individual needs, and by establishing high expectations for student achievement. We strive to create 
students who are critical thinkers, life-long learners, and decision makers who are prepared to be successful not only in school, but in 
their adult lives and a technology competitive world. Alta Vista teaches students to develop and foster a sense of community and 
responsibility for the earth through a positive and healthy world view. We, the staff at Alta Vista, are dedicated to working with 
families, community liaisons, service providers, and education professionals to ensure that each and every child achieves individual 
success.