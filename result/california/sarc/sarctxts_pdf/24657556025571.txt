Volta Elementary School is one of the three remaining original grammar schools that made up the West Side Union School District. 
The original school was built in 1890. The existing facility was constructed in 1959. The school has five permanent classrooms and 
seventeen portable classrooms. In addition to the classrooms, the campus has an office, library, cafeteria/kitchen/multi-purpose room, 
computer lab, and houses two Severely Handicapped Classes which are operated by the Merced County Office of Education. Volta also 
has a morning and afternoon LBUSD preschool. The school is housed on 6.8 acres and is on a traditional calendar. The rural setting of 
Volta School, complete with a view of the Western hills, cows across the fence, and a small school population, provides a peaceful, 
family-like environment for our students and staff. Students learn best when a supportive and enriched learning environment is 
provided. The Volta staff works cooperatively to deliver instructional programs that are educationally sound, and support our students’ 
social and academic growth. 
 
The Volta Elementary School staff has aligned all instruction to the California Common Core State Standards. Teachers develop pacing 
calendars, curriculum maps, and lesson plans which are reviewed and revised to meet the academic needs of all students. Teachers 
meet weekly to review assessment results and to develop action plans to assist students with learning and to provide universal access 
to the core curriculum. Student Study Team meetings are held to help teachers plan for struggling students. At-risk students are 
offered intervention services. Many tools are used to measure student academic achievement. Classroom assessments are used to 
determine initial reading and math levels. Assessment components of the District created Units of Study for reading and math are 
used to determine progress. Beginning in the 2014-2015 school year, students have been given the Smarter Balanced Assessment to 
determine mastery of California Common Core State Standards. 
 
Volta Elementary School teachers are observed and evaluated by the principal on a regular basis. Tenured teachers are evaluated 
every two years. Non –tenured teachers are evaluated each year. The evaluation process follows a District adopted timeline and aligns 
to the California Standards for the Teaching Profession. Individual teachers and the principal collaborate to discuss and set 
improvement goals. 
 
Volta teachers have been focused on teaching the standards, assessing student progress, analyzing data, and adjusting instruction as 
needed. Volta students have been focused on working diligently to learn grade level standards. An intervention class has been 
established to provide specific and intense instruction and support for students struggling to learn to read.