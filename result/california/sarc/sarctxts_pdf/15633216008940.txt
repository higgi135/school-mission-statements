John C. Fremont Elementary School has a record for improvement, a faculty that is professionally skilled and personally committed to 
meeting the learning needs of students and a student body which is enthusiastic and motivated to perform well. 
 
John C. Fremont School is a STEAM Academy School located in Southeast Bakersfield on a 13-acre site. The School is open Monday 
through Friday from 7:30 a.m. until 5:30 p.m. and serves Pre-K through 6th grade students. 
 
The mission of Fremont School is to educate and inspire all students to reach their maximum potential and become productive citizens 
of a rapidly changing society.