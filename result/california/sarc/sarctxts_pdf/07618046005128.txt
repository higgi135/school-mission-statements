We are excited that you are interested in learning more about Montair Elementary. Historically 
Montair is a California Distinguished School, which makes us very proud. We feel it reflects the 
outstanding instruction provided by our staff and the tremendous support provided by our parent 
community. 
 
Here are just some of the highlights of our school program: 
 
• A rigorous, standards-based curriculum with SRVUSD adopted texts and research-based 

supplemental instructional materials, supported by library and technology resources 

• Use of assessment data to determine the learning needs of each child and differentiated 
instruction tailored to move all children including English Learners and students with 
disabilities to proficient or advanced levels on state and district standards 
Fall and spring student-involved conferences in which teachers and families review student 
work samples, analyze achievement, and set goals 
Intervention and enrichment programs (including para-professional support 
classroom) to support the academic, social, emotional, and physical needs of our students 

in every 

• 

• 

• Highly skilled, dedicated, and caring staff. 
• High quality professional development training with leading organizations such as the 
Columbia Teachers College Reading and Writing Project, supported by ongoing modeling, 
practice, and coaching and the integration of math strategies from Silicon Valley Math Initiative 
Four Special Day Classes - a flow-through program providing a sequenced and coordinated 
instructional program for 1st-5th grade students with mild learning disabilities 

• 

• Academic Talent Program - a magnet program for highly gifted 4th and 5th grade students 

from across the district 

• An active and supportive parent community – Our PTA and Education Foundation volunteers 
support instructional activities in our classrooms, supervise our playgrounds, and raise funds 
to support vital intervention and enrichment programs. 

• A partnership with San Ramon Valley High School that brings over 50 high school teaching 

assistants to work in our classrooms four days a week 

• A vibrant visual and performing arts program providing experiences for students at every grade 

• 

level in art, music, and drama 
Social/Emotional skills programs including Soul Shoppe, Character Trait of the Month, 
Rainbow, and Discovery Center counseling 

• A caring and supportive learning environment that teaches all children to be safe, kind, and 

productive. 

• A commitment to making 21st Century skills part of our curriculum through the integration of 

technology in the classroom. 

 
We look forward to working with you as a member of our school community. 
 
Ondi Tricaso - Principal 
 
otricas@srvusd.net 
 
 

 

2017-18 School Accountability Report Card for Montair Elementary School 

Page 1 of 9 

 

rigorous standards-based curriculum and instruction responsive to the unique learning needs of every child 

a physically and emotionally safe school environment that fosters trust, respect, and a sense of personal and civic responsibility 

a collaborative school community that draws on the strengths of all stakeholders to support our students and celebrate learning 

Mission Statement 
At Montair School we are dedicated to providing: 
 
• 
 
• opportunities for all students to discover and cultivate their own intellectual, artistic, and athletic interests and talents 
 
• 
 
• 
 
School Profile 
Located in a park-like setting on the west side of the San Ramon Valley, Montair Elementary was established in 1958. Montair hosts several 
magnet programs for the San Ramon Valley Unified School District including four classes of children with special needs, two ATP classes, 
and one transitional kindergarten which greatly enrich the school’s climate. 
 
Montair families have high expectations for student achievement as well as the staff, and play an active role in the daily life of the school. 
This partnership with all stakeholders involved, defines Montair’s positive atmosphere of learning.