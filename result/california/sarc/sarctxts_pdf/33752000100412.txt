Monte Vista is a school where all children participate in an instructional program that fosters 
individual growth, academic progress, cultural literacy, and good citizenship. Our staff strives to 
provide each student with the skills and assets necessary to meet life’s challenges with honesty, 
integrity, courage, and the desire for intellectual and personal excellence. 
 
We continue to provide a standards-based education program while implementing the academic 
standards that will prepare children to work with others in the 21st century. We believe in providing 
a safe and supportive learning environment. We believe that everyone has the right to be treated 
with kindness, respect, and dignity; that perseverance and determination are key to achieving one’s 
personal best; and that open communication builds understanding and supports informed choices. 
It is also our belief that trust and cooperation are crucial elements for a successful school 
community and that responsibility and self-discipline are fundamental qualities for success! 
 
Pamela Picchiottino, Principal 
 
Highlights & Achievements: 

• Our school took part in the third year of the California Assessment of Student 
Performance and Progress (CASSPP) assessments. 50% of students in grades 3 – 5 met 
or exceeded standards in English Language arts, and 43% of students in grades 3 – 5 
met or exceeded standards in Math. 

• 

• We implemented a math-fact fluency program that focuses on students’ mastery of 
basic facts, along with a new concept-based math program. We have also started an 
adaptable ELA and Math program called iReady. The diagnostics and instruction 
components support CAASPP assessments. 
For our struggling readers, we instituted an intervention program during the school day 
to ensure access for all of the students who needed it. This program is developed 
around the concept of a learning center where all of our resources are available to all 
students. In the learning center we have trained staff to assist students as well as 
intervention programs to meet their needs. The intervention programs include READ 
180, an intensive supplemental reading program that replaces the basic curriculum and 
helps accelerate struggling readers; I-Read, a supplemental program provided in 
addition to the regular reading program to help students meet their grade-level goals; 
and Systems 44, a program that focuses on the 44 phonemes (sounds) of the English 
language. 

• We promote a well rounded education by providing opportunities for visual and 
performing arts exposure through 5th grade Band, Ballet Folklorico, Poetry, Hip Hop, & 
Cultural Dance, 4th grade Visual Arts & Recorders. 3rd Grade cartooning & Hip Hop. 
2nd Grade Ballroom Dancing & Intro to Art. 1st grade Intro to Music & Musical Theater. 
Kindergarten Sing-A-Long and Hip Hop. 3rd - 5th grade choir and K - 5th grade school 
musicals. 

 

2017-18 School Accountability Report Card for Monte Vista Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Monte Vista Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

45 

45 

45 

0 

0 

1 

0 

1 

0 

Monte Vista Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

922 

27 

50 

Teacher Misassignments and Vacant Teacher Positions at this School 

Monte Vista Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.