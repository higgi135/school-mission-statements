About Our School 
 
Cajon Park School serves approximately 1000 students from kindergarten through eighth grade and is located in the foothills of city 
of Santee. Cajon Park strives to be an innovative school that meets the academic, social and emotional needs of each child that we 
serve. Cajon Park is beloved by parents, students, staff and alumni. When you walk on the campus and meet our community members 
it is easy to see why. Cajon Park is a compassionate school that cares deeply for our students. 
 
At Cajon Park, we have developed a diagnostic and prescriptive Response to Intervention and Instruction program that has served as 
a model for schools in other districts. We strive to prepare our students for success in college and career, and expose our students 
the skills necessary to be successful in the 21st century workforce. We have increased engineering opportunities for students in all 
grade levels. 
 
Cajon Park is a Trauma Informed school that has developed caring and respectful interaction practices and a therapeutic response 
approach when working together. We have developed Positive Behavior Intervention and Support systems that are proactive by 
teaching positive behaviors and are committed to positively recognizing every student publicly in front of the school community each 
year. 
 
Cajon Park Vision: 
Cajon Park inspires meaningful learning through creativity, collaboration, and critical thinking to prepare learners for an ever-changing 
world. 
 
“Where Young Minds Meet Open Doors” 
SANTEE SCHOOL DISTRICT VISION, MISSION, BELIEF STATEMENTS, AND GOALS 
 
Adopted May 1, 2012 
 
MISSION STATEMENT 
 
Santee School District assures a quality education, empowering students to achieve academic excellence and to develop life skills 
needed for success in a diverse and changing society. 
 
VISION STATEMENT 
Santee School District will be an innovative leader in education, 
inspiring students to realize their unique potential. 
 
BELIEF STATEMENTS 
Children are our first priority. Therefore we believe…. 
All students can learn. 
Student growth, academic performance, and positive personal development are the highest measures of student and district success. 
Trust, integrity, respect, citizenship, honesty, responsibility, commitment, and pride are the foundations on which our district is built. 
Students should understand and respect the origin of the nation, the law of the land, and the principles of our democracy. 
Parent and community involvement in our schools is crucial to the academic success of our students. 
Knowledgeable, motivated, and inspired employees assure the success of our students. 
Everyone has the right to learn and work in a safe, healthy, orderly, and clean environment. 
The district operates efficiently and effectively through focused leadership, fiscal responsibility, and open communication, with a 
strong academic program as the top priority. 

2017-18 School Accountability Report Card for Cajon Park School 

Page 2 of 11 

 

 
BOARD GOALS 
Educational Achievement 
Assure the highest level of educational achievement for all students. 
 
Learning Environment 
Provide a safe, engaging environment that promotes creativity, innovation, and personalized learning 
 
Fiscal Accountability 
Financially support the vision, mission, and goals of the District by maximizing resources, controlling expenses, and managing assets 
to ensure fiscal solvency and flexibility. 
 
Staff Development 
Implement a staff development plan as the cornerstone of employee performance and growth. 
 
Student Well-Being 
Provide social, emotional, and health service programs, integrated with community resources, to foster student character and 
personal well-being.