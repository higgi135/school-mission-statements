Nestled in a peaceful Los Angeles suburb, Lakeview Elementary, home of the Lions, is the pride of 
its neighborhood. Located at 11500 Joslin Street in the City of Santa Fe Springs, the facility is 
comprised of twenty-four classrooms, a multi-purpose room, a library, a computer lab. Lakeview 
Elementary has had a recent infusion of technology with five computers in each kindergarten 
through first grade classroom and a mobile lab in each second through fifth classroom . With over 
510 students in grades TK-5 the school sits at the hub of a tightly knit community where school 
staff, parents, and community leaders work hand in hand to provide a safe, nurturing, learning 
environment. 
 
Recognized as a former California Distinguished School by the California Department of Education, 
a California Business for Education Excellence Honor Roll School (2015, 2016, 2017), and a California 
Gold Ribbon School (2016), Lakeview focuses on high academic achievement for all students. 
Teachers provide purposeful and strategic instruction using the California State Standards and 
district pacing guides. There is a shared commitment to consistently implement best practices and 
research based strategies. As a Professional Learning Community, teachers work in collaborative 
teams to analyze data, modify instruction, and provide intervention as needed to ensure student 
success. Students at Lakeview Elementary are motivated and encouraged to think critically, 
communicate effectively, and be active participants in successfully achieving their academic and 
personal growth goals to become lifelong learners. 
 
At Lakeview we foster a partnership of school, home, and community through ongoing 
communication and community events. Our supportive Parent Teacher Organization (PTO) works 
closely with the school to provide funds and programs which enrich the school experience for our 
students. To further extend their academic programs, students participate in programs such as 
Meet the Masters, as well as classes in art, music, and physical education. 
 
Our most recent LLCSD parent survey stated that 99% of Lakeview parents were satisfied with their 
child's school. The community of Lakeview Elementary is dedicated to creating a learning 
environment that supports high expectations for student achievement and behavior. We believe 
that all students can achieve at high levels. Our passion for successful student achievement and 
our focus on collaboration, community ownership, and pride are elements that define Lakeview 
Elementary School and make it an outstanding place for all. 
 
 
Michael Williams, Principal 
 

2017-18 School Accountability Report Card for Lakeview Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Lakeview Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

23 

22 

21 

0 

0 

0 

0 

0 

0 

Little Lake City School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

9 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Lakeview Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.