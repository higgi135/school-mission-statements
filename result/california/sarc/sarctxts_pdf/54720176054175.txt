Oak Valley Union School is a rural community consisting of 563 students and 57 full and part time employees. The Oak Valley School 
community, which includes a highly qualified staff, the Board of Trustees, students and parents; seeks to provide all students with a 
quality educational experience, steeped in a rich history of high standards, and caring for the whole child. With an energetic focus on 
students’ self worth, providing new experiences, and preparation for success in the 21st century; we are committed to graduating 
students who will make an impact in their community and their world.