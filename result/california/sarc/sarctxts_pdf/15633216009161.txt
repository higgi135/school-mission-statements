The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters. As you read the 
Report Card, you will gain a better understanding of Voorhies as a school with a record for improvement, a faculty that is professionally 
skilled and personally committed to meeting the learning needs of students and a student body which is enthusiastic and motivated 
to perform well. Voorhies was chosen “School of the Year 2000” by the Bakersfield City School District Educational Foundation. 
 
Voorhies School, a kindergarten through sixth grade elementary school, is located northeast of central Bakersfield, on a 10.7 acre site 
bordered by Pioneer Drive, Park Avenue and Foothill Drive. Voorhies maintains a strong academic program that is enhanced by the 
inclusion of a Dual Immersion Program. 
 
Voorhies Mission Statement: Voorhies School is committed to educating all students in a safe learning environment through working 
together in a collaborative culture. We will create genuine and effective partnerships with parents and community, while maintaining 
high expectations for all students and ourselves.