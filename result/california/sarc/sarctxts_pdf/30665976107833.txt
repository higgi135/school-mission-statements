Woodland Elementary is the first part of a two site educational setting for students who live on the 
east side of Costa Mesa. The school provides instruction for students in transitional kindergarten 
through second grade in classroom settings of approximately 24-26 students:1. We offer an 
extended day kindergarten that begins at 8:00 am and ends at 2:00 pm. Grades 1 and 2 begin at 
8:15 am and end at 2:40 pm. After students complete second grade at Woodland, they continue 
their education at Kaiser Elementary School for instruction in grades three through six. After 
completing sixth grade, the students report to Ensign Middle School for grades seven and eight and 
proceed to Newport Harbor High School until their graduation from grade twelve. 
 
Woodland Elementary is committed to assisting students achieve success and self-confidence by 
providing an outstanding academic program with an emphasis on literacy and math skills. As a staff, 
we believe in providing a challenging academic program; fostering success at school; ensuring a 
safe, orderly and supportive learning environment; teaching high standards of behavior, 
responsibility, and citizenship and encouraging all students to develop to the best of their potential, 
academically and socially. Technology is integrated throughout the curriculum and used as a tool 
to enhance learning in all areas. We believe that educational technology helps students and 
teachers solve problems more creatively and efficiently by developing critical thinking skills by 
providing students with a means to organize and process information. 
 
 
 
 
 

----

---- 

Newport-Mesa Unified School 

District 

2985 Bear St. 

Costa Mesa, CA 92626 

(714) 424-5033 
www.nmusd.us 

 

District Governing Board 

Charlene Metoyer, President 

Martha Fluor, Vice President 

Dana Black, Clerk 

Ashley Anderson, Member 

Michelle Barto, Member 

Vicki Snell, Member 

Karen Yelsey, Member 

 

District Administration 

Dr. Frederick Navarro 

Superintendent 

Mr. Russell Lee-Sung 

Deputy Superintendent, 
Chief Academic Officer 

Ms. Leona Olson 

Assistant Superintendent, 

Human Resources 

Mr. Tim Holcomb 

Assistant Superintendent, 

Chief Operating Officer 

Dr. Sara Jocham 

Assistant Superintendent, 

Student Support Services/SELPA 

Mr. Jeff Trader 

Executive Director, 

Chief Business Official 

Dr. Kirk Bauermeister 

Executive Director, 
Secondary Education 

Dr. Kurt Suhr 

Executive Director, 

Elementary Education 

 

 

2017-18 School Accountability Report Card for Woodland Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Woodland Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

27 

27 

28 

0 

0 

0 

0 

0 

0 

Newport-Mesa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1105 

5 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

Woodland Elementary School 

16-17 

17-18 

18-19