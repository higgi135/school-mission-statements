Juanita Blakely Jones Elementary School opened July 31, 2006. Currently, there are 18 regular education classrooms in use that support 
students from kindergarten through sixth grade, providing sufficient space for instruction. The school proudly showcases a computer 
lab, library, multi-purpose room, a staff development room, a staff resource room, and an administration office. Jones Elementary 
also has professional office space for support staff (i.e. RSP teacher, speech therapist, psychologist, etc.). 
 
Mission Statement: To ensure that all students acquire the skills and knowledge essential for high levels of academic success by 
addressing the intellectual, social, cultural, and emotional needs of all students.