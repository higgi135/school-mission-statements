Gladstone Street Elementary (PK-5) School, home of the Eagles, has a rich history of almost six 
decades of educating pupils in the heart of the City of Azusa. Teachers and staff work in partnership 
to provide all students a rigorous education that will allow them to compete in the global economy 
as adults. In addition, the school develops responsible and productive citizens; creates partnerships 
with parents, families and the community; and provides a physically and emotionally safe place for 
all pupils. Gladstone Street Elementary School is dedicated to maintaining a solid partnership 
between school, parents, Azusa Pacific University (APU), and community to ensure the success of 
all students. We offer parent education at each grade level as a way of encouraging parents to take 
an active part in their child’s education. 
 
Our staff recognizes the important role that self-esteem plays in the success of each child. In 
partnership with McKinley Center, we provide counseling for students on campus. In conjunction 
with creating a positive environment for learning, we have identified school-wide behavior 
expectations for students called "Positive Behavior Intervention System" (PBIS). Our PBIS motto is: 
"Gladstone Street Students S.O.A.R." S-Show pride and Respect, O-Obtain Goals,A- Act Responsibly, 
and R-Remember Safety. Students have many opportunities to be recognized for their academic 
achievements such as Reading Counts, Student of the Month, Eagle Effort, Academic Achievement, 
Perfect Attendance, Honor Roll and Principal’s Honor Roll, Gold Presidential Award, Silver 
Presidential Award and Reclassification. Teachers value, and are committed to their continuing 
professional development by participating in workshops, collaboration and study Professional 
Learning Communities, (PLCs). At Gladstone Street there is an academic focus with interventions 
throughout the instructional day as well as before and/or after school. 
 
Gladstone Street Elementary is a place of excellence where students will recognize and achieve full 
potential in their academic, creative, personal, and moral development. This is accomplished by 
supporting students socially, emotionally, and academically through academic differentiation, 
intervention, and rigor. 
 
Our beliefs include: 

• All pupils can learn and achieve at high standards regardless of gender, class, ethnicity, 

or culture. 
Social-emotional health and development is critical to achieve academic success. 

• 
• Achievement is tangible, clearly-defined and measurable. 
• 

• 

Strong 21st century instruction is necessary to prepare students to become college and 
career ready. 
Early and effective instructional support occurs throughout the day to guarantee 
student success. 
Effective learning involves the entire community. 

• 
• A clean, safe and orderly campus promotes social and academic success. 

We will: 

• Promote positive community participation and facilitate continuous communication 

that ensures active parent involvement through school functions. 

• Provide a safe and orderly school site that adheres to discipline that is firm, fair, 

progressive, and consistent. 

• Collaboratively establish, maintain, and accomplish high expectations through open-

ended, creative thinking in academics and behavior. 

• Model and provide positive reinforcement for good moral conduct exhibiting Positive 
Behavior Characteristics: S.O.A.R-Show respect, Obtain goals, Act responsibly and 
Remember Safety. 

2017-18 School Accountability Report Card for Gladstone Street Elementary 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Gladstone Street Elementary 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

18 

14 

14 

1 

0 

2 

0 

1 

0 

Azusa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

441 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Gladstone Street Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.