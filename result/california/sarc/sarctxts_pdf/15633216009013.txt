Jefferson Elementary is a small neighborhood school built in 1917 with approximately 510 students in attendance from Kindergarten 
through 5th grade. The school has a culturally diverse student population. Jefferson offers high quality English Language Arts and Math 
instruction with an emphasis on English Language Development. Second language learners are provided specialized instruction daily 
during the English Language Development time block. Fine Arts are encouraged through our instrumental music, flutophone, and choir 
programs as well as art in our after school program. We emphasize a positive school environment by implementing PBIS, student 
mentoring, Active Supervision, and the Second Step Intervention Program. 
 
The Jefferson staff is committed to ensuring that all students reach high levels of academic achievement by: 

• Having high expectations for our students and staff. 
• Building positive relationships with students and parents. 
• Working in collaborative teams and using student data when planning for Good First Instruction. 
• Meeting the instructional needs of ALL students.