Core Values 
Respect 
Relationships 
Responsibility 
Results 
 
Core Belief 
It is our moral duty and privilege to provide all students with the excellent education they deserve. 
 
Guiding Principle 
Our work requires virtue and virtu. 
 
Mission Statement 
To develop the character and competencies students need in school and life. 
 
Vision Statement 
Every student in our school will promote to middle school without the need for remediation. 
 
Teaching & Learning Goals 
Read smart. 
Think straight. 
Write clearly. 
Work hard. 
Try again. 
Be kind. 
 
Commitments 
Continuous improvement 
Exceeding expectations 
 
Simple Truth 
Hard work is hard. 
 
Motto 
“If you really do not want to, nothing can make you. 
If you really do want to, nothing can stop you.” 
 
 

2017-18 School Accountability Report Card for Allison Elementary 

Page 2 of 12