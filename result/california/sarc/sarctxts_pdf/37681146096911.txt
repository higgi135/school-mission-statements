Fallbrook Homeschool Academy (FHA) provides an innovative, flexible option for parents who want 
to be a co-educator for their K-8th grade student. Our focus is on “creating leaders every day.” Our 
CORE program offers an option for blended on campus/at home education for K-8th grades. This 
rigorous academic program and successful parent/teacher partnering has contributed to our 
students' success. As part of the Fallbrook Union Elementary School District our unique program 
offers a variety of support including teacher advisers, Parent Teacher Trainings, student textbooks 
with teacher editions, Internet based programs and a variety of curriculum support including a 
Parent Resource Library. Our CORE students excel with a complement of targeted on campus 
instruction with highly qualified credentialed teachers and the one-on-one instruction provided 
during their home schooling days. FHA students are becoming leaders in school and in our 
community as they develop 21st century skills while they learn and apply "The 7 Habits of Highly 
Effective People" by Dr. Stephen Covey. With strong family and community support, the 
outstanding expertise of our staff, and a shared commitment to learning, we are creating our future 
leaders! 
 
Mission Statement 
The mission of the Fallbrook Homeschool Academy is to provide educational alternatives through 
a professionally supported independent study program; the CORE program. We believe all students 
can learn and we provide educational options tailored to the wide variety of needs of the families 
and students in our community. We are dedicated to providing opportunities for students to 
achieve academically and socially thus enabling them to become lifelong learners and responsible 
members of an ever-changing society. 
 

----
---- 
Fallbrook Union Elementary 

School District 

321 North Iowa Street 

Fallbrook, CA 92028-2108 

(760) 731-5400 
www.fuesd.org 

 

District Governing Board 

Siegrid Stillman, President 

Patty de Jong, Vice President 

Lisa Masten, Clerk 

Caron Lieber 

Susan Liebes 

 

District Administration 

Candace Singh, Ed.D. 

Superintendent 

 

2017-18 School Accountability Report Card for Fallbrook Homeschool Academy 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Fallbrook Homeschool Academy 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

2 

0 

0 

2 

0 

0 

2 

0 

0 

Fallbrook Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

245 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Fallbrook Homeschool Academy 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.