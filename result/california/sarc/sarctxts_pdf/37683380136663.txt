America's Finest Charter (AFC) is a WASC-accredited, direct funded TK-12 charter school, which was established in Fall 2011 serving 
the low-income, multi-cultural communities of City Heights and Chollas View. America's Finest Charter was approved by San Diego 
Unified School District (SDUSD) in March 2011 to serve grades K-8. The small school model provides every child with individualized 
attention and a close school-family relationship. The school provides 180 days of instruction following a modified calendar with 
instruction delivered in self-contained classrooms. AFC expanded to add high school in Fall 2017, and now serves 460 students in 
grades TK-11. AFC offers a rigorous academic program providing students with a high quality education and families with school 
choice. AFC offers three unique features: (1) we are a K-12 school, so students can continue with AFC until graduation; (2) small class 
sizes and many opportunities for students to reach their potential; and (3) Arts in the Afternoon classes. AFC offers instrumental music, 
choir, piano, art, robotics, cheer, sports, cooking, Mad Science, etc. from 3:30 p.m. to 6:00 p.m. Monday through Friday at K-8 site. 
AFC also has a reading club and a running club that meet before school from 8:00 a.m. to 8:45 a.m. at the K-8 site. In addition, a 
tutoring program is offered to all students one hour after school, and the tutoring is provided by certificated staff. 
 
America's Finest Charter mission will help our students achieve their American Dream. Our vision is to accelerate our students’ 
academic achievement by developing 1) healthy students, 2) proficient readers and writers with 3) higher-order thinking skills who 
are capable of 4) problem solving and are 5) self-motivated, 6) competent, 7) lifelong learners, and 8) social interactors prepared for 
the workforce of the 21st Century. Our school will raise awareness and cultivate an appreciation for the American values and work 
ethics in order for all students to “Seize the American Dream.” Our motto is “Work hard, learn everything you can, always do your 
very best, look to help others and your success will follow.” Students engage in cross-curricular, inquiry-based, hands-on project-
based learning that provides challenging and meaningful ways to master skills and concepts with a level of understanding that allows 
students to apply their knowledge to new situations. The Schoolwide Learner Outcomes (SLO) address how we effectively educate our 
students. The following are the schoolwide learner outcomes for AFC: 
America’s Finest Charter Graduate are: 
Active global citizens who: 
Demonstrate compassion for others regardless of culture, race, or religion 
Contribute to the well-being of our school 
 
Functioning scholars in the 21st century who: 
Use technology to: share ideas, organize information, and conduct research 
Explore ideas about college and career 
 
Critical thinkers who: 
Ask questions and synthesize information 
Connect concepts across subject areas 
Apply knowledge to real life situations in order to solve problems 
 
Successful communicators who: 
Exchange ideas both written and orally 
Listen closely and ask for clarification 
Work cooperatively with one another 
 
 

2017-18 School Accountability Report Card for America's Finest Charter School 

Page 2 of 11