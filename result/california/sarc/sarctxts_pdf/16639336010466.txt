Island School is proud of its history and tradition of being an outstanding school. The staff is dedicated to providing all students with 
instruction that is challenging and meets the New State Standards as well as the Next Generation Science Standards. Our motto “Strive 
for Excellence” is emphasized in our academic programs as well as our character education program, Character Counts! We are 
committed to providing a safe school with a positive learning environment for all of our students. We appreciate the support of our 
parents and community. We were honored with being named a California Gold Ribbon School June 2016 and California Distinguished 
School June 2014. Mission Statement: The Island Union Elementary School District is dedicated to providing a positive learning 
environment and a quality educational program that will develop the inherent capabilities of all students, help prepare them to be 
successful in high school, and be good citizens. To achieve this mission we strive to follow these goals. 1. The District will create a safe 
and positive school climate, recognize individual differences, and encourage each student to reach his/her full potential. 2. The District 
will provide strong instructional programs which are revised in a timely manner to ensure continued student achievement based on 
the changing needs of students. 3. The District will develop and maintain strong school, parent, community, business, and interagency 
partnerships.