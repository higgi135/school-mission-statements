Spring Lake Elementary is a public school within the Woodland Joint Unified School District. The 
school opened in August 2018 with Transitional Kindergarten (TK) through Third grades. Next year 
and thereafter, the school will grow an additional grade until it becomes a TK-Sixth grade campus. 
Spring Lake Elementary offers a unique STEAM (Science, Technology, Engineering, Art and Math) 
focus with a strong emphasis on Project-Based Learning that builds 21st Century Skills. The focus 
of STEAM is developing rigorous math and science skills through engineering and art. The context 
is engaging and provides challenges for real-world solutions to problems students can relate to. 
 
Vision Statement: 
We are inspiring the leaders and innovators of tomorrow, so that all students will thrive in their 
education, college and career paths. 
 
Mission Statement: 
The mission of Spring Lake Elementary School is to empower and engage all learners through 
innovative learning, authentic challenges, creative solutions and joyful practices. We will help 
students discover their unique talents through a caring and collaborative learning community, 
while maintaining academic rigor and high expectations. 
 
Community and District Profile 
Woodland is located in Yolo County and has a rich heritage of community spirit and neighborly 
charm. With a population of more than 56,000 people, the town is situated twenty miles from 
downtown Sacramento and 85 miles from San Francisco. Woodland Joint Unified School District 
includes six preschools, eleven elementary schools, one charter elementary school, two middle 
schools, two comprehensive senior high schools, a continuation high school, and an adult school, 
and served approximately 10,550 students in the 2017-2018 school year. In the 2018-2019 school 
year, 
 
Spring Lake Elementary School operates on a traditional calendar and for the 2018-19 school year 
has 170 students enrolled in grades TK-3. 
 

2017-18 School Accountability Report Card for Spring Lake Elementary School 

Page 1 of 8 

 

• 

•