John Wash Instructional Vision: 
Our students will learn and persevere to be college/career ready in a safe, collaborative and engaging environment. 
For more than 50 years, John Wash Elementary has held strong to the traditions of community and family.On Friday's as the school 
day begins 670 students, 25 teachers, 1 Curriculum Support Provider, RSP teacher, RSP aide, 1 principal, and many parents gather 
together for the morning flag salute and the singing of the John Wash Fight Song. John Wash Elementary (JWE) is located in a semi-
rural area, situated five miles southeast of Fresno on the western boundary of Sanger Unified School District. The eastern and southern 
areas of John Wash Elementary are rich in agriculture, while the northern and western areas are growing suburban areas. The school 
was opened in 1962 and serves children in grades preschool through sixth. JWE student demographics are 19% Asian, 44% 
Hispanic,15% White, 3% African-American, 18% two or more races and 1% American Indian or Alaska Native. 
 
JWE’s academic excellence continues to be a top priority. Our CAASSP scores in ELA and Math have been the following: In 2016/17 
ELA was 49% met or exceeded and Math was 45% met or exceeded. In 2017/18 ELA was 56% met or exceeded and Math was 51% met 
or exceeded. ELA increased by 7% and Math increased by 6%. 
 
JWE has established a system of mutual accountability of standards based learning and common instructional practices. JWE system 
has three components, 1) Professional Learning Communities, 2) Pyramid of Interventions, and 3) Direct Instruction, that work 
together to ensure student success. Professional Learning Communities provide teachers the opportunity to collaborate and establish 
SMART goals. JWE Pyramid of Intervention monitors student learning. Direct Instruction establishes strategies to teach the standards 
with this year's emphasis on writing to prepare our students for the Common Core. 
 
JWE comprehensive programs are designed to educate and develop the “whole child.” Staff, students and community members exhibit 
the five core values of “Community of Caring,” character education program, The five values, family, trust, respect, caring, and 
responsibility are displayed in every classroom. In 2012 we instituted a Positive Behavior Intervention System using the acronym 
R.O.A.R.S. This stands for Responsibility, OnTask, Achievement, Respect and Self Control. These behaviors are seen on our entire 
campus and students are rewarded with R.O.A.R.S tickets. Learning Enrichment Activity Program (LEAP) is an after-school program 
that is an extension of the school day providing students with homework assistance, enrichment, art, and PE. Students in 4th, 5th, and 
6th grade participate in the district music program with a credentialed music teacher. The PTA has many active leadership positions 
and last year parents volunteered over 4,000 hours to JWE. This amount of participation reinforces the commitment of all JWE 
stakeholders to providing students with a well-rounded education. 
 
The community of JWE believes in providing firm traditions and a family atmosphere. As staff, students, and parents walk through the 
campus they are met with motivational banners, the first summarizes our mission, “Every child, everyday, whatever it takes!” JWE 
looks forward to maintaining the traditions of high expectations and positive attitudes. With academic goals and a character value of 
the month, students at all levels are challenged with high expectations to reach their fullest potential. As a result, we are a four-time 
Bonner Character Award winner through California State University, Fresno.We are also a two-time Title I Academic Achievement 
Award winner. In 2009 John Wash Elementary was recognized as the first school in the history of Sanger Unified School District to 
receive the prestigious NATIONAL BLUE RIBBON AWARD. 
 
 In 2015 JWE was recognized as a Gold Ribbon School and earned a California merit award in civic learning. 
 
 

2017-18 School Accountability Report Card for John Wash Elementary School 

Page 2 of 12