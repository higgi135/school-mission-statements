The Heritage Contra Costa County Office of Education (CCCOE) Special Education programs provide 
a full range of services designed to meet the needs of severely handicapped students from infancy 
through age 22. Programs emphasize the integration of students with age-appropriate peers, 
placement in the least restrictive environment, a comprehensive curriculum for the severely 
handicapped, and programs to support transition. Leadership and support exist to ensure the 
quality of all students’ instructional programs as well as having facilities that are appropriate and 
maintained in a safe condition. This special education curriculum for severely handicapped students 
is based on the core areas identified in the California State Department of Education curriculum 
frameworks. 
 
The Intensive program serves students in grades 9-12 in a special day class. The program assists 
students with autism, autistic-like behaviors and intensive behaviors to manage academic, social 
and functional demands through a variety of approaches within a structured environment. As 
appropriate, staff provides opportunities for integration with age-appropriate peers, community 
outings to practice social skills and modifications for academic programming. 
 
The Early Start program is part of the CCCOE and serves infants and toddlers through a home -based 
and a classroom program. Children, age birth to three, who have vision, hearing, orthopedic 
problems or other developmental disabilities are the targeted population. The team, consisting of 
a teacher, speech or occupational therapist, and specialists in vision and hearing assist the family 
in providing assessments and developing an Individual Family Service Plan. 
 
 

-

--- 

----

---- 

Contra Costa COE 

77 Santa Barbara Road 
Pleasant Hill, CA 94523 

(925) 942-3388 

www.cocoschools.org 

 

District Governing Board 

Fatima S. Alleyne, Ph.D. 

Sarah Butler 

Vikki J. Chavez 

Mike Maxwell 

Annette Lewis 

 

District Administration 

Lynn Mackey 

Superintendent 

Lindy Khan 

Senior Director of Student 

Programs 

Rebecca Vichiquis 

Director, Student Programs 

Tom Scruggs 

Director, Student Programs 

 

2017-18 School Accountability Report Card for Heritage CCCOE Special Education Programs 

Page 1 of 8 

 

• 

•