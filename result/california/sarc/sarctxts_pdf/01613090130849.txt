The school's vision is embedded in the district's mission that states: 
 
Mission 
 
To ensure lifelong learning, San Lorenzo Unified School District, staff, families and community, will collaborate to advance authentic 
knowledge, skills and attributes that will ensure our students are creative and critical thinkers, ready to fully participate in our changing 
community and world. 
 
Vision 
 
As a result of their education, all students will become compassionate, collaborative and creative problem-solvers, who are resilient, 
well-informed, civically-engaged advocates for equity and social justice. 
 
As a result of our district’s mission and vision, our students will grow within the framework of our Graduate Profile. 
 
Our graduates will demonstrate the elements of the board approved graduate profile: 
 
I. College and Career Ready by displaying: 

• Mastery of core knowledge as outlined by the California Common Core Standards. 
• Ability to use core knowledge for further inquiry and exploration in a variety of fields and areas of interest. 
• 
• 
• Clearly articulated post graduate plan. 

Transferable skills that support future success in college such as bilingualism, critical thinking, teamwork, etc. 
The ability to navigate and explore college and career opportunities. 

II. Socially and Civically Engaged by displaying: 

• A clear understanding of self, personal needs, and identity. 
• The ability to be independent thinkers who are resilient, empathetic, healthy, and collaborative. 
• An aptitude to critically analyze information with which to make informed decisions. 
• Cultural competence and value of/for diversity. 
• Knowledge and confidence to be an up stander. 
• A disposition to act as agents of change for social justice. 

III. Effective Communicators by displaying: 

• Proficiency to listen with purpose and intent. 
• Skills to articulate ideas clearly and appropriately for the audience. 
• Negotiation and resolution strategies. 
• Capability to give and receive feedback. 
• Effective use of written language supported with evidence. 

IV. Creative and Innovative by displaying: 
• Visionary solutions to problems. 
• Ability to evaluate the effectiveness of a solution. 
• Capacity to respond to real world challenges. 

 

2017-18 School Accountability Report Card for Arroyo High School (AHS) 

Page 2 of 17 

 

V. Technologically Proficient and Responsible by displaying: 
• Computational, research, and information fluency. 
• Responsible digital citizenship and appropriate use of social media. 
• Ability to engage with new technology.