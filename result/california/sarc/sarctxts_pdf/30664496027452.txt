Welcome to Laurel Elementary Magnet School of Innovation and Career Exploration! The pride, enthusiasm, and excitement that 
Laurel was founded on in 1922 is still evident today and shared with you in this year's School Accountability Report Card. 
 
 The success of Laurel Elementary School’s students is due to the dedication of teachers, parents and volunteers who work together. 
 
Mission and Vision: 
 We are a diverse educational community dedicated to mindfully and compassionately engaging, elevating, and inspiring all learners. 
 
At Laurel Elementary School, we believe that all students are innovative thinkers who problem solve, explore, communicate, and strive 
to be positive citizens and leaders in our community. We honor and appreciate diversity and understand that learning is a collaboration 
of students, staff, and the greater community to provide a positive student centered learning environment that prepares students for 
a dynamic and advancing future. 
 
 Laurel staff and students are positive digital and community citizens who strive to create positive learning experiences for all. 
 
 Laurel Elementary Magnet School of Innovation and Career Exploration. Where students innovate, create, and explore! 
 
 In our long-standing tradition of excellence, we maintain our dedication to students, parents, and community members. We rely on 
staff to provide the best educational experience possible. We are confident that our children take with them the necessary skills and 
tools to achieve both academically and in their personal life. 
 
School Profile 
 Laurel Elementary School is proud to be recognized as a Brea Historical Landmark. Built in 1922, Laurel is a Preschool - 6th grade 
school located in the heart of the city of Brea. While many structural changes and modernization projects have taken place over the 
years, we are proud of our history and celebrate over 95 years of excellence in education.