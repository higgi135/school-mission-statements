Lincoln Crossing Elementary School opened in August of 2006. The school serves students in grades 
TK-5 with RSP, Speech, and intervention support services offered. Science and music curriculum are 
provided to students in grades 1-5. The school has a library, science lab, music room, seven 
Chromebook carts, both a 1-5 and K playground and multipurpose room. 
 
The mission of the Lincoln Crossing Elementary School team is to prepare our students with the 
skills, knowledge and attitudes to become lifelong learners and responsible, contributing members 
of society. We model "The Colt Way" by being safe, respectful, responsible and problem solvers in 
class, in play and in life. 
 
 
 

----

---

- 

Western Placer Unified School 

District 

600 6th Street Suite 400 

Lincoln, CA 95648 

916-645-6350 
www.wpusd.org 

 

District Governing Board 

Paul Carras 

Brian Haley 

Kris Wyatt 

Damian Armitage 

Paul Long 

 

District Administration 

Scott Leaman 
Superintendent 

Kerry Callahan 

Assistant Superintendent, 

Educational Services 

Audrey Kilpatrick 

Assistant Superintendent, Business 

Services 

Gabe Simon 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for Lincoln Crossing Elementary School 

Page 1 of 9