Our School Mission 
A professional learning community that works collaboratively to ensure high levels of learning for all. 
 
Our School Vision 
Villacorta Elementary School aspires to be a model professional learning community that enriches the lives of students by meeting 
their academic and social-emotional needs in a caring and stimulating 21st Century environment.