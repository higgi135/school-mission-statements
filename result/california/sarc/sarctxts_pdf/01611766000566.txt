Cabrillo Elementary is a small learning community with a culture of caring and collaboration. 
Cabrillo's teachers and support staff hold high expectations for students, and implement standards-
based instruction. Teachers provide differentiated activities throughout the school day to ensure 
that all students succeed. In the science lab, students participate in engaging hands-on and inquiry-
based instruction. The physical education specialist implements an engaging standards-based 
curriculum that directly involves students in the analysis of their health portfolio. The computer 
specialist develops and refines the skill set students will need to fully take advantage of the G Suite 
for Education set of collaborative tools (formerly known as GAFE) while following the district-
adopted technology standards. 
 
Cabrillo is a leader in technology with a 2:1 ratio of students to web-enabled device. Students have 
daily access to Chromebooks in the classroom where two teachers share a single Chromebook cart 
filled with a class set of chromebooks. This level of on-demand access allows teachers to fully 
integrate the G Suite for Education tools providing opportunities for student collaboration that 
extend and enrich learning opportunities. Students build foundational reading skills through daily 
use of Lexia, as well as refine their math skills with Mathletics. With this unprecedented level of 
access, Cabrillo teachers are able to provide targeted interventions for students based on their 
performance on the various web applications. In addition, teachers are able to accurately level their 
students for the intervention block with easyCBM, and demonstrate growth over time. 
 
The Cabrillo staff collaborates during their scheduled professional learning communities (PLC) 
during early-release Wednesdays. Teachers work in grade-level teams with the priority standards 
breaking them into discrete units of instruction that support Fremont Unified School District’s 
emphasis on the essential questions of what should be taught and how should it be assessed. 
 

2017-18 School Accountability Report Card for CABRILLO ELEMENTARY SCHOOL 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

26 

24 

25 

0 

0 

0 

0 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1713 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

0 

0 

0 

0 

0 

0 

0 

0 

Vacant Teacher Positions 
Notes: 
1) “Misassignments” refers to the number of positions filled by teachers who lack 
legal authorization to teach that grade level, subject area, student group, etc. 
 
2) Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners. 
 

0