In the Spring of 2004, the School Board voted to have Hooker Oak School pilot a K-8 program. Due to the lack of support to keep the 
7th and 8th grade students, and the movement of all sixth graders to the Jr. High campuses, we currently are a TK-5 with the ability 
to expand if the need arises. At the same time, the neighborhood boundaries were disbanded. Hooker Oak School is housed in a 70 
year old building in a family neighborhood in Chico. 
 
 
 
Hooker Oak School is a District and State of California Alternative School of Choice which serves pupils from all over Chico. The eighteen 
classes are self-contained and many are looped where teachers follow the students. Grades 1 & 2, 4 & 5 are looped. Grades TK,K, 3 
and 6 are self-contained and do not loop due to the individuality of the programs. Instruction is delivered in a thematic, integrated 
instructional strategy with the opportunity of several "Being There" (field trip) experiences. Parental involvement is encouraged for at 
least two hours per week (on average) in the program. 
 
 
 
The "Hooker Oak School Mission and Vision" was re-vitalized in the spring of 2012. It represents the goals and philosophy of the school. 
The Vision simply stated is "Growing a Community of Learners. Growth through the joy of creativity and discovery. Growth through 
educational excellence. Growth through positive life choices." The vision is expanded through the school's mission statement: "The 
learning environment at Hooker Oak will promote student participation in a variety of authentic and engaging curricular activities 
which foster the development of the whole child. Students will demonstrate a proficiency in the curriculum standards while being 
supported in a nurturing educational program, balanced with high expectations for accountability, shared by students, teachers, and 
parents."