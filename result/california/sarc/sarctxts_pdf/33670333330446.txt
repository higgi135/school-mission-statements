Centennial High School will prepare students for post-secondary education and the work force by creating a rigorous and relevant 
learning environment that meets the academic, social, and emotional needs of all students. 
 
Centennial High School Vision Statement: 
 
Centennial High School seeks to develop and support the emotional, social, and academic needs of all our students by offering diverse 
program options that provide students with opportunities to develop in a safe and productive environment. In order to support the 
needs of its diverse student populations, Centennial engages in partnerships with local businesses and post-secondary institutions to 
prepare students for the 21st century workforce. Furthermore, Centennial High School staff will remain accountable in their efforts to 
raise student achievement and provide learning environments where continuous student improvement is part of the school culture 
through the use of Professional Learning Communities. Finally, staff members will participate in a multitude of ongoing professional 
development opportunities, provided on and off site, in order to remain up to date on current best practices with a focus on improving 
teaching and learning for all students. 
 
 
Located in the foothills of Corona, in Riverside County, Centennial High School is a large, comprehensive high school serving an 
ethnically diverse student body of 3,200. Recent statistics show our current demographics of the school are 55% Hispanic, 21% White, 
10% African-American and 13% of various Asian, Hawaiian, European, and Middle Eastern ethnicities. There are currently 32 different 
languages spoken by students on campus. Students come from families that primarily reside in single-family detached homes, 
although there are many who live in apartment homes surrounding the school. Currently, approximately 55.5% of the students qualify 
for free or reduced lunch. 
 
Student academic levels are almost as diverse as the ethnicity of our students. Over 1600 students are enrolled in at least one honors 
class, including Advanced Placement and International Baccalaureate/Middle Years Program courses. Over 200 students are 
participating in the Puente program and another 375 students are enrolled in our AVID program. Approximately 200 students are 
English Learners and 330 students are receiving services through Special Education that includes Basic, Essentials, Intensive 
Intervention, and Severely Handicapped Programs. 
 
All students receive California State Standards-based instruction in core curricular classes, although we are transitioning to Common 
Core, and have the opportunity to participate in elective programs including AVID, Puente, Career Technical Education, and Visual and 
Performing Arts, and student leadership classes such ASB, Peer Counseling, Renaissance, Link, and Unity.