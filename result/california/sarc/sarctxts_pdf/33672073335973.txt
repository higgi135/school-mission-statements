Thank you for taking the time to review Perris High School's School Accountability Report Card. In 
accordance with Proposition 98, every school in California is required to issue an annual School 
Accountability Report Card that fulfills state and federal disclosure requirements. Parents will find 
valuable information about our academic achievement, professional staff, curricular programs, 
instructional materials, safety procedures, classroom environment, and condition of facilities. 
 
The school motto at Perris High is "Never Give Up." We encourage our students to constantly 
challenge themselves and grow from those challenges. As Perris Panthers we take PRIDE in 
everything we do. We are Positive Respectful Individuals Dedicated to Excellence. Our ongoing 
focus is to ensure our students are college/career ready when they walk out our doors as graduates. 
We offer a variety of instructional programs with emphasis on academics as well as career technical 
education. Perris High School believes that all students should have the choice of where to go and 
what to do with their lives. It is our responsibility to provide them with the academic, social, and 
emotional support necessary to enable them to be good decision makers that have a positive 
impact on the world around them. 
 
Perris High School provides a warm, stimulating environment where students are actively involved 
in learning academics as well as positive values. Students receive a standards-based, challenging 
curriculum by dedicated professional staff based on the individual needs of the students. Ongoing 
evaluation of student progress and achievement helps us refine the instructional program so 
students can achieve academic proficiency. We have made a commitment to provide the best 
educational program possible for Perris High School's students, and welcome any suggestions or 
questions you may have about the information contained in this report or about the school. 
Together, through our hard work, students will be challenged to reach their full potential. 
 
Juan Santos, Principal 
 
 
PHS Mission Statement: 
Perris High School's professional educators will provide a broad-based curriculum that affords our 
students opportunities to acquire the skills needed to understand and contribute to an 
interdependent and rapidly-changing world. 
 
PUHSD Mission Statement: 
The mission of Perris Union High School District is to create high quality relevant learning 
opportunities for all in a safe and caring environment. We will develop a high quality, caring staff 
who will be dedicated to learning, and connect students to their education and potential goals. We 
will care for all students while developing a growth mindset through collaboration, creativity, 
communication, and critical thinking. 
 
 

2017-18 School Accountability Report Card for Perris High School 

Page 1 of 13 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Perris High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

103 

98 

88 

0 

0 

0 

0 

2 

0 

Perris Union High School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

431 

7 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Perris High School 

16-17 

17-18 

18-19