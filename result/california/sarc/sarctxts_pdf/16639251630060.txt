The mission of Hanford Night Continuation is to provide alternative education opportunities for high school students outside of the 
comprehensive school setting. The staff is dedicated to providing programs and curriculum that meet common core state standards, 
and appeal to student interest, aptitude, and competencies. The vision of Hanford Night Continuation is that we are an educational 
community committed to providing all students with a competitive education and multiple learning opportunities in a safe, orderly 
environment, fully equipped to teach 21st century skills. As a learning community, we strive to improve student achievement, to be 
responsive to community needs, and to foster accountability for all. Part of our establishment of a restorative justice program is 
developing a posture, which represents the characteristics that we desire our students to emulate. These characteristics, this posture, 
is also created to drive the actions of the staff, parents, and students; all stakeholders. Our staff developed and adopted the posture 
of KNIGHT- Knowledgeable, Noble, Integrity, Grateful, Hard-working, and Trustworthy. We will develop as a staff a plan to roll out this 
posture January of 2019. 
 
The school’s purpose is to provide alternative educational opportunities for students who are 16-18 years old in need of unit recovery 
and specialized instruction. For general education student needing additional time to graduate we offer a fifth year option as well. The 
school serves those who cannot attend the larger, comprehensive high school due to extenuating circumstances. Hanford Night 
Continuation High School is an alternative high school setting during the afternoon/early evening. HNC High School consists of a 3 
period day with students participating in one period of core or elective classes. The other two periods are focused on credit recovery. 
We have a reduced credit requirement for graduation. Due to adjustments in curriculum we are not an A-G school. There are five full-
time teachers and one counselor assigned to HNC, there are an additional three hourly certificated employees and 2 classified 
employees that are assigned as well. Lack of credits is the overarching reason students are referred to the alternative education 
programs. The minimum credit loss to qualify is 25 credits.The school’s program is designed to assists students in recovering needed 
credits. Juniors have the ability to go back to their home school if they reach 155 credits at the end of the fall Junior semester and 195 
credits prior to the start of their Senior year. Seniors starting in alternative education will finish in alternative education.