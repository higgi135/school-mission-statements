Welcome to Sherwood School, home of the Sharks. In this document, you will find detailed 
information about your child’s school. Please take the time to peruse our School Accountability 
Report Card to learn about the many programs that make Sherwood a great place to learn and 
grow. 
 
Sherwood School is a professional learning community that holds student achievement as its 
primary focus. Our school wide goal is for all Sharks to meet or exceed their grade level California 
Common Core Standards in all curricular areas. We place particular emphasis on the core academic 
areas of language arts, mathematics, and English language development. We also recognize that 
the social development of each child is unique and critical to growing citizens who will work 
together to make Sherwood and the world a better place. We are a community school that actively 
seeks to involve our parents in the educational experience of our students. We welcome you to be 
a part of our group of volunteers who play a role in making Sherwood an outstanding community 
center. As you read through this report, please consider ways in which you can contribute to your 
child’s school experience. Welcome to the home of the Sharks. 
 
Our vision is that through shared accountability for all of our students throughout the school, a 
strong core instructional program, research validated intervention programs, and a well-developed 
program improvement process we will become an exemplary school in serving our student 
population and community. Team members will ensure that every student learns grade level 
curriculum and is supported in her/his need for assistance in meeting California content standards 
as they reflect the Common Core Standards. We are a caring and committed staff who will regularly 
reflect upon: 

• Breaking down barriers that impede school-wide teamwork between all members of 

our school community (including parents) 
Essential learning goals and curricular content 
Successful instructional strategies and techniques 

• 
• 
• Analysis of student need to drive instruction and positive behavior support 
• Professional development to exemplify education as a life-long opportunity 
Social development as a key to becoming caring and conscientious citizens 
• 

 
 We have demonstrated excellence in our Response to Intervention model and take pride in our 
ability to provide highly differentiated learning for our students' wide range of ability levels. Our 
shift to a standards-based curricular program, combined with small group instruction, can be also 
credited as contributing factors to our students' success. In order to support transition to the 
common core standards, Benchmark, Designated ELD instruction, Integrated ELD instruction, small 
group instruction and Lexia was introduced to provide staff support. Sherwood continues to be a 
district leader with regard to Response to Intervention (RTI). Our kindergarten team continued a 
morning-only schedule and thus was able to provide highly qualified academic support for our 
Kinder extended intervention program. The focus is to target and support Kindergarten students in 
the area of early literacy skills. Kindergarten teachers received .5 hour of instructional aide support 
on a daily basis. The instructional aides received training in early literacy instruction, which 
increased their ability to remediate learning gaps in young learners. First and second grade students 
received the support of our small group instruction intervention program, Imagine Learning, LEXIA, 
Individual Student Folders, and teacher led targeted after school interventions. 

2017-18 School Accountability Report Card for Sherwood Elementary School 

Page 1 of 10 

 

Third through sixth grade students that are not meeting standards benefit from our Systems 44, Rewards, Imagine Learning, and LEXIA 
intervention programs. Our after school program, Best and Education Safety Time (BEST), also provided student support in academic, 
enrichment and recreational programs. Overall, these many supplemental services are highly beneficial to our students. 
 
Sherwood continues with the implementation of PBIS or "Positive Behavior and Intervention Support" as a school program to assist our 
students in learning appropriate behaviors. This program has been implemented to create school-wide expectations, rules and 
procedures. OLWEUS anti bullying program was implemented and staff took place through out the year to built capacity among all 
stakeholders. The implementation of Playworks at Sherwood School resulted in national recognition as a Playworks model school. All staff 
participate. Sticks and Stones counseling provided services to at risk students on a 1:1 basis. An additional counselor position was provided 
through the OLWEUS anti bullying program. This counselor worked with at-risk students in a 1:1 counseling setting; this counselor also 
provided character education to all classrooms. We also implemented our Student Peer Mediator's program and Safety Patrol Program 
supported and run by students and supported by staff and parents.