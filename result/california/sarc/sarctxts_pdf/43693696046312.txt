Cassell School staff is dedicated and determined to meet the state and federal government’s goal of 100% proficient in reading 
language arts and math. We consider our school to be special; the majority of our teaching staff has over 15 years of teaching 
experience, and they are talented teachers. All teachers work to provide additional instruction to students who need support. After 
school and enrichment programs such as City Year, YMCA, and Little Heroes help to make our students well rounded citizens during 
their tenure here at Cassell. 
 
Our goals for the school year include: 
Increasing the percentage of proficient and advanced students in all grade levels in order to make them college and career ready. 
Building a strong and solid relationship between the school, staff, students, parents and community Encouraging parent participation 
in all aspects of the school. 
Increasing student motivation and dedication to achieving academically. 
Teaching students to be nonviolent and drug free. 
 
Our friendly and competent Cassell staff focuses on creating and building a well-rounded multicultural atmosphere that nurtures and 
develops well prepared citizens for the future. We encourage parent participation and communication. It is truly the best way for 
children to succeed. Always remember that we have the best interest of the children in mind. Everything we do is geared towards our 
student’s future. As an instructional leader I encourage you to take an active role in the future of your child’s education.