The mission of the Cabrillo Unified School District, an exemplary learning community committed to valuing our distinct cultural 
identities, is to develop critical thinkers and socially responsible citizens who actively participate in their individual learning and 
development fully prepared to embrace their next challenge. We accomplish this by utilizing our unique natural resources and 
engaging the entire Coastside community and its partners in providing a rich and academically rigorous curriculum in a safe and 
nurturing environment. 
 
El Granada Elementary School is a diverse school community that fosters the academic and social development of each student. Among 
the academic highlights are a school-wide commitment to Reading and Writing Workshop, a robust implementation of hands-on 
science including a wonderful lab for 4th and 5th grade students, the high level use of computers and technology in all content areas, 
and a 40-year tradition of celebrating Arts and Science Day. El Granada has an extended day Kindergarten enrichment program 
beginning each December. The El Granada parent community is highly involved as evidenced by the PTO's commitment and support 
of enrichment programs and school resources and materials including classroom supplies and playground equipment. In addition to 
funding our Kindergarten through third grade PE and Music program, the PTO also supports the Art in Action program that teaches 
art technique and history through the examination great and world recognized masters. Teachers and parents partner to make this a 
valuable learning experience. 
 
Some of the highlights of our instructional program include Teacher’s College Reading and Writing Workshop implemented in all 
classrooms; 1:1 Technology in all classrooms; summer pre-kindergarten program to promote school readiness; K-5 physical education 
program that emphasizes movement and appreciation for physical and mental health; library technician that maintains the library, 
reads literature to students, and assists with classroom research; science specialists of upper grade classrooms; full time Reading 
Specialist to support instruction; a music program that addresses vocal, theory and instrumental music instruction and an award 
winning after school program for fourth and fifth grade students.