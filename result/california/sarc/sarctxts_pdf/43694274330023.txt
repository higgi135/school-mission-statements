Santa Teresa High School focuses on the "Saints Way"--Social Emotional Learning, Content Area 
Mastery, Performance, and Assessment, and Educational Empowerment. It is the mission of Santa 
Teresa High School to provide a safe and caring learning environment where students achieve the 
academic, aesthetic, personal, and social development required to continue learning and pursuing 
post secondary education, to compete in a changing job market, and to participate in a 
multicultural, democratic society. 
 
Santa Teresa High School is home to more than 2150 amazing students who mature into fantastic 
young adults due to academic growth and personal achievement. With the assistance of nearly 
150 staff members, the students at Santa Teresa High School have a variety of academic and extra-
curricular opportunities that make an excellent high school experience. Santa Teresa High School 
offers 24 Advanced Placement courses (American Government, Art History, Biology, Calculus AB, 
Calculus BC, Chemistry, Computer Science A, English Language, English Literature, Environmental 
Science, French Language, Macro Economics, Physics 1 and 2, Principles of Computer Science, 
Psychology, Spanish Language, Spanish Literature, Statistics, Studio Art: 2D, Studio Art: Drawing 
and Painting, US History, and World History), a variety of World Languages (American Sign 
Language, French, Spanish, and Vietnamese), a variety of Performing Arts (Concert Band, Drama, 
Film Studies, Guitar, Jazz Ensemble, Marching Band, Musical Theater, Technical Theater, and Wind 
Ensemble), a variety of Visual Arts (Art, Crafts, Digital Photography, Drawing and Painting, and 
Multimedia) and two Career Technical Education pathways: Computer Science and Multi-Media. 
In addition, Santa Teresa High School boasts an award winning Leadership program, nearly three 
dozen different student clubs, an award winning International Relations Club, an award winning 
Marching Band, an award winning Robotics program, and an award winning Spirit program. For 
our students who need a little extra help, Santa Teresa High School offers tutorial three days a week 
during the school day; co-taught Biology, co-taught English 1, co-taught Physics, co-taught 
American Government, and co-taught Economics; a comprehensive tutoring map; a plethora of 
tutoring programs available throughout the week, and our Student Family Center. Athletically, 
Santa Teresa High School is one of the few schools with nearly all of the teams competing in the 
highest division and routinely competing in the various CCS tournaments. 
 
Santa Teresa High School is a tremendously safe campus due to the vigilance of all students and 
staff and the support and involvement of our families. Drugs and weapons are not a part of the ST 
culture. Also, Santa Teresa High School is blessed with tremendous parent and guardian support. 
Santa Teresa High School would not enjoy success without the continued support of the Santa 
Teresa Organization of Parents and Staff (STOPS), the Santa Teresa Athletic Booster Club (STABC), 
the Santa Teresa Music and Arts Association (STMAA), the African American Student Advocates 
(AASA), and the Latino Parent Coalition (LPC). 
 
Welcome to the home of the Saints! 

2017-18 School Accountability Report Card for Santa Teresa High School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Santa Teresa High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

111 

95 

93 

3 

0 

1 

0 

3 

0 

East Side Union High School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

991.5 

50.6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Santa Teresa High School 

16-17 

17-18 

18-19