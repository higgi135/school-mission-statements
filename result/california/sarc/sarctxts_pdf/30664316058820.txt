Vision Statement: Dale Junior High School is a learning community preparing students for the 21st century. 
 
Mission Statement: Dale Junior High School is to provide a caring environment which allows every student the opportunity and skills 
to LEAD. 
 
Highlights: 
Dale Junior High School is a collaborative learning community, preparing students for the 21st century. Dale Junior High School offers 
a rigorous curriculum and many opportunities to explore elective classes. Dale Junior High School has a strong after-school program 
with a variety of opportunities for exploration, including sports, and tutoring. Teachers collaborate weekly to refine curriculum and 
instructional methods in their professional learning communities. 
 
In 2015, Dale Junior High School received a Gold Ribbon award for LEAD time, which is its intervention/enrichment period. LEAD stands 
for Leadership, Effort, Academics, and Determination. The staff recognized a need for student support and figured out a plan to help 
students. The LEAD program offers students choices for intervention and enrichment, and helps students reduce D/F grades. The LEAD 
time advisement period is used for reteaching content, retaking tests, make-up work, completing homework, or enrichment. It is a 
great opportunity for students to receive extra help from teachers. LEAD time supports students during the school day and shows kids 
that we care about their success.” 
 
Demographic Information: 
Dale Junior High School, located in Anaheim, California, serves 1,087 students, in which 87% participate in the free and reduced meal 
program, and 29% are English Learners. The demographic profile also indicates the following regarding student subgroups: 76% 
Hispanic, 7% White, 9% Asian, 3% Filipino, 2% African American, 2% Native American/Pacific Islander, 1% other.