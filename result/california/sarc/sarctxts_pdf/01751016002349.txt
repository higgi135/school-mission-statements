It is my pleasure to welcome you to Valley View's Elementary School Annual Accountability Report 
Card. Valley View is home of the Mustangs and our vibrant community of learners. Valley View is 
located in the eastern region of Pleasanton and serves students in transitional kindergarten through 
fifth grade following a traditional calendar. At the beginning of the 2018-19 school year, 655 
students were enrolled. The English Only and Spanish Dual Immersion programs here at Valley 
View each serve approximately half our school's population and are strongly committed to 
equitable learning, academic excellence, and social development for all students. Our experienced 
and fully credentialed staff meets the needs of ALL of our students through MTSS, multi-tiered 
systems of supports. Tier 1 includes interventions and enrichment in the classroom, Tier II supports 
are offered during our Universal Access time and Tier III supports are offered through our Special 
Education department. The curriculums used for both our DI and EO programs are aligned with the 
California State Standards and frameworks, and state compliance criteria. 
 
Honored in 2003 with the CABE (California Association of Bilingual Education) Seal of Excellence 
and in 2006 as a California Distinguished School, Valley View continues to stand out as a school that 
is student-centered focused on academic achievement for all children. Our dedicated staff 
continually reassesses our academic outcomes and refines our teaching and learning practices 
around student success. 
 
Valley View and all schools in Pleasanton Unified set very high expectations for student 
achievement and continue to implement Best Practices in Teaching with Common Core State 
Standards (CCSS). The Common Core aims to prepare all students for college and career readiness 
and as 21st Century Learners who will think critically and at deeper levels, and collaborate with 
others to become resourceful, responsible, and engaged world citizens. Valley View Mustangs will 
“Make a Better World!” 
 
Our faculty, School Site Council (SSC) comprised of teachers, staff, and parents, and English Learners 
Advisory Committee (ELAC); regularly reviews site assessment data to develop the School Plan for 
Student Achievement (SPSA) each year. Our main focus this year will be equity for all learners and 
the narrowing of the achievement gap that exists for our diverse student population especially our 
ELL (English Language Learners). 
 
Mission: We are a child-centered, forward thinking, learning community, educating each of our 
students in an individually appropriate manner within an environment of personal safety and 
mutual respect to become well-informed, productive, and socially responsible citizens. 
 

2017-18 School Accountability Report Card for Valley View Elementary School 

Page 1 of 9 

 

We Believe... 

Collaboration and open communication support a positive teaching and learning environment 

 All students are unique, can learn and deserve a challenging and enriching curriculum. 
 
 Working as a team leads to high levels of achievement and character development for students and staff 
 
 Maintaining a safe environment, physically and emotionally, creates a positive place for students to learn and staff to work 
 Having a sense of humor and pursuing the joy of learning creates a positive school culture and climate 

Parents play a vital role in their child's education and are valued partners in this effort 

 
Our school community benefits from active parent and community participation in regular and extracurricular school activities. Many 
other positive school activities rely and depend on parent volunteers who help with school fairs, school beautification projects, and other 
special events. Valley View’s PTA coordinates year-round fundraising to foster school and community efforts that support powerful 
teaching and learning. Friends and families are always invited to become active members of Valley View’s Community of Learners. It is 
with that same enthusiasm and pride that we say to you, "Welcome to the View!"