Maple Elementary School is privileged to serve over 650 Kindergarten through fifth grade students. 
Our theme is "Be an A.C.E!" (Academics + Character = Excellence). Our staff is dedicated to the 
pursuit of academic excellence and positive character development for ALL students. We stress the 
"value of work" and the "value respect" in order to guide students toward success. We provide an 
enriching instructional program and a supportive learning environment to form the basis for 
lifelong personal growth. 
 
Staff members at Maple School encourage parents to be involved in their child's education. We 
encourage parents to participate in Parent/Teacher Conferences and average over 95% 
participation. Parents may help make decisions at school by participating in the Parent/Teacher 
Organization (PTO), School Site Council (SSC), English Language Learners Advisory Council (ELLAC) 
and/or our annual survey. 
 
Maple School also has several Family Nights planned that offer parents and families opportunities 
to interact with one another and have fun. Examples of the Family Nights are the Fall Harvest 
Festival, making Gingerbread Houses in December, a Valentine Social, and a spring skate night. 
Maple School also sponsors a Back-to-School Night and an Open House. Parent Coffee Clubs are 
also offered to parents throughout the year and cover various topics. 
 
Maple School follows a Comprehensive School Safety Plan to ensure the safety of all students, staff 
members and visitors on the Maple campus. The safety drills are reviewed with staff and students 
and practiced monthly. After the opening of school, all school gates are locked and visitors must 
enter the school through the office. At that time, a visitors pass must be obtained before any guests 
are allowed into a classroom or onto the campus. The school safety plan is reviewed regularly and 
input for improvement is given from members of the SSC/ELAC. 
 
Professional development is given to all teachers, where improving instruction is the focus. 
Teachers also have the opportunity to improve their instruction by meeting as a grade-level or cross 
grade to discuss best instructional practices. Grade levels also meet regularly to analyze student 
data and discuss instructional strategies that are successful. When needed, demonstration lessons 
by fellow teachers, district resource teachers, or Maple School's principal are also utilized. We 
believe that the staff development offered to staff has been an integral component of our growth 
and success of moving all students toward proficiency. 
 

2017-18 School Accountability Report Card for Maple Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Maple Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

32 

30 

29 

1 

0 

2 

0 

4 

0 

Tulare City School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

450 

27 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Maple Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.