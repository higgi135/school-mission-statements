Old Adobe Elementary Charter School is located on the outskirts of Petaluma, California and serves 346 students in grades TK-6th 
grade. We are a close-knit community of staff, students, and families working together to help children grow into knowledgeable and 
responsible people. 
The mission of the Old Adobe Elementary Charter School (OAECS) is to provide an educational experience that creates knowledgeable, 
caring, and responsible young people. The curriculum at OAECS is well-rounded and infused with the arts while establishing values 
leading to responsible treatment of the environment. The OAECS will produce students who can express themselves in many ways 
and have an understanding of ecology and their place in the environment. 
Students will use dance, drama, visual arts, theater, writing, technology and other means to demonstrate their learning and express 
their creativity. Our students will know how to be leaders in learning situations, treat those around them with respect, resolve conflict, 
and become part of their community. OAECS will instill in our students the required learning as well as foster creativity and 
responsibility.