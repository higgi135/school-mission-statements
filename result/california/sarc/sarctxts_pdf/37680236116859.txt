Arroyo Vista Charter School (AVCS) opened in July 1999 serving a K-6 population. Arroyo Vista is 
one of 45 elementary schools and 2 independent charters in the Chula Vista Elementary School 
District. There are seven permanent pods and 12 and a half relocatable classrooms on the Arroyo 
Vista campus. Arroyo Vista serves students from Transitional Kindergarten through eighth grade. 
 
Arroyo Vista's K-6 Spanish-English Dual Language Immersion Program was designed as a 90/10 
Dual Immersion Model. This model was selected to provide students with optimal opportunities 
to become bilingual and biliterate. A lottery process is used to select students for the program and 
half of the students are English Only and half are bilingual Spanish/English. The bilingual students 
serve as role models in Spanish. 
 
Beginning in July 2011, Arroyo Vista expanded to include a Middle School. Students hailing from 
22 different elementary sites enroll in Arroyo Vista's middle school each year. These students 
scored exceptionally well on the CAASPP assessment. In 2017 96% of seventh graders either met 
or exceeded in English Language Arts and 89% either met or exceeded in Math. In Eighth grade 
88% either met or exceeded in ELA and 85% either met or exceeded in Math. Middle school 
students have a choice of varied electives, such as, Archery, Kitchen Science, Photography, 
Robotics, Yearbook and Sign Language. 
 
Our school-wide goal for the year 2017/18 is to implement Common Core State Standards (CCSS) 
across all areas of the curriculum with a special emphasis on Math and to ensure that assessment 
practices match the Smarter Balance Assessment. Professional development for staff this year 
placed emphasis on Technology in the classroom, Mathematical Practices and closing the 
achievement gap for English Learners, Students with Disabilities and Socio-economically 
Disadvantaged Students. The CAASPP Assessment was administered in spring of 2017 to students 
in grades third through sixth. These students did exceedingly well. In grades 3 through 6, 75% either 
met or exceeded in ELA and 65% either met or exceeded in Math. 
 
Local Measures Assessment was given to Kindergarten through second grade students at the end 
of the school year. In kindergarten, 66% met in Reading; 67% met in Writing and 96% met in Math. 
In First grade 84% met in Reading; 67% met in Writing and 85% met in Math. In second grade, 89% 
met in writing and 87% met in Math. Reading has a different assessment. 
 
Mission 
Arroyo Vista Charter School is a central component in the EastLake community where it is located. 
The goal of staff and parents is to create lifelong learners and to have ALL students acquire the 
necessary skills to make them college and career ready for the twenty-first century. Our motto, 
Together Everyone Achieves More (T.E.A.M.), reflects our approach to learning. Staff, students, 
parents and community work diligently to establish a foundation of collaboration, cooperation and 
collegiality within which high expectations are established to promote student success. AVCS’ staff, 
students and parents believe in encouraging mutual respect, the sharing of knowledge and 
expertise and in developing an appreciation for life experiences. Diversity is embraced and 
celebrated throughout our community of learners. With the “whole child” as our focus, the AVCS 
T.E.A.M. is committed to ensuring that every student participates to the fullest extent possible to 
achieve educational and social success. 
 

2017-18 School Accountability Report Card for Arroyo Vista Charter School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Arroyo Vista Charter School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

36 

0 

Teaching Outside Subject Area of Competence 

NA 

43 

37 

0 

0 

0 

 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Arroyo Vista Charter School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

 

 

 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.