Hilltop High School’s Mission Statement: 
Hilltop High School will provide an academic, supportive, and collaborative culture that is safe and inclusive so that each student may 
become an advocate for his or her own success. 
 
OUR BELIEFS 
The Hilltop High School faculty and staff commit to the Hilltop High School mission statement based upon the following beliefs: 
 
We believe in solution-oriented, effective collaboration between all stakeholders. 
We believe in understanding each student’s needs and providing support on their road to success. 
We believe in promoting a positive and equitable school culture through inclusion, support, and involvement. 
We believe in cultivating a drive for continuous learning through active engagement and growth opportunities. 
We believe in community engagement that provides further opportunities for student success. 
 
Hilltop High School is one of twelve comprehensive high schools in the Sweetwater Union High School District. Along with eleven 
middle schools, one continuation school, several alternative education programs, and five adult schools, Hilltop High School is part of 
the largest secondary school district in the nation. Hilltop High School was recognized by Educational Results Partnership (ERP) and 
the Campaign for Business and Education Excellence (CBEE) for high achievement in student success as one of 1,866 public schools in 
California to receive the title of 2016 Honor Roll Star School, a part of a national effort to identify higher-performing schools and 
highlight successful practices that improve outcomes for students and includes measures of college readiness. In 2017 U.S. News & 
World Report ranked Hilltop High School number 1,930 of over 19,000 eligible schools in the U.S., and number 373 within California. 
This silver-ranking is based on our performance on state-required tests and how well we prepare students for college. 
 
At the core of our success is the variety of programs available to students. The Foreign Language and Global Studies (FLAGS) magnet, 
Academies of Hospitality and Tourism (H&T) and Information Technology (IT) programs enable students to pursue education through 
specialized pathways. 
 
The successful academic record at Hilltop High School is a result of various academic, support, and enrichment programs offered to 
students including: Accelerated, Honors, Advanced Placement (AP), Advancement Via Individual Determination (AVID), Structured 
English Immersion (SEI), Designated Academic Language Development (ALD), Prep for Success, Career Technical Education (CTE), Visual 
and Performing Arts (VAPA) courses, and Academic Decathlon. Students with special needs are served through several options 
including the Learning Center and RSP Learning Center, an on-site alternative for at-risk students, and the Moderate-to-Severe (MOS) 
program which provides vocational, functional, and daily life skills for students with developmental delays and limitations. 
 
Hilltop also offers a variety of enrichment programs such as Regional Occupation Program (ROP), Visual and Performing Arts courses, 
Photography, Drama, Band, and unique courses such as Guitar and Academic Decathlon. There is an active student organization (ASB) 
with over 65 clubs. A full array of sports for boys and girls is offered with participation increasing each year to approximately 50% of 
the student population. Trained peer mediators in two Connect Crew classes are available to assist students as needed with academic 
and social issues. 
 
In 2013-2014, Hilltop High School was one of several schools in our district that received a 21st Century After School Safety and 
Enrichment for Teens (ASSET) grant. The primary purpose of the 21st Century High School ASSET Program is to offer students a broad 
array of services, programs, and activities before and after school. These services, programs, and activities are designed to reinforce 
and complement the regular academic program of participating students. The ASSET grant makes it possible to offer a rich menu of 
after school academic and recreational activities. The program runs from 2:45 p.m. to 6:00 p.m., five days a week. The menu of services 
is updated weekly and shared with staff, students, and community members. 
 

2017-18 School Accountability Report Card for Hilltop Senior High 

Page 2 of 13 

 

In fall of 2012, Hilltop High School was one of four schools in the district that became part of the Chula Vista Promise Neighborhood 
(CVPN) grant. The CVPN is a federally funded program that provides comprehensive approaches to changing the odds for our 
struggling, most at risk children. Coordinated by South Bay Community Services, CVPN brings together a collaboration of partners 
focused on family, education, health and community to inspire all children in the designated areas to achieve academic excellence and 
aspire to a college and career track.