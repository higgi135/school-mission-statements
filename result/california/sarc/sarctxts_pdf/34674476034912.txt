MISSION STATEMENT 
Skycrest participated in a comprehensive strategic planning process during the winter and spring of 2011. A diverse group of staff, 
parents, and community members developed the following mission statement that was formally adopted by Skycrest Site Council in 
May of 2011. In October 2013 the Strategic Planning team reconvened to review the progress of the School Strategic Plan. 
Recommendations for change were made with Action Teams reconvening in January 2014 to review the tactics. The Strategic Planning 
Team and Skycrest staff reviewed the data from the Action Teams in May 2014 and information was brought to School Site Council in 
September 2014. 
 
The mission of Skycrest Elementary School, valuing academic excellence, positive character development, and diversity, is to inspire 
every student to responsibly contribute to an ever-changing world by engaging in an innovative curriculum that challenges each 
individual’s learning within a culturally enriched environment in partnership with families, school, and community. 
 
SCHOOL PROFILE 
Skycrest Elementary is one of 41 elementary/K-8 schools in the San Juan Unified School District. The curriculum provided is aligned to 
the Common Core State Standards. Staff at Skycrest implement the Common Core State Standards (CCSS) in their teaching and in 
designing and establishing their daily lessons. The school supports cultural awareness on a daily basis through its diverse literature 
selections and other school activities. Through our character education program, Skycrest places a heavy focus on establishing 
students with strong, positive character traits. Grade levels present at each assembly, focusing on one of the specific traits. Monthly 
character assemblies are held with students recognized for demonstrating these traits every day. 
 
Skycrest has a BRIDGES After-School program that is funded by the 21st Century Grant and ASSETS. This program provides after school 
enrichment, homework completion time, structured physical education games, and parent activities. The BRIDGES program is offered 
at low cost to families. 
 
To support our English Learner students, Designated English Language Development (D-ELD) is planned daily, with students receiving 
support in their designated groups: expanding, emerging, bridging and enriching. All teachers within each grade level provide D-ELD 
and integrated ELD strategies in all core content areas throughout the day. 
 
The focus for our professional learning cycle will be Comprehensive Balanced Literacy, specifically writing, and math problem solving, 
with an emphasis of incorporating grade level standards and increasing of the Depth of Knowledge (DOK) rigor that is presented within 
each class and throughout the grade level. Designated English Language Development (ELD) is scheduled for all grade levels with 
English Learners (ELs) placed in groups to support them at their English Language level. Further collaboration around Writing and Math 
are planned to support students in writing throughout the day and throughout all curricula areas and to support students in both a 
conceptual and procedural understanding of math. Data release days are scheduled around guided reading, running records, text 
levels, and writing with a focus around common understanding and norms throughout each grade level. The California Assessment of 
Student Performance and Progress (CAASPP) data shows that focus on increased language development is necessary for all students-
-both English Learners (ELs) and English Only (EO) students. With approximately 50% of our students population being English Learners, 
it is our top priority to increase vocabulary acquisition and oral language for all students. Continued professional development will be 
presented to support teachers in using strategies and techniques that will support all students in the area writing and math. 
 
Skycrest School Objectives: 

• All students will develop and apply 21st century skills such as problem solving, critical and relative thinking, collaboration, 

• 

and the application of technology. 
Each year, all students will achieve at least one year's growth towards grade level Common Core Standards as determined 
by multiple measures. 

• We will model and integrate positive character traits within our community. 

2017-18 School Accountability Report Card for Skycrest Elementary School 

Page 2 of 12 

 

Tactics: 
1: We will effectively utilize innovative instructional strategies, data analysis, and technology to increase student achievement and 
academic success in alignment with Common Core State Standards. 
 
2: We will actively engage and build trusting relationships with students, families, and our diverse community to create a unified, 
collaborative learning environment focused on academic success and the social/emotional well-being for each student by utilizing the 
Skycrest Character Traits and engaging families as valued partners in the educational process.