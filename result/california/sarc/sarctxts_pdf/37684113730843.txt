A 1999 and 2005 California Distinguished School, and 2015 a 6-year clear WASC accredited school. Eastlake High School is a four-year 
comprehensive high school. It was opened in the fall of 1992. Eastlake High School had its first graduating class in June, 1995. Language 
programs include Spanish, French, Filipino, and Japanese. Advanced Placement classes are offered in English language and literature, 
calculus, Spanish language and literature, U.S. history, economics, art, art history, government, biology, physics, chemistry, statistics, 
Japanese language, French, and world history. The Ruth Chapman Center for the Performing Arts, the finest performing arts center in 
the South Bay, is home to Eastlake music and drama programs. 
 
Eastlake High School operates on a modified year-round schedule that begins in late July and has a block schedule for students who 
take three classes a day, similar to a college schedule. Students have the opportunity for tutoring two times a week during ELP 
(Extended Learning Period). After-school tutoring is also available for students on Monday through Thursday during the Titan 
Homework Zone. The school library houses 18,000 volumes. The school’s Pre-Engineering Program received a Golden Bell Award in 
2006. The school offers an extensive CTE program. A highly successful robotics program is also in place. 
 
Students in the Sweetwater Union High School District are expected to master state and district standards which will prepare them to 
meet the challenges of the 21st century. 
 
The mission of Eastlake High School, where today’s learning shapes tomorrow’s success, is to ensure a comprehensive educational 
experience that maximizes opportunities for student achievement through a system of learning distinguished by: 

Implementing a variety of educational pathways that include extracurricular options. 
Fostering a variety of partnerships within the community that support Eastlake students. 
Providing a safe, student-centered learning environment. 

• Creating a supportive environment that joins students, staff, parents and the Eastlake community in the educational process. 
• Maintaining a comprehensive network of support systems that recognizes the needs of each student 
• Developing skills necessary to succeed in the work place and higher education. 
• 
• 
• 
• Building a culture of the Titan way – Respectful actions, attitude and language. 
• 
• Valuing the diversity of people and ideas. 
• Our professional staff and involved community are committed to encouraging the emotional and social development of 
students through understanding and respect of multiple perspectives, diverse cultures, and individual responsibility so they 
become productive and capable citizens in an ever-changing world. 

Celebrating the achievement of Titans.