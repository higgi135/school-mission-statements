For over 10 years, The Wonderful Company, founded by Lynda and Stewart Resnick, has been working side-by-side with the residents 
of the Central Valley to impact positive, lasting change in the community, with education at the center of multi-generational change. 
The Resnicks have generously sponsored several initiatives to improve the community of Lost Hills - including preschools, a park and 
health and wellness programming for local youth. In 2017, alongside the community, the Resnicks founded Wonderful College Prep 
Academy Lost Hills, a free public charter school. The mission of the Academy is to graduate students who are prepared to earn a 
college degree and lifetime of promising career opportunities. 
 
The Academy currently serves approximately 200 students in grades K-4 and 6, with the intention of becoming a full K-12 school. In 
its first year, the Academy found many successes - with strong family engagement and student culture as evidenced by the high 
student retention numbers. The Academy offers students a well-rounded and holistic education with a focus on health and wellness 
and a comprehensive curriculum.