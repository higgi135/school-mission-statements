Built in 1962, Foothill High School is one of eighteen comprehensive high schools in the Kern High 
School District (KHSD), the largest high school district in California. Foothill’s boundary area is 
largely agricultural, located on the eastern edge of Bakersfield. Foothill’s attendance area includes 
established homes and rural areas south, east and west of the school. Foothill receives students 
from four school districts; Edison, Fairfax, Bakersfield City, and Caliente. From these four districts, 
Foothill receives the majority of students from six schools Five bus routes serve Foothill each day 
including one from Caliente, which is a 90-minute bus ride for students. Additional bus routes are 
provided for students in some special education programs. A majority of students who attend FHS 
are from low-socioeconomic areas. Approximately 85 percent of students who attend Foothill 
qualify for the free or reduced meal program. 
 
Beginning in the 2016-2017 school year, after much conversation, research and data analysis, FHS 
began to transition from International Baccalaureate Program to the Advanced Placement (AP) 
Program. Factors leading to the change included increasing cost to the school and students, 
declining number of students taking the exams, and declining number of colleges accepting IB exam 
scores. Based on our data, students in AP will earn college credit for passing the exam at a greater 
rate than IB. Advanced Placement classes began in junior year classes, as seniors needed the 
opportunity to complete their IB program. Classes for seniors began in the 2017-2018 school year. 
A sophomore level AP course was added in the 2018-2019 school year. FHS currently offers 
fourteen AP classes spanning grades 10 through 12. The AP courses and grade level are: 
 
English Language – 11 World History – 10 Calculus – 10, 11, 12 
 
English Literature – 12 US History – 11 Statistics – 10, 11, 12 
 
Chemistry – 11, 12 Government – 12 2D Studio Art – 10, 11, 12 
 
Environmental Science – 11, 12 Economics – 12 French Language – 11, 12 
 
Spanish Language – 11, 12 Spanish Literature – 11, 12 
 
Foothill High School has several relevant Career Technical Education offerings including two 
California Partnership Academies; Agribusiness and Computer, Design and Engineering. Students in 
grade 9 can begin to take courses in one of seven pathways. The pathways include; Agribusiness, 
Ag Mechanics, Architectural Design, Business Management, Cabinetry and Millwork, Information 
Support Services, and Archiving. Students complete their pathway by taking introductory, 
concentration and capstone courses over four years. Foothill has the highest number of pathway 
completers in the district with 999 students completing a pathway in the 2017-2018 school year. 
Foothill is fortunate have strong community partnerships that support these programs. At this time, 
the students in the Cabinetry pathway, Architectural Pathway and Ag Mechanics Pathway are 
working collaboratively with the Kern County Museum to complete a project that will provide a 
children’s center with props designed and fabricated by FHS students. 
 

2017-18 School Accountability Report Card for Foothill High School 

Page 1 of 14 

 

FHS science teachers and students have developed a new partnership with the Valley Fever Institute of Kern Medical Center. This new 
partnership is bringing understanding and collaboration to our science students as they work with the medical professionals at the 
institute designing lessons and gathering data. About 40 of these students presented those lessons to Voorhies Elementary School (one 
of our feeders) for an after school, interactive event for parents and students to engage in lessons about the air and water quality in our 
valley. 
 
Foothill offers courses in a four-year AVID (Advancement Via Individual Determination) program. The goal of AVID is to prepare first 
generation college-bound students, who fall in the middle of the academic scale, for acceptance to and success in a four-year university. 
These students receive additional tutoring and assistance in SAT preparation and college applications. FHS dedicates teaching formula 
and administrative formula to the AVID program. A teacher/counselor receives one period of administration to coordinate the AVID 
program. Three teachers have one section each of AVID with one of them being a combination 11/12 class. 
 
APEX is a credit recovery course offered throughout the school day and after school. Students enrolled in APEX attend class and access 
content online. Students work at their own pace, completing units at school and at home. It is possible for students to complete more 
than one course during a semester making this a good option for transfer students and students who are deficient in credit to stay on 
track for graduation. 
 
A review of the school’s ESLRs began in 2016 with the Instructional Advisory Council. Due to changes in testing and accountability, the 
ESLRs were no longer effective to asses student learning. A committee formed to evaluate the effectiveness of the ESLRs and revisions 
were tasked with presenting to the Advisory council, administration and staff. In the process of creating the current Schoolwide Learner 
Outcomes from the previous ELSRs, the committee made recommendation to revise the Mission/Vision to reflect what FHS believes 
students should know and be able to do. 
 
Our current student enrollment is displayed in the chart below: 
 
2018-2019 Student Enrollment by Grade Level 
 
Grade Level Number of Students 
• Grade 9 602 
• Grade 10 508 
• Grade 11 492 
• Grade 12 473 
 
2018-2019 Student Enrollment by Group 
• Group % of Total Enrollment 
• Black or African American 6% 
• American Indian or Alaska Native 4% 
• Asian < 1% 
• 
• Hispanic or Latino 90% 
• Native Hawaiian or Pacific Islander 0% 
• White 6% 
• 
• 
• 
• 
 
Mission 
The mission of Foothill High School is to provide a safe and inclusive learning environment that educates, empowers and inspires all 
students to achieve high levels of learning and to become caring, respectful, responsible citizens who are college and career ready. 
 
Vision 
Foothill students will be academic achievers who utilize 21st Century skills to contribute to communities and global society. 
 
 

Two or More Races 0% 
Socioeconomically Disadvantaged 89% 
Students with Disabilities 11% 
Foster Youth <1% 

Filipino < 1% 

 

2017-18 School Accountability Report Card for Foothill High School 

Page 2 of 14 

 

Schoolwide Learner Outcomes 
 
Foothill students will exhibit: 
 
Effective Communication 

• 
• 

Express thoughts clearly in writing and speaking 
Listen effectively to decipher meaning, including knowledge, values, attitudes, and intentions 

Critical Thinking 

• Make decisions and solve problems 
• Analyze and evaluate evidence, arguments, claims, and beliefs 

Collaboration 

• Demonstrate the ability to work effectively and respectfully with diverse teams 
• Exercise flexibility and willingness to be helpful in making necessary compromises to accomplish a common goal 

Creativity 

Imagine innovative designs 

• 
• Demonstrate originality and inventiveness 

Technological Literacy 

• Use technology to access, evaluate, create, and communicate information 
• Use technology to design, build, and solve practical problems