Ruben Salazar High School is the continuation high school for the El Rancho Unified School District 
and proudly serves the community of Pico Rivera. RSHS is fully accredited by the Western 
Association of Colleges and Universities (WASC) and was awarded a maximum 6 year accreditation 
in both 2009 and 2015. Our current term expires in 2021. In 2012 and 2016, RSHS was recognized 
as a Model Continuation High School. Model continuation school is the highest accolade a 
continuation school can receive. Salazar enrollment is comprised of approximately 180 11th and 
12th grade students. El Rancho High School is RSHS' primary feeder school. The ethnic composition 
of the community is 97% Hispanic, 3% White. The majority of students are socio-economically 
disadvantaged with 72% of students receive a free or reduced lunch. RSHS follows a traditional 
school calendar aligned with the school district calendar. RSHS is on a trimester system which allows 
students the opportunity to earn credits at a faster pace than the traditional semester system. 
Salazar High School has 10 general education teachers,1 special education teacher (RSP) and 2 
instructional aides. Administrative and support staff include one principal, one full-time counselor, 
1 part-time mental health counselor and interns, a part time school psychologist, a secretary and a 
clerk. RSHS is committed to using technology to engage students and improve the instructional 
program. The school has 10 mobile carts that each hold 20-30 Chromebooks. Students also have 
access to a media center and computer lab making RSHS a 1:1 technology school. Ruben Salazar 
also has 4 Promethean Boards for instructors as teachers utilize Google Classroom as their digital 
learning platform. 
 
RSHS is committed to ensuring that all students succeed and as a result provides flexible schedules 
and offers a wide range of academic and elective courses. The school offers online courses through 
APEX Online Learning. This provides students an additional method to recover credits. Students 
may also earn credits through work experience and community service programs. RSHS works in 
collaboration with Rio Hondo College and Tri-Cities ROP to offer onsite college and vocational 
courses during and after school. Rio Hondo representatives conduct lunchtime workshops that 
provide students with post-graduation information and resources. Rio Hondo also offers two 
courses on-campus, which students can enroll in concurrently for elective credit. RSHS has a 
student government class that sponsors many on-campus activities to create a sense of community. 
The school also participates in Character Counts, Olweus Bully Prevention and Safe Dates. RSHS 
works closely with the Rotary Club, Soroptimist, Woman's Club and other local organizations to 
offer students volunteer opportunities. RSHS also works in partnership with local business to 
provide additional resources to improve the instructional program. Additionally, RSHS has a 
Positive Behavioral Interventions and Supports (PBIS) team whose goal is to provide behavior 
supports to assist students to achieve social, emotional and academic success. 
 
SCHOOL VISION: Supporting students in finding their path to success. 
 
MISSION STATEMENT: Ruben Salazar High School strives to create a culture of learning and 
collaboration where students are recognized as individuals with specific needs. Students are 
empowered to learn and encouraged to take academic risks while becoming life-long learners. 
Ruben Salazar High School is dedicated to providing students with a rigorous and relevant 
standards-based curriculum that fosters each students' unique talents and strengths. Teachers and 
staff at Ruben Salazar High School motivate students to be independent thinkers who can problem 
solve. Ruben Salazar High School is committed to working in partnership with parents and the 
community to support each student's path to becoming a productive and technologically literate 
citizen in the global community. 

2017-18 School Accountability Report Card for Ruben Salazar High School 

Page 1 of 9 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Ruben Salazar High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

10 

2 

0 

9 

2 

0 

10 

1 

0 

El Rancho Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

355 

13 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ruben Salazar High School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.