Gault Elementary School is a caring community that fosters respect, values diverse cultures, 
languages, and promotes positive school climate through the use of PBIS. Through a meaningful 
and appropriate curriculum, we work to ensure that all students experience continuous growth in 
academic and social skills in order to become complex thinkers, collaborative learners, and clear 
communicators. We do this through excellence in teaching, parent and community partnership, 
and support from our school district. We are a diverse population: 70% Latino, 60% English Learner, 
and 71% Free and Reduced Lunch. We are a relatively small elementary school with approximately 
340 students, 23 certificated teachers, an RTI coordinator, 23 classified support staff, full time 
principal, bilingual school counselor and bilingual community coordinator. We have several 
programs at Gault that provide students with lifelong learning skills, however it has been our work 
around climate and culture that has made a noticeable impact on student learning in the classroom. 
 
 
 

 

 

----

---- 

Santa Cruz City Schools 

133 Mission Street, Suite 100 

Santa Cruz, CA 95060 

(831) 429-3410 
www.sccs.net 

 

District Governing Board 

Sheila Coonerty 

Deedee Perez-Granados 

Cynthia Ranii 

Jeremy Shonick 

Patricia Threet 

Deborah Tracy-Proulx 

Claudia Vestal 

 

District Administration 

Kris Munro 

Superintendent 

Dorothy Coito 

Assistant Superintendent 

Educational Services 

 

Patrick Gaffney 

Assistant Superintendent 

Business Services 

 

Molly Parks 

Assistant Superintendent 

Human Resources 

 

 

2017-18 School Accountability Report Card for Gault Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Gault Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Santa Cruz City Schools 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

22 

25 

19 

0 

0 

0 

0 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

314 

8 

6 

Teacher Misassignments and Vacant Teacher Positions at this School 

Gault Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.