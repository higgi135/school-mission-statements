School Mission Statement: 
Polaris High School's mission is to provide safe and individualized support to students who need an alternative setting. Through 
instruction that is based on the current state-adopted standards, we help our students improve their academic and social skills so they 
can think critically, communicate appropriately, navigate life responsibly, and strive for advancement. 
 
General Information: 
Polaris High School is a voluntary alternative educational placement for students who would benefit from a flexible academic schedule. 
Students enter the independent studies program throughout the academic year with varying skills and deficiencies; therefore, the 
programs offered at Polaris do not follow the traditional 18-week, two-semester model. The flexible schedule allows the students to 
work at different paces and to complete courses at different times. Polaris students are each assigned to a supervising teacher and 
Instruction is tailored to the student's needs. Students must meet the District’s minimum graduation requirements of 220 credits to 
earn a high school diploma. Completed coursework must include the District’s requirements of: English 1-4, U.S. History, World History, 
Government, Economics, Math 3, Math 4, Math 5, Introduction to Functions, Earth Science, Biology, Art, as well as Computer Ethics, 
PE, Health, and electives. Targeted instruction is offered via APEX online courses and ALEKS supplementary coursework. Career 
Technical Education (CTE) courses include accounting, wood, law and legal, culinary arts, and masonry. 
 
Highlights: 
Polaris teachers have all been trained in APEX and now offer online classes as well as the traditional independent study coursework. 
Students can attend Polaris on the Trident Campus or at the District's recently opened independent study centers at Kennedy and 
Katella high schools. 
 
Demographic Information: 
Annual enrollment at Polaris fluctuates throughout the year but typically ranges between 30 to 35 students per teacher. A September 
12, 2018 snapshot showed enrollment at 91, in which 69.2% were socioeconomically disadvantaged, 13.2% were English Learners, 
and 19.8% were McKinney-Vento. In addition, the majority of students were Hispanic (56%) and female (67%).