As a California public charter, Harvest Ridge Cooperative Charter School provides a variety of unique and personalized services to its 
students by combining the structure of classroom instruction with the flexibility afforded independent study programs, all within a 
framework of high academic standards, in a small and supportive school environment. During the 2017-18 school year, we offered an 
Independent Study hybrid model, a home study model, and a traditional classroom-based offering. Each of our programs offers core 
instruction through experiential learning, technology and virtual instruction, combined with an innovative enrichment program. We 
are open to all families/students committed to our mission/educational vision, without tuition or academic entrance requirements 
and honoring the uniqueness of each individual, and seek students from diverse backgrounds in order to build a strong, inclusive 
community and prepare all students for lives in a multicultural society. 
 
Our rigorous, standards-based academic curriculum meets student needs with personalized learning and differentiated curriculum. 
Our school promotes original and creative outcomes, effective sequencing of tasks and time management, increased ability to 
collaborate, and student involvement in decision-making. 
 
Mission: Harvest Ridge Cooperative Charter School is committed to collaborating with parents, teachers, and students to provide a 
high quality student-centered educational experience, which enhances the joy of learning and inspires a commitment to lifelong 
learning. 
 
Vision: Harvest Ridge Cooperative Charter School is committed to nurturing the whole student by cultivating high academic 
achievement and strong social development. Students are encouraged to discover and pursue their interests and talents within a 
compassionate and supportive environment. Our students become self-motivated and confident as they develop 21st Century skills. 
 
Harvest Ridge staff members are enthusiastic, creative, and committed to providing an engaging and relevant education that prepares 
students to adapt and thrive in a rapidly changing world. Our staff sets high standards, fosters individuality, encourages group 
collaboration, and differentiates for individual student needs. 
 
Parents are an integral part of our school community. They work collaboratively to support classroom instruction, serve on school 
committees, participate in campus stewardship, and are active partners in their child’s education. 
 
Community connections and global awareness are encouraged through outreach programs, field trips, and service projects. 
 
 
 

2017-18 School Accountability Report Card for Harvest Ridge Cooperative Charter School 

Page 2 of 11