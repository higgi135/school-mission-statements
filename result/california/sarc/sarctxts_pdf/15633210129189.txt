The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters. As you read the 
Report Card, you will gain a better understanding of Paul L. Cato as a school with a record for improvement, a faculty that is 
professionally committed to meeting the learning needs of students and a student body which is enthusiastic and motivated to 
perform well. 
 
Our vision at Paul L. Cato Middle School is to mold students into 21st century leaders who are prepared to compete for careers in the 
future. Paul L. Cato Middle School will ensure high levels of learning in a positive environment through accountability, communication 
and collaboration.