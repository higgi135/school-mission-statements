Alvord Unified School District was established in 1896 and began with a single elementary school. 
The elementary school was later converted into a high school, and in 1963, it became known as 
Alvord Continuation High School. 
 
Alvord Continuation High School is a small high school following a traditional schedule that provides 
students in grades ten through twelve with an alternative to the comprehensive high school setting. 
As a specialized education program, alternative education is tailored to meet the needs of at-risk 
secondary school students who are credit deficient and/or whose grades prevent them from 
graduating. We accommodate individual learning styles while maintaining high expectations for all 
students and offer individualized attention and flexibility to meet the needs of all students who 
wish to earn a diploma. Our goal is to help students re-engage with their education and prepare 
them for their next step in being life-long learners. 
 
We are very proud of our history, students, staff, parents, and programs. We believe in building 
and maintaining strong partnerships with our parents and community; parent communication is 
strongly encouraged. 
 
We invite you to come and experience the park-like setting and serene atmosphere of one of 
Riverside's cleanest and most beautiful schools. We are fully accredited and recognized as a 
California Model Continuation High School. We are focused on providing all students with the 
opportunities to achieve. It is our promise that "All students will realize their unlimited potential." 
 
Mission Statement 
Alvord Unified School District, a dynamic learning community that embraces innovation, exists to 
ensure all students attain lifelong success through a system distinguished by: 

• Active and inclusive partnerships 
• Relationships that foster a culture of trust and integrity 
• High expectations and equitable learning opportunities for all 
• A mindset that promotes continuous improvement 
• Multiple opportunities for exploration and creativity 
• Professional development that promotes quality teaching and learning 
• Access to learning experiences that promote a high quality of life 

 
In addition, all schools strive to attain the Alvord vision that all students will realize their unlimited 
potential. 
 

---

- 

----

---- 

Alvord Unified School District 

9 KPC Parkway 

Corona, CA 92879 

(951) 509-5070 

www.alvordschools.org 

 

District Governing Board 

Robert Schwandt, President 

Carolyn M. Wilson, Vice President 

Lizeth Vega, Clerk 

Julie A. Moreno, Member 

Joanna Dorado, Ed.D., Member 

 

District Administration 

Allan J. Mucerino, Ed.D. 

Superintendent 

Dr. Robert E. Presby 

Assistant Superintendent, Human 

Resources 

Susana Lopez 

Assistant Superintendent, Business 

Services 

Julie Kohler-Mount 

Interim Assistant Superintendent, 

Educational Services 

Susan R. Boyd 

Assistant Superintendent, Student 

Services 

Kevin Emenaker 

Executive Director, Administrative 

Services 

 

2017-18 School Accountability Report Card for Alvord Continuation High School 

Page 1 of 9