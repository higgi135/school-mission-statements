Our school mascot is the Heroes, in honor of our school founder Mr. Louis VanderMolen. 
 
VanderMolen Fundamental Elementary community (parents, students, and staff and business partners) will embark on a collaborative 
journey to transform our learners into everyday scholars, leaders, and heroes. 
 
During the 2016-2017 school year, a committee of teachers met throughout the year with Corona-Norco Unified School District officials 
to reevaluate VanderMolen's values and goals. This group of teachers were called to adopt the P.B.I.S. strategies into the VanderMolen 
culture. This process led them to interviewing school community members, including our certificated staff, classified staff, noon 
supervisors, parents, and students. This process involved having all stake holders identify the “perfect aspects” of an elementary 
school. These conversations led us to identify key aspects that what would strengthen the foundation of our school. During the 2017-
2018 school year, the VanderMolen staff and community are implementing a system that will positively impact the school culture. The 
key values at VanderMolen include a focus on academics and social/emotional development. These values are respect, responsibility, 
compassion, and integrity. At VanderMolen, we S.O.A.R in the classroom, hallways, multi-purpose room, playground, lunch tables, 
bathrooms, and stairs (reSpect, respOnsibility, compAssion, and integRity). 
 
Professional Learning Communities are an integral aspect of our school culture and mission. We have developed four guiding questions 
which are accounted for in every fiscal and policy decision. These include: 

• What do we want our students to learn? 
• How will we know that they have learned it? 
• How will we engage students in relevant learning? 
• How will we respond if they don’t learn? 

Our mission statement states the following: At VanderMolen, staff, students, and parents are committed to all students learning. We 
will achieve a higher level of learning through respect, responsibility, compassion, and integrity. 
 
VanderMolen Elementary School became the 31st elementary school in the CNUSD on July 6, 2009. At the beginning of the 2018-2019 
school year, 985 students were enrolled, including 11.70% in special education, 17.50% qualifying for English Language Learner 
support, and 56.90% qualifying for free or reduced price lunch. We are a multi-track, year-round school that services students in 
Transitional Kindergarten through Sixth grade. Our school boundaries straddle the two communities of Eastvale and Jurupa Valley in 
the northeast portion of the CNUSD. These communities are a blending of suburban Corona with the rural agricultural of Norco. 
Economically, the two communities are very diverse; one portion with new, larger homes built on small acreage, while the other 
section has more mature, smaller homes on large agricultural parcels. Interstate 15 runs through the middle of VanderMolen’s 
attendance area and great care has been taken to ensure that both communities have equal access to the programs at VanderMolen. 
Our students reflect the rich ethnic diversity of our school district. 
 
Our school has 33 regular education teachers, 7 special education teachers, including a lower/upper grade Severely Handicapped 
Classroom and three separate Special Day Classes, 2 Speech and Language Specialists,12 instructional assistants, 1 amazing school 
secretary, 2 eight-hour clerks, 2 part time clerks, 1 health clerk, 1 library clerk, a school counselor (3 days), 3 custodians, and an 
awesome full-time Assistant Principal. We have a beautiful library with a VDI computer lab within it and another VDI lab next door. 
Campus-Catering utilizes the multipurpose room and serves balanced and nutritious breakfasts and lunches to students and staff. 
 
VanderMolen adheres to all District policies regarding instructional minutes, minimum days, and professional development 
opportunities. 
 

2017-18 School Accountability Report Card for VanderMolen Fundamental Elementary School 

Page 2 of 15 

 

VanderMolen became one of the pilot schools for the Transitional Kindergarten program during the 2011-12 school year. This program 
is designed to meet the specialized needs of students who are four-years when beginning their school experience. The program is an 
appropriate balance of academic and social development skills. Our teachers work closely within the Kinder team and with other 
Transitional teachers across the District within a professional learning community. In addition, we are utilizing all district adopted core 
materials. 
 
We have 246 English learners representing eighteen different languages groups. There are 110 students receiving speech services. Our 
RSP teachers work with 43 students, utilizing both a push-in and pull-out model. In fact, the RSP and SST programs are built along the 
principles of Response To Intervention guidelines, published by the California State Department of Education. 
 
VanderMolen was created to be a cutting edge instructional facility, designed around a technology focus. All classrooms have 
instructional Mimio-boards, student responders and document cameras. All classrooms contain a minimum of 4 student computers 
and 1 teacher station. An interactive website, which will allow better communications with families, is under design. We were selected 
by the Pearson Educational Foundation to be a model-demonstration school. 
 
The VanderMolen PTA has eight people on its executive board, with a general membership of over 800. Parents are encouraged to 
become active in their child’s education through Coffee with the Principal, ELAC, SSC meetings, Family Math Nights, Movies on the 
Lawn and our Community Craft Fair. Moreover, parents are welcome to volunteer in their child’s classroom. 
 
Our grade level teams are organized around the principles of Professional Learning Communities. This has greatly aided in the creation 
of an instructional team approach which has strengthened the educational foundation of our students. CNUSD has provided teachers 
with a weekly one-hour early dismissal, which our staff utilizes for PLC time within grade levels. These meetings are designed around 
reviewing student achievement, discussions on best instructional practices and designing future interventions/enrichment 
opportunities for students. 
 
In the 2016-2017 school year, the VanderMolen teaching staff implemented a school wide intervention for our struggling readers. A 
committee of teachers worked together with the staff and administration to make sure that students were appropriately assessed 
and reassessed throughout the year. This endeavor helped struggling readers receive instruction at their reading levels in small groups. 
The school saw many of their struggling readers exit the intervention over time due to this instruction. The staff united over these 
students, and the implications of this collaboration saw student growth and strengthened the staff culture.