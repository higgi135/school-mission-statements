The Transitional Learning Center, TLC, located in the Central Sierra Mountains at Lake Tahoe in the Lake Tahoe Unified School District, 
is a small continuation high school providing students who, for various reasons, have been unable to succeed in the traditional 
comprehensive programs with support to reach their goals of high school completion and successful transition to post-secondary 
educational and career options. 
 
For the 2018-2019 school year no students are nor have enrolled in the TLC to date.