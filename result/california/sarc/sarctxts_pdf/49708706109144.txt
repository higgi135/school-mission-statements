Schaefer Charter School is a culturally diverse school that serves 340 kindergarten through sixth grade students. 75% percent of 
Schaefer Charter School students reside within the Piner-Olivet Union School District. 29% of students are English Language Learners, 
60% Socio Economically Disadvantaged, and 1% Students with Disabilities. 
 
Schaefer Charter School has fifteen full classrooms, a STEAM lab, a music room, a multi-purpose room, a library, a field and a blacktop 
with play structures for student use. The kindergarten classrooms have their own separate, enclosed playground and eating area. 
 
The staff at Schaefer includes a full time principal, a full time office manager, fifteen full-time teachers, and two custodians. All teachers 
are fully credentialed and highly qualified. Part-time staff includes 6 program assistants, a PE Technician, and a credentialed music 
teacher. Nurse services are regularly available. Special programs include the Resource Specialist Program and Speech and Language 
Program (services provided through the Piner-Olivet Union School District) and the English Language Development (ELD) and 
Intervention Program for students not meeting grade level standards (provided by the classroom teachers and program assistants). 
All students with intervention needs in English Language Arts and Math are served throughout the day in a Learning Center as well as 
within their regular education classroom. 
 
Curriculum is standards based and designed to be engaging. Teachers received professional development in the Common Core State 
Standards (CCSS) and utilize the CCSS as the basis for their classroom instruction. Technology is used to enhance instruction and 
increase student engagement. Classrooms are equipped with Chrome books and iPads. The technology in grades K-1 is at a 2:1 ratio, 
and in grades 2-6 at a 1:1 ratio. 
 
Schaefer students demonstrate creativity, initiative, leadership and innovation through their engagement with learning, interactions 
with staff, community, and each other. The Schaefer Student Council meets regularly to plan events and school-wide improvement 
efforts. Parents are an integral part of Schaefer Charter School. We welcome volunteers to help in classrooms, on field trips, for special 
school activities, with planning student community service activities, and for other special tasks. Back to School Night, Open House, 
Movie Nights, and special family events are well attended by our families. 
 
Schaefer Charter School Mission: 
We pride our school community on promoting a learning environment that empowers all students to reach their fullest potential. We 
are committed to creating a nurturing and caring environment that supports children in the development of their academic, social and 
creative potential. 
 
Across the school community, adults and children behave in ways that demonstrate respect, safety and responsibility. These standards 
for behavior are the basis of the norms that have been established for collaborative working relationships amongst staff and parents 
as well as classroom interactions between teachers and students. Everyone in the school community plays a vital role in creating a 
caring and supportive learning environment for each student. 
 
 
 

2017-18 School Accountability Report Card for Schaefer Charter School 

Page 2 of 10