Summit Charter Academy (SCA) is located in the west side of Porterville, California in rural Tulare County, in the heart of the San 
Joaquin Valley. The district consists of nine school sites: four K-6 campuses; one 7-8 campus; SCA-Mathew, which is a K-5 dual 
immersion campus; SCA-Lombardi, which is a K-5 International Baccalaureate School; Summit Charter Intermediate Academy is a 6-7 
International Baccalaureate Candidate School; and Summit Collegiate Charter Academy (SCCA), which is a 8-12 International 
Baccalaureate-Early College High School. Summit Charter Academy is a WASC accredited school. Summit Charter Academy also offers 
a K-8 homeschooling component. Current enrollment is approximately 2,200 students in grades TK-12. Burton School District Mission: 
The Burton experience will prepare students to be college and world ready. Summit Charter Academy allows students to explore the 
world before them as the staff challenges each student to rise to the pinnacle of physical, artistic, and intellectual awareness. The 
Charter School educates students of the Burton District to demonstrate a firm commitment to their community, exhibit strong 
personal character, critically analyze information, and communicate articulately. Summit Charter Academy is committed to developing 
critical thinkers and problem solvers who value the perspectives and cultures of the global community. This charter school delivers a 
rigorous transdisciplinary curriculum. In addition, the school focuses on these specific program areas: 

1) Acquisition of a Second Language:(Parent Choice) Our program includes a dual language model with students learning in both 
Spanish and English or an English program with the opportunity for Spanish enrichment. The goal is for students to attain 
advanced levels of functional proficiency in two languages and to promote understanding of and appreciation for the cultures 
represented by the languages studied. 
Integrated Fine and Performing Arts with Academics: The arts contain a rich body of knowledge that will aid all students in 
understanding the world around them and enhance their learning in all academic areas. It is the goal of the school that 
students be offered an art-enriched environment that encourages students to make connections and judgments that validate 
their learning. 

2) 

3) Experiential/Project-based Learning: We will provide students with the opportunity to proceed through standards based 

4) 

curricular areas by working collaboratively or individually on projects that reflect their individual needs. 
International Baccalaureate: Summit Charter Intermediate Academy is an IB school with the Middle Years Programme and 
Summit Charter Lombardi is an IB school with the Primary Years Programme. 

5) Technology infused into curriculum delivery and student learning. Each TK-5th grade class has at one iPad for every students. 
All 6th through 8th grade students have iPads and all 9th-12th grade students have Macbook Air devices. Beginning in 6th 
grade, students take their devices home as well as using them during the instructional day. 

6) Parental support and involvement at school, at home, and in the community: Parents are meaningfully involved in supporting 
the school and their student's education. The Student Learning Outcomes state that Summit Charter Academy students are 
of high character who value collaborating with others to think critically about the problems our world faces. 

2017-18 School Accountability Report Card for Summit Charter Academy 

Page 2 of 14 

 

Working collaboratively with others, SCA students develop creative ideas and innovative solutions to these real world problems. 
People of Character Demonstrate responsibility in their academic and social interactions Show respect towards others and themselves 
Display confidence and motivation, Have goals for their future Critical Thinkers, Identify problems in the real world and create solutions 
through inquiry and Develop creative ideas and seek innovation, Take risks and view mistakes as opportunities for growth, Are active 
participants in the changing world around them Global Citizens Appreciate the cultures of others, Work well with diverse populations 
and Use technology as a tool to connect to the international community, Understand their individual responsibility to nature and our 
environment Collaborative and Collaborate with peers with open mindedness, Use collaboration as a tool to achieve their goals, 
Communicate through a variety of medias, Effectively read, write, listen and speak while learning and collaborating Work to bridge 
barriers culturally, geographically and linguistically. SCA Mathew is staffed with one principal, one vice-principal, 23 regular classroom 
teachers, and one part time Resource Specialists. All kindergarten classrooms receive part-time instructional aide services. Other 
classified employees include: eight part-time intervention aides, two part-time special education aides, a full-time custodian, one full-
time secretary, one part-time clerk, and one part-time Library Technician. Additionally, the school receives services from a district 
nurse and a district School Psychologist. Fourth and fifth grade students are also provided instruction from a district instrumental 
music teacher and one physical education teacher. The Mathew Campus implements The Character Counts! Program, and its six pillars 
(Trustworthiness, Respect, Responsibility, Fairness, Caring, Citizenship) are instilled in students and staff. Each month one student 
from each class is recognized for being a leader and an example of good character. The school community is also involved in service 
projects inspired by a dedication to helping others. Service projects such as canned food drives and recycling are examples of just two 
of the ways that students and staff are giving back to the community. The Mathew Campus is equipped with desktop computers in 
classrooms, in the library, and in the office. Each permanent classroom is equipped with a 70 inch smart TV. Staff and students have 
access to a 21st century classroom with 7 smart TV's, green screens, and video recording iPad. All K-5 classrooms have a set of iPads; 
one for each student. All have access to the internet. Three classrooms are equipped with Smartboards. Telephones with outside 
access are available in all classrooms. 
 
Electronic networking is in place at our school. Burton School District and Summit Charter Academy websites are in place and enable 
parents and community to keep in touch with our school. SCA Lombardi is staffed with one principal, one vice principal, 1 part-time IB 
Coordinator, twenty-three regular classroom teachers, one full-time Spanish teacher, and one full-time Resource Specialist. All 
kindergarten classrooms receive part-time instructional aide services. Other classified employees include: five part-time intervention 
aides, one part-time ELL/Migrant aide, a full-time and three part-time custodians, one full-time secretary, two part-time clerks, three 
part-time Special Education aides, and a part-time Certified Library Technician. Additionally, the school receives services from a district 
nurse and a district School Psychologist. Fourth and fifth grade students are also provided instruction from a district instrumental 
music teacher and a physical education teacher. SCA-Lombardi is equipped with desktop computers in the office. Students have access 
to a 21st Century Classroom equipped with 6 portable TVs, rolling whiteboards, and a large mounted TV. All K-5 classrooms have a set 
of iPad devices (1 for each student). Two classrooms are equipped with Smartboards and all classrooms have either mounted LCD 
projectors or portable flat-screen TVs, Apple TV devices, and document cameras. The school website, social media accounts, marquee, 
and Blackboard are used to communicate with parents and the community. Teachers also use the Seesaw app and Remind app to 
communicate classroom events with families. The Lombardi Campus is an International Baccalaureate (IB) school. Through the IB 
Primary Years Programme, we are teaching students to be knowledgeable, inquirers, open-minded, risk-takers, balanced, reflective, 
caring, principled, thinkers, and communicators. Project-based learning, Spanish enrichment, and literacy units (reading, writing, and 
discussing) are focal points of the IB programme. IB School Pledge for the Primary Years Programme: We, as an official authorized IB 
World School, promise to foster inquiry, embrace global citizenship and encourage empathy towards all. We do this in a way that 
allows us to inquire and understand others through their language, culture, and points of view. We will live by the I.B. attitudes and 
attributes, so that we will proudly represent ourselves as part of the I.B. Organization. IB Student Pledge for the Primary Years 
Programme: I pledge to be an open-minded thinker; to do my best to have a healthy, caring attitude. I will be committed to learning 
through inquiry and showing enthusiasm each day. I believe that quality and creative work equals success. 

2017-18 School Accountability Report Card for Summit Charter Academy 

Page 3 of 14 

 

I can use my knowledge and curiosity to be successful in the world. Summit Charter Intermediate Academy is staffed with one principal, 
one vice principal, 1 part-time IB Coordinator, one part time academic counselor, 14 regular classroom teachers, 4 part time teachers, 
and one full-time Resource Specialist. Other classified employees include: two part-time intervention aides, one part-time ELL aide, 
one part time physical education aide, one part time campus supervisor, a full-time and two part-time custodians, one full-time 
secretary, two part-time clerks, one part-time Special Education aide, one full time inclusion aide, one part time inclusion aide, and a 
part-time Library aide. Additionally, the school receives services from a district nurse and a district School Psychologist. Summit 
Charter Intermediate is equipped with Mac computers in the office. Every classroom is equipped with a TVs, Apple TV devices, and 
document cameras. Every student in 6th and 7th grade has been issued an iPad. Every teacher has been issued a Macbook and an 
iPad. The school website, social media accounts, and Blackboard are used to communicate with parents and the community. Teachers 
in 6th grade also use the Seesaw app and Remind app to communicate classroom events with families. The Intermediate Campus, 
under the Collegiate campus, is an authorized International Baccalaureate (IB) school. Through the International Baccalaureate Middle 
Years Programme, we are teaching students to be knowledgeable, inquirers, open-minded, risk-takers, balanced, reflective, caring, 
principled, thinkers, and communicators. Summit Charter Collegiate Academy is staffed with one principal, one vice principal, one vice 
principal/IB Coordinator, one full time counselor, one part time counselor, thirty-one full time classroom teachers, one part time 
classroom teacher, one mentor teacher and three Resource Specialists. Other classified employees include: a full-time and part-time 
custodian, one full-time secretary, one registrar, one one part-time clerks, four part-time aides, two full time aides, one physical 
education aide, two part-time campus security supervisor, and two part-time Certified Library Technicians. Additionally, the school 
receives services from a district nurse and a district School Psychologist. The Collegiate Campus is an International Baccalaureate (IB) 
school. Through the International Baccalaureate Middle Years Programme, we are teaching students to be knowledgeable, inquirers, 
open-minded, risk-takers, balanced, reflective, caring, principled, thinkers, and communicators. We are preparing students for their 
lives after high school internationally. The Collegiate Campus is equipped with a computer lab that each consist of 32 desktop 
computers. Every high school student has been issued a Macbook Air and every junior high student has been issued an iPad. There are 
Wi-Fi devices available for students who need home internet access. The school website and phone system are used to communicate 
with parents and the community. The Collegiate Campus is also offering a College Express Pathway which provides students the 
opportunity to earn up to 68 college credits and an AS degree in Business Administration from Porterville College by the time the 
graduate from high school. The high school and college are working together to provide opportunities through concurrent enrollment, 
dual enrollment and articulated courses to provide students multiple opportunities to earn college credit. The entire staff at Summit 
Charter Academy is committed to providing the best education possible to its students. We believe that all students can succeed 
regardless of their race, background, or ability. To that end, we strive to provide a nurturing atmosphere that encourages our students 
to try their best daily without fear of failure. Summit Charter Academy is a place where excellence is stressed, both in academics as 
well as in all aspects of life. At our school, each student is recognized as a special person who can make valuable contributions to our 
future world.