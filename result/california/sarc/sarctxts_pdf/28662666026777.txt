The students at Bel Aire Park Magnet School are served by 18 credentialed full time teachers and a support staff that includes a Title 
1 Reading Support Teacher, a Resource Specialist, a Magnet Lead/PYP Coordinator, a Magnet theme Instructional Coach, part time 
music and PE specialists, a speech therapist, a part-time school psychologist and nurse, a bilingual community liaison and five 
instructional assistants. Bel Aire Park is an International Baccalaureate World School authorized to provide their Primary Years 
Program to all students. These are schools that share a common philosophy—a commitment to high quality, challenging, international 
education that Bel Aire Park believes is important for our students. The program uses student driven inquiry as a stance for instruction 
and includes opportunities for students to build understanding through hands-on, exploratory learning in a caring, supportive 
environment. In 2018, a Communications and Media theme was added to the school program. The campus is a community-orientated 
facility – used after hours for childcare, adult education classes, and baseball, soccer and football leagues 
 
Mission Statement: Bel Aire Park community will work collaboratively to provide meaningful, authentic learning experiences that 
inspire wonder, creativity, and enthusiasm in our students. We strive to develop life-long learners with the communication skills 
necessary to share their thinking and their compassion to create a more peaceful world.