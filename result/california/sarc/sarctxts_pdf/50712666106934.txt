Our vision at Sisk Elementary is that all students can learn at high levels when instruction meets their needs. 
 
Sisk Elementary opened in the fall of 1987; consisting of several portable classrooms. Enrollment in Salida was growing rapidly and 
Sisk became a permanent school site in summer of 1994; it was surrounded by a newer, middle-class subdivision which housed a large 
number of commuters. Upon the school’s initiation, the staff established our school’s mission: “To teach each child the skills necessary 
for life-long learning”. More than twenty years have passed and we remain steadfastly committed to achieving our mission each new 
school year; employing a variety of strategies and maximizing existing resources to support student success. 
 
One of our key resources is the collective experience and constancy of the teaching staff. Several staff member have taught in Salida 
for over 20 years, and many teachers have been part of Sisk since its inauguration as a permanent elementary campus in 1994. Our 
academic program maximizes our people and fiscal resources, recognizes and addresses the needs of our diverse population, and 
focuses consistently on student learning. Parental support is a significant element; we enjoy the full benefits of an active Parent 
Teacher Group which provides our families opportunities for involvement in school events. We believe in the value of providing 
children a well-rounded educational experience which includes a music program, physical education program, exposure to the fine 
arts, S.T.E.A.M. instructional days, access to technology, assemblies, and field trips. 
 
Our goal at Sisk Elementary School is that all students achieve at high levels. Sisk Elementary is an educational community of teachers, 
parents and community members who collaborate to provide students with a balanced education. Our goal is to ensure that every 
student reads independently and meets grade level standards by 3rd grade. Professional Learning Communities engage in ongoing 
evaluation of student progress and achievement to refine the instructional program so that students can achieve academic proficiency. 
Intervention for students who are performing below grade level is provided during school hours. Students access the core curriculum 
in English Language Arts, ELD and Mathematics using state-adopted curriculum programs. The staff at Sisk stands proud of our 
students’ achievements, and of our academic and extracurricular offerings.