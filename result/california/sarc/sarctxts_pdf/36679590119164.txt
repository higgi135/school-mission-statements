Mesa View Middle School opened in August of 2009 in Calimesa, California, as the first of the district's schools to be built within 
Riverside County. The construction of the school was completed in 2006, but the lack of a suitable access road delayed its opening 
until the district built a 1.25 mile extension to Sandalwood Drive in 2008. Mesa View became a STEM magnet school in 2012. The 
school houses sixth, seventh, and eighth grade students from three feeder K-5 elementary schools (Calimesa, Valley, and Wildwood) 
in the Yucaipa-Calimesa Joint Unified School District. Calimesa is located approximately 20 miles east of San Bernardino in the foothills 
of the San Bernardino Mountains between the Los Angeles Basin and Palm Springs. The Yucaipa Valley, which includes the cities of 
Yucaipa and Calimesa, covers approximately 45 square miles and has a current population of approximately 70,000 residents. The 
valley was once a rural, farming community with many retired residents. Recent years have brought several new housing 
developments to the area, attracting a diverse and younger population. Mesa View is also less than a mile from County Line Road, 
which divides San Bernardino and Riverside Counties. Most of Mesa View’s students live in Yucaipa, a slow-growing town of 
approximately 60,000 residents on the other side of County Line Road in San Bernardino County. 
 
Mesa View Middle School has 844 students in 49 classrooms. The school was built to one day be the district's second high school. It 
includes three specialized science labs, two computer labs, a language lab, a library, an art room with a kiln and pottery wheels, a 
"LifeSkills" classroom for special education students with extreme needs, two County special education classrooms, a dance classroom, 
a suite of rooms for music instruction, classrooms dedicated to drama and choir, a large multi-purpose room with facilities for many 
types of productions, a career center, a full-service kitchen and serving area, a weight room, a full set of athletic fields (including an 
oval track and a practice football field), and two student locker rooms for physical education. 

• Mesa View Middle School is one of two middle schools in the Yucaipa-Calimesa Joint Unified School District. The mission 
of the district is: “Innovative Program World Class Education." The entire school community is committed to that goal. 
Mesa View offers a high quality, STEM-focused academic program, support classes, rich elective offerings, and 
interventions that are aligned with program offerings at other sites in our school district. 

MISSION STATEMENT 
 
The mission of Mesa View Middle School is to prepare our students to be productive leaders, learners, and citizens of the 21st Century. 
We are dedicated to: 

• Focusing on student learning and rigorous standards through the use of critical thinking, problem solving, creativity, and 

oral & visual communication skills, 

• Fostering academic and personal responsibility based upon respect, integrity, commitment, patience, honesty, 

perseverance, and tolerance, 

• Providing positive leadership experiences that promote social awareness and contribute to the school and the community, 
• Developing student self awareness of individual strengths, interests, and personal health so students are prepared for their 

future. 

• Bringing a hands-on, meaningful STEM (science, technology, engineering, & math) experience to every Mesa View student. 

MOTTO 
 
“Learners Today, Leaders Tomorrow” 
 
DISTRICT PROFILE 
 

2017-18 School Accountability Report Card for Mesa View Middle School 

Page 2 of 11 

 

Located in San Bernardino County, nestled at the base of the San Bernardino Mountains, the Yucaipa-Calimesa Joint Unified School 
District educates approximately 8,500 kindergarten through twelfth grade students from the diverse suburban communities of Yucaipa 
and Calimesa. The district is proud of its long tradition of academic excellence. The district is comprised of six elementary schools 
(Grades TK-5); one dependent charter school (Grades K-8); two middle schools (Grades 6-8); one comprehensive high school campus 
(Grades 9-12); a community day school (Grades 7-12); an independent study program PEP and PEP+ (Grades K-8); a continuation high 
school (Grades 9-12); an online academy (Grades 9-12), a special education success program (Grades K-12) including a preschool 
program; and an adult education program. The Yucaipa-Calimesa Joint Unified School District is dedicated to educational excellence 
and the continuous academic growth of all students. 
 
A Message from the Superintendent 
 
Dear Yucaipa-Calimesa Community, Parents and Students: 
 
YCJUSD is honored to serve students in two wonderful communities. We, as a team, continue to prepare our students to be successful 
in the 21st century. We work collaboratively with community partners, businesses and colleges to provide the best possible education 
for each and every student. The district employs high quality employees, who care for student well-being as well as academics. The 
school sites have a variety of clubs and programs to suit student interests and all our elementary schools have an after school care 
program. We strive to provide a wide variety of high quality services and programs in a safe environment. 
 
As you become a partner of the YCJUSD, please take the opportunity to be involved. It is our desire to work hand in hand with parents 
to support our children. The best way to get involved is to start at the school site. Our principals can help guide you to the many 
opportunities that exist. No amount of involvement is too small! We also offer classes for parents that will help you and your child in 
their educational journey. We have the Family Learning Center which offers a host of classes that support parents in learning strategies 
to work with children in grades K-12. Please take an opportunity to view the website to learn more about the classes. 
 
The role of educating children in our two communities is taken very seriously and we appreciate your trust. My goal, as your 
Superintendent, is to ensure that high quality instruction is delivered daily, our campuses are secure and well maintained, money is 
spent wisely, and students graduate from Yucaipa High School prepared to be successful! 
 
This school year is the opportunity to work with you in supporting education of our children. Please do not hesitate to contact your 
principal or the district office if you have questions about the district or how to become involved.