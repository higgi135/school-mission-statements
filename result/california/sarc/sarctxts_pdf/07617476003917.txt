Donald Rheem Elementary School is a K-5 elementary school located in the Moraga School District. The total enrollment in 2018-2019 
was 414 and the average class size is 23 students. All classrooms have Teaching Assistants who work from five to ten hours per week, 
depending on grade level assignments. The school has a number of programs in addition to regular classroom activities. Art, Music, 
and Physical Education programs are delivered to all students by teachers credentialed in those areas. A computer lab with thirty 
student stations is provided to K-5th grades on a regular instructional basis. All grades have computers in the classroom. Weekly 
scheduled library time is provided to all grade levels. 
 
An English Language Learner (ELL) program is offered to those who qualify for this service. Our Special Education staff consists of a 
full-time and part-time Resource Specialist and assistant, a part-time School Psychologist and a part-time Speech and Language 
Pathologist. In addition, a "Kids Connection" program is available one day per week to provide support for students with school 
adjustment or social skill issues, and a school counselor is available one and a half days a week to support student needs and school-
wide, social and emotional learning. 
 
Rheem School's staff, students and parents are committed to meeting the needs of all who attend by providing an effective 
instructional program designed to improve students' academic, social and physical growth. We are in full support of the mission of 
the Moraga School District to provide a supportive and challenging academic environment that encourages critical and creative 
thinking, maximizes individual students' strengths and motivates them to be lifelong learners and responsible citizens. This is 
accomplished by a dedicated team of educators, parents, community members and students.