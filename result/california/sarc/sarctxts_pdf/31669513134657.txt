Mission: Lincoln High School is dedicated to success for all students, and is relentless in BUILDING 
MEN and WOMEN OF INTEGRITY! 
 
Vision: Each and every student at LHS will be COLLEGE or CAREER READY. A student may choose 
not to go to college, but he/she will be college ready and have OPTIONS in life (college, military, 
career technical education, etc.) 
 
Collective Commitments/Values 
Each student will be supported by the teaching staff to achieve at high levels in the areas of 
academics, socio/emotional well-being, and creative problem solving. 
We commit to increased professionalism in timeliness, appearance, behavior, and accountability 
for ALL in the school community. 
We commit to high expectations and excellence by focusing on healthy relationships where the 
classroom is a safe place where everyone is engaged. 
We commit to meaningful and productive collaborations about student learning with mutual 
accountability. 
We commit to building positive relationships with students, staff, parents, and community 
members to support student success. 
 
Schoolwide Learner Outcomes 
Safe, Respectful, Responsible learners who participate in the school and local community both 
positively and ethically while finding safe and constructive opportunities to influence its direction. 
True Communicators, who are able to verbally communicate ideas and information effectively for 
a variety of purposes and audiences. 
Reflective Listeners, who are able to listen effectively and critically with literal and analytical 
comprehension in a variety of situations. 
Informed Readers, who are able to read actively and critically from a variety of materials and genres 
with literal and analytical comprehension for a variety of purposes. 
Proactive Planners, who set realistic and challenging goals in an education plan to meet the 
demands of university, career technical education, military, or workforce career pathways. 
Effective Writers, who communicate ideas and information coherently in writing, using the 
conventions of standard English, for a variety of purposes and audiences. 
Self-directed students, who are able to apply STRIPES principles in gathering materials and 
knowledge from a variety of sources and utilize them to solve problems creatively and effectively. 
 
 

2017-18 School Accountability Report Card for Lincoln High School 

Page 1 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Lincoln High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

74 

75 

1 

0 

1 

6 

76 

0 

10 

Western Placer Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

320 

0 

15 

Teacher Misassignments and Vacant Teacher Positions at this School 

Lincoln High School 

16-17 

17-18 

18-19