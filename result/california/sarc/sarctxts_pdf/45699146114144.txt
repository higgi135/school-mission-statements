North State Aspire Academy is an academic-alternative school for students having behavior and attendance difficulties in the regular 
education environment. The mission of North State Aspire Academy is to assist students in their progress towards responsibility as it 
relates to academic achievement and personal development. We strive to see that each individual makes significant progress toward 
reaching their potential. 
 
Community Day students are encouraged to change their behavior, their image of themselves and their future. The students are 
separated from their former peer group and placed in a smaller environment where it is safe to make necessary changes without the 
pressures or influence of past associates. Students who want to return to the regular school program find North State Aspire Academy 
an excellent experience. The staff find it very rewarding to see a students make the choices necessary for them to be successful. 
 
MISSION 
Inspire – Believe – Achieve 
 
VISION 
Inspire – The entire staff support, include, and serve all students from across our community, creating a culture of excellence, 
challenging them to be successful, continuous learners who are academically, socially, and emotionally prepared for the future. 
Believe-Using student-centered curriculum and engaging instruction, enhanced with cutting-edge technology, we provide positive and 
safe classrooms that focus on the whole child. 
Achieve – With students, families, and the community as equal partners, we are dedicated to preparing confident, healthy, respectful, 
and responsible students who can succeed and be productive tomorrow, next year, in high school, and in their post-graduation college 
and/or work careers.