Margarita Middle School is the Temecula Valley Unified School District's (TVUSD) original middle school, opening its doors in 
1988. Margarita Middle School is located in the heart of Temecula and has a diverse student population of approximately 850 
students. The mission of the Margarita Middle School Community is to provide a safe, healthy environment to foster achievement, 
respect, and pride in our students. Margarita Middle School is supported by an active Parent Teacher Student Association, 
which enhances the school's tight-knit culture. Our partnership with parents is supported by a variety of parent volunteer and parent 
celebration opportunities throughout the year. Through strong academic expectations as well as through STEM, Coding, Music, Art, 
ASB, Science Olympiad, Yearbook, Drama, CJSF and National Honor Society, we are preparing our students to live and work in the 
world of 2030, 2050, and 2075.