Nogales High School prepares our students for a successful future through a first-class educational experience. 
 
Nogales High School is one of 19 Rowland Unified School District sites. Built in 1962, Nogales serves students in grades 9-12. The staff 
and community at Nogales work collaboratively to ensure the implementation of policies and practices that demonstrate a respect 
for all individuals within the culturally diverse school community. 
 
Core Values 
INTEGRITY: We believe in principles that support ethical decision-making, positive role modeling, and a commitment to 
professionalism. 
 
RESPECT: We believe in the appreciation and celebration of both commonalities and diversities of individuals and groups. In addition, 
we will: 
Foster respect for democratic principles and citizenship; 
Promote respect for our environment by developing responsibility for its preservation. 
 
SAFETY: We believe in and are committed to providing safe schools and a secure environment for all students and staff. 
 
STUDENT-CENTERED FOCUS: We believe in keeping the student at the center of all decisions. Providing meaningful, challenging 
curriculum, programs, and practices will: 
Promote student involvement; 
Provide equal learning opportunities; 
Ensure a strong academic foundation and balance among basic skills; 
Problem-solving, and critical thinking; 
Encourage life-long learning. 
 
EXCELLENCE: We believe in high standards for personal performance in pursuit of an ideal in all of or endeavors, as demonstrated by: 
Well-defined expectations within a supportive environment; 
Rigorous instructional programs; 
Commitment to succeed. 
 
RESPONSIBILITY WITH ACCOUNTABILITY: We believe each individual can and should be responsible and accountable for his/her 
decisions and actions. In support of this value, we will: 
Promote the development and empowerment of individuals and groups; develop appropriate goals and measures of success. 
 
NOGALES HIGH SCHOOL 
SCHOOLWIDE LEARNER OUTCOMES 
(SLO's) 
 
EFFECTIVE COMMUNICATORS: Who 
1. Express significant information and ideas through verbal and non-verbal means. 
2. Demonstrate the use of a variety of information-gathering techniques and information resources. 
3. Demonstrate the ability to receive and interpret the messages of others, verbal and non-verbal, and respond appropriately. 
4. Are responsible citizens that develop collaborative skills to work effectively within their family, the school, the community and 
society. 
 
 

2017-18 School Accountability Report Card for Nogales High School 

Page 2 of 15 

 

CRITICAL THINKERS: Who 
1. Analyze, interpret, evaluate, and assess appropriate knowledge and information. 
2. Demonstrate ability to recognize and analyze problems then apply problem solving strategies to real-life situations. 
3. Set achievable short and long range goals and assess their own progress. 
4. Process, integrate, and synthesize information through reading, writing, listening and speaking strategies. 
5. Demonstrate a knowledge, understanding and respect of cultural and social differences, perspectives and common experiences 
among people. 
 
PERFORMANCE ACHIEVERS: Who 
1. Perform and/or create at or above district / state benchmarks levels (i.e. CAHSEE, STAR, district benchmark assessment). 
2. Are able to express themselves analytically and creatively. 
3. Are able to demonstrate inductive and deductive reasoning. 
4. Explore, develop and utilize strategies for life-long educational and career options. 
 
RESPONSIBLE CITIZENS: Who 
1. Commit to the highest ethical standards. 
2. Contribute positively to their community. 
3. Demonstrate knowledge, understanding and respect for cultural and social differences. 
4 Explore develop and employ strategies for life-long educational and career options.