The community of Borrego Springs is located 80 miles northeast of San Diego and is isolated by the surrounding mountains and 600,000 
acres of the Anza-Borrego Desert State Park. Borrego Springs High School is a comprehensive high school. The high school features art 
classes and a variety of ROP/CTE courses. Borrego Springs High also offers AP English, AP History, AP Spanish and AP Science classes 
on-line. There are nine classrooms, a science lab, a multipurpose room, a library with computer work stations and an additional 24 
port computer lab. There is also a cafeteria, a faculty lunch room, and a middle/high school office. Each classroom has internet 
connectivity and at least three or four desktop computers for research projects. The entire campus has wi-fi available to students and 
staff, with a security access code. The science lab has eighteen computers in a lab-type arrangement which facilitates the incorporation 
of technology into science. Each student is given a chrome book for their own personal use. The students will keep the chrome books 
upon graduation. Digital projectors are installed in each classroom. 
 
The educational community of Borrego Springs includes parents, students, staff in partnership with the community. It is the mission 
of the Borrego Springs High School Community to graduate students with an accredited high school diploma, prepared for post-high 
school studies and the world of work.