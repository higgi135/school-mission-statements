Chadbourne is a school committed to continuing examination of its core mission, to provide a 
rigorous standards-based academic program that is balanced by a variety of interventions, as well 
as enrichment activities and programs, which stretch the unique potential of each child. Our motto, 
“excellence in education through excellence in teaching” remains the most important in our 
continuing tradition of providing excellence and equity for all students through differentiated 
instruction and continued enhancement of teaching and learning strategies. Chadbourne is also 
incorporating a Character Education program where our specialists work together to create 
enrichment activities that support our standards but also work on building well rounded students. 
 
Chadbourne is a K-6 school with an enrollment of approximately 780. We celebrate the diversity 
of our student population. In addition to our regular education classes, we have one Special Day 
Classe (SDC) providing instruction to grades five and six serving mild/moderate special education 
students, we also have two SDC classes providing instruction to grades TK/K through third grade. 
As well as giving them opportunities to participate in other classes to gain a wide variety of 
exposure to different curriculum and methods of teaching. 
 
Our close partnership with our parents and community enhances the quality of teaching and 
learning experiences for both students and staff. Dedicated parents and our parent group, the 
Chadbourne Family and Faculty Association (CFFA) are our greatest supporters. Through a 
voluntary Pioneer Pledge, we are able to provide music and visual arts instruction from qualified 
professionals for all our students that are aligned to the common core state standards. The Pioneer 
Pledge also provides funds for technology upgrades in classrooms and the campus as a whole. 
Students learn music and art appreciation through a parent taught and designed Fine Arts Mini 
Experience (FAME). Our classroom teachers also integrate performance and visual arts with 
language arts, mathematics, science and social science. We have an active Technology Committee 
made up mostly of parents who share their expertise in technology by giving us technical support 
and help in maintaining our network connectivity. Our classes also enjoy learning life, earth, and 
physical science at Mission Creek. 
 
 

----
---- 
Fremont Unified School District 

4210 Technology Drive 

Fremont, CA 94538 

(510) 657-2350 

www.fremont.k12.ca.us 

 

District Governing Board 

Yang Shao, Ph.D. 

Michele Berke, Ph.D. 

Desrie Campbell 

Ann Crosbie 

Larry Sweeney 

 

District Administration 

Kim Wallace, Ed.D. 

Superintendent 

Raul A. Parungao 

Associate Superintendent 

Debbie Ashmore 

Assistant Superintendent, 

Instruction 

Raul M. Zamora, Ed.D. 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for CHADBOURNE ELEMENTARY SCHOOL 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

36 

35 

38 

0 

0 

0 

0 

1 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1713 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

0 

0 

0 

0 

0 

0 

0 

0 

Vacant Teacher Positions 
Notes: 
1) “Misassignments” refers to the number of positions filled by teachers who lack 
legal authorization to teach that grade level, subject area, student group, etc. 
 
2) Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners. 
 

0