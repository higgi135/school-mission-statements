West Orange Elementary School is one of 27 elementary schools in the Orange Unified School district. We are located in a residential 
area consisting primarily of single-family detached homes and apartments in the area of Main St. and Chapman Ave. in the city of 
Orange. There are 420 students enrolled in Transitional Kindergarten through 5th grade during the 2018/2019 school year. West 
Orange maintains a traditional school calendar. 
 
The original school site was built in 1890 and the existing permanent structure was built in 1949. The West Orange School community 
is committed to establishing an environment of quality learning. Teachers align instruction and assessments with the California State 
Content Standards. The students are challenged to reach their full potential in order to be productive citizens in a global society. 
 
West Orange Elementary takes a child-centered approach to education. Every child has the capacity to learn and the right to the best 
possible education. Instruction is differentiated to meet the individual student's needs and learning styles. West Orange has a 
diagnostic Response to Intervention and Instruction system in place where every student is given a universal screening assessment 
three times per year. The results of these assessments guide teachers in forming homogeneous groups where the specific needs of 
each student are addressed during a scheduled intervention time five days a week for 30 minutes in language arts. 
 
West Orange continues to excel in academics and exceed the State's academic targets. We are committed to creating a safe and caring 
environment that fosters the intellectual, social, emotional and physical growth of each student. The development of literacy, critical 
thinking, and problem solving skills are crucial to the success of our students as we focus on vocabulary development and reading 
comprehension. 
 
The West Orange staff has worked diligently and efficiently in order for our community to be favorably impressed by the quality of 
instructional program and our desire to prepare responsible individuals to develop excellence. West Orange was selected by the 
California State Department of Education to receive the Gold Ribbon School Award in 2016 as well as the Title I Academic Achievement 
Award in recognition of the West Orange WIN program. It is our goal to do whatever it takes to promote continuous and sustained 
academic achievement for every student at West Orange. 
 
Vision: 
West Orange Elementary School will strive for academic excellence by providing all students a meaningful and rigorous learning 
experience in order to develop the intellectual, creative, social, emotional, technological, and physical skills necessary to prepare them 
for the next phase of their lives. 
 
Mission: 
Attributes of a Warrior Scholar: 
 
We expect our students to: 

Speak, listen and write in complete sentences 

• Be able to read, analyze and evaluate complex text, both fiction and nonfiction; independently 
• 
• Critically think, communicate and collaborate to creatively solve problems in a variety of ways across disciplines 
• Use evidence from text to support an argument across disciplines 
• Demonstrate Global Competencies: investigate the world, recognize perspectives, communicate ideas and take action 

(Council of Chief State School Officers, 2010) 

• Understand that it is okay to solve problems in different ways 
• Use and understand numbers and number relationships for problem solving with automaticity and fluency 
• Be productive citizens by showing respect, empathy, kindness, responsibility, ad tolerance within the community 

 

2017-18 School Accountability Report Card for West Orange Elementary School 

Page 2 of 11