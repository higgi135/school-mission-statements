Ripona School enrolls grades Transitional Kindergarten through eighth grade on a traditional school calendar system. Ripona School is 
one of five elementary schools served by the Ripon Unified School District, located in San Joaquin County. In addition, the district has 
one comprehensive high school with an independent study program and a continuation school. Ripona School is part of the Ripon 
Unified School District. For the 2017-2018 school year, the enrollment for grades TK-8 was 429 students. The ethnic makeup of the 
school was 48.6% Hispanic, 43.1% Caucasian, 4.5% Asian, .5% African-American, .5 Pacific Islander, and 2.9% other/multiple ethnicity. 
 
Our school staff included a total of 20 Credentialed Teachers, 1 Administrator, and the following support personnel: 6 Instructional 
Assistants, 2 Secretaries, 1 librarian, 2 Bilingual Aide, 1 Bilingual Aide/Family Liaison, and two Custodians. Our school has an active 
Parent Club, School Site Council, and English Language Advisory Committee (ELAC). 
 
Our Mission is as follows: 
We are committed to working together with parents and the community to provide a high quality education. The school will create a 
safe learning environment characterized by trust and respect. We ensure that each student will be a contributing citizen in an ever 
changing diverse and global society.