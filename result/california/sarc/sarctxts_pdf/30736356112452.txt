Robinson Elementary School, a California Distinguished School, is committed to helping students 
achieve success and self-confidence by providing an outstanding academic program in a nurturing 
environment. Our students successfully completed the California Assessment of Student 
Performance and Progress (CAASPP), under the Common Core State Standards, and improved our 
overall scores in both Literacy/English Language Arts and Mathematics. Fifth grade students also 
completed the next phase of the California Science Test (CAST) pilot exam. The information gained 
from our students' performance on this test guides our instructional development. We look 
forward to these challenges as we are constantly seeking out and refining best practices. 
 
Our vision is to provide the highest quality educational experiences to enable all students to 
become contributing members of society. Students are empowered with the technical skills to 
succeed in our evolving world, the ability to think and express themselves clearly, and the values 
necessary to be responsible citizens. 
 
Robinson Elementary provides all children with experiences in the process of learning. The faculty 
teaches a standards-based curriculum that provides students the skills of reading, writing, 
mathematics and technology. The expanded core curriculum includes music, art, physical 
education, health, social sciences and science. These learning experiences allow for universal 
access that takes place in a setting providing a healthy balance between structure and spontaneity 
within an atmosphere which features outstanding physical resources. Accomplishments and 
performances in areas such as academics, the arts, community service, character development and 
citizenship are recognized. 
 
We believe in: 
~a challenging academic program 
~fostering success at school 
~providing a safe environment 
~developing a sense of "family" at school 
~responsive communication 
~providing experiences in extra-curricular activities 
~building good character through teaching high standards of behavior, responsibility and 
citizenship 
 
 
Jonathan Kaplan, Principal 
 

2017-18 School Accountability Report Card for Robinson Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Robinson Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

25 

21 

21 

0 

0 

0 

0 

1 

0 

Saddleback Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1,157 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Robinson Elementary School 

16-17 

17-18 

18-19