Grace Hudson Elementary School houses the Ukiah Unified School District’s Dual Language 
Immersion Program (Spanish/English) that serves students in grades TK-5. Grace Hudson 
Elementary School has a certificated staff of 27 comprised of: 18 classroom teachers, 1.0 FTE 
Student Success Coordinator, 1.0 School Counselor, 2.0 FTE Reading/Intervention teachers, a 1.0 
FTE special education resource teacher, a 1.0 FTE Physical Education teacher, a .4 FTE 
speech/language therapist consultant, a .20 FTE school psychologist, and a .20 FTE District Nurse. 
Grace Hudson Elementary School has an additional staff of 31 classified employees including: a 
classified after school program coordinator, one parent/community 
four PE 
paraprofessionals, two bilingual resource paraprofessionals, 
 two bilingual kindergarten 
paraprofessionals, one SPED Para professional, one half time SPED paraprofessional, four part time 
ASES paraprofessionals, two secretaries, one health assistant, two full time custodians and one 
half time custodian, two crossing guards, a library technician, three lunch time supervisors, and two 
food service workers. Staff members of Grace Hudson Elementary School have a strong 
commitment to excellence in teaching and promoting bilingualism, bi-literacy, and biculturalism 
through the use of the CA Common Core standards, as well as the commitment to self, others and 
the environment. 
 
School Vision and Mission 
Mission: Grace Hudson Elementary School provides highly qualified teachers, rigorous bilingual, 
biliterate, and technology enhanced curriculum while fostering cross cultural appreciation and 
promoting family involvement through Spanish Dual Language Immersion . 
 
Vision 
All students moving on from Grace Hudson Elementary School will be bilingual, biliterate, and 
multicultural and leave with: 
 
A mind able to solve problems and think critically 
Eyes that see multiple points of view 
A smile that displays a love of learning 
A heart filled with empathy and ready to help ourselves and others. 
Hands ready to work hard and protect the environment 
A foot forward, ready to succeed in the world 

2017-18 School Accountability Report Card for Grace Hudson Elementary School 

Page 1 of 7 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Grace Hudson Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

23 

22 

22 

2 

0 

0 

0 

0 

0 

Ukiah Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

335 

22 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.