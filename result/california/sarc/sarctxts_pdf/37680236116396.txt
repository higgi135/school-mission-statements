Thurgood Marshall Elementary School (TMES), established in 1999, is located in San Diego County 
and serves a culturally and economically diverse population of 660 Transitional Kindergarten 
through sixth grade students. The Chula Vista Elementary School District, with 49 schools including 
charters is the largest elementary school district in California. Our diverse population includes 
Hispanic, Asian, Filipino, White, Two or More Races, and African American students. 
 
Diversity is celebrated at Marshall school and the cultures of our students are incorporated into 
our instructional curriculum and school-wide activities. A highly visible mural facing our community 
proudly displays the rich cultural heritage of our students. This mural, created in collaboration with 
Southwestern College, is updated to reflect the cultures of our newly enrolling students. Our school 
community values: honesty, integrity, respect, diversity, commitment, perseverance, teamwork, 
equity, optimism, and creativity. We are a school committed to justice, equality, and human rights 
in the spirit and ideals of Supreme Court Justice Thurgood Marshall. 
 
TMES is a caring, safe community that focuses on the academic and social/emotional development 
of all students. 
 
Vision Statement 
Through the use of best practices, technology, community involvement, and collaboration, we will 
provide an inclusive personalized learning experience that inspires lifelong learning for everyone. 
 
Mission Statement 
Through excellence, rigor, innovation, creativity, and compassion, we inspire lifelong learners. 
 
 

----

---- 

Chula Vista Elementary School 

District 

84 East J Street 

Chula Vista, CA 91910-6100 

(619) 425-9600 
www.cvesd.org 

 

District Governing Board 

Leslie Ray Bunker 

Armando Farias 

Laurie K. Humphrey 

Eduardo Reyes, Ed.D. 

Francisco Tamayo 

 

District Administration 

Francisco Escobedo, Ed.D. 

Superintendent 

Jeffrey Thiel, Ed.D. 

Assistant Superintendent, Human 
Resources Services and Support 

Oscar Esquivel 

Deputy Superintendent, Business 

Services and Support 

Matthew Tessier, Ed.D. 

Assistant Superintendent, 

Innovation and Instruction Services 

and Support 

 

2017-18 School Accountability Report Card for Thurgood Marshall Elementary School (TMES) 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Thurgood Marshall Elementary School (TMES) 16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

36 

0 

Teaching Outside Subject Area of Competence 

NA 

29 

31 

0 

0 

0 

0 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.