King-Chávez Academy of Excellence Academy (KCAE) is a K-8 charter school located in southeast San Diego. Our mission is to "Seek 
excellence in Academics, the Arts and Athletics from the Foundation of Love." This statement provides our curricular, instructional, 
and philosophical focus. We strive to create an academically rigorous, supportive, and safe environment where the relationship 
between the teacher, student, and family can thrive. KCAE provides its students with on-site academic and social supports to promote 
academic success for all. Curriculum and instruction is delivered by a dedicated and qualified faculty who hold high academic 
expectations for all students.