Pioneer School is located in Somerset, in the center of a small rural district serving several local communities spread over a vast 
geographic area. The Pioneer Union School District consists of three school campuses — Pioneer Elementary, Mountain Creek Middle 
School, and Walt Tyler Elementary School. 
 
Pioneer Elementary School provides students in grades K-5 with an opportunity to academically excel to their capacity and to develop 
confidence in themselves. The school strives for a close and respectful “family atmosphere” with a strong partnership between the 
home and the school. 
 
Core Values 
The mission of the Pioneer Union School District is best accomplished through the advancement of these core values: 
1. The uniqueness of the individual 
2. The partnership among home, school, and community 
3. A respectful, compassionate, cooperative and safe environment 
 
Guiding Principles 
The following principles will guide our work: 
1. Students will be provided programs, instruction and/or activities designed to enhance the development of positive attitudes, 

patience, tolerance, appreciation of different cultures and productive membership in society. 

2. Students will be provided programs, instruction, activities and/or guidance to develop mental and moral bravery, caring attitudes 

toward people and education, to gain a healthy view of competition and to learn age appropriate skills.