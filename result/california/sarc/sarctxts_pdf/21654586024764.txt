Coleman Elementary School, located east of Highway 101 near central San Rafael, is one of eight 
elementary schools in the San Rafael Elementary District and serves students from the Dominican 
neighborhood, the San Rafael High School area, and downtown San Rafael. Coleman was rebuilt in 
2003 and is a lovely community school. The staff and community focus on literacy in reading, 
writing and mathematics. Our Parent Teacher Organization focuses on community building and 
fund raising, and are able to support all students access a variety of opportunities, including PE, Art, 
Music and Technology. 
 
Coleman serves students in grades kindergarten through fifth in central San Rafael. Our current 
enrollment is approximately 406 children. We are comprised of 46% Socioeconomically 
Disadvantaged students and 32% English Learners. Our school is supported by our Parent Teacher 
Organization (PTO); School Site Council; our Site English Language Advisory Committee (SELAC), 
and an active Student Council. Our collective mission is to guide and support each child in the 
journey toward academic and personal excellence. 
 
New for 2018-19, Coleman is engaging in a push for a Greener campus. The school has engaged 
with Zero Waste Marin, activated a student Green Team and is moving toward a school that 
produces as little waste as possible. 
 
 
 

2017-18 School Accountability Report Card for Coleman Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Coleman Elementary School 

16-17 17-18 18-19 

Teacher Credentials 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

San Rafael City Schools 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

20 

17 

19 

0 

0 

0 

0 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

248 

0 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Coleman Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.