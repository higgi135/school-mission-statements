A Message from the Principal, 
Helen Wittmann Elementary School is a place where students gain an outstanding “world class” 
education with a veteran teaching staff and strong community support. Our “Enter to Learn, Leave 
to Achieve” model focuses on producing well-rounded students who are intelligent, adaptable and 
thoughtful of others. Our school wide expectations of being Respectful, Responsible and 
Compassionate make Wittmann a nurturing and supportive learning environment. We are a 
“Peacebuilders” school and promote peaceful interactions amongst staff and students through a 
positive behavior system. Our staff members use a system of Praise Notes and other reward 
systems to keep our students academically and emotionally engaged. 
Beyond our rigorous core curricula, Arts education is supported through Wittmann’s Beginning and 
Advanced Bands, Art 4 Kids, weekly Vocal Music Rotation Classes, Robotics, and abundant field trips 
to performances at the nearby Cerritos Performing Arts Center. Our lunchtime sports tournaments, 
as well as our annual ‘Jump-a-thon’, ‘Wittmann Olympics’ and District Champion Track Team keep 
our students physically active. Overall, Wittmann has 19 different enrichment and career 
opportunities for our students ranging from video game programming to performing arts. 
Wittmann is a special place where all staff, parents and students are incorporated into our learning 
community and a school that supports and encourages the involvement of all school stakeholders. 
The “Wittmann Organized Warriors” or WOW supports our school by funding programs such as 
music, technology, field trips and student events that enhance the learning experience for all 
children. This year our students will experience Robotics and compete in regional Math, Reading 
and Science competitions. WOW is a special part of our school and allows our staff to introduce 
new and exciting experiences for our students. Our staff and parent groups work closely together 
to create a unified vision for success and truly embody that together, “We are Wittmann!” 
 
Miguel Marco 
Principal 
Wittmann Elementary School 
 
Wittmann Elementary School, located in the city of Cerritos, serves 596 students in grades 
transitional kindergarten to six on a traditional calendar system. Wittmann Elementary School is 
dedicated to ensuring the academic success of every student and providing a safe and 
comprehensive educational experience. 
 
Helen Wittmann Elementary is a school community that maintains very high academic and 
behavioral expectations. Wittmann utilizes all available resources to enable students to become 
life-long learners who possess the ability to achieve their utmost potential. Through the schoolwide 
"Peacebuilders" program, we promote a safe, nurturing, and stimulating environment that 
promotes personal responsibility and social competence. Wittmann provides a welcoming 
atmosphere in which parents and community members are encouraged to become immersed in 
student's educational endeavors. 
 
Mission Statement 
Teaching by example, we inspire students to love learning, support one another, and follow their 
dreams. 
Vision Statement 
Our vision is to be a school renowned for producing excellent students, compassionate beings, and 
responsible citizens. 

2017-18 School Accountability Report Card for Helen Wittmann Elementary School 

Page 1 of 9 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Helen Wittmann Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

22 

23 

23 

0 

0 

0 

0 

0 

0 

ABC Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Helen Wittmann Elementary 
School 
Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

16-17 

17-18 

18-19 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.