Our Mission 
It is the mission of Sierra House Elementary School, in partnership with families, to provide a high achieving learning environment to 
prepare our students to be; educated, caring, organized, responsible and empowered citizens. 
 
Our Vision 
We as a school community work together to provide opportunities that will meet the individual needs of all students. We work 
collaboratively to enable our children to grow and learn in an environment that fosters a positive, empowered learning experience. 
 
We will continue to: 

• Create partnerships with families and work as a community to shape students who are caring, organized, responsible and 

empowered. 
Integrate positive mindset techniques to allow all children to learn equally. 

• 
• Continue to develop ongoing, meaningful collaboration to allow cross integration of all subject matter and diversity of 

teaching modals. 

• Provide more hands-on STEAM based opportunities to allow our students to excel now and in the future. 

Our Values 
There are four key values that we instill in our students and staff every day: 
 
Caring- Care for yourself, others and the environment 
 
Organized- Organize your work and your actions 
 
Responsible- Be responsible for your thoughts, your actions and your learning 
 
Empowered- Empower yourself and others through your thoughts, words and actions 
 
Sierra House Elementary School is located in the geographic heart of South Lake Tahoe, California. Nestled in the Sierra Nevada 
mountain range and located near the California/Nevada state line, South Lake Tahoe is a resort town where many families depend on 
seasonal employment in the casinos and ski resorts. In October of 2018, our enrollment was 467 students. The Sierra House student 
population was composed of 45.2% female and 54.8% male students. Demographically, our student population was 38.8% Hispanic, 
48.8% Caucasian, 5.4% Filipino, 1.3% Black and 5.7% “Other.” Our October of 2018 Free and Reduced Lunch report estimates that 
54.2% of our students qualified for Free or Reduced Lunches. 30.4% of our students were designated as English Learners. We have 19 
regular education classes and 2 Special day classes for moderate-severely handicapped students. 
 
As a Professional Learning Community, we welcome collaborative discussions to help determine the best instructional strategies to 
utilize with our students on an individualized level. In order to allow us to monitor progress every student has an Individual Student 
Report in Illuminate to record assessment scores four times during the year. We have a very strong intervention team that uses data 
to drive placement and instructions of all students who need extra support in reading. Additionally, team “Data Meetings” are held at 
least three times a year and regularly scheduled monthly grade-level collaboration meetings support staff’s ability to analyze student 
performance and progress towards our school goals. Response to Intervention (RTI) is another way that our staff is working together 
to individualize instruction and differentiate to assure high levels of student learning. Our entire staff is involved in our work in RTI, 
with a focus on both targeted academics and behavior. 
 

2017-18 School Accountability Report Card for Sierra House Elementary School 

Page 2 of 12 

 

Sierra House Elementary has initiated higher level technology use throughout the school day. SMART Boards, document cameras and 
chrome books are installed in every classroom. Students regularly use a variety of technology systems to help enhance learning most 
recently including Mystery Science, Prodigy, Seesaw, and Gizmos among others. We integrate computer technology education into 
our curriculum through STEAM and all students K-5th currently learn to code using a variety of robotic systems including; BeeBots, 
Ozbots, Lego WeDo, Kibo, Spheros and Little Bits. 
 
In 2012/13 our school initiated a school wide theme focusing on ‘Fitness, Health and Mountain Sports’. As of 2018 we have a well-
established garden science-based program, which includes elements of nutrition, healthy eating through cooking and robotics. We 
also have a variety of stable long-term partnerships supporting our sports and health education. We work with local businesses like 
Heavenly Vail and the South Lake Tahoe Ice Arena as well as with local agencies such as CalFresh to enhance all of our learning 
programs. Currently our students enjoy the following experiential learning experiences: 
 
K-5th grades all receive the opportunity to ski at Heavenly Valley 
 
K-5th grades receive professional art lessons twice a year at school 
 
3rd grade have ice skating lessons at City of South Lake Tahoe Ice Arena 
 
4th grade have swimming lessons at City of South Lake Tahoe Recreation and Swim Complex 
 
4-5th grades study instrumental music year-round at school 
 
Our strong fitness-based PE program allows every student K-5th to receive 200 mins of PE within a two-week period. Students also 
participate daily in fitness-based activities within the classroom, and structured activities during recess times. Additionally, parents 
and teachers participate in before and after school enrichment opportunities to expose students to new sports (e.g., Ice Hockey) and 
physical activities (e.g., Mountain Biking Club). 
 
All of these educational opportunities help to instill a life-long ethic of staying physically active and healthy and involve all demographic 
groups at our school.