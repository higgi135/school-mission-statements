Mountain School, located in the town of Gasquet, is one of eleven schools in the Del Norte County Unified School District. Gasquet is 
located on Highway 199, within the Smith River National Recreation Area, about 30 miles east of Crescent City. The area offers many 
outdoor recreational activities utilizing the proximity of the Pacific Ocean, the Smith and Klamath Rivers, Redwood National and State 
Parks, and the Six Rivers National Forest Area. The natural beauty of the area and the abundant wildlife provide a basis for a strong 
tourism sector of the local economy. The area’s largest employers are federal, state, and local government agencies. 
 
Gasquet Mountain Elementary is a K-8, safe, respectful and responsible school that honors the diversity and cultures of its students. 
Our school is 1 to 1 with technology devices for our students. Our learners have the opportunity to work in multi-grade classrooms 
and learn from each other as well as their teachers. The goals of Mountain Elementary School are to continue to offer a STEM Program 
(science, technology, engineering and mathematics) with an emphasis on environmental science. In a world that’s becoming 
increasingly complex, where success is driven not only by what you know, but by what you can do with what you know, it’s more 
important than ever for our youth to be equipped with the knowledge and skills to solve tough problems, gather and evaluate 
evidence, and make sense of information. These are the types of skills that students learn by studying science, technology, engineering, 
and math—subjects collectively known as STEM. We will continue to focus on literacy and mathematics as well as the newly adopted 
Next Generation Science Standards. 
 
Currently, the school has four classrooms, a K-2, a 3-5, and a 6-8 class that is co-taught by two teachers. Teachers employ a variety of 
strategies to personalize and differentiate learning for students. Some of our programs include personalized math instruction with 
Khan Academy, Prodigy, Freckle, and Google Classroom for digital content and personalized learning. 
 
The entire staff is committed to providing each student the highest quality education possible. Our Parent Teacher Organization 
continues to be a tremendous support for community events and student programs. 
 
Mission Statement: 
Each student who attends Gasquet Mountain School will leave with the necessary social, academic and performance skills needed to 
be successful in school and life.