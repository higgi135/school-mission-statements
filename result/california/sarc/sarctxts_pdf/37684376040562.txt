The mission of the Vallecitos School District is to: 

 

• Develop clear and focused learning goals based on high expectations of every student and staff member. Monitor student 
learning frequently to guide instruction and improve student learning. Provide a safe and orderly environment and a 
collaborative school-wide culture. Build positive and productive parental relationships. Encourage all students to become 
productive citizens. Vallecitos School District – Where Achievement Matters! 

Vallecitos School District Core Values: As a collaborative team, we will… 

• Hold high expectations for student learning. Frequently monitor student learning. Involve every student in setting academic 
goals. Ensure all school related decisions are guided by what is in the best interest of student learning. Provide a safe and 
orderly school environment. Demonstrate integrity and hold students and staff accountable for their actions. Treat 
students and staff fairly and with respect. Praise and encourage students. Encourage students and staff to ask for help and 
be willing to help others. Maintain open communication with staff, students and parents. Arrive to school each day with a 
positive attitude. Be flexible and open-minded to new ideas. Contribute time and effort to Vallecitos so the district can run 
effectively.