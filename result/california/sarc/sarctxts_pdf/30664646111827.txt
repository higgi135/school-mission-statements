John S. Malcom Elementary School, a 2001 National Blue Ribbon School and a California Distinguished School, serves pre-school 
through fifth grade students. Our school, which opened in September 1994, is located on a scenic bluff less than two miles from the 
Pacific Ocean in beautiful Laguna Niguel. John S. Malcom Elementary School prepares students to be college and career ready while 
nurturing character development and problem-solving skills. Staff receives the strong support of parent and community volunteers in 
providing many enrichment opportunities across the curriculum. 
 
The Malcom staff is committed to providing a high quality learning environment that emphasizes mastery of grade level standards 
while meeting the individual needs of all students in the core curriculum. Science, Technology, Engineering, Art, and Math (STEAM) 
instruction at Malcom is supported through our outdoor classroom Gerhard Garden, Earth Lab, STEAM Lab, Computer Labs, 
Chromebooks, Lego Robotics, Primary and Block Music Programs, Art Studio, and a partnership with Discovery Education. Malcom 
scholars are faced with daily opportunities to solve real-life application problems while practicing the skills of communication, 
collaboration, creativity and critical thinking. In addition, scholars follow the 3R's every day (Relationships, Respect, and Responsibility) 
and work to solve problems inside and outside of the classroom. The 5th grade Student Leaders at Malcom (SLAM) program provides 
students with ongoing opportunities to participate in community service. 
 
The ultimate goal of the John S. Malcom School Community is a commitment to make a positive difference in ourselves and the world 
in which we live. 
 
MOTTO 
“Making a Difference in Ourselves and in Our World” 
 
VISION 
All scholars will succeed through a partnership among peers, staff, and parents. 
 
MISSION STATEMENT 
At Malcom School, scholars and staff strive for academic excellence while building relationships, respect, and responsibility so that 
scholars will meet the challenges of a rapidly changing world.