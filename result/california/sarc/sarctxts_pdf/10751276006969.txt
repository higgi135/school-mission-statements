McCabe Elementary School is one of 7 schools within the Mendota Unified School District, a rural 
area located 40 miles west of Fresno. According to CBEDS information taken in October 2018, 
student enrollment for the 2018-2019 school year totals 858 students in grades 2, 3, 4, 5, and 6. 
The student population is made up of approximately 98% Hispanic students and 2% other. Poverty 
is apparent in the agriculture-based community, where 97% of the students qualify for the Free or 
Reduced Lunch Program. The English learner population makes up 69% of the total student 
population. Migrant students make up 8% of the total student population. 
 
The McCabe Elementary program builds upon its strengths. The teaching staff is stable with 
minimal turnover in the past fifteen years. 
 
The district and site administration is dedicated in providing the necessary forum, time, and 
structure to allow for staff development, workshops, and curriculum planning. Teachers at McCabe 
Elementary are currently CLAD or BCLAD certified. Our staff is also involved in ongoing professional 
development through the Fresno County Office of Education, Bridges to Leadership Program, T4 
Lesson Delivery Training, Kagan Structures, the Teacher Induction Program, Guided Reading 
Strategies, and SWUN Math strategies and other district developed professional development. 
Additionally, we have a leadership team that is composed of teachers and administration that is a 
key component in the decision-making process. Our paraprofessionals are also committed to 
continuing their education. 
 
The majority of our student data is recorded using Illuminate which serves as an ongoing evaluation 
tool of student progress. The staff uses classroom data to follow the progress of each student, to 
provide focused Response to Intervention instruction, and to develop differentiated curriculum and 
instruction delivery for our students. 
 
Our one to one chromebook student computers are internet ready which provide access to our 
Accelerated Reader and Math Programs, the Jiji math program, benchmark assessments, Common 
Formative Assessments, and Smarter Balance Assessment Component. Additionally, every 
classroom is equipped with SMART Technology or Promethean Boards, which includes a document 
camera, an infrared sound system, and a microphone. We have also added the program Imagine 
Learning, which targets our Migrant, students in our Intensive groups, and EL population. 
 
On a regular school day, students in 4th – 6th grade are afforded a 345 instructional minute day, 
3rd grade is afforded a 335-minute day, and 2nd grade is afforded a 325 instructional minute day. 
Several minimum days have been scheduled throughout the 2018-2019 school year for Professional 
Development, which include but are not limited to English Language Development, technology, 
instructional strategies and methods, test-taking preparation, literacy, common core, math and/or 
curriculum 
 
McCabe Elementary School will build upon student individual strengths while addressing challenges 
by providing the highest quality education. We are dedicated to reaching all students through data 
driven collaboration and reflective instruction. We provide opportunities for students to succeed, 
evaluate, and build problem solving skills that will prepare them for their future. 
 

2017-18 School Accountability Report Card for McCabe Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

McCabe Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

25 

26 

28.5 

8 

0 

8 

0 

8 

0 

Mendota Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

148 

14 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

McCabe Elementary School 

16-17 

17-18 

18-19