Gomez Elementary School opened in fall of 2013. The school has 27 classrooms, a media center, a library, a cafeteria, a multipurpose 
room with a stage, a music room, a parent center, a counselor’s office, and an administration office which houses the administration, 
speech pathologist, educational psychologist, health aide, PE Specialist and the staff room. The facility strongly supports teaching and 
learning through its ample classroom and playground space. 
 
Gomez Elementary School serves students in grades K-6 and is the only school in the district to offer the ABE Maintenance program 
that promotes bilingualism and bi-literacy in grades K-6. We offer before and after school intensive instruction, a GATE club, and an 
after-school CAPS program. We are a PBiS school and our mission is to build a positive community that fosters a setting for lifelong 
learning and that builds academic excellence and bi-literacy in a fun, engaging, and inspiring way in a safe, respectful, and responsible 
environment. We strive to prepare our students to reach their academic potential and to compete in the global society of the twenty-
first century.