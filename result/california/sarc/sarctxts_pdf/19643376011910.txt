Bret Harte Elementary school provides an atmosphere that promotes academic success for all of our students. Our partnership with 
families and the community supports students in academic achievement, social development, and becoming citizens of the community 
and world. 
 
At Bret Harte Elementary we believe the following: 
 

· 
· 
· 

· 

Every student has the ability to learn and succeed in a caring nurturing environment. 
Children are more alike than they are different, and that both these similarities and differences must be celebrated. 
Parents are a valuable component in the education of their children. We want to be partners with parents and encourage 
active participation. 
High expectations and consistent assessment of student progress toward meeting standards are essential for every student 
to do his or her best. We will prepare students to be successful in today's world. 

 
At Bret Harte, our mission is to develop and maximize the basic academic skills, citizenship, personal growth and problem-solving skills 
of all students by providing a quality instructional program that is accessible to all children, and addresses their individual and diverse 
needs through shared decision making while meeting state and District guidelines.