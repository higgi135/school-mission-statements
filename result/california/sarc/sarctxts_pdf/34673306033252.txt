Theodore Judah Elementary School stresses success for every student every day. Development of cognitive and intellectual skills, 
problem solving abilities, concern for others, and an appreciation of the arts and technology are our goals. Student progress is 
continually monitored. Ongoing services and workshops are provided for our teachers through district wide opportunities and on site 
staff development activities to enhance the effectiveness of instruction. 
 
Theodore Judah's music program is available to students in grades 4 and 5. Band is open to 5th graders, while strings is available to 
all 4th and 5th grade students. Among the many activities students are given the opportunity to participate in include: Student 
Council, Spirit Days, Read Across America Day, STEM Day, spelling bee in upper grades, and Family Science Night. The library and 
computer lab are available to students on a regular basis. 
 
Parent participation is encouraged to foster a close, cooperative relationship between home and school. Our Parent Teacher 
Organization (PTO) is very active, providing many activities and events for Theodore Judah students and families. Pawprints, our 
weekly newsletter, keeps parents informed about schedules, events and activities happening at school. An online computer program, 
Power School, allows two way communication between teacher and parent and includes the ability for a parent to check grades and 
attendance. Frequent communication, regular conferences and parent involvement in the classroom all help to build a positive 
learning environment. 
 
Theodore Judah also has a very active community program where high school students, senior citizens, business leaders, and a faith 
based organization spend time volunteering in our classrooms. Walmart, Target, Raley's, Intel, Kaiser, Micron, Folsom PD and Folsom 
Fire have all donated funds or time to Theodore Judah. Each year Intel awards our school a monetary gift based on volunteer hours to 
be used primarily for math, science and technology. Last year Intel volunteers earned us over $18,000. Junior Achievement of 
Sacramento also sends volunteers to present interesting lessons to enrich the curriculum for our students. 
 
The staff at Theodore Judah works hard to provide a caring atmosphere where students can learn from outstanding professionals who 
are well trained and highly educated. It is our mission, along with that of the Folsom Cordova Unified School District, to provide 
excellence in educational programs that carry high expectations for each student's achievement and success. At Theodore Judah we 
are "proud of our past and committed to our future." 
 
Theodore Judah encourages and respects a connective relationship between home and school. We also value our community contacts 
and parent participation. The strong parent support that we receive helps us in our goal of providing an excellent educational program. 
 
Our student enrollment, reported on the California Basic Educational Data System (CBEDS) in October 2017 was 613 students. 
 
 

2017-18 School Accountability Report Card for Theodore Judah Elementary School 

Page 2 of 11