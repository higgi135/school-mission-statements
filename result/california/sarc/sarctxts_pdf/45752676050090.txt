Welcome to Buckeye School of the Arts (BSA). This School Accountability Report Card (SARC) will provide you with a wealth of 
information about our school accomplishments, resources, programs, students, and staff. Buckeye School of the Arts has launched off 
in new and exciting directions that are guaranteed to provide a successful educational environment for your children. At Buckeye, we 
believe that when parents team up with the school, everyone wins! Your support and involvement is important to us and your child's 
success is our number one priority. 
 
Buckeye School of the Arts Mission Statement: 
To provide a safe, supportive environment which promotes academic excellence, responsible citizenship, and a life-long desire for 
learning. 
 
Buckeye School of the Arts Vision Statement: 
It is the vision of Buckeye School of the Arts that the united efforts of students, parents, community, and staff will develop students 
who... 
Value themselves 
Come to school ready to learn 
Speak and write effectively 
Engage in environmental concerns and science 
Compute and problem solve 
Use resources of information and technology 
Appreciate and participate in visual and performing arts 
Value personal health and fitness 
Interact respectfully within the school community and in society 
 
The vision is that BSA students, families, and staff strive for high quality student work within a safe, enriched, and caring environment, 
utilizing a wide variety of resources and strategies.