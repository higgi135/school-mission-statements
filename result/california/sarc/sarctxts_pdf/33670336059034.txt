CFIS Beliefs 

 

• 
• 
• 

The goal of our educational programs is to prepare students to become contributing members of society 
Education is the key to opportunity and social mobility 
The success of our school system depends on the commitment of all staff (administrators, teachers, and support staff) to 
high quality standards, expectations, and performances. 

• A safe and supportive learning environment with high standards of learning promote student achievement 

CFIS Mission Statement 
 
The mission of Corona Fundamental Intermediate School is to provide students with a rigorous, standards-based educational program 
within a safe, supportive learning environment. With an emphasis on problem solving, critical thinking, and effective communication 
skills, students will become contributing members of society who respect peers, authority, and country. 
 
CFIS Vision Statement 
 
The entire CFIS staff is committed to providing a safe, supportive learning environment, with emphasis on high academic achievement, 
which will prepare our students to become contributing members of society. 
 
Located in the heart of Corona’s Historical Circle, Corona Fundamental Intermediate School has stood watch over the city, a shining 
example of culture, architecture, and education. Over time, Corona has grown from an agricultural citrus community to a suburban 
community of over 154,000 residents. As the population exploded, CFIS successfully converted to a year-round schedule. Since then, 
CFIS has transitioned to a traditional calendar to align with the remainder of the schools in Corona and Norco. Through it all, CFIS has 
stood the test of time and remained the premier site for middle school academic instruction and excellence. 
 
Corona Fundamental has two main buildings. The first is a three-floor building that contains administration, counseling, 36 classrooms, 
two computer labs, and a small fitness lab. The second houses the library, the café, six classrooms, an Career and Technical Education 
center, band/choir and woodshop. Our library offers over 18,000 books including those on the state recommended reading list 
including varied levels of books for research, class sets of novels, and Caldecott and Newberry Award winners. The CFIS Teacher 
Resource Center offers a place where teachers can view supplemental materials for extending and enriching the curriculum, and view 
a selection of videos. In addition to these structures, CFIS has a multipurpose room, with the boys’ and girls’ locker rooms at the back 
of the building. 
 
Corona Fundamental Intermediate School is a back-to-basics fundamental intermediate school whose motto “Traditional Values—
High Expectations—Successful Students” embodies the heavy emphasis placed upon academic excellence. Parent-school partnership 
is the foundation of the CFIS program and is critical to its overall success. Students from throughout the District apply to attend CFIS 
and, when necessary, students are chosen through a lottery system. CFIS does not have a feeder school, but the school does have a 
priority system. The first priority is to serve the immediate neighborhood children. These students come from a low socio-economic 
area of Corona and flourish with the structure and support Corona Fundamental provides. CFIS also gives first priority to students who 
have a sibling who will be in eighth grade at CFIS. Second priority students come from Lincoln Alternative Elementary School, which 
espouses the same basic philosophy of Corona Fundamental. Corona Fundamental’s third priority is to serve students from throughout 
the entire Corona-Norco community. 
 
Corona Fundamental has a diverse population of approximately 789 students served on a traditional school calendar. The composition 
of the student body for 2018-19 is 68.5% Hispanic, 13.9% white, 9.7% Asian, 1.7% Black/African American, and 3.1% Filipino. 
Additionally, 62.6% our students are economically disadvantaged, 15% of students are English Learners and 5.5% are in the special 
education program. 

2017-18 School Accountability Report Card for Corona Fundamental Intermediate School 

Page 2 of 14 

 

 
Corona Fundamental offers a wide range of programs, including English Language Development, special education, Pre-Ap classes, 
Read 180, Math 180, AVID, and the International Baccalaureate Middle Years Program. During their 351-minute instructional day, 
students have seven periods. In addition to core classes in language arts, mathematics, social studies, science, and physical education, 
students choose from a wide variety of elective classes including woodshop, computer science, and visual and performing arts. For 
students struggling in literacy and mathematics, CFIS offers a Read 180 General and Essentials elective, ELA academy elective, Math 
180 elective , and math academy elective. In 2004-2005, CFIS implemented an inclusive learning program to support all at-risk 
students. Resource specialists assist the general education teacher by accommodating or modifying lessons to ensure that all students 
learn. Specialists and instructional assistants work in the classroom to determine and meet needs of every student. Students on IEP's 
are also placed in a Academic Support Core Basics class to support literacy and math gaps. 
 
Over the last few years, Corona Fundamental has focused on he social emotional side of our student population. Our counselors have 
created a comprehensive counseling plan with supports for at- risk students as well as developing all students into leaders. Currently 
the Riverside Area Rape Crisis center runs a Be Strong and My Strength group that teaches boys and girs how to be young adults and 
leaders in the future. We have students participating in Peer mediation as well as a Flywise leadership group though our counseling 
department. 
 
Over the last 3 years we have established a school wide PBIS team called the Culture and Climate Task Force. Our Tier 1 team has 
developed the Falcon Way through various staff meetings and planning days. The Falcon Way is taught through video lessons and 
embedded into classrooms for positive reinforcement. This year we have established a Tier 2 team who will be working on developing 
supports on site for students who are not successful with the Falcon Way. 
 
Important aspects of the CFIS culture include the following: 

Insistence on good study habits, self-discipline, and responsibility as defined in the agreement. 

• Voluntary enrollment. 
• High behavioral and academic expectations are defined by an agreement signed by the student and parent. 
• Emphasis placed on etiquette and citizenship. 
• 
• Every adult on campus is responsible for every child. 
• Lunchtime teacher and student participation in activities that generate team spirit and school bonding. 
• Emphasis on patriotism, supported by the display of patriotic bulletin boards. 
• Positive parent contact policy. 
• Strict enforcement of dress code with parental support. 
• Parent-supported ethics policy. 
• Attention to lunchtime decorum and maintenance of a clean campus. 
• High student, parent, and staff attendance at school functions. 
• Parent-provided transportation. 
• Low teacher turnover.