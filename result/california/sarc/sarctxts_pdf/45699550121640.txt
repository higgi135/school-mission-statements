Cottonwood Creek Charter School is located in the historic town of Cottonwood in Northern California. Cottonwood Creek Charter 
School is sponsored by the Cottonwood School District and offers a quality, tuition free education to K -8 students. Our family-friendly 
school schedules are independent study based. This allows parents to have the option of homeschooling their students or sending 
them to our site based classrooms for a more traditional classroom education. This model ensures that each student, whether home 
schooled or attending site classes, receives a personalized learning plan that focuses on his/her educational strengths and needs. Our 
site based classes are small (approximately 22 students per class) to optimize learning and teacher/student interaction. Our site classes 
are held Monday - Thursday mornings. In the afternoon, students have the option to select electives in Music, Art, Foreign Language 
and Technology. Fridays are reserved for field trips. Field trip days provide educational and hands-on learning in nature and the 
community. Our field trips have included: skiing, hiking, visiting local museums, Shasta Dam Tour, Shasta Caverns, and a Community 
Service Day. 
 
Our Mission Statement: 
Students at Cottonwood Creek Charter School will become well rounded participants in their community today as well as in the future. 
We will use a blended learning model that combines both site based classes and Personalized Learning to develop an individualized 
program of study for every student, melding "handson" educational opportunities through technological resources, educational field 
trips, local experts and a dedicated, committed staff of talented teachers and parents.