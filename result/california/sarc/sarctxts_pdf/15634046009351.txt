Cecil Avenue Math and Science Academy (CAMS), “Home of the Patriots”, is one of four middle schools in the Delano Union Elementary 
School District. Cecil Avenue serves 6th, 7th, and 8th grade students. Students come to Cecil Avenue from feeder schools in the district. 
The bulk of our students arrive to us via Princeton Elementary, Terrace Elementary, and Del Vista Math and Science Academy. 
 
School Mission Statement: 
The staff of Cecil Avenue Math and Science Academy accepts and expects high levels of learning from all students. 
 
School Vision: 
The professional learning community of Cecil Avenue Math and Science Academy exists to instill in its students a desire to become 
“lifelong learners”. The staff of Cecil Avenue is committed to do “Whatever It Takes” by providing high quality standards based 
instruction on a daily basis and believes that our “Patriots” are college-bound, especially in math and science.