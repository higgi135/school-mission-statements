Principal Lee Shaw's Message: 
Welcome to the new school year at El Capitan High School, home of the Mighty Gauchos! If you are new to El Capitan, welcome to the 
Gaucho Family! You are coming to an awesome school where students come first, academics are a priority, and excellence is strived 
for inside and outside the classroom. The school is part of a forward-thinking district that strives to be on the cutting edge of education 
as we aim to equip all students to be college and career ready by the time they graduate. At El Capitan, as well as throughout Merced 
Union High School District, staff and students are supported with the latest resources to allow them to function at the highest levels 
in modern education. We strive for our students to be 21st Century learners as well as 21st Century leaders. 
 
Our staff, parents, and community have been extremely supportive in helping El Capitan become established as the great school it 
is.This year, we will continue to work together to enable our students to master the skills required to be successful as they progress 
towards their chosen careers or colleges. We will strive to enable them to think globally and become critical thinkers who are able to 
evaluate and solve real-world problems. Progressing forward, we will continue working together as one family. To help keep this in 
mind, this year’s school hashtag is #gauchofam. When you see Gauchos showing that family attitude and community mindset, please 
use #gauchofam as you post to your social media platform of choice. We are one family. 
 
Welcome! 
 
Lee Shaw 
Principal 
El Capitan High School 
(209) 384-5500 
lshaw@muhsd.org 
“Learn, Love, Lead & Leave a Legacy” 
 
Mission Statement 
El Capitan High School is committed to developing 21st-century leaders who demonstrate integrity, honor, and compassion as they 
gain the academic and technological skills needed to achieve excellence in college and career. 
 
Vision Statement 
Learn, Love, Lead, and Leave a Legacy 
 
School Description 
El Capitan High School is the sixth comprehensive high school in the Merced Union High School District and the third comprehensive 
high school built in the city of Merced. It is the district's first 1:Web school, with a "bring your own device" option, or the option of 
using the school supplied Google Chromebook. All students will have their own device to use at school and at home. The school is 
located in North Merced close the UC Merced campus. The school opened on August 19, 2013 for freshman and sophomore students. 
An additional grade level was added for the 2014-2015 and 2015-2016 school years. El Capitan is rich in diversity: the multitude of 
ethnicities, backgrounds, languages spoken, and cultural heritages represented in the student population are a source of strength and 
learning at the school. 
 
 

2017-18 School Accountability Report Card for El Capitan High School 

Page 2 of 14