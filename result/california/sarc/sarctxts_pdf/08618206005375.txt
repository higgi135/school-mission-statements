School Description: 
Bess Maxwell Elementary School, located in Crescent City, California, is one of eleven schools in the Del Norte County Unified School 
District. Bess Maxwell has a student population of approximately 300 students in Preschool through Grade 5. Our school has many 
programs that support learning and community partnerships. Some of these include Positive Behavioral Interventions and Supports 
(PBIS), The Dolphin Pride Student Assemblies and Awards, Teacher and Classified Appreciation Days, Family Reading Picnic, Spelling 
Bee, Multi-Cultural Day and monthly PTSO sponsored events. Bess Maxwell Elementary School offers Special Communications Class, 
Special Day Class, Speech Program, Resource Specialist Program and After-School Program. Bess Maxwell Elementary is proud to house 
the Del Norte County Hmong Cultural Center. Our school offers students an all-inclusive, standards based education that focuses on 
the “whole child”. Instructional practices at the school emphasize literacy and mathematics by utilizing engaging and innovative 
curriculum supported by engaging online learning opportunities. Bess Maxwell is very proud of the diversity of our school community 
and honors different cultures by observing many traditions throughout the school year. Bess Maxwell staff are dedicated professionals 
that continually demonstrate their kindness, compassion and ability to relate curriculum to the children's real life experiences. We are 
excited to be your school of choice in Del Norte County and thank you for entrusting us with the care and education of your student. 
 
Mission Statement: 
Bess Maxwell offers students an all-inclusive, standards based education that focuses on the “whole child”. Instructional practices at 
the school emphasize literacy and mathematics by utilizing engaging and innovative curriculum supported by engaging online learning 
opportunities. Bess Maxwell is very proud of the diversity of the students and honors different cultures by observing many traditions 
throughout the school year. 
 
School Vision: 
All students attending Bess Maxwell Elementary School will show steady growth in literacy and mathematics throughout each school 
year. 
 
We Believe: 

• All children are unique 
• Children learn in different ways 
• 
• 
• Children learn best in an environment that is supportive and encouraging 
• 
Education is a cooperative effort between home, school and community 

Enrichment is vital to the well-being of children 
Education is a student-centered process 

 

2017-18 School Accountability Report Card for Bess Maxwell Elementary School 

Page 2 of 11