A Message from the Principal 
 
The Mission of Tracy High School is to support and challenge all students in an alternative 
environment in which they will graduate prepared for post-secondary education and career 
opportunities. 
Tracy High School Students are challenged academically within a supportive environment in order 
to graduate prepared for post-secondary education and career opportunities. 
 
Student Learner Outcomes: Students will be able to . . . ENGAGE in collaboration in order to become 
active problem solvers who contribute to their community. EVALUATE the validity and 
effectiveness of a variety of sources in order to support critical thinking and create sustainable 
arguments. UTILIZE relevant information in order to be successful in all courses of study and 
establish future educational and vocational goals. 
 
Tracy High School, along with the other alternative programs in the ABC Unified School District, 
offers students several different ways to make up course credit, graduate and receive a high school 
diploma. There are numerous reasons why students fall behind in school. Family problems, health 
problems, and attendance problems are just a few of those reasons. But, whatever the reason, the 
fact is that failed classes earn no credit and a student must make up those credits in equivalent 
courses in order to graduate. This is the reason every school district in California needs a school 
dedicated to helping students regain their academic position after they have experienced problems 
and setbacks during their tenure in high school. That is the reason districts are required to have 
alternative programs like Tracy (continuation) High School. The ABC Unified School District 
community is fortunate to have one of the best alternative high schools in the state here at Tracy 
High School. 
 
Tracy High School offers students who display good attendance and work habits the chance to earn 
credit at an accelerated pace. Students who come to Tracy behind in academic credits have the 
opportunity to take extra classes, receive work experience credits and earn credits outside the 
classroom (community college, and Adult School). Students dedicated to getting caught up have 
the chance to transfer back to their home schools before graduation or to graduate on time from 
Tracy High School's accredited program. 
 
The Tracy High School faculty, staff, and administration look forward to working with every ABCUSD 
student and parent to create the best educational experience possible. 
 
Tracy (Continuation) High School is located in the city of Cerritos and serves approximately 450 
students over the course of the school year in grades ten through twelve from the cities of Cerritos, 
Artesia and Hawaiian Gardens, as well as portions of the cities of Norwalk and Lakewood. Tracy 
High School is on a traditional school calendar and is able to provide credit recovery for students in 
jeopardy of not graduating and/or dropping out of school. Tracy is dedicated to ensuring the 
academic success of every student while providing a safe and comprehensive educational 
experience. 
 

----

--

-- 

ABC Unified School District 

16700 Norwalk Blvd. 
Cerritos, CA 90703 

(562) 926-5566 
www.abcusd.us 

 

District Governing Board 

Ernie Nishii, President 

Dr.Olga Rios, Vice President 

Sophia Tse, Clerk 

Christopher Apodaca, Board 

Member 

Leticia Mendoza, Board Member 

Maynard Law, Board Member 

Soo Yoo, Board Member 

Leticia Mendoza, Board Member 

 

District Administration 

Dr. Mary Sieu 
Superintendent 

Dr. Valencia Mayfield 

Assistant Superintendent, 

Academic Services 

 

Toan Nguyen 

Assistant Superintendent, 

Business Services 

Chief Financial Officer 

 

Dr.Gina Zietlow 

Assistant Superintendent, 

Human Resources 

 

 

2017-18 School Accountability Report Card for ABC Secondary School 

Page 1 of 10 

 

The Tracy High School community is one where the highest expectations of a student's educational experiences are maintained. Tracy 
utilizes all available resources to enable students to be life-long learners who possess the ability to achieve to their utmost potential. We 
promote a safe, nurturing, and stimulating environment that invites students to actively participate in the educational opportunities made 
available. Tracy provides a welcoming atmosphere in which parents and community members are encouraged to involve themselves in 
their students' educational endeavors. Individual, academic and social needs of students are met during the time they are enrolled, 
creating a desirable environment where all participants are successful.