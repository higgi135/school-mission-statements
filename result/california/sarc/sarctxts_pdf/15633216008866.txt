The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters. 
 
Casa Loma's Vision is for Casa Loma to become the best elementary school in Kern County where all Comets dream big, and aim for 
the stars to succeed in college and beyond. 
 
Our mission is: We partner will all staff, students, families, and the community to ensure all Comets flourish in safe, positive, and 
innovative learning environment.