PRINCIPAL'S MESSAGE 
I'd like to welcome you to Cypress School of the Arts' Accountability Report Card (SARC). In accordance with Proposition 98, every 
school in California is required to issue an annual SARC that fulfills state and federal disclosure requirements. Enclosed you will find 
information about our academic focus, student population, parent involvement, student achievement, professional staff, curricular 
and extra-curricular programs, safety procedures, school environment, and condition of facilities. 
 
Cypress School of the Arts is focused on preparing our students for success. We provide instruction aligned to the California Content 
Standards with an emphasis on critical thinking and problem solving and a themed focus on music, and art instruction. To develop the 
skills necessary for students to make the transition from elementary school to secondary school, we provide explicit instruction in 
organization and study skills using AVID strategies beginning in 4th grade. With ongoing evaluation of our students and programs, we 
strive to meet the individual needs of all. 
 
In the 2009-2010 school year, Cypress received the California Distinguished School Award. In 2010-2011, Cypress received the 
California Academic Achievement Award and the California Business for Education Excellence Awards. 
 
We are committed to providing the best educational program in a caring and safe environment for all of our students. We welcome 
any questions about the information provided in this report or any suggestions to improve our programs make a difference for all of 
our children. 
 
Cypress School of the Arts is a unique school in the Hesperia Unified School District serving as a K-6 parent choice school in the district. 
It is the intent of the Cypress staff to provide an effective academic and social transition from elementary school to the secondary 
level for all of our students. The school environment will be one to provide the nurturing support unique to the elementary level while 
incrementally providing the development of independence organizational skills, critical thinking, and student interest. This will be 
evident in the structure of the school, as well as the instruction itself. 
 
SCHOOL MISSION STATEMENT: “Creating a passion for learning and cultivating the artistic potential in all students” 
 
SCHOOL PROFILE 
Hesperia Unified School District is located in the high desert region of San Bernardino County, approximately 40 miles north of the 
Ontario/San Bernardino valley. More than 20,000 students in grades kindergarten through twelve receive a rigorous, standards-based 
curriculum from dedicated and highly qualified professionals. The district is comprised of 15 elementary schools, 3 of those being 
parent choice schools, 3 middle schools, 3 comprehensive high schools, 1 alternative school, 2 continuation highs schools, 1 
community day school, and 6 charter schools. 
 
Cypress School of the Arts is located in the northwest region of Hesperia and serves students in grades kindergarten through six. At 
the beginning of the 2018-19 school year, 839 students were enrolled. 
 
 

2017-18 School Accountability Report Card for Cypress School of the Arts 

Page 2 of 12