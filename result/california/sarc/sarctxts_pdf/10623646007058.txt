District Vision 
the Board of Trustees of Parlier Unified School District, in partnership with our community, believe that every student can reach their 
maximum potential. 
 
District Mission 
We are committed to: 

 

• Promoting Self Discipline and Respect 
• Challenging and Supporting Educational Success 
• 

Inspiring each student to be Life Long Learner 

School Description 
Chavez Elementary is located in the city of Parlier in Fresno County. Parlier is a place rich with cultural heritage and has a special 
respect for its roots and celebrates its primary culture and language daily. Chavez Elementary is the only school in Parlier Unified that 
has a Bilingual Immersion Program. This alternative program is a Dual Immersion which is designed for English Leaners and Fluent 
English Proficient students. Instruction is provided in two languages in a balanced manner, and is designed to ensure that both groups 
learn to to read, write and speak English and Spanish. 
 
Chavez Elementary serves students in pre-school through sixth grade.This also, includes two SDC classes. In order to achieve mastery 
of the adopted curriculum standards in reading writing, mathematics, social studies, science, and physical fitness, our school 
expectations are for students to achieve to their highest level with the assistance of staff and parents. In addition to the academics at 
our site, we have an After School Program. Some our extra-curricular activities include cultural celebrations,Sports activities, Math 
Club and Music classes for grade 4th -6th grade. 
 
The staff is committed in providing a safe environment for our students. We maintain a safe environment for our students by having 
school policies and practices that ensures that we have adequate supervision before, during and after school. We enforce the policies 
set by our Governing Board and all California Educational Codes to contribute to the best possible working and learning environment. 
 
Chavez Elementary utilizes Positive Behavior Interventions and Supports (PBIS) and TIME to Teach. Positive Behavior Interventions 
and Supports (PBIS) is a process for creating school environments that are more predictable and effective for achieving academic and 
social goals. A key strategy of the PBIS process is prevention. In conjunction with TIME To Teach, our students are provided with Teach-
To's at the beginning of the school year and reviewed throughout the school year to teach the school's expectations, and 
acknowledged for their positive behavior. through instruction , comprehension and regular practice, all stakeholders use a consistent 
set of behavior expectations and rules. When some students do not respond to teaching of the behavioral rules, we must re-teach. 
These practices are creating a school community where all students, parents, and staff have a safe learning environment and ensuring 
student achievement. 
 
With the implementation of the Common Core State Standards, our teachers will focus on teaching the necessary skills that students 
will need for College and Career Readiness. Mathematics and Reading comprehension and writing strategies, will be the emphasis. 
Teachers continue to provide our students with the skills to become proficient writers and the use of strategies that engage all 
students. 
 
Our students in grades 3-6th have been utilizing their IPADs in the classroom to conduct research, learn keyboarding skills, and take 
notes from lectures and videos. All students in K-6 have access to IPADs to use for skills review through IXL and literacy skills through 
AR.The use of technology will continue to be the focus through out the school day. It is our goal that all students are proficient in 
computer literacy skills and technology tools to enhance classroom instruction. 

2017-18 School Accountability Report Card for Cesar E. Chavez Elementary 

Page 2 of 10 

 

 
It is a fact that all students benefit when they come to school everyday and arrive on time. Daily attendance increases academic 
achievement. This also teaches students work habits that are necessary for their education and work careers. It is essential that 
students attend daily to ensure each child receives access to the educational program provided by our highly qualified staff. We work 
with students and parents and emphasizing the benefits of perfect attendance.