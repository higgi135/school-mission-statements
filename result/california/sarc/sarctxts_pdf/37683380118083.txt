At Innovations Academy (IA) charter school, we powerfully create our lives through self-expression, compassionate connection, and 
purposeful learning. Our constructivist-based K-8 school teaches through student-centered activities and inquiry focused learning 
which includes projects, performance and other interactive learning. We utilize a relationship based discipline program for classroom 
and campus behavior management and skill building. 
 
At Innovations Academy, children learn in an environment that respects the intellectual, emotional, and social intelligence of all 
children. We use a multidimensional curriculum to support the innate human desire to learn. 
 
At least 80% of our population attends school in a progressive five day structure. Additionally, for home schooling families, Innovations 
Academy offers a 3-day program for grades K-2, and a 2-day program, known as the Home Learner Community, for grades K-8. Please 
visit our website for additional information about these programs.