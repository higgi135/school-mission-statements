Bonnie Oehl Elementary School has 25 classrooms, a multipurpose room, a computer lab, and an administration office. The campus 
was built in 1968 and modernized in 2009. The facility and staff strongly support teaching and learning with ample classroom and 
playground space, and a staff resource room. The school curriculum also supports the 90/10 Dual Immersion Program. 
 
Bonnie Oehl Elementary fosters a strong school community where educators, administrators, and families form partnerships that 
promote student learning. Our diverse educational programs emphasize our mission to promote: 

Life-long learning 
College and career readiness 

 High expectations for all 
 
 
 Bi-literacy proficiency 
21st century skills 
 
Student motivation 
 
 A safe environment