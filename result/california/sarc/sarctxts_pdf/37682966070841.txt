High expectations and strong support for student learning shape the culture that defines the character of Los Peñasquitos Elementary, 
where student success is the only option. As evidenced in the signs that dangle from our halls, our students and staff believe every 
student will learn, whatever it takes. Los Peñasquitos School, in San Diego, California, is one of twenty-five elementary schools in the 
Poway Unified School District, and is proud to be celebrating over 40 years of enriching student lives. It is located in Rancho 
Peñasquitos, a community in northern San Diego County. We serve 525 students from Pre-kindergarten to fifth grade. Our classes 
include an extended-day student academy for some of our fourth and fifth grade classes. The students of Los Peñasquitos represent 
a cross-section of our community. Forty-two percent of students receive free or reduced-price lunch, twenty-six percent of our children 
are English Language Learners, and there are thirty-five different home languages at our school. At the same time, an ample 
percentage of the children come from middle and upper-middle class families. Economic and cultural diversity are strengths and we 
believe that our students benefit greatly from interaction with other students who share differences with them. The majority of our 
parents, including parents of second language students, are active in many aspects of school life. They serve on the School Site Council, 
help to organize events that celebrate the many cultures represented at our school, are members of the PTA, and are very involved in 
our volunteer organization. Los Peñasquitos values the diversity of our students and their families, and families in turn appreciate 
efforts made by our school to support their children. Every decision we make is driven by our belief that all students will learn, 
whatever it takes. We accept no excuses for poor academic achievement, and are therefore committed to helping every child meet 
the high expectations represented by our district's academic standards. The community is proud not only that Los Peñasquitos' 
students achieve well above national averages, but also that standardized and site-developed test scores reflect continuing and 
significant gains in student achievement. By most measures, student performance has shown consistent and impressive improvement. 
On the State of California's previous assessment system we achieved a statewide 10-10 ranking 9 times, which means we ranked in 
the top 10 percent of all schools and also in the top 10 percent of schools with similar demographics. In the most recent administration 
of the current state assessments, the Smarter Balanced Summative Tests, eighty-one percent of our students met or exceeded the 
standard in English Language Arts and seventy-six percent met or exceeded the standard in Math. On the California Accountability 
System Dashboard, Los Pen students are in the blue part of the dial, 75.4 points above the standard on English Language Arts. Also 
Los Peñasquitos students are 46.5 points above the standard in Math and in the blue part of the dial. Los Pen has been recognized as 
a California Distinguished School, a National Blue Ribbon School, and a Gold Ribbon School. Our continued growth as a professional 
learning community was featured in a nationally recognized book titled “Whatever it Takes” by Dr. Rick DuFour. These awards further 
reflect the commitment to academic excellence that is shared by our students, staff, and parents.