School Mission: The El Marino Language School staff and community are committed to the principle that all students can become 
functionally bilingual and bi literate during the elementary school years, while participating in a balanced educational program which 
promotes academic, social, physical and psychological growth. The school will foster and support creativity, inquiry, intrinsic rewards, 
individuality, self-discipline, a sense of personal worth, mutual respect, and an appreciation of different cultures 
 
Program Goals: At EMLS, students will gain: 
 
1. Language proficiency in both English and the target language: Spanish or Japanese 
 
2. Academic Achievement in both English and the target language 
 
3. Positive self-esteem, cultural awareness and sensitivity 
 
School Description: El Marino Language School (EMLS) is one of five elementary schools in the Culver City Unified School District . 
Every student attending the school is enrolled in one of the school’s two language immersion programs, the Spanish Immersion 
Program (SIP), founded in 1971, or the Japanese Immersion Program (JIP), founded in 1992. In both programs, students learn the 
District curriculum as in other CCUSD schools, but most of the instruction is conducted in the target language, Spanish or Japanese. 
Currently there are 22 classrooms in SIP and 12 classrooms in JIP. El Marino also serves as a site for the CCUSD Transitional Kindergarten 
program. EMLS serves 842 students: of these 24 participate in the TK class, which is an English program, 523 participate in the SIP and 
264 participate in the JIP. El Marino draws its students from the entire district attendance area; the school is ethnically diverse. About 
one third of the students speak a language other than English at home. While English is the primary language of most of our students, 
we have a wide array of home languages, though Spanish and Japanese are the dominant ones. English Learners comprise 22.2% of 
our student population; when factoring in the former English Learners who are now Reclassified as Fluent English Proficient, the 
percentage increases to 31% of our student population. El Marino is made up of a positive cultural diversity, which is one of it s most 
distinguishing features. El Marino has been named in the past as a National Blue Ribbon School and a California Distinguished School, 
and has also been recognized for excellence in Arts Education. Parents, students, staff and community members work together to 
contribute to the school's success.