The intent of the School Accountability Report Card is to communicate information to you about our students, our staff, our programs, 
and the progress students are making. Irwin High School began operation in the 1990-1991 school year with the purpose of providing 
high school age students who have fallen behind in credits the opportunity to accrue enough credits to re-enter the traditional high 
school or complete the requirements for a diploma from Irwin High School. 
 
The mission of Irwin High School is to provide a supportive and personalized learning program so students can attain a high school 
diploma and experience success in college, a career, and become contributing members of society.