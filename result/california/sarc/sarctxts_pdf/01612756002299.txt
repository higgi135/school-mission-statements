The Havens School community is located in Piedmont, a city of about 11,000 residents, nestled in 
the hills above the San Francisco Bay Area. The original school opened in 1901 and has continuously 
been modernized and renovated to accommodate the growing student population. 
 
The faculty of Havens Elementary School is a team of professionals, firm in their resolve to bring a 
world class education to our students. Classroom teachers, teacher specialists in music, art, library, 
reading, math, science, technology and special education, collaborate to integrate curriculum in 
order to best meet the academic, social and emotional needs of our students. We strive to cultivate 
critical thinking, social responsibility and collaboration in students and faculty alike. 
 
Our support staff of custodians, office staff, para educators and administrators work with students, 
families and staff to maintain a positive learning environment for all students so that they can 
realize their potential. 
 
We redefined our mission and values in Fall 2018: 

Everyone at Havens Belongs. 

• 
• We are Caring. 
• We are Courageous. 
• We have Integrity 

 

Piedmont City Unified School 

 

District 

760 Magnolia Ave. 
Piedmont, CA 94611 

(510) 594-2600 

http://www.piedmont.k12.ca.us/ 

 

District Governing Board 

Amal Smith 

Cory Smegal 

Sarah Pearson 

Megan Pillsbury 

Andrea Swenson 

 

District Administration 

Randall Booker 
Superintendent 

Cheryl Wozniak 

Asst Superintendent, Ed Services 

Ruth Alahydoian 

Chief Financial Officer 

Stephanie Griffin 

Director of Instructional 

Technology 

Hillary Crissinger 

Director of Special Education 

Pete Palmer 

Director of Facilities 

 

2017-18 School Accountability Report Card for Havens Elementary 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

16-17 17-18 18-19 

Havens Elementary 

With Full Credential 

Without Full Credential 

40 

0 

35 

0 

1.6 

29 

0 

0 

Teaching Outside Subject Area of Competence 

2.9 

Piedmont City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

172 

6 

1 

Teacher Misassignments and Vacant Teacher Positions at this School 

Havens Elementary 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.