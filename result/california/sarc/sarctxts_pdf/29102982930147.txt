The mission of John Muir Charter Schools is to meet the unique educational, social and emotional needs of our diverse student 
population in safe and nurturing environments that fosters personal, professional and academic growth. Through collaboration with 
our partner agencies and relevant, rigorous instruction toward a high school diploma, students gain the skills to achieve lifelong, 
sustainable employment and become proactive members of their communities.