Herbert Hoover Elementary School is a K-5 choice program. It is funded in the same manner as other elementary schools in PAUSD 
and enjoys the same resources as other elementary schools, e.g.: P.E., music instruction, special education services, and enrichment 
opportunities. The school has three self-contained classrooms at each grade level, K-5. 
 
The school program includes the full range of curriculum for elementary schools in PAUSD with special emphasis on uninterrupted 
instructional time where students receive a balance of explicit direct instruction delivered through mini-lessons coupled with 
structured, student-centered learning activities including the workshop model for reading, writing, and math that increase the depth 
of student learning and the complexity of their thinking. A block schedule allows grade levels to teach the same academic subject at 
the same time in order to accommodate student needs through a system of flexible grouping. Student success is fostered through the 
clear communication of high academic and behavioral expectations combined with differentiated support, the routine practice of 
effective communication skills and study habits, and the provision of varied social and emotional learning experiences in a safe, orderly 
environment. 
 
The regular classroom/PAUSD program is supplemented with support from primary and intermediate Instructional Assistants who 
hold a degree or better in the areas of language arts, math, and/or science. Hoover also hosts a resident art teacher who provides a 
minimum of 45 minutes of art per class on a weekly basis. SEL supports include counseling services through Acknowledge Alliance for 
all grades, Playworks which works with our students to create an inclusive, positive environment on our playground, weekly yoga with 
SEL elements in all classrooms, the ABC reading program through Project Cornerstone, and the school-wide implementation of 
Responsive Classroom strategies. Teachers facilitate data-guided instruction in a classroom environment which utilizes best practices 
and addresses the needs of all children. Homework is assigned at grade levels 1-5 in accordance with district homework policies. 
Academic and social behavior expectations for children are high, with a tiered system of support conducive to student learning, 
growth, and the development of personal responsibility consistent with these high standards. 
 
Kindergarten is a parent participation program at Hoover. In first through fifth grade, parents participate in a wide variety of volunteer 
opportunities outside of the classroom during the academic day in order to promote student independence and deeper connection 
with peers, teachers, and Instructional Assistants. 
 
Hoover serves families from the communities of Palo Alto, Los Altos Hills, Stanford, and East Palo Alto. Attendance rates are high. 
 
In first through fifth grades, student progress is reported via bi-monthly reports/updates, and trimester report cards. These 
comprehensive reports summarize academic and social-emotional learning progress, including completed homework and areas 
requiring attention. Parent communications are strongly emphasized to assure students’ optimum learning and development. 
 
Admission to the school is open to all Palo Alto residents. Applications for inclusion in an annual lottery are available in the school 
office. Parents and other members of the community may visit classrooms during open house sessions held three times annually. 
 
We expect staff and parent commitment to and support of Hoover’s Guiding Principles: 
 
Academics: 
We provide a collaborative, teacher-facilitated, student-centered instructional program, based on Common Core, state and district-
mandated standards. We emphasize the importance of the social and emotional growth of each child. We emphasize the importance 
of uninterrupted instructional time, which enables students to build a solid foundation for academic success, while providing regular 
opportunities for students to increase the depth of their learning and the complexity of their thinking. 
We promote curiosity, questioning, critical thinking, and problem solving skills. 
We maintain high expectations and utilize proven best practices in order to maximize each student's potential and facilitate the 
achievement of academic excellence. 

2017-18 School Accountability Report Card for Herbert Hoover Elementary School 

Page 2 of 13 

 

We emphasize individual accountability as a key ingredient of a student's academic achievement. 
We promote the development of autonomous learners: students who have good communication skills and study habits, are organized, 
and work independently. 
 
School Environment: 
We provide a safe, welcoming environment with clear academic and behavioral expectations coupled with differentiated support. 
We adhere to a school-wide language based on making safe, respectful, responsible choices. 
We are a Project Cornerstone school and have fully implemented the ABC Reading Program. 
We have adopted the Responsive Classroom approach to learning on a school-wide basis and continue to utilize conflict resolution 
strategies. 
We have yoga in every classroom with social skills embedded in each lesson (empathy, mindfulness, problem solving, stamina, growth 
mindset, etc...) 
We utilize the Social Thinking curriculum by Michelle Garcia-Winner in primary grades. 
We provide resilience-building support through the Acknowledge Alliance program, Project Resilience. 
We expect every staff member to be responsible for the safety and learning of every child in the school. 
We expect parental involvement in each child’s personal and academic growth. 
We expect cooperation, communication, and respect among all members of our community.