The mission of Prestwood Elementary School is to guide children in becoming educated, compassionate, and socially responsible 
participants in their local and global communities. 
 
Prestwood will achieve this mission through four main areas: academics, life skills, community, and environment. 
Academics: Students will engage in purposeful, relevant, real-world projects rooted in inquiry-based learning. 
Life Skills: Students will demonstrate safety, develop responsibility, and show respect for themselves and others. (empathy) 
Community: Students will participate in a learning community that fosters a sense of pride, inclusion, and connection based on 
interactions of mutual respect among all members of the community. 
Environment: We will build a safe and positive environment that differentiates for all students, teaches environmental stewardship, 
and promotes joyfulness. 
 
As the Prestwood staff, we implement all adopted curriculum with integrity and provide equitable access for all students. 
As the Prestwood staff, we cultivate positivity and joy through a love of learning with excitement, enthusiasm, and celebration. 
As the Prestwood staff, we build relationships with students and create a comfortable and safe learning environment for all. 
As the Prestwood staff, we model and promote high expectations for ourselves and our students in all we do.