Mission Elementary is a student-centered school, our goal is to make learning accessible to all. We 
have an excellent school-wide reading program that supports all students, especially English 
learners and at risk students. Students learn reading skills in small groups through high interest 
leveled readers The school has a library of over 1,500 sets of developmentally appropriate leveled 
books. Students who attend Mission learn grade level standards through integrated use of 
technology and Project-Based learning. Teachers at Mission help students develop 21st Century 
Skills by fostering critical thinking, problem solving, and creativity in all subjects. In addition, 
students learn to work in collaboration and to communicate effectively with their peers and 
teachers using technology to expand their learning. 
 
At Mission, staff, families, and community members collaborate to ensure a safe learning 
environment and maximize the academic potential of each student. We promote individual 
responsibility and respect for others through character education, The 8 Keys of Excellence, and 
our four school-wide rules. Clear established procedures are taught and reinforced on an ongoing 
basis. Students are recognized in a variety of ways for their good behavior and decision-making. 
Mission school promotes a safe and healthy way of life; fitness training and exercise take place daily 
during PE and recess. Educational field trips and assemblies help strengthen real-world concepts, 
allowing students to make new connections to the curriculum. 
 
The entire teaching staff is committed to student success. Mission teachers meet each week to plan 
lessons, create assessments, develop differentiated (customized) instruction for students, and 
coordinate engaging learning activities. The staff’s professionalism, collaboration, and focus on 
student achievement makes Mission Elementary a place where all students achieve their full 
potential. 
 
Mission Elementary was modernized in 2013. 
 

2017-18 School Accountability Report Card for Mission Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Mission Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

29.00 32.80 

0.0 

0.0 

0.0 

 

 

 

 

Oceanside Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

24.32 

1 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Mission Elementary School 

16-17 

17-18 

18-19