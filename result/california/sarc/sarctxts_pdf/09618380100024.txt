Soaring Like Raptors 
Oak Meadow Elementary School is a TK-5 school nestled in the rolling, oak studded hills of El Dorado 
County. At Oak Meadow, cooperation, compassion, and celebration of diversity are encouraged in 
a safe and secure environment. In partnership with students, staff, families and communities, Oak 
Meadow’s mission is to provide meaningful educational experiences to encourage lifelong learning. 
We work together to support and develop each child intellectually, physically, socially and 
emotionally. Our school mascot, the raptor, reflects our desire to soar to new heights in learning 
each day. Oak Meadow is a National Blue Ribbon School and a California Distinguished School 
recognized for academic achievement. 
 
School Profile 
Oak Meadow Elementary School is one of seven elementary schools, including a Charter 
Montessori located on the Blue Oak campus and the Buckeye Union Mandarin Immersion Charter 
School at Oak Meadow, and two middle schools in the Buckeye Union School District. The district 
was established in 1858 and currently serves the communities of Shingle Springs, Cameron Park 
and El Dorado Hills in El Dorado County. During the 2017-2018 school year, 697 Transitional 
Kindergarten through fifth grade students were enrolled at the school. 
 

 

 

----

---- 

Buckeye Union Elementary 

School District 

5049 Robert J Mathews Parkway 

El Dorado Hills, CA 95762 

(530) 677-2261 

www.buckeyeusd.org 

 

District Governing Board 

Brenda Hansen-Smith 

Winston Pingrey 

Kirk Seal 

Gloria Silva 

Jon Yoffie 

 

District Administration 

David Roth, Ph. D. 
Superintendent 

David Roth, Ph.D. 
Superintendent 

Patty Randolph 

Director of Curriculum and 

Instruction 

Nicole Schraeder 

Director of Student Services 

 

2017-18 School Accountability Report Card for Oak Meadow Elementary School 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Oak Meadow Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

32 

27 

27 

0 

0 

1 

0 

0 

0 

Buckeye Union Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

219 

1 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Oak Meadow Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.