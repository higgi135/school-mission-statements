Oak Valley Mission Statement: Students are at the heart of Oak Valley, and meeting their needs is our first priority. 
 
 Oak Valley Middle School is in the City of San Diego, California, nestled within a beautifully master planned community of 4S Ranch. 
Our student population comes from a diverse socioeconomic background. The ethnic distribution is approximately 8% Hispanic, 9 % 
Indian, 12% Chinese, 2% African American, 8% Filipino, 46% Caucasian, and 15% other. 
 
 Oak Valley runs on a block schedule. Students attend four classes a day, not including lunch. Classes rotate on an odd/even schedule. 
Each Tuesday, students arrive at school later, giving staff the opportunity for 70 minutes of collaboration. The collaboration that takes 
places includes meetings among grade level teams as well as department teams. Our staff focuses on building positive relationships 
between teachers, students, parents, and community. Teachers have an opportunity to interact with students in a variety of ways that 
build a strong sense of community. Students who are not performing at grade level are supported through courses that meet on a 
regular basis. Music, Spanish, Art and Engineering are the electives currently being offered. 
 
 Two Student Services Specialists work to support student well-being in areas of alcohol and other drug use prevention, tobacco use 
prevention, assists teachers in presenting anti-bullying education, anger management, and conflict mediation. Our Falcon Center, a 
student recreation/game center, is a place where students can get together during lunch and other free time to relax, talk to friends, 
make new friends, read, do homework and play games. This type of facility fosters a greater sense of community among students and 
creates a relaxing atmosphere in which to bond. Studies show that when students feel a sense of connectedness to school they 
perform better academically. 
 
 Another exciting program we have at Oak Valley is the OVPL (Oak Valley Peer Leaders) program, which 8th period to work on strategies 
to support all students. OVPL students tutor, mentor, and assist peers with academic and social concerns. The OVPL students provide 
free student-to-student tutoring three days a week in the Library. As a group they organize and lead large group forums once a month 
to tackle topics like bullying and academic honesty. 
 
 The physical education department delivers curriculum, which include components that shift the physical education paradigm of 
teaching students sports to concentrating on student learning life-long health and fitness skills and concepts. The P.E. Program 
provides a Fitness Lab to teach students how their bodies work and show them the effects of exercise on their bodies. 
 
 Currently, Oak Valley has an English Language Learners Program. Students who are Intermediate and above in their English skills are 
placed in clusters in the general education classroom with support provided by an instructional assistant. 24 students with beginning 
English skills are in a self-contained classroom with a CLAD certified teacher for language arts, social studies and science in grade 6-8. 
All teachers at Oak Valley are fully certificated to teach ELL students. 
 
 To further support our goal of personal and intellectual development we have three AVID classes and we implement the use of AVID 
strategies school-wide. We are committed to college readiness for all of our students, modeling our belief through University Day 
every Friday. Staff members wear college alumni clothing. 
 
 The Oak Valley Middle School Library strives to provide a rich selection of resources that support state standards, diverse interests, 
and multiple reading levels and learning styles. Online resources available throughout the campus and students home include DESTINY 
(library catalog), World Book Encyclopedia (encyclopedia, atlases, web-links, Spanish language encyclopedia), Synagog Learning- 
Infatuate (newspapers, magazines, e-books, primary sources, literature databases and reference resources), Today's Science (science 
news written specifically for students), Noodle Tools (the most comprehensive and accurate MLA and APA-style bibliography composer 
on the WEB). The library has four computer stations with access to the resources and Microsoft suite (Word, Excel, and PowerPoint) 
and three computers with access to DESTINY (library catalog). At present, the OVMS library has approximately 13,658 books, 19 
magazine subscriptions, and a collection of DVDs and videos for the classrooms. 

2017-18 School Accountability Report Card for Oak Valley Middle School 

Page 2 of 11