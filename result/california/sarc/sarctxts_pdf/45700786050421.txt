North Cow Creek School (NCCS) is a small, rural school of approximately 255 students in grades K-8, which was first established in 
1882. Located in Palo Cedro, 9 miles east of Redding in beautiful Northern California, the campus includes a gymnasium, computer 
lab, full playground and athletic fields as well as classrooms. 
 
NCCS has a strong focus on academics, as demonstrated by its consistently high Academic scores. 
 
The school program is enhanced by sports, field trips, a middle school activity period, and classroom music. Students receive 
technology education through multiple weekly visits to the computer lab. Students in grades 4 - 8 are each assigned a chromebook or 
tablet for use within the classroom. Technology resources are enhanced through a 100 MB fiber line. 
 
North Cow Creek School also offers a district-run after-school day-care program. The school has been recognized as a California 
Distinguished School, a Governor’s Fitness Challenge award winner, and has received the California Superintendent’s Challenge Award 
i. 
 
The mission of the North Cow Creek School community (parents, students and staff) is to develop in all students the skills to maximize 
their academic potential, social abilities and personal fitness. Each student will demonstrate continuous progress using a variety of 
instructional approaches measured by both formal and informal assessments throughout the year. Students who need assistance will 
receive the benefits of available resources to reach their individual potential in all curricular areas. In partnership with the community, 
we will work to foster an environment of trust, respect and responsible citizenship