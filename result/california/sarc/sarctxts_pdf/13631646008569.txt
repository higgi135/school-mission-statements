The Frank Wright Middle School community is located in the city of Imperial, approximately 111 
miles east of San Diego and approximately 210 miles southeast of Los Angeles. 
 
Imperial Unified School District is the oldest district in the Imperial Valley. It was formed in 1902 
and produced its first graduating class in 1906. The district educates students in grades 
kindergarten through twelve on a traditional calendar system. There are currently two elementary 
schools, one middle school, and one high school in the district. Beginning the 2019-2020 school 
year an additional elementary school will open to accommodate the increase in enrollment due to 
the city of Imperial experiencing a housing and population boom over recent years. Imperial Unified 
School District is committed to providing a strong instructional program for all students to ensure 
excellence in education. This excellence can be seen in the staff who play such a vital role in 
providing a quality educational program. 
 
Frank Wright Middle School is located in the city of Imperial. The school opened its doors in 1957 
to grades seven and eight. During the 2017-18 school year, Frank Wright Middle School had 1034 
students enrolled in grades six through eight. Frank Wright Middle School received its fourth re-
designation this year as a California School to Watch. 
 
Frank Wright Middle School is also home to an award winning band program with a focus on 
community service and outstanding musicianship. Our drumline has participated in The Drumline 
Festival at South West High School during the 2016-17 and 2017-18 school years with high praise 
for their marching and musicianship. Additionally, our Advanced Band has participated in the 
Southern California School Band and Orchestra (SCSBOA) Imperial Valley Festival and received 
Unanimous Superior (2017) and Superior (2018) ratings. This is a prestigious honor that spotlights 
our students' willingness to go above and beyond as young musicians. 
 
More recently, in December 2018, we were awarded "Best Middle School Band" for our 
performance at the Naval Air Facility Christmas Parade. The Frank Wright Band Program has 
experienced an increase in enrollment over the last three years. This is due to our talented band 
director, supportive parents, and enthusiastic students. The students are encouraged to be 
collaborative in such partnered community events as the: Imperial Valley Music Educator's 
Association's (IVMEA) Solo and Ensemble Festival, Half Time Festival Performances at Central High 
School, and the Honor Band Concert at South West High School. Our students also attend 
competitive community events such as the El Centro Christmas Parade and the Imperial Lights 
Parade Performances. 
 
 

2017-18 School Accountability Report Card for Frank M. Wright Middle 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Frank M. Wright Middle 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

44 

42 

42 

1 

0 

1 

0 

1 

0 

Imperial Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

184 

3 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Frank M. Wright Middle 

16-17 

17-18 

18-19