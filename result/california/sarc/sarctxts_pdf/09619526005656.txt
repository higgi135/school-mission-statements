Louisiana Schnell School is one of three schools in the Placerville Union School District. Both Schnell and Sierra Schools serve 
Transitional Kindergarten-5 grade students; Edwin Markham Middle School serves all 6-8th grade students. Our school is named after 
Miss Louisiana “Pete” Schnell, formerly a teacher and administrator in the district. 
 
Schnell School’s mission is: 
 
Students First! - Preparing all students for future success through quality instruction today.