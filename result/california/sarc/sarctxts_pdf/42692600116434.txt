The Orcutt Academy Charter School is a grade K-12 dependent charter school. The high school (grades 9-12) is situated in the semi-
rural community of Orcutt, California, an unincorporated area immediately south of Santa Maria, California. The K-8 is located in the 
town of Los Alamos, approximately thirteen miles from the high school. The Orcutt Academy is fully accredited by the Western 
Association of Schools and Colleges (WASC). 
 
The Orcutt Academy is chartered and operated by the Board of Trustees of the Orcutt Union School District, allowing the students and 
staff access to the district’s powerful educational resources. Students take the same statewide assessments and the school is subject 
to the same accountability measures as other public schools and districts. The school opened its doors in August of 2008. 
 
Mission Statement: 
The mission of the Orcutt Academy is to create a learning community that values the application of academic learning in the “real 
world” while promoting intercultural understanding and respect for others. The creation of new knowledge is encouraged and 
expected, thereby equipping graduates for successful academic and workforce experiences . . . as lifelong learners. 
 
The Orcutt Academy’s Expected School-wide Learning Results (ESLRs): 
 
Orcutt Academy students will: 

• Demonstrate good citizenship through personal integrity, responsibility, and community service. 
• Demonstrate progress toward achieving California’s state standards in all academic areas. 
• 
• Acquire and use the technical and critical thinking skills that enable lifelong learning. 
• Demonstrate intercultural and global understanding through individual and school-wide projects. 

Engage in active learning and apply academic knowledge in real life situations.