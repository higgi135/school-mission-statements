The Canyon Middle School community has earned the distinction of being honored as both a Gold 
Ribbon School and a National School to Watch. Here at Canyon, our Condors SOAR in their 
commitment to Safety, Organization, Achieving goals, and Respect. Canyon's expansive and 
attractive campus supports a wide variety of academic, athletic, and extracurricular activities for 
our students in grades 6-8, in addition to serving as a community venue for many local events. With 
our continued focus on providing multi-tiered systems of support, staff are committed to delivering 
academic rigor, fostering positive relationships to support social-emotional learning, and 
cultivating independent learners who understand the relevance of learning in a 21st century world. 
Teachers and support staff are dedicated to meeting and exceeding state and district standards in 
all areas of instruction and provide appropriate interventions to assist students not meeting 
standards. 
 
Central to the Canyon Middle School culture is the commitment to supporting a successful 
transition from elementary to secondary school. This is supported through Positive Behavioral 
Supports and Interventions (PBIS), student mentorship and leadership programs, counseling 
supports, exploratory and elective options, and varied opportunities for school connection through 
clubs, music, and athletic programs. Students are provided a safe and culturally responsive learning 
environment that focuses on meeting the needs of adolescent learners living in our diverse and 
dynamic society. We regularly seek out family engagement in the educational process and view 
our school as a genuine team effort to support, teach, and guide students on their middle school 
journey. Canyon is pleased to offer Response to Intervention (RtI) including academic supports and 
enrichment programs, in addition our social-emotional learning curriculum specifically addressing 
middle school challenges. Staff collaboration, parent outreach and education opportunities, and 
ongoing communication and dialogue continue to help us improve as we work together towards 
our vision of supporting all students. Together, we SOAR! 
 
Mission Statement 
 
Canyon Middle School is a community with high expectations where students become responsible 
citizens who innovate and problem solve. In this rigorous and flexible environment, our staff works 
together professionally to support and respond to the needs of today’s middle school students. 
 

2017-18 School Accountability Report Card for Canyon Middle School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Canyon Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

65 

66 

64 

1 

0 

0 

0 

0 

0 

Castro Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

443 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Canyon Middle School 

16-17 

17-18 

18-19