Principal's Message 
The staff of Needles High School is committed to providing a learning environment which will enable all students to reach their full 
potential. Special incentive programs are in place to promote academic achievement and social skills. Our students are encouraged to 
reach their full potential supported by an environment that promotes strong character and high academic achievement. School wide 
norms guide everyone's behavior on campus including adults. They include respecting each other, being positive, listening to 
understand, being committed to high standards, and keeping timelines. 
 
Our staff is committed to raising student achievement and we have implemented Professional Learning Communities where the 
teachers collaborate, develop common assessments, pacing guides, and have common planning time. This gives each department 
time to meet, and meet the needs of every student. 
 
AVID is fully implemented at Needles High School and "Rachel's Challenge" supports a kind, caring school community. As Principal, I 
am committed to raising student achievement through meeting with the teachers during their Professional Learning Community time, 
meeting with departments, and meeting with each staff member after each benchmark assessment to see how the teacher could 
better implement their teaching strategies. 
 
The mission of the Needles Unified School District is to provide a free and appropriate education enabling all students to be successfully 
prepared to be productive members of society. Our core values reflect that "All Students Will Learn" and "Education First"