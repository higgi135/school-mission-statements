March Mountain High School is a unique Alternative Education setting. March Mountain High 
School's vision is to focus on developing the whole student by providing a nurturing environment 
and emphasizing transferable skills leading to academic success and a productive, responsible 
citizen. March Mountain High School's mission is to empower students to graduate with the skills 
necessary to successfully compete in a globally connected and diverse world. 
 
The school motto, BELIEVING IS ACHIEVING, is a vivid expression of the sense of vision felt by the 
March Mountain staff, students, and parents. We believe our students are in the process of 
succeeding academically, preparing for a career, and developing a sense of self-worth. 
 
March Mountain High School is a continuation high school that houses a variety of programs 
specifically designed to meet the particular needs of students who have been unsuccessful at the 
comprehensive high school. March Mountain has approximately 300 students who attend classes 
from 8:00 A.M. to 3:00 P.M. and work at an accelerated pace with academic rigor to assist with 
credit recovery. To further assist students with credit recovery, concurrent enrollment in Adult 
Education is available for seniors on a limited basis. In addition, students may enroll in credit 
recovery classes, CTE/ROP or work experience classes to earn credits. Concurrent enrollment with 
Moreno Valley College (MVC) is available. The Cal-SAFE program provides parenting/Child 
Development classes for students with children or for those who will be having a child. The program 
also provides school-time childcare for the babies. Special Education programs serving March 
Mountain students include the Resource Specialist Program (RSP) and English Learner (EL) services 
are also available. Due to the type of programs offered, students need to score a two or better on 
the English Language Proficiency Assessments for California Test (ELPAC) in order to be successful. 
 
In order for a student to receive a high school diploma at MMHS, he/she must complete 225 credits 
in required subject areas and be accountable for all State and District requirements including one 
year of Integrated Math or Algebra 1. At MMHS students are able to earn up to 90 credits a year 
due to the trimester system and accelerated six-week terms. 
 
 
 
 
 

----
---- 
Moreno Valley Unified School 

District 

25634 Alessandro Blvd 

Moreno Valley, CA 92553 

(951) 571-7500 
www.mvusd.net 

 

District Governing Board 

Susan Smith, President 

Jesus M. Holguin, Vice-President 

Cleveland Johnson, Clerk 

Gary E. Baugh. Ed.S., Member 

 

District Administration 

Martinrex Kedziora, Ed.D. 

Superintendent 

Maribel Mattox 

Chief Academic Officer, 

Educational Services 

Tina Daigneault 

Chief Business Official, Business 

Services 

Robert J. Verdi, Ed.D. 

Chief Human Resources Officer, 

Human Resources 

 
 

 

2017-18 School Accountability Report Card for March Mountain High School 

Page 1 of 15 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

March Mountain High School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16 

15 

21 

0 

0 

0 

0 

0 

0 

Moreno Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

March Mountain High School 

16-17 

17-18 

18-19