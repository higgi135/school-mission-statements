Message From Principal 
At R. Paul Krey Elementary School students, parents, staff and community work together to ensure 
that each student receives a rigorous, comprehensive, balanced and integrated educational 
program in a safe and caring environment. All children have the opportunity to develop their ability 
to think critically, solve problems, communicate effectively, work 
independently and 
collaboratively, take risks, make decisions, be creative and help others. With students as our 
priority, the staff will reflect on our teaching practices, share ideas and concerns honestly, 
communicate regularly with parents and the community, and model a lifelong joy of learning 
through our own personal and professional growth. 
 
Welcome to Krey Elementary, 
Brian Jones, Principal 
Brie Estep, Vice Principal 
 
School Mission Statement 
Members of the R. Paul Krey School community believe students learn best in a safe, comfortable 
place where they feel supported and inspired, are not afraid to take risks, and are empowered to 
be a part of their own education. Student attitudes and behaviors will be shaped by our Character 
Education program and the CASH matrix. Our character education program is enhanced by various 
character traits modeled and taught in both the classroom and in assemblies. The staff believes in 
a preventative rather than punitive discipline plan built on the underpinnings of well-taught, 
rehearsed routines and procedures, child-centered instruction that is both multi-modal, and 
developmentally appropriate. Our C.A.S.H. matrix (Come Prepared, Act Respectfully, put Safety 
First, are Hard Workers) teaches students our behavior expectations. Both parents and teachers 
believe the most valuable courses in the curriculum are those that impart students with the 
necessary academic knowledge yet at the same time offer students an opportunity for self-
awareness, creativity, tolerance, and responsibility. Site-based leadership that includes parents, 
teachers, and administration reflect the decision-making process and goal orientation of R. Paul 
Krey Elementary School. 
 
District Mission Statement 
The Brentwood Union School District will provide an exemplary education to all children in a safe, 
student-centered environment designed to nurture the whole child and partner with families to 
prepare globally productive citizens. 
 

----
---- 
Brentwood Union Elementary 

School District 
255 Guthrie Lane 

Brentwood, CA 94513-1610 

(925) 513-6300 

www.brentwood.k12.ca.us 

 

District Governing Board 

Carlos Sanabria 

Emil Geddes 

Jim Cushing 

Steve Gursky 

Scott Dudek 

 

District Administration 

Dr. Dana Eaton 
Superintendent 

Robin Schmitt 

Chief Business Official 

Roxanne Jablonski-Liu 

Assistant Superintendent, Human 

Resources 

Michael Bowen 

Director - Curriculum & Instruction 

Robert Brown 

Director - Maintenance & 

Operations 
Assistant 

 

Chris Calabrese 

Director - Student Services 

Jeff Weiss 

Director - Special Education 

 

2017-18 School Accountability Report Card for R. Paul Krey Elementary School 

Page 1 of 8