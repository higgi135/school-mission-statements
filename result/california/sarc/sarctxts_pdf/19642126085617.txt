A Message from the Principal 
Hello and welcome to Joe A. Gonsalves Elementary School- The staff here at Gonsalves is dedicated 
to providing the best educational experience possible for our students. This is done through a team 
effort where all stakeholders- staff, parents and families, students and community members- work 
collaboratively to create an environment conducive to learning and student achievement. We are 
successful because we provide a rigorous academic program for our students in a safe, nurturing 
and supportive atmosphere. Every student is important and we believe every student can succeed. 
It is this combination of high quality teaching in the classroom, the commitment of our families to 
promote and support learning, and desire of our students to do their very best that makes 
Gonsalves a special place to be. 
 
Through implementing a standards-based curriculum, use of effective instructional strategies, 
grade-level collaboration, data analysis and strategic interventions, while building positive, self-
enhancing and responsible student behaviors, we know that our students will continue to achieve 
and be successful academically. Our school has received many academic awards including being 
named a National Blue Ribbon School, a California Distinguished School, and a California Business 
for Education Excellence Honor Roll School. With the efforts of our staff, students and parents, the 
school experience is enhanced through special activities, assemblies, events and programs that 
help complete a well-rounded education. Students, staff and parents show our school spirit with 
weekly Spirit Day assemblies where we emphasize our Character Trait of the month. Our Gonsalves 
Grizzlies show they are STAR students by following our 4 school rules- Stay Safe, Take Responsibility, 
Act Respectfully and Ready to Learn. At our annual Gonsalves Day event at the end of the year, we 
recognize our past history as well as celebrate the diverse cultures that exist at our school. 
 
Located in Cerritos, California, within the ABC Unified School District, Gonsalves (ES) serves 645 
students grades TK-6 including 3 Special Day Autism classes. Our diverse student population is 
comprised of 63% Asian, 15% Hispanic, 7% White, 8% Filipino, and 5% African American. English 
Learners make up 20% of our student body. 
 Sixteen percent are Socio-Economically 
Disadvantaged; and 10% receive Special Education services. Our school was named after former 
mayor and assemblyman, Joe A. Gonsalves. and officially opened its door in 1973. 
 
Mission Statement 
The mission of the Gonsalves Elementary School community is to educate all students to become 
informed and productive 21st Century citizens. We will maintain a safe school environment that 
enhances effective learning and promotes positive behavior. 
 

----

--

-- 

ABC Unified School District 

16700 Norwalk Blvd. 
Cerritos, CA 90703 

(562) 926-5566 
www.abcusd.us 

 

District Governing Board 

Ernie Nishii, President 

Dr.Olga Rios, Vice President 

Sophia Tse, Clerk 

Christopher Apodaca, Board 

Member 

Leticia Mendoza, Board Member 

Maynard Law, Board Member 

Soo Yoo, Board Member 

Leticia Mendoza, Board Member 

 

District Administration 

Dr. Mary Sieu 
Superintendent 

Dr. Valencia Mayfield 

Assistant Superintendent, 

Academic Services 

 

Toan Nguyen 

Assistant Superintendent, 

Business Services 

Chief Financial Officer 

 

Dr.Gina Zietlow 

Assistant Superintendent, 

Human Resources 

 

 

2017-18 School Accountability Report Card for Joe A. Gonsalves Elementary School 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Joe A. Gonsalves Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

27 

27 

27 

0 

0 

0 

0 

0 

0 

ABC Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Joe A. Gonsalves Elementary 
School 
Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

16-17 

17-18 

18-19 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.