Elim Elementary is located in a small farming community in Central California. It is a Transitional Kindergarten through fifth grade (TK-
5) school with nearly 1,000 students, that operates on a traditional calendar. Our student population is diverse and we pride ourselves 
on meeting the needs of our community of learners. Our entire staff of dedicated teachers and support personnel are committed to 
providing an exemplary learning environment based on high academic expectations for all students using Common Core Standards-
Based Programs with supplemental programs for students with additional needs. Elim supports both an English/Spanish K-5 Dual 
Immersion program and a English/Portuguese Immersion program which is in its second year of implementation. We also have a 
Portuguese cultural enrichment program in grades 3-5. Elim Elementary is a safe and fun community of learners who are 
kind,respectful, and responsible. They set high expectations for themselves and persevere.