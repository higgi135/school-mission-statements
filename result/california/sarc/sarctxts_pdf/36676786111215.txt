Woodcrest is committed to providing a safe and supportive educational environment where 
students, parents, and staff work as a collaborative team. Woodcrest Junior High School holds the 
recognition of being a “Microsoft Showcase School” and an educational Results Partnership “Honor 
Roll School”. High standards in curriculum, technology integration and personal behavior are at 
the forefront of instruction. Students are challenged with engaging Common Core Content 
Standards-based curriculum to increase academic achievement. In addition, students will develop 
skills to become successful, well rounded, responsible individuals who will be productive members 
in our culturally diverse society. We are united in our purpose to create and cultivate life-long 
learners who will make a positive impact on the community we serve. 

----

--

-- 

Chino Valley Unified School 

District 

5130 Riverside Drive 
Chino, CA 91710-4130 

(909) 628-1201 

www.chino.k12.ca.us 

 

District Governing Board 

James Na, President 

Irene Hernandez-Blair, Vice 

President 

Andrew Cruz, Clerk 

Christina Gagnier, Member 

Joe Schaffer, Member 

Alexi Magallanes, Student 

Representative 

 

District Administration 

 

Superintendent 

Sandra Chen 

Associate Superintendent, Business 

Services 

Grace Park, Ed.D. 

Associate Superintendent, 

Curriculum, Instruction, 
Innovation, and Support 

Lea Fellows 

Assistant Superintendent, 
Curriculum, Instruction, 
Innovation, and Support 

Richard Rideout 

Assistant Superintendent, Human 

Resources 

Gregory J. Stachura 

Assistant Superintendent, 

Facilities, Planning, and Operations 

 

2017-18 School Accountability Report Card for Woodcrest Junior High School 

Page 1 of 9