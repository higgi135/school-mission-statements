Montgomery Middle School is located in the southwest region of San Diego County serving the Otay Mesa community. Consisting of 
grades 7 and 8 we follow a modified year round calendar that allows for strategically placed breaks where supplemental academic 
support is provided. Our staff proudly instructs and assists nearly 850 students. We take great pride in educating and nurturing our 
students in this critical two year span while preparing them for high school and beyond. 
 
Outlined in the plan that follows we have identified actions and services that help in meeting the needs of all our students. Through 
a method that mirrored the District’s own Stakeholder Engagement process we took input from students, staff, and our community 
to align District and State priorities with our own specific site needs using data and measurable outcomes. Below is our story as we 
relate Montgomery Middle’s actions in meeting our student needs. These actions paired with our faculty and staff make it great to 
be a Mayan! 
 
Goal 1: Ensure excellence in teaching and learning so each student is prepared to succeed in college and career. 
 
We currently employ 43 certificated teachers and 38 classified staff. Many of our programs support the specific needs of GATE 
identified students, students with disabilities, low income, foster youth, and English learners. We provide training and support through 
weekly Professional Learning Communities, technology workshops, District facilitated trainings, faculty meetings, department 
pullouts, in-service days, and specialty trainings that cover everything from Autism to bilingual education. 
 
In supporting our teachers with providing best first instruction our Professional Development plan is focused on providing support 
with planning, implementing, and sharing of structured student interactions. Our past work with the research of John Hattie's meta-
analysis with Visible Learning guide our professional learning plans. We are building on last years focus of clarity and engagement to 
maximize the impact our instruction has on student success. Our continued commitment to improve literacy is done school-wide 
through the use of Achieve 3000, Accelerated Reader, and AVID strategies while collaborating usage in all subject areas. 
 
Our extensive AVID and AVID Excel programs provide students access to experiences, skills, and mindset to make going to college an 
active goal. Study strategies, peer tutorials, along with mentoring from college tutors provide students with the tools needed to 
succeed on a pathway towards college enrollment. Field trips and collaboration experiences allow for students to see the reality that 
college affords them. Data shows that AVID students earn a higher average GPA than those not taking the course. 
 
The collaborative spirit of our departments and staff are highlighted within our professional learning communities. Led by key faculty 
and supported by the entire staff these groups collaborate to plan for common lesson, assessments, and activities. These groups also 
analyze data and student outcomes to best predict what students will need. Common Core Standards, Next Generation Science 
Standards, along with English Language Arts and English Language Development frameworks guide our work and practices. Using 
Google Forms we have created accountability as well as a system for reflection ultimately increasing the rigor and relevance of our 
classroom instruction. This school year, led by our Assistant Principals, we are meeting with Curriculum Specialists and resource 
teachers to build leadership capacity and support for PLC work. 
 
Technology and 21st century skills play an important role at Montgomery Middle in the support of teaching and learning. Our Site 
Curriculum Specialists, Genius Bar, along with our Blended Learning Specialists provide insight on how to best use technology to 
redefine and enhance classroom instruction. Each student is issued an iPad, however we teach our students over multiple platforms 
while stressing digital citizenship. The formation of our Technology Team led by our Blended Learning Specialist lead the support and 
implementation of technology to further support classroom instruction. 
 

2017-18 School Accountability Report Card for Montgomery Middle School 

Page 2 of 13 

 

Thursday Tech Tips, SDCUE, District trainings, Student Technology Bootcamps, and the 2nd annual Sweetwater Solar Sprint are but a 
few of the events we host or attend in furthering our development in educational technology. Our classrooms are rich in educational 
experiences where students use technology to create, collaborate, and exhibit learning with skills that will serve them in high school, 
college, and the workforce. This year we have added an advanced Computer Science course, makerspace room, as well as increased 
Robotics opportunities. 
 
Through grants and supplemental funding our students are able to take advantage of college tutors in our after-school tutoring 
program GIFT, as well as within the school day in designated classes. These tutors assist with classwork, homework, study skills, and 
prove to be wonderful role models. The program is mirrored at Montgomery High School with many of the tutors working at both 
sites. We will be looking at other grant opportunities, Title I funding, and District supplemental money to increase the amount of 
tutors in our classrooms. 
 
As an Administrative team we are working with the County Office of Education's Program Evaluation Institute to improve our support 
to teaching and learning through the impact of walkthrough observations. Our work initiates with increasing the consistency and 
quantity of our classroom observations. Through collaboration with teachers the focus will develop the quality of our support and 
feedback. This plans focus is primarily on improving teaching and learning but will also have a large impact of several other factors 
that affect student success. 
 
Goal 2: Create a safe and healthy learning environment for each student by building a culture of equity and a positive climate that 
promotes excellence throughout the District. 
 
In creating a safe and healthy environment to support student success we address both the physical and mental needs of our students. 
Our custodial crew along with staff ensure our campus is clean and one that our community can be proud of. With recent Proposition 
O and before that Proposition BB our site has a mixture of renovated rooms along with two new structures that were completed in 
2013. Each classroom has air conditioning, updated computers and projectors, and Wi-Fi throughout campus. Our newest state of 
the art buildings are LEED (Leadership in Energy and Environmental Design) Certified being built with sustainable and local materials 
that operate while conserving resources. Pictured on our Website and easily seen from Picador Boulevard these recently completed 
structures are a highlight within our community. 
 
To further ensure the well being of our kids we employ two campus assistants, 2 Assistant Principals, 3 counselors, a school nurse, 
school psychologist, Community Relations Facilitator, and Community Resource Center. Our parent volunteers have increased in 
numbers each of the past three years and they provide supervision and assistance in a variety of areas. Safety drills are held quarterly 
to prepare for fires, evacuations, lockdowns, heightened security, earthquakes, and reunification. 
 
In serving students from the ages of 12 – 14 we understand our responsibility in helping them deal with the many social and emotional 
changes that occur during their time in middle school. Programs such as AVID, Peer Mediation, WEB (Where Everyone Belongs), ASB, 
SST (Student Study Team), and Club Maya help students feel a sense of pride and belonging to Montgomery Middle School. Some of 
these programs build student’s self esteem and excitement for school while others assist in getting help and support. Our counselors 
and staff are highly trained in assessing student’s social emotional needs and even better at utilizing agencies such as our own 
Community Resource Center to connect families to assistance. 
 
As an entire school site we are going through the Strength-based Institute. Through assessments, trainings, and practice we have 
identified our talents and how to best use them in the world around us. Strengths will give us common language in which to build 
positive relationships in teaching and learning across campus. School activities and culture will further support this growth mindset. 
This year we are building understanding of a strengths-based approach with parents and the community through their inclusion on 
activities at various community and site events. In our partnerships with the County Office of Education, YMCA, SDPD, and Youth 
Development Network we have begun to build a community that supports our students through strengths. 
 
Club Maya, our before and after school program, provides students with experiences that enhance the learning and social 
environment. Our sports programs within Club Maya build pride, self-esteem, and responsibility within participants. High interest 
clubs allow students to express their talents in ways that are not always possible within the normal school day. Field trips and 
collaboration meetings with other schools give our students new experiences to broaden and brighten their future. 
 
Goal 3: Foster and honor parent/guardian and community engagement to support excellence in each student’s success. 
 
Montgomery Middle School welcomes parent volunteers. Our Community Relations Facilitator and Coordinated Integrated Services 
position recruit parents to supervise, assist with activities, prepare materials, and most importantly give input. The Parent Center and 
CIS office serve as a hub for parents to collaborate as well as access many support services. 

2017-18 School Accountability Report Card for Montgomery Middle School 

Page 3 of 13 

 

 
Parent contacts are made through school-wide meeting and through multi-media connections such as texting, email, website, 
Facebook, phone calls, and postal service. Our families are offered a voice through such school meetings as Open House, Academic 
Success Parent Night, Coffee with Principal, Compact for Success trip, College and Career Night, School Site Council, Title I, and the 
English Learner Advisory to name a few opportunities. Invitations to our Mayan Revolution allow parents to shadow their children to 
gain insight to our academic practices and our positive school culture. At each of these events along with our Parent Needs Assessment 
survey we seek and use parental feedback to assist in completing our School Plan for Student Achievement. 
 
To supplement our on site events, meetings, and trainings we are continuing to increase our use of technology to bridge any 
communication gaps. In response to previous parent input we are implementing a school Facebook page this year. Through our 
website we post resources such as video tutorials to assist in accessing grades, presentations on academic requirements, and 
information on upcoming events. Next steps include having minutes and resources from all on site parent meetings posted online. 
 
To further our outreach into the community we pair with SDPD, service clubs, health clinics, community resource centers, and local 
businesses. The connections bring mentorships, rewards, free services, and other opportunities that help our students thrive, 
physically, emotionally, and academically. Community events also give our kids the chance to highlight their accomplishments and 
bring a sense of pride to the Otay Mesa area. 
 
Goal 4: Develop coherent and transparent systems for operational excellence to support each student’s success. 
 
Site groups such as the Faculty Advisory Committee/Site Leadership Team, Classified Advisory Committee, Student Leadership groups, 
Student Study Team, Student Leadership Team, School Site Safety Committee, AVID Site Committee, and School Site Council work to 
identify ways to comprehensively support student success. The groups provide input for our processes and procedures. Meetings 
allow for streamlined communication and feedback on budgets, maintenance, staffing, and programs. The perspectives of all 
stakeholders continually improve our systems of support. 
 
In coordination with our District Office we staff our site with highly qualified personnel. Our faculty and support staff continues to 
receive professional development on site, through District and County Offices, along with related leading edge conferences and 
seminars. These trainings ensure our staff are equipped with the knowledge to assist our students in gaining the 21st century skills 
that they will need in high school, college, and the work force. 
 
Our recent partnerships with the CORE Districts and the National Center for Urban School Transformation have increased our focus 
with school systems. We now have access to a wider range of data that considers climate and culture into academic success. NCUST 
and the CORE Districts provide us with examples of successful schools as well as common frameworks within these schools. Our goal 
is to use this information to identify the systems we have in place that prohibit success so that we can remove them and the focus to 
create systems that increase access to success for all students. 
 
Mission/Vision- Montgomery Middle School's Mission Statement 
Our mission is to discover and cultivate our strengths to create a Mayan family rooted in trust, empathy, responsibility, and resilience 
which will lead to academic and personal success for each student.