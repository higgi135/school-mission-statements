The school mission and vision is posted in all classrooms and regularly articulated via presentations 
at Back to School Night, Parent/Student handbook and most school correspondences. The Ladd 
Lane community works together to continuously analyze evidence and refine school priorities 
according to our focused mission and vision. Utilizing our Professional Learning Community 
Framework, the leadership team, with assistance from the principal worked with the staff to 
develop this focus. In creating our mission and vision statement, staff researched the characteristics 
of powerful organizations. The faculty then brainstormed and prioritized a list of skills that are 
needed for students to be high level learners. Through a partnership with parents, students and 
staff, Ladd Lane School is making great strides in continuously aligning our school priorities with 
our mission. We are committed to ensuring that all of our students achieve to their fullest potential. 
Especially, that the achievement gap of English Learners is significantly decreased via the utilization 
of key signature practices which are aligned with our school vision. 
 
Mission Statement: 
Ladd Lane's mission is to provide an atmosphere where each child is encouraged to strive for 
excellence academically, socially and emotionally in a safe and supportive environment. 
 
Vision: 
Ladd Lane embraces the importance of collaboratively providing services and supports to 
strengthen the academic and social achievements of students while preparing them to be 
productive citizens. 
 
The community is located in the city of Hollister which sits in northern San Benito County, 47 miles 
from San Jose, 39 miles east of the Monterey Peninsula, and 90 miles south of San Francisco. The 
region retains its agricultural heritage. Ladd Lane Elementary serves approximately 670 students in 
grades TK through fifth, including four special day classes that serve students with moderate to 
severe disabilities. Although we are one of the largest elementary schools in the district, Ladd Lane 
prides itself on its "family type" atmosphere. Education, including an academically rigorous 
program, is highly valued by our faculty and families, along with a high degree of staff morale and 
parent satisfaction. Each year we have many more families request or transfer into our school than 
we can accommodate. There is a strong commitment to excellence on the part of the entire faculty. 
 

-------- 

Hollister School District 

2690 Cienega Rd. 

Hollister, CA 95023-9687 

(831) 630-6300 
www.hesd.org 

 

District Governing Board 

Stephen Kain, President 

Robert Bernosky 

Carla Torres - Deluna 

Jan Grist 

Elizabeth Martinez 

 

District Administration 

Diego Ochoa 

Superintendent 

Jennifer Wildman 

Assistant Superintendent, 

Educational Services 

Erika Sanchez 

Assistant Superintendent, Human 

Resources 

Gabriel Moulaison 

Assistant Superintendent, Fiscal 

Services 

Barbara Brown 

Acting Director, Student Support 

Services 

John Teliha 

Director, Facilities 

Jr. Rayas 

Director, Technology & Innovation 

Caroline Calero 

Director, Learning & Achievement 

Ann Pennington 

Director, Student Nutrition & 

Warehouse 

 

2017-18 School Accountability Report Card for Ladd Lane Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Ladd Lane Elementary School 

16-17 17-18 18-19 

Teacher Credentials 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Hollister School District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

33 

33 

30 

0 

0 

0 

0 

3 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

240 

35 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Ladd Lane Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.