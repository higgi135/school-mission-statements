Mission: 
Frank Ledesma's mission is to provide a safe, nurturing environment in which all students learn the essential grade level Common Core 
State Standards, and demonstrate appropriate behavior, to become critical, creative thinkers, and independent problem solvers in a 
diverse society. 
 
Vision Statement: 
At Frank Ledesma Elementary we will create a safe learning environment where the school and community collaborate to develop our 
diverse students to become 21st Century Learners and responsible decision-makers. 
 
This year's initiatives are to: 
. Implement the training from HMH Math, HMH English Language Arts, Fisher & Frey, Read 180/System 44, and 3-D ELD curriculum 
. Continue to Implement the Common Core State Standards for reading, language arts, and math. 
. Implement highly effective daily Math Talks 
. Implement the professional development modules for ELA which address SUSD Systematic Instructional Framework: 
. Lead High-Level, Text-Based Discussions 
. Focus on Process, Not just Content 
. Create Assignments for Real Audiences and with Real Purpose 
. Teach Argument, Not Persuasion 
. Increase Text Complexity 
. Increased Rigor throughout our Curriculum and Instructional practices 
. Continue to implement and refine our practices in Positive Behavior Interventions and Supports 
. Continue to conduct data analysis with our NWEA, Reading Inventory, progress monitoring assessments, and other grade level 
assessments 
. Continue grade level planning during PLC time with emphasis on implementing strategies from trainings (HMH Math, HMH ELA, Fisher 
& Frey) and implementing daily Math Talks 
. Continue to involve our parents through PTO, ELAC, and School Site Council Meetings.