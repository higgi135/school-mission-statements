CORE Placer Charter School is a non-classroom based personalized learning school in the foothills of Placer County, about 30 minutes 
east of Sacramento with a modern log cabin as its main office and resource center. It serves approximately 450 students that reflects 
the diversity of the communities and contiguous counties of Placer, Sacramento, Nevada, Sutter, El Dorado, and Yuba, comprised of 
13 Black or African American students, 23 Hispanic or Latino students, 5 Asian students 409 white students, and 42 students eligible 
for free or reduced meals. Core Placer Charter has gone through many leadership and Board changes in the past three years, but the 
unifying focus has remained the same, one team with one goal-making a difference for our students daily. CORE Placer Charter School 
creates an individualized academic plan for each student based upon his or her educational needs, talents, strengths, and weaknesses. 
This approach supports individual development through a specially designed curriculum aligned with state Common Core Standards, 
while engaging parents and students in learning and goal setting. The Personalized Learning system of education followed by CPCS is 
a unique, blended independent study with learning center support through targeted intervention and enrichment classes, public 
educational model that is tailored to the needs and interests of each individual student. CORE Placer Charter School places a strong 
emphasis on parental involvement, smaller class sizes, more one-on-one teacher and student interaction with attention to differences 
in learning styles. It incorporates student-driven participation in developing the learning process and includes technology access, 
varied learning environments, teacher and parent development programs, and choices in curriculum programs. Mission Statement: 
CORE Placer Charter School utilizes the independent study/personalized learning approach. We support choice of curriculum aligned 
with Common Core Standards, engage parents along with students in learning, and offer classes at our centers and within the 
community. Our goal is to help students achieve measurable academic growth in addition to the social skills necessary for their future 
success. 
 
Core Belief: All Children Can Learn and Have Instructional Excellence Verified Everyday. 
 
Mission Statement: 
Core Placer Charter School utilizes the independent study/personalized learning approach. We support choice of approved and 
adopted curriculum to meet Common Core State Standards (“CCSS”) in our core courses, to engage parents along with students in 
learning, and utilize only school-approved vendors and courses within the local and expanded community. Our goal is to help students 
achieve measurable significant academic growth in addition to providing opportunities to develop and enhance social and life skills 
necessary for their future success. We seek to enable our students to become self-motivated, competent, and lifelong learners. 
 
Vision: To educate, elevate and empower CPCS students to achieve academic success in a personalized learning setting. The key to 
success in personalized learning is the collaboration between the students, parents and assigned, certificated Personalized Learning 
Teacher (“PLT”) who work together to develop a personalized learning plan. The personalized learning approach encourages students 
to be highly involved in their own educational process, thereby becoming self-motivated, competent, life-long learners. 
 
To educate, empower and inspire CORE students to achieve academic success in a more personalized instructional setting. 
 
Core Placer Charter School believes: 

• 
• 
• 
• 
• 
• 
• 
• 
• 
• 

That parents/caregivers play an essential role in their children’s education. 
That partnering with parents/caregivers is key to student success. 
That all students have unlimited potential. 
That innovation is necessary for success. 
That learning should be authentic, creative, meaningful, and rigorous. 
That mindsets are critical. 
That social emotional learning and character education are critical. 
In a connected and caring community. 
In a caring culture that is rigorous, joyful, and fun. 
It is vital for teachers to have time to collaborate with colleagues and parents, and for professional development/training. 

2017-18 School Accountability Report Card for CORE Placer Charter School 

Page 2 of 16