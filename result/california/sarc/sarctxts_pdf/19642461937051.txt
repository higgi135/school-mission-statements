Quartz Hill High School (QHHS) is located in Quartz Hill, California, a small, unincorporated community located in the western portion 
of the Antelope Valley. The Antelope Valley represents the northernmost area of Los Angeles County in Southern California. The 
community lies flanked between the city of Lancaster to the east and the city of Palmdale to the south. Quartz Hill was settled in the 
1880s, and three families—the Lanes, the Goddes and the Stratmans—were responsible for initial settlement. It was first known as 
Belleview, but the name was later changed to Quartz Hill. First opened in 1964, Quartz Hill High School (QHHS) was the third school 
built in the Antelope Valley Union High School District (AVUHSD), which now encompasses eight comprehensive high schools, three 
continuation schools, and other alternative education sites including Adult Education, Community School, six site-specific Independent 
Study programs and six on-site continuation programs. The district currently serves the educational needs of over 25,000 students. 
Quartz Hill High School began the 2018/2019 school year servicing over 2900 students. 
 
Once in the heart of a rural area in the western Antelope Valley, Quartz Hill High School now draws students from an area of mostly 
residential single-family homes, with a smattering of multi-family complexes to the north and east. To the west of the school lie the 
vast, open spaces of the Antelope Valley. The aerospace industry remains the primary employer in the Antelope Valley, with large 
employee bases at Lockheed, Rockwell, Northrup-Grumman and Edwards Air Force Base. Major dependence on aerospace as an 
employment source has generated boom-bust employment cycles, but the cities of Palmdale and Lancaster continue to diversify 
economically. Many parents in the Quartz Hill area commute daily into the Los Angeles area, and most would be classified as middle 
income, but the entire strata of low to high-income families are represented in the attendance zone. The current attendance zone is 
three hundred square miles. 
 
Quartz Hill High School’s eighty-acre permanent facility, located at Avenue L and 60th Street West, was originally built to house 1800 
students. Extensive demographic growth necessitated the construction of additional facilities, including a second gymnasium, the 
expansion of the Library/Media Center, a stadium, and forty-five portable classrooms. The AVUHSD had an open-enrollment policy for 
schools in the district, allowing students to apply to schools outside their enrollment/attendance area. Under open-enrollment, QHHS 
was a preferred site. The District discontinued open-enrollment at the beginning of the 2006-2007 school year and a yearly application 
system is now in place. 
 
In 1998, QHHS became an International Baccalaureate (IB) school and the first IB class graduated in the spring of 2001. As of the most 
current academic year (2018/2019), there are 68 total IB students with 31 being IB diploma candidates and 37 anticipated diploma 
candidates. Quartz Hill is the only school in the district offering IB courses and the IB Diploma in 2018/2019. Students who have met 
IB entry requirements in other district schools may apply for a transfer to Quartz Hill upon entering 11th grade. 
 
Last year, QHHS seniors received some $1,370,373 in scholarship awards. The monetary total included school, local, military, state and 
national scholarship awards. 
 
QHHS Mission Statement: Engage, equip, and empower all students to be problem solvers. 
 
QHHS Vision Statement: REBS 
 
Responsible Citizens - Students will improve the quality of life in our school and community at large by displaying tolerance, respect 
for others, poise, and self-control, and by participating in the democratic process. 
 
Effective Communicators - Students will demonstrate the ability to communicate ides clearly and to respond appropriately to the 
messages of others through reading, writing, listening, and speaking. 
 
Bold Problem Solvers - Students will work to analyze and evaluate information in order to build critical thinking skills that can be used 
to create innovative solutions to real-world problems. 

2017-18 School Accountability Report Card for Quartz Hill High School 

Page 2 of 14 

 

 
Self-Directed Learners - Students will demonstrate a positive work ethic by practicing organizational strategies, planning ahead, and 
using effective study skills independently. 
 
AVUHS District Mission Statement: “Our mission is to provide a safe and secure learning environment that promotes a rigorous 
curriculum and enables our students to develop the necessary academic, technical, and work-related skills of the 21st century.” 
 
AVUHS District Vision Statement: "Our vision is that every student who graduates will be prepared to pursue college and any career 
to which he or she aspires."