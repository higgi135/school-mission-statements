Bassett High School is located in the San Gabriel Valley, fifteen miles east of Los Angeles. Bassett Unified School District attendance 
areas include unincorporated areas of Los Angeles County (Valinda and Bassett) and portions of the cities of La Puente, Baldwin Park, 
and City of Industry. Bassett High School opened its doors in September 1965, serving the surrounding community for 53 years. The 
school population has two significant subgroups: Hispanic/Latino and socio-economically disadvantaged. The school’s ethnic diversity 
has remained stable. 87.4% of students participate in the free or reduced lunch program. 16.6% of our students are English Learners 
and the GATE population has increased since 2007 and is currently 106 students. The school offers the following programs: a Regional 
Services Orthopedically Impaired program, a Special Education program, an English Learners program, dual enrollment with Mt SAC, 
and a Health Academy which has has existed for 17 years. 
 
Mission 
Bassett High School provides a diverse, high quality education in a safe and supportive environment to empower students to become 
self-directed and goal oriented, to value themselves and others, and to use technology effectively. 
 
Vision 
BHS prepares college and career ready students to become life-long learners, responsible citizens and globally competitive individuals 
by promoting creativity, communication and collaboration.