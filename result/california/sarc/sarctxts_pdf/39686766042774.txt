The Professional Learning Community at Taylor Leadership Academy begins each day with 
assuming positive intent and taking part in a collective mindfulness activity. Taylor Leadership 
Academy serves an ethnically diverse population of students, 100% of students receive free and 
reduced breakfast and lunch, and around 28% of the student population are identified as English 
Language Learners. We are committed to fostering a place of learning for all students to learn and 
excel within a democratic society through collaboratively engaging the heads and hearts of all 
learners, inspiring and promoting the use of their imaginations, and the desire to always put forth 
your best effort. Our vision is that we are leaders, learners, and thinkers preparing for our future. 
Our mission is working collaboratively as a professional learning community we will close the 
achievement gap by preparing all students for college, career readiness, to be successful in a global 
society, and utilizing their imagination and critical thinking skills. Our social-emotional learning 
commitment is that we will treat ourselves and others with respect and kindness at all times. As a 
staff we pride take pride in creating an inspiring school environment that promotes creativity and 
respectful self-expression, critical thinking, solution oriented approaches, and all learners 
developing and utilizing a growth mindset. 
 
At Taylor Leadership Academy we strive to provide a safe learning environment for all students 
while implementing rigorous instructional practices that inspire and promote students' active 
participation in their learning while enjoying the learning process and developing their capacity to 
be creative, innovative, and transfer their academic knowledge into practice. Taylor Leadership 
Academy provides educational opportunities that are aligned with AVID, Direct Interactive 
Instruction, Common Core State Standards, STEM, Academic Parent Teacher Team meetings, and 
the development of a professional learning community driven by student data and the needs of the 
learners we serve. Some of the programs that are utilized at Taylor Leadership Academy to support 
student achievement and social-emotional growth are: AVID, PLUS, ST Math, Imagine Learning, 
Point Break Anger Management groups, PBIS, Restorative Justice Circles, and No Bully Solution 
Teams. Teachers utilize Units of Study as a framework for delivery of Common Core State Standards 
instruction in the areas of ELA, Math, and ELD and utilize MAP, CCSS pre, post, and performance 
task assessments as well as teacher created common formative assessments to measure student 
academic growth. Science and Social Studies are taught using core textbooks and all students 
receive their P.E. instructional minutes as outlined by the CDE guidelines. 
 
Our Family Resource Center offers on-going programs and training that provide learning 
opportunities for parents, guardians, and community members including health, medical, and 
dental services, parent and student counseling, tax services, health and parenting classes, 
community social events, and weekly Thursday parent coffees. 
 
School wide goals represent our desire to reduce chronic absenteeism, increase daily student 
attendance, provide research-based instructional practices utilizing DII and AVID research-based 
strategies that engage and prepare all students to be proficient readers by the end of third grade, 
provide CCSS math 
increasing students' problem solving and 
mathematical reasoning abilities so they will be successful in understanding higher level math and 
pass Algebra courses, and through utilizing AVID instructional strategies preparing students to 
graduate high school with A-G requirements so they are college and career ready. At Taylor 
Leadership Academy we are actively facilitating the development of active leaders, learners, and 
thinkers. 

instruction that supports 

2017-18 School Accountability Report Card for Taylor Leadership Academy 

Page 1 of 11 

 

 
The school goals in the areas of Math and Reading for the 2017-2018 school year are: 
 
By June of 2019, the school-wide percentage of students who meet their projected growth target for Reading on the MAP assessment will 
increase by 10% in reading, as compared to the 2018 MAP growth data. Students will move from 49% to 59% of students meeting their 
projected growth target in Reading as measured by MAP. 
 
By June of 2019, the school-wide percentage of students who have met or exceeded the standard target for Reading as measured on the 
SBAC will increase by 10% in reading, as compared to the 2018 SBAC growth data. Students will move from 16% to 26%. 
 
By June of 2019, the school-wide percentage of students who meet their projected growth target for Math on the MAP assessment will 
increase by 10% as compared to the 2018 MAP growth data. Students will move from 48% to 58% of students meeting their projected 
growth target in Math as measured by MAP. . 
 
By June of 2019, the school-wide percentage of students who have met or exceeded the standard target for Math as measured on the 
SBAC will increase by 10% in Math, as compared to the 2018 SBAC growth data. Students will move from 13% to 23%. 
 
Benjamin Yang 
Principal 
Taylor Leadership Academy 
Stockton, CA 
1101 Lever Boulevard 
(209)-933-7290 
 
 

 

2017-18 School Accountability Report Card for Taylor Leadership Academy 

Page 2 of 11 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Taylor Leadership Academy 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

18 

18 

22 

4 

0 

5 

0 

2 

0 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Taylor Leadership Academy 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

1 

4 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.