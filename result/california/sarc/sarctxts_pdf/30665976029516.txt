Whittier School is located at 1800 Whittier Avenue within the City of Costa Mesa. The school serves 
675 transitional kindergarten through sixth grade students. In addition, 79 preschool students are 
served in an on-campus preschool setting. Our community consists predominantly of apartment 
dwellings. 77% of our students are English Language Learners and are learning formal English as 
well as literacy skills. 100% of our students qualify for the free or reduced lunch program. 52% of 
students participate in additional classroom support and/or intervention outside of the homeroom. 
Just over 9% of students work with a Specialized Academic Instructor and/or Speech and Language 
Pathologist per his/her IEP. Whittier also offers a Spanish Dual Immersion Program beginning in 
kindergarten. We have an active parent community participating in PTA and ELAC. We also partner 
with local community organizations and churches to support our students, parents, and staff. 
 

----

---- 

Newport-Mesa Unified School 

District 

2985 Bear St. 

Costa Mesa, CA 92626 

(714) 424-5033 
www.nmusd.us 

 

District Governing Board 

Charlene Metoyer, President 

Martha Fluor, Vice President 

Dana Black, Clerk 

Ashley Anderson, Member 

Michelle Barto, Member 

Vicki Snell, Member 

Karen Yelsey, Member 

 

District Administration 

Dr. Frederick Navarro 

Superintendent 

Mr. Russell Lee-Sung 

Deputy Superintendent, 
Chief Academic Officer 

Ms. Leona Olson 

Assistant Superintendent, 

Human Resources 

Mr. Tim Holcomb 

Assistant Superintendent, 

Chief Operating Officer 

Dr. Sara Jocham 

Assistant Superintendent, 

Student Support Services/SELPA 

Mr. Jeff Trader 

Executive Director, 

Chief Business Official 

Dr. Kirk Bauermeister 

Executive Director, 
Secondary Education 

Dr. Kurt Suhr 

Executive Director, 

Elementary Education 

 

 

2017-18 School Accountability Report Card for Whittier Elementary School/Preschool 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Whittier Elementary School/Preschool 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

38 

41 

44 

0 

0 

0 

0 

0 

0 

Newport-Mesa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1105 

5 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.