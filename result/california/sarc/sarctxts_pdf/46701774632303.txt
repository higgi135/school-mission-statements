Downieville is located on the western slopes of the Sierra Nevada and is the county seat. Its year-round population is approximately 
325, but that number swells during the summer due to tourism. Presently, the chief employers in the community are the County of 
Sierra, Cal-Trans, tourist-related businesses and the schools. Along with recreation, the economy was formerly based in mining and 
forestry, but the last decade has seen an employment decline in these areas causing an exodus of families from the region. Many 
homes have been purchased as second homes and are only used during the summer tourist season. 
 
Downieville Junior-Senior High School is a small community of four classrooms. We have few teachers that need to wear many hats. 
There are two full time teachers and two part time. A full time teacher instructs the core math and science courses, another full time 
teacher instructs the social science courses, and the part time teachers instruct core english courses and wood shop. In addition to 
their main subject matter, these teachers also provide a selection of elective offerings. 
 
The school is located at the same site as the elementary school. The schools share their site administrator, office, special education, 
facility, custodial and kitchen staff. This highly unique school offers the students the opportunity to receive ample direct attention 
from their certificated instructors. Therefore, it is very difficult for our students to “fall through the cracks”. Because of our small size 
and limited teachers, we are forced to offer several classes with combined grade levels. These combined classes are then rotated every 
other year. This allows the kids an opportunity to work at their grade level or move up or down as needed to become successful. 
 
The goal of instruction at Downieville Jr/Sr High School is to help students achieve performance at grade level standards and to be 
secondary education ready. We assess this by tracking our own formal assessments as well as standardized tests. 
 
The attendance area includes several small communities from Bassetts to Alleghany along the Highway 49 corridor.