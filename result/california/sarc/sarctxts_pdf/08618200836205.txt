School Description and Mission Statement: 
Mission: Our mission is to provide students with alternative forums for demonstrating the talents, leadership and skills required to 
earn a high school diploma, and prepare for college and career readiness. 
 
Vision: Sunset High School is a place where respect, encouragement, acceptance, responsibility and personal growth are encouraged. 
All staff and students work together to create a safe, successful learning environment, and a sense of community. Our efforts aim to 
promote citizenship through the thoughtful development of the individual. 
 
Sunset MAPS (Expected Schoolwide Learner Outcomes) Sunset High School Graduates will be: 
 
Moral and ethical individuals who: Show tolerance for others, and advocate for what is right. 
Academic Achievers who: Are employable, use technology, and make educational and career plans for the future. 
Physically and emotionally healthy adults who: Become lifelong learners, evaluate a variety of life options and choices, and 
communicate effectively. 
Socially responsibly citizens who: Perform community service projects, know that they impact the world, are positive role models, 
know how to identify and solve problems. 
 
 
School Discription: 
Sunset High School is committed to engaged student learning, and promoting a school culture where students choose to attend every 
day. As an alternative secondary school, we are dedicated to positively re-capturing students who are severely credit deficient; have 
a history of chronic absenteeism, or discipline referrals and suspension; or simply students who felt they didn't fit’ in a traditional, 
comprehensive high school setting. 
 
Sunset High School will provide opportunities to students for reciprocal teaching, peer instruction, and collaboration while 
implementing our experiential learning program benefiting both our students as teachers, and visiting elementary students from 
around the county. 
 
Sunset High School will continue to transition from the ‘packet-culture’ of the past, to engaged, direct instruction. This includes 
implementing a one-to-one (tech device to student) focus for our students to access current technology and information. 
 
Sunset High School supports students as individuals who are always ‘on our radar’, who will take responsibility for their work habits, 
attendance, personal interactions with others, and behavior.