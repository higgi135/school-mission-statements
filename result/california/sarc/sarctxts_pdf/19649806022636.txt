John L Webster School is located in Malibu, California. Malibu is a small, coastal community with a 
population around 13,000 residents. The city, well known for its celebrity residents, beaches, and 
Pepperdine University, is located in Los Angeles County and is a short distance from Los Angeles, 
the state's largest city. Approximately 15 million tourists visit the city each year. 91.5% of the 
residents are white, 6.1% are Hispanic and nto other minority groups constitute more than 2.6%. 
14.2% of the population speak a language other than English in the home. The median housing 
price of $1,000, 001 is well above the national and state average. The median household income 
of $125,202 is also above the state average of $60,883. 
 
 
John L Webster School is part of the Santa Monica Malibu Unified School District (SMMUSD). 
SMMUSD serves approximately 11,300 students in both Malibu and neighboring Santa Monica 
communities. The district is comprised of 10 elementary schools, 3 middle schools, 2 high schools, 
1 alternative high school, 1 continuation high school, 1 adult school and and an Early Childhood 
Development Program. The district had an attendance rate of 95% during the 2015-2016 school 
year. 
 
 
District Vision: As a community of learners, the Santa Monica-Malibu Unified School District works 
together in a nurturing environment to help students be visionary, versatile thinkers; resourceful, 
lifelong learners; effective, multilingual communicators; and global citizens. We are a richly varied 
community that values the contributions of all its members. We exist to prepare all students in 
their pursuit of academic achievement and personal health and to support and encourage them in 
their development of intellectual, artistic, technological, physical and social expression. 
 
 
John L Webster is a grades TK-5 elementary school, located in the central part of the city of Malibu, 
and has a total enrollment around 300 students. 76% of the total student population is white, 12% 
is Hispanic or Latino, 6.2% two or more races, and 3.4% Asian. 7.9% of the population are classified 
and English Learners and 5.1% are classified as students with a disability. 6.8% of the student 
population is identified as socioeconomically disadvantaged. The school employs 14 teachers. 
Class sizes in grades K-3 have an average of 23 to 1, and in grades 4 and 5 the average is 30 to 1. 6 
teachers have earned the prestigious National Board Certification. In addition to the classroom 
teachers, the district supports the school site with a Literacy Coach, and Language and Literacy 
Interventionist, Classroom Instructional Assistants, a school psychologist, part time health office 
specialist and school nurse, part time attendance clerk, and office manager. 
 
 
Webster Vision: At Webster School, staff, parents, and community members work together to 
create a joyous and caring learning environment in which all students are supported, challenged, 
and successful. Webster students master the New California Standards, develop higher level 
thinking skills, and connect their learning to the real world in all areas of academic study, the arts, 
technology, and character development. 
 
 

 

2017-18 School Accountability Report Card for John L. Webster Elementary School 

Page 1 of 13 

 

Principal’s Message: 
 
Webster’s students take great pride in their school, in their learning, and in themselves because they are immersed in an environment 
where it is abundantly clear that everyone around them cares very deeply about their happiness and success. Virtually everyone who 
visits our school comments on how happy and purposeful our students are at school. They are surrounded by teachers who are passionate 
about teaching meaningful content via processes that support student engagement. They are also taught by specialists whose love of art, 
music, gardening, and technology is communicated unmistakably in every lesson. We also work diligently on Character Counts and our 
school follows the Olweus Bully Prevention Program. 
 
 
 
Our school has embarked on a comprehensive effort to make ensure every student meets the curreny California Standards. The elements 
of this effort include State-adopted standards-based instructional materials at the heart of the instructional program, a variety of 
supplemental materials, a consistent focus on professional development, and the regular use of assessment data to inform and guide 
instruction. Six Webster classroom teachers and our Literacy Coach have achieved National Board Certification, this is more than half our 
teaching staff! Webster was also recognized as a California Distinguished School in 2010 and 2014, based on the work our staff does on a 
daily basis with our students. 
 
 
Webster families view our school as a highly valued partner in the challenging, satisfying work of raising their children. Parents at Webster 
have very high expectations for us, but these are matched by their personal involvement and support. The most often-repeated comment 
from parents at Webster is, “I wish I could go to school here!” We regard that as high praise indeed. 
 
 

 

2017-18 School Accountability Report Card for John L. Webster Elementary School 

Page 2 of 13 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

John L. Webster Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

14 

0 

0 

 

 

 

 

 

 

Santa Monica-Malibu Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.