Mission: All students and staff will learn at the maximum academic level possible. 
 
Vision: The student population is very diverse, ethnically and linguistically. Currently Roosevelt student population is 55% Hispanic, 
26% White, 3% African American, and the remaining 16% is made up of Asian, Native American, Pacific Islander, and two or more 
races. Over 177 students at Roosevelt are designated English Learners. Roosevelt serves the “newcomers” who have only been in the 
United States 4 years or less, and long term EL students who have been in the US longer than 4 years. Academic Language 
Development classes , or ALD, are offered to support our long term English Language Learners. To support the newcomers for the 
entire district, the Language Institute was developed. The Language Institute is intensive language acquisition classes that also 
incorporate the core subject material. The students have an ELD class (English Language Development), acculturation, and Spanish 
for Spanish Speaker. The students are out with the general education students for math, science, history and PE. 
 
For our general education students who need extra help, Roosevelt has developed math support classes called Math 180. The 
students in these classes have an extra math class in place of their elective. Success Skills was also developed as an intervention for 
students that struggle in multiple subject areas. Success Skills involves working with students on basic skills needed to be successful 
in all areas of education and life. For struggling readers, Read 180 is offered. This is a computer based class that is proven to bring 
students reading level up multiple grade levels if they are behind. 
 
In addition to general education students, there are four special day classes, one is a severely handicapped class, two are learning 
handicapped classes, and one is for students who are limited learning handicapped. All of the special education classes have 
credentialed teachers along with multiple instructional para professionals to support the students. 
 
Roosevelt also offers a complete GATE/Honors program for students who show potential for moving on to advanced placement classes 
in high school. Elective choices include Band, chorus, string orchestra, art, computers, Robotics, Spanish, and Study Skills. Roosevelt 
has made many strides to keep up with the technology movements in our society. In order to keep up with the needs of these many 
portable devices the school has wireless capabilities throughout and all student are provided with a laptop to utilize during the school 
years. Additionally, both English Language Arts and Social Science have adopted digital curriculum. 
 
Roosevelt has a dedicated staff of both veteran and newer teachers. They do everything they can to make sure the students are 
learning. After school tutoring is offered Monday through Thursday for any students. There are also a variety of after school clubs for 
students to become involved with. For a more committed after school experience Roosevelt also houses a free After School Program 
where more than 120 students attend up to 6:00 P.M. Students have the opportunity to do homework and become involved in many 
activities ranging from sports to cooking club. 
 
Roosevelt prides itself in our academics. Students are rewarded as much as possible for their hard work and dedication. With our 
Renaissance program students with good grades and good behavior in all of their classes are rewarded. Staff and students both enjoy 
being at Roosevelt. A lot of importance is placed in Roosevelt PRIDE. Proud Roosevelt Individuals Developing Excellence.