Kernville Elementary School, serving K through fifth graders, is dedicated to ensuring the academic success of every student and 
providing a safe and comprehensive educational experience.The school is located in a rural, recreational setting approximately 40 
miles northeast of Bakersfield. The school serves the 
communities of Wofford Heights, Kernville and River Kern. Kernville Elementary School operates on a traditional school calendar. 
During the 2016-2017 school year, between 125-135 students were enrolled in grades kindergarten through fourth. 
 
Mission Statement: 2017-2019: A COMMUNITY UNITED IN CREATING LIFELONG LEARNERS TO IMPROVE OUR FUTURE THROUGH 
EDUCATION 
 
To meet this challenge, our school community is actively engaged creating a safe environment that teaches academics and character 
development, enhanced by a strong partnership between home and school. We have developed a strong professional learning 
learning community in order to align and improve our practices in implementing the California Common Core State Standards, and 
the instructional shifts inherent in those documents. During the 2016-2017 school year, administrative and certificated instructional 
staff participated in professional development delivered by The Leader In Me. Our professional learning was focused on essential 
components in growing leaders in our community, beginning with our school. Instructional Rounds focused on instruction and learning 
of the California Standards--particularly regarding the use and development of students' academic language. Further, we spent 
significant time learning about and practicing planning for increased student-to-student academic conversation around content and 
increasing the rigor and depth knowledge in what students are required to know and be able to demonstrate per their learning. 
 
Our school builds teams of teachers, students, parents, and community members to enhance the academic and social emotional 
learning environment. These teams include: 
 

1. School Site Council: The School Site Council (consisting of school staff, parents, and community members) is a major 
governing body that meets regularly to address programs as well as varying components that make up the school. They act 
as a communication liaison between the community and the school. 

2. Parent Teacher Club: The Parent Teacher Club hosts fundraisers and promotes school programs by supplying additional funds 

for teachers and students. 

3. Student Assistance Teams: Student Assistance Teams meet to discuss strategies to help struggling children with academics, 

attendance and/or behavior. 

4. District Advisory/Budget Committee: The District Advisory/Budget Committee, lead by the superintendent, provides 

stakeholder input regarding needs for expenditures. 

5. Positive Behavior Intervention System (PBIS) Leadership Teams: PBIS Leadership Teams work in collaboration with the 

principal to improve school climate by implementing systematic approaches to creating a positive learning environment. 

6. Professional Learning Communities: Professional Learning Communities are formed at each grade level. A teacher leader is 
appointed to lead the PLC. Our Professional Learning Communities continue to study of the shifts in common core instruction 
and learning with a focus on lesson design for close reading, academic vocabulary development, student academic discourse 
and writing across the curriculum. We have implemented trimester benchmark assessments in mathematics and English 
Language Arts. 

 
 
 
 
 

2017-18 School Accountability Report Card for Kernville Elementary School 

Page 2 of 11