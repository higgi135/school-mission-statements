Vision Statement: 
Every student, Inspired to Learn, Empowered to Excel Every Day! 
 
Foothill High School (FHS) is a public, four-year comprehensive school, located in North Tustin, Orange County, California. Built in 1963, 
the campus sprawls over fifteen acres and is clean, attractive, and in good repair. Until 1991, most FHS students came from middle to 
upper-middle income families. To add diversity, enrich the experiences of all students, and balance enrollment, the Tustin Unified 
School District (TUSD) revamped attendance boundaries, resulting in an overall increase in attendance to a rate of 2,535 in 2016-17, 
thereby increasing cultural diversity and broadening the range of socioeconomic levels within the school. FHS student body includes 
32% socioeconomically disadvantaged, 6% English Learners, 44% Hispanic/Latino, and 42% White students. Of the 772 
socioeconomically disadvantaged students, 83% are Hispanic/Latino. 
 
During the 2018-19 school year, FHS is committed to the following improvement goals: 
 
Goal 1: By June 2019, FHS students’ academic achievement, as measured by the CAASPP in ELA and Mathematics, will demonstrate a 
3% increase of students meeting or exceeding standards for the spring 2019 assessment. Students in significant subgroups of 
Hispanic/Latino and socio-economically disadvantaged will increase performance by 5%. 
 
Goal 2: By June 2019, EL students’ academic achievement will demonstrate a 5% increase of students meeting or exceeding standards 
for the spring CAASPP assessment in ELA and mathematics. 
 
Goal 3: By June 2019, SPED students’ academic achievement will demonstrate a 5% increase of students meeting or exceeding 
standards for the spring CAASPP 2019 assessment in ELA and mathematics. 
 
Goal 4: FHS will actively participate in the Challenge Success Program to identify a Wellness solution to support all students. We will 
research solutions, revise practice, and implement the improvement surrounding student health and balance. 
 
Schoolwide, FHS has a rich tradition of excellence in academics, athletics, arts, and a robust career tech education program. We have 
over 20 AP and IB academic classes to choose from, CIF championship athletic programs, award winning art, and world ranked career 
tech ed opportunities.