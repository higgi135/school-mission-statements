Welcome to Madrid Middle School – home of the Bulldogs! 
 
VISION: 
Madrid Middle School is a place where staff, students, and families work together to develop 
students who achieve academic excellence and demonstrate social responsibility in a positive way. 
 
Mission: 
The Mission of Alfred S. Madrid Middle School is to prepare all students for college readiness and 
success in a global society. 
 
Mountain View School Districts Vision, Mission, and Core Values 
VISION Each member of the District will hold themselves accountable for each student to become 
a life-long learner. Each student will become a problem solver, critical thinker, an effective 
communicator and a positive contributor to their school community. 
 
MISSION 
Our Mission is: We are committed to each student attaining academic success. “Inspiring Each 
Student to Succeed Every Day” 
 
CORE VALUES 
“In all we do, we treat everyone with dignity and respect.” High Expectations: We have high 
expectations for each student and each adult in our District. We place no limits on students’ 
learning due to race, family income, gender identity, native language, or area of residence. 
Enrichment: We believe in supporting the needs of the whole child through enrichment activities. 
Family, Staff and Community Engagement: We engage families, staff and community members in 
supporting student achievement. Accountable: We hold ourselves accountable for making 
decisions in the best interests of our students. Honesty: We are open, honest and transparent in 
our communication. Fiscal Responsibility: We hold ourselves fiscally responsible to ensure we can 
achieve our mission. 
 

2017-18 School Accountability Report Card for Madrid Middle School 

Page 1 of 10 

 

Madrid has been serving grades six through eight in the community of El Monte since 1992-93. Teachers and staff are committed to high 
academic achievement for all students in a safe, nurturing learning environment. The Mission of Alfred Madrid Middle School is to prepare 
all students for college readiness and success in a global society. It is our goal at Madrid Middle School to challenge every student with 
rigorous and relevant curriculum that fosters relationships, supports student learning, promotes collaboration, and builds skills that will 
ensure students are college and career ready. Madrid is proud to be an AVID school offering 4 sections of AVID and also we are in the 
process of supporting the school-wide AVID model. Our commitment to students is reflected in our master schedule having diverse 
offerings in Language Arts and Math courses designed to address our students’ strengths and areas of need. This includes classes designed 
to assist newcomers, Honors students, and Special Education students. We also offer intervention programs in Language Arts and Math 
for students who are facing challenges. In pursuit of this goal, teachers have developed a matrix to ensure that students are taught the 
essential standards. Teachers meet in professional learning communities and data reflection meetings to monitor the progress of all 
students in their classrooms. Students receive additional support during the school day and in extended day opportunities. Students who 
meet academic standards are recognized at our trimester Renaissance Rally. Madrid is proud to partner with THINK Together, an after-
school program that supports the academic, social and emotional needs of the middle school student. The THINK Together program 
complements the school's academic program with enrichment activities and learning. THINK Together has shown to benefit students in 
their homework, academic performance, and attitude. 
 
Computer class, Robotics, video journalism, Art, AVID, award-winning marching band and axillary, Speak Up competition, Students Run 
Los Angeles, Sea Perch (underwater robotics program), Drama Club, Chess club, Coding club, are among the courses and activities that 
are offered to students at Madrid Middle School. Our school promotes the college for all students through the participation in the 
California GEAR UP and AVID program. 
 
The school promotes parental involvement in the educational process, offering comprehensive programs and guides for the benefit of 
both student and the parent. The local community and Madrid Middle School benefit greatly from their collaboration and commitment 
to each other. Parents and the community members are encouraged to participate in organized activities such as fundraising, tutoring, 
and chaperoning, as well as various committees and councils established to preserve the vital line of communication between the school 
and the community. 
 
Our goal in presenting you with the information within this report card is to keep our community well-informed. We want you to feel 
part of our “family” so if you have any questions or are interested in making an appointment to discuss this report, please call our school. 
If you wish to become involved in the school’s activities or simply volunteer to work in a classroom, please contact Madrid’s Community 
Liaison at (626) 652-4309. 
 

 

2017-18 School Accountability Report Card for Madrid Middle School 

Page 2 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Madrid Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

39 

37 

38 

0 

0 

0 

0 

0 

0 

Mountain View School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

383.8 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Madrid Middle School 

16-17 

17-18 

18-19