La Sierra High School (LSHS) was established in 2000 by the Tulare County Office of Education (TCOE) as a free, alternative public high 
school open to all students in the county and surrounding counties. La Sierra Charter School is an approved Western Association of 
Schools and Colleges and while military philosophy serves as a basis for discipline, respect/ decorum, the school is not affiliated with, 
and does not recruit for, the armed forces. 
 
Mission: 
Honor, Courage, and Academic Excellence. 
At La Sierra, we are committed to providing students with values that develop a sense of brotherhood, self-discipline, character, and 
respect for others. 
 
Our programs provide leadership to inspire, empower and lead others through promotions and honorary ranks. 
The school also provides service learning to develop personal honor and pride within the community and post-secondary pathways 
for students in pursuit of academic excellence and talents.