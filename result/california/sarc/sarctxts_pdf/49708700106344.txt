School and Program Description 
 
Relevant Curriculum 
 
The education experience at Northwest Prep for both students and teachers is contextual, active, and authentic. Project-Based work 
and field studies are the norm. Academic competencies are integrated with professional skills and guided by the Driving and Guiding 
questions developed by students and staff. Each semester’s long-term whole school project is driven by an over-arching Driving 
Question. The curriculum in all classes is carefully designed to scaffold and build student’s knowledge and abilities to be able to 
adequately and appropriately address this question and Guiding Questions through a series of formative benchmarks, and finally 
through summative, high stakes Academic Exhibitions. 
 
Personalized Learning 
 
At NWP every student is well known and respected. NWP’s enrollment of 107 students, and staff of 8 teachers, including the school’s 
Principal, are small by design. Students are blended in cohorts in grades 7/8, 9/10 and 11/12 for most classes, but during all long-term 
projects student “teams” include all grade levels 7-12. Teachers share responsibility for all of NWP’s students and work with them all 
over several years. Advisors serve as advocates for their students, and as a liaison between staff, students, and parents. Teachers also 
facilitate NWP’s Crossroads program. 
 
Authentic and Powerful Accountability 
 
Students, parents and staff use a broad range of approaches to measure academic and personal growth. The most important and 
valuable assessment is consistent conversation. This is the norm in all NWP classes. Conversation, facilitation, and mediation regarding 
student work and overall process occurs one-on-one, with small groups, and through seminars. Both students and teachers routinely 
examine progress and proficiency using systematic inquiry and reflection. Students employ personal web portals to compile and share 
digital portfolios, collaborate with peers on project benchmarks, post blogs, video conference, archive work, and post work for peer 
and teacher evaluation. All students participate in culminating Academic Exhibitions where they are evaluated by members of the 
entire school community. Prior to graduation each student must formally present and defend a multimedia portfolio of work and 
experiences that demonstrates evidence that they have achieved proficiency in Northwest Prep’s Leadership Skills (see below). 
 
A Community of Learners 
 
Northwest Prep’s students, staff, and parents are active participants in a truly collective endeavor focused on instilling in students a 
desire to develop successful habits of mind, and an appreciation for the importance of continuing education. We work hard to develop 
strong positive relationships and a culture of appreciation, trust, and respect within the entire school community. Students produce 
whole-school Community Meetings where they communicate important information, showcase student talents and accomplishments 
(music, poetry, athletics, etc.), and address issues they feel are important in town hall styled forums. 
 
 

 

2017-18 School Accountability Report Card for Northwest Prep Charter School 

Page 2 of 14 

 

Real World Immersion 
 
Though Northwest Prep is a small educational enclave, it is not an island. Students and staff work to regularly find opportunities for 
students to interact with other students, and successful adults, in the community and beyond. Northwest Prep encourages and 
accommodates students to take classes at Santa Rosa Junior College, and Sonoma State University, as well as online courses in areas 
that we can’t offer due to staffing constraints. In the course of their project work, students are required to interact with experts in the 
fields that they are exploring, and with professional mentors with whom they are communicating, job shadowing, or interning with in 
the Crossroads program. Students are also required to communicate through email or video-conferencing with students from other 
communities and even countries in order to gain diverse perspectives on project topics and issues. 
 
NWP’s Crossroads program guides students through multi-year college and career exploration, job shadows, and professional 
internships. All students are also taught professional public speaking and communication skills on an ongoing basis. The main goal of 
the program is to, over time, create individualized pathways for each NWP student. When students leave Northwest Prep, they leave 
with an informed plan for their future. Grades 7 and 8 focus on exploring their interests, abilities, and talents. Grades 9 and 10 focus 
on exploration of the vast range of careers and different types of colleges and training programs that prepare students for them. 
Grades 11 and 12 learn how to identify and secure professional internships and develop a plan to actively contribute in their 
placement. NWP links students to all levels and fields of the professional workplace. We provide students with the opportunity to 
apply the many practical benefits from their PBL experience, to provide a service to their mentors and their organizations, and to 
further develop their professional and academic skills. The Crossroads experience empowers student's ability to make informed 
choices for life after high school. 
 
NWP Leadership Skills are the foundation of the Northwest Prep program. They are the backbone of NWP’s school culture, curricular 
goals, and the basis for assessment of each student's progress and growth. When students leave Northwest Prep for targeted post-
secondary experiences, they leave proficient in all of the Leadership Skills. We know from our graduates that students who are truly 
proficient in these skills, will be successful no matter what path they choose after high school. 
 
Personal Integrity 
 
Students handle themselves with confidence and act with honesty and courage. They commit to their developing beliefs and are willing 
to assume roles as inquiring observers, active participants, and dynamic leaders. They demonstrate positive working relationships 
across diverse groups, accept personal responsibility for their actions, and remain open to learning from the feedback and guidance 
of others. 
 
Productive Collaboration 
 
Students develop and use the skills necessary to plan and engage in group projects. They work to resolve social and logistical conflicts 
and devise solutions to meet diverse needs. They collectively set goals and develop strategies to meet those goals. They evaluate the 
effectiveness of their approach and constructively adapt to new understandings as they arise. 
 
Critical and Creative Thinking 
 
Students identify problems and pursue opportunities from multiple perspectives. They locate, organize, analyze, and apply key 
information in inventive and imaginative ways. They design, evaluate, and employ a variety of strategies, tools, and skills to achieve 
innovative results. Students are independent, creative, and critical thinkers who question and connect to the world around them from 
both big picture and focused perspectives. 
 
Effective Communication 
 
Students understand and practice effective communication using verbal and nonverbal language with intent, awareness, and accuracy. 
They are empathetic, emotionally intelligent, persuasive, and articulate. They are skillful self-advocates who effectively communicate 
their needs. Students internalize and present their understandings and ideas with confidence and clarity. Employing a variety of media, 
they use practical, academic, and artistic abilities to convey meaning in a clear and engaging fashion. 
 
Reflective Learning 
 
Students excel at making critical observations about their own learning and potential. They formulate meaningful and relevant 
questions that inspire and encourage further inquiry. Students consistently take charge of their education by reflecting upon and 
revising their own practices. 

2017-18 School Accountability Report Card for Northwest Prep Charter School 

Page 3 of 14 

 

 
Citizenship and Global Responsibility 
 
Students are engaged and informed citizens. They are empowered to create positive change in themselves, their communities, and 
the world. They are mindful and principled decision makers who understand the long and short-term effects of their actions on others 
and the environment. They practice compassionate, ethical, and active citizenship in local, global, and virtual settings. Students strive 
to achieve balance between their own needs and the needs of others. 
 
Resiliency and Drive 
 
Students are adept at taking intelligent risks and view mistakes as necessary steps toward learning and growth. They consciously 
identify their intentions and desires. They possess the tenacity and determination to work individually and collaboratively. They are 
self-motivated and self-regulated. Northwest Prep students confront challenges and persevere through adversity. 
 
School Purpose and Expected School Wide Learning Results 
 
Northwest Prep Charter School is a small, personalized, rigorous learning community where students are deeply engaged in, and 
thoughtful about, their learning. Teachers know students well and guide them towards expected outcomes by teaching them to ask 
good questions, and identify and solve meaningful problems. All students participate in a standards-based, academically rigorous 
Workplace Learning Program that allows them to interact with successful adults, apply their talents to real world challenges, and 
exhibit their skills and problem-solving abilities. Northwest Prep features standards-based, project and problem-based instruction, 
integrated curriculum, advanced technology, visual and performing arts, and performance-based assessments. 
 
Upon graduation all NWP students are expected to have become highly proficient in all of the NWP Leadership Skills, present a 
Graduation Portfolio Defense, and be well-prepared for college and the workplace. 
 
Northwest Prep Expected Schoolwide Learning Outcomes (ESLR's) 
 
The Northwest Prep Leadership Skills have been adopted as the Expected Schoolwide Learning Results (ESLRs). Students are evaluated 
each semester on their growth in each of the Leadership Skills. In order to graduate from NWP, students must demonstrate evidence 
of proficiency, and receive an assessment of “capable” or “excellent” on all Leadership Skills in their Graduation Portfolio. 
 
NWP Academic Performance Outcomes 

• Read critically and write persuasively in the English language 
• Recognize and describe relationships and patterns mathematically to solve concrete and abstract problems 
• Apply scientific concepts and skills to solve problems, and use critical thinking skills to interpret scientific data 
• Demonstrate an understanding of historical, political, social, and economic issues from multiple perspectives 
• Use digital technologies appropriately as tools to enhance the achievement of academic and aesthetic goals and interpret, 

experience, create, and present original ideas and products