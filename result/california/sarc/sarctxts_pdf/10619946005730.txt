It is the mission of Alvina Elementary Charter School to recognize the value and spirit of each and every one of our students. With 
complete dedication, it is our goal to: 

• Promote Academic Student Success. 
• Recognize the Value of Each Child. 
• 
• Develop Student Learning to its Fullest Potential. 
• 
Enable Students to Develop A Love For Learning. 

Inspire a Partnership with the Home. 

Alvina Elementary Charter School is a small single-school district, serving a farming community, and located approximately 10 miles 
south of Fresno. Our name is reportedly derived from the two major crops grown near the original school site, alfalfa and vineyards. 
The Alvina District was established in 1912. We became a feeder school to Caruthers High School in 1914. Our present school site was 
erected in 1955. We converted to charter school district status in August 2000. We believe that our isolation from neighboring 
residential and commercial activities promotes our sense of school community, provides an atmosphere where teachers and students 
feel safe and comfortable, and fosters pride in our school grounds and buildings. 
 
We provide educational services to students enrolled in transitional kindergarten through eighth grade. We have ten certificated 
teachers on staff as well as twelve instructional aides, and four support staff members. We pride ourselves on our student to staff 
member ratio. It is our goal to maintain our small classroom averages. 
 
Our goal at Alvina Elementary Charter School, in partnership with our community, is to deepen student knowledge, develop a strong 
sense of core ethical values, and to provide students with the skills needed to become lifelong learners. We maintain our focus on our 
students’ academic development, and we strive to help each student grow emotionally, physically, and socially.