Monte Vista Elementary School is located at the corner of West Monta Vista Avenue and South 
Townsend Street. We serve students from the community in TK-5 and offer four Head Start 
classrooms, offering full and half day programs. 
 
School Mission: 
Monte Vista Elementary School staff, parents, and community are committed to developing a 
positive school culture wherein we will collaborate to provide a well-rounded, rigorous learning 
experience utilizing high quality curricular and instructional programs that will prepare our students 
for college and career. We are focused on raising the achievement of all students as we engage, 
inspire, and challenge them to become productive and positive citizens and ethical leaders in the 
21st century. 
 
School Vision: 
The vision of the Monte Vista school staff and parents is to work together to establish an excellent 
foundation for life-long learning, while developing respectful, responsible citizens that are 
prepared to make positive contributions to the local and global community. 
 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently 
serving nearly 49,300 students in grades K-12, residing in the city of Santa Ana. As of 2017-18, 
SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 alternative high 
schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or 
Reduced Price Meal program, 39% qualifying for English language learner support, and 
approximately 13% receiving special education services. Our district’s schools have received 
California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I 
Academic Achieving Schools, and Governor’s Higher Expectations awards in honor of their 
outstanding programs. In addition, 20 schools have received the Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have 
developed and maintained high expectations to ensure every student’s intellectual, creative, 
physical, emotional, and social development needs are met. The district’s commitment to 
excellence is achieved through a team of professionals dedicated to delivering a challenging, high 
quality educational program. Consistent success in meeting student performance goals is directly 
attributed to the district’s energetic teaching staff and strong parent and community support. 
 

 

 

----

---- 

----

---- 

Santa Ana Unified School District 

1601 East Chestnut Avenue 
Santa Ana, CA 92701-6322 

714-558-5501 
www.sausd.us 

 

District Governing Board 

Valerie Amezcua – Board President 

Rigo Rodriguez, Ph.D. Vice – 

President 

Alfonso Alvarez, Ed.D. – Clerk 

John Palacio – Member 

 

District Administration 

Stefanie P. Phillips, Ed.D. 

Superintendent 

Alfonso Jimenez, Ed.D. 
Deputy Superintendent, 

Educational Services 

Thomas A. Stekol, Ed.D. 
Deputy Superintendent, 
Administrative Services 

Mark A. McKinney 

Associate Superintendent, Human 

Resources 

Daniel Allen, Ed.D. 

Assistant Superintendent, K-12 

Teaching and Learning 

Mayra Helguera, Ed.D. 

Assistant Superintendent, Special 

Education/SELPA 

Sonia R. Llamas, Ed.D., L.C.S.W. 
Assistant Superintendent, K-12 
School Performance and Culture 

Manoj Roychowdhury 

Assistant Superintendent, Business 

Services 

Orin Williams 

Assistant Superintendent, Facilities 

& Governmental Relations 

 

2017-18 School Accountability Report Card for Monte Vista Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Monte Vista Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

24 

24 

21 

0 

0 

0 

0 

0 

0 

Santa Ana Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1986 

0 

11 

Teacher Misassignments and Vacant Teacher Positions at this School 

Monte Vista Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.