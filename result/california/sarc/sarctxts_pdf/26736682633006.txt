Principal's Message 
 
I'd like to welcome you to Coleville High School's Annual School Accountability Report Card. In accordance with Proposition 98, every 
school in California is required to issue an annual School Accountability Report Card that fulfills state and federal disclosure 
requirements. Parents will find valuable information about our academic achievement, professional staff, curricular programs, 
instructional materials, safety procedures, classroom environment, and condition of facilities. 
 
Coleville High School provides a warm, stimulating environment where students are actively involved in learning academics as well as 
positive values. Students received a standards-based, challenging curriculum by dedicated professional staff and based on the 
individual needs of the students. Ongoing evaluation of student progress and achievement helps us refine the instructional program 
so students can achieve academic proficiency. We have made a commitment to provide the best educational program possible for 
Coleville High School's students and welcome any suggestions or questions you may have about the information contained in this 
report or about the school. Together, through our hard work, our students will be challenged to reach their maximum potential. 
 
Mission Statement 
 
The mission of ESUSD, a community ascending from varied pasts toward future promise, is to ensure that each student achieves 
academic and personal excellence, through a unique educational system distinguished by: 

student-centered learning environments with no boundaries 

• 
• devoted and passionate staff 
• 
• 
• 

engaged partnerships within our communities 
technology that bridges the gaps with all communities and the world 
innovative risk in a secure environment 

School Profile 
 
Coleville High School is located in the northern region of Coleville and serves students in grades nine through twelve following a 
traditional calendar. At the beginning of the 2017-18 school year, 62 students were enrolled, including 16% in special education, 17% 
qualifying for English Language Learner support, and 53% qualifying for free or reduced-price lunch. Students were assessed with the 
California Smarter Balanced Assessment Test, and California reports the following test scores for 11th grade: 
 
2018 Smarter Balanced Assessment Test 

• ELA: 116.5 Points from Level 3 
• Math: 0 Points from Level 3 

2017 Smarter Balanced Assessment Test 

• ELA: 63.64% Standard Exceeded, 27.27% Standard Met, 9.09% Standard Nearly Met 
• Math: Scores were not reported--In order to protect student privacy, an asterisk (*) will be displayed instead of a number 

on test results where 10 or fewer students had tested. 

2016 Smarter Balanced Assessment Test 

• ELA: 53% Standard Exceeded, 40% Standard Met, 7% Standard Nearly Met 
• Math: 0% Standard Exceeded, 27% Standard Met, 40% Standard Nearly Met, 33% Standard Not Met 

2015 Smarter Balanced Assessment Test 

• ELA: 19% Standard Exceeded, 63% Standard Met, 19% Standard Nearly Met 
• Math: 6% Standard Exceeded, 13% Standard Met, 25% Standard Nearly Met, 56% Standard Not Met 

2017-18 School Accountability Report Card for Coleville High School 

Page 2 of 12