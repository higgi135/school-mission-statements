Principal's Message 
Needles Middle School serves grade 6-8 students residing in and around the City of Needles. The current student body hovers around 
255 students. Many students travel 50 or more miles each way to attend school. Needles Middle School consists of eleven teachers, 
two teachers shared with Needles High School, and four support staff. The teaching and support staff work diligently toward the goal 
of academic success for each and every student. Due to our small size, we are able to cater to the diverse needs of all our students. 
Needles Middle School has fully implemented the AVID program and the school was awarded recognition by the California Department 
of Education and The Middle Grade Alliance, for enduring commitment to academic excellence and continued achievement a middle 
school. 
 
Needles Middle School has implemented "Rachel's Challenge" to support a kind, caring school community. As principal, I am 
committed to raising student achievement through meeting with the teachers during their Professional Learning Community time, 
meeting with departments, and meeting with each staff member after each benchmark assessment to see how the teacher could 
better implement their teaching strategies. 
 
School Mission Statement 
Needles Middle School is committed to offering every student equal access to educational opportunities in a safe and orderly 
environment. We will continually seek to improve the quality of school life and instruction with the long-range goal of helping each 
individual student become a responsible and productive lifelong learner in a global society. 
 
Needles Middle School Vision Statement: 
The Needles Middle School staff and school community are committed to equipping all students with the skills and knowledge 
necessary to be academically proficient. We will ensure our students are prepared to successfully engage in 21st Century College and 
Career Pathways. 
 
The mission of the Needles Unified School District is to provide a free and appropriate education enabling all students to be successfully 
prepared to be productive members of society. Our core values reflect that "All Students Will Learn" and "Education First"