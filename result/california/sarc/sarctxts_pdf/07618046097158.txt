Welcome to Montevideo Elementary. It is our vision to build a community of learners who are eager 
and excited to learn, grow and think critically. We do our best to create a welcoming environment, 
where parents want to partner with us, to provide their students the very best education possible. 
 
Here at Montevideo we firmly believe: 
 
Every child deserves a free and top quality education. 
 
ALL students can learn when learning is relevant. 
 
The Common Core Standards provide equity, opportunity, access to higher level thinking and a 
rigorous education for all. 
 
Character development and rigorous academics are equally important in preparing our students 
for secondary school, college and the workforce. 
 
Students flourish when education includes art, music, science and social science. 
 
Parent participation is key to student success. 
 
Education leads to advancement and provides choices for all students. 
 
I look forward to a productive and collaborative year with staff, parents and students. 
 
Mission Statement 
 
At Montevideo School we are committed to providing success for all students: 

• A safe and nurturing learning environment, with respect and encouragement for 
individual differences and opportunities to learn the skills necessary to become caring 
and responsible global citizens. 

• Academic excellence 

in a rigorous standards-based curriculum, differentiated 

according to student need. 
Success for each child, through collaboration between home and school. 

• 
• Opportunities for each child to discover a love of learning based on their own 

intellectual, artistic, and athletic skills and interests. 

 

----

--

-- 

San Ramon Valley Unified School 

District 

699 Old Orchard Dr. 
Danville, CA 94526 

(925) 552-5500 
www.srvusd.net 

 

District Governing Board 

Ken Mintz, Board President 

Rachel Hurd, Board Vice President 

Greg Marvel, Board Clerk 

Mark Jewett, Board Member 

Susanna Ordway, Board Member 

 

District Administration 

Rick Schmitt 

Superintendent 

Toni Taylor 

Deputy Superintendent, 

Educational Services 

 

Keith Rogenski 

Assistant Superintendent, 

Human Resources 

 

Gregory Medici 

Chief Business Officer 

Business Office 

 

Gary Black 

Assistant Superintendent, 

Facilities & Operations 

 

 

2017-18 School Accountability Report Card for Montevideo Elementary School 

Page 1 of 9 

 

School Profile 
 
Montevideo is located in a cheerful suburban neighborhood of San Ramon where families and local businesses support their schools. 
Attached to the beautifully kept enclosed campus is a large sprawling community park with a baseball diamond and grassy field used for 
soccer and other sporting practices and organized games. The location provides a perfect setting for the 655 Montevideo Transitional 
Kindergarteners through fifth graders. Our campus has 26 classrooms plus a few portables and another large portable building, which 
houses Kids Country, our onsite after and before school care program. Montevideo also has a multipurpose room, library, science lab, 
garden, computer lab and several smaller rooms used for small group instruction. 
 
 At Montevideo Elementary School every child is provided this fantastic physical space for learning, but more importantly, they are also 
provided with a metaphorical canvas to explore their individual passions and interests in a safe and nurturing environment. It is our goal 
to deliver rigorous curriculum, which allows for individual acceleration and encourages creativity and critical thinking. We believe strongly 
that every child will achieve success through great collaboration amongst our teachers, staff, the principal, students and their families. 
 
 Montevideo’s demographic is comprised of great diversity with several distinct ethnicities including South and East Asian, Hispanic, 
African American, African, Caucasian, Pacific Islander and Native American students. Many of our students are learning English as a second 
or even third language. Our students and families speak over 15 different home and school languages. This cultural diversity amongst our 
students makes our learning community all the richer and provides students the opportunity to learn from each other about different 
cultures and ideas. We also strongly believe in the need for differentiated learning and Response to Intervention to meet the needs of 
every unique student on our campus. We provide various interventions and programs to meet the needs of our students including GATE, 
Special Education, reading, math, English Language Learner Intervention and Social Emotional groups and counseling to support individual 
student learning. 
 
 The staff at Montevideo is committed to providing a holistic approach to student learning, which includes Common Core Standards based 
curriculum. Every classroom offers a balanced literacy language arts program, which includes Columbia’s Readers and Writer’s Workshop, 
Fountas and Pinell word work and spelling program and/or Words Their Way word work and spelling. All teachers use guided reading in 
their classrooms to meet the needs of their diverse learners. They also supplement their Language Arts blocks with GLAD- Guided 
Language Acquisition Design, and other various methods and materials to enhance the learning environment. Our teachers are also 
immersed in the Silicon Valley Math Initiative, Marcey Cook strategies, Making Math Real, Singapore Math and other problem solving-
based methodologies to enhance the current adopted math curriculum. In addition to our rigorous and balanced ELA and Math curriculum, 
Art, Science, Social Studies and Music are integrated daily. We also implement culturally relevant teaching strategies in order to make all 
subject matter engaging and relevant to all students. 
 
 It is obvious from the moment you step onto our campus that the school culture is one of positivity. From our Mustang Motto: C.A.R.E. 
(Cooperation, Accountability, Respect, Empathy), Sanford Harmony social-emotional curriculum and staff work with mindfulness, anyone 
can feel the joy of teaching and learning from the minute you walk onto the campus. We welcome anyone to join us at the home of the 
Mustangs.