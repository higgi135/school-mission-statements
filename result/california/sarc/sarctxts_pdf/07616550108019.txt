Message From Principal 
At Marsh Creek Elementary we take pride in our strong community spirit, our instructional practices 
designed to meet the needs of all students, and our rich list of student programs. Marsh Creek is a 
place where teacher commitment to student success is high, where diversity is valued, where 
students love learning, and where achievement is realized for parents, teachers, and students! 
 
The Marsh Creek Community is committed to: 

• Having high expectations for academics and maximizing student achievement for ALL 

students; 

• Appreciating the development of the whole child; 
• Working with parents, community, and staff to create a collaborative culture; 
• Keeping students highly engaged and motivated to maximize learning time; 
• 

Structuring a safe, supportive environment that fosters respect, inspires life-long 
learning, and develops a caring, civic-minded community 

 
District Mission Statement 
The Brentwood Union School District will provide an exemplary education to all children in a safe, 
student-centered environment designed to nurture the whole child and partner with families to 
prepare globally productive citizens. 
 

-------- 

 

 

----
---- 
Brentwood Union Elementary 

School District 
255 Guthrie Lane 

Brentwood, CA 94513-1610 

(925) 513-6300 

www.brentwood.k12.ca.us 

 

District Governing Board 

Carlos Sanabria 

Emil Geddes 

Jim Cushing 

Steve Gursky 

Scott Dudek 

 

District Administration 

Dr. Dana Eaton 
Superintendent 

Robin Schmitt 

Chief Business Official 

Roxanne Jablonski-Liu 

Assistant Superintendent, Human 

Resources 

Michael Bowen 

Director - Curriculum & Instruction 

Robert Brown 

Director - Maintenance & 

Operations 
Assistant 

 

Chris Calabrese 

Director - Student Services 

Jeff Weiss 

Director - Special Education 

 

2017-18 School Accountability Report Card for Marsh Creek Elementary School 

Page 1 of 8