School Vision Statement: 
 
Orangeview will provide a safe, modern, and equitable learning environment built on positive relationships where staff, students, 
parents, and community collaborate to design innovative instruction that prepares students to be productive community members 
who are College and Career Ready. 
 
Highlights: 
 
Orangeview Junior High School has developed programs to meet the diverse needs of students through course offerings. For example, 
we provide a full selection of honors courses for advanced students, literacy support and interventions for struggling students, and 
we fully integrate many of our special education students into collaboratively taught Math and English classes. 
 
In addition to the specialized courses, our counseling department offers a comprehensive list of support services to assist students 
through the academic and social challenges of junior high school. We provide one-on-one meetings, group sessions for anger 
management, organizational skills, grief, and we partner with outside agencies to connect students and families with additional 
support. 
 
At Orangeview Junior High School we are committed to: 

• A coordinated instructional program in which teachers collaborate to ensure all students learn 
• A systematic response to students’ academic and social needs 

So that all students can: 
 
 1. Collaborate 
 
 2. Communicate 
 
 3. Be Creative 
 
 4. Think Critically 
 
We strive to equip students with 21st-century skills that are needed for college and careers. 
 
 

 

2017-18 School Accountability Report Card for Orangeview Junior High School 

Page 2 of 11 

 

General Information: 
 
Orangeview Junior High School offers the following courses, which meet University of California A-G requirements: Spanish 1, Spanish 
Speakers 1, Spanish Speakers 2, and Japanese 1. Currently, 211 students are enrolled in these courses. We also offer Advancement 
Via Individual Determination (AVID), in which 160 students are enrolled for the 2018-19 year. Orangeview Junior High School offers 
honors classes in English language arts (ELA), mathematics, history, and science. Seven hundred and forty-nine students are enrolled 
in Visual and Performing Arts (VAPA) classes, and 200-to-250 students are participating in intramural sports each quarter. Orangeview 
Junior High School has Career and Technical Education (CTE) course offerings that lead to pathways at the high school level, such as 
Wood Manufacturing, Exploring Technology, Business Technology, and Multimedia Production, in which 373 students are enrolled. 
Orangeview Junior High School also hosts the GATE Orchestra program for all students in the District. Orangeview Junior High School 
offers the following support programs to help close the achievement gap: ELA and English Language Development (ELD) support 
classes, inclusion classes for students in the Resource program, Multi-Tiered Systems of Support (MTSS) framework of interventions, 
Parent Partnership Nights, a parent center, after-school tutoring, individual and group counseling, Backpack Rescue, Positive 
Behavioral Intervention and Supports (PBIS), and a variety of clubs. Orangeview Junior High School has partnered with community 
programs such as Anaheim Achieves, Gang Reduction and Intervention Program (GRIP), Tiger Woods Learning Center, West Anaheim 
Youth Center, AmeriCorps, and Casa Youth Shelter to provide services for our students. Orangeview Junior High School has an 
extensive rewards system for all students, including Student of the Day, Student of the Month, Panther Pride Awards, PRIDE Bucks, 
Honor Roll and High Honor Roll, Growth Awards, Perfect Attendance, Panther success stickers, and Student of the Year awards. 
 
Demographic Information: 
 
Orangeview Junior High School, located in Anaheim, California, serves 862 students, in which 81% participate in the free and reduced 
meal program, and 31% are English Learners. The demographic profile also indicates the following regarding student subgroups: 68.1% 
Hispanic, 10.3% Asian, 8.3% White, 5% Filipino, 4% African American, and, 3.3% Pacific Islander/ Native American, and 1.1% two or 
more races.