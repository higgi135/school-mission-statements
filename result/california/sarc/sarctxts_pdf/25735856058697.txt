At Modoc Middle School, we provide sixth, seventh, and eighth grade students with a strong district-adopted curriculum in all subject 
areas. We are concentrating on improving students’ achievement through a variety of programs, including AVID, Accelerated Reader, 
before school, at lunch and after-school tutoring, ELA intervention, and a core support class. Our staff welcomes parent input and 
communicates with parents regularly via the phone, email, and student planners. Our 30-station computer lab, I-Pad cart and 4 
Chrome Book Carts are running well and we are focusing on integrating technology in all curricular areas. All core curriculum 
classrooms have interactive Smart Board technology. Every year we use Title I funds to pay for instructional aides, who provide 
individual assistance in core curriculum. These para-professionals are also available to students before school, at lunch, and after 
school for extra tutoring and to provide a quiet place to study. 
 
Buildings 
 
Modoc Middle School was built in 1929 and expanded in 1951 and 1959. Energy efficient remodeling was done in 1986. The school 
has two wings. The east wing has three classrooms in use for 6th graders. The west wing has three classrooms, a computer lab, a 
library, and a science lab. There are several outside classrooms which house core classes along with art, music and physical education. 
Special education services are provided in a classroom in the west wing and also an outside classroom. There is a multipurpose room 
equipped with a kitchen where food for the whole district is prepared. This area is also used as a cafeteria, sports/assembly area, and 
a stage. The office complex is in the west wing. Student restrooms are in both wings. The playground provides basketball hoops and a 
large area of asphalt, as well as large grassy areas for play and sport. Our campus is clean, attractive, safe, and well-maintained. 
 
Library 
 
Modoc Middle School library has recently received a facelift and acts as a student lounge during break. The library is housed in one 
room that also serves as a meeting and tutoring room. Students are scheduled to visit the library once a week during their 
English/Language Arts classes. The library’s hours vary with the availability of the librarian, who also runs the library at Alturas 
Elementary School. The librarian runs a book club for the middle school students. 
 
Computers 
 
Modoc Middle School has a permanent computer lab, an ipad cart, 4 Chrome Book Carts and 7 chrome books to be used in 
Intervention. Staff members are incorporating iPad2’s and educational apps into their lessons. Students and staff use a variety of 
software applications and online recourses daily, including Microsoft Word, PowerPoint, Excel, Google Classroom, Google sheets, and 
Kahoot. The Accelerated Reader program encourages students to read independently. All teachers use the district-wide networked 
Aeries program for attendance and grading. 
 
Homework 
 
Homework provides an opportunity for students to learn responsibility and practice the concepts they’ve learned in class. Each student 
is given an AVID binder that includes a student planner, dividers, pencil pouch and paper at the beginning of the school year. Teachers 
avoid assigning work on Fridays unless the student has makeup work to complete. Students and parents have access to the Aeries.net, 
a web portal that allows students and parents to check student progress and grades. Teachers post the week’s assignments online. 
Modoc Middle School provides before-school, lunch, and after-school tutoring (8:00 – 8:20 a.m., 12:00-12:25 p.m., and 3:00 – 3:30 
p.m. in the afternoon). 
 
 
 

2017-18 School Accountability Report Card for Modoc Middle School 

Page 2 of 10