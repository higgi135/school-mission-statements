San Bernardino High School has 97 classrooms, a library, an auditorium, a multipurpose room, a gymnasium, a counseling office, and 
an administration office. The campus was built in 1929 and modernized in 1993 and 2013. Five new special education classrooms were 
built in the 2006-07 school year. The facility strongly supports teaching and learning through its ample classroom and athletic space, 
and a staff resource room. 
 
San Bernardino High School will provide a caring and supportive learning environment based on fair, firm, and consistent policies, 
practices, and student-centered programs.