Stine School’s name originated from a local farmer, Phillip A. Stine. Mr. Stine, one of the first men to farm the area, was instrumental 
in the construction of the Stine Canal. Later Stine Road was built along the course of the canal and was named after the canal. Stine 
Elementary School, opened in 1900 and later built at its present site in 1957. It was named for the nearby road. Stine Elementary 
School, supports and strives to maintain the concept of “Excellence in Education” as defined by the Panama-Buena Vista Union School 
District. Our teachers strive to ensure student learning is evident. We believe this attitude pays off in student test scores and 
achievement. The School Accountability Report Card was established by Proposition 98, an initiative passed by California voters in 
November 1988. The Report Card, to be issued annually by local school boards for each elementary and secondary school in the state, 
provides for parents and other interested people a variety of information about the school, its resources, its successes, and the areas 
in which improvements are needed. 
 
Stine Elementary School received LCFF and Title 1 fund from the federal government. The Title 1 funds strengthened our Language 
Arts and Mathematics Programs, utilizing intervention practices in grades kindergarten through sixth. Our LCFF funds were used to 
assist identified English Language Learners, while other monies enhanced the library and media program. Money from LCFF fund 
various programs, projects, and materials and partially fund our Intervention Specialist and various instructional aides. 
 
Stine's purpose statement is, creating an atmosphere of academic, social, and behavioral excellence for all. A standards-based 
curriculum and sound assessment strategies provide the foundation for continuous improvement of student performance and school 
programs. Students will leave Stine Elementary School with a positive attitude toward learning, a strong foundation in basic skills, and 
the ability to meet future academic challenges.