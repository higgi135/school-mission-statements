School Description Alta Elementary School opened in 1955 at its current site and is situated midway 
between Reedley and Orange Cove. The school serves approximately 360 students in grades TK-5 
during the 2017-18 school year and includes a staff of 15 teachers. Alta Elementary School teachers 
and staff are dedicated to ensuring the academic success of every student and providing a safe and 
productive learning experience. 
 
School Mission Statement 
The mission of Alta Elementary School is to provide excellence in education to a diverse community 
through exemplary programs, service, and activities that foster a life-long commitment to academic 
and character development. 
 

 

 

----

---- 

----

---- 

Kings Canyon Joint Unified 

School District 
1801 10th Street 
Reedley, CA 93654 

559.305.7010 

www.kcusd.com 

 

District Governing Board 

Craig Cooper 

Robin Tyler 

Manuel Ferreira 

Noel Remick 

Sarah Rola 

Clotilda Mora 

Jim Mulligan III 

 

District Administration 

John Campbell 
Superintendent 

Roberto Gutierrez 

Deputy Superintendent, Human 

Resources 

Monica Benner 

Assistant Superintendent, 
Curriculum and Instruction 

Mary Ann Carousso 

Administrator, Student Services 

Jose Guzman 

Administrator, Educational 

Programs 

Adele Nikkel 

Chief Financial Officer 

 

2017-18 School Accountability Report Card for Alta Elementary School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Alta Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

17 

15 

16 

0 

0 

0 

0 

1 

0 

Kings Canyon Joint Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

422 

38 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Alta Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.