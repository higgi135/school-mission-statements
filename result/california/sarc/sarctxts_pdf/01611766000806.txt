Welcome to Patterson Elementary School, where we are committed to providing an excellent 
educational program to allow all students the opportunity to reach their fullest potential. At 
Patterson we take this charge seriously and work collaboratively to support all children achieving 
success through an innovative curriculum and instructional strategies. The school was first built in 
1964 with three main buildings. A few years ago, a shelter was added to provide additional space 
for students to eat lunch. In 2017, Patterson opened the first new building in 43 years. The building 
is beautiful and has eight gorgeous classrooms. It's a two story building with the most advanced 
technology on campus. It is equipped to house up to 240 students of 4th through 6th graders with 
a ratio of 30 to 1. However, it is housing 4 second grade classes with 24 to 1 and 4 third grade 
classes of 28 to 1 for a total of 208 students. The 2018-19 school year has seen an increase in 
student population. At capacity we can house approximately 900 students but are currently around 
785. We have the following classes 2 TK, 5 Kinder, 4 first grade, 5 second, 4 third, 4 fourth, 4 fifth, 
3 sixth, and 2 SDC. Our staffing includes a speech therapist, resource teacher, psychologist, 
counselor, school nurse, a library media specialist, one computer specialist, one science specialist, 
1 full time P.E teacher, 1 .8 P.E.teacher. We are supported by an office secretary, an office assistant, 
1 a.m. custodian and 2 p.m. custodians. There are a host of parent volunteers and 9 noon 
supervisors to provide supervision during our student lunches. Together, we are working to ensure 
that every child has the opportunity to succeed and reach their fullest potential. We have 
implemented The Eight Great Traits Program and we hold weekly morning assemblies in which we 
reinforce the trait of the month. We believe that recognizing our students' efforts will motivate 
them and encourage them to continue to reach success. We have monthly school-wide character 
assemblies and quarterly recognition assemblies. Our students can also participate in basketball 
and soccer during the year as we take part of the districts' sport leagues. We strive each and every 
day to provide our students with a quality education which will prepare them to be global citizen. 
 
We have an active PTA that works with the teaching staff in a collaborative forum to bring high 
interest and motivating assemblies to our school. The PTA sponsors hands-on, curriculum-based 
field trips for each grade level. They provide additional support to each teacher, who is a member 
of our PTA, through a monetary donation. Students are afforded the opportunity to participate in 
array of different activities during the school day. PTA also sponsors our fall and winter festivals, 
Panther Prowl fundraiser, Make A Difference Day, and teacher and staff recognitions. They are 
working to ensure that in three years every Patterson student will have a technology device. Our 
partnership helps move our school forward and provides diverse learning opportunities. 
 
Mission Statement: Patterson Elementary provides a challenging and innovative learning 
environment where students have the opportunity to develop their potential as: (1) creative 
problem solvers, and (2) compassionate leaders and change makers. Based on students' needs, 
researched best practices and results from assessments and surveys, differentiated instruction, 
students will improve their academic achievement in an innovative and collaborative environment. 
Students will be empowered to advocate for their own education and be active participants in their 
academic, emotional and social growth in preparation to meet challenges of living in a fast ever 
changing world. Mutual respect for our culturally diverse community is established and ongoing 
through effective communication between families, students and staff. 
 

2017-18 School Accountability Report Card for PATTERSON ELEMENTARY SCHOOL 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

29 

35 

36 

0 

0 

0 

0 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1713 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

0 

0 

0 

0 

0 

0 

0 

0 

Vacant Teacher Positions 
Notes: 
1) “Misassignments” refers to the number of positions filled by teachers who lack 
legal authorization to teach that grade level, subject area, student group, etc. 
 
2) Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners. 
 

0