Paramount Elementary implements programs that help every child achieve academic success. 
During our daily, school-wide intervention time, we provide one hour of instruction specially based 
on each individual student’s needs. Students receive specific interventions to remedy any literacy 
deficiencies. W e also provide our benchmarked students with a challenging curriculum. 
 
After school tutoring is offered in math and language arts to students needing extra support. W e 
also address student’s social, behavioral, cultural and physical development to ensure a w ell-
balanced school program. W e believe a strong home-school connection is important to provide 
the best education possible. Parents are welcome on our campus and are encouraged to volunteer 
and attend special events throughout the year. W e aim to communicate effectively with families 
at all times. 
 
Parents make a difference at Paramount! Leadership at Paramount Elementary School is a 
responsibility shared among district administration, the principal, instructional staff, students, and 
parents. Beginning in the 2017-2018 school year, primary leadership duties w ere assumed by 
Principal Antonio Flores. Mr. Flores has 24 years of experience in education in the following 
positions: instructional aide, teacher, resource teacher, assistant principal and principal.T he 
principal oversees the day-to-day operations of the school. 
 
Coordinating with the principal is the Leadership Team, which meets bi-monthly to focus on 
instructional issues.Teachers meet as Grade-Level Teams to align instruction to State standards. 
Staff members and parents participate on various committees that make decisions regarding the 
priorities and direction of the educational plan to ensure instructional programs are consistent with 
students’ needs and comply with district goals. These committees include English Learners Advisory 
Committee (ELAC), and School Site Council (SSC). 
 

2017-18 School Accountability Report Card for Paramount Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Paramount Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16 

16 

16 

0 

0 

0 

0 

0 

0 

Azusa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

441 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Paramount Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.