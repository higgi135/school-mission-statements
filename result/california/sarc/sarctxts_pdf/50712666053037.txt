Vision: 
We create a learning environment that fosters academic excellence and creativity in children, and empowers every student to succeed 
and to become a responsible and caring global citizen. 
 
Mission: 
To empower every student to achieve their personal best we endeavor to maintain a strong and active partnership between family, 
school, and community. We provide a safe and supportive learning environment, and we have high expectations of staff, families, and 
students and of our community. 
We hold firm to the belief that all students can learn at high levels. Teachers, administrators and staff are committed to the continuous 
improvement of classroom instruction and intervention practices to advance student performance. Our culture builds the capacity of 
teachers, staff, students and administrators by fully implementing the District's Four Pillars of professional practice as they relate to 
pedagogy, leadership, and organization. These pillars are 1) High Expectations for All, 2) Standards-Aligned Differentiated Instruction, 
3) Professional Collaboration and Accountability, and 4) Safe Climate and Strong Relationships. Our goal is to create a culture where 
teachers, staff, and administration work interdependently to address the following four: 
1. What do we want students to learn (concepts and skills)? 
2. How will we know if they learned (formative assessments and checking for understanding)? 
3. What will we do when they do not learn (re-teaching, response to intervention, differentiated instruction)? 4. What will we do when 
they do learn (enrichment, differentiated instruction)? 
To ensure that we are meeting the needs of all students, Salida Elementary School staff consistently monitor student learning and 
instructional and leadership practices. Site staff schedule time to review and monitor student achievement data and identify effective 
learning intentions and lesson design. 
 
Salida Elementary staff provides a well-rounded curriculum so that every learner can build on their strengths and interests.