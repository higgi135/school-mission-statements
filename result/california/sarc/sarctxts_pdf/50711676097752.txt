Lakewood School is situated in the eastern suburban section of the City of Modesto in Stanislaus County. Built in 1978, it is one of the 
22 elementary schools in the District and is composed of 20 classrooms. Centrally located within a neighborhood of middle-income 
homes, the school serves students from kindergarten through 6th grade.€ It is the aim of Lakewood School to encourage and assist 
every student to: 
1) Acquire basic skills to function in daily life and pursue educational interests as productive and worthy citizens; 
2) Acquire skills and attitudes to deal effectively with other students in school and social experiences; 
3) Develop talents that will assist them in improving recreational interests and help improve their self esteem and self-image. 
4) Envision education as a lifelong pursuit which brings intellectual fulfillment. 
5) Develop multi-cultural understanding and appreciation among the students from all backgrounds with and without disabilities