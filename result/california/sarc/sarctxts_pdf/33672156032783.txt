Our mission is to educate the whole child to succeed in a global society. To this end, every child will be provided with quality 
instructional experiences that recognize, support and maintain high expectations for all students. 
 
 
 
Washington Elementary is one of the district's seven schools that has both an English Language Instructional program as well as a Dual 
Language Immersion program for Spanish and English. In addition, Washington is known for its commitment to the arts with art 
lessons for all classrooms, a band program for fifth through sixth and a strings program at fourth through sixth grades.