Description of District 
The Visalia Unified School District is the oldest school district in Tulare County. The district is comprised of 25 elementary schools, 5 
middle schools, 4 comprehensive high schools, 1 continuation high school, 5 charter schools, 1 adult school, and a school that serves 
orthopedic handicapped students. Over 32,000 students Pre-K to adult are served through the Visalia Unified School District. 
 
Description of School 
Crestwood Elementary School served approximately 640 students in grades TK-6 in 2017-18. Our teachers and staff are dedicated to 
ensuring the academic success of every student and providing a safe and productive learning experience. The school holds high 
expectations for the academic and social development of all students. Curriculum planning, staff development, and assessment 
activities are focused on assisting students in mastering the Common Core academic content standards, as well as increasing the 
overall student achievement of all subgroups. 
 
School Mission Statement: 
Our mission is to promote high academic achievement, maintain a safe and respectful learning environment and to foster a strong 
collaborative partnership between school, parents and community. 
We believe that with exploration, discovery, and knowledge, the possibilities for our students' futures are endless. 
 
We will achieve this by ensuring that students: 

 will encounter a challenging and interesting curriculum based on common core standards 
 will experience a variety of instructional strategies including the use of technology to enhance learning opportunities 
 will build high self-esteem through success in personal achievement based on a foundation valuing hard work, perseverance, 

trustworthiness, caring, responsibility, respect, fairness, and citizenship.