Frank West School is one of 43 schools in the Bakersfield City School District. Built in 1958, Frank West is located in a south-central 
neighborhood that is over 60 years old and provides transportation to over 300 students in grades Kindergarten through 5th. We have 
approximately 799 students. An elementary instructional program in traditional, English immersion, and Special Education classes 
services our students. Frank West School has a full-time Principal, Vice Principal, Academic Program Leader, Academic Coach, Behavior 
Intervention Specialist, Family and Community Engagement Specialist (FACE), 9 Special Education teachers, and 32 full-time 
credentialed teachers who teach in self-contained classrooms. All K-3 classes average 21.5 students, with 5 kindergarten classes 
providing an extended day schedule. Frank West has a fully operational media lab and library. In the afternoon, students can extend 
their learning day in the Afterschool Education and Safety Program (ASESP). The ASESP meets the needs of over 100 students by 
offering activities such as homework assistance, art, science, and physical exercise through games and team sports. 
 
Frank West continues to educate our students so that they can achieve academic success now and in the future. Many subgroups 
showed continued growth, especially our English Learners. We will continue to implement strategies that will help all students meet 
their annual EL goals to prepare them for English language proficiency. 
 
Frank West Elementary 
 
Vision 
Frank West’s vision is to be a leader in education through a collaborative and supportive learning community that ensures and builds 
relationships with all students to inspire them to become lifelong learners. 
 
Mission 
Frank West’s mission is to ensure a safe and positive learning environment in which all students are valued and cared for, receive an 
equitable education, and are prepared to be college and career ready with the support of our community and parents.