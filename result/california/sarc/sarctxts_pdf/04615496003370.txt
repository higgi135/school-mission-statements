Sierra Avenue Elementary School is located in the Thermalito Union Elementary School District near the town of Oroville, California. 
The community is a mix of single-family dwellings, apartments, and a few small businesses. The school is one of three K-5 schools in 
the district and currently serves approximately 450 students in transitional kindergarten through fifth grade. Students leaving Sierra 
Avenue in the 5th grade attend Nelson Avenue Middle School, which is also part of the Thermalito Union Elementary School District. 
Sierra Avenue Elementary School staff and community strive to work together to create a balanced program that produces articulate, 
confident, and academically skilled students. Parents play very important roles through active participation and involvement in the 
school site council and Parent Teacher Organization (PTO). They are encouraged to participate in various annual events and special 
activities, and volunteer service in and out of the classroom. Sierra has a strong whole-school family atmosphere with an emphasis on 
common values for all. Interventions such during-school and after-school programs, and small group instruction are offered for 
students who are performing below grade level. 
 
Sierra Avenue Elementary’s mission is to develop competent, responsible, and self-directed students who positively impact their 
community.