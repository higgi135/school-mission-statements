Arrowhead Elementary School has 17 classrooms on site serving students in grades pre-K to 6. The school was built in 1953 and was 
modernized in 2011-12, providing sufficient space for instruction. The school and its staff strongly support the diverse academic and 
social development needs of all students. The school facility strongly supports teaching and learning through a modernized computer 
lab, library, large sized-classrooms and playground space. 
 
The mission of Arrowhead Elementary School is to provide an effective educational program based on shared decision-making by staff 
and parents to meet the special needs of a diversified population and prepare our students to be successful in an ever-changing 
society.