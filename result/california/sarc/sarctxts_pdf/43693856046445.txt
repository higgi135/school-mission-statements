Fammatre Charter Elementary School, named a 2008 California Distinguished School, is an outstanding school that values academic 
excellence, enjoys parental and community support, and produces well-prepared students. Located in west San Jose, bordering Los 
Gatos and Campbell, Fammatre serves approximately 550 students in grades TK -5. Additionally, we have two special day classes on 
our campus and two special day preschools. A safe, warm learning environment is evident as one enters the beautifully landscaped 
campus. At Fammatre School we believe that all our students can learn, grow, and experience success in school. We believe that 
children learn best when they are taught in ways that address their learning styles and that a positive, safe learning environment 
fosters successful student achievement. We encourage individual dignity, self-esteem, self-responsibility, and a sense of belonging to 
the school and community. Our focus is on teaching the whole child with an emphasis on creativity, communication, collaboration, 
and critical thinking to ensure students become productive citizens. 
 
Vision Statements 

• Our District has high standards where success for all is expected and achieved. 
• Our District models global citizenship teaching real-world connections and practical applications. 
• All students achieve in a variety of ways to meet social and academic goals. 
• Collaboration is evident in all facets of our school community. 
• 

Everyone is a role model for lifelong learning.