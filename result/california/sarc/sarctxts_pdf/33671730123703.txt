Painted Hills Middle School (PHMS) is a newer school located in the City of Desert Hot Springs, 
California. It is one of five middle schools in the Palm Springs Unified School District. It serves sixth, 
seventh, and eighth grade students from Desert Hot Springs, Whitewater, Painted Hills, North Palm 
Springs, and Sky Valley. Painted Hills Middle School is in its 8th year of operation and served 
approximately 800 students during the 2017-2018 school year. The ethnic composition of the 
school is 10% African American, 72% Hispanic, 13% White, and 5% Other (Native American, Korean, 
Chinese, Asian Indian, Other Asian, Samoan, Filipino, Two or more races). Approximately 23% of 
the students enrolled at the school are classified as English Language Learners (ELL) and 13% of 
students are identified as having a disability (SWD). Approximately 87% of our students are 
receiving a free or discounted lunch and 100% of our students receive free breakfast in our 
innovative Breakfast-In-The-Classroom program. Additionally, 100% of our students are eligible for 
supper - a transition meal program for students after school. We have 36 fully credentialed 
certificated staff members to instruct the students at Painted Hills Middle School. Our Special 
Education and English Language Learner (ELL) teachers are supported by classroom aides and serve 
students in a co-teach inclusion model. The school has two assistant principals, two counselors, one 
math coach shared with another school, one part-time psychologist, one nurse (1 day per week), 
and one speech therapist (1.5 days per week). All core subjects, Math, Language Arts, Social Studies, 
and Science courses are offered and aligned to the California Content Standards. Principal’s 
Exchange teacher generated interim assessments are utilized as mastery and benchmark 
assessments in math and language arts. Reading Plus and the ELD component of the Study Sync 
curriculum are utilized for the intensive intervention curriculum at PHMS. The following electives 
are offered at PHMS: AVID, creative writing, beginning band, wind ensemble, jazz band, choir, 
advanced choir, art, advanced art, ASB, project based learning (PBL), robotics and engineering, 
CrossFit, leadership, community service, Office T.A., and Library T.A.. Painted Hills Middle School is 
working hard to increase daily attendance. Students are recognized every trimester for outstanding 
attendance during grade level assemblies. The schools prevention specialist works closely with a 
team of community aides from the central office to identify students with attendance concerns and 
address their needs. In addition, a monthly School Attendance Review Board (SARB) meeting is 
conducted at the Desert Hot Springs Police Department to offer a convenient meeting place for 
families and students to meet with district office staff to address attendance issues. PHMS 
rigorously enforces a zero tolerance policy on fighting, weapons, drugs, bullying, and gang 
associated activities. Opportunities for parent involvement include: Synergy ParentVue, Painted 
Hills Middle School Parent Teacher Student Association (PHMS PTSA), school site council, English 
learner advisory committee (ELAC), and parent workshops. 
 
School Mission: 
Painted Hills Middle School will prepare our students for high school by equipping them with the 
social and academic skills needed to succeed and become college and career ready. Our school 
evaluates the effectiveness of our SPSA every spring and our School Site Council (SSC) and other 
leadership groups have the opportunity to review all student achievement data. Monitoring 
comments will be added to our SPSA throughout the year. Revisions to our SPSA, and subsequent 
Board approval, will occur if there are substantial budget and/or material changes during the school 
year. 
 

2017-18 School Accountability Report Card for Painted Hills Middle School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Painted Hills Middle School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

35 

36 

36 

0 

1 

0 

0 

0 

0 

Palm Springs Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1038 

18 

34 

Teacher Misassignments and Vacant Teacher Positions at this School 

Painted Hills Middle School 

16-17 

17-18 

18-19