Paradise Charter School is an innovative approach to K-8 education in Modesto, California. Using the "School within a School" model, 
the goal is to create a quality, site based school of choice, which will stress character development, leadership, technology, citizenship, 
honor and educational excellence. The vision statement for this unique school is "Success through Character, Honor, and Educational 
Excellence". Paradise Charter School provides an ideal environment for students to reach their highest academic potential. The 
school's primary focus is on meeting the State Standards. The learning philosophy is grounded in the belief that all students can learn 
and all students will learn. This philosophy is rooted in the research findings of effective school design. The vision is that students will 
be motivated in a learning environment rich with active and egaging curriculum, relevant to the students and their lives. The objective 
is to enable students to become self-motivated, competent and lifelong learners. 
 
Our belief is that preparing students for the real world is critical to lifelong success. The core belief is that all students can be successful 
if learning institutions: 
 

* Define what students need to know and are able to do. 
* Provide a set of linked, relevant learning experiences that allow students to show what they know. 
* Give students the time and attention they need to be successful. 

 
Paradise Charter School also provides an after-school program that assists students with homework, tutoring help, guitar, band, dance, 
gardening and cooking. Our vision is to provide a safe and orderly place for students to thrive and grow academically and socially.