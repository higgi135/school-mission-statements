SCHOOL PROFILE 
Will Rogers Middle School prides itself on being a diverse learning community. The ethnic breakdown of Will Rogers Middle School is 
as follows: 10% of the students are African American, 1% Asian, 18% Hispanic or Latino, 61% White, 2% Filipino, 1% Pacific Islander, 
and 1% American Indian. Additionally, Will Rogers Middle School is home to an Independent Living Skills (ILS) Program. 
 
Will Rogers Middle School governance is an inclusive decision-making process created to maintain formal and effective decision 
making procedures for creating and implementing policies which support the school’s mission and core values. The foundation of the 
school’s governance is founded on the following conditions: 

 

• Committee decisions are made by committees and not by individuals. 
• Members represent the entire school community. 
• Procedures reflect the democratic process. 
• 
Each committee represents a forum created specifically for dealing with issues of concern. 
• All decisions are scrutinized to insure they support of the vision, mission, and core values. 
• All committee members share responsibility for decision outcomes. 
• Committee business is clearly communicated to the entire school community. 
• 
• Decisions promote continuous school-wide instructional, curricular, and process choices that will have positive effects on 

The committee process insures an equal opportunity of expression by members of the school community. 

student academic achievement and behavior. 

MISSION STATEMENT 
The mission of Will Rogers, a student-centered middle school that values diversity, is to inspire every student to achieve their highest 
potential, grow in self confidence, and be college and career ready through meaningful learning experiences, challenging innovative 
instruction, and strong partnerships with students, staff, parents, and the community. 
 
SCHOOL STRATEGIC PLANNING COMMITTEE 
The Will Rogers Middle School Strategic Planning Committee is identified under the governance structure of the school, as the Strategic 
Planning Team. This committee meets as needed and is comprised but not limited to the members of our administrative team, teaching 
staff, classified staff members, students, and community members. The Strategic Planning Team was established as part of the school’s 
overall governance structure in 2014. The Strategic Planning Team makes recommendations on policy and procedures as it relates to 
school climate and safety of students, staff and the community as a whole. The Strategic Planning Team takes recommendations 
forward to both the school’s Leadership Committee and School Site Council for final approval on any new policies or procedures. 
 
Representatives: 
• Parents 
• Students 
• Representative from each Department 
• Administrator 
• Custodial Staff 
• Counselor 
• Classified Representative 
• Security Representative 
• Leadership Team Members 

 

 

2017-18 School Accountability Report Card for Will Rogers Middle School 

Page 2 of 14 

 

Selection Process: 
 
Department leaders and Principal recruit committee members to serve on the committee. The vice principal will recruit 
representatives from the classified staff. 
 
Term of Service: 
 
The maximum consecutive years a member may be on the committee is five years. 
 
Responsibilities 

• We will always operate schools which are physically and emotionally safe with environments conducive to learning. 
• School and program plans will always be aligned with the Strategic Plan of the district. 
• We will not tolerate behavior or language which demeans the dignity or worth of any individual or group. 
• No program or service will be retained unless it makes an optimal contribution to the mission and benefits continue to 

justify the costs. 

• We will not tolerate ineffective performance by any staff member. 

No new program or service will be accepted unless: 

It is consistent with the strategic plan, 
Its benefits clearly justify the costs 

• 
• 
• Provisions are made for professional development, program evaluation, and communication with constituents. 

Scope of Authority: 
 
1. Only designated representatives may vote. 
 
2. Final recommendations require 100% approval from voting members in attendance. 
 
3. The committee will make recommendations to the Leadership Team and School Site Council for approval before implementation of 
any school wide program(s). 
 
Leader: 
 
Principal and Internal Coordinator 
 
Control of Agenda: 
 
A trained district facilitator will help guide the committee through this process. 
 
Tactics: The committee will support the school’s vision, mission, core values, and academic improvement plan by: 
 
1. We will enrich each curricular area by effectively integrating technology into teaching and learning in every classroom. 
 
2. We will ensure staff employs effective instructional practices to maximize student achievement. 
 
3. We will create and implement a system for monitoring and evaluating student’s academic and behavioral performance. 
 
4. We will build trusting relationships with students, families, and the community to create a unified collaborative and safe 
environment. 
 
Collection of Data Relevant to Scope of Authority: 

• PASS/home suspension referrals 
• Success and participation in academics 
• Custodial inspection 
• ADA 
• Surveys 
• Process Data 

2017-18 School Accountability Report Card for Will Rogers Middle School 

Page 3 of 14 

 

Attendance: Regular attendance is expected at all scheduled meetings. Members will be responsible for sending an alternate in case 
of absence. Attendance will be noted in meeting minutes. 
 
Frequency of Meeting: Once per month or as needed. 
 
Core Outcomes for Students 
 
As a natural result of successful completion of his or her course of study: 

• Students will develop the values and skills to participate as responsible citizens in a democratic society. 
• Students will develop the necessary core content skills and knowledge, including critical thinking, problem solving, and 

information processing to prepare for life-long learning and achievement. 

• Students will develop confidence in their abilities to achieve at high levels both academically and socially. 
• Students will develop the ability to contribute to, understand, utilize, and appreciate multicultural expressions of and 

professional options in the visual, performing, and industrial arts. 

Core Beliefs about the Conditions for Learning 
 
As fundamental tenets of the learning process, the school community believes that: 

• Every person is unique and has equal worth. 
• Everyone can and will learn. 
• People learn in different ways and at varied paces. 
• Education is the shared responsibility of students, families, teachers, staff, and community. 
• Quality education expands opportunities throughout a person’s life. 
• Challenging people to meet high expectations leads to exceptional learning and remarkable results. 
• Nurturing relationships and healthy environments are necessary for individuals to thrive. 
• Diversity is a valuable asset that strengthens and enriches our community. 
• Personal development and community well-being depend on individual responsibility. 
• Everyone benefits when people willingly contribute to the well-being of others. 
• Honesty and integrity are essential to build trusting relationships. 
• Access to a quality public education is essential to our democracy. 

Core Commitments about How We Operate Together 
 
As fundamental tenets about working together, the school community believes that: 

• Success, while being encouraged, expected, celebrated, valued, appreciated, and cultivated is the result of hard work and 

preparation that includes feedback for improved performance; failure is not a statement of capability. 

• Through reflection upon the results of multiple assessments, the school community will strive for personal, professional, 

and academic growth and improvement. 

• The entire school community will practice open, honest, and respectful communication that contributes to a safe and 

inclusive environment. 

• The school community shares accountability for student outcomes and improvement options. 
• Safety nets are in place to aid each student’s progress toward expected achievement targets. 

At Will Rogers Middle School safety is our first priority. All students deserve to learn in an environment that is free from distraction, 
in which they feel safe and protected. We believe that nothing should get in the way of the academic achievement of students. 
 
Will Rogers Middle School believes that it is the responsibility of students, parents and staff to assist students in becoming life long 
learners who are prepared to be productive citizens at the completion of high school. 
 
Students are expected to follow the Positive Behavioral Invention System (PBIS) on a daily basis: 
 
I. Be Ready 
 
II. Be Kind 
 
III. Be Safe 
 

2017-18 School Accountability Report Card for Will Rogers Middle School 

Page 4 of 14 

 

IV. Be Responsible 
 
The School Strategic Planning Committee reviewed data from numerous sources to develop appropriate goals and objectives for the 
Strategic and Safe School Plan. 
 
OBJECTIVES: 

• All students will demonstrate continual growth toward common core standards through multiple measures of assessment. 
• Upon transition to high school, every student will acquire and apply college and career readiness skills, including critical 

• 

thinking, problem solving, collaboration, written and oral communication. 
In order to become contributing, responsible and caring members of our diverse community, all students will consistently 
demonstrate the character traits of Pride, Ownership, Wisdom, Excellence and Respect (P.O.W.E.R.) (This has been 
changed to the Mustang 4 in 2017) 

• By 2017, in order to ensure success for all students, achievement gaps between the highest and lowest performing groups 

will be reduced by 50%. 

AREAS OF PRIDE AND STRENGTH 
 
Will Rogers Middle School believes that students are the greatest resource for establishing a positive learning environment that 
promotes a safe school for students and staff. The school’s Associated Student Body (ASB) organization is a pivotal group of student 
leaders that organize and promote multiple positive activities on campus that includes but is not limited to, positive youth 
development assemblies, multi-cultural events, community service projects, and overall leadership development for students. 
 
Many of the Will Rogers Middle School students are actively involved in our student clubs or athletic programs through our partnership 
with the park and recreation department. These clubs include the following: Wrestling Club, AVID, Student Government Rep Council, 
and Club LIVE. These clubs give students an opportunity to engage and connect to the school. 
 
Will Rogers Middle School also offers a comprehensive academic and behavioral counseling center. All students have access to a 
counselor that helps monitor and work with students during their three year enrollment at our school. Our counselor provides direct 
instruction to classes through their career units. Our counselor also assists students when crisis situations arise. 
 
1 We will foster student led goal setting and provide opportunities for evaluation of academic and behavioral objectives. 
 
2. We will employ innovative instructional practices to maximize student achievement and engagement 
 
3. We will continue to build trusting relationships with students, families and the community to create a unified collaborative and safe 
environment. 
 
The Will Rogers School Safe School Plan will be shared with the public through various means that include but not limited to the follow: 

• The School Strategic Planning Team will be notified of publication, and be given a hard copy of the plan. 
• The School Leadership Team and School Site Council will review and approve the plan by the end of February 2014. 
• The Safe School Plan will be posted on the Will Rogers Middle School Web page for the community to view. 
• The Safe School Plan will be located in the Site Emergency Plan Binder. 
• The district will be sent one electronic copy for the Safe Schools office. 

On an annual basis, our School Site Council and Strategic Planning Committee will evaluate and review the prior year’s Safe School 
Plan. This process will involve meeting to determine areas of strength and concern. Our team will recommend refinements based on 
data and the experiences brought forward to the committee. Steps to modify the plan will take place based on discussion and 
consensus. The new plan will then be implemented and shared with the community through the process outlined in Section VI. It is 
the intent of Will Rogers Middle School is to have the Safe School Plan be a document that will be a working and fluid plan that can be 
adjusted as the needs change on a middle school campus. 
 
 

2017-18 School Accountability Report Card for Will Rogers Middle School 

Page 5 of 14