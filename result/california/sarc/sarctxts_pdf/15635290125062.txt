Tierra Del Sol (TDS) is the newest continuation school in the Kern High School District. We opened 
in the fall of 2011 with 64 students, and we have graduated over 900 students in the past 7 years. 
TDS services between 400 and 500 students annually, and is staffed to service 340 students at one 
time. TDS is 100% free and reduced lunch. In the 2017-2018 school year, TDS provided services to 
465 students, and the ethnic breakdown was as follows: 82% Hispanic, 11% African american, 5% 
White, 1% Native American, and 1% Other. We were 59% male and 41% female. 43% of our 
population were 12th graders, 11% were 5th year seniors, 36% were 11th graders, 8% were 10th 
graders, and 2% were 9th graders. 82% of TDS students had a positive outcome. We define a 
positive outcome as graduating, returning to the student back to their home school, returning to 
TDS for the next year, and utilizing the services of the Kern County Superintendent of Schools 
(KCSOS) for reasons not related to discipline. TDS graduated 120 students, sent 41 back to their 
home school ( 22 graduated with their home school), rolled 185 over to the 2017-2018 school year, 
and we helped 15 remain in school by utilizing programs at the KCSOS. 
 
TDS has 2 administrators, 17 teachers, 2 counselors, 3 security, 3 instructional support staff, 4 
clerical staff, 2 custodial staff, 2 food service staff, a part-time School Social Worker, a full-time 
Intervention Specialist, and a part-time Community Specialist. TDS offers courses in Math, English, 
Earth Science, Biology, Health, Physical Education, a Mild/Moderate Special Education class, 
Independent Study, Home Hospital Instruction, Social Studies, Art, Business, and Construction 
(Dual Enrollment credits with Bakersfield College are available to students). All day classes are 65 
minutes in duration unless we are on a special schedule. TDS students receive 325 minutes of 
instruction daily. This is 145 minutes longer than the state required 180 minutes. TDS is fully funded 
out of the district's LCAP, Title 1, Lottery, and CTEIG, and CRBG funds. All courses use district 
approved courses of study. Additionally, TDS students have access to the CTE courses available at 
the ROC that is located adjacent to the TDS campus. TDS students can also continue their UC A-G 
course work by dual enrolling in the district's online program, Kern Learn. 
 
With over 200 Chromebooks and 70 student computers, there are enough computer for each 
student that attends each day. TDS' teachers are able to vary their instruction with technology, and 
many of them use Google Classroom so that students can stay connected to the class when they 
are absent from school. In addition to the traditional direct instruction, TDS' teachers use Kagan 
Structures and project based instructional practices. TDS has added 3-D technology, weights and 
audio equipment, and Virtual Reality technology to enhance student engagement. 
 
TDS' new and colorful facility, colorful and beautiful landscape, abundance of technology, diversity 
of course selection, and dedicated staff make it a unique learning environment for our students, 
and these attributes causes TDS to stand out from other continuation schools in the state. 
 
The mission of Tierra Del Sol High School is to provide personalized instruction and support in an 
alternative setting. We will achieve this by bringing educational experiences to students that are 
unique to the continuation school environment. TDS is staffed with highly motivated teachers and 
support staff who understand its unique population and work together, along with the 
administration, in the best interest of each student. These experiences will raise academic levels 
which will be monitored by ongoing assessments and interviews. Our students will take 
responsibility for their actions and futures. The school works hand in hand with community leaders 
and local businesses to ensure success, so that our students serve as role models for all of society. 
 

2017-18 School Accountability Report Card for Tierra Del Sol Continuation High School 

Page 1 of 12 

Vision Statement 
Tierra Del Sol’s vision is that every student becomes a graduate with a career pathway completed.