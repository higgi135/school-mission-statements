Shandin Hills Middle School has 53 classrooms, a library, a multipurpose room, a parent resource center, and an administration office. 
The campus was built in 1968, and five new special education classrooms were built in the 2006-07 school year. The facility strongly 
supports teaching the new Common Core State Standards (CCSS) and 21st Century Skills through its ample technological classrooms, 
technological courses and the belief that all students will be prepared for high school, college and beyond. 
 
Shandin Hills Middle School is a community that has high expectations of success for each student by embracing individual differences 
and learning styles in a safe and orderly environment. Through collaboration with parents and the community, we are committed to 
ensuring academic excellence and continuously fostering real world applications so that all students become independent learners 
and responsible citizens.