Rollingwood Elementary School is devoted to educating children holistically as world citizens, 
preparing them for college and career, and teaching students about nutrition and healthy living. 
The themes of academic excellence, character education, and social responsibility are a trademark 
of the school’s culture. Our community embraces the idea that by working together, we can provide 
the resources, encouragement and man power to help all students attain their educational goals. 
We believe all children are unique and deserve the opportunity to succeed, regardless of their 
challenges, and that the greater community is responsible for organizing to ensure every student’s 
success. 
 
For 2018-2019 school year, we have a focus on PBIS (Positive Behavior Interventions and Supports). 
We are continuing our tradition of teaching to the whole child and not only focus on academic 
excellence, but also developing the skills necessary to succeed in life. We have also partnered with 
Skyline College and the San Mateo County Office of Education to focus on sustainability and 
environmental literacy. 
 
During the 2017-18 school year we continued to focus on implementing our Common Core State 
Standards, as well as focused our professional development on the Next Generation Science 
Standards. We implemented school-wide science, technology, engineering and mathematics 
(STEM) challenges where students researched a topic and presented their findings. We want our 
students to make a positive difference in the world, so we are continued our monthly spirit days 
that benefitted different charities. 
 
Colleen Hennessy 
 
Principal 
 
School Mission Statement 
At Rollingwood Elementary School, we have high expectations for all of our students. We strive to 
collaborate with one another to help our students reach their full academic potential. By bringing 
together parents and community for various educational and social programs, we envision a school 
where all of our students and families feel included. We provide a safe, friendly, encouraging and 
academically rigorous learning environment for our students. Rollingwood is committed to 
developing critical thinkers, ethical citizens and lifelong learners who are contributing members of 
our community. 
 
San Bruno Park School District Belief Statements 
We believe that: 

• Public education serves a vital role in our society. 
• Honesty and openness at all levels create trusting relationships. 
• 

Every student has capacity and a desire to learn. It’s our task to capture and expand 
that desire and encourage students to do their best. 
In education, one size does not fit all; we need to address each child’s needs 
individually. 
The community must be included in the education of their children. 

• 

• 
• We must act on our beliefs and serve as role models. 
• We must provide a safe and secure education environment. 

2017-18 School Accountability Report Card for Rollingwood Elementary 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Rollingwood Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

13 

0 

0 

9 

0 

0 

13 

1 

0 

San Bruno Park School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Rollingwood Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.