Adams School serves 434 students in kindergarten through grade six. We have a student population 
rich in cultural diversity which provides opportunities for all students to grow, learn, and achieve. 
The mission of Adams School is to instill in each child a sense of self worth, independence, and 
responsibility which will enable each student to become a life-long learner, a successful citizen, and 
be able to reach his or her full potential. Adams teachers are committed to providing all students 
with a positive learning experience that is developmentally appropriate, promotes self-expression 
and creativity, embraces the diversity of our population, meets the needs of all students, promotes 
safety and responsibility, is supported by all necessary staff, materials and equipment, and 
welcomes parents as an integral part of the learning process. 
 
Parents are encouraged to take an active role in the education of their child/children at Adams 
School. The staff and Parent Teacher Organization (PTA) are continually reaching out to the 
community to bring more participation into the total school program. Parents, staff and community 
members are actively involved in contributing time, energy, and money to the school. Reading is 
central to the academic program at Adams School. Our focus is to provide students with core 
academic instruction coupled with opportunities for individualized independent reading. The 
Accelerated Reader program offers an individualized reading comprehension opportunity for all 
students by identifying each student's reading level and providing access to over 11,000 books in 
the library. Each student is assessed three times a year using the District Literacy Assessments. The 
results of these assessments are used to create targeted reading interventions from beginning 
phonemic awareness and phonics instruction, to decoding multi-syllabic words. Emerging and 
beginning literacy is systematically taught through the SIPPS program in our general education and 
special education classes. 
 
Many supplementary programs are provided. Students in TK through sixth grade have hands-on 
science lessons, which cover areas of engineering, life, earth and physical science. The PTA 
sponsors the Fibo Art program, which exposes the students to three of the great artist each year 
through FIBO art. Sixth grade students have the opportunity to learn leadership skills as members 
of the school's active Student Council. Students in fourth, fifth, and sixth grades can be involved in 
the lunch time soccer tournament program. Special services include Title I, School Improvement, 
Speech and Language, Psychologist, Specialized Academic Instruction, Physical education specialist 
for fourth through sixth grades, a Kindergarten through sixth grade music instructor, and a 
kindergarten through sixth grade science specialist. Project Kidz Connect also provides an extended 
school day for targeted students, academic instruction, and physical exercise opportunities for first 
through sixth grade students. Adams also offers upper grade students the opportunity to learn 
music and play in an orchestra. Students practice during lunch and after school and perform at 
various functions in the local community. The school Chromebook program and mobile computer 
labs provide many software programs for extended learning as well as Internet access. 
 
You can visit Adams School via the school's web page or come spend a day at Adams School and 
see how exciting learning can be. 
 
 
 

2017-18 School Accountability Report Card for Adams Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Adams Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

25 

29 

32 

0 

0 

0 

0 

0 

0 

Newport-Mesa Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1105 

5 

7 

Teacher Misassignments and Vacant Teacher Positions at this School 

Adams Elementary School 

16-17 

17-18 

18-19