The Independent Learning Academy (ILA) opened its doors on September 8, 2014. The ILA is available to students who reside within 
the boundaries of the Burbank Unified School District (BUSD). Students wishing to attend the ILA must be at least within two grade 
levels in reading/vocabulary. The school serves students in grades 7-12. While ILA students must follow the District-adopted curriculum 
and meet the District graduation requirements, independent learning offers flexibility to meet individual student needs, interests, and 
styles of learning. The ILA operates as a school of choice as defined by the CDE. It operates as an academy staffed by four full-time 
credentialed teachers who coordinate the program, meet with students and conduct assessments and an administrator who shares 
supervision with the adult school. 
 
Mission Statement: 
 
The ILA provides an educational experience in an environment where students feel safe, supported, and engaged in their learning 
process. We encourage students to work to their full potential, to exhibit independent thought, self-confidence, creativity, and 
imagination. 
 
Vision Statement: 
 
The ILA seeks to foster a challenging learning environment that encourages high expectations, for success through independent 
learning which allows for individual differences and learning styles. Our vision for the ILA is to provide a safe, orderly, caring and 
supportive environment where our diverse students are valued as individuals, and guided by positive relationships with supportive 
staff. We strive to have our parents/guardians, teachers and community actively involved in our students' learning. 
 
https://www.burbankusd.org/District/Portal/independent-learning-academy-ila