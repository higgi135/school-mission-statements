Chula Vista Hills is one of 49 schools in the Chula Vista Elementary School District, including five 
dependent and three independent charter schools. Chula Vista Hills was dedicated on February 16, 
1989. The school lies west of Southwestern College on a 10-acre site adjoining two Chula Vista 
parks. A well-lit parking area and landscaped grounds greet visitors, staff, and students. Each of five 
buildings has four classrooms, a central workroom, and four offices. Five portables house four 
classrooms and a YMCA daycare program. 
 
Mission 
The mission of Chula Vista Hills Elementary School is to foster the continual academic and social 
growth of all students by engaging them in challenging, enriching learning experiences that meet 
their individual needs. We are committed to providing a rigorous academic program in a safe, 
positive learning environment and to collaborating with parents, community, and one another to 
ensure student success. 
 
Vision: 
We will empower students to be critical thinkers and effective communicators in order to reach 
their highest potential. We will do this by providing a rigorous, relevant, and innovative learning 
environment that challenges all students to achieve academic success and express themselves 
creatively. 
 
School Focus 
At CV Hills we are focused on using the workshop model to instill a love of reading and writing in 
our students. Teachers regularly use thinking routines to engage students in critical thinking and 
collaborative conversations. This year, we are building our expertise around the practice of 
conferring so that we can meet students where they are and provide differentiated and 
personalized support for their learning. At CV Hills we are also committed to building relationships 
with our students and families and ensure that social emotional learning is part of our daily 
instruction. 
 
 

----

---- 

Chula Vista Elementary School 

District 

84 East J Street 

Chula Vista, CA 91910-6100 

(619) 425-9600 
www.cvesd.org 

 

District Governing Board 

Leslie Ray Bunker 

Armando Farias 

Laurie K. Humphrey 

Eduardo Reyes, Ed.D. 

Francisco Tamayo 

 

District Administration 

Francisco Escobedo, Ed.D. 

Superintendent 

Jeffrey Thiel, Ed.D. 

Assistant Superintendent, Human 
Resources Services and Support 

Oscar Esquivel 

Deputy Superintendent, Business 

Services and Support 

Matthew Tessier, Ed.D. 

Assistant Superintendent, 

Innovation and Instruction Services 

and Support 

 

2017-18 School Accountability Report Card for Chula Vista Hills Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Chula Vista Hills Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

24 

0 

Teaching Outside Subject Area of Competence 

NA 

25 

25 

0 

 

0 

 

Chula Vista Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

 

 

 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.