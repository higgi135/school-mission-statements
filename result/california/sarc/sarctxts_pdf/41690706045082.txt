Los Cerritos is a small school of approximately 310 TK-5 grade students. We are a Title I school and 
we serve English Learners representing 16 languages. We are a 1:1 school; all students have access 
to a laptop and every primary student also has access to an iPad. Our special education programs 
include K-2 Academic Instruction, Academic Support and Speech and Language Therapy. Every 
Wednesday is an Early Release Day for students which allows teachers to collaborate weekly, We 
also have classroom instructional aides and bilingual instructional aides to support student 
learning. Additional learning opportunities occur after school with our Academic Center/FUN Clubs 
and Gene Academy. 
 
Our mission is to foster creativity and independence using growth mindset. We are committed to 
encouraging our students to collaborate about their successes and challenges. We will provide a 
caring and enthusiastic environment to our community of scholars. 
 

 

 

----

---- 

----
---- 
South San Francisco Unified 

School District 

398 B. Street 

South San Francisco, CA 94080 

650.877.8700 
www.ssfusd.org 

 

District Governing Board 

John C. Baker 

Patricia A. Murray 

Daina R. Lujan 

Eddie Flores 

Mina A. Richardson 

 

District Administration 

Shawnterra Moore, Ed.D. 

Superintendent 

Keith B. Irish 

Assistant Superintendent, Educational 

Services and Categorical Programs 

Jay Spaulding, Ed.D. 

Assistant Superintendent Human Resources 

and Student Services 

Ted O 

Assistant Superintendent, Business Services 

Leticia Bhatia, Ed.D. 

Director English Learner Programs, 

Categorical Programs and Special Projects 

Jason Brockmeyer 

Director of Innovation, Community Outreach 

and Special Projects 

Valerie Garrett, Ed.D. 

Director of Student Performance, Program 
Evaluation, and Instructional Interventions 

Ryan Sebers 

Director of Student Services 

Velma Veith, Ed,D. 

Director, Pupil Personnel Services and Special 

Education 

Bryant Wong 

Director of Technology 

Ronald Vose 

Director of Facilities and Safety 

Fran Debost, MS, RDN 

Director of Nutrition Services and 

Distribution 

 

2017-18 School Accountability Report Card for Los Cerritos Elementary 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Los Cerritos Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

17 

19 

20 

0 

0 

0 

0 

0 

0 

South San Francisco Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

412 

16 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Los Cerritos Elementary 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.