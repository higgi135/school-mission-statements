Cadwallader Elementary School is located in the center of the Evergreen School District in southeast 
San Jose. Our school is a cohesive partnership of students, parents, educators, support staff, and 
community members. Our school community takes pride in providing a quality education in a 
caring, supportive, and challenging environment. We have rigorous academic expectations 
combined with a climate that promotes student creativity and respect for cultural diversity. 
Students are challenged to enthusiastically and willingly develop to their full potential. 
 
With the ever present themes of life long learning and school pride, Cadwallader Elementary School 
provides all students with a motivating environment in which to learn. This is reflected in the 
prevalent atmosphere that says extraordinary learning happens here! Teachers are on the forefront 
of research-based practices that best respond to students’ learning needs. All teachers 
communicate on a regular basis with parents and the support staff. They are open, caring, and 
dedicated individuals. Our group of highly qualified, effective educators demonstrate a positive 
attitude toward their profession, the Evergreen School District, and the Evergreen community they 
serve. 
 
We were deemed a California Distinguished School and a Title 1 Academic Achieving School for the 
2009-10 school year. For the 2010-11 school year, we were awarded the distinct honor of being a 
Title 1 Academic Achieving School for two years in a row. Our API score for the 2012-13 school year 
was 895 and for the 2013-14 school year, we were awarded the Title 1 Academic Achieving School 
award for the third time! We are extremely proud of these accomplishments since it validates our 
efforts in ensuring that all students receive a well rounded education. 
 
Cadwallader Vision 
At Cadwallader we inspire, excite, and empower all students to become lifelong learners with the 
knowledge and critical thinking skills to reach their personal goals and have a positive impact in our 
world. 
 
Cadwallader Mission 
Our Cadwallader staff is committed to: 

• 

celebrating and appreciating diversity within our community while establishing 
integrity, perseverance, respect and empathy. 

 ensuring students have a safe environment in which they feel nurtured, valued, and 
empowered to develop a strong sense of self-worth to fulfill their hopes and dreams 
and be part of the community. 

 modeling for students how to think critically, collaborate, and challenge themselves to 

 

have a growth mindset. 
inspiring students to achieve academic excellence in our ever changing world by 
developing lifelong learners using research-based practices. 

2017-18 School Accountability Report Card for Cadwallader Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Cadwallader Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

19 

17 

18 

0 

0 

0 

0 

0 

0 

Evergreen Elementary School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

483 

3 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Cadwallader Elementary School 

16-17 

17-18 

18-19