Windsor Creek Elementary School, home to 400 second and third grade students, is located adjacent to the 4 ½ acre Windsor Town 
Green. Our campus is nestled between the Windsor Creek, home to blue and valley oaks some of which are over 200 years old, and 
new residential neighborhoods bustling with children and young families.We are extremely proud of our efforts to provide our 
students with both an exemplary academic program and a safe and nurturing environment, where their uniqueness and differences 
are truly respected. Parents are such a vital component to our school community that we provide volunteers with a volunteer 
coordinator. We provide students with quality enrichment programs in physical education, library, music, and computers. Most 
classrooms have a 2:1 ratio of chrome books to enhance our academic program. Our students have gardens, where our District Funded 
Gardener, have monthly lessons, and a greenhouse in which to conduct observations and experiments. We celebrate our students 
achievements at monthly award assemblies designed to encourage, motivate, and recognize their efforts. Our Student Leadership 
consists of Class Representatives and meet weekly to create spirit days, monthly kindness activities and write speeches to present to 
their class with upcoming events. Our Socio-Emotional Curriculum, Responsive Classroom, consists of daily Morning Meetings and 
Closing Circles, where students are encouraged to share and create a community feel within their classroom. The entire staff puts 
students' needs first and work collaboratively to create an atmosphere where students are excited to come to school to learn and 
have fun. 
 
Our highly trained and skilled staff is committed to supporting our students in reaching their full potential, both academically and 
socially. Stop by and visit our campus, we are certain you’ll catch our Wolf Cub Pride! 
It is the mission of the Windsor Unified School District to provide a supportive and nurturing environment for all students. Students 
shall acquire the basic skills of knowledge, along with the thinking skills needed for problem-solving and decision-making relevant to 
a changing world. 
Windsor students shall exhibit personal and social maturity through responsible behavior, developed from understanding and respect 
for the diversity of all life, and a genuine caring for others.