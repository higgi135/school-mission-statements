SCHOOL MISSION 
Etiwanda Community Day School is operated by the Etiwanda School District and is located adjacent to Summit Intermediate School. 
The school serves mandatory and other expelled students, students referred by a School Attendance Review Board, and other high-
risk youths. Intermediate school students are placed in ECDS in accordance with board policy. 
 
Challenging curriculum and instruction aligned to individual student strengths and learning styles are provided during the instructional 
day. Students benefit from lessons which focus on resiliency, self-esteem, and positive social skill development. The classroom 
teacher is assisted by two instructional aides, a physical education specialist, and a school counselor. Collaboration from the San 
Bernardino County Office of Education, law enforcement, and the West End SELPA provide additional support services. 
 
DISTRICT & SCHOOL PROFILE 
Etiwanda School District serves over 14,000 TK-8 students residing in the cities of Rancho Cucamonga, Fontana, Alta Loma, and 
Etiwanda. The district currently operates thirteen TK-5 elementary schools and four intermediate schools (grades 6-8) and a 
Community Day School. Etiwanda’s graduating eighth-grade students are served by Chaffey Joint Union High School District for grades 
9-12. Homeschooling program, preschool program, and childcare are provided at some schools within the district. More information 
is available on the district website or by contacting the district office at (909) 899-2451. 
 
The district’s commitment to excellence is achieved through a team of professionals dedicated to delivering a challenging, high-quality 
educational program. Etiwanda School District appreciates the outstanding reputation it has achieved in local and neighboring 
communities. Consistent success in meeting student performance goals is directly attributed to the district’s energetic teaching staff 
and strong parent and community support.