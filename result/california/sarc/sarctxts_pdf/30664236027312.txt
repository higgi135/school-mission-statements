Horace Mann School is one of 23 schools in the Anaheim Elementary School district. The school is named after Horace Mann, the 
“Father of Public Schools” in the United States. In 2018-19, approximately 800 PreK-6th grade students attended Horace Mann. 
Mission: Educating students to participate in a global community 
 
Foundational Skills: The primary focus at Horace Mann Elementary School is to increase reading achievement through the use of the 
research-based lesson design, interventions, enrichment and supports. 
 
Social Emotional Learning: Awarded Silver Level Positive Behavior Intervention and Supports from the California PBIS Coalition 
We are NOW an eSTEAMed district; preparing our students for the 21st century. 
“E”ngagement 
 
Learning Links -Children and caregiver classes for ages 0-5 years 
 
Social Emotional Learning Curriculum 
“E”quity 
 

• All students have access to the range of programs on site 

 
“S”cience- Mystery Science 
 

• 
• 

K-5 a stamadrsdalinged video based hand-on curriculum 
6th Grade- CA Science Framework based curriculum 

 
“T”echnology- Code Campus 
 

• 
• 

to 1 devices 
K-6 20 week teacher led computer programing instruction 

 
“E”ngineering-Engineering is Elementary 
 

• Bytes and Bots-Computer programing, Codeology, Buildology, and MakerSpace 
• 

“A”rts-VAPA Standards 

 
Music teacher for all students 
 

• 
• 
• 

Pre-K General Music 
5th grade exploratory instrumental wheel 
6th grade single instrument focus 

 
“M”athematics-Standards for Mathematical Practice 
 

• 
• 

ST Math 
Counting Collections in primary grades 

 

 

2017-18 School Accountability Report Card for Mann Elementary School (2018-19) 

Page 2 of 12 

 

“Ed”- Dual Immersion 
 

Kindergarten and First grade classes in Spanish 
• 
• Adding continuing grade level each school year 
• 

State Assessments: California Dashboard 

 
English Language Arts increased significantly and Math increased from the 2017-2018 school year. English Language Learners 
increased significantly over the previous year.