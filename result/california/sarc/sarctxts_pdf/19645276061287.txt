Principal’s Message 
Our goal is to introduce students to areas of Visual and Performing Arts with the intent to further 
develop skills in creativity, communication, collaboration, and critical 
thinking. We want to increase student engagement and help foster the dreams and talents of our 
students. We have a 
zero period to allow for students to make room in their schedule for one or more electives. We 
have a 6th grade 
elective wheel which includes classes in art, drama, music, AVID strategies and Robotics. In 7th and 
8th grade students have VAPA offerings with the following classes: Advanced Art, Advanced Guitar, 
Orchestra, Dance, Adventures in Media, Multi-Media, and Drama. We also offer our students 
Project Lead the Way STEM classes by offering both a 7th and 8th grade class. 
We also have the 6th grade transition program Where Everybody Belongs (WEB). This 8th grade 
leadership program is focused around the successful transition for 6th grade students to middle 
school. Selected 8th grade students are trained to mentor 6th grade students and provide support 
throughout the year. Every 6th grade student has a WEB leader and there are 40 8th grade WEB 
leaders. We have implemented AVID schoolwide strategies with an emphasis on college and career 
readiness. Finally, we implement the OLWEUS Bully Prevention Program, Character Counts, and 
PBIS. 
 
School Mission Statement 
North Park Middle School is dedicated to producing academically proficient students of strong 
character by engaging all stakeholders and establishing high expectations for all students. We value 
integrity and accountability and believe that all students have the potential to learn. 
 

 

 

----

---- 

----

--

-- 

El Rancho Unified School District 

9333 Loch Lomond Dr. 
Pico Rivera, CA 90660 

(562) 801-7300 
www.erusd.org 

 

District Governing Board 

Dr. Aurora R. Villon 

Gabriel A. Orosco 

Lorraine M. De La O 

Dr. Teresa L. Merino 

Jose Lara 

 

District Administration 

Karling Aguilera-Fort 

Superintendent 

Mark Matthews 

Assistant Superintendent, Human 

Resources 

Jacqueline A. Cardenas 

Assistant Superintendent, 

Educational Services 

 

Dora Soto-Delgado 

Director, Student Services 

Reynaldo Reyes 

Director, Alternative/Adult 

Education 

Dean Cochran 

Director, Special Education 

Roberta Gonzalez 

Director, Early Learning Program 

 

2017-18 School Accountability Report Card for North Park Academy of the Arts 

Page 1 of 8