Our mission is to provide an environment that encourages and motivates students to excel 
academically while working towards completing the requirements for a high school diploma. 
 
Our vision is to work to create an environment that inspires all learners to reach their full potential 
for a brighter future. 
 
At the current time, Mendota Continuation High and Mendota Community Day School have three 
(3) full time teachers and approximately thirty (30) students combined. Both programs have a 
common principal and location. All core subject areas are offered to students in a small classroom 
setting. Students at both Mendota Continuation and Mendota Community Day School begin 
classes a short time later than the traditional high school. The school site is located in the Westside 
of Mendota. Students are given the opportunity to earn credits towards their high school or adult 
education diploma. 
 
The facilities at Mendota Continuation High School include a school office and three classrooms. A 
student lunchroom is also provided. Smartboard Systems are provided in every classroom and 
MCHS also has Chromebooks for individual student use. This technology ensures that MCHS 
students have access to the technology available to all students in the Mendota Unified School 
District. 
 
Class sizes are small at Mendota Continuation High School. Only three teachers currently teach at 
MCHS, with one four and a half hour instructional aide, one office secretary and a principal. 
Students are given instruction in all subject areas with three different teachers. The school day is 
unique in that students are only given instruction from three different teachers; however, in an 
effort to create a traditional high school type setting, students switch classrooms for six periods a 
day. This allows students the opportunity to move about throughout the day and to also receive 
instruction from different teachers and be exposed to different teaching styles. 
 
Throughout the day, students are given assistance from an instructional assistant where they have 
the opportunity to make up credits using the Grad Point web based credit recovery program. This 
program gives students additional credits in all subject areas. Students work at their own pace to 
earn credits in areas that they are deficient. They are allowed to work on this program once all of 
their classwork is completed. Students also have the opportunity to work on Grad Point in their 
homes. Progress is tracked online by the students and staff and recorded towards the students' 
graduation requirements. 
 
MCHS and MCDS students can also attend after school tutoring that is offered four days a week 
with an instructor that can provide class instruction as well as allow students to work on Grad Point. 
Students that are behind and attempting to recover credits are encouraged to take advantage of 
this weekly opportunity. 
 
Parent contacts and appointments are made where valuable information can be shared with 
parents and students in the presence of the teachers and school principal as needed throughout 
the semester. This gives parents an opportunity to gain an understanding of the school programs 
as well as to see the academic plan of each student at Mendota Continuation HIgh School and 
Mendota Community Day School. 
 

2017-18 School Accountability Report Card for Mendota Continuation High School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Mendota Continuation High School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

2 

0 

0 

2 

0 

0 

3 

0 

0 

Mendota Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

148 

14 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.