Abraxas High School, the continuation high school for the Poway Unified School District, takes great pride in its commitment to 
students. Set on a campus that was designed and built specifically for alternative education in 1978, the school was completely 
renovated and modernized in 2008. Abraxas High School was designated a Model Continuation School in 1998, 2006, and again in 
2010. The main campus consists of twenty-four classrooms and includes a computer lab, art room, science lab, media center, and 
administration building. The campus has state of the art computers and mobile computer carts for each classroom to use. The 
buildings surround lawns and an extensive patio area with two gazebos. A large grassy field serves as a multipurpose soccer, softball 
and PE facility. Also included are handball/racquetball courts, a tennis court, basketball and volleyball courts, and a small weight lifting 
patio. 
 
The student population of approximately 250 is made up of young adults between the ages of 15 and 22, with the majority of those 
students residing within the Poway Unified School District boundaries. Abraxas houses two programs- a Diploma Program serving high 
school aged students and a Transition Program for special-needs young adults. 
 
The Diploma Program delivers instruction that is individualized, based on student needs, across a continuum of methods ranging from 
traditional classroom delivery to independent study. The instructional model is designed to accommodate a fluctuating student 
enrollment. Curricular offerings comply with State Model Curriculum Standards and PUSD requirements for graduation and are 
personalized to meet the needs of students with a wide range of skill levels, motivation and aspirations. Special education RSP and 
SDC services are provided as needed. Every student works with a homeroom teacher to develop an individualized program and to 
prioritize goals. Teachers help to develop student's confidence and self-esteem, as well as develop a sense of responsibility and 
accountability. Credits are issued as soon as they are earned, with an emphasis on individual student learning. 
 
The Work Experience Program emphasizes job shadowing, mentors and guest speakers, along with classroom instruction about job 
skills and learning how to obtain and keep a position. The program includes students who are employed full time, working up to 32 
hours per week, and are monitored by the Work Experience Coordinator. The full-time work experience students are required to be 
on site, four hours each week. 
 
The Independent Study (IS) Program provides an educational setting for up to 10% of students who are not able to attend school daily 
during regular school hours. Students are required to meet with the IS teacher once each week to present and review the twenty plus 
hours of work completed from the previous week. 
 
The Transition Program serves students with special needs. The program has an enrollment of approximately 80 students with 
moderate to severe disabilities. The program is community-based and follows the Individualized Critical Skills Model that meets the 
needs of 18- to 22-year old adults with developmental disabilities. The Transition Program also facilitates individual development in 
the areas of transportation/mobility within the community, daily living skills such as budgeting, banking, cooking, and utilizing 
resources in the community. The emphasis is always toward maximizing an individual's potential for independence and self-sufficiency. 
 
Abraxas benefits from a full-time Student Services Coordinator on site. The responsibilities include coordinating the mentor program, 
providing student support in the areas of substance abuse, tobacco cessation and peer mediation/counseling. 
 
Mission Statement 
 
Prepare young adults for high school graduation and to be responsible, caring and contributing members of society 
 
Our Vision of Abraxas High School: 
 
A school climate that is safe, supportive and personalized 

2017-18 School Accountability Report Card for Abraxas Continuation High School 

Page 2 of 13 

 

 
A culture that values diversity and is committed to high expectations for all 
 
A relevant and rigorous curriculum that integrates academic learning and essential life skills 
 
Individual and collaborative approaches to engage students in learning 
 
Attention to individual students, enhancing their ability to make positive life-long choices