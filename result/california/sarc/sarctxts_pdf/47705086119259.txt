Principal’s Message 
The Yreka Community Day School is one of three schools in the Yreka Elementary School District. It presently serves students in third 
through eighth grades. Students who attend Yreka Community Day School have instruction in the core subject areas but also benefit 
from instruction in library science, physical education, fine arts, as well as having the services of a nurse. 
 
Our goal at Yreka Community Day School is to provide every student with an alternative educational setting so that those students 
who are having difficulty in the comprehensive setting can have a place to go and work at their own pace. By so doing, they catch up 
to where they should be academically. The ultimate goal is to take a student who is struggling and get him or her to the point to be 
able to return to the comprehensive school setting. 
 
District’s Mission Statement 
It is the mission of the Yreka Union School District to provide each student with a safe, caring and supportive atmosphere which will 
foster the intellectual, emotional, and social growth necessary to become a productive and responsible citizen who accepts cultural 
differences. All parents, students, teachers and staff are part of a supportive team helping children develop personal, educational, 
social, and ethical values.