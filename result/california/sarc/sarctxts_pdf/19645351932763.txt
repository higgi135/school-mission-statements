Housed in a classic 1927 landmark building, El Segundo High School claims the mantle of “California 
Distinguished High School,” an award bestowed upon the campus in spring of 2003 and again in 
2007. Furthermore, in 2015, Newsweek Magazine placed ESHS in the top 500 of high schools 
nationally. Additionally, the El Segundo Unified School District, in 2007, had the honor of being 
named one of Standard and Poor’s twenty-nine “academic out performer” school districts in the 
state, giving new meaning to the El Segundo High School motto: “Enter To Learn, Go Forth for 
Service.” In 2015, El Segundo was named an Apple Distinguished School, and in 2018 was awarded 
the Golden Bell Award. El Segundo High School not only weaves into its cultural fabric the beliefs 
embraced by all its stakeholders: students, faculty, parents, and community, but also proudly 
verbalizes those same ideals in its Mission Statement. 
 
El Segundo High School is a comprehensive (grades 9-12) high school featuring more than 150 
courses. It maintains a strong, mostly college preparatory, academic program. Courses include 
Advanced Placement, Honors, PLTW Engineering and Biomedical Science Pathways, Arts and 
Business Pathways, Special Education, English Language Development, reading intervention 
programs, as well as vocational programs both on campus and at the Southern California Regional 
Occupational Center (SCROC) in Torrance. Students are also able to take advantage of various 
internships with local businesses. While over 63% of our students complete the University of 
California “A-G” requirements, approximately 57% (Class of 2016) are accepted to four-year 
universities. Approximately 40 percent choose community colleges or vocational education, and 
3% join the military or go immediately into the work force as full-time employees. Upon completion 
of their senior year, El Segundo High School graduates have a clear plan that outlines a path for 
college and career. 
 
Due to its small student size of approximately 1200 students, El Segundo High School students enjoy 
substantial personal attention from teachers, counselors, coaches, and administrators. 
Approximately 700 students are involved in campus clubs and activities. In any given season, more 
than 20% of students participate in sports. 
 
Mission Statement 
El Segundo High School will provide students with the necessary skills and knowledge to become 
life long learners, effective communicators, and socially productive citizens who are prepared for 
life choices and challenges in a global society. 
 

2017-18 School Accountability Report Card for El Segundo High School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

El Segundo High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

53.2 

49 

49 

1 

0 

1 

0 

1 

0 

El Segundo Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

155 

2 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

El Segundo High School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.