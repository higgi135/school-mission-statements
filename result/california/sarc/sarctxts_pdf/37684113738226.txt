Sweetwater High is extremely proud that in 2014-2015 they received a full WASC accreditation of 6 years with a 3 year review. 
 
The mission of Sweetwater High School, the distinguished, educational cornerstone of National City committed to academic, social 
and ethical growth for its diverse students, is to ensure graduates possess skills necessary for success in post-secondary education and 
career aspirations and become contributing members of their community through a system of learning characterized by: 

 

• A rigorous and relevant curriculum for all students 
• A culture of learning, emphasizing high expectations for all students, staff and parents 
• A caring professional staff leading students to maintain healthy relationships and make positive life choices 
• A community focused on accountability, assessment and achievement for all stakeholders 
• Resources and opportunities to maximize student achievement and potential 

Sweetwater High School is extremely proud of the achievements of its students. The pride felt by students, faculty and staff is palpable. 
Recognitions abound: district, state and community officials continue to recognize the achievements of Sweetwater High School. 
 
We are extremely proud of our teaching staff and all of their efforts in maximizing student achievement. The Sweetwater staff has 
continued to overcome the many challenges faced by our student body and the community at-large. All of our students are anxious 
to achieve. 
 
The myriad of programs offered at SUHI complement student academics and they are key to allowing students to showcase their 
talents. We are proud of our award winning band, choir, MCJROTC, athletic teams, our after school programs and our very active 
Associated Student Body. 
 
Students in the Sweetwater Union High School District are expected to master state and district standards which will prepare them to 
meet the challenges of the 21st century. 
 
Sweetwater High School’s after school and intervention programs are standards based interventions geared for all students’ individual 
needs.