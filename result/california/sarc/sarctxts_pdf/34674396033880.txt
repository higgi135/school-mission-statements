The mission of David Lubin Elementary School is to instill a love of scientific inquiry and application 
and to empower students to pursue a life of choices by learning in an interdisciplinary environment 
that emphasizes the strong work ethic and critical thinking needed to solve problems in the real 
world. 
 
Our focus on creativity, critical thinking, communication and collaboration is essential to prepare 
students for the future. To be effective in the 21st century, today our children must learn to create, 
evaluate, and effectively utilize information, media, and technology. 
 
We recognize an unprecedented opportunity to engage and include all students in the challenge 
and promise made possible through a rigorous innovative STEAM program. As a designated STEAM 
school, we will establish science, technology, engineering, arts, and math as an integral part of 
learning for a future in which science belongs to all of us. Tomorrow's opportunities begin today. 
 
 
 

 

 

----

---- 

----

Sacramento City Unified School District 

---- 

5735 47th Avenue 

Sacramento, CA 95824 

(916) 643-7400 
www.scusd.edu 

 

District Governing Board 

Jessie Ryan, President, Area 7 

Darrel Woo, 1st VP, Area 6 

Michael Minnick, 2nd VP, Area 4 

Lisa Murawski, Area 1 

Leticia Garcia, Area 2 

Christina Pritchett, Area 3 

Mai Vang, Area 5 

Rachel Halbo, Student Member 

 

District Administration 

Jorge Aguilar 

Superintendent 

Lisa Allen 

Deputy Superintendent 

Iris Taylor, EdD 

Chief Academic Officer 

John Quinto 

Chief Business Officer 

Cancy McArn 

Chief Human Resources Officer 

Alex Barrios 

Chief Communication Officer 

Cathy Allen 

Chief Operations Officer 

Vincent Harris 

Chief Continuous Improvement & 

Accountability Officer 

Elliot Lopez 

Chief Information Officer 

Christine Baeta 

Instructional Assistant Superintendent 

 

2017-18 School Accountability Report Card for David Lubin Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

David Lubin Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

22 

28 

28 

0 

0 

0 

0 

0 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

David Lubin Elementary School 

16-17 

17-18 

18-19