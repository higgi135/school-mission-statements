Eagle Peak Middle School is in its 21st year of operation, having opened in August of 1997. It is one 
of two middle schools in the Ukiah Unified School District and is located approximately eight miles 
north of the city of Ukiah in the rural community of Redwood Valley. The design of the campus is 
characterized by two story buildings that house classrooms and encircle the library/administration 
complex. Eagle Peak serves 5th, 6th, 7th and 8th grade students, with a population of 
approximately 520 students. This is the eighth year Eagle Peak has served 5th grade students as a 
result of the closure of Redwood Valley School. Twenty-eight certificated staff and thirty-two 
classified staff are employed at Eagle Peak. The school’s administration includes a principal, 
assistant principal, and counselor. The school runs a traditional six period secondary schedule five 
days a week, for grades 6 through 8, and self-contained classrooms for grade 5. 
 
Now in its sixth year, Eagle Peak hosts a specialized program for students with autism. 
 
In October of 2017, the Redwood Valley Fire destroyed hundreds of homes in the Redwood Valley 
Community. Eagle Peak partnered with local agencies, clubs, and community members to support 
the fire victims. 
 
In December of 2016, the Ukiah Unified School District Board of Trustees approved a plan to change 
Eagle Peak in to a STEM magnet school beginning in the fall of 2017. 
 
Our STEM vision statement: 
We believe all young people should be taught to think deeply and critically, preparing them to 
become the innovators, educators, researchers, and leaders who can solve the most pressing 
challenges facing our nation and our world. To that end, we will provide hands-on learning with a 
focus on science, technology, engineering and mathematics in a supportive, small-school 
environment, putting students on a path to pursue college and graduate school education that 
prepares them for employment in high demand and high paying STEM fields. 
 
School Vision and Mission: 
The Eagle Peak administration and staff are committed to providing students with the 
understanding and skills necessary to cope with their own changes from childhood to adolescence 
and with the academic skills and knowledge essential for continued school success. Specific goals 
include: 

• Developing students with skills in reading, writing, speaking, listening, computer 
technology, and mathematics, while also developing students who are healthy and 
physically fit. 

• Developing students with good character, self respect, self worth and good citizenship. 
• Developing students with critical thinking skills and a commitment to lifelong learning. 

2017-18 School Accountability Report Card for Eagle Peak Middle School 

Page 1 of 8 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Eagle Peak Middle School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

19 

23 

21 

5 

1 

5 

0 

3 

0 

Ukiah Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

335 

22 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Eagle Peak Middle School 

16-17 

17-18 

18-19