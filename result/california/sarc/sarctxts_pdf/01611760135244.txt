Mission San Jose High School’s faculty and staff are committed to excellence in education, to high 
moral and ethical standards, and to the development of successful individuals. Mission San Jose 
has a tradition of educational excellence. We received a full six-year Western Association of Schools 
and Colleges accreditation in 2014. Mission San Jose graduates every senior, our 2018 graduation 
rate is 100%, and the attendance rate for the school is 99 percent. Ninety-five percent of our 
graduates matriculated to post-secondary education, with 85 percent attending four-year colleges 
or universities, including UC and CSU campuses. Of the graduating class of 2018 who went to 
college, 170 enrolled in UC campuses, 58 enrolled in CSU campuses, while 173 attended private or 
out-of-state schools. 
 
We have an extensive Advanced Placement (AP) program of 20 courses, including AP Chinese 
Language and Culture, AP Japanese, AP Music Theory, AP Computer Science, and AP Studio Art. 
Mission San Jose also offers three college level courses on campus including C++, Discrete Math, 
Multivariable Calculus, and Linear Algebra. Mission San Jose is the district leader in average SAT 
scores and continues to increase the mean score yearly, with a writing/reading average score of 
707 and math average score of 730 for this year. 
 
In 2009 Mission San Jose was nominated as a National Blue Ribbon School of Distinction for the 
second time. Mission San Jose was recognized in the Spring of 2015 as a California Gold Ribbon 
School for its exemplary programs to support all students, Challenge Success, STEM Success, 
Writers Block, and weekly Advisory/Collaboration. Over half of our approximately 2,000 students 
participate on one or more athletic team and there are 90 clubs and organizations on campus to 
support student interests. 
 
Our Mission Statement is as follows: “Preparing the Next Generations for the Global Community.” 
 
Mission San Jose High School prepares students for an innovative, evolving, advanced and culturally 
diverse global community. We believe that a solid, well-rounded education is the basis for personal 
and professional development. We create a positive, safe and supportive learning environment. 
We nurture the academic, personal, and social development of our students, preparing them for a 
lifetime of learning, service, and leadership. 
 
*Parents partner with the school in assisting students in their academic achievement and growth 
toward personal maturity. 
 
*Students engage in learning as active participants in their own development. 
 
*Teachers focus on creating a rigorous and relevant curriculum which will prepare students to be 
college- and-career-ready. 
 
*Teachers, counselors, and administrators all support rigor and relevance for the student and place 
an emphasis on ensuring that positive and collaborative relationships are established to foster 
deeper and more meaningful learning. 
 
 

2017-18 School Accountability Report Card for MISSION SAN JOSE HIGH SCHOOL 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

District 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

85 

83 

84 

0 

0 

0 

0 

1 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1713 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

0 

0 

0 

0 

0 

0 

0 

0 

Vacant Teacher Positions 
Notes: 
1) “Misassignments” refers to the number of positions filled by teachers who lack 
legal authorization to teach that grade level, subject area, student group, etc. 
 
2) Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners. 
 

0