Cesar Chavez Junior High School (CCJHS) is located in the community of Ceres in Stanislaus County. The school serves a student 
population of 651 students (330 7th grade students and 321 8th grade students). CCJHS opened in 2011 and reflects the community 
in that it serves a wide range of cultural, linguistic, and socio-economic stakeholders. The school's beautiful campus boasts state of 
the art classrooms, a library, a multipurpose room, a dance studio, a music building, an art room, and a gymnasium. The grounds 
include a quarter-mile track, a football field, basketball courts, a softball diamond, and a baseball diamond. Cesar Chavez offers a 
variety of core and elective classes to all students: English Language Arts (ELA), mathematics, social studies, science, physical education 
(PE), and various electives. Special education programs on campus include resource (RS), learning handicapped (SDC/LH), and severely 
handicapped (SH). 
 
Cesar Chavez Junior High School's Mission Statement is: T.I.G.E.R.S "As a Team, Cesar Chavez JHS is committed to providing an 
Innovative Goal-oriented Education with a Rigorous curriculum, leading to Success for all students." As educators we collectively agree 
to commit to learning for all students. The School Site Council (SSC) meets throughout the school year to provide input, review, and 
evaluate the School Plan for Student Achievement (SPSA), Local Control Accountability Plan (LCAP), and Site Strategic Plan; provide 
suggestions for future directions; and to oversee the general school improvement process. The English Learner Advisory Committee 
(ELAC) and Family Engagement Team (FET) also meet throughout the school year. In addition, the school staff encourages parent 
participation in the classroom in an ongoing effort to make parents an integral part of the education of their children. Parents are 
welcome to visit classrooms (as evidenced by Cesar Chavez's Parent Site Visit) and chaperone school sponsored events (such as school 
dances and field trips). Information regarding upcoming events and special activities is sent home regularly through the Parent Square 
messaging system, school website, social media (Facebook, Twitter, Instagram), Remind (a phone texting application), and monthly 
Tiger Times newsletter. 
 
CCJHS will continue to focus on incorporating literacy across all content areas, as well as gaining deeper knowledge of the California 
Math standards. Regarding English Learner (EL) students specifically, CCJHS has 24 that have been placed in an English Language 
Development (ELD) class, with the hope that their English will improve enough so that they can drop the ELD course. CCJHS ELs are 
specifically focused on, especially those that are considered Long Term English Learner (LTEL) students, to ensure academic growth. 
District instructional coaches work with CCJHS teachers and admin so that all become well versed in strategies to assist these students 
in growing academically. 7% of CCJHS students most recently tested in the overall Early Advanced and Advanced levels on the 
California English Language Development Test (CELDT), and because of this, CCJHS staff are hoping that approximately that percentage 
of students will attain Reclassified Fluent English Proficient (RFEP) status. In addition, ELs took the English Language Proficiency 
Assessments of California (ELPAC), which replaced the CELDT, for the first time at the end of the last school year. Data will be used 
from that assessment to make further decisions regarding CC's ELs.