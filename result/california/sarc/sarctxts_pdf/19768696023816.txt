Peter Burnett Elementary is one of three elementary schools located in the Wiseburn Unified 
School District (enrollment: 4,000) located just south of LAX. 
 
Burnett serves 477 3rd-5th grade students: 41.1% qualify for FRPM: 16.6% are ELL, 18% identify as 
white; 59% Hispanic/Latino; 9% Black, 8% 2 or more races, and 2% Asian. There are 22 classroom 
teachers which include ELD, RSP, and SDC, one full-time counselor, credentialed art and music 
teachers,and one Principal. Since 2010, Burnett has been identified as a Title 1 school and now 
implements a school-wide 1:1 Chromebook initiative. 
 
At Burnett Elementary we strive to bring out our BEST in all we do. While our BEST acronym guides 
us within our positive behavior reinforcement system (B-be responsible, E-effort all the way, S-
safety first, T-treat all with respect), it also stands for something more. As 21st century learners, 
we know that “to be our best” involves being engaged and reflective citizens of the world. Our 
students, teachers, and staff are all committed to putting their BEST foot forward and are eager to 
jump into learning with an open and curious mind. We see a culture of continuous improvement in 
all aspects of school life at Burnett. Students are learning to be flexible in their thinking, valuing 
mistakes as learning opportunities, and using one another as resources to enhance their learning. 
 
The learning community at Peter Burnett is supported by all parents, teachers, students, and staff. 
Through our PTA and Wiseburn Education Foundation we are able to provide our students with a 
curriculum that is well-rounded and rich with hands-on, engaging learning opportunities. Our 
community partnerships through local businesses such as Chevron, provide PLTW resources to help 
our students learn 21st-century science and engineering skills. 
 
The staff continues to work together at improving our Professional Learning Community, whereby 
teachers and other support staff collaborate by using local assessments to improve student 
achievement. Our teachers at Burnett are also on a path of continuous growth and modeling to 
their students the growth mindset in action. Teachers are taking the time to collaborate and think 
critically as we introduce our new Language Arts series, Wonders, and continue to develop and 
enhance our CGI curriculum. This process is showing our students that we are ALL lifelong learners 
and our capacity to be flexible and adapt and change over time, leads us to make deeper 
connections and insights into our learning. In addition, students who require more intensive 
support reading and language support receive intervention before or after school. Our students 
utilize programs such as Lexia, Literably, and Accelerated Reader 360 to strengthen students 
decoding, fluency, and comprehension skills. 

----

-

--- 

Wiseburn Unified School District 

201 N. Douglas Street 
El Segundo, CA 90245 

(310) 725-2101 

www.wiseburn.org 

 

District Governing Board 

JoAnne Kaneda 

Roger Bañuelos 

Neil Goldman 

Nelson Martinez 

Israel Mora 

 

District Administration 

Dr. Blake Silvers, Ed.D. 

Superintendent 

Dr. Aileen Harbeck, Ed.D. 
Assistant Superintendent, 

Educational Services 

Cathy Waller 

Director of Psychological Services 

Ana Montes 

Director of Human Resources 

David Wilson 

Chief Business Official 

Vince Madsen 

Director Facilities Planning 

 

2017-18 School Accountability Report Card for Peter Burnett Elementary School 

Page 1 of 10 

 

 
In Mathematics, we continue to deepen our understanding of mathematical concepts and practices through the lens of Cognitively Guided 
Instruction (CGI). The CGI philosophy emphasizes a student-centered approach to teaching mathematics that focuses on problem-solving, 
collaboration, and reflection. Students and teachers engage with concepts at a deeper level and utilize a wide variety of strategies to 
demonstrate their learning and understanding. Teachers are provided support through district led trainings as well as through support 
from the district sponsored Math TOSA. The Math TOSA is a mentor to classroom teachers and helps them delve deeper into their practice 
and enhance their daily instruction. This model mirrors the support that was provided through the Cotsen Foundation. Now in our third 
year of the teacher/mentor cycle, the focus has evolved to include training and support for all teachers in the area of CGI and to provide 
teachers with more opportunities to share practices with one another and look at student work to help guide instruction. In addition, My 
Math is the adopted textbook used by teachers as a primary component of their math instruction. Both ST Math and Simple Solutions are 
supplemental resources that help build students knowledge in key math concepts. 
 
A Multi-Tiered System of Support continues to be implemented to allow students to receive immediate and specific academic and 
behavioral assistance where needed. Students are identified for intervention by test results, teacher recommendation, and school and 
district assessments. Our Student Study Team added new procedures for staff members to review supports for students and provide 
strategies for parents to support their children at home. These procedures include utilizing both formative and summative assessments, 
classroom observations, as well as, considering a student's social-emotional well-being to best determine the appropriate course of action 
for support. This process involves our school counselor, intervention specialists, parents, and classroom teachers in collectively 
developing strategies to support positive student outcomes. 
 
This success is evident in our students' 2017-18 test scores. Burnett students demonstrated growth school-wide in English Language Arts 
and closed the achievement gap in ELA and Math two years in a row. Burnett's scores reflect positive growth as our teaching practices 
aligned with the state standards and curriculum and instruction became more student-centered and interactive within the classroom. 
Teachers collaborate to reflect on and refine best practices and use local assessments (observations, surveys, student work) to improve 
student achievement. In the Spring of 2018, Burnett was awarded the honor as a California Distinguished School. 
 
Being our BEST is a continuous process that involves compassion, collaboration, creativity, and community. The learning community at 
Peter Burnett strives to reach those goals each and every day.