Orrenmaa Elementary School is located in the southwestern region of Riverside and serves students 
in grades transitional kindergarten through five following a traditional calendar. 
 
At Orrenmaa, our vision is to create a school environment where ALL students, staff and the 
community are empowered with a growth mindset for learning in a culture that nurtures the 
social/emotional, physical fitness and academic skills to achieve life long excellence. This is 
accomplished through expanding professional development of all staff, engaging families and 
involving the community. In the area of social-emotional, we provide a safe learning environment 
by having shcool and district safety plans in place. We support a student behavior through Positive 
Behavior Intervention Support (PBIS). We also have our Bully Free Friends Club. The Bully Free 
Friends picks up character education and works with students on how to be a friend, how not to be 
a victim and how to treat others so no one is bullied. The school wears purple on Thursdays to 
support no bullying on campus. We are implementing Ron Clark strategies and skills school wide by 
focusing on an essential behavior each week. Students collaborate, encourage, and celebrate 
together with others who are in the same house as them. This year we started 100 Mile Club to 
encourage students to stay fit, while continuing to support the AUSD Wellness Policy. We have 
been recognized for our Silver status Alliance for a Healthier Generation Award. Finally, to engage 
in academic rigor, we are in our third year of AVID to develop students' academic and organizational 
skills to succeed in school. We also provide college and career awareness through College Tuesdays, 
with each class adopting a college and wearing the college shirts on Tuesdays. We have a yearly 
college rally for all colleges to be represented. This year we will have our 3rd annual College and 
Career Day, which TK through 5th grade participates in rotating through learning about career 
vehicles and listening to speakers. All these activities make up the culture of Orrenmaa, and families 
are encouraged to be involved with many parent opportunities. 
 
Mission Statement 
Alvord Unified School District, a dynamic learning community that embraces innovation, exists to 
ensure all students attain lifelong success through a system distinguished by: 

• Active and inclusive partnerships 
• Relationships that foster a culture of trust and integrity 
• High expectations and equitable learning opportunities for all 
• A mindset that promotes continuous improvement 
• Multiple opportunities for exploration and creativity 
• Professional development that promotes quality teaching and learning 
• Access to learning experiences that promote a high quality of life 

 
In addition, all schools strive to attain the Alvord vision that all students will realize their unlimited 
potential. 
 

-

--- 

----

---- 

Alvord Unified School District 

9 KPC Parkway 

Corona, CA 92879 

(951) 509-5070 

www.alvordschools.org 

 

District Governing Board 

Robert Schwandt, President 

Carolyn M. Wilson, Vice President 

Lizeth Vega, Clerk 

Julie A. Moreno, Member 

Joanna Dorado, Ed.D., Member 

 

District Administration 

Allan J. Mucerino, Ed.D. 

Superintendent 

Dr. Robert E. Presby 

Assistant Superintendent, Human 

Resources 

Susana Lopez 

Assistant Superintendent, Business 

Services 

Julie Kohler-Mount 

Interim Assistant Superintendent, 

Educational Services 

Susan R. Boyd 

Assistant Superintendent, Student 

Services 

Kevin Emenaker 

Executive Director, Administrative 

Services 

 

2017-18 School Accountability Report Card for Allan Orrenmaa Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Allan Orrenmaa Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

23 

23 

22 

0 

0 

0 

0 

0 

0 

Alvord Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

665 

5 

37 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.