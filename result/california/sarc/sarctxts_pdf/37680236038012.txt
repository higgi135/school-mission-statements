Originally a one-room schoolhouse, Sunnyside Elementary School is a historic school located in 
Bonita, within the boundary of the 49 schools of the Chula Vista Elementary School District. The 
school serves students in transitional kindergarten through sixth grade as well as several special 
day classes for students with special needs. The current building structure was built in 1959 and is 
scheduled to be extensively modernized during the summer of 2019. The school follows a modified, 
year-round calendar but will follow a traditional calendar during its upcoming summer of 
modernization. Sunnyside is a "community" school and is proud of its historic, “small town” feel. 
 
Sunnyside Elementary has 21 classrooms, a computer lab, a library, a child care room (YMCA), an 
auditorium and an administration building. The school serves a population of approximately 450 
students and maintains a variety of backgrounds, languages, and ethnicities with respect to the 
students and families it serves. Sunnyside students promote to Bonita Vista Middle School in the 
Sweetwater Union High School District. 
 
Sunnyside Elementary continues to fully implement Common Core State Standards (also known as 
the New California Standards), with a focus on improving reading ability, implementing speaking 
and listening standards, and aligning mathematics instruction with research-based practices. In 
alignment with the District's mission to close the achievement gap through high impact language 
development strategies, the teaching staff is committed to improving self-efficacy for all students 
by enhancing instructional routines using success criteria and feedback. General education 
classrooms and special day class classrooms alike receive weekly professional development and bi-
weekly collaboration time. 
 
Sunnyside Elementary is fortunate to have a very supportive and active Parent Teacher 
Organization, also known as "Parents' Club". Each year, this group of parents continues to make a 
significant contribution to our school and it's mission. Besides supporting technology, the PTO also 
supports experiences, events and assemblies for students in all grade levels. This year, the PTO 
technology grant will go toward the purchase of new computers in our computer lab. This has 
enabled the school to purchase school-issued laptops for each teacher and replace outdated 
technology in classrooms around the school. 
 
Sunnyside Elementary is a "Cal-Well" school and has placed a great emphasis on improving the 
social-emotional situations of its students. The school has adopted the Pillars of Character and 
hosts monthly lessons, activities, and recognition ceremonies concerning each character pillar. The 
school utilizes the Sanford Harmony curriculum and Harmony Circles are implemented throughout 
the school. It has an active Positive Behavior Interventions and Supports (PBIS) / School Culture 
team that drives the implementation of proactive structures like "Restorative Practices" and 
"Trauma-Informed Care". The school partners with the San Diego County Office of Education to 
house a Mental Health Intern through the Cal-Well Program on our campus which provides support 
to up to 40 - 50 students by fostering healthy self-concepts, social skills, and problem-solving skills 
for school and life success. 
 
Before-and-after school clubs and programs at Sunnyside include the Sunnyside Bulldog Band, 
FitKids America, Robotics, Coding Club, Garden Club, Mathletes, Extended Day, Jumpstart Reading, 
Student Council, YMCA Licensed Childcare, and DASH.

2017-18 School Accountability Report Card for Sunnyside Elementary School 

Page 1 of 11 

 

 
Sunnyside Vision Statement 
Our motto, "Bright Futures Begin at Sunnyside," supports our vision that each of our students does indeed have a bright future and that 
when all staff, parents and community work together on behalf of the children, we form a powerful alliance that will enable each child to 
reach his or her potential. To achieve our vision we are committed to ensuring that all students are provided a dynamic, well-balanced 
education in a nurturing, accepting, child-centered, environment where learning is viewed as a worthwhile, life-long adventure. 
 
Sunnyside Mission 
The staff of Sunnyside School endeavors to provide students an instructional program and a learning environment which will promote: 

Effective interpersonal and communication skills. 

• Mastery of skills needed to be college and career ready 
• 
• Creativity and an appreciation for the arts. 
• Responsibility and self-discipline. 
• Motivation for lifelong learning. 
• Acceptance and appreciation of others. 
• Knowledge of physical and emotional well being. 
• 

Technology Literate