Welcome to Wilson Elementary, we are proud to be recognized as a California Distinguished School! 
As of 2015-16 CDE (California Department of Education) is not longer reporting API (Academic 
Performance Index) scores. CDE continues to work on an updated assessment measurement which 
has not yet been published. But, we are proud of our 900 API from 2010. 
 
The annual school report was established as an accountability component of proposition 98, passed 
by California’s voters in November 1988. This report, which is issued by all elementary and 
secondary schools in the State, will provide you with information about our school’s achievements, 
resources, students and staff. 
 
Wilson Elementary School is known for a commitment to its tradition of academic excellence. We 
have determined students, staff, and parents working together as a team to provide a rich 
curriculum and high-quality programs. We are fortunate to have many experienced and 
knowledgeable staff members who are eager to assist students in their growth. For plan's 
questions or comments at (626) 287-0497 or to Principal Ms. 
Ivonne Contreras at 
contreras_i@sgusd.12.ca.us. 
 
School Mission Statement: 
The mission of Wilson Elementary School, in cooperation with our parents and community, is to 
promote the future success of our students by developing responsible citizens and critical thinkers. 
We encourage the joy of learning by providing an invigorating, stimulating, and challenging learning 
environment. Our whole-child approach develops our students’ self-worth and bridges individual, 
cultural, and academic differences. 
 
School Vision Statement: 
The Wilson Elementary School community shares a common vision with the greater San Gabriel 
Unified School District as a whole. The Wilson community is committed to provide all students, at 
all grade levels, with equal access to the core curriculum. The Wilson staff focuses on engaging and 
supporting students of all diverse backgrounds and various needs. 
 

----

---- 
San Gabriel Unified School 

District 

408 Junipero Serra Drive 
San Gabriel, CA 91776 

(626) 451-5400 

www.sgusd.k12.ca.us 

 

District Governing Board 

Cheryl A. Shellhart, President 

Dr. Gary Thomas Scott, Vice 

President 

Rochelle Kate Haas, Clerk 

Andrew L. Ammon, Member 

Christina Alvarado, Member 

 

District Administration 

Dr. John Pappalardo 

Superintendent 

Yolanda Mendoza 

Deputy Superintendent, 

Educational Services 

Joyce Yeh 

Assistant Superintendent of 

Business Services 

Ross Perry 

Assistant Superintendent, Human 

Resources 

 

2017-18 School Accountability Report Card for Wilson Elementary School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Wilson Elementary School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

18 

18 

17 

0 

0 

0 

0 

0 

0 

San Gabriel Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

234 

4 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Wilson Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.