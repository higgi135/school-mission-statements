The Gateway School, housed at Stokoe Elementary School, opened in July 2017 to provide walk-on 
speech services for preschoolers and other students who have an Individualized Education Program 
(IEP) plan. Services are provided by an Alvord Unified School District speech-language pathologist. 
 
Mission Statement Alvord Unified School District, a dynamic learning community that embraces 
innovation, exists to ensure all students attain lifelong success through a system distinguished by: 

• Active and inclusive partnerships 
• Relationships that foster a culture of trust and integrity 
• High expectations and equitable learning opportunities for all 
• A mindset that promotes continuous improvement 
• Multiple opportunities for exploration and creativity 
• Professional development that promotes quality teaching and learning 
• Access to learning experiences that promote a high quality of life 

---- 

Alvord Unified School District 

9 KPC Parkway 

Corona, CA 92879 

(951) 509-5000 

www.alvordschools.org 

 

District Governing Board 

Julie A. Moreno, President 

Robert Schwandt, Vice President 

Carolyn M. Wilson, Clerk 

Joseph J. Barragan, Member 

Art Kaspereen, Jr., President 

 

District Administration 

Allan J. Mucerino, Ed.D. 

Superintendent 

Lou Obermeyer, Ed.D. 

Interim Superintendent of Schools 

Susana Lopez 

Assistant Superintendent, Business 

Services 

Susan Boyd 

Assistant Superintendent, Human 

Resources 

Kevin Emenaker 

Executive Director, Administrative 

Services 

Angela Gallardo-Hopkins 

Executive Director of Elementary 
Teaching, Learning & Professional 

Development 

Julie Koehler-Mount 

Executive Director of Secondary 
Teaching, Learning & Professional 

Development 

 

2017-18 School Accountability Report Card for Gateway School 

Page 1 of 6