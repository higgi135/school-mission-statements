At April Lane Elementary School, we believe all children are able to learn. April Lane provides children with a positive and challenging 
educational experience in a caring environment that develops self-esteem, self-motivation, physical wellness, and responsibility. Our 
school strives to promote an enthusiasm for learning by recognizing and stimulating special talents in all students and providing every 
opportunity for maximum student achievement. The educational environment and teaching strategies are designed to meet the goals 
and objectives of a dynamic curriculum including Wonders and Go Math. Students in fourth grade are participating in the Arts Pilot 
Program which includes band, string instruments, and art. AVID strategies are taught to students in third through fifth grade. Students 
are preparing to become responsible citizens by learning to care for themselves, others, and the world community in order to take 
their places as productive members of an integrated, democratic society. We believe education is a cooperative effort involving home, 
school, and community.