New Vista Middle School staff provide a warm, nurturing, and academic learning environment for 
all students in grades 6, 7 and 8. As students advance through their three middle school years here, 
they transition from two partner teachers in the sixth grade into a departmentalized setting in 
grades 7 and 8. The staff is committed to the success of each child through the variety of structured 
programs, enrichments, and interventions. In addition to meeting our students’ academic needs, 
NVMS has created a culture of connectedness among students and staff through Capturing Kids’ 
Hearts, ASB Leadership, PLUS Student Leadership, AVID, Band, Coffees with the Counselors, 
Pastries with the Principal, Lunch Time Activities, Family Fun Nights, PTO, Cultural Heritage events, 
and many other positive activities on campus. Students enjoy coming to school as evidenced by 
our attendance rates. 
 
Our school-wide theme is focused on our college and career bound Tigers! This theme is supported 
through our growing AVID program that is implemented in our AVID elective and integrated in all 
courses in all grade levels. (AVID, Advancement Via Individual Determination, is sponsored by both 
district and site funding.) In addition to our AVID electives, we also provide a variety of electives in 
order to provide students with options as they pursue their personal best each day. AVID gives 
support in the areas of writing, inquiry, collaboration, organization, reading, note-taking, study 
skills, college and career research, and preparation for standardized tests. AVID students also 
receive additional support from college tutors during their AVID elective class 3 times a week. 
 
These additional electives include, AVID EXCEL (supports EL learners with language acquisition), 
STEM classes (focusing on Science, Technology, Engineering, and Mathematics), Band, PLUS (Peer 
Leaders Uniting Students), Band, Choir, Guitar, Art, Spanish I, Spanish II, Yearbook, Computer 
Literacy, Guided Study Hall, and Teacher Aide. Many of our students take advantage of extra and 
co-curricular activities on campus including sports, Campus Counselor Time, Robotics, GATE classes 
and enrichment activities, and CJSF (California Junior Scholastic Federation). 
 
We are committed to the continual refinement of our electives in order to further enhance the 
academic and extracurricular programs at NVMS so that all students are proud to call themselves 
Tigers as we support their unique abilities to attend college and pursue the career of their dreams. 
 

2017-18 School Accountability Report Card for New Vista Middle School 

Page 1 of 9