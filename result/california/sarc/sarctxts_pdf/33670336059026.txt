Motto: Bulldog PRIDE: Creating 21st Century Excellence. 
 
Mission: 
All students at Auburndale Intermediate School will strive for their personal best as scholars and positive citizens of our community. 
 
Vision: 
Auburndale Intermediate School creates learners through supported, rigorous academic curriculum, in a collaborative setting while 
instilling the necessary tools for success in high school and beyond. 
 
Auburndale Intermediate School is located in Corona, California, which is fifty miles southeast of Los Angeles, CA. Most of its residents 
commute to either Orange or Los Angeles County because of employment. The school consists of 7th and 8th grade students and 
follows a single-track traditional calendar. There are approximately 680 students at Auburndale. The ethnic breakdown is 77% 
Hispanic or Latino, 10% White not Hispanic, 6% African American, 4% Asian, 1% Filipino, 1% Pacific Islander. 77% of the students 
participate in the Free and Reduced Price Lunch Program. 23% of the students are English Language Learners, Approximately 14% of 
Auburndale students are bused to school. We have excellent STEM, Dual Language Immersion, AVID, WEB, Leadership, Advisory and 
Music programs that sponsor activities both during the school day and after school.