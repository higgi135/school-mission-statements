At Van Buren Elementary School we: 

• Collaborate around common data to implement best practices. 
Facilitate purposeful learning which leads to high achievement. 
• 
• 
Foster strong community connections which creates a welcoming, respectful, and safe 
learning environment. 

Mastery of Common Core grade level standards in reading, writing, and mathematics forms the 
foundation of our instructional programs. 
 
Science, social studies, physical education and visual and performing arts round out our curriculum. 
 
We are proud to offer our students 1:1 technology with a classroom set of laptops in every 
classroom, Transitional Kindergarten through 8th grade. 
 
We continue to grow our collection of library books each year, to ensure that our students have 
access to multiple genres and endless choice of titles to enjoy. 
 
We also continue to build strong collaboration and partnerships with our parents and families. 
 
Their involvement and participation is needed and valued. Communication is a vital component to 
the success of our students. A school-wide newsletter, the Panther Press, goes home monthly and 
weekly information can be found on our website and on the marquee in the front of our school. 
For more information, pictures, and celebratory news, please view the Van Buren Elementary 
School web site at http://www.stocktonusd.net/VanBuren. 
 
We look forward to working together with the community to create new opportunities and new 
accomplishments for our students this year. 
 

2017-18 School Accountability Report Card for Van Buren Elementary 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Van Buren Elementary 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

19 

18 

22 

5 

0 

6 

0 

6 

0 

Stockton Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

1517 

266 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Van Buren Elementary 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

1 

0 

0 

0 

0 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.