For over 50 years Warren High School has offered a strong academic and co-curricular program to 
its students. Warren High offers many Honors and Advanced Placement (AP) level, as well as highly 
specialized Career and Technical Education (CTE) pathways. Students can access the Animation, 
Construction Technology, Culinary Arts, Film and Television, and Project Lead the Way 
(engineering) pathways as early as the ninth grade, getting hands on experience in state of the art 
facilities with industry level equipment. Our Advancement via Individual Determination (AVID) 
program helps students who are the first in the family to go to college to complete the college 
entrance requirements. The program enjoys a 100% acceptance rate to four-year colleges and 
universities for seniors completing the program. A wide range of academic and scholarship 
recognitions are offered, including the California Scholarship Federation program and the 
Distinguished Graduate program. 
 
In addition to the academic program, Warren High School offers outstanding co-curricular 
programs including twenty-two different athletic teams for boys and girls. Students at Warren have 
the opportunity to participate in award winning fine arts programs which include vocal and 
instrumental music, art, as well as theatre and dance. The school has a dynamic student activities 
program which includes a large number of service focused clubs. 
 
If you have any questions or are interested in making an appointment to discuss this report, please 
call our school. 
 
Laura Rivas, PRINCIPAL 
 
 

 

 

----

--

-- 

Downey Unified School District 

11627 Brookshire Ave. 
Downey, CA 90241-7017 

(562) 469-6500 
www.dusd.net 

 

District Governing Board 

Tod M. Corrin 

Donald E. LaPlante 

D. Mark Morris 

Giovanna Perez-Saab 

Barbara R. Samperi 

Martha E. Sodetani 

Nancy A. Swenson 

 

District Administration 

John A. Garcia, Jr., Ph.D. 

Superintendent 

Christina Aragon 

Associate Superintendent, Business 

Services 

Roger Brossmer 

Assistant Superintendent, 

Educational Services - Secondary 

Wayne Shannon 

Assistant Superintendent, 

Educational Services - Elementary 

Rena Thompson, Ed.D. 

Assistant Superintendent, 

Certificated Human Resources 

John Harris 

Director, College and Career Ready 

Veronica Lizardi 

Director, Instructional Support 

Programs 

Alyda Mir 

Director, Secondary Education 

Marian Reynolds 

Administrator, Student Services 

Patricia Sandoval, Ed.D. 

Director, Special Education 

 

2017-18 School Accountability Report Card for Warren High School 

Page 1 of 10 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Warren High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

144 

139 

139 

2 

1 

2 

0 

1 

0 

Downey Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

861 

13 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Warren High School 

16-17 

17-18 

18-19