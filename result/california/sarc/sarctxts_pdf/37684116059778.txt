Granger Junior High School is a three-year junior high school. Granger opened in 1955, and now serves around 1000 students in grades 
7 – 9. In 2010, Granger was named a California Schools To Watch—Taking Center Stage Model School and a California Business for 
Education Excellence Honor Roll School, in recognition of routines that promote academic excellence and close the achievement gap. 
 
The school offers a rigorous curriculum which includes programs for the gifted and talented, as well as an extensive schoolwide literacy 
program. All 8th grade students are enrolled in Algebra and all 9th grade students are enrolled in Biology. Band is also a very healthy 
program at Granger, gaining special recognition and is a source of school pride. 
 
Students in the Sweetwater Union High School District are expected to master state and district standards which will prepare them to 
meet the challenges of the 21st century. 
 
The mission of Granger Junior High School is to provide a climate and culture that ensures that each student experiences equitable 
growth and success in literacy and curricular skills, which will create a foundation for students to establish and achieve personal, 
educational, and career goals. 
 
STUDENTS DEVELOP: 
• Goal Setting 
• 
• 
• 
• 

Community Service 
Critical Thinking Skills 
Citizenship Skills 
Commitment to Academic Success 

 
STAFF FOSTER: 

• Growth Mindset 
• 
• 
• 
• 

Positive School Culture 
Critical Thinking Skills 
Positive Relationships with Students, which include Restorative Practices 
Equitable Instructional Practices across All Subjects 

 
THE COMMUNITY SUPPORTS: 

College and Career Readiness 
Integration of Community Service, Instruction, and Reflection 
Partnerships in Education 

• 
• 
• 
• Academic Success 
• 

Family Involvement