Principal’s Message 
Skyridge School reflects a commitment that all students can learn and supports all students to be successful. Staff and parents work 
together to create a positive, encouraging, exciting learning environment to improve all student achievement. Throughout the campus, 
there is a focus on standards-based education and project based learning. During the school year, students are recognized for academic 
achievement, responsibility, improvement, and attendance. Our staff, teachers, parents, students, and PTC will continue to work 
together for the advancement of the students in meeting academic, social, physical, and emotional growth. Skyridge displays great 
teamwork. 
 
Mission Statement 
Each child in the Auburn Union School District will think analytically, solve problems, work cooperatively, explore creatively, and master 
common core standards. No matter their ability and background, students will be challenged and engaged, and obtain college and 
career readiness skills for a globally connected society. 
 
Vision Statement 
We stand together to place each child at the heart of every decision.