Prospects High School is an Alternative Education Program designed to offer students in the AUSD an alternative to the comprehensive 
high school model. It is an independent study program which allows students to complete their high school courses at home; with 
direction from a certificated teacher. It is designed for the student who is self-motivated and requires minimal instruction to complete 
their course assignments. Students meet with their teacher one to two hours per week to receive the week's lessons, correct 
homework, take tests and receive instruction. Prospects offers a high school diploma and is WASC accredited. 
 
It is Prospects High School's mission to provide students with the values, skills, and knowledge necessary to ensure lifelong learning, 
strong self-esteem, and responsible citizenship. The vision of Prospects High School is to provide a safe environment and a nurturing 
community where learners grow in knowledge, confidence and independence. Our school community has embraced the following 
core values: We believe that all students can learn We believe that every student deserves opportunities We believe that honoring 
diversity strengthens us We believe academic success is the only option