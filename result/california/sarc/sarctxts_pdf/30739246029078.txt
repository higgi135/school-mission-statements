The pursuit of excellence at Lee School begins with its Mission Statement that ensures the success of every scholar in a safe and 
nurturing environment that fosters life long learning. The School Site Council, teachers and principal review and update our mission 
statement annually to insure it reflects the educational philosophy embraced by our staff and community: We make a difference, one 
scholar at a time. We believe all children can learn and that we are here because of our commitment to the success of each scholar. 
The School staff strives to create a positive and healthy school climate which fosters responsible behavior, model citizens of character 
and a passion for learning. Our mission statement continues with our belief that: 
 
All scholars will be active participants in an educational process where they: 

• Meet or exceed state identified, grade level standards in language arts, mathematics, science and history/social studies. 
• Develop and apply problem-solving and critical-thinking skills in a program that deals with real-world issues and challenges. 
• Use technology as a tool for learning, for acquiring, analyzing, and using information, and for accessing the wider world of 

people and ideas. 

• Demonstrate consideration for others, respect and celebrate diversity. 
• 
• Develop their special talents in academic areas, as well as in the arts. 

Learn to work cooperatively and independently, making choices and taking responsibility for their learning and behavior. 

 
 The heart of the school’s mission centers on rigorous curriculum, high expectations, skillful staff members, continual staff 
development, character education, social responsibility and family/community partnerships. By continually promoting character 
development, we prepare scholars to become responsible citizens in our diverse society. Small class size, passionate educators 
devoted to student learning coupled with a generous and supportive parental community provides a continuous opportunity for 
optimal learning.