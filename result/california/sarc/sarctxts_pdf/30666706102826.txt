Douglas MacArthur Fundamental Intermediate School (MacArthur Fundamental) is located at West 
Alton Avenue and Flower St., next to Lillie King Park in the city of Santa Ana. MacArthur 
Fundamental is a public school of choice for students in grades six through eight. As a school of 
choice, students residing in the district's attendance boundaries are eligible for enrollment through 
a lottery system. Students attending a fundamental elementary school within the district are given 
first priority for enrollment. At MacArthur Fundamental, students, staff, and parents are 
committed to academic excellence, citizenship, patriotism, and respect for all. As a fundamental 
school, MacArthur Fundamental is committed to educating students through a highly-structured 
program of standards-based academic skills and enrichment and the establishment of good study 
habits. Staff members share the common goals of instilling within each student a sense of 
responsibility, patriotism, pride in accomplishment, and a positive self-image. These goals are 
accomplished by a commitment from our staff, parents, and students and are supported with 
accountability. 
 
School Vision: 
Students, staff, and parents are committed to excellence in academics, citizenship, patriotism, 
responsibility and respect for all. 
 
School Mission: 
Provide a balanced, comprehensive, standards-based core curriculum with a steadfast emphasis on 
high expectations and accountability for all students to prepare students for the 21st century. 
 
District Profile 
Santa Ana Unified School District (SAUSD) is the seventh largest district in the state, currently 
serving nearly 49,300 students in grades K-12, residing in the city of Santa Ana. As of 2017-18, 
SAUSD operates 36 elementary schools, 9 intermediate schools, 7 high schools, 3 alternative high 
schools, and 5 charter schools. The student population is comprised of 80% enrolled in the Free or 
Reduced Price Meal program, 39% qualifying for English language learner support, and 
approximately 13% receiving special education services. Our district’s schools have received 
California Distinguished Schools, National Blue Ribbon Schools, California Model School, Title I 
Academic Achieving Schools, and Governor’s Higher Expectations awards in honor of their 
outstanding programs. In addition, 20 schools have received the Golden Bell Award since 1990. 
 
Each of Santa Ana Unified School District’s staff members, parent, and community partners have 
developed and maintained high expectations to ensure every student’s intellectual, creative, 
physical, emotional, and social development needs are met. The district’s commitment to 
excellence is achieved through a team of professionals dedicated to delivering a challenging, high 
quality educational program. Consistent success in meeting student performance goals is directly 
attributed to the district’s energetic teaching staff and strong parent and community support. 
 

----

---- 

Santa Ana Unified School District 

1601 East Chestnut Avenue 
Santa Ana, CA 92701-6322 

714-558-5501 
www.sausd.us 

 

District Governing Board 

Valerie Amezcua – Board President 

Rigo Rodriguez, Ph.D. Vice – 

President 

Alfonso Alvarez, Ed.D. – Clerk 

John Palacio – Member 

 

District Administration 

Stefanie P. Phillips, Ed.D. 

Superintendent 

Alfonso Jimenez, Ed.D. 
Deputy Superintendent, 

Educational Services 

Thomas A. Stekol, Ed.D. 
Deputy Superintendent, 
Administrative Services 

Mark A. McKinney 

Associate Superintendent, Human 

Resources 

Daniel Allen, Ed.D. 

Assistant Superintendent, K-12 

Teaching and Learning 

Mayra Helguera, Ed.D. 

Assistant Superintendent, Special 

Education/SELPA 

Sonia R. Llamas, Ed.D., L.C.S.W. 
Assistant Superintendent, K-12 
School Performance and Culture 

Manoj Roychowdhury 

Assistant Superintendent, Business 

Services 

Orin Williams 

Assistant Superintendent, Facilities 

& Governmental Relations 

 

2017-18 School Accountability Report Card for Douglas MacArthur Fundamental Intermediate School 

Page 1 of 9