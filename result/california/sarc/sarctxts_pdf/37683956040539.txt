MISSION STATEMENT 
Our highest priority at Sunnyslope School is to develop curious, joyful, culturally competent, and proficient literacy users that embrace 
a “growth mindset” needed to thrive in the 21st Century. We support our students towards advancing their literacy and numeracy 
skills, particularly in the areas of reading comprehension and conceptual understanding. By engaging students at high levels of 
cognition through academic speaking, reading, and writing centered around complex and authentic tasks, Sunnyslope students will 
graduate with pride in themselves and their community, with a sense of curiosity and perseverance, and with confidence in their 
academic and social abilities to meet the challenges of an ever-changing world. The fundamental belief that every child can learn and 
will rise or fall to the level of our expectations is the driving force of our work. 
 
The mission of Sunnyslope is to work collaboratively to engage and empower students in their quest toward academic excellence and 
transformational leadership. We model respect, responsibility, and high expectations of excellence so that each individual can thrive 
and leave Sunnyslope with the confidence and intellectual capacity to influence positive change in the world. 
 
SCHOOL OVERVIEW 
Sunnyslope Elementary School (grades TK-6) is located in San Diego, California with a student enrollment of approximately 520 
students. The school action plan is the result of a coordinated effort involving Sunnyslope School faculty, support staff, and 
parents/guardians in collaboration with key South Bay Union School District leadership personnel. 
 
Sunnyslope School serves a diverse student population and offers a dual language program to promote bilingualism and bi-literacy. 
The predominant ethnic group is Hispanic with a population of 90%. Our ethnicities include 3% white, 2% African American, 5% Asian, 
Filipino,or Pacific Islander. Sixty-one percent of our students speak a language other than English and approximately 98% of these 
students have Spanish as a primary language. In addition, 77% of students are identified as socio-economically disadvantaged and 
88% of students pertain to LCFF unduplicated count.