Mission: 
At Lost Hills Union School District, we are fully committed to ensuring students become self-motivated, critical thinkers, and productive 
members of society, through high expectations in collaboration with parents and community. 
 
Vision: 
The Vision of Lost Hills Union School District is to create highly successful schools where students achieve academic excellence in a 
safe, supportive environment. 
 
Core Beliefs: 
We believe that: 
* Lost Hills Union School District is essential to our community's growth; 
* Our principal, teachers, and staff make the critical difference in student achievement; 
* We are responsible for building and maintaining high performing schools that ensure all students will successfully acquire the 
knowledge, skills, and values necessary for success; 
* Engaging the students' families and the community in the education process enhances learning and academic achievement; and 
* Building positive relationships and cultural understanding will create a welcoming, safe learning environment for all students, 
parents, and staff. 
 
Superintendent’s Message 
Lost Hills Union School District is guided by the goal of providing the best schools possible. To reach this goal, the District has 
committed to three objectives. First objective is high levels of student achievement consisting of academics, and citizenship. Second 
objective is safety which promotes learning. The third objective is professional growth for staff to meet the changing needs in 
education. These goals are incorporated into the district’s vision and mission statements, and emphasized during our meetings with 
staff and parents. Being the best school possible allows students to be successful in academics, citizenship and in their careers. 
 
Community and District Profile 
The Lost Hills Union School District was formed in 1940 from antecedents dating to 1885. Located in rural northwestern Kern County, 
approximately 45 miles from the central Bakersfield area. The District covers 501 square miles and is composed of mostly large farms. 
The District maintains two schools, Lost Hills Elementary School (grades TK-5), and A.M. Thomas Middle School (grades 6-8). During 
the 2017-18 school year, Lost Hills Union School District had a total enrollment of 510 students. A.M. Thomas Middle School had a 
total enrollment of 186 students. Additionally, A.M. Thomas Middle School's 2017-18 student population consisted of 98.2% Hispanic, 
75.3% Socioeconomically Disadvantaged, 42.7% English Learners, 26.7% Migrant, and 6.4% Students with Disabilities. 
 
The staff at A.M. Thomas Middle School is dedicated to providing students a challenging academic program that promotes individual 
and community responsibility. Each year our students gain experiences that prepare them to become successful contributors to 
society. The instructional focus is geared towards the English Language Learner (ELL) program and the Migrant Sheltered English 
Program. These programs include hands-on project-based instruction, especially in the sciences. 
 
The goal at A.M. Thomas Middle School is to provide an excellent education through our dedicated teachers and administration. 
 
 

2017-18 School Accountability Report Card for A. M. Thomas Middle School 

Page 2 of 11