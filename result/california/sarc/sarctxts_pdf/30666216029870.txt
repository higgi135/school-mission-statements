Perched amidst the foothills overlooking Orange, Olive Elementary School is a traditional track, kindergarten through sixth grade 
school of approximately 340 students and twenty-five staff members. The school is situated on a historical site which has consistently 
housed various elementary schools since June 6, 1876. The original school bell is mounted at the entrance to Olive school. The 
gymnasium, once the Olive Civic Center building, has been designated a historical landmark by the U.S. Department of the Interior. 
Prior to Olive's opening as a school, the property was a stopping point on the El Camino Real, a rest stop halfway between the San 
Juan Capistrano and San Gabriel of Los Angeles Missions. Olive School was re-opened as a public school by the Orange Unified School 
District in September of 1999. 
 
We value and honor our cultural diversity and are proud of the cross-cultural participation in school events. We provide a strong, 
integrated curriculum which includes Language Arts, Math, Science, Social Sciences, Social EMotional Learning, and the incorporation 
of instructional technology. We focus on the development of a balance of skills, knowledge, understanding, and application. Olive 
Elementary is committed to providing a success-oriented and safe learning environment for all students. 
 
It is my pleasure to invite you to read our School Accountability Report Card. Our staff models the belief that learning is a life-long 
journey. We have a strategic framework for whole school improvement based upon an analysis of student achievement and the 
selection of research based instructional strategies. Teachers are trained in a variety of instructional strategies with an emphasis on 
the 21st Century Teaching and Learning: the 4 Cs: communication, collaboration, critical thinking and creativity. We recognize that in 
order to empower our students and prepare them for the demands of the future, we also need to empower ourselves. We have an 
Instructional Leadership Team which meets regularly to guide our staff through the process of effective research-proven strategies 
and practices. We have two mobile laptop carts, each with 40 laptops in our 6th grade classrooms, full Chromebook/HP Stream carts 
in first--fifth grade classrooms, as well as additional iPads in all classrooms so that every student has access to technology. Instructional 
technology is used to increase and enhance student and teacher access to information, their ability to use data, their productivity and 
creativity. 
 
Our Olive families and staff share a common vision to create a community of life-long learners who possess strong character traits of 
respect and responsibility. The staff is committed to treating students with dignity and creating an environment that provides 
maximum opportunities for learning. Our goal is to provide students with a strong educational foundation so that they leave Olive 
Elementary with educational and personal experiences that have prepared students academically, and instilled problem solving and 
critical thinking skills necessary to become successful 21st Century Learners. We provide each student with a challenging and rigorous 
curriculum that addresses Common Core State Standards. We believe every child can achieve academic success. Our staff is dedicated 
to providing every student with quality instructional experiences which recognize, support, and maintain high expectations. Our school 
district supports our vision by providing staff development, instructional coaching, and support district wide. 
 
The mission of Olive Elementary School is to become a high performing community school that engages all students in exemplary 
teaching and learning experiences that are rigorous, differentiated, meaningful and take place in a safe and orderly environment! 
 
As never before, we will focus on making our vision and mission the core of our daily work with children, teachers and parents. We 
are creating a culture of college readiness and this is the year we will build shared understandings about what it means, and ways 
parents and teachers can work together to ensure every student works to meet his greatest potential! 
 
Excellence! Every Student! Every Day! 
 

2017-18 School Accountability Report Card for Olive Elementary School 

Page 2 of 13