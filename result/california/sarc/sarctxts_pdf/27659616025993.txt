Fremont Elementary is one of 12 schools in the Alisal Union School District. Fremont serves over 
800 students from Transitional Kindergarten through sixth grade. Our enrollment includes one 
Special Day class/General Academics (SDC), and two Life Skills, Special Education classes. In 
addition, we have one Resource Specialist Program (RSP), which includes RSP teacher and aide, 
Speech Therapist, one part-time psychologist, one full-time counselor with the Whole-Child 
Division Counseling Program, and a part time Sticks and Stones counselor. We continue to house 
two Head Start programs, one district preschool, and two Monterey County (MCOE) special 
education classes. Please note that students in early learning, and MCOE programs are not included 
in the reported number of students enrolled at Fremont School. Currently, the school consists of 
36 self-contained classrooms. The school was build in the early 1940's and as such, the grounds and 
infrastructure continue to be transformed into a modern learning environment and a significant 
resource for the community. Most recently a 2-story building was constructed that contains 16 
classrooms. On going, by phase, construction will continue to take place for a full modernization of 
our school. Each classroom is equipped with projections systems and Document Cameras. Each 
classroom also has desktop computers for student access and a laptop for teacher use including 
color printers in each classroom. All students have 1:1 access to devices (Dell Venues and Chrome 
books). 
 
Mission: 
Fremont Elementary is a collaborative community of lifelong learners that believes that EVERY child 
is able to learn; learning is a fundamental human right and as such, every student deserves an equal 
and equitable learning opportunity to receive access to high-quality education regardless of 
disability, cultural background, socioeconomic status, or gender. Students need to feel welcome to 
be able to express themselves and have a voice in what and how they will be learning in order to 
become a fully privileged citizen. ALL learners are empowered to persevere as leaders and develop 
innovative strategies to succeed academically, socially, emotionally, and behaviorally-as "Whole" 
students, where all three areas (academic, social-emotional, and behavior) are regarded as critical 
components of an integral whole-child's success. 
 

--

-- 

----
---- 
Alisal Union School District 

155 Bardin Road 
Salinas, CA 93905 
(831) 753-5700 
www.alisal.org 

 

District Governing Board 

Fernando Mercado, President 

Guadalupe Ruiz Gilpas, Vice 

President 

Robert Ocampo, Member 

Noemí M. Armenta, Member 

Antonio Jimenez, Member 

 

District Administration 

Dr. Héctor Rico 
Superintendent 

Mr. James Koenig 

Associate Superintendent, 
Business and Fiscal Services 

 

Mr. Quoc Tran 

Assistant Superintendent, 

Educational Services 

 

Mr. Ricardo Cabrera 

Associate Superintendent, 

Human Resources 

 

Dr. Jairo Arellano 

Assistant Superintendent, 

Whole Child Services 

 

 

2017-18 School Accountability Report Card for Fremont Elementary School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Fremont Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

32 

6 

0 

27 

12 

0 

27 

8 

0 

Alisal Union School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

5 

0 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Fremont Elementary School 

16-17 

17-18 

18-19