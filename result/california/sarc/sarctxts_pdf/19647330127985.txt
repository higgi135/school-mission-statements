About Us 
Ingenium Charter Middle School opened its door to the Canoga Park community in the fall of 2012, where it started providing quality 
education to approximately 80 students. ICMS has now relocated and increased its enrollment to 229 students in Winnetka, CA, only 
three miles east of the Canoga Park community. ICMS has continued to provide quality education to students enrolled in 6th through 
8th grade. Its academic program offers an array of elective classes, art, yearbook, and leadership; while also providing opportunities 
for students who need more academic support in their PLT, Personalized Learning Time classes, and grade level specific intervention 
courses which are offered through the day. In addition to providing a strong academic program during the day, ICMS offers an 
Extended Learning Program during after school hours. The ELP program consists of a time and space dedicated to further developing 
academic skills, which is supported by the day program teachers, and sports component. Students who participate in the ELP program 
sports, have the opportunity to compete against other schools and develop new skills in collaboration, sportsmanship, and time 
management. 
 
Students and staff at ICMS pride themselves in fostering a family feel, one which supports one another and has become resilient in 
moments of change and transition. Students at ICMS are respectful to one another and are constantly engaged in conversations about 
becoming agents of change for themselves and for the community that surrounds them. The faculty and staff at ICMS collaborate on 
a weekly basis to ensure proper communication and accountability of their practice which positively impacts the academic outcome 
of its students. T 
 
Our Mission 
At ICMS, we are committed to developing empowered learners by creating a nurturing community that promotes happiness and 
growth.