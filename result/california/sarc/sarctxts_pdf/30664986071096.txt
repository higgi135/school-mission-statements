A. Vision Statement 
Talbert believes in STUDENTS: collaboration, engagement, innovation, problem solving, and rigor for ALL. 
 
B. Mission Statement 
Talbert Middle School's mission is to prepare students to become articulate, thoughtful, productive, and contributing citizens of the 
future. To prepare students for the future, we must provide a safe, stimulating, inspiring educational environment which challenges 
each student's capacity to grow intellectually while at the same time respects and supports his or her need to grow socially and 
emotionally. Our mission is to provide a program where students will: 

• Use and apply higher level thinking skills. 
• Become an integral, important part of the school and community; connected in a positive way not only to their peers but 

also to the staff and school as a whole. 
Take responsibility for their learning and think independently. 

• 
• Develop the ability to make appropriate moral and ethical judgments as befits a citizen in a democratic society. 
• 

Learn and apply the characteristics of a a "Good Learner." 

Talbert Middle School offers a diversified program, featuring a wide variety of activities, both curricular and extracurricular. These 
opportunities promote high student interest, a sense of tradition, and a quality learning experience for all students. Some highlighted 
opportunities include lunch-time homeroom sports league, after school sports, Homework Club, 
 
Talbert is a STEAM (Science Technology Engineering Arts and Math) campus, providing real-world science and engineering experiences 
to seventh and eighth-grade cohorts, as well as, the rest of the campus through electives and a science fair. The STEAM program 
provides opportunities for students through field trips and hands-on experiences, including robotics, remotely operated vehicles, 
computer coding, animation, and a high level of technology integration. Furthermore, all students participate in the science fair, 
providing authentic real world problem solving in science. 
 
In addition to academics, the staff at Talbert Middle School strives to assist students in their social and personal development. Staff 
members are trained to recognize at-risk behavior in all students. The school values the importance of on-site counseling and has 
procedures in place to ensure that students receive the services they need. Staff members are devoted to helping students deal with 
problems and assisting them to reach positive goals. 
 
Additionally, Talbert Middle School is in Year 3 Implementation of Visible Learning. Visible Learning is a program focused on the factors 
that most impact student achievement, based on extensive research by Dr. John Hattie. For the 2018-19 school year, the focus is Depth 
and Complexity which will increase rigor in classroom instruction. A 5 Year Visible Learning Plan provides guidance for Talbert staff in 
regards to instruction and school culture.