Our mission at Valencia is to provide a challenging learning environment for diverse learners while 
developing upstanding character. Our educational program focuses on 21st century learning skills 
by integrating music, art, and technology. We believe in fostering communication, collaboration, 
creativity, and critical thinking. 
 
Valencia Elementary School officially changed its name to Valencia Academy of the Arts in the 
summer of 2015. During the transition, stakeholders, parents, students, staff, and community 
provided input in establishing the direction of the school. At Valencia, we have three major 
outcomes: 

Equitable Access to the Arts 

• 
• Development of Creative and Cognitive Skills through the Arts 
• Become Conscientious Global Scholars with a Foundation in the Arts 

Valencia will provide a rigorous instructional program for approximately 450 students from 
transitional kindergarten through grade five. The educational program focus reflects the school’s 
vision and mission; academic excellence, high expectations, and character development for life-
long learning and success. During the 2014-2015 school year, Valencia Academy of the Arts 
incorporated a Visual Arts and Performing Arts curriculum by providing instrumental music, choral 
music, and visual arts. Valencia is mindful of the proud tradition of strong academic programs, 
instructional excellence, and community involvement, which the school has maintained since 1951. 
Over the years, the Valencia staff and community have been continually dedicated to providing the 
best educational opportunities for every child, maintaining the rigors of Common Core, while 
providing a foundation in the arts. 
 

 

 

----

---- 

----

--

-- 

El Rancho Unified School District 

9333 Loch Lomond Dr. 
Pico Rivera, CA 90660 

(562) 801-7300 
www.erusd.org 

 

District Governing Board 

Dr. Aurora R. Villon 

Gabriel A. Orosco 

Lorraine M. De La O 

Dr. Teresa L. Merino 

Jose Lara 

 

District Administration 

Karling Aguilera-Fort 

Superintendent 

Mark Matthews 

Assistant Superintendent, Human 

Resources 

Jacqueline A. Cardenas 

Assistant Superintendent, 

Educational Services 

 

Dora Soto-Delgado 

Director, Student Services 

Reynaldo Reyes 

Director, Alternative/Adult 

Education 

Dean Cochran 

Director, Special Education 

Roberta Gonzalez 

Director, Early Learning Program 

 

2017-18 School Accountability Report Card for Valencia Academy of the Arts 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Valencia Academy of the Arts 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

19 

19 

18 

0 

0 

0 

0 

0 

0 

El Rancho Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

355 

13 

3 

Teacher Misassignments and Vacant Teacher Positions at this School 

Valencia Academy of the Arts 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.