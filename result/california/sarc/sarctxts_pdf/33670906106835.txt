The mission at Camino Real Elementary School is to enhance the ability of all students to succeed in reaching district and Common 
Core standards and to perform as responsible citizens in an ever-changing society. All students will be challenged to experience the 
joy and satisfaction of learning through the creative efforts of our faculty, parents, and the community. 
 
Camino Real Elementary School houses approximately 800 students in grades TK-6. It boasts the highest assessment scores within 
the Jurupa Unified School District and is a Title 1 Academic Achievement Award winning school. 
 
Most of Camino's students come from moderate-income homes. Approximately 15% of our students are bussed to and from school, 
while others walk with their parents. Forty percent of our student population participates in the federal Free and Reduced Lunch 
program. The school is comprised of 33 classrooms, a multipurpose room, library, and main office. Students in grades TK-3 maintain 
a 25:1 student to teacher ratio and grades 4-6 maintain a 32:1 student to teacher ratio. 
 
Camino Real serves a diverse student population of which ten languages are spoken in the homes. One hundred and forty students 
are identified as English learners, receiving ELD instruction and access to core programs in structured English immersion and 
mainstream classrooms deemed appropriate for their levels of English fluency. In addition, Camino Real has over 100 GATE (gifted 
and talented) students who received differentiated instruction within the classroom as well as after school enrichment classes. 
 
The level of parent involvement at Camino Real is very high. We have an active PTA which conducts fundraising to support educational 
experiences for our students, coordinate parent volunteers, and provide activities which extend and enhance learning experiences 
and foster school spirit. Our Fifth/Sixth Grade Booster Club provides support for sixth grade students to participate in science camp 
and other culminating activities in their final year at Camino Real. Our School Site Council, GATE, and ELAC committees meet regularly 
as representatives of community and school staff to learn about and give input into school programs and the development of the 
School Plan.