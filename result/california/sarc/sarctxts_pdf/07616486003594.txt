Fremont Elementary School is dedicated to providing a safe, supportive environment that is conducive to student achievement. Staff, 
students, parents and the community come together to create an atmosphere where students grow to become confident, 
independent learners and embrace the educational experience. Diversity is celebrated and promoted and allowed to enrich the 
community of learners. Fremont Elementary ensures excellence for all.