Prospect Necessary High School is an alternative high school in the Mt. Diablo Unified School District. We focus on small class sizes, 
close interaction with teachers and staff, and credit 
recovery. 
 
Prospect N.S.H.S. Mission 
Prospect Necessary Small High School students, who are behind in credits or prefer an alternative learning environment, will graduate 
high school in a timely manner. 
 
Vision 
The vision of Prospect High School community is to develop life-long learners, good citizens and individuals who demonstrate respect 
for others, personal integrity and perseverance in their everyday lives. 
 
Student Learning Goals 
Each student will: 
create, follow and revise a high school graduation plan 
be productive in both activity and behavior 
strive to earn a minimum of 15 credits each quarter 
demonstrate learning of course content through successful completion of assignments, assessments and projects 
 
Staff and Student Expectations 
Be Respectful 
Act with Integrity 
Strive for Excellence