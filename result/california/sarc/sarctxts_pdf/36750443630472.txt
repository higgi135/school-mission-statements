PRINCIPAL'S MESSAGE 
Hello and welcome to Mojave High School, one of two continuation high schools in the Hesperia Unified School District. Mojave High 
School was opened over 30 years ago to meet the unique needs of students who had fallen behind in credits and needed another 
opportunity to be successful in high school and beyond. Students who attend Mojave High are provided a safe, flexible learning 
environment to complete graduation requirements and explore post-secondary options. 
 
The document you are reading is Mojave High's School Accountability Report Card (SARC). In accordance with Proposition 98, every 
school in California is required to issue an annual report that fulfills state and federal disclosure requirements. In this report, parents 
and guardians will find valuable information about our academic achievement, professional staff, curricular programs, classroom 
environment and the condition of facilities. 
 
Mojave High School received a 6 year Western Association of Schools and Colleges (WASC) Accredidation and has been recognized as 
a model continuation high school in the state of California. Mojave students attend school full-time and are expected to complete the 
same requirements for graduation as all other students in the district. 
 
The teaching staff is working hard to make certain that all students receive a comprehensive, rigorous and relevant education. Many 
hands-on, active learning approaches are utilized. 
 
 
We believe Mojave High offers a unique high school experience. We hope you will take the time to review this document and perhaps 
visit our school in the future. 
 
SCHOOL PROFILE 
Hesperia Unified School District is located in the high desert region of San Bernardino County, approximately 40 miles north of the 
Ontario/San Bernardino valley. More than 20,000 students in grades kindergarten through twelve receive a rigorous, standards-based 
curriculum from dedicated and highly qualified professionals. The district is comprised of 15 elementary schools which includes 3 
choice schools. At the secondary level Hesperia has 3 middle schools, 3 comprehensive high schools, 1 alternative school, 2 
continuation high schools, 1 community day school, and 6 charter schools. 
 
SCHOOL MISSION STATEMENT 
Mojave High School exists for its students. Our alternative program fosters self-esteem, academic growth, and the life skills necessary 
in preparing our students to become responsible, contributing citizens in the 21st Century. 
 
Mojave's Vision Statement: At Mojave High School, students and their needs are placed at the core of the educational process. Our 
alternative program helps all students develop academic competence and the life skills necessary to become contributing members 
of our democratic society. 
 
Mojave High School Learner Outcomes: 
 
Mojave students have the FACTS in preparation for college, careers, and life: 
Foundation 
Achievement 
Character 
Transition 
Skills 
 

2017-18 School Accountability Report Card for Mojave High School 

Page 2 of 15