Upon graduation, Happy Camp High School students will be college and career ready. Our mission is to prepare students to meet that 
goal by providing a positive environment with useful tools for life-long learning and productivity. 
 
Major Achievements: 
 
Happy Camp High School was named as an Honor Roll school for 2015. 
 
Our school offers district-wide online classes, college classes, and Aeries - the online information system where students and parents 
can view their grades, assignments, attendance and more. 
 
Our local website serves to inform students, parents, and our community, about the activities at our school. http://www.happycamp-
highschool.com 
 
Every single student has access to the digital world throughout their day, including at home. We can offer Chromebooks and mobile 
Hot Spots for use away from school. 
 
We believe that skill set development extends beyond books. We have installed a CNC plasma cutter, five 3-d printers and we have 
updated most of our wood and welding shop. A new CNC router has been installed as well. 
 
Our projects have produced beautiful 3-D art work, furniture, metal sculptures, and repairing small engines in CTE classes such as 3-D 
imaging, metal shop and wood shop. 
 
Our students create our Yearbook and have leadership roles in many areas of their high school life with us. 
 
Our College and Career Readiness focus takes students out to see college campuses and different career fields on a face to face level. 
 
One quarter of our students attended the Presidential Inaugural address in 2017. 
 
We maintain our sports program and offer football, basketball, cheerleading, volleyball, track & field, softball, and baseball. 
 
We believe that student learning begins when relationships are nurtured. We use the Capturing Kids' Hearts system to help us toward 
creating an enjoyable, safe and academically challenging environment. 
 
We have an Athlete Committed Program that focuses students on healthy lifestyle choices in the areas of - avoiding drugs and alcohol, 
nutrition, sleep, effective training and recovery, leadership and sportsmanship. 
 
We have an active Indian Parent Committee who help us deliver a culturally sensitive academic program and provides cultural and 
college/career success programs and field trips for all our students. 
 
Parents are involved in our school through Site Council, Parent Boosters, Indian Parent Committee, LCAP Advisory Committee, and 
attendance in school conferences, programs, and events. 
 
 

2017-18 School Accountability Report Card for Happy Camp High School 

Page 2 of 12