Phil D. Swing's Mission Statement 
We, the staff and parents of Phil D. Swing Elementary, are committed to making education relevant to all students in order for them 
to reach their maximum potential by creating and maintaining an environment that ensures that every member of the school 
community reaches a high level of academic achievement as determined by local, state, and national standards. We commit to a 
comprehensive system of support to assure this outcome. 
 
Vision Statement 
Our vision is to create and sustain a family friendly atmosphere that empowers all the individuals to develop into lifelong learners 
ready to meet the challenges of the 21st Century. Phil Swing students will move beyond the basics and embrace 21st Century Skills 
which include: Communication, Collaboration, Critical Thinking, and Creativity. 
 
Phil D. Swing's Commitment 
 
1. We are committed to a Safe and Supportive Learning Environment. 
2. We are committed to College and Career Readiness. 
3. We are committed to Highly Qualified Faculty and Staff. 
4. We are committed to strong Home School and Community Partnerships. 
5. We are committed to Accountability for All.