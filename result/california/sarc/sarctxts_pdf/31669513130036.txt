Our vision statement: Be Empowered” is clear, focused and sets the tone for building the culture 
of our school. It is our mission to create a program in which every student feels empowered to 
develop purpose, values and positive habits that lead to personal and academic success. Phoenix’s 
curricular emphasis is based on the state’s common-core standards while focused on the various 
learning styles and achievement levels of our students. Student Outcomes or (ESLR’s) are the 
motivating force in curricular development and teaching methodology. The Student Outcomes are 
intended to encourage and challenge students to become the following: 

Effective communicators 

• Responsible citizens 
• 
• Academic achievers 
• Career planners 
• Healthy individuals 

 
Students complete assignments, portfolios and projects that measure progress in meeting our 
Outcome goals. To ensure completion, a senior project based on the ESLER’s is required for 
graduation. The administration and staff have high expectations for our students and strive to give 
them the opportunity to be successful and return to the comprehensive high school or graduate 
from Phoenix. 
 

----

---

- 

Western Placer Unified School 

District 

600 6th Street, Suite #400 

Lincoln, CA 95648 

(916) 645-6350 
www.wpusd.org 

 

District Governing Board 

Paul Carras 

Brian Haley 

Kris Wyatt 

Damian Armitage 

Paul Long 

 

District Administration 

Scott Leaman 
Superintendent 

Kerry Callahan 

Assistant Superintendent - 

Educational Services 

Audrey Kilpatrick 

Assistant Superintendent - 

Business 

Gabe Simon 

Assistant Superintendent - HR 

 

2017-18 School Accountability Report Card for Phoenix High School 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Phoenix High School 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

16-17 17-18 18-19 

5 

0 

0 

5 

0 

0 

5 

0 

0 

Western Placer Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

320 

0 

15 

Teacher Misassignments and Vacant Teacher Positions at this School 

Phoenix High School 

16-17 

17-18 

18-19