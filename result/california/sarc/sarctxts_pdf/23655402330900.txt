Anderson Valley Jr/Sr High School is known for its innovative approaches to learning and preparing graduates to be ready for college 
and career. The AVHS staff employ a process for systematic use of student performance data to measure the effects of school programs 
and services and to guide staff development and to modify instruction for all students. Utilizing Multi-Tiered Systems of Supports 
(MTSS) and cutting edge Project Based Learning strategies, staff is able to improve the performance of English Learners and engage 
all students in enriching academic opportunities. 
 
Anderson Valley Junior Senior High School creates and sustains a safe and effective school cuture using Positive Behavior Intervention 
and Supports (PBIS). The school offers a wide range of afterschool enrichment programs and a competitive sports program. Our school 
offers a full comprehensive A-G curriculum. The Senior Seminar course requirement includes a robust Senior Project, Service Learning, 
Job Shadowing and other college and career preparation activiites, which not only increases the rigor of the Senior year and prepares 
our graduates for college and beyond. Our school boasts a very high graduation rate and the majority of our graduates go on to two- 
and four-year colleges directly out of high school. 
 
Career Pathways opportunities include Agriculture, Art Media and Entertainment, and Construction. AP and honors course offerings 
include AP Studio Art, US History, Economics, Government, English, Calculus, Physics and Spanish Language/Culture. Students have 
many dual-enrollment courses to choose from and many opportunities to take courses for college credit. We are proud that our 
graduates leave AVHS prepared for college and careers in the 21st century. 
 
Anderson Valley Junior/Senior High School aims to inspire our school community to be lifelong learners dedicated to personal 
excellence and responsible citizenship. 
 
Through collaboration with families and our community, we are committed to: 

• 
• 

Fostering integrity, self-motivation, curiosity, and creativity in all students; 
Teaching academic excellence and career preparation, as well as cultural and technological literacy, by offering challenging 
educational opportunities; 

• Celebrating the unique diversities within our community and developing a sense of social responsibility; 
• 
• Preparing students to demonstrate leadership, self-direction, confidence, and a strong work ethic to help guarantee a 

Supporting a caring, well-trained faculty and staff who promote creative expression and critical thinking; and, 

sustainable future. 

 

2017-18 School Accountability Report Card for Anderson Valley Junior/Senior High School 

Page 2 of 21