Our School is named for Leo B. Hart, a pioneer in the field of education in Kern County and a former Shafter resident. Mr. Hart was 
the founder of Arvin Federal Emergency School and is considered one of the greatest humanitarians of his time. He is best known for 
the emergency schoolrooms he built for immigrants during the 1930s. Leo B. Hart Elementary School is located at 9501 Ridge Oak 
Drive in southwest Bakersfield. Since its opening in 1988, Hart School has enjoyed strong parent and community support. 
 
At Leo B. Hart Elementary School, "We will engage and support all students in the journey of becoming lifelong learners." Furthermore, 
this year's vision is that "Each student will progress emotionally, socially, and/or academically this year."