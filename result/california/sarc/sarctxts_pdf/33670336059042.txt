Raney Intermediate’s Mission is to lead all students to their academic and social excellence in order to be contributing members of 
the local, global, and digital community. 
 
Vision Statement: Raney is a professional learning community that will be renowned for addressing the needs of ALL learners through 
personalized learning in addition to serving as a Demonstration School for AVID, IB, and STEM. 
 
Letha Raney is one of ten intermediate schools in the Corona-Norco Unified School District. Raney is one of two intermediate schools 
that offers a sixth grade program. The school is located in the city of Corona, and is an integral part of the community. Established in 
1966, Raney Intermediate was named after the late Letha Raney, a teacher and principal in the District for over forty years. Letha 
Raney was the first junior high school principal in Corona. Currently we serve 800 students and offer three programs education 
concentration, IB, STEM, and AVID. Our priority is focused on personalized learning and knowing learners and their individual academic 
and social emotional needs. Our classrooms are connected to the Internet to foster 21st Century learning, we are currently year three 
of the district's first Bring Your Own Device digital environment where every student either brings a device or is provided with a school 
issued device and our Wi-Fi infrastructure is designed to support all students and staff. Students, staff, and parents use Canvas as a 
learning management system. Teachers are trained in the process of Professional Learning Communities and teachers spearhead 
efforts of this professional development. Our school focuses on meeting the needs of the whole child both academic and 
social/emotional. We have adopted the 8 Keys of Excellence character education program that encourages the healthy development 
of all our students. We provide an environment for learning where student needs are addressed in regular classroom settings and in 
intervention classes. We are fortunate to have teachers who are specially trained in Math and Language Arts providing interventions 
for our students. Our English Learner program works with students in developing English focusing on academic vocabulary and reading. 
Our International Baccalaureate program serves 180 students from across the district. This program is designed to fuel student 
creativity and to teach them to recognize the relationships between school subjects and the outside world. We are in year three of 
our STEM program that provides our students with an honors and non-honors STEM pathway for core subject instruction. Our STEM 
program has a focus on robotics, computer programming, and 3D printing and currently supports 100 students. Our school AVID 
program supports 180 students. AVID Students receive support in developing study skills, organization skills, and note taking skills as 
well as instruction that promotes collaborative discourse and supports student achievement through structured tutorial support, note 
taking skills, organization, grade checks, and leadership skills. 
 
Our School Accountability Report Card (SARC) is available on our school’s website address at http://www.RaneyKnights.org and is a 
valuable source of information on student performance and conditions effecting the learning environment including: 
• 
• 
• 
• 
• 

Pupil achievement, and progress toward meeting academic goals 
Progress in reducing dropout rates 
Expenditures per pupil and types of services funded 
Progress toward reducing class sizes and teaching loads 
The total number of credentialed teachers, the number relying upon emergency credentials, and the number of teachers working 
outside their subject areas of competence 
The quality and currency of textbooks and other instructional materials 
The availability of qualified personnel to provide counseling and other pupil support services 
The availability of qualified substitute teachers 
The safety, cleanliness, and adequacy of school facilities 
The adequacy of teacher evaluations and opportunities for professional improvement 
Classroom discipline and climate for learning, including suspension and expulsion rates 
Staff training and curriculum improvement 
The quality of instruction and school leadership 
The degree to which pupils are prepared to enter the work force 
The number of instructional minutes provided 
The number of minimum days scheduled 

• 
• 
• 
• 
• 
• 
• 
• 
• 
• 
• 

2017-18 School Accountability Report Card for Letha Raney Intermediate School 

Page 2 of 13