Bellevue Senior Elementary School is a 7-8 school serving approximately 200 students. It is located in the community of Atwater just 
off Highway 99. It is one of ten schools in the Atwater Elementary School District. 
 
Many of our students are learning English as a second language, therefore teaching reading and promoting the use of academic 
language is a high priority. Bellevue prides itself on its high standards of success for all students. 
 
As part of the Atwater Elementary School District, Bellevue School is committed to providing a high quality academic core program in 
an educational environment that promotes each student’s physical, intellectual, emotional, and social growth and that prepares each 
student to become a productive and responsible citizen. 
 
Students who are identified as needing remediation and additional support in English Language Arts and Math are targeted for 
academic intervention. The administrative staff and school leadership team closely monitor the implementation and effectiveness of 
the academic program by reviewing student work, observing teaching practices, analyzing assessment data, and providing direct 
feedback to staff, students and parents.