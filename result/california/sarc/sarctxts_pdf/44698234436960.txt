Costanoa High School is located on the vibrant Branciforte Small Schools Campus (BSSC). Ark 
Independent Study High School, Monarch Community School, and Alternative Family Education all 
share our campus. Costanoa students benefit from small class sizes and strong relationships with 
staff members. We focus on student growth over time by developing the following habits: Use your 
mind well. Do the right thing. Work hard. Costanoa has a diverse population that includes 100 
students in grades nine through twelve. Costanoa is fully accredited by the Western Association of 
Schools and Colleges. 
 
Our schoolwide action plan focuses on developing literacy and math skills to prepare students for 
the rigors of community college. All students complete a senior project as a graduation 
requirement. The staff has a strong culture of collegiality focused on instructional improvement 
and the promotion of student success. Our staff works together to improve and and adjust 
instruction in response to student assessment. Students work to master standards in all subjects 
and to complete required credits for graduation, the English Language Arts Portfolio, and the senior 
project. Our faculty Leadership Team and School Site Council (SSC) guide the focus and direction of 
the school. Our student leadership group also has an active voice in school activities, such as Spirit 
Week, and the development of school policies. 
 
INSPIRED PURPOSE-We personalize education for every student. 
MISSION-We are a small and diverse community that supports students’ academic and personal 
growth. Students learn through integrated thematic instruction, participate in experiential learning, 
develop a foundation in positive socioemotional practices, and build connections to Cabrillo 
College. 
 

----

---- 

Santa Cruz City Schools 
133 Mission St., Suite 100 

Santa Cruz, CA 95060 

(831) 429-3410 
www.sccs.net 

 

District Governing Board 

Sheila Coonerty 

Deedee Perez-Granados 

Cynthia Ranii 

Jeremy Shonick 

Patricia Threet 

Deborah Tracy-Proulx 

Claudia Vestal 

 

District Administration 

Kris Munro 

Superintendent 

Dorothy Coito 

Assistant Superintendent 

Educational Services 

 

Patrick Gaffney 

Assistant Superintendent 

Business Services 

 

Molly Parks 

Assistant Superintendent 

Human Resources 

 

 

2017-18 School Accountability Report Card for Costanoa Continuation High School/Branciforte Small Schools Campus 

Page 1 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Costanoa Continuation High 
School/Branciforte Small Schools Campus 
With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

Santa Cruz City Schools 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

16-17 17-18 18-19 

5 

0 

0 

7 

0 

0 

5 

0 

0 

16-17 17-18 18-19 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

314 

8 

6 

Teacher Misassignments and Vacant Teacher Positions at this School 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.