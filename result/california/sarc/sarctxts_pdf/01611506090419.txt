Palomares Elementary is committed to providing a quality environment for student learning. We 
are dedicated to success for all students, families and staff. Our goal is to provide inspiration for 
life-long learning while implementing positive, effective approaches to education that build a sound 
academic foundation and promote self-esteem and interpersonal skills. 
 
In 2015-16, Palomares Elementary received the Gold Ribbon School honor from the California 
Department of Education. In 2011-2012 and 2007-2008, Palomares Elementary School was 
recognized as a California Distinguished School. In 2005, Palomares was recognized as a NCLB 
National Blue Ribbon School. These high honors were earned through the academic efforts of the 
students, the support of the parent community, and the dedication of the staff. 
 
On April 14, 2018, Palomares Elementary School celebrated it's 150th Anniversary. Established in 
1868, the original charter was signed by President Abraham Lincoln. 
 
We celebrate learning as an engaging and dynamic experience. Students are immersed in a 
challenging standards-based curriculum in Language Arts, Mathematics, Science, History-Social 
Science, and Visual and Performing Arts. Teachers work collaboratively to ensure that a balanced 
comprehensive curriculum builds logically and consistently throughout the elementary grades. The 
Palomares staff continues to implement innovative teaching strategies to assure all students have 
the best opportunity to learn. As students interact and apply their learning to real life situations, 
they are instilled with a life-long passion to ponder ideas, set goals, share in a vision, view others' 
perspectives and challenge themselves. 
 
We value collaboration between home and school. Parents are encouraged to actively participate 
in their child's elementary school experience. Opportunities for direct involvement include 
membership on School Site Council and our Palomares Parent and Teacher Club (PPTC), 
volunteering in the classroom with special projects, teaching a lesson as a professional in one's 
field, assisting in the library, supporting school-wide fundraisers and seasonal celebrations, and 
participating in campus beautification days. Our parent community is an invaluable asset to our 
school. We believe communication is an integral link in a successful partnership, and to maintain 
that relationship, teachers and the principal are readily available for open door discussions. This 
partnership ensures that each child excels to reach his or her fullest potential. 
 
 

 

----
---- 
Castro Valley Unified School 

District 

4400 Alma Ave. 

Castro Valley, CA 94546 

(510) 537-3000 

www.cv.k12.ca.us 

 

District Governing Board 

Dot Theodore, Trustee 

Gary C. Howard, Trustee 

Jo A.S. Loss, Trustee 

Monica Lee, Trustee 

Lavender Lee Whitaker, Trustee 

 

District Administration 

Parvin Ahmadi 
Superintendent 

Dr. Jason Reimann 

Assistant Superintendent, 

Educational Services 

 

Dr. Sherri Beetz 

Assistant Superintendent, 

Human Resources 

 

Suzy Chan 

Assistant Superintendent, 

Business Services 

 

 

2017-18 School Accountability Report Card for Palomares Elementary School 

Page 1 of 9 

 

Highlights of the school program for the 2017-2018 school year include: 
 

• 
• 

Community Watershed Science Expo with over 1,000 Alameda County students attending 
Local, county, and federal partnerships that support our standards-based instructional program, which utilizes the natural school 
environment known as the Palomares Watershed Project 

Second Step (anti-bully and character education curriculum) 
Parent Academies 

Participation in Castro Valley Light Parade 

• Art in Action 
• Music Program for K-3 sponsored by our PPTC 
• Music Program for 4-5 provided by our school district 
• 
• 
• Ribbon Cutting Ceremony for newly built amphitheater, track and field and blacktop configuration 
• Hawk-a-thon 
• 
• Veterans Day Special Assembly 
• 
• 
• 
• 
• 

Fine Arts Festival 
Spring Fling (musical performance) and Art Show 
150th Anniversary Celebration 
First Place in Rowell Ranch Rodeo Parade 
Exceptionally Qualified Staff 

 
Mission Statements: 

• We promote life-long learning for our students, families, and staff. 
• We believe that learning is a complex process involving the active engagement of students in meaningful lessons that make 

connections to the world around them. 

• We provide all students with a challenging, academic, standards-based curriculum that responds to students' needs and 

strengths. 

• We actively engage students in an interdisciplinary curriculum that includes the essential elements for mastery of state and 

district standards. 

• We believe that classrooms should reflect learning environments that promote respect, sensitivity, and inclusion of diverse 

student cultures and lifestyles. 

• We encourage and enable students to demonstrate self-management skills that reflect responsible choices and respect 

success. 

• We maintain a school climate that fosters a sense of community, a joy for learning, student well-being, and academic success. 
• We recognize that for learning to take place, students need a safe, organized, and enriched environment that includes positive 

emotional support, novel challenges, and opportunities to learn at appropriate levels. 

• We value the partnership between home and school where parents are seen as active members of the school community. 
• We promote the development of collaborative partnerships with community and local agencies. 

 

 

2017-18 School Accountability Report Card for Palomares Elementary School 

Page 2 of 9 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Palomares Elementary School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

7 

1 

0 

8 

0 

0 

7 

0 

0 

Castro Valley Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

443 

6 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Palomares Elementary School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.