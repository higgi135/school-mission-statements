The South County Consortium (SoCC) consists of the nine school districts in Petaluma (Petaluma Elementary and Secondary, Old 
Adobe, Waugh, Cinnabar, Two Rock, Wilmar, Dunham, and Two Rock). There are two programs: non-intensive preschool program and 
the preschool-high school program. The non-intensive preschool program includes services for students aged 3-5 whose primary 
disability is speech and language impairment. The array of services includes speech and language therapy only, speech and language 
and occupational therapy, STRETCH program (4 days a week, for either 1 or 2 hours, and includes speech therapy, occupational 
therapy, and pre-academics). The SoCC preschool to high school program is for students with moderate to severe disabilities. They 
receive the majority of their instruction in special day classes on various campuses in Petaluma. All additional services are provided 
within the class. The Lifeskills classes are heterogeneous and include students with autism, intellectual disabilities, and speech and 
language impairment. The therapeutic support program (TSP) is for students with emotional disabilities. 
 
 
The mission of the South County Consortium is to support school districts and families in Petaluma and surrounding areas by providing 
highly specialized programs for students in preschool through high school in their own communities. We support our students by 
providing integrative, individualized and strength based programs that foster communication, critical thinking, and technology skills. 
In collaboration with school teams, families, and community we assist students in gaining independence, achievement, and skills for 
becoming positive members of the community. 
 
 
The 2017/2018 goals of the SoCC are to increase integration opportunities, incorporate mindfulness in the classroom curriculum, and 
increase technology opportunities to increase learning and engagement.