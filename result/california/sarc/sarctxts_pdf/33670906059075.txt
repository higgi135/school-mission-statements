Jurupa Middle School makes an impact on all students by empowering all students to achieve personal excellence. Our mission is for 
every student to demonstrate PRIDE. It is our desire that our students show Positivity. Respect. Integrity. Determination. Enthusiasm. 
Our school wide positive behavior support system (PBIS) supports our behavior skills. 
 
Jurupa Middle School embraces the District motto of "Learning Without Limits" by striving to empower each child to unlock their 
potential to achieve in school, career, and life by offering a true comprehensive middle school program designed to meet the 
intellectual, social, and emotional needs of students as they successfully transition from elementary school to high school. Academic 
programs and clubs include: ASB, AVID, Choir, Concert Band, cross country team, C-STEM coding and robotics, Dual Immersion, 
Drumline, Girls Who Code club, honors-level classes, Journalism, Yearbook, Video Production, Homework Club, Victory Club, and after-
school tutoring to help ensure academic success.