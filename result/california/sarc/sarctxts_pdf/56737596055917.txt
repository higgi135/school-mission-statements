MISSION STATEMENT 
 
Walnut's mission statement, "Academic Excellence in a Safe, Enriching Environment," provides a guiding light for our instructional, 
cultural, and fiscal decisions. Our motto, "Dedicated to the BEAT 2 (Bringing Education, Art, and Technology Together)”, explains our 
investment in both the arts as well as digital fluency. All stakeholders work together to develop well-rounded students equipped to 
thrive in the 21st century. The mutual efforts of staff, students, parents, and community members all contribute to Walnut’s success. 
 
SCHOOL DESCRIPTION 
 
Walnut Elementary is a neighborhood school serving approximately 300 students, TK-5th grade, in picturesque Newbury Park, CA. We 
value the family feel on our campus and we are proud of our multicultural student body. Our most important goals at Walnut are the 
academic and social development of our students. Walnut's devoted staff members share a common philosophy of excellence in 
instruction, high expectations, and the belief that the elementary experience sets the foundation for a lifetime of learning. We work 
hard to close any achievement gaps and are proud to be a California Honor Roll School. As the recipient of a Title 1 grant, Walnut 
benefits from extra federal funding used to increase student achievement, including hiring additional credentialed teachers to provide 
small group intervention and enrichment. In the spirit of collaboration, all teachers regularly meet in professional learning 
communities for professional development, data analysis, and instructional discussions, pinpointing how to better meet students' 
academic and behavioral needs. The teaching staff has implemented a school-wide intervention program called Target Time, which 
provides 30 minutes of daily, targeted, small group intervention for every student in grades 1-5. These skills-based groups are 
determined by assessment data and are reevaluated each trimester. Each small group is taught by a credentialed teacher using 
research-based curriculum and provides a "just right" fit of remediation, deeper practice, or enrichment. In order to support students 
in the social-emotional arena, our school counselor uses Target Time once per week to offer flexible social skills groups. Additional 
interventions on campus include after school classes, a credentialed academic specialist dedicated to our Kindergarten program, and 
individualized computer programs that may also be accessed at home. A strong, integrated visual and performing arts program 
enhances the core curriculum. Walnut invests in specialists to teach art, music, drama, dance, computer technology, and physical 
education classes for additional enrichment. Students gain digital fluency working with a variety of digital devices and our free after-
school Coding Club is extremely popular. We also offer a Create Club recess option that allows students to use their imaginations to 
create art projects with recycled materials. 
 
Walnut provides a nurturing, disciplined, and caring learning community. All staff members train in the CHAMPS Positive Behavior 
Intervention System, which is evident campus-wide in classrooms and common areas. Students follow our Guidelines for Success: "Be 
Safe, Be Respectful, and Be Prepared”. The STAND PROUD program, created by CVUSD elementary school counselors and taught 
throughout the year to fifth grade students, promotes positive character traits including responsibility, acceptance, and 
trustworthiness. All students and staff members have received instruction in Bucket Filling 101, based on the book, Have You Filled a 
Bucket Today? by Lisa Grimes. In addition to the intrinsic reward gained by filling each other’s emotional buckets, students receive 
positive reinforcement for their acts of kindness at awards assemblies and drawings at our monthly Wildcat Pride Gatherings. Kindness 
and Anti-bullying instruction is given at the classroom level throughout the school year, including how to get immediate help if a 
student feels bullied or is worried about a classmate. Our TK class is piloting the Sanford Harmony social-emotional learning program 
this year. Walnut participates in Unity Day and Kindness Week and we are listed as a 2018 Designated Kindness School. 
 

2017-18 School Accountability Report Card for Walnut Elementary School 

Page 2 of 14 

 

Walnut School values the surrounding community and celebrates partnerships with local groups including the Assistance League of 
Conejo Valley, CSVP Senior Volunteers, YMCA, Horace Mann, Delta Kappa Gamma, St. Julie Billiart's Catholic Church, and LightShine 
Community Church. These community members support Walnut's students by providing classroom assistance, extra school supplies, 
donated books for students to take home, and after-school homework assistance. LightShine has also "adopted" families in need at 
the holidays and volunteered on weekends for campus beautification. A Walnut alumna donates annually to support our Create Club 
and dance programs. We always enjoy working with surrounding Newbury Park cluster schools, especially welcoming Walnut alumni 
back on campus. Students from Sequoia Middle School and Newbury Park High School regularly volunteer at school events and 
participate in after-school learning activities such as homework help and Coding Club.