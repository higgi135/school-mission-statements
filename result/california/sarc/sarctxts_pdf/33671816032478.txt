District Mission Statement 
Our mission is to enrich, empower and enhance our students’lives through education. 
 
District Vision Statement 
Palo Verde Unified School District will constantly seek to improve its culture of academic excellence. 
We expect every student to read, write and calculate competently. 
We expect every teacher to apply consistent standards, evaluate student performance accurately, and coach students with diligence 
and compassion. 
We expect every parent, student and teacher to support the mutual quest for excellence. Educational progress 
can never happen without truth as its foundation, and it is to the wonderful truth of student potential and the challenging truth of 
student performance that we are unalterably committed. 
 
School Mission Statement 
We at Margaret White Elementary School will be respectful, responsible and caring citizens. We will be hardworking and pro- ductive 
problem solvers at school, at home and in our community. 
 
The SSC approves the school site plan and oversees part of the school budget. The ELAC helps students learning English feel welcome 
at school. Margaret White Elementary has very active PTC that oversees and raises funds for student activities. 
 
School-to-home communication takes place at all levels throughout the year to keep parents up-to-date on their student’s progress 
as well as school issues. The principal and PTC representatives send letters home throughout the year to keep parents apprised of 
school events, meeting dates, fundraisers, activities and parent classes. Classroom activities, instructional issues and home-support 
tips are provided in newsletters from the classroom teachers. Parents have the option of using email to communicate with their child’s 
teacher. Margaret White also holds monthly Informational Nights. The school marquee displays upcoming events and special 
announcements. The Margaret White staff often utilizes telephone voicemail distribution technology that allows tailored messages to 
be sent to the entire school to select students or groups (NEHS, grade levels, etc.). 
 
Margaret White Elementary invites parents to get involved in their child’s educational experience. Any parent who wishes to donate 
their time and talents or would like more information may contact the office or Principal April Smith at (760) 922-5159. 
 
 

2017-18 School Accountability Report Card for Margaret White Elementary School 

Page 2 of 12