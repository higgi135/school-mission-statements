“Leonardo da Vinci K-8 School is a community dedicated to the development f the whole child as a 
literate, curious, and inventive person, responsible for the preserving and creating beauty in our 
cultural and natural environments. We will foster this Renaissance child of the Arts and Sciences 
through Integrated Thematic Instruction.” 
 
Leonardo da Vinci K-8 School has rigorous gate level curriculum that extends the Common Core 
Standards. We teach the standards through the use of board adopted texts, thematic instruction 
(Highly Effective Teaching formerly called Integrated Thematic Instruction), and projects. In 
addition, we intertwine the following into each thematic unit: field trips, large end-of-unit projects, 
research projects at every grade level, science instruction, social science curriculum that connects 
to the English Language Arts Program, and math that is conceptual and functional. We are 
dedicated to visual and performing arts and the arts in integrated into our units of study. Students 
participate in site and district tests and assessments. 
 
Program highlights: We are a Parent Participation school. We have monthly evening teacher/parent 
meetings so that parents have an understanding of the upcoming curriculum, projects, field trips, 
and volunteer opportunities. We offer a range of programs that extend our school mission of 
educating the Renaissance child: Mathletes, GATE level curriculum K-6, Honors/GATE math and ELA 
classes 7-8, After-school GATE Program, 4th-8th Grade Band and Orchestra, Drama Enrichment 
Class, Student Government, K-6 Art Lab, K-6 Art Links Program, 7-8 Art Lab and Visual Art Elective, 
K-8 Science Lab, Morning Sing K-3, Art Rotation 1-3, 8-9 Field Trips, per year (every grade level), 
Competitive Athletic Program, Organic Garden, Library, Computer Labs, Culturally Inclusive 
Assembly Program, Young Author’s Competition, and Science Fair. Our students of the Renaissance 
participate in regional and local academic competitions: MESA, History Day, Speech and Debate, 
Mathletes, Nature Bowl and Quizball. We encourage our students and families to be active with 
round rounded activities to develop every aspect of their child's growth and development. 
 
There are several family and community nights: two annual science nights, Winter Fair, Harvest 
Hoedown, Renaissance Fair, Daughters of daVinci Dance, and Mother-Son Sock Hop. LdV Hosts High 
School Prep Event, Middle School High Summer Preparation event Dolphinpalooza, and District 
Athletic Events. 
 
Parents of students at Leonardo da Vinci are encouraged to contribute 40 hours a year of volunteer 
time to the educational program. Parent participation allows teachers to offer centers, workshops 
and small group instruction for both academic and enrichment exercises. We have several active 
parent leadership groups: PTC, ELAC, and SSC. Additionally, we have several parent sub 
committees: arts, culturally inclusive education, arts committee, science committee, historian and 
yearbook committee. We have several parent leadership roles, per classroom that include: class 
manager, field trip coordinator, garden chair, art links, treasurer and library. 
 
We are dedicated to our mission and vision. 
 
 

2017-18 School Accountability Report Card for Leonardo da Vinci K-8 School 

Page 1 of 12 

A. Conditions of Learning 
 
State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Leonardo da Vinci K-8 School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

34 

37 

40 

2 

0 

0 

0 

0 

0 

Sacramento City Unified School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

2007 

116 

0 

Teacher Misassignments and Vacant Teacher Positions at this School 

Leonardo da Vinci K-8 School 

16-17 

17-18 

18-19 

Teachers of English Learners 

Total Teacher Misassignments 

Vacant Teacher Positions 
* 

0 

0 

0 

0 

0 

1 

0 

0 

0 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.