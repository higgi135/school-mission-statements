Theodore Bird High School is the independent study high school in the Sonora Union High School 
District. In recognition that some students can learn best in settings other than conventional 
classrooms, Bird High School’s three teachers offer a program of independent and personalized 
instruction to meet the varying needs and ability levels of their students. All courses are taught via 
the independent study method. Students do not attend daily classes; instead, each student meets 
with an instructor one-to-one, at a prearranged time and place each week. The instructor assigns 
the student work, which is completed by the student on his/her time. 
 
Beginning in Fall 2011 and continuing into the 2018-2019 school year, Bird High School began using 
APEX Learning Systems to provide on-line instruction in some core subjects. The completed work 
is evaluated by the instructor at the next meeting, and then more work is assigned for the next 
week. It is the responsibility of the student and the parent to ensure the student attends the 
appointment and has all of his/her work completed every week. Students are individually assigned 
work to meet their performance aptitude. 
 
The school is located on the district’s Alternative Education Campus at the Dome. Bird High School 
completed the self-study process for continued WASC accreditation in the spring of 2017 where 
they received a six year accreditation. 
 
Mission Statement: Theodore Bird High School seeks to provide, within a safe and supportive 
environment, an individualized, academically challenging course of study, which will promote 
personal/social growth, foster positive relationships between students and staff, provide a 
pathway to gainful employment and/or post-secondary education. 
 

2017-18 School Accountability Report Card for Theodore Bird High School 

Page 1 of 8 

State Priority: Basic 
The SARC provides the following information relevant to the State priority: 
Basic (Priority 1): 
• 

Degree to which teachers are appropriately assigned and fully 
credentialed in the subject area and for the pupils they are teaching; 
Pupils have access to standards-aligned instructional materials; and 
School facilities are maintained in good repair 

• 
• 

Teacher Credentials 

Theodore Bird High School 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 

3 

0 

0 

1.8 

0 

0 

3 

0 

0 

Sonora Union High School District 

16-17 17-18 18-19 

With Full Credential 

Without Full Credential 

Teaching Outside Subject Area of Competence 
 

♦ 

♦ 

♦ 

♦ 

♦ 

♦ 

42 

4 

1 

Teacher Misassignments and Vacant Teacher Positions at this School 

Theodore Bird High School 

16-17 

17-18 

18-19 

Note: “Misassignments” refers to the number of positions filled by teachers 
who lack legal authorization to teach that grade level, subject area, student 
group, etc. 
*Total Teacher Misassignments includes the number of Misassignments of 
Teachers of English Learners.