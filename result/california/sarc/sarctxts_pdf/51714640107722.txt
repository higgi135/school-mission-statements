River Valley High School (RVHS) is located in an expanding portion of Yuba City and is the newest high school in the Yuba City Unified 
School District. Established in 2005, River Valley High is a Grade 9-12 comprehensive high school. The school was built in response to 
community growth in part due to an affordable housing market near a large metropolitan area (Sacramento). In recent years, growth 
in the community has waned as a result of the nationwide real estate slow-down, however, the local school population has remained 
constant. In spring of 2016, we had a WASC visit and we received a six year accreditation with a two day mid-cycle review. 
 
The school day at RVHS is structured utilizing a 4x4 block schedule, which allows for substantial periods of time to be dedicated to 
learning and instruction and less time on routine tasks such as taking roll. Four classes are held each day that are 1.5 hours in length 
and divided over two terms each school year (fall and spring terms). At the end of each term, students change both classes and 
teachers, much like college courses. The 4x4 block schedule affords a greater number of options for students, as they are provided an 
opportunity to take more courses during their four-year high school career than under the traditionally structured six-period school 
day. This allows students to advance their studies during their high school career and provides more opportunities for all students to 
explore a multitude of career pathways and/or options for college. Additionally, teachers average 90 student contacts per school day 
rather than the normal 150, allowing a more personal teaching and learning experience for students and staff. 
 
The administrative team is working with RVHS staff to improve a program designed for the English Language Learner (ELL) population. 
The program design is structured to assist ELLs to learn English quickly and to be equipped with the English language skills necessary 
for academic success. The English Language Development (ELD) staff is working toward teaching students discrete subskills in the 
reading, writing, and the conversational skills necessary to assist students in learning English quickly. The goal is for students to 
transition into core classes and meet graduation requirements on time or utilize one additional year. Content teachers also work to 
accommodate the needs of ELL students within challenging, grade-level academic courses. 
 
River Valley High School offers the opportunity for all students to be involved in some activity of their choosing. RVHS provides a 
variety of campus clubs and activities such as Link Crew for transitioning freshmen and opportunities for student leadership, as well 
as sports in the Tri County Conference. Additionally, RVHS students have been accepted to a number of major universities and colleges. 
The school community is especially proud of the many accomplishments of our graduating classes. 
 
RVHS Mission: 
 
Schoolwide Learner Outcomes: 
 
What? We Prepare all Students to be College and Career Ready. 
 
How? We Maximize Student Learning Through: Writing, Inquiry, Collaboration, Organization and Reading. (WICOR) 
 
Why? We believe successful people are: Effective Communicators, Critical Thinkers and Technologically Literate 
 
Responsible community members show: Positivity, Respect, Integrity, Determination and Excellence. (PRIDE) 
 
 
 
 

2017-18 School Accountability Report Card for River Valley High School 

Page 2 of 13