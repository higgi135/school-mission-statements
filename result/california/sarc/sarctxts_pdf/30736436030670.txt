Marjorie Veeh Elementary motivates and inspires life-long learning in a safe and well organized environment utilizing engaging 
teaching strategies in both, our general education English classes and in our Spanish Dual Language Immersion Magnet Program. 
 
Our mission at Marjorie Veeh is to provide our students with a world class education that prepares them for College and Career 
Readiness: 

Spanish Dual Language Immersion Program. 

• 
• Digital Literacy. 
• Opportunities for Cultural Diversity and Equity. 
• 
• Recognize student successes. 
• Provide students with opportunities for social, emotional growth and development. 

Intervention and enrichment programs to support all learners.