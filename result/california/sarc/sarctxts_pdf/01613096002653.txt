Principal’s Message 
The purpose of this report is to provide parents and community members with an up-to-date overview of our school and its operation. 
 
Lorenzo Manor is located in an unincorporated area of Hayward, but is part of the San Lorenzo Unified School District. Most students 
come from small single-family homes and from a number of small apartment complexes. It has a dedicated teaching staff that provides 
a well-balanced educational program aligned with state standards. Students receive a solid grounding in Language Arts, Mathematics, 
History/Social Science, and Science, and its application to the real world. This is supplemented with art, music, physical education, and 
other enrichment activities that build self-esteem and help students become good decision-makers. Critical thinking and problem 
solving strategies are stressed throughout the curriculum. Staff has undertaken a particular focus in implementation of the Common 
Core State Standards over the past 5 years. 
 
Our support staff includes one full time and one part-time English Learner Coordinator/Title I Teachers, one full-time music teacher, 
two part time music teachers, 3 part time PE teacher, one bilingual instructional assistant, a Resource Specialist and a Resource 
Instructional Assistant, a part-time Computer Media Technician and a part-time Library Media Technician. 
 
Through funding of categorical programs, as well as several grants, students participate in a number of supplemental school programs. 
1.5 Teachers on Special Assignment provide EL/Literacy coaching as well as program coordination and implementation of academic 
intervention services, including a Response to Intervention program. The ASES grant supports our on site after school Boys and Girls 
Club program for first through fifth grade students. Staff is trained in and receives support annually in use of SEAL and GLAD Strategies, 
and the site works with a consultant to provide character education and conflict resolution programs through Soul Shoppe and our 
Restorative Justice initiatives. Student Council is active in providing events to enhance a positive school climate and leadership 
opportunities for students. 
 
Lorenzo Manor’s Mission Statement: The Lorenzo Manor community empowers all students to reach their full academic potential 
and to be socially responsible, lifelong learners. 
 
Vision Statements 
1. Strive towards all students being literate in Reading and Writing and competent in Mathematics. 
2. Engage students in rigorous learning opportunities to promote problem solving and critical thinking 
3. Develop a partnership with parents, teachers, students and staff, with all actively involved in preparing students for success in a 
changing society. 
4. Create a positive and safe school climate where students consistently show respect, solve problems, and make wise choices, as well 
as cultivate an enjoyment of learning. 
5. Develop, teach, and promote empathy, social awareness and responsibility 
6. Collaborate within and across grade levels and communicate common goals 
 
 
The Lorenzo Manor School mission and vision statements are aligned with the district goals as outlined in the Local Control 
Accountability Plan (LCAP): 
1. Student Achievement: The San Lorenzo Unified School District will ensure equitable learning opportunities and outcomes for all 
students 
2. Student Engagement: The San Lorenzo Unified School District will nurture students to become committed, caring and connected 
3. Basic Services: The San Lorenzo Unified School District will provide high quality services, schools, and staff 
4. Parent Involvement: The San Lorenzo Unified School District will cultivate meaningful partnerships and authentic family engagement 
 
 

2017-18 School Accountability Report Card for Lorenzo Manor Elementary School 

Page 2 of 11