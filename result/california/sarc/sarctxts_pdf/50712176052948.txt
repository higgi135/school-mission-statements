At Las Palmas we focus on the implementation of the Common Core Standards to ensure that our students are college and career 
ready. We are providing intervention in Reading and Math in order to support our students that our having difficulty in their learning. 
Teachers are being provided staff development and collaboration time to learn about the standards and to develop quality lessons. 
We became a "No Excuses" University Recognized school as of February, 2013! The No Excuses philosophy is a set of expectations we 
have implemented to ensure that all our students have the opportunity to attend college or be prepared for the workforce after high 
school. High expectations and strong support for student learning shape the culture that defines the character of Las Palmas 
Elementary, where student success is the only option. Our students and staff believe that every student will learn, whatever it takes. 
Las Palmas School is proud to share its accomplishments in our School Accountability Report Card (SARC). 
 
Our Major Achievements include: 
We have various opportunities for parent participation in school-wide events, such as "Coffee with the Principal," Open House, Back-
to- School Night, Parent surveys and Parent Nights, ELAC and SSC and our NEU Parent Night. 
 
We promote Good Character and have Positive Behavior Supports Interventions in place for students 
 
We have student leadership, such as student council (where students hold various offices and work with a teacher to work on 
numerous student projects). We also have our Solutions Team, which looks at solving problems such as bullying and helps with conflict 
resolution. We have implemented a Buddy Bench system to help students having difficulty at school. 
 
We have implemented an Exploration Class where students are able to choose a class for the semester to learn something new. We 
have classes in: coding, art, Lego robotics, music, theatre, engineering, movement, science, environmental science and dance. 
 
School Vision: Ensure excellence in education and cultivate healthy, contributing citizens! 
 
School Mission: The Las Palmas Community believes that we have a collective responsibility to develop and maintain a positive, safe 
and unlimited learning environment. 
“The children we teach are limited only when we choose to limit ourselves.”-Debbie Miller 
 
School Goal: 
That each child experience academic success and progress by meeting their greatest potential. Whether that means they learn to read, 
move up one proficiency level, or maintain their advanced status, each child will learn and move forward. 
 
School Commitment: 
At Las Palmas we are committed to helping each student recognize their greatness so they can achieve their full potential. This means 
each child without excuse will experience academic success. We are committed to searching tirelessly for the spark that will ignite in 
each child a love for learning so that they will come to view themselves as scholars, people who are destined for higher education. 
 
 
 
 
 

2017-18 School Accountability Report Card for Las Palmas Elementary School 

Page 2 of 11