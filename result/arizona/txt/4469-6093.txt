Our mission is to work closely with parents, students, district administration, community and governing board to provide high-quality comprehensive elementary education.  This includes the acquisition of basic skills in reading, math, oral and written communication.  We are a community of learners and educators who celebrate children by creating a child-centered, emotionally safe environment.  Each child is unique, deserving of respect with something special to offer the school/community.



