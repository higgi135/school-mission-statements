Mission: At Thompson Ranch School, we prepare our students for opportunities of tomorrow by providing them with 21st Century skills of critical thinking, problem solving, collaboration, communication, and creativity to ensure a successful future.

Vision: Committed to creating caring relationships, responsible individuals, and collaborative, life-long 21st Century learners.