Desert Rose Academy, as an alternative high school providing credit recovery for students with poor academic standing, will “Honor the Promise of Education” By:
• Training students in the fundamental skills needed to graduate high school, transition into continuing education or college, and explore career choices.
• Expanding how students learn how to think.
• Creating life options / opportunities for each graduate.
