It is the mission of our school to help each and every child realize his or her full potential and become a productive and responsible citizen and lifelong learner who is able to use technology effectively and appreciate the multicultural society in which we live as we prepare for the challenges presented by the 21st century.

