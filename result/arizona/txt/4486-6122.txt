"Every Student, Every Day, Preparing for Tomorrow"  
At Clarkdale-Jerome School we: 
--Achieve high academic excellence
--Cultivate personal accountability for all
--Encourage citizenship and respect