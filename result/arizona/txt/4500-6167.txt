Tierra del Sol is a student-centered community where we embrace the strengths of each student’s talents and academic abilities through affirmation of their individuality, culture, and character to become active members of society.

