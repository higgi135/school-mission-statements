To train a Generation for Excellence.  To provide proven Back to the
Basics traditional education.  To return to a Character Counts
whole person understanding of education where positive character
traits will be modeled, expected, and taught.  To foster parental
partnering in the educational process of our children.