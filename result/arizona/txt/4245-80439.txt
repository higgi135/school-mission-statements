It is our goal to provide a caring and positive learning environment, which promotes respect and excellence in
academic and social interactions. All students shall have the opportunity to achieve their potential through
varied educational programs designed to meet their needs. Through communication and cooperation we
create an enthusiastic learning climate that honors and accepts diversity. 