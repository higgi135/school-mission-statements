Inspiring Every Child to Achieve!

We embrace the diversity of our community by creating an engaging and meaningful educational environment for all our children. We develop learners who make responsible decisions and adapt to the challenges of the future.