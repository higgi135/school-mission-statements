Our MISSION is to educate ALL students in a safe and nurturing environment where ALL children learn at high levels.

Our VISION is to empower our community to be well rounded learners, responsible decision makers and independent thinkers who show integrity and respect. 