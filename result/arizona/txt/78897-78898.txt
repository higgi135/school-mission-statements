Educating tomorrow's leaders TODAY!

It is the purpose of the Academy of Tucson to Provide, Prepare and Graduate.

Provide - A faculty of respected, encouraging and caring educators in a safe and supportive learning environment.

Prepare - Students for a knowledge-based, multicultural global economy.

Graduate - Students who are educated, responsible future leaders, contributors to society and lifelong learners.