Pinnacle Peak Preparatory School is committed to achieving high academic standards
and a lifelong love of learning. Through a partnership between school, family, and
community, students will maximize their potential using the Core Knowledge©
sequence, Common Core Standards, in conjunction with state and district standards.
As a diverse community of learners, we will encourage a cooperative and collaborative
effort to maintain a safe, healthy, and respectful environment. 