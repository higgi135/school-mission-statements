Mission Statement

In collaboration with our community and all other stakeholders, Desert Heights Charter Schools will provide a rigorous, standards based academic environment through a balance of intensive, compassionate teaching.  We are dedicated to the development of vibrant, socially-responsible individuals who possess the essential life skills necessary for success.







Vision Statement

The vision of Desert Heights Charter Schools is to promote academic excellence, integrity of character, and social awareness in each individual student.
