We Value:
*Middle School Students
*Kindness
*Integrity & Respect
*Accountability, Persistence & Responsibility
*Independent, Creative, & Critical Thinking
*Collaboration
*Diversity
*Our Community
*College Readiness & Lifelong Learning

We Believe:
*In high expectations for ourselves & our students
*In open communication
*In building caring relationships
*In rigor
*In being goal oriented and targeted
*In our families