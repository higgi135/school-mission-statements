The mission of Sam Hughes School is to create a safe and nurturing learning environment of professional educators, staff, students, their families, and community members. It is our collective responsibility to set high expectations and to ensure that learning takes place for all.

Sam Hughes School is a place where children and adults are encouraged to take risks and to experience the joy of learning.

Together, we will develop responsible, involved citizens who are motivated to think critically, to learn continuously, and are free to face the challenges of the future.