VISION 
Rancho Viejo is a community of learners with a determined focus on achievement and excellence for all.
 
EVERY STUDENT - EVERY DAY 

MISSION: To achieve our vision we will:
- Provide High Quality Instruction
- Ensure Intentional Data Analysis
- Deploy Resources Effectively
- Leverage Technology
- Build a Climate and Culture of Community
