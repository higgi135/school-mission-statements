Cortina Elementary School is a community school in the Higley Unified School District serving neighborhood kindergarten through sixth grade students. Our classrooms provide each and every child the opportunity to learn and grow in a challenging and supportive environment.

We make learning relevant and fun through discovery science, current events, hands-on mathematics and engaging students in rich literature and text.  Additionally, we provide a well rounded curriculum of physical education, art, technology, and music.

We offer a full menu of athletics, arts, and school sponsored clubs and organizations to meet the needs and interests of each student. 

Since 2017, our students' performance on the state assessments and the AZMERIT has enabled us to be labeled as an A rated school by the Arizona A-F Accountability System.  Furthermore, in 2014, Cortina was awarded the "A+ School of Excellence" Award.

We invite you and your child to join our professional teaching staff, dedicated parents and hard-working students at Cortina Elementary School where we are dedicated to life-long learning for all.