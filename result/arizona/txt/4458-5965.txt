The mission of Calabasas School is to empower all to be successful, high achieving learners. Our vision is to create S.U.C.C.E.S.S. 
Students who Understand Content and are Committed to Excellence in a Safe and Structured environment. CK8 Values: Accountability, Character, Collaboration, Communication, Continuous Learning, Excellence, Honesty, Humor, Optimism, Teamwork.
