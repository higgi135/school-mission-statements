Inspiring Every Child to Achieve!

Kenilworth School equals a community where everyone is Thinking and Learning every day.