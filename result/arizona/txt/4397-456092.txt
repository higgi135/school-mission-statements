VISION STATEMENT
Blue Ridge is the heart of our community; a family of diverse
members committed to ensuring a purposeful, creative learning
environment within a caring, collaborative culture for all.
We are rich in tradition, achievement, and innovation.
We are BR!