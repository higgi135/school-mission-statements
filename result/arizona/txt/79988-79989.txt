Growing Minds




Inspiring individual academic growth by fostering the love of learning




Guiding Hearts




Helping individuals to love themselves, one another, and their community




Giving Hope




Providing an environment for everyone to discover and pursue their passion and goals in life




Gaining Leaders




Developing servant leaders to influence the world around them, today and tomorrow
