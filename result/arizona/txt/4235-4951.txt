Mendoza Elementary 
Vision, Mission, Values and Promise

Our Vision…
Learning for LIFE    
Our Mission…
To develop social, emotional, and academic growth in every student 
Our Core Values…
At Mendoza Elementary we:
•	Are polite, respectful, accountable, well-prepared, and safe
•	Focus on learning                 
•	Embrace challenges
•	Celebrate growth
Our Promise…
At Mendoza we promise to create a learning environment in which every student feels empowered by their education.
