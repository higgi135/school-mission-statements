It is the mission of Empire High School to provide students with the tools and structure to be college-ready through challenging curriculums and a variety of educational opportunities. 

We believe: 
supportive relationships enhance learning opportunities,
a smaller student body enables a more personal educational experiences,
learning occurs inside and outside the classroom, and individual differences can be best respected and valued in a safe environment.