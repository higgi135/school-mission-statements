The mission of every school in the Tolleson Union High School District is being committed to developing the potential of all students, staff and community.

The vision of TUHSD is learning today, leading tomorrow.

Strategic Areas:
College, Career and Life Ready
Communication and Partnerships
Creating a Highly Effective Workforce