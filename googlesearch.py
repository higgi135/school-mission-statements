import configparser
import requests


class GoogleSearch():

    def __init__(self, config_file=None):
        """Initiate class and load configs."""

        # Configs loaded should include self.api_key, self.base_url, self.engine_id
        self.config_file = config_file if config_file else "search.cfg"
        self.__load_configs("search")

        self.params = {}

        self.params["cx"] = self.engine_id
        self.params["key"] = self.api_key

        self.print_items = None

        self.stop = False

    def search(self, sstring, params={}, print_items=True):
        """Search for given term and retrieve first page of results.

        args:
            sstring(str): Search string to send to google search API, should already be formatted/escaped/etc.
        kwargs:
            params(dict): Params to add or overwrite existing ones.
            print_items(bool): If true print title and url for each result.
        """
        self.print_items = print_items
        self.__update_params(params)

        self.params["q"] = sstring
        print("Now searching: {0}".format(sstring))

        self.__send()

    def __send(self):
        """Use existing parameters to send get request."""
        r = requests.get(self.base_url, params=self.params)
        scode = r.status_code

        if scode == 200:

            self.results = r.json()
            if self.results is None:
                print("Retrieved no results.")

            self.__print_results()

        else:

            message = "Search failed with code {0}".format(scode)
            print(message)
            self.results = None
            print(r.content)
            self.stop = True

    def __print_results(self):
        """Print results of search or other operation."""
        totalresults = self.results["queries"]["request"][0]["totalResults"]
        print("Returned {0} results".format(totalresults))

        if self.print_items:
            self.__print_items()

    def __print_items(self):
        """Print item summaries for search results."""
        items = self.results["items"]
        for idx, item in enumerate(items):

            print("  {0} {1}".format(idx + 1, item["title"]))
            print("    {0}".format(item["link"]))

    def __update_params(self, params):
        """Update existing params.

        args:
            params(dict): dict where key and value correspond to google api params.
        """
        for k, v in params.items():
            self.params[k] = v

    def __load_configs(self, section):
        """Load configs by section.

        args:
            section(str): must correspond to a section name in the associated config file.
        """
        config = configparser.ConfigParser()
        config.read(self.config_file)

        keys = config[section]

        for key in keys:
            value = keys[key]
            setattr(self, key, value)
